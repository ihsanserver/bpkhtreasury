def showQuery(sender,app):
    app.UserAppObject.UserActivityLog('SELECT MENU', sender.Caption, '')
    FormFullPath = sender.StringTag
    formname = FormFullPath.split("/").pop()
    fc = app.FindForm(formname)
    if fc == None:
      form = app.CreateForm(FormFullPath,formname,2,None,None)    
      form.Show()
    else:
      fc.Show()
#--

def showQuery_3(sender,app):
  app.UserAppObject.UserActivityLog('SELECT MENU', sender.Caption, '')
  FormFullPath = sender.StringTag
  vType = sender.NumberTag
  add_info = sender.Name
  formname = FormFullPath.split("/").pop()
  container = app.FindForm(formname)
  if container == None:
    ph = app.CreateValues(['add_info', add_info])
    container = app.CreateForm(FormFullPath,formname,vType,ph,None)    
  container.Show()
#--

def ShowForm_2(sender,app):
    app.UserAppObject.UserActivityLog('SELECT MENU', sender.Caption, '')
    FormFullPath = sender.StringTag
    add_info = sender.Name
    formname = FormFullPath.split("/").pop()
    ph = app.CreateValues(['key', 'R'],['add_info', add_info],['vMode', 'R'])
    form = app.CreateForm(FormFullPath,formname,0,ph,None)    
    form.Show()
#--

def ShowForm_1(sender,app):
    app.UserAppObject.UserActivityLog('SELECT MENU', sender.Caption, '')
    FormFullPath = sender.StringTag
    add_info = sender.Name
    formname = FormFullPath.split("/").pop()
    #ph = app.CreateValues(['key', 'R'],['add_info', add_info],['vMode', 'R'])
    form = app.CreateForm(FormFullPath,formname,0,None,None)    
    form.Show()
#--

def showQuery2(sender,app):
  app.UserAppObject.UserActivityLog('SELECT MENU', sender.Caption, '')
  FormFullPath = sender.StringTag

  formname = FormFullPath.split("/").pop()
  container = app.FindForm(formname)
  if container == None:
    form = app.CreateForm(
      FormFullPath,
      formname,
      2,
      None,
      None
    )
    container = form.FormContainer
  container.Show()  
#-- 
    
def ShowForm(sender,app):
  app.UserAppObject.UserActivityLog('SELECT MENU', sender.Caption, '')
  FormFullPath = sender.StringTag
  vType = sender.NumberTag or 0

  formname = FormFullPath.split("/").pop()
  form = app.CreateForm(
    FormFullPath,
    formname,
    vType,
    None,
    None
  )
  form.FormContainer.Show()

def mnuOtorisasiClick(sender, context):
  context.UserAppObject.UserActivityLog('SELECT MENU', sender.Caption, '')
  formid = 'enterprise://monitoringAdministration/fListOtorisasi'
  fContainer = context.FindForm(formid)
  if fContainer == None:
    form = context.CreateForm(formid, formid, 2, None, None)
    form.initParameters("treasury", "TRE")
    fContainer = form.FormContainer
  fContainer.Show()
#--

def RegPlacingClick(menuitem, app):
  app.UserAppObject.UserActivityLog('SELECT MENU', menuitem.Caption, '')
  frm = app.CreateForm('fRegAccount', 'fRegAccount', 0, None, None)
  kode_entri = menuitem.StringTag
  frm.defineUIFromCode(kode_entri)
  frm.activate()
  frm.FormContainer.Show()
#--

def RegDepositoClick(menuitem, app):
  app.UserAppObject.UserActivityLog('SELECT MENU', menuitem.Caption, '')
  frm = app.CreateForm('deposito/fRegAccount', 'fRegAccount', 0, None, None)
  kode_entri = menuitem.StringTag
  frm.defineUIFromCode(kode_entri)
  frm.activate()
  frm.FormContainer.Show()
#--

def RegReksadanaClick(menuitem, app):
  app.UserAppObject.UserActivityLog('SELECT MENU', menuitem.Caption, '')
  frm = app.CreateForm('reksadana/fRegAccount', 'fRegAccount', 0, None, None)
  kode_entri = menuitem.StringTag
  frm.defineUIFromCode(kode_entri)
  frm.activate()
  frm.FormContainer.Show()
#--

def RegSukukClick(menuitem, app):
  app.UserAppObject.UserActivityLog('SELECT MENU', menuitem.Caption, '')
  frm = app.CreateForm('sukuk/fRegAccount', 'fRegAccount', 0, None, None)
  kode_entri = menuitem.StringTag
  #frm.defineUIFromCode(kode_entri)
  #frm.activate()
  frm.FormContainer.Show()
#--

def RegBorrowingClick(menuitem, app):
  app.UserAppObject.UserActivityLog('SELECT MENU', menuitem.Caption, '')
  frm = app.CreateForm('fRegBorrowing', 'fRegBorrowing', 0, None, None)
  kode_entri = menuitem.StringTag
  frm.defineUIFromCode(kode_entri)
  frm.activate()
  frm.FormContainer.Show()
#--

def mnuOverrideClick(sender, context):
  context.UserAppObject.UserActivityLog('SELECT MENU', sender.Caption, '')
  formid = 'enterprise://monitoringAdministration/fListOverride'
  fContainer = context.FindForm(formid)
  if fContainer == None:
    form = context.CreateForm(formid, formid, 2, None, None)
    form.initParameters("treasury", "TRE")
    fContainer = form.FormContainer
  fContainer.Show()
  
def dataauditclick(sender, context):
  context.UserAppObject.UserActivityLog('SELECT MENU', sender.Caption, '')
  formid = 'enterprise://monitoringAdministration/fDataAudit_Query'
  form = context.FindForm(formid)
  if form == None:
    form = context.CreateForm(formid, formid, 2, None, None)
    form.initParameters("treasury", "TRE")
  #--
  
  form.Show()
#--

def reportEODClick(menuitem, app):
  app.UserAppObject.UserActivityLog('SELECT MENU', menuitem.Caption, '')
  frmid = 'report/fReportPOD' 
  app.CreateForm(frmid, frmid, 0, None, None).Show()    
#--
  
