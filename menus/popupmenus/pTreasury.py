def EfektifkanClick(menuitem, context):
  formid = 'Transaksi/Transfer/fEfektifkanSetoranNotaDebit'
  Id_NotaDebitOutward = context.GetFieldValue('NotaDebitOutward.Id_NotaDebitOutward')

  app = context.OwnerForm.ClientApplication
  ph = app.CreateValues(
    ['Modus_SetData', 2],
    ['Id_NotaDebitOutward', Id_NotaDebitOutward]
  )
  form = app.CreateForm(formid, formid, 0, ph, None)
  form.Show()

