def AddClickCounterpart(item, context):
  app = context.OwnerForm.ClientApplication
  
  f = app.CreateForm('Treasury/fCounterpartNew', 'fCounterpartNew', 0, None, None)
  f.Show()
  
def ViewClickCounterpart(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fCounterpartView', 'fCounterpartView', 0, ph, None)
  frm.Show()
  
def EditCounterpartClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fCounterpartEdit', 'fCounterpartEdit', 0, ph, None)
  frm.Show()
  context.Refresh()
  