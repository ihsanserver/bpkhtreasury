def AddClickPlacing(item, context):
  app = context.OwnerForm.ClientApplication
  
  f = app.CreateForm('Treasury/fPlacingNew', 'fPlacingNew', 0, None, None)
  f.FormContainer.Show()
  
def mnuPeragaanClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key], ['act_type', 'V'])
  frm = app.CreateForm('fViewAccount', 'fViewAccount', 0, ph, None)
  #frm.SetNomorRekening(key)
  frm.FormContainer.Show()
  #context.Refresh()
    
def EditClickPlacing(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key])
  frm = app.CreateForm('fEditAccount', 'fEditAccount', 0, ph, None)
  #kode_entri = sender.StringTag
  #frm.defineUIFromCode(kode_entri)
  #frm.activate()
  #frm.SetNomorRekening(key)
  frm.FormContainer.Show()
  context.Refresh()
  
def TutupAccountClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key], ['act_type', 'C'])
  frm = app.CreateForm('fViewAccount', 'fViewAccount', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()
