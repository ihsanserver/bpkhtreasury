def AddClickBorrowing(item, context):
  app = context.OwnerForm.ClientApplication
  
  f = app.CreateForm('Treasury/fBorrowingNew', 'fBorrowingNew', 0, None, None)
  f.FormContainer.Show()
  
def ViewClickBorrowing(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  #app.ShowMessage(key)
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fBorrowingView', 'fBorrowingView', 0, ph, None)
  frm.Show()
  context.Refresh()
  
def CetakClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  context.OwnerForm.pyFormObject.CetakSertifikat(key)
  
def EditClickBorrowing(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fBorrowingEdit', 'fBorrowingEdit', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()
  
def PencairanClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fBorrowingPencairan', 'fBorrowingPencairan', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()
  
def BagiHasilClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fBorrowingBagiHasil', 'fBorrowingBagiHasil', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()
  
def TambahBagiHasilClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fBorrowingBagiHasil', 'fBorrowingBagiHasil', 0, ph, None)
  frm.Show()
  context.Refresh()  