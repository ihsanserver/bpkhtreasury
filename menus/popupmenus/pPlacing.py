def AddClickPlacing(item, context):
  app = context.OwnerForm.ClientApplication
  
  f = app.CreateForm('Treasury/fPlacingNew', 'fPlacingNew', 0, None, None)
  f.FormContainer.Show()
  
def ViewClickPlacing(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fPlacingView', 'fPlacingView', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()
  
def EditClickPlacing(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fEditPlacing', 'fEditPlacing', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()
  
def PencairanClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fPlacingPencairan', 'fPlacingPencairan', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()
  
def BagiHasilClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.KeyObjConst
  
  ph = app.CreateValues(['key', key])
  frm = app.CreateForm('Treasury/fPlacingBagiHasil', 'fPlacingBagiHasil', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()