def mnuPeragaanClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key], ['act_type', 'V'])
  frm = app.CreateForm('hajj/fViewAccount', 'hajj/fViewAccount', 0, ph, None)
  #frm.SetNomorRekening(key)
  frm.FormContainer.Show()
  #context.Refresh()
    
def EditClickPlacing(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key])
  frm = app.CreateForm('fEditAccount', 'fEditAccount', 0, ph, None)
  #kode_entri = sender.StringTag
  #frm.defineUIFromCode(kode_entri)
  #frm.activate()
  #frm.SetNomorRekening(key)
  frm.FormContainer.Show()
  context.Refresh()
  
def TutupAccountClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key], ['act_type', 'C'])
  frm = app.CreateForm('hajj/fViewAccount', 'hajj/fViewAccount', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()
  
def BATransactionClick(sender, context):
  app = context.OwnerForm.ClientApplication
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key])
  frm = app.CreateForm('hajj/fTransaksiHajjSA', 'hajj/fTransaksiHajjSA', 0, ph, None)
  #frm.FormContainer.Show()
  frm.Show()
  context.Refresh()
