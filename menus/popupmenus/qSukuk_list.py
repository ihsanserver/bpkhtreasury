
def AddClickPlacing(item, context):
  app = context.OwnerForm.ClientApplication
  app.SetLocalResourceMode(0)
  f = app.CreateForm('Treasury/fPlacingNew', 'fPlacingNew', 0, None, None)
  f.FormContainer.Show()
  
def mnuPeragaanClick(sender, context):
  app = context.OwnerForm.ClientApplication
  app.SetLocalResourceMode(0)
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key], ['act_type', 'V'])
  frm = app.CreateForm('sukuk/fViewAccount', 'sukuk/fViewAccount', 0, ph, None)
  #frm.SetNomorRekening(key)
  frm.FormContainer.Show()
  #context.Refresh()
    
def EditClickPlacing(sender, context):
  app = context.OwnerForm.ClientApplication
  app.SetLocalResourceMode(0)
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key], ['act_type', 'U'])
  frm = app.CreateForm('sukuk/fEditAccount', 'fEditAccount', 0, ph, None)
  #kode_entri = sender.StringTag
  #frm.defineUIFromCode(kode_entri)
  #frm.activate()
  #frm.SetNomorRekening(key)
  frm.FormContainer.Show()
  context.Refresh()
  
def TutupAccountClick(sender, context):
  app = context.OwnerForm.ClientApplication
  app.SetLocalResourceMode(0)
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key], ['act_type', 'C'])
  frm = app.CreateForm('sukuk/fViewAccount', 'fViewAccount', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()
  
def KuponClick(sender, context):
  app = context.OwnerForm.ClientApplication
  app.SetLocalResourceMode(0)
  key = context.GetFieldValue('Nomor_Rekening')
  ph = app.CreateValues(['nomor_rekening', key], ['act_type', 'M'])
  frm = app.CreateForm('sukuk/fViewAccount', 'fViewAccount', 0, ph, None)
  frm.FormContainer.Show()
  context.Refresh()
