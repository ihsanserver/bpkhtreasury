import sys
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.modman as modman
import rpdb2

import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.net.message as message
modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipTrx;listMember"): return

  uideflist.PrepareReturnDataset()
  rec = uideflist.uipTrx.Dataset.AddRecord()
  rec.kode_jenis = 'TRC'
  rec.kode_entri = 'TRU001'
  #--

def reject(config, params, returns):
  pass
  #oFinAgent.status_update = 'F' 

def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000', True, True)
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  
  rec = params.uipTrx.GetRecord(0)
  config.BeginTransaction()
  try :
    nomor_ref = rec.nomor_ref.strip()
    
    recOtor = otorisasi.prepareParameterOtorisasi(params)
    recOtor.kode_entri = 'TRU001'
    recOtor.keterangan = "Transaksi Umum Treasury, Nomor Ref: %s" % (nomor_ref)
    recOtor.info1 = str(nomor_ref)
    recOtor.info2 = None
    recOtor.tipe_target = 0
    recOtor.id_peran = "OM"
    recOtor.nama_tabel = 'Transaksi'
    recOtor.tipe_key = 1
    recOtor.key_string = str(nomor_ref)
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph) 

    
    config.FlushUpdates() # cek generate SQL
    config.Commit()
    isErr = 0
    errMsg = ""
  except :
    config.Rollback()
    isErr = 1
    errMsg = str(sys.exc_info()[1])
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg])
#--  

## save, authorize & reject NEW
def authorize(config, params, returns):
  try:
    #rpdb2.start_embedded_debugger('000')  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject

    UploadTrx = modman.getModule(config,'UploadTrx')
    UploadTrx.upl_trx_treasury(config, params)
    # process other fields here 

    #oAsset.status_update = 'F'
    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--  
  
