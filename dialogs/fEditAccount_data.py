import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.util.dbutil as dbutil
import com.ihsan.net.message as message
import sys
import com.ihsan.util.modman as modman
import com.ihsan.util.debug as debug
import rpdb2
#rpdb2.start_embedded_debugger('000', True, True)
modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

class record: pass
                                                                           
def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  mlu = config.ModLibUtils
  helper = pobjecthelper.PObjectHelper(config)
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipHelper;uipTreasury"): return
  tglIndo = modman.getModule(config,'TglIndo')
  fr = params.FirstRecord
  if fr.Dataset.IsFieldExist('nomor_rekening'):
    nomor_rekening = fr.nomor_rekening
  else:
    raise Exception, 'Invalid parameter'
    
  #raise Exception, 'nomor_rekening: %s' % fr.act_type
  oPeriodHelper = helper.CreateObject("core.PeriodHelper")
  tgl_acc = oPeriodHelper.GetAccountingDate()
    
  try:
    pobjconst = 'PObj:TreasuryAccount#nomor_rekening=' + nomor_rekening
    uideflist.SetData('uipTreasury', pobjconst)
    
    oTreasury = helper.GetObject("TreasuryAccount", nomor_rekening)
    oTreasury = oTreasury.CastToLowestDescendant()
    ds = uideflist.uipTreasury.Dataset
    if ds.RecordCount < 1:
      raise Exception, 'Data not found or data integrity error. nomor_rekening = %s' % nomor_rekening
    rec = ds.GetRecord(0)
    rec.basis_hari_pertahun = oTreasury.LProduk.basis_hari_pertahun
    rec.caption = 'Edit Data Account %s' % (oTreasury.getJenisTreasury())    
    if rec.tgl_jatuh_tempo not in [None,0] and rec.tgl_jatuh_tempo <= tgl_acc:
      rec.label_jt ='ACCOUNT SUDAH JATUH TEMPO TANGGAL '+tglIndo.tgl_indo(config,rec.tgl_jatuh_tempo,2)
      
  except:
    raise Exception, debug.getExcMsg()
  
  if oTreasury.status_rekening==3:
    raise Exception, "TIDAK BISA MELAKUKAN TRANSAKSI\nREKENING %s SUDAH LUNAS / TUTUP" % (nomor_rekening)

#-- 
def authorize(config, params, returns):
  try :
    #rpdb2.start_embedded_debugger('000', True, True)  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    rec = params.uipTreasury.GetRecord(0)
    dsAkad = params.GetDatasetByName('uipData')
    recAkad = dsAkad.GetRecord(0)
    ph = {'recParent': rec, 'recAkad': recAkad}
    
    oTreasury = helper.GetObject("TreasuryAccount", rec.nomor_rekening)
    oTreasury = oTreasury.CastToLowestDescendant()
    
    # info user transaksi / otor
    dsInfo = params.__inputinfo
    recInfo = dsInfo.GetRecord(0)
    oTreasury.id_otorentri = recInfo.id_otorisasi
        
    # process transaksi edit         
    oTreasury.editTreasury(ph)                                    
    
    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--  
  
def reject(config, params, returns):
  pass
    
def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000')
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  rec = params.uipTreasury.GetRecord(0)
  override_state = ''
  config.BeginTransaction()
  try :
    recOtor = otorisasi.prepareParameterOtorisasi(params)
    recOtor.kode_entri = 'TREDIT'
    recOtor.keterangan = "%s - %s" % (rec.caption, rec.nomor_rekening)
    recOtor.info1 = rec.nomor_rekening
    #recOtor.info2 = None
    recOtor.tipe_target = 0
    recOtor.id_peran = "OM"
    recOtor.nama_tabel = rec.contract_class_name
    recOtor.tipe_key = 1
    recOtor.key_string = rec.nomor_rekening
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    resOtor = app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph)
    
    id_otorisasi = resOtor.FirstRecord.id_otorisasi
    override_state = setOverride(config, id_otorisasi) 
    
    isErr = 0
    errMsg = ""
    config.Commit()
  except :
    config.Rollback()
    isErr = 1
    errMsg = "%s.%s" % (str(sys.exc_info()[0]), str(sys.exc_info()[1]))
    nomor_rekening = ""
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg], ['nomor_rekening', rec.nomor_rekening], ['override_state', override_state])
#--  
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def setOverride(config, id_otorisasi):
  dParam ={
    'otorentri': config.MapDBTableName("enterprise.otorentri"),
    'id_otorisasi':id_otorisasi
  }
  dbutil.runSQL(config, '''
    UPDATE %(otorentri)s SET override_state='F' WHERE id_otorisasi=%(id_otorisasi)s
  ''' % dParam )
  override_state = 'F'
  
  return override_state
  