import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.fileutils as utils
import sys
import os
from os.path import getsize

def FormSetDataEx(uideflist, parameter):
  config = uideflist.Config
  helper = phelper.PObjectHelper(config)
    
  if parameter.DatasetCount <= 0 :  
    Id_User = config.SecurityContext.InitUser
    recParam = uideflist.uipInput.dataset.AddRecord()
    
    oUser = helper.GetObject('Enterprise.User', Id_User)
    if oUser.IsNull: raise Exception, 'User data undefined.'
    recParam.tipe_akses_cabang = oUser.tipe_akses_cabang

    periodHelper = helper.CreateObject('core.PeriodHelper')
  
    oToday = periodHelper.GetToday()
    float_oToday = oToday.GetDate()
          
    recParam.Tanggal_Produksi = float_oToday        
  else:      
    rec = parameter.FirstRecord
    if rec == None: return
    s_tgl = uideflist.Config.FormatDateTime('yyyymmdd', rec.tanggal)
    
    path = 'c:/dafapp/rawdata/jemaah/%s' % s_tgl
    
    aListFile = utils.GetListOfFileName(path, ['csv','xls','xlsx'], 1)
    
    for aName in aListFile:
      rec = uideflist.uipReport.Dataset.AddRecord()
      rec.NamaFile = aName
      filepath = "%s/%s" % (path, aName)
      sizeFile = (getsize(filepath)/1024.0)
      rec.sizeFile = "%s KB" % config.FormatFloat(',0.00', sizeFile)
    #-- for

def LoadReport(config, parameters, returnpacket):
    status = returnpacket.CreateValues(["Is_Err", 0],["Err_Message", ""])

    try:
      helper = phelper.PObjectHelper(config)      
      rec = parameters.FirstRecord
      s_tgl = config.FormatDateTime('yyyymmdd', rec.tanggal)
      
      path = 'c:/dafapp/rawdata/jemaah/%s' % s_tgl
      filename = path + "\\" + rec.filename

      sw = returnpacket.AddStreamWrapper()
      sw.LoadFromFile(filename)
      sw.MIMEType = config.AppObject.GetMIMETypeFromExtension(filename)

    except:
      status.Is_Err = 1
      status.Err_Message = str(sys.exc_info()[1])


 