class fReportPOD:

  def __init__(self, formObj, parentForm):
    self.form = formObj
    self.app = formObj.ClientApplication
    self.oPrint = self.app.GetClientClass('PrintLib','PrintLib')()

  def bRefreshClick(self, sender):
    self.uipReport.ClearData()
    
    ph = self.app.CreateValues(
      ['tanggal', self.uipInput.Tanggal_Produksi]
    )
    self.FormObject.SetDataWithParameters(ph)
    
  def bDownloadClick(self, sender):
    self.ExecReport(0)

  def bPrintClick(self, sender):
    self.ExecReport(3)
    
  def ExecReport(self, Action):
    if self.uipReport.RecordCount == 0 :
      raise Exception, '\n\nTidak ada data yang di tampilkan'
          
    # Action : 1 = peragaan , 2 = print
    ph  = self.app.CreateValues(
      ['tanggal', self.uipInput.Tanggal_Produksi],
      ['filename', self.uipReport.NamaFile])

    res = self.form.CallServerMethod("LoadReport", ph)

    status = res.FirstRecord
    if status.Is_Err: raise Exception, 'PERINGATAN:' + status.Err_Message

    if Action == 2 :
      self.app.ShowMessage('Masukkan kertas ke dalam printer')
  
    # 1= peragaan , 2 = print
    self.oPrint.doProcess(self.app, res.packet, Action)

  def Show(self):
    #if self.uipInput.Cabang_User == '000':
    #  self.pFilter_LCabang.enabled = 1
    
    self.FormContainer.Show()
    
