class fInputNewAccount:
  arrValidationDef = [['nilai_nav', 'num', 'value > 0', 'Nilai Nav harus diisi dan lebih besar dari 0']
    , ['harga_beli', 'num', 'value > 0', 'Harga Beli harus lebih besar dari 0']
  ]
  
  def __init__(self, formObj, parentForm):
    self.parentForm = parentForm
    pass
  #--
  
  def GetArrValidationDef(self):
    return self.arrValidationDef
  
  def OnEnterNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnEnterNum(sender)
  #--
  
  def OnExitNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnExitNum(sender)
  #--    

  def nilai_navOnExit(self, sender):
    uipParent = self.parentForm.uipTreasury
    self.uipData.harga_beli = uipParent.nominal_deal/self.uipData.nilai_nav
    