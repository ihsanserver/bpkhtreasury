import com.ihsan.util.otorisasi as otorisasi
import sys
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman

modman.loadStdModules(globals(), ['libs'])

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipData"): return
  
  uideflist.PrepareReturnDataset()
  rec = uideflist.uipData.Dataset.AddRecord()
  rec.nom_ppap = 0
