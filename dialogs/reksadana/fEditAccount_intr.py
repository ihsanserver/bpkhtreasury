class fInputNewAccount:
  arrValidationDef = [['nilai_nav', 'num', 'value > 0', 'Nilai Nav harus diisi dan lebih besar dari 0']
    , ['nilai_unit', 'num', 'value > 0', 'Nilai Unit harus lebih besar dari 0']
    , ['harga_beli', 'num', 'value > 0', 'Harga Beli harus lebih besar dari 0']
    , ['nomor_ref_deal', 'str', 'len(value) > 0', 'Nomor Referensi harus diisi']
    , ['kolektibilitas', 'num', 'value > 0', 'Kolek Belum Dipilih']
  ]
  
  def __init__(self, formObj, parentForm):
    self.parentForm = parentForm
    pass
  #--
  
  def GetArrValidationDef(self):
    return self.arrValidationDef
  
  def OnEnterNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnEnterNum(sender)
  #--
  
  def OnExitNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnExitNum(sender)
  #--
  
  def nilai_navOnExit(self, sender):
    self.count_nilaitunai()
    
  def count_nilaitunai(self):
    uip = self.uipData
    uip.Edit()
    uip.nilai_tunai_sekarang = uip.nilai_unit*uip.nilai_nav
  
  def count_ppa(self):
    uip = self.uipData
    uip.Edit()
    uip.nom_ppap = uip.nilai_tunai_sekarang*0.1

  def nilai_tunai_sekarangOnExit(self, sender):
    self.count_ppa()
    