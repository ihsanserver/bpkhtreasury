class fFinCustGroups:
  
  # format : fieldname, type:str|num, acceptedCondition, errMsg
  arrDefValidation = [ ['group_code', 'str', 'len(value) > 0', 'Kode Group belum diinput'] 
      , ['group_name', 'str', 'len(value) > 0', 'Nama Group belum diinput'] 
      , ['LNasabah.Nomor_Nasabah', 'str', 'len(value) > 0', 'Nasabah Induk Belum Input']
 ]

  arrCostValidation = [ ['LMember.Nomor_Nasabah', 'str', 'len(value) > 0', 'Anggota Group Belum Input'] ]
 
  _cols = [ 'LMember.Nomor_Nasabah','LMember.Nama_Nasabah','description', 'expossure' ]

  dRef = {
     'Lgroup_city' : 'DATI2'
  }
  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    pass
  #--  
  
  def getValidationMsg(self, uip, list_val):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    #uip = self.FinProduct
    # generic form
    arrErrMsg = userApp.doValidation(uip, list_val)
    
    return arrErrMsg
  #--   

  def Show(self, vMode):
    uip = self.FinCustGroups
    uip.Edit()
    uip.jenis_aksi = vMode
    #uip.product_code = uip.kode_produk
    if vMode =='R':
      self.FormObject.Caption = 'Tambah Group Nasabah'
      #self.pFinProduct_product_code.Enabled = True

    elif vMode =='E':
      self.FormObject.Caption = 'Ubah Group Nasabah'
      self.pFinCustGroups_group_code.ReadOnly=True
      
    elif vMode =='V':
      self.FormObject.Caption = 'Lihat Detil Group Nasabah'
      self.pFinCustGroups.SetAllControlsReadOnly()
      self.pAction_btOk.Enabled = False
      self.pMember.Visible=0
      #self.FormObject.PyFormObject.multipages1.GetPage(1).TabVisible = 1

    self.FormContainer.Show()

  def saveClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uip = self.FinCustGroups
    
    arrErrMsg = self.getValidationMsg(uip, self.arrDefValidation)
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else :        
      form.CommitBuffer()
      params = form.GetDataPacket()

      resp = form.CallServerMethod('save', params)    
      status = resp.FirstRecord
      if status.isErr:
        app.ShowMessage('Error :' + status.errMsg)
      else:
        app.ShowMessage('Berhasil disimpan')    
        sender.ExitAction = 1
      
  def defineUIFromCode(self, kode_entri):
    pass
  #--

  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False

  def activate(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    
  def LNasabahOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    res = uapp.checkStdLookup(sender, nField, '''nomor_nasabah;nama_nasabah;Limit_Nom_Tarik_Tunai''' )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "rekeningliabilitas@lookupNasabahPlus", nField, "nomor_nasabah;nama_nasabah;Limit_Nom_Tarik_Tunai")
    if nField=='LMember':
      uip = self.uipData
      uip.Edit()
      uip.expossure = uip.GetFieldValue(nField+'.Limit_Nom_Tarik_Tunai')

    return res

  def LFinCompanionOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    res = uapp.checkStdLookup(sender, nField, '''comp_nik;comp_name''' )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "rekeningfinancing@lookupPendamping", nField, "comp_nik;comp_name")

    return res

  def bAddOnClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uip = self.uipData
    uig = self.FinCustGroups
    
    CostErrMsg = self.getValidationMsg(uip, self.arrCostValidation)

    if len(CostErrMsg) > 0 :
      CosterrMsg = "\n".join(CostErrMsg)
      app.ShowMessage(CosterrMsg)
    else:
      uig.Edit()
      uig.group_expossure += uip.expossure
      self.IsiGridDetail()
  
  def ReffOnExit(self, sender):
    form = sender.OwnerForm
    uip = self.FinCustGroups
    uapp = form.ClientApplication.UserAppObject
    nField = sender.Name

    res = uapp.checkStdLookup(sender, nField, 
      '''reference_code;reference_desc;refdata_id'''
    )
    if res != None:
      return res
      
    kategori = self.dRef[nField]
    ref_lookup = "reference@refJoinMapCat"
    uapp = form.ClientApplication.UserAppObject
    res = uapp.stdLookup(sender, ref_lookup, nField, 
      "reference_code;reference_desc;refdata_id", 
      None,
      {
        'reference_code':uip.GetFieldValue('%s.reference_code' % nField),
        'reference_name':'',
        'nama_kategori':kategori,
        'addCond':''
      })
      
    return res

  def bEditOnClick(self, sender):
    grid = self.listMember
    uig = self.FinCustGroups
    uip = self.uipData
    uip.Edit()
    for i in self._cols:
      n = i.split(';')
      col_name = val_name = n[0]
      if len(n) >1: val_name=n[1];
      uip.SetFieldValue(val_name,grid.GetFieldValue(col_name))
    
    uip.fMode = 'edit'
      
    uig.Edit()
    uig.group_expossure -= uip.expossure
    self.pMember_bAdd.caption = '&Simpan'
    self.pMember_LMember.SetFocus()
    
    
  def bDelOnClick(self, sender):
    grid = self.listMember
    app = self.FormObject.ClientApplication
    uig = self.FinCustGroups

    dlg = app.ConfirmDialog('Yakin akan menghapus data?')
    if dlg:
      uig.Edit()
      uig.group_expossure -= grid.expossure
      grid.Delete()
  
  def IsiGridDetail(self):
    res = self.uipData
    grid = self.listMember
    if res.fMode =='edit':
      grid.Edit()
    else:
      grid.Append()

    for i in self._cols:
      n = i.split(';')
      col_name = val_name = n[0]
      if len(n) >1: val_name=n[1];
      grid.SetFieldValue(col_name, res.GetFieldValue(val_name))

    grid.Post()
    res.ClearData()
    self.pMember_bAdd.caption = '&Tambah'

