import sys
class fEditAccount:

  FRAME_MAPPING = {
    'D':'deposito/fEditAccount', #deposito
    'F':'fasbis/fEditAccount', #FASBIS
    'S':'sima/fEditAccount', #SIMA
    'B':'sukuk/fEditAccount', #sukuk
    'R':'repo/fEditAccount', #reverse repo
    'A':'reksadana/fEditAccount'  #reksadana
  }
  
  dRef = {
     'Lperingkat_debitur':'PERINGKAT_'
     ,'Llembaga_pemeringkat':'LEMBAGA_PEMERINGKAT'
     ,'Lhubungan_bank':'R_HUBUNGAN_DENGAN_BANK'
     ,'Lstatus':'RSTATUS'
     ,'Lnegara_lawan':'R_KODE_NEGARA'
     ,'LKategori_Portofolio':'REF_KATEGORI_PORTOFOLIO'
  }
  
  def __init__(self, formObj, parentForm):
    self.nomor_rekening = ''
    self.app = formObj.ClientApplication
  #--
  
  def SetNomorRekening(self, norek) :
    self.nomor_rekening = norek
    params = self.app.CreateValues(['nomor_rekening',norek])
    self.FormObject.SetDataWithParameters(params)
    
  def FormAfterProcessServerData(self, formobj, operationid, datapacket):
    # function(formobj: TrtfForm; operationid: integer; datapacket: TPClassUIDataPacket):boolean
    app = formobj.ClientApplication
    uipFin = self.uipTreasury
    
    self.nomor_rekening = uipFin.nomor_rekening
    #self.uipFilter.ForceEdit()
    #self.uipFilter.nomor_rekening = self.nomor_rekening
    ct = uipFin.jenis_treasuryaccount 
    #raise Exception, ct
    form_id = self.FRAME_MAPPING.get(ct, None)
    if form_id == None:
      formObj.ShowMessage("Form ID not defined for details of this contract type: %s" % ct)
      return
    if form_id != '':
      if formobj.systemcontext != '':
        form_id = '%s://%s' % (formobj.systemcontext, form_id)
      frAkad = self.frameAkad.Activate(form_id, app.CreateValues(['nomor_rekening', self.nomor_rekening]), None)
    #self.detail_levelOnChange(self.pInput_detail_level)
    self.FormObject.Caption = uipFin.caption
    if uipFin.label_jt<>None:
      self.pForm_label_jt.visible=1
      self.pForm_label_jt.Caption=uipFin.label_jt
        
    #setFormView
    self.SetFormView()    
  #--

  def bSaveOnClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uipHelper = self.uipHelper
    
    #set data untuk pengecekan
    uipTreasury = self.uipTreasury
    uipTreasury.Edit()
    
    if uipTreasury.jenis_treasuryaccount not in ['A']:
      self.count_ujroh()
    if uipTreasury.jenis_treasuryaccount in ['D']:
      self.count_tunai()    
    
    dlg = app.ConfirmDialog('Yakin Edit Account..?') 
    if dlg:
      # ambil dari frame
      formAkad = self.frameAkad.ContainedFormObject
      formAkad.CommitBuffer()
      fp = formAkad.GetDataPacket()
              
      form.CommitBuffer()
      params = form.GetDataPacket()
      
      params.Packet.AcquireAnotherPacket(fp.Packet)
      resp = form.CallServerMethod('save', params)    
      status = resp.FirstRecord
      if status.isErr:
        app.ShowMessage('Error :' + status.errMsg)
      else:
        if status.override_state=='F':
          app.ShowMessage('Silahkan Override transaksi Untuk Selesaikan Entri transaksi..!')
        
        app.ShowMessage('%s berhasil diedit. Nomor rekening = %s\r\nEfektif setelah otorisasi' % (uipTreasury.caption, uipTreasury.nomor_rekening))    
        sender.ExitAction = 1
      #end else
  #--

  def defineUIFromCode(self, kode_entri):
    pass
  #--

  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False
  #--

  def activate(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    #app.ShowMessage('activate() called')
    if self.authorizeMode:
      ph = app.CreateValues(['operation', 'view_authorize'], ['id_otorisasi', self.id_otorisasi])
    else:
      self.setDefaultFormData()
      ph = None
    systemContext = formObj.SystemContext
    uipFin = self.uipTreasury
    ct = uipFin.jenis_treasuryaccount 
    form_id = self.FRAME_MAPPING.get(ct, None)
    if systemContext != "":
      subframe_id = "%s://%s" % (systemContext, form_id)
    else:
      subframe_id = form_id
    #app.ShowMessage('system context: ' + subframe_id)
    pyForm = self.frameAkad.Activate(subframe_id, ph, None)
    if self.authorizeMode:
      pyForm.FormObject.SetAllControlsReadOnly()
  #--

  def OnEnterNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnEnterNum(sender)
  #--
  
  def OnExitNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnExitNum(sender)
  #--
  
  def AsDateTime(self, dateTuple):
    modDateTime = self.FormObject.ClientApplication.ModDateTime
    tanggal = modDateTime.EncodeDate(dateTuple[0], dateTuple[1], dateTuple[2])
    return tanggal      

  def jangka_waktuOnExit(self, sender):
    # procedure(sender: TrtfDBEdit)
    app = self.FormObject.ClientApplication
    uip = self.uipTreasury
    if uip.jangka_waktu == None:
      app.ShowMessage('Jangka Waktu harus diisi..!!')
      self.pForm_jangka_waktu.SetFocus()
    else:
      opendate=self.AsDateTime(uip.tgl_buka)  
      uip.tgl_jatuh_tempo = opendate + uip.jangka_waktu
      
  def eqv_rate_realOnExit(self, sender):
    uip = self.uipTreasury
    self.count_ujroh()
    if uip.jenis_treasuryaccount in ['D', 'R']:
      self.count_tunai()
    
  def count_ujroh(self):
    uip = self.uipTreasury
    formAkad = self.frameAkad.ContainedFormObject
    uipAkad = formAkad.GetUIPartByName('uipData')
    uipAkad.Edit()
    uipAkad.ujroh = round(uip.nominal_deal*(uip.eqv_rate_real/100)*uip.jangka_waktu/uip.basis_hari_pertahun,6)
    #raise Exception, '%s x %s x %s x %s : %s' % (uipParent.nominal_deal, (uip.eqv_rate_real/100), uipParent.jangka_waktu, uipParent.basis_hari_pertahun, uip.ujroh)
                    
  def count_tunai(self):
    uip = self.uipTreasury
    formAkad = self.frameAkad.ContainedFormObject
    uipAkad = formAkad.GetUIPartByName('uipData')
    uipAkad.Edit()
    uipAkad.nilai_tunai = uip.nominal_deal + uipAkad.ujroh    
  
  def ReffOnExit(self, sender):
    form = sender.OwnerForm
    uip = self.uipTreasury
    uapp = form.ClientApplication.UserAppObject
    nField = sender.Name

    res = uapp.checkStdLookup(sender, nField, 
      '''reference_code;reference_desc;refdata_id'''
    )
    if res != None:
      return res
      
    kategori = self.dRef[nField]
    #addCond = self.getAddedCondition(nField)
    #if nField in ('Llembaga_pemeringkat','Lperingkat_debitur', 'Lhubungan_bank', 'Lstatus', 'Ljenis_operasional', 'Lnegara_lawan'):
    ref_lookup = "reference@lookup_refdata"
      
    uapp = form.ClientApplication.UserAppObject
    res = uapp.stdLookup(sender, ref_lookup, nField, 
      "reference_code;reference_desc;refdata_id", 
      None,
      {
        'reference_code':uip.GetFieldValue('%s.reference_code' % nField),
        'reference_name':'',
        'nama_kategori':kategori,
        #'addCond':addCond
      })
    
    if nField in ('Llembaga_pemeringkat','Lperingkat_debitur'):  
      self.definedRefChild(nField)      
    
    return res

  def definedRefChild(self, nField):
    uip = self.uipTreasury
    if nField in ('Llembaga_pemeringkat'): 
      if uip.GetFieldValue(nField+'.reference_code')<>'00':
        self.pAdditionalData_Lperingkat_debitur.enabled=1
        self.pAdditionalData_rating_date.enabled=1
        self.dRef['Lperingkat_debitur'] = 'PERINGKAT_'+uip.GetFieldValue(nField+'.reference_code')
        uip.ClearLink('Lperingkat_debitur')
        self.pAdditionalData_Lperingkat_debitur.SetFocus()
      else:
        self.pAdditionalData_rating_date.enabled=0
        self.pAdditionalData_Lperingkat_debitur.enabled=0
        #uip.ClearLink('Lperingkat_debitur')
        uip.SetFieldValue('Lperingkat_debitur.refdata_id', uip.GetFieldValue('Llembaga_pemeringkat.refdata_id'))
        uip.SetFieldValue('Lperingkat_debitur.reference_code' ,uip.GetFieldValue('Llembaga_pemeringkat.reference_code'))
        uip.SetFieldValue('Lperingkat_debitur.reference_desc', uip.GetFieldValue('Llembaga_pemeringkat.reference_desc'))
    
  def SetFormView(self):
    uipTrea = self.uipTreasury
    if uipTrea.act_type in ['C', 'M']:
      self.pAction_bSave.visible = True
    
    if uipTrea.jenis_treasuryaccount == 'F':
      self.FormObject.PyFormObject.multipages1.GetPage(1).TabVisible = False
      #self.pForm_Keterangan.Visible = False
      #self.pForm_eqv_rate_real.Visible = False
      self.pForm_nisbah.Visible = False 
        
    if uipTrea.jenis_treasuryaccount in ['D', 'R']:
      self.pAdditionalData_additional_fitur.Visible = False
      self.pAdditionalData_status_instrumen.Visible = False 
      self.pAdditionalData_metode_pengukuran.Visible = False 
      self.pAdditionalData_LKategori_Portofolio.Visible = False  
      self.pAdditionalData_periode_imbalan.Visible = False
    
    if uipTrea.jenis_treasuryaccount == 'R':
      self.pForm_nisbah.Visible = False
    
    if uipTrea.jenis_treasuryaccount == 'A':
      self.pForm_nisbah.Visible = False
      self.pForm_eqv_rate_real.Visible = False