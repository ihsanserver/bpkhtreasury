import os
import sys
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.lib.remotequery as remotequery

vQry ='''
    item
      Expanded = False
      FieldName = '%s'
      Title.Caption = '%s'
      Width = %s
      Visible = True
    end
  '''
#--

def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)

def getSQLClient(config, params):
  mlu = config.ModLibUtils
  fr = params.FirstRecord
  dCols = {
    2:['nomor_rekening','l','S','NO. REKENING',80],
    3:['nama_rekening','rt','S','NAMA NASABAH',190],
    4:['nomor_nasabah','cs','S','NO. NASABAH',80],
    5:['produk','','S','PRODUK',80],
    6:['kode_cabang','rt','S','KODE CABANG',80],
    7:['kode_valuta','rt','S','KODE VALUTA',80],
    8:['saldo','','S','SALDO',100]
  }

  lcond = ["1=1"]
  
  if len(fr.cabang) > 0:
    lcond.append( '''rt.kode_cabang = '%s' ''' % fr.cabang)
  if len(fr.nama_nasabah) > 0:
    lcond.append( '''upper(rt.nama_rekening) LIKE upper(%s) ''' % mlu.QuotedStr('%%%s%%' % fr.nama_nasabah))
  if len(fr.no_cif) > 0:
    lcond.append( '''upper(cs.nomor_nasabah) LIKE upper(%s) ''' % mlu.QuotedStr('%%%s%%' % fr.no_cif))
  if len(fr.no_rek) > 0:
    lcond.append( '''upper(l.nomor_rekening) LIKE upper(%s) ''' % mlu.QuotedStr('%%%s%%' % fr.no_rek))
    
  str0qlCond = ''
  if len(lcond) > 0:
    str0qlCond = " AND ".join(lcond)
  
  d = {'addCond': str0qlCond}   
  d.update(dbutil.mapDBTableNames(config, ['core.rekeningtransaksi', 'core.rekeningliabilitas', 'core.produk', 'core.rekeningcustomer']))
  
  sSQL = '''
  SELECT distinct
    rt.kode_cabang,
    l.nomor_rekening, 
    rt.nama_rekening,
    rt.kode_valuta,
    cs.nomor_nasabah,        
    pr.kode_produk||' - '||pr.nama_produk as produk,
    'A' as disposisi_nominal,
    rt.status_rekening,
    l.is_blokir,         
    case when l.status_restriksi='T' then 9999999999 else rt.saldo end saldo 
  FROM %(rekeningliabilitas)s l, %(rekeningtransaksi)s rt, %(produk)s pr, %(rekeningcustomer)s cs 
  ''' % d
  sql = {}
  sql['SELECTFROMClause'] = sSQL
    
  sql['WHEREClause'] = '''
      l.nomor_rekening=rt.nomor_rekening
      AND rt.nomor_rekening=cs.nomor_rekening
      AND l.kode_produk=pr.kode_produk
      AND l.jenis_rekening_liabilitas in ('G', 'T','P')
      and rt.status_rekening=1 AND
      %(addCond)s
  ''' % d
  
  sql['keyFieldName'] = 'l.nomor_rekening'
  sql['altOrderFieldNames'] = 'l.nomor_rekening'
  sql['baseOrderFieldNames'] = 'l.nomor_rekening'
  
  _cols = dCols.keys()
  _cols.sort()
  lView = []
  for i in _cols:
    ls = dCols[i]
    t_view = vQry % (ls[0],ls[3],ls[4])
    lView.append(t_view)
  
  c_setting = " ".join(lView)
  
  sql['columnSetting'] = '''
  object TColumnsWrapper
    Columns = <
      %s
      >
  end
  ''' % c_setting

  return sql
#--

def getFinAccount(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()
  sqlStat = getSQLClient(config, clientpacket)

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('Inisialisasi proses...')

  '''
  app.ConWriteln(sqlStat['SELECTFROMClause'])
  app.ConWriteln(sqlStat['WHEREClause'])#+'  '+sqlStat['altOrderFieldNames'])
  app.ConRead('')
  '''
  try :
    rqsql = remotequery.RQSQL(config)
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '')
    rqsql.initOperation(returnpacket)
    rqsql.maxRow = 50
  
    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#   

def FormOnSetDataEx(uideflist, params):
  config = uideflist.config
  rec = uideflist.uipSelection.Dataset.AddRecord()
  
  helper = pobjecthelper.PObjectHelper(config)
  Id_User = config.SecurityContext.InitUser
  Kode_Cabang = config.SecurityContext.GetUserInfo()[4]    
  oCabang = helper.GetObject('enterprise.Cabang', Kode_Cabang)
  
  rec.SetFieldByName('edCabang.Kode_Cabang', Kode_Cabang)
  rec.SetFieldByName('edCabang.Nama_Cabang', oCabang.Nama_Cabang) 