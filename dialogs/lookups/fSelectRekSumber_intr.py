import sys

class frmSelectForImages:
  def __init__(self, formObj, parentForm):
    pass
    
  def getRekSumber(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    res = self.FormContainer.Show()
    if res == 1:
      qInd = self.qInd  
      return {
        'nomor_nasabah': qInd.GetFieldValue("nomor_nasabah"),
        'nama_rekening': qInd.GetFieldValue("nama_rekening"),
        'nomor_rekening': qInd.GetFieldValue("nomor_rekening"),
        'kode_cabang': qInd.GetFieldValue("kode_cabang"),
        'kode_valuta': qInd.GetFieldValue("kode_valuta"),
        'produk': qInd.GetFieldValue("produk"),
        'saldo': qInd.GetFieldValue("saldo"),
        'disposisi_nominal': qInd.GetFieldValue("disposisi_nominal"),
        'status_rekening': qInd.GetFieldValue("status_rekening"),
        'is_blokir': qInd.GetFieldValue("is_blokir")
      }
    else:
      return None
    
  def bCariIndividuClick(self, button):
    formObj = self.FormObject; app = formObj.ClientApplication
    uip = self.uipSelection
    edCabang = uip.GetFieldValue('edCabang.Kode_Cabang') or ""
    edCustNo = uip.edCustNo
    edCustName = uip.edCustName
    edCustRek =  uip.edCustRek
    if edCustNo == None and edCustName == None and edCustRek == None:
      formObj.ShowMessage("Silahkan isi salah satu kolom pencarian")
      return
    else:
      dParam = app.CreateValues(['nama_nasabah', edCustName or ""], 
                                ['no_cif', edCustNo or ""],
                                ['no_rek', edCustRek or ""],
                                ['cabang', edCabang])
      ph = formObj.CallServerMethod("getFinAccount", dParam)
      rec = ph.FirstRecord
      if rec.error_status:
        app.ShowMessage(rec.error_message)
      else:
        self.qInd.SetDirectResponse(ph.Packet)
    #--
  #--
  
  def bOKClick(self, btn):
    formObj = self.FormObject; app = formObj.ClientApplication
    if self.qInd.Eof:
      app.ShowMessage('Select data first')
      btn.ExitAction = 0
    else:
      btn.ExitAction = 1
    #
  #-

  def qIndOnDoubleClick(self, sender):
    # procedure(sender: TrtfQuery)
    formObj = self.FormObject; app = formObj.ClientApplication
    if self.qInd.Eof:
      app.ShowMessage('Select data first')
    else:
      formObj.Close(1)
    pass

  def edCabangOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    res = uapp.checkStdLookup(sender, nField, '''kode_cabang;nama_cabang''' )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "ent_cabang@lookupCabang_trx", nField, "kode_cabang;nama_cabang")
    return res