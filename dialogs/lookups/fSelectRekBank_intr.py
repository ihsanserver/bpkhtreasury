import sys

class frmSelectForImages:
  def __init__(self, formObj, parentForm):
    pass
    
  def getRekBank(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    res = self.FormContainer.Show()
    if res == 1:
      qInd = self.qInd  
      return {
        'nama_rekening': qInd.GetFieldValue("nama_rekening"),
        'nomor_rekening': qInd.GetFieldValue("nomor_rekening"),
        'kode_cabang': qInd.GetFieldValue("kode_cabang"),
        'kode_valuta': qInd.GetFieldValue("kode_valuta"),
        'produk': qInd.GetFieldValue("nama_produk"),
        'saldo': qInd.GetFieldValue("saldo"),
        'counterpart': qInd.GetFieldValue("nama_counterpart"),
        'status_rekening': qInd.GetFieldValue("status_rekening")
      }
    else:
      return None
    
  def bCariIndividuClick(self, button):
    formObj = self.FormObject; app = formObj.ClientApplication
    uip = self.uipSelection
    edCabang = uip.GetFieldValue('edCabang.Kode_Cabang') or ""
    edCustName = uip.edCustName
    edCustRek =  uip.edCustRek
    if edCustName == None and edCustRek == None:
      formObj.ShowMessage("Silahkan isi salah satu kolom pencarian")
      return
    else:
      dParam = app.CreateValues(['nama_nasabah', edCustName or ""], 
                                ['kode_counterpart', uip.GetFieldValue('edCounterpart.kode_counterpart') or ""],
                                ['no_rek', edCustRek or ""],
                                ['cabang', edCabang])
      ph = formObj.CallServerMethod("getFinAccount", dParam)
      rec = ph.FirstRecord
      if rec.error_status:
        app.ShowMessage(rec.error_message)
      else:
        self.qInd.SetDirectResponse(ph.Packet)
    #--
  #--
  
  def bOKClick(self, btn):
    formObj = self.FormObject; app = formObj.ClientApplication
    if self.qInd.Eof:
      app.ShowMessage('Select data first')
      btn.ExitAction = 0
    else:
      btn.ExitAction = 1
    #
  #-

  def qIndOnDoubleClick(self, sender):
    # procedure(sender: TrtfQuery)
    formObj = self.FormObject; app = formObj.ClientApplication
    if self.qInd.Eof:
      app.ShowMessage('Select data first')
    else:
      formObj.Close(1)
    pass

  def edCabangOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    res = uapp.checkStdLookup(sender, nField, '''kode_cabang;nama_cabang''' )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "ent_cabang@lookupCabang_trx", nField, "kode_cabang;nama_cabang")
    return res

  def edCounterpartOnExit(self, sender):
    uip = self.uipSelection
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_counterpart;nama_counterpart'

    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupCounterpart", nField, lsData, 
      None,
      {
        'tipe_counterpart':'A'
      })

    return res
