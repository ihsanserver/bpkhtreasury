import os
import sys
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.util.modman as modman

GRID_ACCOUNT = 'gridsettings.listaccount_cols'
#--

def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)

def getSQLClient(config, params):
  mlu = config.ModLibUtils
  fr = params.FirstRecord

  lcond = []
  
  lcond.append( ''' a.jenis_treasuryaccount in ('C') ''' )
  if len(fr.cabang) > 0:
    lcond.append( '''b.kode_cabang = '%s' ''' % fr.cabang)
  if len(fr.nama_nasabah) > 0:
    lcond.append( '''upper(b.nama_rekening) LIKE upper(%s) ''' % mlu.QuotedStr('%%%s%%' % fr.nama_nasabah))
  if len(fr.kode_counterpart) > 0:
    lcond.append( '''upper(a.kode_counterpart) LIKE upper(%s) ''' % mlu.QuotedStr('%%%s%%' % fr.kode_counterpart))
  if len(fr.no_rek) > 0:
    lcond.append( '''upper(a.nomor_rekening) LIKE upper(%s) ''' % mlu.QuotedStr('%%%s%%' % fr.no_rek))
    
  str0qlCond = ''
  if len(lcond) > 0:
    str0qlCond = " AND ".join(lcond)
  
  _dict = {
    'treasuryaccount':config.MapDBTableName("treasuryaccount"),
    'rekeningtransaksi':config.MapDBTableName("core.rekeningtransaksi"),
    'treafasbis':config.MapDBTableName("treafasbis"),
    'treaproduk':config.MapDBTableName("treaproduk"),
    'treacounterpart':config.MapDBTableName("treacounterpart"), 
    'listperanuser':config.MapDBTableName("enterprise.listperanuser"), 
    'listcabangdiizinkan':config.MapDBTableName("enterprise.listcabangdiizinkan"), 
    'id_user': mlu.QuotedStr(config.SecurityContext.InitUser),
    'addCond':str0qlCond
  } 
  _dict['LCDJoinType'] = 'LEFT JOIN' #if rSQL.tipe_akses_cabang=='S' else 'INNER JOIN'
  
  sSQL = '''
    SELECT DISTINCT 
      a.Nomor_Rekening,
      b.NAMA_REKENING,
      -(b.saldo) as saldo, 
      a.kode_produk, 
      b.kode_cabang, b.kode_valuta,
      a.tgl_buka,
      a.kode_counterpart,
      cp.nama_counterpart,
      cp.nama_counterpart as counterpart,
      pr.nama_produk,
      a.kode_produk||' - '||pr.nama_produk as produk,
      b.keterangan,
      decode(b.status_rekening,1,'AKTIF','TUTUP') status_rekening
    FROM %(treasuryaccount)s a
    	INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
			INNER JOIN %(treaproduk)s pr ON a.kode_produk=pr.kode_produk
			INNER JOIN %(treacounterpart)s cp ON a.kode_counterpart=cp.kode_counterpart

      %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = b.kode_cabang AND lc.id_user = %(id_user)s
  ''' % _dict
  
  sql = {}
  sql['SELECTFROMClause'] = sSQL
    
  sql['WHEREClause'] = '''
      %(addCond)s
  ''' % _dict
  
  sql['keyFieldName'] = 'a.nomor_rekening'
  sql['altOrderFieldNames'] = 'a.nomor_rekening'
  sql['baseOrderFieldNames'] = 'a.nomor_rekening'
  
  gsModule = modman.getModule(config, GRID_ACCOUNT)
  sql['columnSetting'] = gsModule.cols_setting('C', 'A')

  return sql
#--

def getFinAccount(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()
  sqlStat = getSQLClient(config, clientpacket)

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('Inisialisasi proses...')

  '''
  app.ConWriteln(sqlStat['SELECTFROMClause'])
  app.ConWriteln(sqlStat['WHEREClause'])#+'  '+sqlStat['altOrderFieldNames'])
  app.ConRead('')
  '''
  try :
    rqsql = remotequery.RQSQL(config)
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '')
    rqsql.initOperation(returnpacket)
    rqsql.maxRow = 50
  
    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#   

def FormOnSetDataEx(uideflist, params):
  config = uideflist.config
  rec = uideflist.uipSelection.Dataset.AddRecord()
  
  helper = pobjecthelper.PObjectHelper(config)
  Id_User = config.SecurityContext.InitUser
  Kode_Cabang = config.SecurityContext.GetUserInfo()[4]    
  oCabang = helper.GetObject('enterprise.Cabang', Kode_Cabang)
  
  rec.SetFieldByName('edCabang.Kode_Cabang', Kode_Cabang)
  rec.SetFieldByName('edCabang.Nama_Cabang', oCabang.Nama_Cabang) 