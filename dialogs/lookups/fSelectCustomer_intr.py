import sys

class frmSelectForImages:
  def __init__(self, formObj, parentForm):
    pass
    
  def getCustomer(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    res = self.FormContainer.Show()
    if res == 1:
      qInd = self.qInd  
      return {
        'nomor_nasabah': qInd.GetFieldValue("nomor_nasabah"),
        'nama_nasabah': qInd.GetFieldValue("nama_lengkap"),
        'tipe_nasabah': qInd.GetFieldValue("tipe_nasabah"),
        'group_code': qInd.GetFieldValue("group_code"),
        'group_name': qInd.GetFieldValue("group_name"),
        'cust_group_id': qInd.GetFieldValue("cust_group_id")
      }
    else:
      return None
    
  def bCariIndividuClick(self, button):
    formObj = self.FormObject; app = formObj.ClientApplication
    edCustNo = self.pIndSearch_edCustNo
    edCustName = self.pIndSearch_edCustName
    if edCustNo.Text == "" and edCustName.Text != "":
      ph = formObj.CallServerMethod("searchByName", app.CreateValues(['name_prefix', edCustName.Text]))
      self.qInd.SetDirectResponse(ph.Packet)
      dsStatus = ph.packet.status
      rec = dsStatus.GetRecord(0)
      if rec.has_more_data:
        app.ShowMessage("Too many results available. Please enter more specific filter")
    elif len(edCustNo.Text) >= 4 and edCustName.Text == "":
      ph = formObj.CallServerMethod("searchByCustNo", app.CreateValues(['customer_no', edCustNo.Text]))
      self.qInd.SetDirectResponse(ph.Packet)
    else:
      formObj.ShowMessage("Select cust.no or name")
      return
    #--
  #--
  
  def bOKClick(self, btn):
    formObj = self.FormObject; app = formObj.ClientApplication
    if self.qInd.Eof:
      app.ShowMessage('Select data first')
      btn.ExitAction = 0
    else:
      btn.ExitAction = 1
    #
  #-

  def qIndOnDoubleClick(self, sender):
    # procedure(sender: TrtfQuery)
    formObj = self.FormObject; app = formObj.ClientApplication
    if self.qInd.Eof:
      app.ShowMessage('Select data first')
    else:
      formObj.Close(1)
    pass