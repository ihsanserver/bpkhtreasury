import os
import sys
import com.ihsan.lib.remotequery as librq
import com.ihsan.util.dbutil as dbutil
                              
def execQrySELECT(config, params, returns): # dummy handler when users click refresh / top button
  rq = librq.RQResult()
  rq.resultcode = librq.RTBLRES_NOCHANGE             
  rq.composeResult(returns)
#--

def searchByName(config, params, returns):
  mlu = config.ModLibUtils
  fr = params.FirstRecord
  if len(fr.name_prefix) < 3:
    raise Exception, 'At least 3 characters of name prefix is required'
  sLike = mlu.QuotedStr("%s%%" % fr.name_prefix)  
  d = {'strLike': sLike} 
  d.update(dbutil.mapDBTableNames(config, ['core.NasabahIndividu', 'core.Nasabah', 'core.Individu','core.NasabahKorporat','fincustadditionaldata','fincustgroups']))
  
  sSQL = '''
    SELECT
      nsb.nomor_nasabah,
      'Individual' as tipe_nasabah, 
      ind.nama_lengkap, 
      ind.jenis_kelamin,
      ind.alamat_rumah_jalan as alamat_jalan,
      ind.alamat_rumah_kecamatan as kecamatan,
      ind.tanggal_lahir,
      ind.id_individu as id,
      fca.group_code, fca.group_name, fca.cust_group_id  
    FROM 
      %(NasabahIndividu)s ni,
      %(Nasabah)s nsb 
      left join (
        select a.nomor_nasabah, b.group_code, b.group_name, a.cust_group_id from %(fincustadditionaldata)s a 
        left join %(fincustgroups)s b on a.cust_group_id=b.cust_group_id
      ) fca on nsb.nomor_nasabah=fca.nomor_nasabah,
      %(Individu)s ind
    WHERE
      ni.nomor_nasabah = nsb.nomor_nasabah AND
      ind.id_individu = ni.id_individu AND
      ind.nama_lengkap LIKE %(strLike)s
    UNION
    SELECT
      nsb.nomor_nasabah,
      'Corporate' as tipe_nasabah, 
      nk.nama_perusahaan, 
      'None' as jenis_kelamin ,
      nk.alamat_jalan as alamat_jalan,
      nk.alamat_kecamatan as kecamatan,
      nk.tanggal_siupp as tanggal_lahir,
      0 as id,
      fca.group_code, fca.group_name, fca.cust_group_id  
    FROM 
      %(NasabahKorporat)s nk,
      %(Nasabah)s nsb
      left join (
        select a.nomor_nasabah, b.group_code, b.group_name, a.cust_group_id from %(fincustadditionaldata)s a 
        left join %(fincustgroups)s b on a.cust_group_id=b.cust_group_id
      ) fca on nsb.nomor_nasabah=fca.nomor_nasabah
    WHERE
      nk.nomor_nasabah = nsb.nomor_nasabah AND
      nk.nama_perusahaan LIKE %(strLike)s
    ORDER BY nama_lengkap
  ''' % d
  sql = config.CreateSQL(sSQL).RawResult
  if sql.Eof:
    raise Exception, "Data not found"
  rq = librq.RQResult()
  rq.maxRow = 50
  rq.rawResult = sql
  rq.resultcode = librq.RTBLRES_NORMAL
  rq.composeResult(returns)
  ds = returns.AddNewDatasetEx("status", "has_more_data: integer")
  rec = ds.AddRecord()
  rec.has_more_data = not sql.Eof
#--
  
def searchByCustNo(config, params, returns):
  mlu = config.ModLibUtils
  fr = params.FirstRecord
  if(len(fr.customer_no) < 8):
    raise Exception, 'At least 8 digits of customer number is required'
  sSQL = '''
    SELECT
      nsb.nomor_nasabah, 
      ind.nama_lengkap, 
      ind.jenis_kelamin,
      'Individual' as tipe_nasabah, 
      ind.alamat_rumah_jalan as alamat_jalan,
      ind.alamat_rumah_kecamatan as kecamatan,
      ind.tanggal_lahir,
      ind.id_individu as id,
      fca.group_code, fca.group_name, fca.cust_group_id  
    FROM 
      %(NasabahIndividu)s ni,
      %(Nasabah)s nsb
      left join (
        select a.nomor_nasabah, b.group_code, b.group_name, a.cust_group_id from %(fincustadditionaldata)s a 
        left join %(fincustgroups)s b on a.cust_group_id=b.cust_group_id
      ) fca on nsb.nomor_nasabah=fca.nomor_nasabah,
      %(Individu)s ind
    WHERE
      ni.nomor_nasabah = nsb.nomor_nasabah AND
      ind.id_individu = ni.id_individu AND
      nsb.nomor_nasabah LIKE %(customer_no)s
    UNION
    SELECT
      nsb.nomor_nasabah, 
      nk.nama_perusahaan as nama_lengkap, 
      'None' as jenis_kelamin,
      'Corporate' as tipe_nasabah, 
      nk.alamat_jalan as alamat_jalan,
      nk.alamat_kecamatan as kecamatan,
      nk.tanggal_siupp as tanggal_lahir,
      0 as id,
      fca.group_code, fca.group_name, fca.cust_group_id  
    FROM 
      %(NasabahKorporat)s nk,
      %(Nasabah)s nsb
      left join (
        select a.nomor_nasabah, b.group_code, b.group_name, a.cust_group_id from %(fincustadditionaldata)s a 
        left join %(fincustgroups)s b on a.cust_group_id=b.cust_group_id
      ) fca on nsb.nomor_nasabah=fca.nomor_nasabah
    WHERE
      nk.nomor_nasabah = nsb.nomor_nasabah AND
      nsb.nomor_nasabah LIKE %(customer_no)s
    ORDER BY nama_lengkap
  '''
  sLike = mlu.QuotedStr("%s%%" % fr.customer_no)  
  d = {'customer_no': sLike} 
  d.update(dbutil.mapDBTableNames(config, ['core.NasabahIndividu', 'core.Nasabah', 'core.Individu','core.NasabahKorporat','fincustadditionaldata','fincustgroups']))
  sSQL = sSQL % d
  sql = config.CreateSQL(sSQL).RawResult
  if sql.Eof:
    raise Exception, "Data not found"
  rq = librq.RQResult()
  rq.maxRow = 50
  rq.rawResult = sql
  rq.resultcode = librq.RTBLRES_NORMAL
  rq.composeResult(returns)
#--