import com.ihsan.util.otorisasi as otorisasi
import sys
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman

modman.loadStdModules(globals(), ['libs'])

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipData"): return
  
  nomor_rekening = params.FirstRecord.nomor_rekening
  pobjconst = 'PObj:TreaFasbis#nomor_rekening=' + nomor_rekening
  uideflist.SetData('uipData', pobjconst)

  ds = uideflist.uipData.Dataset
  rec = ds.GetRecord(0)
  rec.saldo_accrue = -rec.saldo_accrue
