class fInputNewAccount:
  arrValidationDef = [#['eqv_rate_real', 'num', 'value > 0', 'Ujroh (%) harus diisi dan lebih besar dari 0']
    #, ['ujroh', 'num', 'value > 0', 'Nilai Ujroh harus lebih besar dari 0'], 
    ['kolektibilitas', 'num', 'value > 0', 'Kolek Belum Dipilih']
  ]
  
  def __init__(self, formObj, parentForm):
    self.parentForm = parentForm
    pass
  #--
  
  def GetArrValidationDef(self):
    return self.arrValidationDef
  
  def tipe_fasbisOnChange(self, sender):
    if sender.ItemIndex==1: 
      tipe_fasbis='S'
    else:
      tipe_fasbis='F'

    self.settipe_baghas(tipe_fasbis)

  def settipe_baghas(self, tipe_fasbis):
    if tipe_fasbis=='S':
      self.panel1_tipe_baghas.Visible=True
    else:
      self.panel1_tipe_baghas.Visible=False
  
  def OnEnterNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnEnterNum(sender)
  #--
  
  def OnExitNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnExitNum(sender)
  #--
  