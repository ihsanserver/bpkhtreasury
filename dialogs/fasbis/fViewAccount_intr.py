class fInputNewAccount:
  arrValidationDef = [#['eqv_rate_real', 'num', 'value > 0', 'Ujroh (%) harus diisi dan lebih besar dari 0']
    #, ['ujroh', 'num', 'value > 0', 'Nilai Ujroh harus lebih besar dari 0'], 
    ['adjustment_pendapatan', 'num', 'value >= 0', 'Adjustment Pendapatan harus diisi 0 atau lebih besar dari 0']
    #, ['LPaymentSrc.nomor_rekening', 'str', 'len(value) > 0', 'Nomor Rekening harus diisi'    ]
  ]
  def __init__(self, formObj, parentForm):
    self.parentForm = parentForm
    pass
  #--
  
  def GetArrValidationDef(self):
    return self.arrValidationDef
  
  def FormAfterProcessServerData(self, formobj, operationid, datapacket):
    # function(formobj: TrtfForm; operationid: integer; datapacket: TPClassUIDataPacket):boolean
    uipParent = self.parentForm.uipTreasury
    if uipParent.act_type in ['C'] or self.uipData.status in [3]:
      self.panel1_adjustment_pendapatan.Visible = True 
      #self.panel1_LPaymentSrc.Visible = True
      
    if self.uipData.status in [3]:
      self.panel1_adjustment_pendapatan.ReadOnly = True
    
    if self.uipData.tipe_fasbis=='S':
      self.panel1_tipe_baghas.Visible=True
  
  def LPaymentSrcOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name

    res = uapp.checkStdLookup(sender, nField, '''nomor_rekening;nama_rekening''' )      
    if res != None:
      return res
    res = uapp.stdLookup(sender, "rekeningliabilitas@lookupAllAccount", nField, "nomor_rekening;nama_rekening")

    return res