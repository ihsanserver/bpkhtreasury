import sys
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.modman as modman
import rpdb2

import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.net.message as message
modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  
  if otorisasi.loadDataOtorisasi(uideflist, params, "FinCustGroups;listMember"): return

  keyconst = params.FirstRecord.key
  if keyconst <>'R':
    uideflist.SetData('FinCustGroups', keyconst)
    group_id = keyconst.split("=")[-1]    

    _cols = ['LMember.Nomor_Nasabah','LMember.Nama_Nasabah','description','expossure']
          
    sSQL='''
       select 
         a.*, b.nama_nasabah, COALESCE(c.os_pokok,0) as expossure 
       from FinCustGroupLink a 
         left join %(nasabah)s b on (a.nomor_nasabah=b.nomor_nasabah)
        	LEFT JOIN (
        		select 
        			rc.NOMOR_NASABAH, 
        			sum(COALESCE(CASE WHEN fa.contracttype_code='I' THEN nilai_kafalah ELSE fa.dropping_amount END,0)) plafond,
        			sum(round(
        				case 
        					when fa.contracttype_code in ('F') then 
        						(fa.dropping_amount - COALESCE(pai.principal_payment_ijr,0)) 
        					when fa.contracttype_code in ('I') then 
        						nilai_kafalah 
        					when fa.contracttype_code in ('A','H') then 
        						-(rt.saldo + fa.arrear_balance + mrb.mmd_balance + fa.profit_arrear_balance) 
        					else 
        						fa.dropping_amount 
        				end
        			,2 )) as os_pokok,
        			count(1) jml_nsb
        		from %(rekeningtransaksi)s rt
        			inner join finaccount fa on rt.NOMOR_REKENING=fa.NOMOR_REKENING
        			LEFT JOIN finmurabahahaccount mrb ON fa.nomor_rekening=mrb.nomor_rekening
        			LEFT JOIN (
        				select id_schedule,sum(principal_amount) as principal_payment_ijr,sum(realized_margin) as profit_payment_ijr 
        				from finpaymentscheddetail where seq_number>0 and realized='T'
        				group by id_schedule
        			) pai on pai.id_schedule=fa.id_schedule   	
        			LEFT JOIN kafalah kaf ON fa.nomor_rekening=kaf.nomor_rekening
        			inner join %(rekeningcustomer)s rc on fa.NOMOR_REKENING=rc.NOMOR_REKENING
        		where fa.contracttype_code <>'W' and RT.STATUS_REKENING=1
        		group by rc.nomor_nasabah
        	) c on a.nomor_nasabah=c.nomor_nasabah	        
       where a.cust_group_id = %(group_id)s
    ''' % {'group_id':group_id, 'nasabah':config.MapDBTableName("core.Nasabah"),
           'rekeningtransaksi':config.MapDBTableName("core.rekeningtransaksi"),
           'rekeningcustomer':config.MapDBTableName("core.rekeningcustomer")}
    valid = config.CreateSQL(sSQL).RawResult  

    uipGrid = uideflist.listMember.Dataset
    group_expossure = 0
    while not valid.Eof:
      oRek = uipGrid.AddRecord()
      for col_name in _cols:
        _cName = col_name.split(".")[-1]
        oRek.SetFieldByName(col_name, eval('valid.'+_cName))
      group_expossure += valid.expossure
      valid.Next()
    #raise Exception, oFinColAsset.group_asset_type
    uip = uideflist.FinCustGroups
    rec = uip.Dataset.GetRecord(0)
    rec.group_expossure = group_expossure
    #rec.cust_group_id   = group_id
     
  else:
    uideflist.PrepareReturnDataset()
    rec = uideflist.FinCustGroups.Dataset.AddRecord()
    rec.group_expossure = 0
     

def reject(config, params, returns):
  helper = pobjecthelper.PObjectHelper(config)
  rec = params.FinCustGroups.GetRecord(0)
  
  cust_group_id = rec.cust_group_id
  oFinCustGroups = helper.GetObject("FinCustGroups", {'cust_group_id': cust_group_id})
  #oFinAgent.status_update = 'F' 

def cekGroupCode(config, group_code):
  sSQL = "select * from FinCustGroups where group_code='%s'" % group_code
  q = config.CreateSQL(sSQL).RawResult
  if not q.Eof:
    raise Exception, 'Kode Group %s sudah terdaftar!!' % group_code

  return 1
#--  

def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000', True, True)
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  
  rec = params.FinCustGroups.GetRecord(0)
  config.BeginTransaction()
  try :
    group_code = rec.group_code.strip()
    if rec.jenis_aksi =='R':
      cekGroupCode(config, group_code)
    
    recOtor = otorisasi.prepareParameterOtorisasi(params)
    recOtor.kode_entri = 'FCGR01'
    recOtor.keterangan = "Entri Group Nasabah, Nama %s" % (rec.group_name)
    recOtor.info1 = str(group_code)
    recOtor.info2 = None
    recOtor.tipe_target = 0
    recOtor.id_peran = "OM"
    recOtor.nama_tabel = 'FinCustGroups'
    recOtor.tipe_key = 1
    recOtor.key_string = str(group_code)
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph) 

    
    config.FlushUpdates() # cek generate SQL
    config.Commit()
    isErr = 0
    errMsg = ""
  except :
    config.Rollback()
    isErr = 1
    errMsg = str(sys.exc_info()[1])
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg])
#--  

## save, authorize & reject NEW
def authorize(config, params, returns):
  try:
    #rpdb2.start_embedded_debugger('000')  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    rec = params.FinCustGroups.GetRecord(0)
    recGroupLink = params.listMember
    className = 'FinCustGroups'
    oAsset = helper.GetObject(className, rec.cust_group_id)
    ph =  {'FinCustGroups': rec,'GroupLink': recGroupLink } 
    if oAsset.IsNull:
      oAsset = helper.CreatePObject(className, ph)
    else:
      oAsset.Edit(ph)
    # process other fields here 

    #oAsset.status_update = 'F'
    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--  
  
