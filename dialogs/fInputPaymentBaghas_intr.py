import sys
class fInputPaymentBaghas:
  FRAME_MAPPING = {
    'D':'deposito/fInputPaymentBaghas', #deposito
    #'F':'fasbis/fViewAccount', #FASBIS
    'S':'sima/fInputPaymentBaghas', #SIMA
    'B':'sukuk/fInputPaymentBaghas', #sukuk
    'R':'repo/fInputPaymentBaghas', #reverse repo
    'A':'reksadana/fInputPaymentBaghas'  #reksadana
  }
  
  def __init__(self, formObj, parentForm):
    self.nomor_rekening = ''
    self.app = formObj.ClientApplication
  #--
  
  def AsDateTime(self, dateTuple):
    modDateTime = self.FormObject.ClientApplication.ModDateTime
    tanggal = modDateTime.EncodeDate(dateTuple[0], dateTuple[1], dateTuple[2])
    return tanggal
    
  def SetNomorRekening(self, norek) :
    self.nomor_rekening = norek
    params = self.app.CreateValues(['nomor_rekening',norek])
    self.FormObject.SetDataWithParameters(params)
    
  def defineUIFromCode(self, kode_entri):
    pass
  #--

  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False
  #--
  
  def activate(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    #app.ShowMessage('activate() called')
    if self.authorizeMode:
      ph = app.CreateValues(['operation', 'view_authorize'], ['id_otorisasi', self.id_otorisasi])
    else:
      self.setDefaultFormData()
      ph = None
    systemContext = formObj.SystemContext
    uipFin = self.uipTreasury
    ct = uipFin.jenis_treasuryaccount 
    form_id = self.FRAME_MAPPING.get(ct, None)
    if systemContext != "":
      subframe_id = "%s://%s" % (systemContext, form_id)
    else:
      subframe_id = form_id
    #app.ShowMessage('system context: ' + subframe_id)
    pyForm = self.frameAkad.Activate(subframe_id, ph, None)
    if self.authorizeMode:
      pyForm.FormObject.SetAllControlsReadOnly()
    
  def FormAfterProcessServerData(self, formobj, operationid, datapacket):
    # function(formobj: TrtfForm; operationid: integer; datapacket: TPClassUIDataPacket):boolean
    app = formobj.ClientApplication
    uipFin = self.uipTreasury
    
    self.nomor_rekening = uipFin.nomor_rekening
    ct = uipFin.jenis_treasuryaccount
    form_id = self.FRAME_MAPPING.get(ct, None)
    if form_id == None:
      formObj.ShowMessage("Form ID not defined for details of this contract type: %s" % ct)
      return
    if form_id != '':
      if formobj.systemcontext != '':
        form_id = '%s://%s' % (formobj.systemcontext, form_id)
      frAkad = self.frameAkad.Activate(form_id, app.CreateValues(['nomor_rekening', self.nomor_rekening]), None)
      self.count_baghas()
    #self.detail_levelOnChange(self.pInput_detail_level)
  #--
  
  def count_baghas(self):
    uip = self.uipTreasury
    formAkad = self.frameAkad.ContainedFormObject
    uipAkad = formAkad.GetUIPartByName('uipData')
    uipAkad.Edit() 
    
    if uipAkad.bayar_sesuai_tanggal == 'T':
      opendate = self.AsDateTime(uip.tgl_buka)  
      todate = self.AsDateTime(uipAkad.tgl_transaksi)
      jangka_waktu = todate - opendate 
      uipAkad.amount = round(uip.nominal_deal*(uip.eqv_rate_real/100)*jangka_waktu/uip.basis_hari_pertahun,6)

  def bayarBaghas(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    
    #set data untuk pengecekan
    uipTreasury = self.uipTreasury
    uipTreasury.Edit()
    
    #cek perhitungan baghas
    self.count_baghas()
    
    dlg_msg = 'Apakah anda yakin?'    
    dlg = app.ConfirmDialog(dlg_msg) 
    if dlg:
      # ambil dari frame
      formAkad = self.frameAkad.ContainedFormObject
      formAkad.CommitBuffer()
      fp = formAkad.GetDataPacket()
              
      form.CommitBuffer()
      params = form.GetDataPacket()
      
      params.Packet.AcquireAnotherPacket(fp.Packet)
      resp = form.CallServerMethod('save', params)    
      status = resp.FirstRecord
      if status.isErr:
        app.ShowMessage('Error :' + status.errMsg)
      else:
        app.ShowMessage('%s berhasil dibuat. Efektif setelah otorisasi' % (uipTreasury.caption))    
        sender.ExitAction = 1
    #end else
  #--