class fReportPlacing:
  def __init__(self, formObj, parentForm):
    pass
  #--
                                    
  def LCabangOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    res = uapp.checkStdLookup(sender, nField, '''kode_cabang;nama_cabang''' )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "ent_cabang@lookupCabang", nField, "kode_cabang;nama_cabang")
    return res
    
  def cv_parameter(self):
    formObj = self.FormObject; app = formObj.ClientApplication; userapp = app.UserAppObject
    uip = self.uipFilter
    ph = app.CreateValues(
      ['kode_cabang', uip.GetFieldValue('LCabang.kode_cabang') or ""],
      ['kode_produk', uip.GetFieldValue('LProduk.kode_produk') or "ALL"],
      ['jenis_treasuryaccount', uip.GetFieldValue('LJenisTreasury.jenis_treasuryaccount') or "ALL"],
      ['status_rekening', uip.status_rekening or "9"],         
      ['all_cabang', uip.cbAllCabang or "0"],
      ['tgl_awal', uip.start_date or 0],
      ['tgl_akhir', uip.end_date or 0],
      ['tipe_treasury', uip.tipe_treasury or ""],
      ['type_report', uip.type_report or ""],
    )
    return ph
    
  def DisplayList(self, sender):
    formObj = self.FormObject; app = formObj.ClientApplication; userapp = app.UserAppObject
    formObj.CommitBuffer()
              
    ph = self.cv_parameter()
    if ph <> 0:
      resp = app.ExecuteScript('rpt_placingaccrual.getQueryData',ph)
    
    rec = resp.FirstRecord
    if rec.error_status:
      app.ShowMessage(rec.error_message)
    else:
      self.qData.SetDirectResponse(resp.Packet)
  #--

  def status_droppingOnChange(self, sender):
    uipFilter = self.uipFilter
    uipFilter.Edit()
    pass

  def cbAllCabangOnClick(self, sender):
    if self.pFilter_cbAllCabang.Checked:
      self.pFilter_LCabang.Enabled = 0  
      self.uipFilter.ClearLink('LCabang')
    else:
      self.pFilter_LCabang.Enabled = 1
      self.pFilter_LCabang.SetFocus()
  #--
  
  def LJenisTreasuryOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    uip = self.uipFilter
    lsData = 'kode_jenis;keterangan;jenis_treasuryaccount'
    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupJenisTreasuryAccrual", nField, lsData, 
      None,
      {
        'jenis_treasuryaccount':'F'
      })
    
    return res

  def LFinProductOnExit(self, sender):
    uip = self.uipFilter
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_produk;nama_produk;jenis_treasuryaccount'
    jenis_treasuryaccount = uip.GetFieldValue('LJenisTreasury.jenis_treasuryaccount')
    
    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupTreaProdukAccrual", nField, lsData, 
      None,
      {
        'tipe_fasbis':'S',
        'jenis_treasuryaccount':jenis_treasuryaccount
      })
      
    return res    

  def bCetakOnClick(self, sender):
    formObj = self.FormObject
    app = formObj.ClientApplication
    userapp = app.UserAppObject
    formObj.CommitBuffer()
    
    ph = self.cv_parameter()
    if ph <> 0:
      resp = app.ExecuteScript('rpt_placingaccrual.GenerateFile',ph)
      
      status = resp.FirstRecord
      if status.Is_Err:
        app.ShowMessage(status.Err_Message)
        return
        
      oPrint = self.FormObject.ClientApplication.GetClientClass('PrintLib_2','PrintLib')()
      oPrint.doProcessByStreamName('gen_report',app,resp.packet,3)
  #--

  def cbAllProdukOnClick(self, sender):
    if self.pFilter_cbAllProduk.Checked:
      self.pFilter_LProduk.Enabled = 0  
      self.uipFilter.ClearLink('LProduk')     
    else:
      self.pFilter_LProduk.Enabled = 1      
      self.pFilter_LProduk.SetFocus()
  #--
  
  def cbAllJenisOnClick(self, sender):
    if self.pFilter_cbAllJenis.Checked:
      self.pFilter_LJenisTreasury.Enabled = 0  
      self.uipFilter.ClearLink('LJenisTreasury')    
    else:
      self.pFilter_LJenisTreasury.Enabled = 1 
      self.pFilter_LJenisTreasury.SetFocus()
  #--