import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.net.message as message
import sys
import com.ihsan.util.modman as modman
import rpdb2
#rpdb2.start_embedded_debugger('000', True, True)

modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

class record: pass
                                                                           
def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipData;uipTreasury"): return

  fr = params.FirstRecord
  nomor_rekening = fr.nomor_rekening
  pobjconst = 'PObj:TreasuryAccount#nomor_rekening=' + nomor_rekening
  uideflist.SetData('uipTreasury', pobjconst)
  
  oTreasury = helper.GetObject("TreasuryAccount", nomor_rekening)
  oTreasury = oTreasury.CastToLowestDescendant()
  ds = uideflist.uipTreasury.Dataset
  if ds.RecordCount < 1:
    raise Exception, 'Data not found or data integrity error. nomor_rekening = %s' % nomor_rekening
  rec = ds.GetRecord(0)

  phTgl = config.AppObject.rexecscript("core", "appinterface/coreInfo.getAccountingDay", config.AppObject.CreatePacket())
  tgl_transaksi = phTgl.FirstRecord.acc_date
  rec.date_now = tgl_transaksi
  rec.tgl_transaksi = tgl_transaksi
  rec.kode_entri = 'TRC001' #transaksi rek.bank
  rec.jenis_mutasi = 'D'
  rec.rek_lawan = 'B'
  rec.keterangan = 'Transaksi rek.bank %s' % nomor_rekening
  
#-- 
def authorize(config, params, returns):
  try :
    #rpdb2.start_embedded_debugger('000', True, True)  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    rec = params.uipTreasury.GetRecord(0)
    rec2 = params.uipData.GetRecord(0)
    ph = {'rec': rec, 'rec2': rec2}

    oTreasury = helper.GetObject("BankAccount", rec.nomor_rekening)    
    # info user transaksi / otor
    dsInfo = params.__inputinfo
    recInfo = dsInfo.GetRecord(0)
    oTreasury.id_otorentri = recInfo.id_otorisasi
    # trx rek bank & header
    oTx = oTreasury.TrxBankAccount(ph)
        
    if rec.rek_lawan=='B':
      oTreasury = helper.GetObject("BankAccount", rec2.NoRekBank) 
    
    # trx rek lawan
    oTreasury.TrxBankAccountRekLawan(ph, oTx)   

    # journal
    oTreasury.TrxBankAccountJournal(oTx)   
    
    #raise Exception, 'OK' 
    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--  
  
def reject(config, params, returns):
  pass
    
def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000')
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  fau = helper.GetPClass('TreasuryUtils')(config)
  rec = params.uipTreasury.GetRecord(0)
  config.BeginTransaction()
  try :
    nomor_rekening = rec.nomor_rekening

    recOtor = otorisasi.prepareParameterOtorisasi(params)
    recOtor.kode_entri = rec.kode_entri
    recOtor.keterangan = "Transaksi Rekening Bank - %s" % (nomor_rekening)
    recOtor.info1 = nomor_rekening
    #recOtor.info2 = None
    recOtor.tipe_target = 0
    recOtor.id_peran = "OM"
    recOtor.nama_tabel = 'TreasuryAccount'
    recOtor.tipe_key = 1
    recOtor.key_string = nomor_rekening
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph) 
    
    isErr = 0
    errMsg = ""
    config.Commit()
  except :
    config.Rollback()
    isErr = 1
    errMsg = "%s.%s" % (str(sys.exc_info()[0]), str(sys.exc_info()[1]))
    nomor_rekening = ""
    #raise Exception, errMsg
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg], ['nomor_rekening', nomor_rekening])
#--  

