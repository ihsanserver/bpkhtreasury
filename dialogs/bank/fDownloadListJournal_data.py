import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.modman as modman
import pyFlexcel
import os
import sys

modman.loadStdModules(globals(), ['sutil'])

MASTER_TEMPLATE  = 'tplListJournalOps.xls'

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)

  
  nomor_rekening = params.FirstRecord.nomor_rekening
  
  oTreaAccount = helper.GetObject("TreasuryAccount", nomor_rekening).CastToLowestDescendant()
  
  recFilter = uideflist.uipFilter.Dataset.AddRecord()
    
  recFilter.Nomor_Rekening = oTreaAccount.Nomor_Rekening
  recFilter.Nama_Rekening = oTreaAccount.Nama_Rekening

  Now = int(config.Now())
  recFilter.start_date  = Now
  recFilter.end_date = Now
#---

                                    
def getTemplateDir(config):
   return config.HomeDir + 'template\\'

def dbg(msg):
  message.send_udp(msg + "\n", 'localhost', 9125)

def CreateExcelHeader(owb, judul_excel, excel_cols, R_=3):
  owb.SetCellValue(1, 1, judul_excel)
  i = 1
  for cols in excel_cols:
    nama_kolom = cols[0]
    cols_split = cols[0].split(';')
    if len(cols_split) > 1: 
      nama_kolom = cols_split[1]
      
    owb.SetCellValue(R_, i, nama_kolom)    
    i += 1
  return owb

def GetNilaiKolomExcel(config, _col, resQ):
  tglIndo = modman.getModule(config,'TglIndo')
  n_col=_col[0].split(';')[0]
  if _col[1]=='D':
    tgl = eval('resQ.'+n_col)
    v_kolom = tglIndo.tgl_indo(config,eval('resQ.'+n_col),2,1) if tgl<>None else ''
  #elif _col[1]=='F':
  #  v_kolom = config.FormatFloat(',0.00',float(eval('resQ.'+n_col) or 0))
  else:
    v_kolom = eval('resQ.%s' % n_col)
                                                          

  return v_kolom

def PrintXLS(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  helper = pobjecthelper.PObjectHelper(config)
  
  tpl = getTemplateDir(config) + MASTER_TEMPLATE 
  
  workbook = pyFlexcel.Open(tpl)
  workbook.ActivateWorksheet('Data')
  try :
    # tuple data mapping field;alias;datatype
    excel_cols = (
      ("JOURNAL_NO;NOMOR JURNAL",'S'),
      ("JOURNAL_DATE;TANGGAL JURNAL",'D'),      
      ("AMOUNT_DEBIT;NOMINAL DEBIT",'S'),
      ("AMOUNT_CREDIT;NOMINAL KREDIT",'S'),
      ("ACCOUNT_CODE;KODE ACCOUNT",'S'),
      ("ACCOUNT_NAME;NAMA ACCOUNT",'S'),
      ("SUBACCOUNTCODE;NO REKENING",'S'),
      ("SUBACCOUNTNAME;NAMA REKENING",'S'),
      ("DESCRIPTION;KETERANGAN",'S'),
      ("REFERENCE_NO;NOMOR REFERENSI",'S'),
      ("ADD_DESCRIPTION;KETERANGAN_TAMBAHAN",'S'),
      ("TAG_JOURNAL;TAG JOURNAL",'S'),
    )
    
    judul_excel = "List Jurnal Transaksi"
    workbook = CreateExcelHeader(workbook, judul_excel, excel_cols)
    
    sqlparams = {
      'core_journal' : config.MapDBTableName('core.Journal')
      , 'core_journalitem' : config.MapDBTableName('core.JournalItem')
      , 'core_account' : config.MapDBTableName('core.Account')
      , 'treajournalbank' : config.MapDBTableName('TreaJournalBank')
      , 'nomor_rekening' : param.nomor_rekening      
      , 'start_date' : sutil.toDate(config, param.start_date)
      , 'end_date' : sutil.toDate(config, param.end_date)
    }
    
    sSQL = """
      select j.journal_no, j.journal_date, j.tag_journal, ji.amount_credit, ji.amount_debit, ji.fl_account, a.account_code, a.account_name, ji.subaccountcode, ji.subaccountname
         , j.description, ji.description as add_description, j.reference_no
      from {core_journal} j 
        inner join {core_journalitem} ji on ji.fl_journal = j.journal_no 
        inner join {treajournalbank} tj on tj.journal_no = j.journal_no
        inner join {core_account} a on a.account_code = ji.fl_account 
      where tj.nomor_rekening_bank = {nomor_rekening!r}
        and j.journal_date >= {start_date} and j.journal_date <= {end_date}
      order by j.journal_date, j.journal_no, j.tag_journal, ji.amount_credit, ji.journalitem_no
    """.format(**sqlparams)

    resQ = config.CreateSQL(sSQL).RawResult
        
    _R = 4
    
    while not resQ.Eof :
      _C = 1
      for _col in excel_cols:
        v_kolom = GetNilaiKolomExcel(config, _col, resQ)
        workbook.SetCellValue(_R, _C, v_kolom)        
        _C += 1
        # tutup for 
      _R += 1
      resQ.Next()
      # end while
      
    #dbg('before create corporate')
    #corporate = helper.CreateObject('Corporate')
    #dbg('corporate instantiated')
    FileName  = 'list_jurnal.xls'
    FullName  =  config.UserHomeDirectory + FileName
    dbg(FullName)
    if os.access(FullName, os.F_OK) == 1:
        os.remove(FullName)
    workbook.SaveAs(FullName)
    dbg('xls saved')        
    sw =  returnpacket.AddStreamWrapper()
    sw.Name = 'gen_report'
    sw.LoadFromFile(FullName)
    sw.MIMEType = "application/vnd.ms-excel"
    
    returnpacket.CreateValues(['IsErr', 0])
  except :
    returnpacket.CreateValues(['IsErr', 1], ['ErrMessage', str(sys.exc_info()[1]) ] )
    dbg('SERVER EXCEPTION ERROR : ' + str(sys.exc_info()[1]))

