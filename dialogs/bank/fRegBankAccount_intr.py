class fRegBankAccount :
  # TO USE THIS FORM:
  # call self.defineUIFromCode
  # [call self.defineAuthorizeOperation()] -- optional from authorization framework
  # call self.activate
  # call self.FormContainer.Show()
  
  # format : fieldname, type:str|num, acceptedCondition, errMsg
  arrDefValidation = [ ['LCounterpart.kode_counterpart', 'str', 'len(value) > 0', 'Counterpart harus dipilih'] 
      , ['LProduk.kode_produk' , 'str', 'len(value) > 0', 'Produk harus dipilih']
      , ['LCurrency.Currency_Code', 'str', 'len(value) > 0', 'Kode Valuta harus diisi'    ]
      , ['LCabang.Kode_Cabang'     , 'str', 'len(value) > 0', 'Cabang harus diisi'      ]
      #, ['nominal_deal'     , 'num', 'value > 0', 'Nominal harus diisi dan lebih besar dari 0']
      , ['tgl_buka'      , 'date', 'value > 0', 'Tanggal Buka Harus diisi'               ]
      #, ['jangka_waktu'     , 'num', 'value > 0', 'Jangka Waktu harus diisi dan lebih besar dari 0']
      #, ['tgl_jatuh_tempo'      , 'date', 'value > 0', 'Tanggal Jatuh Tempo Harus diisi'               ]
   ]
  

  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    pass
  
    
  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.kode_entri = kode_entri
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False
  #--
      
  def defineUIFromCode(self, kode_entri):
    self.kode_entri = kode_entri
    formObject = self.FormObject; app = formObject.ClientApplication; userApp = app.UserAppObject
    #--
    self.kode_entri = kode_entri
    self.authorizeMode = False
  #--
  
  def activate(self):
    #setFormView
    #self.SetFormView()
    pass

  def getValidationMsg(self):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    uip = self.uipTreasury
    # generic form
    arrValidation = []
    
    arrDefValidation = self.arrDefValidation
    arrValidation += arrDefValidation
      
    arrErrMsg = userApp.doValidation(uip, arrValidation)
    
    return arrErrMsg
  #--
  
  # save to server
  def saveFasilitas(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uipHelper = self.uipHelper
    
    #set data untuk pengecekan
    uipTreasury = self.uipTreasury
    uipTreasury.Edit()
    
    arrErrMsg = self.getValidationMsg()
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else :
      dlg_msg = 'Apakah anda yakin?'    

      dlg = app.ConfirmDialog(dlg_msg) 
      if dlg:
        form.CommitBuffer()
        params = form.GetDataPacket()
        
        #params.Packet.AcquireAnotherPacket(fp.Packet)
        resp = form.CallServerMethod('save', params)    
        status = resp.FirstRecord
        if status.isErr:
          app.ShowMessage('Error :' + status.errMsg)
        else:
          app.ShowMessage('%s berhasil dibuat. Nomor rekening = %s\r\nEfektif setelah otorisasi' % (uipTreasury.caption, status.nomor_rekening))    
          sender.ExitAction = 1
    #end else
  #--
  
  def AsDateTime(self, dateTuple):
    modDateTime = self.FormObject.ClientApplication.ModDateTime
    tanggal = modDateTime.EncodeDate(dateTuple[0], dateTuple[1], dateTuple[2])
    return tanggal      

  def LProdukOnExit(self, sender):
    uip = self.uipTreasury
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_produk;nama_produk;tipe_counterpart;basis_hari_pertahun;tipe_fasbis'

    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupTreaProduk", nField, lsData, 
      None,
      {
        'jenis_treasuryaccount': uip.jenis_treasuryaccount,
        'tipe_treasury':'P' #uip.kode_entri[2]
      })
    uip.Edit()
    uip.tipe_counterpart = uip.GetFieldValue(nField+'.tipe_counterpart')
    uip.basis_hari_pertahun = uip.GetFieldValue(nField+'.basis_hari_pertahun')
    
    return res


  def LCounterpartOnExit(self, sender):
    uip = self.uipTreasury
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_counterpart;nama_counterpart'

    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupCounterpart", nField, lsData, 
      None,
      {
        'tipe_counterpart':uip.tipe_counterpart
      })

    return res

  def LValutaOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'Currency_Code;Description'

    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res
      
    res = uapp.stdLookup(sender, "parameter@lookupCurrency", nField, lsData)
    return res

  def LCabangOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    lsData = 'kode_cabang;nama_cabang'
    
    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupCabang", nField, lsData)
    return res

      

    