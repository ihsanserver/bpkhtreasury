class fTransaksiBank :
  # TO USE THIS FORM:
  # call self.defineUIFromCode
  # [call self.defineAuthorizeOperation()] -- optional from authorization framework
  # call self.activate
  # call self.FormContainer.Show()
  
  # format : fieldname, type:str|num, acceptedCondition, errMsg
  arrDefValidation = [ ['LCounterpart.kode_counterpart', 'str', 'len(value) > 0', 'Counterpart harus dipilih'] 
      , ['LProduk.kode_produk' , 'str', 'len(value) > 0', 'Produk harus dipilih']
      , ['LCurrency.Currency_Code', 'str', 'len(value) > 0', 'Kode Valuta harus diisi'    ]
      , ['LCabang.Kode_Cabang'     , 'str', 'len(value) > 0', 'Cabang harus diisi'      ]
      #, ['nominal_deal'     , 'num', 'value > 0', 'Nominal harus diisi dan lebih besar dari 0']
   ]
  
  dMap={'B':0, 'G':1, 'L':2}

  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    pass
  
    
  def Show(self):
    self.configureForm(0)    
    self.FormContainer.Show()
  #--
  
  def configureForm(self, idxpage):
    form = self.FormObject;
    mpage = form.PyFormObject.multipages1

    mpage.GetPage(0).TabVisible = 0
    mpage.GetPage(1).TabVisible = 0
    mpage.GetPage(2).TabVisible = 0

    mpage.GetPage(idxpage).TabVisible = 1
    
  #--

  def rek_lawanOnChange(self, sender):
    idxpage = sender.ItemIndex
    self.configureForm(idxpage)    
    uip = self.uipData
    uip.ClearData()
  #--

  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.kode_entri = kode_entri
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False
  #--
      
  def defineUIFromCode(self, kode_entri):
    self.kode_entri = kode_entri
    formObject = self.FormObject; app = formObject.ClientApplication; userApp = app.UserAppObject
    #--
    self.kode_entri = kode_entri
    self.authorizeMode = False
  #--
  
  def activate(self):
    uip = self.uipTreasury
    self.configureForm(self.dMap[uip.rek_lawan])
    #pass

  def getValidationMsg(self):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    uip = self.uipTreasury
    # generic form
    arrValidation = []
    
    arrDefValidation = self.arrDefValidation
    arrValidation += arrDefValidation
      
    arrErrMsg = userApp.doValidation(uip, arrValidation)
    
    return arrErrMsg
  #--
  
  # save to server
  def saveFasilitas(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uipData = self.uipData
    
    #set data untuk pengecekan
    uipTreasury = self.uipTreasury
    uipTreasury.Edit()
    
    arrErrMsg = self.getValidationMsg()
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else :
      dlg_msg = 'Apakah anda yakin?'    

      dlg = app.ConfirmDialog(dlg_msg) 
      if dlg:
        form.CommitBuffer()
        params = form.GetDataPacket()
        
        #params.Packet.AcquireAnotherPacket(fp.Packet)
        resp = form.CallServerMethod('save', params)    
        status = resp.FirstRecord
        if status.isErr:
          app.ShowMessage('Error :' + status.errMsg)
        else:
          app.ShowMessage('Transaksi Bank Account Berhasil dibuat\r\nEfektif setelah otorisasi')    
          sender.ExitAction = 1
    #end else
  #--
  
  def LCabangOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    lsData = 'kode_cabang;nama_cabang'
    
    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupCabang", nField, lsData)
    return res


  def bCariRekeningDebetOnClick(self, sender):
    DataRekening = self.FormRekening.SearchRekeningByKodeJenis(JenisRekening = ['SAV','CA','RAB'])
    if DataRekening == None : return 0
    
    uipTran = self.uipTransaksi
    uipTran.Edit()
    uipTran.Nomor_Rekening_Debet =  DataRekening.NomorRekening
    
    self.SetRekeningDebet()


  def Kode_GLExit(self, sender):
    uip = self.uipData
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name

    res = uapp.checkStdLookup(sender, nField, '''account_code;account_name''' )      
    if res != None:
      return res
      
    res = uapp.stdLookup(sender, "facility@lookupGLAccount", nField, "account_code;account_name")
    #uip.account_name = uip.GetFieldValue(nField+'.account_name')

    return res

  def RC_Code_GL_exit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject                                 
    res = uapp.stdLookup(sender, "facility@lookupProject", 'rc_code_gl', "project_no;name", "kode_gl.account_type;rc_code_gl.project_no")
    return res

  def setRekSumber(self, cust, nField):
    uip = self.uipData
    uip.SetFieldValue(nField+'.nomor_rekening', cust['nomor_rekening'])
    uip.SetFieldValue(nField+'.nama_rekening', cust['nama_rekening'])

    uip.Nomor_Rekening_liab= cust['nomor_rekening']
    #uip.nama_rekening_lab = cust['nama_rekening']
    uip.kode_cabang_liab  = cust['kode_cabang']
    uip.Nama_Valuta_Liab  = cust['kode_valuta']
    uip.Produk_Liab       = cust['produk']
  
  def LRekeningLiabOnBeforeLookup(self, sender, linkui):
    # function(sender: TrtfDBLookupEdit; linkui: TrtfLinkUIElmtSetting): boolean
    uapp = self.FormObject.ClientApplication.UserAppObject
    uip = self.uipData
    dRekSumber = uapp.lookupRekSumber()
    #raise Exception, dRekSumber
    uip.Edit()
    if dRekSumber != None:
      self.setRekSumber(dRekSumber, sender.Name)
    #--
    return 0
  #--

  def setRekBank(self, cust, nField):
    uip = self.uipData
    uip.ClearData()
    if cust['nomor_rekening']==self.uipTreasury.nomor_rekening:
      raise Exception, 'Rekening Lawan tidak boleh sama dengan rekening Bank!'

    uip.SetFieldValue(nField+'.nomor_rekening', cust['nomor_rekening'])
    uip.SetFieldValue(nField+'.nama_rekening', cust['nama_rekening'])

    uip.NoRekBank        = cust['nomor_rekening']
    uip.bank_cabang      = cust['kode_cabang']
    uip.bank_valuta      = cust['kode_valuta']
    uip.bank_produk      = cust['produk']
    uip.bank_counterpart = cust['counterpart']
  
  def LRekBankOnBeforeLookup(self, sender, linkui):
    # function(sender: TrtfDBLookupEdit; linkui: TrtfLinkUIElmtSetting): boolean
    uapp = self.FormObject.ClientApplication.UserAppObject
    dRekSumber = uapp.lookupRekBank()
    if dRekSumber != None:
      self.setRekBank(dRekSumber, sender.Name)
    #--
    return 0
  #--
    