class fDownloadListJournal:
  def __init__(self, formObj, parentForm):
    pass
  #--
                                    
  def bCetakOnClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    form.CommitBuffer()
    
    param = app.CreateValues(
        ['nomor_rekening', self.uipFilter.nomor_rekening], 
        ['start_date', self.uipFilter.start_date or ''],
        ['end_date', self.uipFilter.end_date or '']
      )
    
    resp = self.FormObject.CallServerMethod('PrintXLS', param)
    
    status = resp.FirstRecord
    if status.IsErr:
      app.ShowMessage(status.ErrMessage)
      return
      
    oPrint = self.FormObject.ClientApplication.GetClientClass('PrintLib_2','PrintLib')()
    oPrint.doProcessByStreamName('gen_report',app,resp.packet,3)

