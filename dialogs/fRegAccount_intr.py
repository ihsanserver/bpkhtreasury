class fRegAccount :
  # TO USE THIS FORM:
  # call self.defineUIFromCode
  # [call self.defineAuthorizeOperation()] -- optional from authorization framework
  # call self.activate
  # call self.FormContainer.Show()
  
  kode_entri = ''
  
  eqv_rate='';period_count='';amount_check='';spr_rate='';rep_period=''
  dictMapping = {
       #--Deposito Antar Bank Syariah
      'TRP001': {
        'contract_class_name': "TreaDeposito",
        'caption': "Penempatan Deposito Pada Bank Lain",
        'subframe_id': "deposito/fInputNewAccount",
        'jenis_treasuryaccount': "D",
        'contract_uip_name': "uipData"
      },
       #--Deposito SIMA
      'TRP002': {
        'contract_class_name': "TreaSima",
        'caption': "Penempatan Sertifikat Investasi Mudharabah Antar Bank (SIMA)",
        'subframe_id': "sima/fInputNewAccount",
        'jenis_treasuryaccount': "S",
        'contract_uip_name': "uipData"
      },
       #--Deposito FASBIS/SBIS
      'TRP003': {
        'contract_class_name': "TreaFasbis",
        'caption': "Penempatan FASBIS / SBIS",
        'subframe_id': "fasbis/fInputNewAccount",
        'jenis_treasuryaccount': "F",
        'contract_uip_name': "uipData"
      },
       #--SUKUK Negara
      'TRP004N': {
        'contract_class_name': "TreaSuratBerharga",
        'caption': "Penempatan Surat Berharga Syariah Negara",
        'subframe_id': "sukuk/fInputNewAccount",
        'jenis_treasuryaccount': "B",
        'contract_uip_name': "uipData"
      },
       #--SUKUK Korporasi
      'TRP004C': {
        'contract_class_name': "TreaSuratBerharga",
        'caption': "Penempatan Surat Berharga Syariah Korporasi",
        'subframe_id': "sukuk/fInputNewAccountC",
        'jenis_treasuryaccount': "B",
        'contract_uip_name': "uipData"
      },
       #--SUKUK SBSN
      'TRP004S': {
        'contract_class_name': "TreaSuratBerharga",
        'caption': "Penempatan SBSN",
        'subframe_id': "sukuk/fInputNewAccountS",
        'jenis_treasuryaccount': "B",
        'contract_uip_name': "uipData"
      },
       #--Deposito Reverse Revo
      'TRP005': {
        'contract_class_name': "TreaReverseRepo",
        'caption': "Penempatan Reverse Repo",
        'subframe_id': "repo/fInputNewAccount",
        'jenis_treasuryaccount': "R",
        'contract_uip_name': "uipData"
      },
      'TRP006': {
       #--Deposito Reksadana
        'contract_class_name': "TreaReksadana",
        'caption': "Penempatan Reksadana",
        'subframe_id': "reksadana/fInputNewAccount",
        'jenis_treasuryaccount': "A",
        'contract_uip_name': "uipData"
      }
    }
  
  # format : fieldname, type:str|num, acceptedCondition, errMsg
  arrDefValidation = [ ['LCounterpart.kode_counterpart', 'str', 'len(value) > 0', 'Counterpart harus dipilih'] 
      , ['LProduk.kode_produk' , 'str', 'len(value) > 0', 'Produk harus dipilih']
      , ['LCurrency.Currency_Code', 'str', 'len(value) > 0', 'Kode Valuta harus diisi'    ]
      , ['LCabang.Kode_Cabang'     , 'str', 'len(value) > 0', 'Cabang harus diisi'      ]
      , ['nominal_deal'     , 'num', 'value > 0', 'Nominal harus diisi dan lebih besar dari 0']
      , ['tgl_buka'      , 'date', 'value > 0', 'Tanggal Buka Harus diisi'               ]
      , ['jangka_waktu'     , 'num', 'value > 0', 'Jangka Waktu harus diisi dan lebih besar dari 0']
      , ['tgl_jatuh_tempo'      , 'date', 'value > 0', 'Tanggal Jatuh Tempo Harus diisi'               ]
  ]
  
  dRef = {
     'Lperingkat_debitur':'PERINGKAT_'
     ,'Llembaga_pemeringkat':'LEMBAGA_PEMERINGKAT'
     ,'Lhubungan_bank':'R_HUBUNGAN_DENGAN_BANK'
     ,'Lstatus':'RSTATUS'
     ,'Lnegara_lawan':'R_KODE_NEGARA'
     ,'LKategori_Portofolio':'REF_KATEGORI_PORTOFOLIO'
  }  

  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    pass
    
  def defineUserInterface(self, contract_class_name, caption, subframe_id, jenis_treasuryaccount, contract_uip_name):
    self.subframe_id = subframe_id
    self.contract_class_name = contract_class_name
    self.jenis_treasuryaccount = jenis_treasuryaccount
    self.contract_uip_name = contract_uip_name
    self.FormObject.Caption = caption
    
  def defineUIFromCode(self, kode_entri):
    self.kode_entri = kode_entri
    formObject = self.FormObject; app = formObject.ClientApplication; userApp = app.UserAppObject
    entri = self.dictMapping.get(kode_entri, None)
    if entri == None:
      raise Exception, "Entry code %s not found" % kode_entri
    userApp.clientAttrUtil.transferAttributes([
      'contract_class_name', 'caption', 'subframe_id', 
      'jenis_treasuryaccount', 'contract_uip_name'
      ], self, entri)
    if self.parentForm != None:
      self.parentForm.FormObject.Caption = self.caption
    else:
      self.FormObject.Caption = self.caption
    #--
    self.kode_entri = kode_entri
    self.authorizeMode = False
  #--
  
  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.kode_entri = kode_entri
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False

  def activate(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    #app.ShowMessage('activate() called')
    if self.authorizeMode:
      ph = app.CreateValues(['operation', 'view_authorize'], ['id_otorisasi', self.id_otorisasi])
    else:
      self.setDefaultFormData()
      ph = None
    systemContext = formObj.SystemContext
    if systemContext != "":
      subframe_id = "%s://%s" % (systemContext, self.subframe_id)
    else:
      subframe_id = self.subframe_id 
    #app.ShowMessage('system context: ' + subframe_id)
    self.pyForm = self.frameAkad.Activate(subframe_id, ph, None)
    if self.authorizeMode:
      self.pyForm.FormObject.SetAllControlsReadOnly()
    
    #setFormView
    self.SetFormView()
      
  # mengisi beberapa elemen form dengan nilai default 
  def setDefaultFormData(self):
    uip = self.uipTreasury 
    uip.Edit()
    uip.jenis_treasuryaccount = self.jenis_treasuryaccount
    uip.contract_uip_name = self.contract_uip_name
    uip.contract_class_name = self.contract_class_name
    uip.caption = self.caption
    uip.kode_entri = self.kode_entri    
    
  # parsing kondisi validasi dengan memperhitungkan token :parameter_name untuk kondisi yang terkait dengan variabel lain
  def parseCondition(self, uipart, defValidation) :
    TOKEN_PARAM = ':'
    elmt = defValidation
    fname = elmt[0]; ftype = elmt[1] ; cond = elmt[2] ; errMsg = elmt[3]
    arrCondWords = cond.split(' ') ; idx = 0 ; value = ''
    for word in arrCondWords :
      # check token params
      if word[0] == TOKEN_PARAM :
        paramFieldName = word[1:] #without first char
        #get param value
        tmpval = uipart.GetFieldValue(paramFieldName) or ""
        if isinstance(tmpval, str) :
          arrCondWords[idx] = ''' '%s' ''' % tmpval
        else :
          arrCondWords[idx] = str(tmpval)
      else :
        pass # currently do nothing
              
      if ftype == 'str' :
        value = uipart.GetFieldValue(fname) or ""
        value = value.strip()
      elif ftype == 'num' :
        tmpval = uipart.GetFieldValue(fname)
        value = -999 if tmpval == None else tmpval          
      else : raise Exception, "doValidation : unknown type %s" % (ftype)         
      idx = idx + 1
    # rebuild cond
    cond = " ".join(arrCondWords)
    return cond, value
  #--
      
  def getValidationMsg(self):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    uip = self.uipTreasury
    # generic form
    arrValidation = []
    if uip.jenis_treasuryaccount in ['D']:
        arrValidation = [['eqv_rate_real', 'num', 'value > 0', 'Coupon Rate harus diisi dan lebih besar dari 0']]
    
    arrDefValidation = self.arrDefValidation
    arrValidation += arrDefValidation
      
    arrErrMsg = userApp.doValidation(uip, arrValidation)
    
    # specialized form
    formAkad = self.frameAkad.ContainedFormObject
    formAkadPy = self.fr_frameAkad
    arrDefValidationAkad =[]
    arrDefValidationAkad = formAkadPy.GetArrValidationDef()
    uipName = self.dictMapping[self.kode_entri]['contract_uip_name']
    uipAkad = formAkad.GetUIPartByName(uipName) 
    arrErrMsgAkad = userApp.doValidation(uipAkad, arrDefValidationAkad)
    arrErrMsg = arrErrMsg + arrErrMsgAkad
    return arrErrMsg
  #--
  
  def OnEnterNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnEnterNum(sender)
  #--
  
  def OnExitNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnExitNum(sender)
  #--

  def cekLainnya(self):
    # specialized form
    #formAkadPy = self.fr_frameAkad
    #if self.jenis_treasuryaccount in ['F']:
      #formAkadPy.count_ujroh()
    self.count_ujroh()
  
  def AsDateTime(self, dateTuple):
    modDateTime = self.FormObject.ClientApplication.ModDateTime
    tanggal = modDateTime.EncodeDate(dateTuple[0], dateTuple[1], dateTuple[2])
    return tanggal      

  def LProdukOnExit(self, sender):
    uip = self.uipTreasury
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_produk;nama_produk;tipe_counterpart;basis_hari_pertahun;tipe_fasbis'

    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupTreaProduk", nField, lsData, 
      None,
      {
        'jenis_treasuryaccount':uip.jenis_treasuryaccount,
        'tipe_treasury':self.kode_entri[2]
      })
    uip.Edit()
    uip.tipe_counterpart = uip.GetFieldValue(nField+'.tipe_counterpart')
    uip.basis_hari_pertahun = uip.GetFieldValue(nField+'.basis_hari_pertahun')
    
    if uip.jenis_treasuryaccount=='F':
      formAkad = self.frameAkad.ContainedFormObject
      uipAkad = formAkad.GetUIPartByName('uipData')
      uipAkad.Edit()
      tipe_fasbis = uip.GetFieldValue(nField+'.tipe_fasbis')
      uipAkad.tipe_fasbis = tipe_fasbis
      self.fr_frameAkad.settipe_baghas(tipe_fasbis)
    return res
    
  def LCounterpartOnExit(self, sender):
    uip = self.uipTreasury
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_counterpart;nama_counterpart'

    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupCounterpart", nField, lsData, 
      None,
      {
        'tipe_counterpart':uip.tipe_counterpart
      })

    return res

  def LValutaOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'Currency_Code;Description'

    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res
      
    res = uapp.stdLookup(sender, "parameter@lookupCurrency", nField, lsData)
    return res

  def LCabangOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    lsData = 'kode_cabang;nama_cabang'
    
    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupCabang", nField, lsData)
    return res

  def jangka_waktuOnExit(self, sender):
    app = self.FormObject.ClientApplication
    uip = self.uipTreasury
    if uip.jangka_waktu == None:
      app.ShowMessage('Jangka Waktu harus diisi..!!')
      self.pForm_jangka_waktu.SetFocus()
    else:
      opendate=self.AsDateTime(uip.tgl_buka)  
      uip.tgl_jatuh_tempo = opendate + uip.jangka_waktu
  
  def eqv_rate_realOnExit(self, sender):
    self.count_ujroh()
    if self.jenis_treasuryaccount in ['D', 'R']: 
      self.count_tunai()

  def count_ujroh(self):
    uip = self.uipTreasury
    formAkad = self.frameAkad.ContainedFormObject
    uipAkad = formAkad.GetUIPartByName('uipData')
    uipAkad.Edit()
    uipAkad.ujroh = round(uip.nominal_deal*(uip.eqv_rate_real/100)*uip.jangka_waktu/uip.basis_hari_pertahun,6)
    #raise Exception, '%s x %s x %s x %s : %s' % (uipParent.nominal_deal, (uip.eqv_rate_real/100), uipParent.jangka_waktu, uipParent.basis_hari_pertahun, uip.ujroh)
  
  def count_tunai(self):
    uip = self.uipTreasury
    formAkad = self.frameAkad.ContainedFormObject
    uipAkad = formAkad.GetUIPartByName('uipData')
    uip.Edit()
    uipAkad.nilai_tunai = uip.nominal_deal + uipAkad.ujroh
    
  def ReffOnExit(self, sender):
    form = sender.OwnerForm
    uip = self.uipTreasury
    uapp = form.ClientApplication.UserAppObject
    nField = sender.Name

    res = uapp.checkStdLookup(sender, nField, 
      '''reference_code;reference_desc;refdata_id'''
    )
    if res != None:
      return res
      
    kategori = self.dRef[nField]
    #addCond = self.getAddedCondition(nField)
    #if nField in ('Llembaga_pemeringkat','Lperingkat_debitur', 'Lhubungan_bank', 'Lstatus', 'Ljenis_operasional', 'Lnegara_lawan'):
    ref_lookup = "reference@lookup_refdata"
      
    uapp = form.ClientApplication.UserAppObject
    res = uapp.stdLookup(sender, ref_lookup, nField, 
      "reference_code;reference_desc;refdata_id", 
      None,
      {
        'reference_code':uip.GetFieldValue('%s.reference_code' % nField),
        'reference_name':'',
        'nama_kategori':kategori,
        #'addCond':addCond
      })
    
    if nField in ('Llembaga_pemeringkat','Lperingkat_debitur'):  
      self.definedRefChild(nField)      
    
    return res

  def definedRefChild(self, nField):
    uip = self.uipTreasury
    if nField in ('Llembaga_pemeringkat'): 
      if uip.GetFieldValue(nField+'.reference_code')<>'00':
        self.pAdditionalData_Lperingkat_debitur.enabled=1
        self.pAdditionalData_rating_date.enabled=1
        self.dRef['Lperingkat_debitur'] = 'PERINGKAT_'+uip.GetFieldValue(nField+'.reference_code')
        uip.ClearLink('Lperingkat_debitur')
        self.pAdditionalData_Lperingkat_debitur.SetFocus()
      else:
        self.pAdditionalData_rating_date.enabled=0
        self.pAdditionalData_Lperingkat_debitur.enabled=0
        #uip.ClearLink('Lperingkat_debitur')
        uip.SetFieldValue('Lperingkat_debitur.refdata_id', uip.GetFieldValue('Llembaga_pemeringkat.refdata_id'))
        uip.SetFieldValue('Lperingkat_debitur.reference_code' ,uip.GetFieldValue('Llembaga_pemeringkat.reference_code'))
        uip.SetFieldValue('Lperingkat_debitur.reference_desc', uip.GetFieldValue('Llembaga_pemeringkat.reference_desc'))
    
  def SetFormView(self):
    uipTrea = self.uipTreasury
    self.FormObject.PyFormObject.mInput.GetPage(2).TabVisible = False #Override
      
    if uipTrea.jenis_treasuryaccount == 'F':
      self.FormObject.PyFormObject.mInput.GetPage(1).TabVisible = False
      self.pForm_nisbah.Visible = False 
        
    if uipTrea.jenis_treasuryaccount in ['D', 'R']:
      self.pAdditionalData_additional_fitur.Visible = False
      self.pAdditionalData_status_instrumen.Visible = False 
      self.pAdditionalData_metode_pengukuran.Visible = False 
      self.pAdditionalData_LKategori_Portofolio.Visible = False  
      self.pAdditionalData_periode_imbalan.Visible = False 
    
    if uipTrea.jenis_treasuryaccount == 'R':
      self.pForm_nisbah.Visible = False
      
    if uipTrea.jenis_treasuryaccount == 'A':
      self.pForm_nisbah.Visible = False      
      self.pForm_eqv_rate_real.Visible = False
  
  # save to server
  def saveFasilitas(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uipHelper = self.uipHelper
    
    #set data untuk pengecekan
    uipTreasury = self.uipTreasury
    uipTreasury.Edit()
    
    arrErrMsg = self.getValidationMsg()
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else : 
      dlg_msg = 'Apakah anda yakin?'    
      
      dlg = app.ConfirmDialog(dlg_msg) 
      if dlg:
        self.cekLainnya()
        # ambil dari frame
        formAkad = self.frameAkad.ContainedFormObject
        formAkad.CommitBuffer()
        fp = formAkad.GetDataPacket()
                
        form.CommitBuffer()
        params = form.GetDataPacket()
        
        params.Packet.AcquireAnotherPacket(fp.Packet)
                
        resp = form.CallServerMethod('save', params)    
        status = resp.FirstRecord
        if status.isErr:
          app.ShowMessage('Error :' + status.errMsg)
        else :
          #cek limit transaksi
          if status.override_state=='F':
            app.ShowMessage('Transaksi Melebihi Limit User, Override transaksi Untuk Selesaikan Entri transaksi..!')
            
          app.ShowMessage('%s berhasil dibuat. Nomor rekening = %s\r\nEfektif setelah otorisasi' % (uipTreasury.caption, status.nomor_rekening))    
          sender.ExitAction = 1
        
          #Cetak Validasi
          uipTreasury.Edit()
          uipTreasury.id_otorisasi = status.id_otorisasi
          self.cetakValidasi()
        #--
      #--
    #--
  #--
     
  def cetakValidasi(self):
    form = self.FormObject
    app = form.ClientApplication
    # ambil dari frame
    formAkad = self.frameAkad.ContainedFormObject
    formAkad.CommitBuffer()
    fp = formAkad.GetDataPacket()
                
    form.CommitBuffer()
    params = form.GetDataPacket()

    params.Packet.AcquireAnotherPacket(fp.Packet)    
    #cetak Validasi    
    cv = form.CallServerMethod('cetakValidasi', params)    
    status = cv.FirstRecord
    
    if status.isErr:
      app.ShowMessage('Error : %s' + status.errMsg)
      return
    
    try:       
      app.ShowMessage("Masukkan Slip Transaksi ke Printer") 
      oPrint = self.FormObject.ClientApplication.GetClientClass('PrintLib','PrintLib')()
      oPrint.doProcess(app, cv.packet, 2)
    except:
      app.ShowMessage("Proses cetak slip gagal. Silahkan cek koneksi ke printer.")
  #--