import sys
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.modman as modman
import rpdb2

import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.net.message as message

'''
def checkCondition(oTreaAcc):
  if oTreaAcc.isRepaymentCompleted() : raise Exception, "ID: Tidak bisa melakukan transaksi pembayaran,\n Nomor Rekening %s sudah lunas" % oTreaAcc.Nomor_Rekening
  if oTreaAcc.isWriteOff() : raise Exception, "ID: Tidak bisa melakukan transaksi,\n Nomor Rekening %s sudah di write off" % oTreaAcc.Nomor_Rekening
  if oTreaAcc.isAYDA() : raise Exception, "ID: Tidak bisa melakukan transaksi,\n Nomor Rekening %s sudah AYDA" % oTreaAcc.nomor_rekening
'''

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipTreaAcc;uipTreaCollateralAccount;uipDelete"): return
  fr = params.FirstRecord
  
  # form ini tidak memiliki dataset yang disimpan untuk otorisasi, sebagai gantinya
  # data selalu diambil dari database, berdasarkan informasi nomor_rekening
  # untuk kasus otorisasi, nomor_rekening disimpan di object otorisasi, di field info1
  if fr != None and fr.Dataset.IsFieldExist("operation") and fr.operation == "view_authorize":
    id_otorisasi = fr.id_otorisasi
    oOtorEntri = helper.GetObject("enterprise.OtorEntri", id_otorisasi)
    nomor_rekening = oOtorEntri.info1
  else:
    nomor_rekening = fr.nomor_rekening
  oTreasuryAccount = helper.GetObject("TreasuryAccount", nomor_rekening)
  if oTreasuryAccount.IsNull:
    raise Exception, "Data not found. TreasuryAccount: %s" % nomor_rekening
    
  keyconst = "PObj:TreasuryAccount#nomor_rekening=%s" % nomor_rekening
  uideflist.SetData('uipTreaAcc', keyconst)
  
  ds = uideflist.uipTreaAcc.Dataset
  rec = ds.GetRecord(0)
  rec.jenis_treasuryaccount = oTreasuryAccount.jenis_treasuryaccount
  
#--  

def authorize(config, params, returns):
  #rpdb2.start_embedded_debugger('000')  
  helper = pobjecthelper.PObjectHelper(config)
  
  dsTreaAcc = params.uipTreaAcc
  recTreaAcc = dsTreaAcc.GetRecord(0)
  ds = params.uipTreaCollateralAccount

  config.BeginTransaction()
  try :
    dsDelete = params.uipDelete
    for i in range(dsDelete.RecordCount):
      rec = dsDelete.GetRecord(i)
      # process deletion
      if rec.nomor_rekening not in [0, '', None]:
        oColl = helper.GetObject('FinCollateralAsset', rec.nomor_rekening )
        oTreaColAccount = helper.GetObject("TreaCollateralAccount", {'norek_treaAccount': recTreaAcc.nomor_rekening,'norek_finCollateralAsset': rec.nomor_rekening})
        if not oTreaColAccount.IsNull:
          oTreaColAccount.Delete()
  
        #config.FlushUpdates()
        #oColl.calcParipasu()
        oColl.colasset_status_update = 'B' if oColl.isLinked() else 'R'
        #raise Exception, oColl.colasset_status_update
      
    for i in range(ds.RecordCount):
      rec = ds.GetRecord(i)
      status = rec.GetFieldByName("__SYSFLAG")
      #raise Exception, str(status)
      if status == "M":
        nomor_rekening_fincol = rec.GetFieldByName('LFinCollateralAsset.Nomor_Rekening')
        oTreaColAccount = helper.GetObject("TreaCollateralAccount", {'norek_treaAccount': recTreaAcc.nomor_rekening, 
          'norek_finCollateralAsset': nomor_rekening_fincol})
          
        oTreaColAccount.UpdatePengikatan({'TreaCollateralAccount': rec})
      elif status == "N":
        oTreasuryAccount = helper.GetObject("TreasuryAccount", recTreaAcc.nomor_rekening)
        oTreaCollateralAccount = helper.CreatePObject('TreaCollateralAccount', {'TreaCollateralAccount': rec, 'oTreasuryAccount': oTreasuryAccount})
      #--
    #--
    nomor_rekening = recTreaAcc.nomor_rekening
    oTreaAcc = helper.GetObject("TreasuryAccount", {'nomor_rekening': nomor_rekening})
    
    #if dsDelete.RecordCount > 0:
      #oTreaAcc.AfterUnLink() #error disini
    
    oTreaAcc.status_update = 'F'
    #raise Exception, oTreaAcc.collateral_value
    
    #oCustAdditionalData.ProcessUIData(recFinCustAdditionalData)
    config.FlushUpdates() # cek generate SQL
  except :
    config.Rollback()
    isErr = 1
    errMsg = str(sys.exc_info()[1])
    raise Exception, errMsg
  #--
  
#--  
  
def reject(config, params, returns):
   helper = pobjecthelper.PObjectHelper(config)
   recTreaAcc = params.uipTreaAcc.GetRecord(0)
   nomor_rekening = recTreaAcc.nomor_rekening 
   oTreaAcc = helper.GetObject("TreasuryAccount", {'nomor_rekening': nomor_rekening})
   oTreaAcc.status_update = 'F' 

def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000', True, True)
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  
  dsTreaAcc = params.uipTreaAcc
  recTreaAcc = dsTreaAcc.GetRecord(0)
  ds = params.uipTreaCollateralAccount
  ds_temp = ds
  # check input
  A=[]
  for i in range(ds.RecordCount):
    rec = ds.GetRecord(i)
    x=0
    for t in range(ds_temp.RecordCount):
      rec_temp = ds_temp.GetRecord(t)
      if rec.GetFieldByName('LFinCollateralAsset.Nomor_Rekening') == rec_temp.GetFieldByName('LFinCollateralAsset.Nomor_Rekening') :
         x=x+1
    A.append(x)
  if sum(A) > ds.RecordCount :
    raise Exception, 'Tidak boleh menginputkan lebih dari satu agunan yang sama';
     
  config.BeginTransaction()
  try :

    nomor_rekening = recTreaAcc.nomor_rekening 
    oTreaAcc = helper.GetObject("TreasuryAccount", {'nomor_rekening': nomor_rekening})
    oTreaAcc.status_update = 'T' 
    
    recOtor = otorisasi.prepareParameterOtorisasi(params)
    recOtor.kode_entri = 'TRLINK'
    recOtor.keterangan = "Link agunan %s" % (nomor_rekening)
    recOtor.info1 = nomor_rekening
    recOtor.info2 = None
    recOtor.tipe_target = 0
    recOtor.id_peran = "OM"
    recOtor.nama_tabel = 'TreaCollateralAccount'
    recOtor.tipe_key = 1
    recOtor.key_string = nomor_rekening
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph) 
    
    config.FlushUpdates() # cek generate SQL
    config.Commit()
    isErr = 0
    errMsg = ""
  except :
    config.Rollback()
    isErr = 1
    errMsg = str(sys.exc_info()[1])
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg])
#--  



"""
def TreaCollateralAccount_AfterNewRow(sender, data):
  TreaCollateralAccount = sender.ActiveInstance
  TreasuryAccount = sender.UIDefList.FinAcc.ActiveInstance 
  TreaCollateralAccount.norek_fincollateralasset = sender.ActiveRecord.GetFieldByName('LFinCollateralAsset.nomor_rekening')
  TreaCollateralAccount.norek_TreasuryAccount = TreasuryAccount.nomor_rekening
#--
"""

def TreaCollateralAccount_OnSetData(sender):
  rec = sender.ActiveRecord
  obj = sender.ActiveInstance
  oFinCollAsset = obj.LFinCollateralAsset
  #if oFinCollAsset.dasar_nilai == 'P' :
  #  rec.value_pct_use = obj.pct_use
  #if oFinCollAsset.dasar_nilai == 'N' :
  #  rec.value_pct_use = obj.value_use
  rec.liquidation_value = oFinCollAsset.liquidation_value
  rec.reserve_deduct_value = oFinCollAsset.reserve_deduct_value
