class fInputPaymentBaghas:
  def __init__(self, formObj, parentForm):
    self.parentForm = parentForm
    pass
  #--
  
    
  def FormAfterProcessServerData(self, formobj, operationid, datapacket):
    # function(formobj: TrtfForm; operationid: integer; datapacket: TPClassUIDataPacket):boolean
    if self.uipData.tipe_treasury in ['B']:
      self.panel1_saldo_accrue.visible=1
      self.panel1_saldo_margin.visible=0

  def bayar_sesuai_tanggalOnClick(self, sender):
    uip = self.uipData
    uip.Edit()
    if self.panel1_bayar_sesuai_tanggal.Checked:  
      self.panel1_amount.Enabled = False
      self.parentForm.count_baghas()
    else:
      self.panel1_amount.Enabled = True
      self.panel1_amount.SetFocus()