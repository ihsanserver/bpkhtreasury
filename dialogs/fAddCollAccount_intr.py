class fAddCollAccount:
  arrDefValidation = [ ['norek_treaAccount', 'str', 'len(value) > 0', 'Nomor Rekening'] 
      , ['LFinCollateralAsset.Nomor_Rekening' , 'str', 'len(value) > 0', 'Nomor Rekening Jaminan']
      , ['tanggal_pengikatan' , 'date', 'value > 0', 'Tanggal Pengikatan']
      , ['Lref_jenis_pengikatan.reference_code' , 'str', 'len(value) > 0', 'Jenis Pengikatan']
      , ['legal_binding' , 'str', 'len(value) > 0', 'Sifat Pengikatan']
      #, ['value_use' , 'num', 'value > 0', 'Nilai Yang digunakan']
  ]

  arr_valid =[]
  
  dRef = {
     'Lref_jenis_pengikatan':'REF_JENIS_PENGIKATAN',
     'LGolPenerbit':'R_BANK_DAN_PIHAK_KE3',
     'LJenis_Agunan':'R_JENIS_AGUNAN',      
  }

  def __init__(self, formObj, parentForm):
    self.app = formObj.ClientApplication
  #--
  
  def Show(self, params): # invoked for non-authorization mode
    form = self.FormObject    
    self.AssignEdit(params) 
       
    #self.FormObject.Caption += ACaption
    res = self.FormContainer.Show()
    if res == 1:
      form.CommitBuffer()
      phFS = form.GetDataPacket()

      return phFS
    else:
      return 0
  #--

  def AssignEdit(self, data):
    fMode = data['fMode']
    uipCS = self.uipTreaColAccount
    uipCS.Edit()
    uipCS.SetFieldValue('norek_treaAccount', data['norek_treaAccount'])
    uipCS.SetFieldValue('is_ppap_deduction', 'F')     
    uipCS.jenis_treasuryaccount = data['jenis_treasuryaccount']
    
    if fMode == 'edit':
      del data['fMode']
      self.FormObject.Caption = 'Edit Link Agunan'
      self.panel1_LFinCollateralAsset.enabled=0
      uipCS.SetFieldValue('Lref_jenis_pengikatan.reference_code', data['kode_ref'])
      uipCS.SetFieldValue('Lref_jenis_pengikatan.reference_desc', data['keterangan_ref'])   
      uipCS.SetFieldValue('LGolPenerbit.reference_code', data['kode_penerbit'])
      uipCS.SetFieldValue('LGolPenerbit.reference_desc', data['keterangan_penerbit'])    
      uipCS.SetFieldValue('LJenis_Agunan.reference_code', data['kode_agunan'])
      uipCS.SetFieldValue('LJenis_Agunan.reference_desc', data['keterangan_agunan'])
      for col_name in data:
        uipCS.SetFieldValue(col_name, data[col_name])

    uipCS.SetFieldValue('pct_use', 0)
    
  def eRefLinkOnExit(self, sender):
    form = sender.OwnerForm
    uip = self.uipTreaColAccount
    uapp = form.ClientApplication.UserAppObject
    nField = sender.Name

    res = uapp.checkStdLookup(sender, nField, 
      '''reference_code;reference_desc;refdata_id'''
    )
    if res != None:
      return res
    
    if uip.jenis_treasuryaccount in ['S'] and nField == 'LGolPenerbit':
      kategori = 'REF_GOLONGAN_PENERBIT'
    else:
      kategori = self.dRef[nField]     
      
    ref_lookup = "reference@lookup_refdata"

    uapp = form.ClientApplication.UserAppObject
    res = uapp.stdLookup(sender, ref_lookup, nField, 
      "reference_code;reference_desc;refdata_id", 
      None,
      {
        'reference_code':uip.GetFieldValue('%s.reference_code' % nField),
        'reference_name':'',
        'nama_kategori':kategori,
        'addCond':''
      })
      
    uip.Edit()
    if nField == 'Lref_jenis_pengikatan':
      uip.SetFieldValue('ref_jenis_pengikatan', uip.GetFieldValue(nField+'.refdata_id'))
      uip.SetFieldValue('kode_ref', uip.GetFieldValue(nField+'.reference_code'))
      uip.SetFieldValue('keterangan_ref', uip.GetFieldValue(nField+'.reference_desc'))
    if nField == 'LGolPenerbit':
      uip.SetFieldValue('golongan_penerbit', uip.GetFieldValue(nField+'.refdata_id'))
      uip.SetFieldValue('kode_penerbit', uip.GetFieldValue(nField+'.reference_code'))
      uip.SetFieldValue('keterangan_penerbit', uip.GetFieldValue(nField+'.reference_desc'))
    if nField == 'LJenis_Agunan':
      uip.SetFieldValue('jenis_agunan', uip.GetFieldValue(nField+'.refdata_id'))
      uip.SetFieldValue('kode_agunan', uip.GetFieldValue(nField+'.reference_code'))
      uip.SetFieldValue('keterangan_agunan', uip.GetFieldValue(nField+'.reference_desc'))
    
    return res
       
  def LFinCollateralAssetOnExit(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uapp = self.FormObject.ClientApplication.UserAppObject
    uip = self.uipTreaColAccount
    lsField = '''Nomor_Rekening;asset_type;valuation;market_value;liquidation_value;value_use;pemilik_agunan;bukti_kepemilikan'''
    g_nasabah = uip.group_nasabah or ''
    nField = sender.Name
    res = uapp.checkStdLookup(sender, nField, lsField)      
    if res != None:
      return res

    vField = nField[1:]
    res = uapp.stdLookup(sender, "facility@lookupFinCollateralAsset", nField, lsField,None,
       {})
    uip.Edit()
    uip.nomor_rekening = uip.GetFieldValue(nField+'.Nomor_Rekening')
    uip.nama_nasabah = uip.GetFieldValue(nField+'.pemilik_agunan')
    uip.valuation = uip.GetFieldValue(nField+'.valuation')
    uip.market_value = uip.GetFieldValue(nField+'.market_value')
    uip.liquidation_value = uip.GetFieldValue(nField+'.liquidation_value')
    uip.used_value = uip.GetFieldValue(nField+'.value_use')
    
    return res    
          
  def getValidationMsg(self, DefVals):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    uip = self.uipTreaColAccount
    # generic form
    arrErrMsg = userApp.doValidation(uip, DefVals)

    return arrErrMsg
  #--   

  def button1OnClick(self, sender):
    # procedure(sender: TrtfButton)
    form = self.FormObject
    app = form.ClientApplication
                  
    arrErrMsg = self.getValidationMsg(self.arr_valid)
    #arrErrMsg = self.frameSpecific.ContainedForm.getValidationMsg()
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg+ '\nHarus diisi.!!!')
    else :        
      # ambil dari frame
      sender.ExitAction = 1
  #--


  def paripasuOnExit(self, sender):
    # procedure(sender: TrtfDBEdit)
    uip = self.uipTreaColAccount
    uip.paripasu_liquidation_value = uip.market_value*(uip.paripasu/100)