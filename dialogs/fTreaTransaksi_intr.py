class fTreaTransaksi:
  
  # format : fieldname, type:str|num, acceptedCondition, errMsg
  arrDefValidation = [ ['nomor_ref', 'str', 'len(value) > 0', 'Nomor Referensi belum diinput'] 
      , ['keterangan', 'str', 'len(value) > 0', 'Keterangan belum diinput'] 
 ]

  arrTreaValidation = [ ['LMember.Nomor_Nasabah', 'str', 'len(value) > 0', 'Anggota Group Belum Input'] ]
 
  _cols = [
    'kode_jenis:string',      
    'nomor_rekening:string',      
    'nama_rekening:string',      
    'jenis_mutasi:string',      
    'nilai_mutasi:float',      
    'kode_cabang:string',      
    'kode_valuta:string',      
    'kode_tx_class:string',    
    'kode_tx:string',    
    'kode_account:string'     
  ]

  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    pass
  #--  
  
  def getValidationMsg(self, uip, list_val):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    #uip = self.FinProduct
    # generic form
    arrErrMsg = userApp.doValidation(uip, list_val)
    
    return arrErrMsg
  #--   

  def Show(self):
    self.configureForm(0)
    
    self.FormContainer.Show()

  def saveClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uip = self.uipTrx
    
    arrErrMsg = self.getValidationMsg(uip, self.arrDefValidation)
    nom_cr, nom_db = self.gridCheck()
    if abs(nom_cr - nom_db)>0.1:
      arrErrMsg.append('Transaksi Tidak Balance (%s).. total debet (%s) <> total kredit (%s)' % (abs(nom_cr - nom_db), nom_db, nom_cr))
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else :        
      form.CommitBuffer()
      params = form.GetDataPacket()

      resp = form.CallServerMethod('save', params)    
      status = resp.FirstRecord
      if status.isErr:
        app.ShowMessage('Error :' + status.errMsg)
      else:
        app.ShowMessage('Berhasil disimpan')    
        sender.ExitAction = 1
      
  def gridCheck(self):
    grid = self.listMember
    nom_cr = 0
    nom_db = 0
    grid.First()
    for i in range(grid.recordcount):
      if grid.jenis_mutasi=='C':
        nom_cr += grid.nilai_mutasi 
      else:
        nom_db += grid.nilai_mutasi 
      grid.Next()   
        
    return nom_cr, nom_db 

  def defineUIFromCode(self, kode_entri):
    pass
  #--

  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False

  def activate(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    
  def bAddOnClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uip = self.uipData
    
    #CostErrMsg = self.getValidationMsg(uip, self.arrCostValidation)
    CostErrMsg =''
    if len(CostErrMsg) > 0 :
      CosterrMsg = "\n".join(CostErrMsg)
      app.ShowMessage(CosterrMsg)
    else:
      self.IsiGridDetail()
  
  def bEditOnClick(self, sender):
    grid = self.listMember
    uip = self.uipData
    uip.Edit()
    for i in self._cols:
      n = i.split(';')
      col_name = val_name = n[0]
      if len(n) >1: val_name=n[1];
      uip.SetFieldValue(val_name,grid.GetFieldValue(col_name))
    
    uip.fMode = 'edit'
      
    self.pMember_bAdd.caption = '&Simpan'
    self.pMember_LMember.SetFocus()
    
    
  def bDelOnClick(self, sender):
    grid = self.listMember
    app = self.FormObject.ClientApplication

    dlg = app.ConfirmDialog('Yakin akan menghapus data?')
    if dlg:
      grid.Delete()
  
  def IsiGridDetail(self):
    uip = self.uipTrx
    res = self.uipData
    grid = self.listMember
    if res.fMode =='edit':
      grid.Edit()
    else:
      grid.Append()

    grid.jenis_mutasi = res.jenis_mutasi
    grid.nilai_mutasi = res.nilai_mutasi
    grid.kode_jenis   = uip.kode_jenis

    if uip.kode_jenis not in ('GL','LIA'):
      grid.nomor_rekening = res.NoRekBank
      grid.nama_Rekening  = res.GetFieldValue('LRekSukuk.nama_Rekening')
      grid.kode_tx_class  = res.GetFieldValue('GLInterface.Kode_Interface')
      grid.kode_tx        = res.GetFieldValue('GLInterface.deskripsi')
      grid.kode_account   = res.GetFieldValue('GLInterface.kode_account')
      grid.kode_cabang    = res.kode_cabang
      grid.kode_valuta    = res.kode_valuta

    if uip.kode_jenis in ('GL'):
      grid.kode_account   = res.GetFieldValue('Kode_GL.account_code')
      grid.kode_cabang    = res.GetFieldValue('kode_cabang_gl.Kode_Cabang')
      grid.kode_valuta    = res.GetFieldValue('LCurrency.Currency_Code')
      grid.nomor_rekening = "%s-%s-%s" % (grid.kode_account, grid.kode_cabang, grid.kode_valuta)
      grid.nama_Rekening  = res.GetFieldValue('Kode_GL.account_name')
    
    grid.Post()
    res.ClearData()
    self.pMember_bAdd.caption = '&Tambah'
  
  def configureForm(self, idxpage):
    form = self.FormObject;
    mpage = form.PyFormObject.multipages1

    for i in range(2):
      mpage.GetPage(i).TabVisible = 0

    if idxpage <>1:
      mpage.GetPage(1).TabVisible = 1
    else:
      mpage.GetPage(0).TabVisible = 1
    
    if idxpage==3:
      self.pSukuk_kode_instrumen.Visible=1  
    else:
      self.pSukuk_kode_instrumen.Visible=0  
  #--

  def kode_jenisOnChange(self, sender):
    idxpage = sender.ItemIndex
    self.configureForm(idxpage)    
    uip = self.uipData
    uip.ClearData()
  #--

  def LCabangOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    lsData = 'kode_cabang;nama_cabang'
    
    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupCabang", nField, lsData)
    return res


  def bCariRekeningDebetOnClick(self, sender):
    DataRekening = self.FormRekening.SearchRekeningByKodeJenis(JenisRekening = ['SAV','CA','RAB'])
    if DataRekening == None : return 0
    
    uipTran = self.uipTransaksi
    uipTran.Edit()
    uipTran.Nomor_Rekening_Debet =  DataRekening.NomorRekening
    
    self.SetRekeningDebet()


  def Kode_GLExit(self, sender):
    uip = self.uipData
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name

    res = uapp.checkStdLookup(sender, nField, '''account_code;account_name''' )      
    if res != None:
      return res
      
    res = uapp.stdLookup(sender, "facility@lookupGLAccount", nField, "account_code;account_name")
    #uip.account_name = uip.GetFieldValue(nField+'.account_name')

    return res

  def RC_Code_GL_exit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject                                 
    res = uapp.stdLookup(sender, "facility@lookupProject", 'rc_code_gl', "project_no;name", "kode_gl.account_type;rc_code_gl.project_no")
    return res

  def setRekSumber(self, cust, nField):
    uip = self.uipData
    uip.SetFieldValue(nField+'.nomor_rekening', cust['nomor_rekening'])
    uip.SetFieldValue(nField+'.nama_rekening', cust['nama_rekening'])

    uip.Nomor_Rekening_liab= cust['nomor_rekening']
    #uip.nama_rekening_lab = cust['nama_rekening']
    uip.kode_cabang_liab  = cust['kode_cabang']
    uip.Nama_Valuta_Liab  = cust['kode_valuta']
    uip.Produk_Liab       = cust['produk']
  
  def LRekeningLiabOnBeforeLookup(self, sender, linkui):
    # function(sender: TrtfDBLookupEdit; linkui: TrtfLinkUIElmtSetting): boolean
    uapp = self.FormObject.ClientApplication.UserAppObject
    uip = self.uipData
    dRekSumber = uapp.lookupRekSumber()
    #raise Exception, dRekSumber
    uip.Edit()
    if dRekSumber != None:
      self.setRekSumber(dRekSumber, sender.Name)
    #--
    return 0
  #--

  def setRekBank(self, nField):
    uip = self.uipData

    uip.NoRekBank        = uip.GetFieldValue(nField+'.nomor_rekening')
    uip.kode_cabang      = uip.GetFieldValue(nField+'.kode_cabang')
    uip.kode_valuta      = uip.GetFieldValue(nField+'.kode_valuta')
    uip.bank_produk      = uip.GetFieldValue(nField+'.kode_produk')
    uip.bank_counterpart = uip.GetFieldValue(nField+'.kode_counterpart')
    if self.uipTrx.kode_jenis=='TRB':
      uip.kode_instrumen = uip.GetFieldValue(nField+'.kode_instrumen')
  
  def LRekBankOnBeforeLookup(self, sender, linkui):
    # function(sender: TrtfDBLookupEdit; linkui: TrtfLinkUIElmtSetting): boolean
    uapp = self.FormObject.ClientApplication.UserAppObject
    dRekSumber = uapp.lookupRekBank()
    if dRekSumber != None:
      self.setRekBank(dRekSumber, sender.Name)
    #--
    return 0
  #--
  
  def LRekTreasuryOnExit(self, sender):
    uipTrx = self.uipTrx
    uip = self.uipData
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'nomor_rekening;nama_rekening;kode_cabang;kode_valuta;kode_produk;kode_counterpart'
    if uipTrx.kode_jenis=='TRB':
      lsData = lsData+';kode_instrumen'

    res = uapp.checkStdLookup(sender, nField, lsData) 
    if res != None:
      return res

    res = uapp.stdLookup(sender, "facility@lookupTreaAccount", nField, lsData, 
      None,
      {
        'kode_jenis':uipTrx.kode_jenis
      })
    self.setRekBank(nField)

    return res


  def GLInterfaceOnExit(self, sender):
    uip = self.uipData
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_interface;deskripsi;kode_produk;kode_account'

    res = uapp.checkStdLookup(sender, nField, lsData) 
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupGLInterface", nField, lsData, 
      None,
      {
        'kode_produk':uip.bank_produk
      })

    return res


  def LValutaOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'Currency_Code;Description'

    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res
      
    res = uapp.stdLookup(sender, "parameter@lookupCurrency", nField, lsData)

    return res


  def BrowseClick(self,sender):
    app = self.FormObject.ClientApplication

    fileName = app.OpenFileDialog("Open file..", "Excel Files(*.xls;*.xlsx)|*.xls;*.xlsx")

    if fileName not in [None, ""]:
      self.uipTrx.Edit()
      self.uipTrx.import_data = fileName

  def UploadClick(self,sender):
    formObj = self.FormObject
    app = formObj.ClientApplication
    
    uipTrxSession = self.uipTrx
    uipTrxSession.Edit()
    uipTrxSession._cols = str(self._cols)

    filename=uipTrxSession.import_data
    
    if filename in (None,0,'') :
      app.ShowMessage('File tidak ada...')
      return
    
    formObj.CommitBuffer()

    ph = formObj.GetDataPacket()
    sw = ph.Packet.AddStreamWrapper()
    sw.LoadFromFile(filename)
    
    #resp = formObj.CallServerMethod('UploadData',ph)
    resp = app.ExecuteScript('lib_importdata.UploadData',ph)
    
    status = resp.FirstRecord
    if status.Is_Err : 
      app.ShowMessage(status.Err_Message)
      return
    
    grid = self.listMember
    grid.ClearData()
    upl_data = resp.Packet.upl_data
    for i in range(upl_data.recordcount):
      rec = upl_data.GetRecord(i)
      grid.Append()
      #grid.nomor = rec.nomor
      for _col in self._cols:
        dt = _col.split(':')[0]
        grid.SetFieldValue(dt, rec.GetFieldByName(dt))
              
    uipTrxSession.Edit()
    uipTrxSession.import_data = ''
    #uipTrxSession.status_upload = status.sucsess
    
    return
