import sys
class fViewAccount:

  def __init__(self, formObj, parentForm):
    self.nomor_rekening = ''
    self.app = formObj.ClientApplication
  #--
  
  def SetNomorRekening(self, norek) :
    self.nomor_rekening = norek
    params = self.app.CreateValues(['nomor_rekening',norek])
    self.FormObject.SetDataWithParameters(params)
    
  def FormAfterProcessServerData(self, formobj, operationid, datapacket):
    # function(formobj: TrtfForm; operationid: integer; datapacket: TPClassUIDataPacket):boolean
    app = formobj.ClientApplication
    uipFin = self.uipTreasury
    self.nomor_rekening = uipFin.nomor_rekening
    
    self.FormObject.Caption = uipFin.caption
    if uipFin.label_jt<>None:
      self.pForm_label_jt.visible=1
      self.pForm_label_jt.Caption=uipFin.label_jt
    
    #setFormView
    self.SetFormView()
  #--

  def bRefreshClick(self, sender):
    formObj = self.FormObject; app = formObj.ClientApplication; uapp = app.UserAppObject
    # procedure(sender: TrtfButton)
    if (self.uipFilter.filter_transaction_class or "F") == "T":
      kode_tx_class = self.uipFilter.GetFieldValue('LDetilTransaksiClass.kode_tx_class') or ""
    else:
      kode_tx_class = ""
    if sender.Name == 'bShowToday':
      param = app.CreateValues(
        ['nomor_rekening', self.nomor_rekening], 
        ['detail_level', self.uipFilter.detail_level or "D"],
        ['kode_tx_class', kode_tx_class],
        ['src', 'today']
      )
      q = self.qToday
    elif sender.Name == 'bShowHistory':
      param = app.CreateValues(
        ['nomor_rekening', self.nomor_rekening], 
        ['detail_level', self.uipFilter.detail_level or "D"],
        ['kode_tx_class', kode_tx_class],
        ['src', 'history'],
        ['start_date', uapp.stdDate(self.uipFilterHistory.startDate)],
        ['end_date', uapp.stdDate(self.uipFilterHistory.endDate)]
      )
      q = self.qHistory
        
    #ph = self.FormObject.CallServerMethod('getQueryData', param)
    ph = app.ExecuteScript('rpt_jadwal.getQueryData',param)
    dsStatus = ph.Packet.status
    rec = dsStatus.GetRecord(0)
    if rec.error_status:
      formObj.ShowMessage(rec.error_message)
    else:

      q.SetDirectResponse(ph.Packet)

  def LDetilTransaksiClassOnExit(self, sender):
    # procedure(sender: TrtfDBEdit)
    uapp = self.FormObject.ClientApplication.UserAppObject 
    res = uapp.stdLookup(sender, "detil_tx_class@lookup", "LDetilTransaksiClass", "kode_tx_class;deskripsi", "kode_jenis")
    return res
 
  def filter_transaction_classOnClick(self, sender):
    # procedure(sender: TrtfDBCheckBox)
    self.pInput_LDetilTransaksiClass.Visible = sender.Checked
    pass

  def printXLSOnClick2(self, sender):
    self.FormObject.ShowMessage('Under development')
    return

  def printXLSOnClick(self, sender):
    #self.FormObject.ShowMessage('Under development')
    formObj = self.FormObject; app = formObj.ClientApplication; uapp = app.UserAppObject
    # procedure(sender: TrtfButton)
    if (self.uipFilter.filter_transaction_class or "F") == "T":
      kode_tx_class = self.uipFilter.GetFieldValue('LDetilTransaksiClass.kode_tx_class') or ""
    else:
      kode_tx_class = ""
    if sender.Name == 'bPrintToday':
      param = app.CreateValues(
        ['nomor_rekening', self.nomor_rekening], 
        ['detail_level', self.uipFilter.detail_level or "D"],
        ['kode_tx_class', kode_tx_class],
        ['src', 'today']
      )
    else:
      param = app.CreateValues(
        ['nomor_rekening', self.nomor_rekening], 
        ['detail_level', self.uipFilter.detail_level or "D"],
        ['kode_tx_class', kode_tx_class],
        ['src', 'history'],
        ['start_date', uapp.stdDate(self.uipFilterHistory.startDate)],
        ['end_date', uapp.stdDate(self.uipFilterHistory.endDate)]
      )
    #resp = self.FormObject.CallServerMethod('PrintXLS', param)        
    resp = app.ExecuteScript('rpt_jadwal.PrintXLS',param)

    status = resp.FirstRecord
    if status.IsErr:
      app.ShowMessage(status.ErrMessage)
      return
      
    oPrint = self.FormObject.ClientApplication.GetClientClass('PrintLib_2','PrintLib')()
    oPrint.doProcessByStreamName('gen_report',self.app,resp.packet,3)


  def ExportExcelOnClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    form.CommitBuffer()
    
    ph = app.CreateValues(
        ['nomor_rekening', self.uipTreasury.nomor_rekening],
        ['akad', self.uipTreasury.GetFieldValue('LContractType.contracttype_code')]
     )       
    resp = app.ExecuteScript('rpt_jadwal.ExportAngsuran',ph)

    status = resp.FirstRecord
    if status.IsErr:
      app.ShowMessage(status.ErrMessage)
      return
      
    oPrint = self.FormObject.ClientApplication.GetClientClass('PrintLib_2','PrintLib')()
    oPrint.doProcessByStreamName('gen_report',self.app,resp.packet,3)
  #--
  
  def qDataOnDoubleClick(self, sender):
    journal_no =  self.qToday.GetFieldValue('journal_no')
    self.qJournalDetil(journal_no)

  def qHistOnDoubleClick(self, sender):
    journal_no =  self.qHistory.GetFieldValue('journal_no')
    self.qJournalDetil(journal_no)

  def qJournalDetil(self, journal_no):
    app = self.app
    app.SetLocalResourceMode(1)
    formid = 'core://accounting/fInfoJournal'
    #journal_no =  self.qToday.GetFieldValue('journal_no')
    f = app.CreateForm(formid, formid, 0,
     app.CreateValues(['journal_no', journal_no]), ['context', []], "", 1)
    f.ShowJournalInfo()


  def bSaveOnClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uipHelper = self.uipHelper
    
    #set data untuk pengecekan
    uipTreasury = self.uipTreasury
    uipTreasury.Edit()
    
    arrErrMsg = self.getValidationMsg()
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else :
      dlg = app.ConfirmDialog('Yakin Memproses Data..?')   
      if dlg:
        # ambil dari frame
        formAkad = self.frameAkad.ContainedFormObject
        formAkad.CommitBuffer()
        fp = formAkad.GetDataPacket()
                
        form.CommitBuffer()
        params = form.GetDataPacket()
        
        params.Packet.AcquireAnotherPacket(fp.Packet)
        resp = form.CallServerMethod('save', params)    
        status = resp.FirstRecord
        if status.isErr:
          app.ShowMessage('Error :' + status.errMsg)
        else:
          app.ShowMessage('%s berhasil dibuat. Nomor rekening = %s\r\nEfektif setelah otorisasi' % (uipTreasury.caption, uipTreasury.nomor_rekening))    
          sender.ExitAction = 1
        #end else
  #--
  
  def getValidationMsg(self):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    uip = self.uipTreasury
    # generic form
    arrValidation = []      
    arrErrMsg = userApp.doValidation(uip, arrValidation)
    
    return arrErrMsg

  def defineUIFromCode(self, kode_entri):
    pass
  #--

  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False
  #--

  def activate(self):
    pass
  #--
  
  def SetFormView(self):
    uipTrea = self.uipTreasury
     
    if uipTrea.act_type in ['C', 'M']:
      self.pAction_bSave.visible = True
    
  

