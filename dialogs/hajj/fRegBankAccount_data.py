import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.net.message as message
import sys
import com.ihsan.util.modman as modman
import rpdb2
#rpdb2.start_embedded_debugger('000', True, True)

modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

class record: pass
                                                                           
def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipHelper;uipTreasury"): return
  uideflist.PrepareReturnDataset()
  rec = uideflist.uipTreasury.Dataset.AddRecord()    
  phTgl = config.AppObject.rexecscript("core", "appinterface/coreInfo.getAccountingDay", config.AppObject.CreatePacket())
  tgl_transaksi = phTgl.FirstRecord.acc_date
  rec.date_now = tgl_transaksi
  rec.tgl_buka = tgl_transaksi
  rec.jenis_treasuryaccount = 'C'
  rec.kode_entri = 'TRB003'

  def_valuta = helper.GetObject('ParameterGlobalTreasury', 'DEF_VALUTA').nilai_parameter_string   
  def_valuta_desc = helper.GetObject('core.Currency', def_valuta).Description   
  def_cabang = helper.GetObject('ParameterGlobalTreasury', 'DEF_CABANG').nilai_parameter_string   
  def_cabang_desc = helper.GetObject('enterprise.Cabang', def_cabang).Nama_Cabang   

  rec.SetFieldByName('LCurrency.Currency_Code', def_valuta)
  rec.SetFieldByName('LCurrency.Description', def_valuta_desc)
  rec.SetFieldByName('LCabang.Kode_Cabang', def_cabang)
  rec.SetFieldByName('LCabang.Nama_Cabang', def_cabang_desc)
  
#-- 
def authorize(config, params, returns):
  try :
    #rpdb2.start_embedded_debugger('000', True, True)  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    rec = params.uipTreasury.GetRecord(0)
    ph = {'recParent': rec}
    oAccount = helper.CreatePObject('BankAccount', ph)
    
    # info user transaksi / otor
    dsInfo = params.__inputinfo
    recInfo = dsInfo.GetRecord(0)
    oAccount.id_otorentri = recInfo.id_otorisasi
    # process transaksi placing         
    #oAccount.Borrowing(ph)
    
    #set user input & auth
    oAccount.user_input = recInfo.user_input
    oAccount.time_input = recInfo.time_input
    oAccount.user_auth =  config.SecurityContext.initUser
    oAccount.time_auth =  config.Now() 
     
    #raise Exception, oAccount.nomor_ref_deal 
    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--  
  
def reject(config, params, returns):
  pass
    
def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000')
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  fau = helper.GetPClass('TreasuryUtils')(config)
  rec = params.uipTreasury.GetRecord(0)
  config.BeginTransaction()
  try :
    nomor_rekening = fau.getNextAccountNumber(rec.jenis_treasuryaccount)
    rec.nomor_rekening = nomor_rekening

    #raise Exception, rec.kode_entri 
    recOtor = otorisasi.prepareParameterOtorisasi(params)
    recOtor.kode_entri = rec.kode_entri
    recOtor.keterangan = "Registrasi Rekening Bank - %s" % (nomor_rekening)
    recOtor.info1 = nomor_rekening
    #recOtor.info2 = None
    recOtor.tipe_target = 0
    recOtor.id_peran = "OM"
    recOtor.nama_tabel = rec.contract_class_name
    recOtor.tipe_key = 1
    recOtor.key_string = nomor_rekening
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph) 
    
    isErr = 0
    errMsg = ""
    config.Commit()
  except :
    config.Rollback()
    isErr = 1
    errMsg = "%s.%s" % (str(sys.exc_info()[0]), str(sys.exc_info()[1]))
    nomor_rekening = ""
    #raise Exception, errMsg
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg], ['nomor_rekening', nomor_rekening])
#--  

