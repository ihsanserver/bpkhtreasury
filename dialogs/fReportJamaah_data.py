import os
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import calendar
import datetime
from datetime import datetime

def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config; mlu = config.ModLibUtils    
  q = config.CreateSQL("""
      SELECT cbg.kode_cabang, cbg.nama_cabang FROM %(listcabangdiizinkan)s cbglist, %(cabang)s cbg
      WHERE id_user = %(id_user)s AND cbglist.kode_cabang = cbg.kode_cabang
    """ % {
      'listcabangdiizinkan': config.MapDBTableName('enterprise.listcabangdiizinkan'),
      'cabang': config.MapDBTableName('enterprise.cabang'), 
      'id_user': mlu.QuotedStr(config.SecurityContext.InitUser)
    }
  ).RawResult
  
  if q.Eof:
    raise Exception, "User access denied"
  
  uideflist.PrepareReturnDataset()
  ds = uideflist.uipFilter.Dataset
  rec = ds.AddRecord()
  rec.SetFieldByName('lcabang.kode_cabang', q.kode_cabang)
  rec.SetFieldByName('lcabang.nama_cabang', q.nama_cabang)
  rec.cbAllCabang = '0'
  rec.cbAllProduk = '1'
  rec.cbAllCounterpart = '1'
  rec.cbAllValuta = '1'
  rec.status_rekening = '1'
  rec.type_report = '1'   
    
  phTgl = config.AppObject.rexecscript("core", "appinterface/coreInfo.getAccountingDay", config.AppObject.CreatePacket())
  tgl_acc = phTgl.FirstRecord.acc_date
  dt = config.ModLibUtils.DecodeDate(tgl_acc)
  ldm = calendar.monthrange(dt[0],dt[1])[1] 
  #raise Exception, dt
  rec.start_date  = config.ModLibUtils.EncodeDate(dt[0], dt[1], 1)
  rec.end_date = config.ModLibUtils.EncodeDate(dt[0], dt[1], ldm)
#---