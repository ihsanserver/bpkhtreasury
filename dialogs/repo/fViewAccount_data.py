import com.ihsan.util.otorisasi as otorisasi
import sys 
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman

modman.loadStdModules(globals(), ['libs'])

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipData"): return
  
  nomor_rekening = params.FirstRecord.nomor_rekening  
  oTreasuryAccount = helper.GetObject("TreasuryAccount", nomor_rekening)
  if oTreasuryAccount.IsNull:
    raise Exception, "Data not found. TreasuryAccount: %s" % nomor_rekening
    
  pobjconst = 'PObj:TreaReverseRepo#nomor_rekening=' + nomor_rekening
  uideflist.SetData('uipData', pobjconst)