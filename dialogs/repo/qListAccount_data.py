import os
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import com.ihsan.util.modman as modman

GRID_ACCOUNT = 'gridsettings.listaccount_cols'

def dbg(msg):
  #app.ConWriteln(msg)
  #app.ConRead(msg2)
  message.send_udp(msg + "\n", 'localhost', 9122)

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config; mlu = config.ModLibUtils
  q = config.CreateSQL("""
      SELECT cbg.kode_cabang, cbg.nama_cabang FROM %(listcabangdiizinkan)s cbglist, %(cabang)s cbg
      WHERE id_user = %(id_user)s AND cbglist.kode_cabang = cbg.kode_cabang
    """ % {
      'listcabangdiizinkan': config.MapDBTableName('enterprise.listcabangdiizinkan'),
      'cabang': config.MapDBTableName('enterprise.cabang'), 
      'id_user': mlu.QuotedStr(config.SecurityContext.InitUser)
    }
  ).RawResult
  
  if q.Eof:
    raise Exception, "User access denied"
  
  uideflist.PrepareReturnDataset()
  ds = uideflist.uipFilter.Dataset
  rec = ds.AddRecord()
  rec.SetFieldByName('lcabang.kode_cabang', q.kode_cabang)
  rec.SetFieldByName('lcabang.nama_cabang', q.nama_cabang)
  rec.cbAllCabang = '0'
  rec.status_rekening = '1'
  rec.search_field = 'N'
  rec.jenis_treasuryaccount = 'R'
#---
  
def getSQLClient(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils            
  #fr = params.FirstRecord
  param = params.FirstRecord

  #raise Exception, param.all_cabang
  lcond = []
  lcond.append( ''' a.jenis_treasuryaccount in ('%s') ''' % param.jenis_treasuryaccount)
  lcond.append( ''' ('1'='%(all_cabang)s' or b.kode_cabang = '%(kode_cabang)s') ''' % {'kode_cabang':param.kode_cabang,'all_cabang':param.all_cabang} )
  lcond.append( ''' ('0'='%(sr)s' or b.status_rekening = %(sr)s ) ''' % {'sr':param.status_rekening}) 

  if param.kode_produk <> "ALL":
    lcond.append( ''' a.kode_produk='%s' ''' % param.kode_produk)
      
  if len(param.search_word) > 0 :
    if param.search_field == 'N' :
      lcond.append('''upper(b.nama_rekening) LIKE upper('%%%s%%') ''' % param.search_word)
    elif param.search_field == 'R' :
      lcond.append( '''upper(a.nomor_rekening) LIKE upper('%%%s%%') ''' % param.search_word )
    elif param.search_field == 'C' :
      lcond.append( '''upper(d.nomor_nasabah) LIKE upper('%%%s%%') ''' % param.search_word )

  strOqlCond = ''      
  if len(lcond) > 0 :   
    strOqlCond = " AND ".join(lcond)
    strOqlCond = strOqlCond
 
  _dict = {
    'treasuryaccount':config.MapDBTableName("treasuryaccount"),
    'rekeningtransaksi':config.MapDBTableName("core.rekeningtransaksi"),
    'treaproduk':config.MapDBTableName("treaproduk"),
    'treacounterpart':config.MapDBTableName("treacounterpart"), 
    'listperanuser':config.MapDBTableName("enterprise.listperanuser"), 
    'listcabangdiizinkan':config.MapDBTableName("enterprise.listcabangdiizinkan"), 
    'id_user': mlu.QuotedStr(config.SecurityContext.InitUser),
    'strOqlCond':strOqlCond
  } 
  
  sSQL = '''
    SELECT b.tipe_akses_cabang FROM %(userapp)s b WHERE b.id_user='%(id_user)s' 
  ''' % {
    'userapp': config.MapDBTableName("enterprise.userapp"),
    'id_user':config.SecurityContext.InitUser
  }
  rSQL = config.CreateSQL(sSQL).RawResult

  _dict['LCDJoinType'] = 'LEFT JOIN' if rSQL.tipe_akses_cabang=='S' else 'INNER JOIN'

  sSQL = '''
    SELECT DISTINCT 
      a.Nomor_Rekening,
      b.NAMA_REKENING,
      -(b.saldo) as saldo, 
      case when b.status_rekening = 1 then 'AKTIF'
      when b.status_rekening in (3,99) then 'TUTUP' end as status_rekening,
      a.kode_produk, 
      a.tgl_buka,
      a.tgl_jatuh_tempo,
      extract(day from a.tgl_jatuh_tempo - a.tgl_buka) as jml_hari,
			a.nominal_deal,
			a.ujroh,
			a.eqv_rate_real,
			a.nomor_ref_deal,
			a.nilai_tunai,
			nvl(a.ppap_umum,0) ppap_umum,
      nvl(a.ppap_khusus,0) ppap_khusus,
			b.saldo+a.ujroh nominal_gross,
			cp.nama_counterpart,
			pr.nama_produk
    FROM %(treasuryaccount)s a
    	INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
			INNER JOIN %(treaproduk)s pr ON a.kode_produk=pr.kode_produk
			INNER JOIN %(treacounterpart)s cp ON a.kode_counterpart=cp.kode_counterpart

      %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = b.kode_cabang AND lc.id_user = %(id_user)s
	''' % _dict

  sql = {}
  sql['SELECTFROMClause'] = sSQL

  sql['WHEREClause'] = '''
    %(strOqlCond)s
  ''' % _dict
    
  sql['keyFieldName'] = 'b.NAMA_REKENING'
  sql['altOrderFieldNames'] = 'b.NAMA_REKENING'
  sql['baseOrderFieldNames'] = 'b.NAMA_REKENING'
  
  gsModule = modman.getModule(config, GRID_ACCOUNT)
  #col_set = gsModule.cols_setting(param.jenis_treasuryaccount)
  sql['columnSetting'] = gsModule.cols_setting(param.jenis_treasuryaccount, 'A')
  #sql['excel_cols']= EXCEL_COLUMN
  #sql['judul2']= judul2
  
  return sql
#--
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def getQueryData(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()

  sqlStat = getSQLClient(config, clientpacket)
  #raise Exception, sqlStat['SELECTFROMClause']+sqlStat['WHEREClause']
  try :
    rqsql = remotequery.RQSQL(config)
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '')
    rqsql.initOperation(returnpacket)

    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#
