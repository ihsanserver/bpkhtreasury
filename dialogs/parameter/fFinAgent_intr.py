class fFinAgent:
  
  # format : fieldname, type:str|num, acceptedCondition, errMsg
  arrDefValidation = [ ['AgentName', 'str', 'len(value) > 0', 'Nama Agent belum diinput'] 
      , ['LRekEscrow.nomor_rekening', 'str', 'len(value) > 0', 'NoRek Escrow Belum Input']
      , ['LRekDropping.nomor_rekening', 'str', 'len(value) > 0', 'NoRek Pencairan Belum Input']
      , ['agentplafond', 'num', 'value > 0', 'PLafond Belum Input']
  ]
  
  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    pass
  #--  
  
  def getValidationMsg(self, uip, list_val):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    #uip = self.FinProduct
    # generic form
    arrErrMsg = userApp.doValidation(uip, list_val)
    
    return arrErrMsg
  #--   

  def Show(self, vMode):
    uip = self.FinAgent
    uip.Edit()
    uip.jenis_aksi = vMode
    #uip.product_code = uip.kode_produk
    if vMode =='R':
      self.FormObject.Caption = 'Input Agent Pembiayaan'
      #self.pFinProduct_product_code.Enabled = True

    elif vMode =='E':
      self.FormObject.Caption = 'Edit Agent Pembiayaan'
      
    elif vMode =='V':
      self.FormObject.Caption = 'View Agent Pembiayaan'
      self.pFinProduct.SetAllControlsReadOnly()
      self.pAction_btOk.Enabled = False
      self.FormObject.PyFormObject.multipages1.GetPage(1).TabVisible = 1

    self.FormContainer.Show()

  def saveClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uip = self.FinAgent
    
    arrErrMsg = self.getValidationMsg(uip, self.arrDefValidation)
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else :        
      form.CommitBuffer()
      params = form.GetDataPacket()

      resp = form.CallServerMethod('save', params)    
      status = resp.FirstRecord
      if status.isErr:
        app.ShowMessage('Error :' + status.errMsg)
      else:
        app.ShowMessage('%s Berhasil disimpan\r\nEfektif setelah otorisasi' % self.FormObject.Caption)    
        sender.ExitAction = 1
      
  def defineUIFromCode(self, kode_entri):
    pass
  #--

  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False

  def activate(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    
  def LRekLiabilitasOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    res = uapp.checkStdLookup(sender, nField, '''nomor_rekening;nama_rekening''' )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "rekeningliabilitas@lookupAllAccount", nField, "nomor_rekening;nama_rekening")
    return res
