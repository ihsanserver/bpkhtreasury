class fParameterGlobal:
  kode_entri = ''
                                                 
  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    pass
  #--   

  def getValidationMsg(self, uip, list_val):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    #uip = self.FinProduct
    # generic form
    arrErrMsg = userApp.doValidation(uip, list_val)
    
    return arrErrMsg
  #--   
  
  def FormAfterProcessServerData(self, formobj, operationid, datapacket):
    pass


  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False

  def activate(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    
  def defineUIFromCode(self, kode_entri):
    self.configureForm()

  def Show(self, vMode):
    uip = self.uipData
    uip.Edit()
    uip.jenis_aksi = vMode
    uip.kode_parameter = uip.kodeparams
    if vMode =='R':
      self.FormObject.Caption = 'Input Parameter Global'
      #self.pData_status.Visible = False

    elif vMode =='E':
      self.FormObject.Caption = 'Edit Parameter Global'
      self.pData_kode_parameter.Enabled = False
      self.pData_deskripsi.Enabled = False
      self.pData_tipe_parameter.Enabled = False
      
    elif vMode =='V':
      self.FormObject.Caption = 'View Parameter Global'
      self.pData.SetAllControlsReadOnly()
      #self.pAction_btOk.Enabled = False
      #self.FormObject.PyFormObject.multipages1.GetPage(1).TabVisible = 1
    
    self.configureForm()
    #--
      
  def configureForm(self):
    uip = self.uipData
    if uip.tipe_parameter=='N':
      self.pData_nilai_parameter.Visible = True
    elif uip.tipe_parameter=='S':
      self.pData_nilai_parameter_string.Visible = True
    elif uip.tipe_parameter=='D':
      self.pData_nilai_parameter_tanggal.Visible = True

    self.FormContainer.Show()

  
  def bSaveOnClick(self, sender):
    # procedure(sender: TrtfButton)
    form = self.FormObject
    app = form.ClientApplication
    uip = self.uipData
    form.CommitBuffer()
    ph = form.GetDataPacket()
    
    resp = form.CallServerMethod('save', ph)    
    status = resp.FirstRecord
    
    if status.isErr:
      app.ShowMessage('Error :' + status.errMsg)
    else:
      app.ShowMessage('%s Berhasil disimpan\r\nEfektif setelah otorisasi' % self.FormObject.Caption)    
      sender.ExitAction = 1
  #--
  
  def setNilaiDefault(self):
    uip = self.uipData
    uip.nilai_parameter = None
    uip.nilai_parameter_string = None
    uip.nilai_parameter_tanggal = None

    self.pData_nilai_parameter.Visible = False
    self.pData_nilai_parameter_string.Visible = False
    self.pData_nilai_parameter_tanggal.Visible = False
  #--
 
  def tipe_parameterOnChange(self, sender):
    self.setNilaiDefault()
    if sender.ItemIndex==0:
      self.pData_nilai_parameter.Visible = True
    elif sender.ItemIndex==1:
      self.pData_nilai_parameter_string.Visible = True
    elif sender.ItemIndex==2:
      self.pData_nilai_parameter_tanggal.Visible = True
  