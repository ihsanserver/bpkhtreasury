import sys
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.modman as modman
import rpdb2

import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.net.message as message
modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipCounterpart"): return

  keyconst = params.FirstRecord.key
  #raise Exception, keyconst
  if keyconst <>'R':
    uideflist.SetData('uipCounterpart', keyconst)    
    rec = uideflist.uipCounterpart.Dataset.GetRecord(0)
    rec.kodeparams = keyconst.split('=')[-1]
    #raise Exception, keyconst
  else:
    uideflist.PrepareReturnDataset()
    rec = uideflist.uipCounterpart.Dataset.AddRecord() 
    rec.tipe_counterpart = 'A'
    rec.nilai_limit = 0
    rec.limit_used = 0
    rec.is_unlimited = 'T'
    rec.status = 'T'
     
def reject(config, params, returns):
  pass

def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000', True, True)
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  
  rec = params.uipCounterpart.GetRecord(0)
  mode = rec.jenis_aksi
  
  config.BeginTransaction()
  try :    
    recOtor = otorisasi.prepareParameterOtorisasi(params)
    if mode == 'R':
      sql = ''' select count(*) as jumlah 
                from treacounterpart 
                where kode_counterpart = '{0}' '''.format(rec.kode_counterpart)
      jumlah = config.CreateSQL(sql).RawResult.jumlah
      if jumlah > 0:
        isErr = 1
        errMsg = '''Counterpart dengan kode {0} sudah ada, silahkan isi kode lain.'''.format(rec.kode_counterpart)
        raise Exception, errMsg
        
      recOtor.kode_entri = 'TRCADD'
      recOtor.keterangan = "Entri Data Counterpart"
    else:
      recOtor.kode_entri = 'TRCEDIT'
      recOtor.keterangan = "Edit Data Counterpart"
    
    recOtor.info1 = rec.kode_counterpart
    recOtor.info2 = rec.nama_counterpart
    recOtor.tipe_target = 0
    recOtor.id_peran = "SACC"
    recOtor.nama_tabel = 'TreaCounterpart'
    recOtor.tipe_key = 1
    recOtor.key_string = rec.kode_counterpart
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph) 

    
    config.FlushUpdates() # cek generate SQL
    config.Commit()
    isErr = 0
    errMsg = ""
  except :
    config.Rollback()
    isErr = 1
    errMsg = str(sys.exc_info()[1])
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg])
#--  

## save, authorize & reject NEW
def authorize(config, params, returns):
  try:
    #rpdb2.start_embedded_debugger('000')
    #raise Exception, 'Test'  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    rec = params.FirstRecord
    className = 'TreaCounterpart'
    oAsset = helper.GetObject(className, rec.kode_counterpart)
    #ph =  {'TreaCounterpart': rec} 
    if oAsset.IsNull:
      oAsset = helper.CreatePObject(className, rec)
    else:
      oAsset.Edit(rec)
    # process other fields here 

    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--  
  
