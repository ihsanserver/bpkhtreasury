class fTreaProduct:
  #format : fieldname, type:str|num, acceptedCondition, errMsg
  arrDefValidation = [ ['kode_produk', 'str', 'len(value) > 0', 'Kode Produk Belum Diinput'] 
      , ['nama_produk', 'str', 'len(value) > 0', 'Nama Produk Belum Diinput']
      , ['LJenisTreasury.jenis_treasuryaccount', 'str', 'len(value) > 0', 'Jenis Tresury Accpunt Belum Dipilih']
      , ['tipe_treasury', 'str', 'len(value) > 0', 'Tipe Treasury Belum Dipilih'] 
      , ['tipe_counterpart', 'str', 'len(value) > 0', 'Tipe Counterpart Belum Dipilih'] 
      #, ['glupload', 'str', 'len(value) > 0', 'GL Interface Belum Diupload'] 
  ]  
  saveValidation = [ ['glupload', 'str', 'len(value) > 0', 'GL Interface Belum Diupload'] ]
  dRef = {
     'FASBIS':'J_INSTRU_1'
     ,'DEPOSITO':'J_INSTRU_2'
     ,'SURAT BERHARGA':'J_INSTRU_3'
  }
  
  
  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    pass
  #--  
  
  def getValidationMsg(self, uip, list_val):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    arrErrMsg = userApp.doValidation(uip, list_val)
    
    return arrErrMsg
  #--   

  def Show(self, vMode):
    uip = self.uipProduct
    uip.Edit()
    uip.jenis_aksi = vMode
    uip.kode_produk = uip.kodeparams
    if vMode =='R':
      self.FormObject.Caption = 'Register Data Produk'
      self.pTreaProduct_kode_produk.Enabled = True

    elif vMode =='E':
      self.FormObject.Caption = 'Edit Data Produk'
      self.pTreaProduct_LJenisTreasury.Enabled = False
      
    elif vMode =='V':
      self.FormObject.Caption = 'View Data Produk'
      self.pTreaProduct.SetAllControlsReadOnly()
      self.pUpload.Visible=False
      self.pAction_btOk.Enabled = False
      #self.FormObject.PyFormObject.multipages1.GetPage(1).TabVisible = 1

    if uip.GetFieldValue('LJenisTreasury.kode_jenis')=='TRF':
      self.pTreaProduct_tipe_fasbis.Visible = True

    self.FormContainer.Show()

  def saveClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uip = self.uipProduct
    
    InputValidation = self.arrDefValidation+self.saveValidation 
    arrErrMsg = self.getValidationMsg(uip, InputValidation)
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else :        
      form.CommitBuffer()
      params = form.GetDataPacket()

      resp = form.CallServerMethod('save', params)    
      status = resp.FirstRecord
      if status.isErr:
        app.ShowMessage('Error :' + status.errMsg)
      else:
        app.ShowMessage('%s Berhasil Disimpan. Efektif setelah otorisasi' % self.FormObject.Caption)    
        sender.ExitAction = 1
      
  def defineUIFromCode(self, kode_entri):
    pass
  #--

  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False

  def activate(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    
  def button1OnClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    form.CommitBuffer()
    
    uipP = self.uipProduct
    arrErrMsg = self.getValidationMsg(uipP, self.arrDefValidation)
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else :        
      ph = app.CreateValues(\
        ['kode_produk', uipP.kode_produk],
        ['nama_produk', uipP.nama_produk],
        ['tipe_treasury', uipP.tipe_treasury or 'P'],
        ['kode_jenis', uipP.GetFieldValue("LJenisTreasury.kode_jenis")]
      )
  
      resp = form.CallServerMethod('GenerateFile',ph)
      status = resp.FirstRecord
      if status.Is_Err:
        app.ShowMessage(status.Err_Message)
        return
        
      oPrint = self.FormObject.ClientApplication.GetClientClass('PrintLib_2','PrintLib')()
      oPrint.doProcessByStreamName('gen_template',app,resp.packet,3)
  #--
    
  def BrowseClick(self,sender):
    app = self.FormObject.ClientApplication

    fileName = app.OpenFileDialog("Open file..", "Excel Files(*.xls;*.xlsx)|*.xls;*.xlsx")

    if fileName not in [None, ""]:
      self.uipData.Edit()
      self.uipData.tmp_file = fileName

  def UploadClick(self,sender):
    formObj = self.FormObject
    app = formObj.ClientApplication
    
    uipTrxSession = self.uipData

    filename=uipTrxSession.tmp_file
    
    if filename in (None,0,'') :
      app.ShowMessage('File tidak ada...')
      return
    
    formObj.CommitBuffer()

    ph = formObj.GetDataPacket()
    sw = ph.Packet.AddStreamWrapper()
    sw.LoadFromFile(filename)
    
    resp = formObj.CallServerMethod('UploadData',ph)
    
    status = resp.FirstRecord
    if status.Is_Err : 
      app.ShowMessage(status.Err_Message)
      return
    
    self.uipGrid.ClearData()
    glinterface = resp.Packet.glinterface
    for i in range(glinterface.recordcount):
      rec = glinterface.GetRecord(i)
      self.uipGrid.Append()
      self.uipGrid.nomor = rec.nomor
      self.uipGrid.kode_tx_class = rec.kode_tx_class
      self.uipGrid.acc_type = rec.acc_type
      self.uipGrid.batasan_saldo = rec.batasan_saldo
      self.uipGrid.deskripsi = rec.deskripsi
      self.uipGrid.gl_account = rec.gl_account
      self.uipGrid.status_gl = rec.status_gl
      self.uipGrid.nama_account = rec.nama_account
      self.uipGrid.Id_GLInterface = rec.Id_GLInterface
        
    uipTrxSession.Edit()
    uipTrxSession.tmp_file = ''
    uip = self.uipProduct
    uip.Edit()
    uip.glupload = 'T'
    return


  def LJenisTreasuryOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    uip = self.uipProduct
    res = uapp.checkStdLookup(sender, nField, '''kode_jenis;keterangan;jenis_treasuryaccount;tipe_treasury''' )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupJenisTreasury", nField, "kode_jenis;keterangan;jenis_treasuryaccount;tipe_treasury")
    tipe_treasury=uip.GetFieldValue(nField+".tipe_treasury")
    jenis_treasuryaccount=uip.GetFieldValue(nField+".jenis_treasuryaccount")
    
    if tipe_treasury=='A':
      self.pTreaProduct.GetControlByName('tipe_treasury').Enabled = 1
      uip.tipe_treasury = 'P'
    else:
      self.pTreaProduct.GetControlByName('tipe_treasury').Enabled = 0
      uip.tipe_treasury = tipe_treasury
    
    if jenis_treasuryaccount=='F':
      self.pTreaProduct.GetControlByName('tipe_fasbis').Visible = 1
    else:
      self.pTreaProduct.GetControlByName('tipe_fasbis').Visible = 0

    return res


  def LJenisInstrumenOnExit(self, sender):
    form = sender.OwnerForm
    uip = self.uipProduct
    uapp = form.ClientApplication.UserAppObject
    jenis_treasury = uip.GetFieldValue('LJenisTreasury.kode_jenis')
    nField = sender.Name
  
    res = uapp.checkStdLookup(sender, nField, 
      '''reference_code;reference_desc;refdata_id'''
    )
    if res != None:
      return res
      
    if jenis_treasury == 'TRF':
      jenis_instrumen = 'FASBIS'
    elif jenis_treasury in ('TRD', 'TRC', 'TRH'):
      jenis_instrumen = 'DEPOSITO'
    elif jenis_treasury in ('TRA', 'TRB', 'TRS', 'TRR'):
      jenis_instrumen = 'SURAT BERHARGA'
      
    kategori = self.dRef[jenis_instrumen]
    ref_lookup = "reference@lookup_refdata"

    uapp = form.ClientApplication.UserAppObject
    res = uapp.stdLookup(sender, ref_lookup, nField, 
      "reference_code;reference_desc;refdata_id", 
      None,
      {
        'reference_code':uip.GetFieldValue('%s.reference_code' % nField),
        'reference_name':'',
        'nama_kategori':kategori,
        'addCond':''
      })
      
    #self.definedRefChild(nField)      
    return res

  def LPoolOfFundOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    res = uapp.checkStdLookup(sender, nField, '''kode_pof;nama_pof''' )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "product@getPoolOfFund", nField, "kode_pof;nama_pof")

    return res