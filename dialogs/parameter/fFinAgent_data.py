import sys
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.modman as modman
import rpdb2

import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.net.message as message
modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  
  if otorisasi.loadDataOtorisasi(uideflist, params, "FinAgent"): return

  keyconst = params.FirstRecord.key
  if keyconst <>'R':
    uideflist.SetData('FinAgent', keyconst)    
    #rec = uideflist.FinAgent.Dataset.GetRecord(0)
    #raise Exception, keyconst
     
  else:
    uideflist.PrepareReturnDataset()
    rec = uideflist.FinAgent.Dataset.AddRecord()
     
def reject(config, params, returns):
  helper = pobjecthelper.PObjectHelper(config)
  recFinAgent = params.FinAgent.GetRecord(0)
  
  FinAgentId = recFinAgent.FinAgentId
  oFinAgent = helper.GetObject("FinAgent", {'FinAgentId': FinAgentId})
  oFinAgent.status_update = 'F' 

def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000', True, True)
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  
  recFinAgent = params.FinAgent.GetRecord(0)
  config.BeginTransaction()
  try :
    FinAgentId = recFinAgent.FinAgentId
    oFinAgent = helper.GetObject("FinAgent", {'FinAgentId': FinAgentId})
    oFinAgent.status_update = 'T' 
        
    recOtor = otorisasi.prepareParameterOtorisasi(params)
    recOtor.kode_entri = 'FAG03'
    recOtor.keterangan = "Entri Data Agent, Nama %s" % (recFinAgent.AgentName)
    recOtor.info1 = str(FinAgentId)
    recOtor.info2 = None
    recOtor.tipe_target = 0
    recOtor.id_peran = "OM"
    recOtor.nama_tabel = 'FinAgent'
    recOtor.tipe_key = 1
    recOtor.key_string = recFinAgent.AgentName
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph) 

    
    config.FlushUpdates() # cek generate SQL
    config.Commit()
    isErr = 0
    errMsg = ""
  except :
    config.Rollback()
    isErr = 1
    errMsg = str(sys.exc_info()[1])
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg])
#--  

## save, authorize & reject NEW
def authorize(config, params, returns):
  try:
    #rpdb2.start_embedded_debugger('000')  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    recFinAgent = params.FinAgent.GetRecord(0)
    className = 'FinAgent'
    oAsset = helper.GetObject(className, recFinAgent.FinAgentId)
    ph =  {'FinAgent': recFinAgent} 
    if oAsset.IsNull:
      oAsset = helper.CreatePObject(className, ph )
    else:
      oAsset.Edit(ph)
    # process other fields here 

    oAsset.status_update = 'F'
    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--  
  
