class fqTreaProduct:
  def __init__(self, formObj, parentForm):
    pass
  #--
  def Show(self):
    self.FormContainer.Show()

  def bDisplayOnClick(self, sender):
    formObj = self.FormObject; app = formObj.ClientApplication; userapp = app.UserAppObject
    uip = self.uipFilter
    ph = app.CreateValues(
      ['search_word', uip.search_word or ""],
      ['search_field', uip.search_field or ""]
    )
    resp = formObj.CallServerMethod("getQueryData", ph)
    
    rec = resp.FirstRecord
    if rec.error_status:
      app.ShowMessage(rec.error_message)
    else:
      self.qData.SetDirectResponse(resp.Packet)
  

  def button2OnClick(self, sender):
    formObj = self.FormObject; app = formObj.ClientApplication; userapp = app.UserAppObject
    uip = self.uipFilter
    ph = app.CreateValues(
      ['search_word', uip.search_word or ""],
      ['search_field', uip.search_field or ""],
    )
    resp = formObj.CallServerMethod("GenerateFile", ph)
    
    rec = resp.FirstRecord
    if rec.Is_Err:
      app.ShowMessage(rec.Err_Message)
    else:
      oPrint = self.FormObject.ClientApplication.GetClientClass('PrintLib_2','PrintLib')()
      oPrint.doProcessByStreamName('gen_report',app,resp.packet,3)