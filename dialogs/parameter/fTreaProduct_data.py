import sys
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.modman as modman
import rpdb2
import os, pyFlexcel
import xlrd
import types, re

import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.net.message as message
modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipProduct;uipData;uipGrid"): return

  keyconst = params.FirstRecord.key
  #raise Exception, keyconst                
  if keyconst <>'R':
  
    uideflist.SetData('uipProduct', keyconst)    
    rec = uideflist.uipProduct.Dataset.GetRecord(0)
    rec.kodeparams = keyconst.split('=')[-1]
    rec.jnstreasury = rec.GetFieldByName('LJenisTreasury.tipe_treasury')
    #raise Exception, 
     
    # glinterface
    ls_gl=['kode_tx_class','acc_type','deskripsi','gl_account','nama_account','status_gl','batasan_saldo','Id_GLInterface']
    sSQL = '''
      SELECT  
        a.KODE_TX_CLASS, a.ACC_TYPE, a.DESKRIPSI, b.KODE_ACCOUNT gl_account
        , b.Id_GLInterface, a.batasan_saldo, 'T' status_gl, c.account_name nama_account 
      FROM %(DETILTRANSAKSICLASS)s a
        LEFT JOIN %(GLINTERFACE)s b ON (a.KODE_TX_CLASS=b.KODE_INTERFACE AND b.KODE_PRODUK='%(KODE_PRODUK)s') 
        LEFT JOIN %(ACCOUNT)s c ON (b.KODE_ACCOUNT=c.ACCOUNT_CODE) 
      WHERE a.KODE_JENIS='%(KODE_JENIS)s' and a.acc_type<>'N'
        AND a.batasan_transaksi in ('A', '%(TIPE_TREASURY)s') 
      ORDER BY a.KODE_TX_CLASS
    ''' % {
        'DETILTRANSAKSICLASS': config.MapDBTableName('core.DETILTRANSAKSICLASS'),
        'ACCOUNT': config.MapDBTableName('core.ACCOUNT'),
        'GLINTERFACE': config.MapDBTableName('GLINTERFACE'),
        'KODE_JENIS': rec.GetFieldByName('LJenisTreasury.kode_jenis'),
        'TIPE_TREASURY': rec.tipe_treasury,
        'KODE_PRODUK': rec.kodeparams
    }
    qKol = config.CreateSQL(sSQL).RawResult
    qKol.First()
    oKol = uideflist.uipGrid.Dataset
    no = 0
    while not qKol.Eof:
      no += 1
      gKol = oKol.AddRecord()
      gKol.nomor = no
      for fl in ls_gl:
        gKol.SetFieldByName(fl,eval('qKol.'+fl)) 
    
      qKol.Next()

    if no > 1:
      rec.glupload = 'T'

  else:
    uideflist.PrepareReturnDataset()
    rec = uideflist.uipProduct.Dataset.AddRecord()
    rec.SetFieldByName("LPoolOfFund.kode_pof",'000')
    #rec.glupload = 'F'
  #rec.sheet_name='data'

     
def reject(config, params, returns):
  pass

def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000', True, True)
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  
  rec = params.uipProduct.GetRecord(0)
  mode = rec.jenis_aksi
  
  config.BeginTransaction()
  try :    
    recOtor = otorisasi.prepareParameterOtorisasi(params)
    if mode == 'R':
      sql = ''' select count(*) as jumlah 
                from treaproduk
                where kode_produk = '{0}' '''.format(rec.kode_produk)
      jumlah = config.CreateSQL(sql).RawResult.jumlah
      if jumlah > 0:
        isErr = 1
        errMsg = '''Produk dengan kode {0} sudah ada, silahkan isi kode lain.'''.format(rec.kode_produk)
        raise Exception, errMsg
        
      recOtor.kode_entri = 'TRPADD'
      recOtor.keterangan = "Entri Data Produk"
    else:
      recOtor.kode_entri = 'TRPEDIT'
      recOtor.keterangan = "Edit Data Produk"
    
    recOtor.info1 = rec.kode_produk
    recOtor.info2 = rec.nama_produk
    recOtor.tipe_target = 0
    recOtor.id_peran = "SACC"
    recOtor.nama_tabel = 'TreaProduk'
    recOtor.tipe_key = 1
    recOtor.key_string = rec.kode_produk
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph) 

    
    config.FlushUpdates() # cek generate SQL
    config.Commit()
    isErr = 0
    errMsg = ""
  except :
    config.Rollback()
    isErr = 1
    errMsg = str(sys.exc_info()[1])
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg])
#--  

## save, authorize & reject NEW
def authorize(config, params, returns):
  try:
    #rpdb2.start_embedded_debugger('000')
    #raise Exception, 'Test'  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    rec = params.uipProduct.GetRecord(0)
    GlInterface = params.uipGrid

    className = 'TreaProduk'
    oAsset = helper.GetObject(className, rec.kode_produk)
    ph =  {'Produk': rec,'recGL':GlInterface} 
    if oAsset.IsNull:
      oAsset = helper.CreatePObject(className, rec)
    else:
      oAsset.Edit(rec)
    # process other fields here
    #insert glinterface 
    oAsset.EditGLInterface(ph)
    #raise Exception, 'OK'

    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--  
  
def GenerateFile(config, params, returns):
  tipeT = {'P':'Placing','B':'Borrowing'}
  try:
    status = returns.CreateValues(
      ['sucsess',0],
      ['Is_Err',0],['Err_Message','']
    )
    
    rec = params.FirstRecord    
    app = config.AppObject
    app.ConCreate('out')
    app.ConWriteln('Inisialisasi proses generate template...')
    uploadDir = config.HomeDir + 'template\\tpl_glinterface.xls'
    oSheet = pyFlexcel.Open(uploadDir)
    oSheet.ActivateWorksheet('data')
    #oSheet = LoadTemplateFile(config, uploadDir, "TPL_IuranBiayaDaftar.xls")
    
    oSheet.SetCellValue(3,3, rec.kode_produk)
    oSheet.SetCellValue(4,3, rec.nama_produk)
    oSheet.SetCellValue(5,3, tipeT[rec.tipe_treasury])
    
    sSQL = '''
      SELECT  
      a.KODE_TX_CLASS,a.ACC_TYPE,a.DESKRIPSI,b.KODE_ACCOUNT,nvl(b.Id_GLInterface,0) as id_gl, a.batasan_saldo 
      FROM %(DETILTRANSAKSICLASS)s a
      LEFT JOIN %(GLINTERFACE)s b ON (a.KODE_TX_CLASS=b.KODE_INTERFACE AND b.KODE_PRODUK='%(KODE_PRODUK)s') 
      WHERE a.KODE_JENIS='%(KODE_JENIS)s' and a.acc_type<>'N'
        AND a.batasan_transaksi in ('A', '%(TIPE_TREASURY)s') 
      ORDER BY a.KODE_TX_CLASS
    ''' % {
        'DETILTRANSAKSICLASS': config.MapDBTableName('core.DETILTRANSAKSICLASS'),
        'GLINTERFACE': config.MapDBTableName('GLINTERFACE'),
        'KODE_JENIS': rec.kode_jenis,
        'TIPE_TREASURY': rec.tipe_treasury,
        'KODE_PRODUK': rec.kode_produk
    }
    rSQL = config.CreateSQL(sSQL).RawResult
    rSQL.First()
    
    i = 0
    _startLine = 7
    while not rSQL.Eof:
      i = i+1
      oSheet.SetCellValue(_startLine,1,i)
      oSheet.SetCellValue(_startLine,2,rSQL.kode_tx_class)
      oSheet.SetCellValue(_startLine,3,rSQL.acc_type)
      oSheet.SetCellValue(_startLine,4,rSQL.batasan_saldo)
      oSheet.SetCellValue(_startLine,5,rSQL.deskripsi)
      oSheet.SetCellValue(_startLine,6,rSQL.KODE_ACCOUNT)

      oSheet.SetCellValue(_startLine,40,rSQL.id_gl)
      _startLine=_startLine+1
      
      rSQL.Next()
    #--endwhile
    sBaseFileName = 'glinterface_%s.xls' % rec.kode_produk

    sFileName = config.UserHomeDirectory + sBaseFileName
    if os.access(sFileName, os.F_OK) == 1:
      os.remove(sFileName)
    #--endif
    oSheet.SaveAs(sFileName)
  
    #return packet
    sw = returns.AddStreamWrapper()
    sw.Name = 'gen_template'
    sw.LoadFromFile(sFileName)
    sw.MIMEType = "application/vnd.ms-excel"
      
    #--endelse
  except:
    status.Is_Err = True
    status.Err_Message = str(sys.exc_info()[1])
    
def ValidateDataString(data, jenis=0):
  dict_none = ['n/a','N/A','NULL','']
  sValidValue = ''
  if data in dict_none:
    data = None

  if data!=None:
    try:
      sValidValue = data
      if type(data) == types.IntType or type(data) == types.FloatType:
        sValidValue = str(int(data))
    except:
      pass
      
  if jenis == 1:
    sValidValue = re.sub(r'[^\w]', '', sValidValue)
    
  return sValidValue.strip()  

def UploadData(config,params,returns):
  status = returns.CreateValues(
    ['sucsess',0],
    ['Is_Err',0],['Err_Message','']
  )

  returns.AddDataPacketStructureEx('glinterface',
      ';'.join([
          'nomor:integer',
          'kode_tx_class:string',
          'acc_type:string',
          'deskripsi:string',
          'gl_account:string',
          'nama_account:string',
          'status_gl:string',
          'batasan_saldo:string',
          'Id_GLInterface:integer']
  ))

  returns.BuildAllStructure()
  
  rec = params.uipData.GetRecord(0)
  recP = params.uipProduct.GetRecord(0)
  #fileName =rec.tmp_file.split('\\')[-1]

  if params.StreamWrapperCount > 0:
    sw = params.GetStreamWrapper(0)
  else:
    status.Is_Err = 1
    status.Err_Message = 'Download stream not found'
    return
  #--

  fileNamePath = config.HomeDir + 'template\\tmp_upl\\upl_glinterface.xls'
  sw.SaveToFile(fileNamePath)
  wb = xlrd.open_workbook(fileNamePath)
  oSheet = wb.sheet_by_name('data')
      
  _Line = 0
  _Col = 0
  _trxCount = 0
  config.BeginTransaction()
  try:
    _startLine = 6
    _checkLine = _startLine
    required_field = str(oSheet.cell_value(_checkLine,1)).strip()

    glinterface = returns.AddNewDataset('glinterface')

    num_rows = oSheet.nrows

    while required_field not in ['', None] and _checkLine < num_rows:
      _trxCount += 1      
      
      required_field = str(oSheet.cell_value(_checkLine,1)).strip()
      gl_account = str(oSheet.cell_value(_checkLine,5)).strip()
      batasan_saldo = str(oSheet.cell_value(_checkLine,3)).strip()      
      if gl_account in ['', 'None'] and batasan_saldo <> 'O':
        raise Exception, 'Kode account untuk tx_class %s tidak boleh kosong!' % (required_field)   
      else:   
        data = glinterface.AddRecord()
        data.nomor = oSheet.cell_value(_checkLine,0)
        data.kode_tx_class = required_field
        data.acc_type = ValidateDataString(oSheet.cell_value(_checkLine,2)).strip()
        data.deskripsi = ValidateDataString(oSheet.cell_value(_checkLine,4)).strip()
        data.gl_account = ValidateDataString(oSheet.cell_value(_checkLine,5)).strip()
        
        data.batasan_saldo = batasan_saldo
        data.Id_GLInterface = int(oSheet.cell_value(_checkLine,39) or 0)

        status_gl, nama_account = cekAccount(config, data)
        if status_gl=='F': raise Exception, nama_account
        data.status_gl = status_gl
        data.nama_account = nama_account
      
      _checkLine += 1      
      #--
    #--endwhile

    config.Commit()
    status.sucsess = _trxCount
  except:
    config.Rollback()
    status.Is_Err = 1
    status.Err_Message = str(sys.exc_info()[1])
    
    return
  #--
  
def cekAccount(config, rec):
  aType = {'A':'Asset','L':'Liability','I':'Income','X':'Expense','M':'Off Balancesheet','E':'Equity'}                                                    
  sSQL = '''
    SELECT ACCOUNT_CODE,ACCOUNT_NAME,ACCOUNT_TYPE, IS_DETAIL 
    FROM %(ACCOUNT)s WHERE ACCOUNT_CODE='%(kode_Account)s'
  ''' % {
        'ACCOUNT': config.MapDBTableName('core.ACCOUNT'),
        'kode_Account': rec.gl_account
  }
  q = config.CreateSQL(sSQL).RawResult
  
  err_msg ='ERROR: kode tx class %s [%s] ' % (rec.kode_tx_class, rec.deskripsi) 
  status ='F'
  if rec.batasan_saldo=='O' and rec.gl_account in [None,'']:
    status ='T'
    a_name = None
  else:
    a_name =err_msg+'account (%s) tidak ditemukan' % rec.gl_account
    if not q.Eof:
      status ='T'
      a_name =q.ACCOUNT_NAME
      if rec.acc_type <> q.ACCOUNT_TYPE:
        status ='F'
        a_name =err_msg+'harus diisi GL %s ' % (aType[rec.acc_type])
      if q.IS_DETAIL<>'T':
        status ='F'
        a_name =err_msg+'bukan account detail (SSL)'
  
  return status, a_name

