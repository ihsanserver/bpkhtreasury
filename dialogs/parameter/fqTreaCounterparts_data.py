import os
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import com.ihsan.util.modman as modman
import openpyxl
from openpyxl.cell import get_column_letter
from openpyxl.styles import Border
from openpyxl.styles import Font
from openpyxl import Workbook
import com.ihsan.fileutils as fileutils
from copy import deepcopy
from com.ihsan.lib import queryreport
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.attrutil as atutil

modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

def dbg(msg):
  #app.ConWriteln(msg)
  #app.ConRead(msg2)
  message.send_udp(msg + "\n", 'localhost', 9122)

def getSQLClient(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils            
  #fr = params.FirstRecord
  param = params.FirstRecord

  lcond = []
  lcond.append("1=1")
  if len(param.search_word) > 0 :
    if param.search_field == '1' :
      lcond.append('''upper(kode_counterpart) LIKE upper('%%%s%%') ''' % param.search_word)
    if param.search_field == '2' :
      lcond.append('''upper(tipe_counterpart) LIKE upper('%%%s%%') ''' % param.search_word)
    if param.search_field == '3' :
      lcond.append('''upper(nama_counterpart) LIKE upper('%%%s%%') ''' % param.search_word)

  strOqlCond = ''      
  if len(lcond) > 0 :   
    strOqlCond = " AND ".join(lcond)
    strOqlCond = strOqlCond
 
  _dict = {
    'strOqlCond':strOqlCond,
    'treacounterpart':config.MapDBTableName("treacounterpart")
  } 
  
  sSQL = '''
    SELECT a.*, enum_description as tipecounterpart,
    case 
      when a.status='T' then 'Aktif'
      when a.status='F' then 'Tidak Aktif'
    end status_counterpart
    FROM %(treacounterpart)s a
    LEFT JOIN enum_varchar e on e.enum_value = a.tipe_counterpart and e.enum_name = 'eTipeCounterpart'    
  	''' % _dict

  sql = {}
  sql['SELECTFROMClause'] = sSQL

  sql['WHEREClause'] = '''
    %(strOqlCond)s
  ''' % _dict
  #raise Exception, sql['WHEREClause']
  
  sql['keyFieldName'] = 'a.kode_counterpart'
  sql['altOrderFieldNames'] = 'a.kode_counterpart'
  sql['baseOrderFieldNames'] = 'a.kode_counterpart'
  sql['columnSetting'] = '''
    object TColumnsWrapper
      Columns = <
        item
          Expanded = False
          Title.Caption = 'Kode Counterpart'
          FieldName = 'kode_counterpart'
          Width = 100
          Visible = true
        end
        item
          Expanded = False
          FieldName = 'tipecounterpart'
          Title.Caption = 'Tipe Counterpart'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nama_counterpart'
          Title.Caption = 'Nama Counterpart'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nilai_limit'
          Title.Caption = 'Nilai Limit'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'is_unlimited'
          Title.Caption = 'Is Unlimited'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'limit_used'
          Title.Caption = 'Limit Used'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'remark_counterpart'
          Title.Caption = 'Remark'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'status_counterpart'
          Title.Caption = 'Status'
          Width = 80
          Visible = True
        end
        >
    end
  
  ''' % _dict

  #sql['excel_cols']= EXCEL_COLUMN
  #sql['judul2']= judul2
  
  return sql
#--
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def getQueryData(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()

  sqlStat = getSQLClient(config, clientpacket)
  #raise Exception, sqlStat['SELECTFROMClause']
  try :
    rqsql = remotequery.RQSQL(config)
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '')
    rqsql.initOperation(returnpacket)
    #raise Exception, '2 tes '

    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#
def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config 
  mlu = config.ModLibUtils
  rec = uideflist.uipFilter.Dataset.AddRecord()
  rec.search_field = '1'
  #pass

def styleCell(owb, R_, C_):
  #style border
  owb.cell(row=R_,column=C_).style.borders.top.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.bottom.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.right.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.left.border_style = Border.BORDER_THIN
#--

def GenerateFile(config, params, returns):
  sqlStat = getSQLClient(config, params)
  try:
    status = returns.CreateValues(
      ['sucsess',0],
      ['Is_Err',0],['Err_Message','']
    )
    
    rec = params.FirstRecord
    oRec = atutil.GeneralObject(rec)    
    app = config.AppObject
    app.ConCreate('out')
    app.ConWriteln('Inisialisasi proses generate template...') 
     
    fileNamePath = config.HomeDir + 'template\\tpl_fasbis_int.xlsx'
  
    wb = openpyxl.load_workbook(fileNamePath)
    wsExcel = wb.get_sheet_by_name('data')
    
    order_by = sqlStat['baseOrderFieldNames'].replace(';',',')
    sSQL = sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause']+' ORDER BY '+order_by
    #app.ConWriteln(sSQL)
    #app.ConRead('')
    q = config.CreateSQL(sSQL).RawResult
    q.First()
          
    _cols = (
       ("kode_counterpart;Kode Counterpart",'S',17)
      ,("tipecounterpart;Tipe Counterpart",'S',16)
      ,("nama_counterpart;Nama Counterpart",'S',31)
      ,("nilai_limit;Nilai Limit",'F',15) 
      ,("limit_used;Limit Used",'F',15)
      ,("is_unlimited;Is Unlimited",'S',12)
      ,("remark_counterpart;Remark",'S',30) 
      ,("status_counterpart;Status",'S',10)
    )  
    
    #header
    wsExcel.cell(row=1,column=0).value = 'LIST DATA COUNTERPART'
    wsExcel.cell(row=2,column=0).value = 'TREASURY' 
    
    startLine = 4
    styleCell(wsExcel, startLine, 0)
    wsExcel.cell(row=startLine,column=0).style.font.bold = True
    wsExcel.cell(row=startLine,column=0).style.alignment.wrap_text= True
    wsExcel.cell(row=startLine,column=0).style.alignment.horizontal = "center"
    wsExcel.cell(row=startLine,column=0).style.alignment.vertical = "center"
    wsExcel.cell(row=startLine,column=0).value = 'NO.'  
    wsExcel.column_dimensions['A'].width = 6
    
    _rCol = 1
    for i in range(len(_cols)):
      ls = _cols[i][0].split(";") 
      styleCell(wsExcel, startLine, _rCol)
      wsExcel.cell(row=startLine,column=_rCol).style.font.bold = True 
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.wrap_text= True
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.horizontal = "center"
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.vertical = "center"
      wsExcel.cell(row=startLine,column=_rCol).value = '%s' % (ls[1])
      _rCol += 1
             
    _rRow = 5
    _no = 1
    while not q.Eof:   
      _rCol = 1
      styleCell(wsExcel, _rRow, 0)
      wsExcel.cell(row=_rRow,column=0).style.alignment.horizontal = "center"
      wsExcel.cell(row=_rRow,column=0).value = _no 
      for i in _cols:
        ls = i[0].split(";")
        if i[1]=='D':
         tgl = eval('q.'+ls[0])
         nVal = "%s-%s-%s" %(str(tgl[0]).zfill(4), str(tgl[1]).zfill(2), str(tgl[2]).zfill(2)) if tgl<>None else ''
        else:
         nVal = eval('q.'+ls[0])
        
        _col = get_column_letter(_rCol+1)
        styleCell(wsExcel, _rRow, _rCol)
        wsExcel.cell(row=_rRow,column=_rCol).value = nVal 
        wsExcel.column_dimensions[_col].width = i[2]
        
        if i[1]=='F':
          wsExcel.cell(row=_rRow,column=_rCol).style.number_format.format_code = '#,##0.00�'
        else:
          wsExcel.cell(row=_rRow,column=_rCol).style.alignment.horizontal = "center"
            
        _rCol += 1
      
      _no += 1  
      _rRow += 1        
      q.Next()
          
    
    fileHasil = config.UserHomeDirectory + 'reportfasbis.xlsx'    
    fileutils.SafeDeleteFile(fileHasil) #Melakukan Hapus File yang sudah ada sebelumnya
    wb.save(fileHasil)    
  
    #return packet
    mmtype = fileNamePath.split(".")[-1]
    sw = returns.AddStreamWrapper()
    sw.name = 'gen_report'
    sw.LoadFromFile(fileHasil)
    if mmtype=='xlsx':
      sw.MIMEType = ".%s" % mmtype
    else:
     sw.MIMEType = config.AppObject.GetMIMETypeFromExtension(fileNamePath)
      
    #--endelse
  except:
    status.Is_Err = True
    status.Err_Message = _dbg_excmsg()#str(sys.exc_info()[1])
