import os
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message

def dbg(msg):
  #app.ConWriteln(msg)
  #app.ConRead(msg2)
  message.send_udp(msg + "\n", 'localhost', 9122)

def getSQLClient(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils            
  #fr = params.FirstRecord
  param = params.FirstRecord

  lcond = []
  lcond.append("1=1")
  if len(param.search_word) > 0 :
    if param.search_field == 'N' :
      lcond.append('''upper(agentname) LIKE upper('%%%s%%') ''' % param.search_word)

  strOqlCond = ''      
  if len(lcond) > 0 :   
    strOqlCond = " AND ".join(lcond)
    strOqlCond = strOqlCond
 
  _dict = {
    'strOqlCond':strOqlCond,
    'finagent':config.MapDBTableName("financing.finagent")
  } 
  
  sSQL = '''
    SELECT 
    	a.*
    FROM %(finagent)s a
  	''' % _dict

  sql = {}
  sql['SELECTFROMClause'] = sSQL

  sql['WHEREClause'] = '''
    %(strOqlCond)s
  ''' % _dict
  #raise Exception, sql['WHEREClause']
  
  sql['keyFieldName'] = 'a.agentname'
  sql['altOrderFieldNames'] = 'a.agentname'
  sql['baseOrderFieldNames'] = 'a.agentname'
  sql['columnSetting'] = '''
    object TColumnsWrapper
      Columns = <
        item
          Expanded = False
          FieldName = 'finagentid'
          Width = 60
          Visible = FALSE
        end
        item
          Expanded = False
          FieldName = 'agentname'
          Title.Caption = 'Nama Agent'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'agentaddress'
          Title.Caption = 'Alamat Agent'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RekEscrow'
          Title.Caption = 'NoRek Escrow'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RekDropping'
          Title.Caption = 'NoRek Pencairan'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'agentplafond'
          Title.Caption = 'Plafond'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'agentusedplafond'
          Title.Caption = 'Plafond Telah Digunakan'
          Width = 90
          Visible = True
        end
        >
    end
  
  ''' % _dict

  #sql['excel_cols']= EXCEL_COLUMN
  #sql['judul2']= judul2
  
  return sql
#--
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def getQueryData(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()

  sqlStat = getSQLClient(config, clientpacket)
  #raise Exception, sqlStat['SELECTFROMClause']
  try :
    rqsql = remotequery.RQSQL(config)
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '')
    rqsql.initOperation(returnpacket)
    #raise Exception, '2 tes '

    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#
def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config 
  mlu = config.ModLibUtils
  rec = uideflist.uipFilter.Dataset.AddRecord()
  rec.search_field = 'N'
  #pass
  
