import sys
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.modman as modman
import rpdb2

import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.net.message as message
modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  mlu = config.ModLibUtils
                                   
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipData"): return
  
  if params.FirstRecord.vMode<>'R':
    keyconst = params.FirstRecord.key
    uideflist.SetData('uipData', keyconst)
    rec = uideflist.uipData.Dataset.GetRecord(0)
    rec.kodeparams = keyconst.split('=')[-1]
    #raise Exception, keyconst
  else:
    uideflist.PrepareReturnDataset()
    rec = uideflist.uipData.Dataset.AddRecord()
    rec.tipe_parameter = 'N'
     
def reject(config, params, returns):
  helper = pobjecthelper.PObjectHelper(config)
  rec = params.uipData.GetRecord(0)
  #---
  
def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000', True, True)
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  
  rec = params.uipData.GetRecord(0)
  config.BeginTransaction()
  try :
    recOtor = otorisasi.prepareParameterOtorisasi(params)
    if rec.jenis_aksi == 'R':
      sql = ''' select count(*) as jumlah 
                from ParameterGlobalTreasury 
                where kode_parameter = '{0}' '''.format(rec.kode_parameter)
      jumlah = config.CreateSQL(sql).RawResult.jumlah
      if jumlah > 0:
        isErr = 1
        errMsg = '''Parameter dengan kode {0} sudah ada, silahkan isi kode lain.'''.format(rec.kode_parameter)
        raise Exception, errMsg
      recOtor.kode_entri = 'TRPGADD'
      recOtor.keterangan = "Entri Parameter Global, kode_parameter %s" % (rec.kode_parameter)
    else:
      recOtor.kode_entri = 'TRPGEDIT'
      recOtor.keterangan = "Edit Parameter Global, kode_parameter %s" % (rec.kode_parameter)      
    
    recOtor.info1 = rec.deskripsi
    recOtor.info2 = None
    recOtor.tipe_target = 0
    recOtor.id_peran = "SACC"
    recOtor.nama_tabel = 'ParameterGlobalTreasury'
    recOtor.tipe_key = 1
    recOtor.key_string = rec.kode_parameter
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph) 

    
    config.FlushUpdates() # cek generate SQL
    config.Commit()
    isErr = 0
    errMsg = ""
  except :
    config.Rollback()
    isErr = 1
    errMsg = str(sys.exc_info()[1])
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg])
#--  

## save, authorize & reject NEW
def authorize(config, params, returns):
  try:
    #rpdb2.start_embedded_debugger('000')  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    rec = params.uipData.GetRecord(0)
    className = 'ParameterGlobalTreasury'
    oAsset = helper.GetObject(className, {'kode_parameter':rec.kode_parameter})
    ph =  {'datapaket': rec} 
    if oAsset.IsNull:
      oAsset = helper.CreatePObject(className, ph )
    else:
      oAsset.Edit(ph)
    # process other fields here 

    #oAsset.status_update = 'F'
    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#-- 