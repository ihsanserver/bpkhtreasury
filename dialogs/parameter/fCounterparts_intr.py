class fCounterparts:
  #format : fieldname, type:str|num, acceptedCondition, errMsg
  arrDefValidation = [ ['kode_counterpart', 'str', 'len(value) > 0', 'Kode Counterpart Belum Diisi'] 
      , ['nama_counterpart', 'str', 'len(value) > 0', 'Nama Counter Belum Diinput']
      , ['tipe_counterpart', 'str', 'len(value) > 0', 'Tipe Counter Belum Dipilih']
      #, ['agentplafond', 'num', 'value > 0', 'PLafond Belum Input']
  ]
  
  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    pass
  #--  
  
  def getValidationMsg(self, uip, list_val):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    #uip = self.FinProduct
    # generic form
    arrErrMsg = userApp.doValidation(uip, list_val)
    
    return arrErrMsg
  #--   

  def Show(self, vMode):
    uip = self.uipCounterpart
    uip.Edit()
    uip.jenis_aksi = vMode
    uip.kode_counterpart = uip.kodeparams
    if vMode =='R':
      self.FormObject.Caption = 'Register Data Counterpart'
      self.pTreaCounterpart_kode_counterpart.Enabled = True

    elif vMode =='E':
      self.FormObject.Caption = 'Edit Data Counterpart'
      self.pTreaCounterpart_kode_counterpart.Enabled = False
      
      
    elif vMode =='V':
      self.FormObject.Caption = 'View Data Counterpart'
      self.pTreaCounterpart.SetAllControlsReadOnly()
      self.pAction_btOk.Enabled = False
      #self.FormObject.PyFormObject.multipages1.GetPage(1).TabVisible = 1

    self.FormContainer.Show()

  def saveClick(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uip = self.uipCounterpart
    
    arrErrMsg = self.getValidationMsg(uip, self.arrDefValidation)
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else :        
      form.CommitBuffer()
      params = form.GetDataPacket()

      resp = form.CallServerMethod('save', params)    
      status = resp.FirstRecord
      if status.isErr:
        app.ShowMessage('Error :' + status.errMsg)
      else:
        app.ShowMessage('%s Berhasil disimpan\r\nEfektif setelah otorisasi' % self.FormObject.Caption)    
        sender.ExitAction = 1
      
  def defineUIFromCode(self, kode_entri):
    pass
  #--

  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False

  def activate(self):
    formObj = self.FormObject; app = formObj.ClientApplication
