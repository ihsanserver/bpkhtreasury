import os
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message

def dbg(msg):
  #app.ConWriteln(msg)
  #app.ConRead(msg2)
  message.send_udp(msg + "\n", 'localhost', 9122)

def getSQLClient(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils            
  #fr = params.FirstRecord
  param = params.FirstRecord

  lcond = []
  lcond.append("nvl(a.status,'T')='T'")
  if len(param.search_word) > 0 :
    if param.search_field == '1' :
      lcond.append('''upper(kode_parameter) LIKE upper('%%%s%%') ''' % param.search_word)
    if param.search_field == '2' :
      lcond.append('''upper(tipe_parameter) LIKE upper('%%%s%%') ''' % param.search_word)
    if param.search_field == '3' :
      lcond.append('''upper(nilai_parameter) LIKE upper('%%%s%%') ''' % param.search_word)

  strOqlCond = ''      
  if len(lcond) > 0 :   
    strOqlCond = " AND ".join(lcond)
    strOqlCond = strOqlCond
 
  _dict = {
    'strOqlCond':strOqlCond,
    'ParameterGlobalTreasury':config.MapDBTableName("ParameterGlobalTreasury")
  } 
  
  sSQL = '''
    SELECT 
      a.kode_parameter, a.deskripsi
      , decode(a.tipe_parameter,'N','Number','S','String','Tanggal') tipe_parameter
      , a.nilai_parameter as nilai_number
      , a.nilai_parameter_string as nilai_string
      , a.nilai_parameter_tanggal as nilai_tanggal
    FROM %(ParameterGlobalTreasury)s a
 	''' % _dict

  sql = {}
  sql['SELECTFROMClause'] = sSQL

  sql['WHEREClause'] = '''
    %(strOqlCond)s
  ''' % _dict
  #raise Exception, sql['WHEREClause']
  
  sql['keyFieldName'] = 'a.kode_parameter'
  sql['altOrderFieldNames'] = 'a.kode_parameter'
  sql['baseOrderFieldNames'] = 'a.kode_parameter'
  sql['columnSetting'] = '''
    object TColumnsWrapper
      Columns = <
        item
          Expanded = False
          FieldName = 'kode_parameter'
          Title.Caption = 'Kode Parameter'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'deskripsi'
          Title.Caption = 'Deskripsi'
          Width = 320
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'tipe_parameter'
          Title.Caption = 'Tipe Parameter'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nilai_number'
          Title.Caption = 'Nilai Number'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nilai_string'
          Title.Caption = 'Nilai String'
          Width = 80
          Visible = True
        end                          
        item
          Expanded = False
          FieldName = 'nilai_tanggal'
          Title.Caption = 'Nilai Tanggal'
          Width = 80
          Visible = True
        end
        >
    end
  
  ''' % _dict

  #sql['excel_cols']= EXCEL_COLUMN
  #sql['judul2']= judul2
  
  return sql
#--
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def getQueryData(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()

  sqlStat = getSQLClient(config, clientpacket)
  #raise Exception, sqlStat['SELECTFROMClause']
  try :
    rqsql = remotequery.RQSQL(config)
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '')
    rqsql.initOperation(returnpacket)
    #raise Exception, '2 tes '

    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#
def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config 
  mlu = config.ModLibUtils
  #insurance_kind = params.FirstRecord.add_info
  rec = uideflist.uipFilter.Dataset.AddRecord()
  userinfo = config.SecurityContext.GetUserInfo()
  #raise Exception, userinfo
  #rec.insurance_kind = insurance_kind
  #pass
  
