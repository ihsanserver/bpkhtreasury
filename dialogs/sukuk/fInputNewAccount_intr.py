class fInputNewAccount:
  arrValidationDef = [['ujroh', 'num', 'value > 0', 'Nilai Ujroh harus lebih besar dari 0']
    , ['kolektibilitas', 'num', 'value > 0', 'Kolek Belum Dipilih']
  ]
  
  dRef = {           
     'LGolongan_Nasabah':'REF_GOLONGAN_NASABAH'
  }
    
  def __init__(self, formObj, parentForm):
    self.parentForm = parentForm
    pass
  #--
  
  def GetArrValidationDef(self):
    return self.arrValidationDef
  
  def OnEnterNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnEnterNum(sender)
  #--
  
  def OnExitNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnExitNum(sender)
  #--
  
  def ReffOnExit(self, sender):
    form = sender.OwnerForm
    uip = self.uipData
    uapp = form.ClientApplication.UserAppObject
    nField = sender.Name

    res = uapp.checkStdLookup(sender, nField, 
      '''reference_code;reference_desc;refdata_id'''
    )
    if res != None:
      return res
      
    kategori = self.dRef[nField]
    #addCond = self.getAddedCondition(nField)
    #if nField in ('Llembaga_pemeringkat','Lperingkat_debitur', 'Lhubungan_bank', 'Lstatus', 'Ljenis_operasional', 'Lnegara_lawan'):
    ref_lookup = "reference@lookup_refdata"
    uapp = form.ClientApplication.UserAppObject
    res = uapp.stdLookup(sender, ref_lookup, nField, 
      "reference_code;reference_desc;refdata_id", 
      None,
      {
        'reference_code':uip.GetFieldValue('%s.reference_code' % nField),
        'reference_name':'',
        'nama_kategori':kategori,
      })
    
    if nField in ('Llembaga_pemeringkat','Lperingkat_debitur'):  
      self.definedRefChild(nField)      
    
    return res

  def definedRefChild(self, nField):
    uip = self.uipData
    if nField in ('Llembaga_pemeringkat'): 
      if uip.GetFieldValue(nField+'.reference_code')<>'00':
        self.panel1_Lperingkat_debitur.enabled=1
        self.panel1_rating_date.enabled=1
        self.dRef['Lperingkat_debitur'] = 'PERINGKAT_'+uip.GetFieldValue(nField+'.reference_code')
        uip.ClearLink('Lperingkat_debitur')
        self.panel1_Lperingkat_debitur.SetFocus()
      else:
        self.panel1_rating_date.enabled=0
        self.panel1_Lperingkat_debitur.enabled=0
        #uip.ClearLink('Lperingkat_debitur')
        uip.SetFieldValue('Lperingkat_debitur.refdata_id', uip.GetFieldValue('Llembaga_pemeringkat.refdata_id'))
        uip.SetFieldValue('Lperingkat_debitur.reference_code' ,uip.GetFieldValue('Llembaga_pemeringkat.reference_code'))
        uip.SetFieldValue('Lperingkat_debitur.reference_desc', uip.GetFieldValue('Llembaga_pemeringkat.reference_desc')) 

  def harga_beliOnExit(self, sender):
    # procedure(sender: TrtfDBEdit)
    self.count_nilaitunai()
    
  def count_nilaitunai(self):
    uip = self.uipData
    uipParent = self.parentForm.uipTreasury
    uip.Edit()
    uip.nilai_tunai = uipParent.nominal_deal*(uip.harga_beli/100)