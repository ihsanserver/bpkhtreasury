class fReportPlacing:
  def __init__(self, formObj, parentForm):
    self.app = formObj.ClientApplication
  #--
                                    
  def LCabangOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    res = uapp.checkStdLookup(sender, nField, '''kode_cabang;nama_cabang''' )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "ent_cabang@lookupCabang", nField, "kode_cabang;nama_cabang")
    return res
    
  def cv_parameter(self):
    formObj = self.FormObject; app = formObj.ClientApplication; userapp = app.UserAppObject
    uip = self.uipFilter
    ph = app.CreateValues(
      ['kode_cabang', uip.GetFieldValue('LCabang.kode_cabang') or ""],
      ['kode_produk', uip.GetFieldValue('LProduk.kode_produk') or "ALL"],
      ['kode_counterpart', uip.GetFieldValue('LCounterpart.kode_counterpart') or "ALL"],
      ['kode_valuta', uip.GetFieldValue('LCurrency.Currency_Code') or "ALL"],
      ['status_rekening', uip.status_rekening or "9"],         
      ['all_cabang', uip.cbAllCabang or "0"],
      ['tgl_awal', uip.start_date or 0],
      ['tgl_akhir', uip.end_date or 0],
      ['tx_awal', uip.tx_start_date or 0],
      ['tx_akhir', uip.tx_end_date or 0],
      ['tgl_acc', uip.tgl_acc or 0],
      ['type_report', uip.type_report or ""],
      ['kode_remark', uip.kode_remark or "ALL"],
      ['keyword_search', uip.keyword_search or ""],
      ['tipe_treasury', "P"],
      ['jenis_treasuryaccount', "B"],
    )
    return ph
    
  def DisplayList(self, sender):
    formObj = self.FormObject; app = formObj.ClientApplication; userapp = app.UserAppObject
    formObj.CommitBuffer()
    
    ph = self.cv_parameter()
    if ph <> 0:
      resp = app.ExecuteScript('rpt_sukukTx.getQueryData',ph)
    
    rec = resp.FirstRecord
    if rec.error_status:
      app.ShowMessage(rec.error_message)
    else:
      self.qData.SetDirectResponse(resp.Packet)
    #qFinCollateralAsset.DisplayData()
  #--

  def bCetakOnClick(self, sender):
    formObj = self.FormObject
    app = formObj.ClientApplication
    userapp = app.UserAppObject
    formObj.CommitBuffer()
    
    ph = self.cv_parameter()
    if ph <> 0:
      resp = app.ExecuteScript('rpt_sukukTx.GenerateFile',ph)
      
      status = resp.FirstRecord
      if status.Is_Err:
        app.ShowMessage(status.Err_Message)
        return
        
      oPrint = self.FormObject.ClientApplication.GetClientClass('PrintLib_2','PrintLib')()
      oPrint.doProcessByStreamName('gen_report',app,resp.packet,3)
  #--

  def status_droppingOnChange(self, sender):
    uipFilter = self.uipFilter
    uipFilter.Edit()
    pass

  def cbAllCabangOnClick(self, sender):
    if self.pFilter_cbAllCabang.Checked:
      self.pFilter_LCabang.Enabled = 0  
      self.uipFilter.ClearLink('LCabang')
    else:
      self.pFilter_LCabang.Enabled = 1
      self.pFilter_LCabang.SetFocus()
  #--

  def LFinProductOnExit(self, sender):
    uip = self.uipFilter
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_produk;nama_produk'
    
    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupTreaProduk", nField, lsData, 
      None,
      {
        'tipe_treasury':'P',
        'jenis_treasuryaccount':'B'
      }) 
    
    uip.Edit()
    uip.tipe_counterpart = uip.GetFieldValue(nField+'.tipe_counterpart')
    #uip.basis_hari_pertahun = uip.GetFieldValue(nField+'.basis_hari_pertahun')
    #setfiltertype enabled
    self.pFilter_type_report.Enabled = 1
      
    return res    

  def cbAllProdukOnClick(self, sender):
    if self.pFilter_cbAllProduk.Checked:
      self.pFilter_LProduk.Enabled = 0  
      self.uipFilter.ClearLink('LProduk')    
    else:
      self.pFilter_LProduk.Enabled = 1      
      self.pFilter_LProduk.SetFocus()
  #--

  def LCounterpartOnExit(self, sender):
    uip = self.uipFilter
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_counterpart;nama_counterpart'

    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupCounterpart", nField, lsData, 
      None,
      {
        'tipe_counterpart':uip.tipe_counterpart
      })

    return res

  def LCurrencyOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'Currency_Code;Description'

    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res
      
    res = uapp.stdLookup(sender, "parameter@lookupCurrency", nField, lsData)
    return res

  def cbAllCounterpartOnClick(self, sender):
    if self.pFilter_cbAllCounterpart.Checked:
      self.pFilter_LCounterpart.Enabled = 0  
      self.uipFilter.ClearLink('LCounterpart')     
    else:
      self.pFilter_LCounterpart.Enabled = 1      
      self.pFilter_LCounterpart.SetFocus()

  def cbAllValutaOnClick(self, sender):
    if self.pFilter_cbAllValuta.Checked:
      self.pFilter_LCurrency.Enabled = 0  
      self.uipFilter.ClearLink('LCurrency')     
    else:
      self.pFilter_LCurrency.Enabled = 1      
      self.pFilter_LCurrency.SetFocus()

  def type_reportOnChange(self, sender):
    idxpage = sender.ItemIndex
    self.pFilter_kode_remark.Visible = 0
    if idxpage in (2,1):
      self.uipFilter.kode_remark = 'ALL'
      self.pFilter_kode_remark.Visible = 1


  def qDataOnDoubleClick(self, sender):
    if self.uipFilter.type_report=='1':
      nomor_rekening =  self.qData.GetFieldValue('nomor_rekening')
      self.qAccountDetil(nomor_rekening)
    elif self.uipFilter.type_report=='2':
      journal_no =  self.qData.GetFieldValue('journal_no')
      self.qJournalDetil(journal_no)
    else:
      pass
    #--
    
  def qJournalDetil(self, journal_no):
    app = self.app
    app.SetLocalResourceMode(0)
    formid = 'core://accounting/fInfoJournal'
    #journal_no =  self.qToday.GetFieldValue('journal_no')
    f = app.CreateForm(formid, formid, 0,
     app.CreateValues(['journal_no', journal_no]), ['context', []], "", 1)
    f.ShowJournalInfo()
  #--
  
  def qAccountDetil(self, nomor_rekening):
    app = self.app
    app.SetLocalResourceMode(0)
    ph = app.CreateValues(['nomor_rekening', nomor_rekening], ['act_type', 'V'])
    frm = app.CreateForm('sukuk/fViewAccount', 'sukuk/fViewAccount', 0, ph, None)
    #frm.SetNomorRekening(key)
    frm.FormContainer.Show()
  #--
