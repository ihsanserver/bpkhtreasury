import com.ihsan.util.otorisasi as otorisasi
import sys 
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman

modman.loadStdModules(globals(), ['libs'])

def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config                     
  helper = pobjecthelper.PObjectHelper(config)
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipData"): return
  
  nomor_rekening = params.FirstRecord.nomor_rekening  
  oTreasuryAccount = helper.GetObject("TreasuryAccount", nomor_rekening)
  if oTreasuryAccount.IsNull:
    raise Exception, "Data not found. TreasuryAccount: %s" % nomor_rekening
    
  pobjconst = 'PObj:TreaSuratBerharga#nomor_rekening=' + nomor_rekening
  uideflist.SetData('uipData', pobjconst)
  
  ds = uideflist.uipData.Dataset
  rec = ds.GetRecord(0)

  if rec.kolektibilitas in [1]:
    rec.nom_ppap = oTreasuryAccount.ppap_umum
  else:
    rec.nom_ppap = oTreasuryAccount.ppap_khusus
  
  rec.saldo_accrue = oTreasuryAccount.ujroh+oTreasuryAccount.saldo_accrue
  
  '''  
  oProduk = helper.GetObject("TreaProduk", oTreasuryAccount.LProduk.kode_produk)
  oProduk = oProduk.CastToLowestDescendant()
  rec.tipe_treasury = oProduk.tipe_treasury
  '''