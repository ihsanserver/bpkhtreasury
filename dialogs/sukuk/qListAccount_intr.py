class qListAccount:
  def __init__(self, formObj, parentForm):
    pass
  #--
                                    
  def LCabangOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    res = uapp.checkStdLookup(sender, nField, '''kode_cabang;nama_cabang''' )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "ent_cabang@lookupCabang", nField, "kode_cabang;nama_cabang")
    return res
    
  def DisplayList(self, sender):
    formObj = self.FormObject; app = formObj.ClientApplication; userapp = app.UserAppObject
    #uip = self.uipFilter
    formObj.CommitBuffer()
    uip = self.uipFilter
    ph = app.CreateValues(
      ['kode_cabang', uip.GetFieldValue('LCabang.kode_cabang') or ""],
      ['kode_produk', uip.GetFieldValue('LProduk.kode_produk') or "ALL"],
      ['search_word', uip.search_word or ""],
      ['search_field', uip.search_field or ""],
      ['jenis_treasuryaccount', uip.jenis_treasuryaccount or "ALL"],
      ['status_rekening', uip.status_rekening or "9"],         
      ['tgl_awal', uip.start_date or 0],
      ['tgl_akhir', uip.end_date or 0],
      ['all_cabang', uip.cbAllCabang or "0"]
      #['tgl_histori_akhir', userapp.stdDate(uip.tgl_histori_akhir)]
    )
    #ph = formObj.GetDataPacket()

    resp = formObj.CallServerMethod("getQueryData", ph)
    
    rec = resp.FirstRecord
    if rec.error_status:
      app.ShowMessage(rec.error_message)
    else:
      self.qData.SetDirectResponse(resp.Packet)
    #qFinCollateralAsset.DisplayData()
  #--

  def status_droppingOnChange(self, sender):
    uipFilter = self.uipFilter
    uipFilter.Edit()
    pass

  def cbAllCabangOnClick(self, sender):
    if self.pFilter_cbAllCabang.Checked:
      self.pFilter_LCabang.Enabled = 0  
      self.uipFilter.ClearLink('LCabang')
    else:
      self.pFilter_LCabang.Enabled = 1
      self.pFilter_LCabang.SetFocus()
  #--

  def LFinProductOnExit(self, sender):
    uip = self.uipFilter
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_produk;nama_produk'

    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupTreaProduk", nField, lsData, 
      None,
      {
        'jenis_treasuryaccount':uip.jenis_treasuryaccount,
        'tipe_treasury':'P'
      })

    return res