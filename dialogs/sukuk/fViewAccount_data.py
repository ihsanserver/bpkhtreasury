import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.net.message as message
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.util.modman as modman
import com.ihsan.util.debug as debug
import rpdb2
#rpdb2.start_embedded_debugger('000', True, True)
modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

class record: pass
                                                                           
def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  mlu = config.ModLibUtils
  helper = pobjecthelper.PObjectHelper(config)
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipHelper;uipTreasury"): return
  tglIndo = modman.getModule(config,'TglIndo')
  fr = params.FirstRecord
  if fr.Dataset.IsFieldExist('nomor_rekening'): # view akun pembiayaan
    nomor_rekening = fr.nomor_rekening
  else:
    raise Exception, 'Invalid parameter'
    
  oPeriodHelper = helper.CreateObject("core.PeriodHelper")
  tgl_acc = oPeriodHelper.GetAccountingDate()
    
  try:
    pobjconst = 'PObj:TreaSuratBerharga#nomor_rekening=' + nomor_rekening
    uideflist.SetData('uipTreasury', pobjconst)
    
    oTreasury = helper.GetObject("TreaSuratBerharga", nomor_rekening)
    oTreasury = oTreasury.CastToLowestDescendant()
    ds = uideflist.uipTreasury.Dataset
    if ds.RecordCount < 1:
      raise Exception, 'Data not found or data integrity error. nomor_rekening = %s' % nomor_rekening
    rec = ds.GetRecord(0)
    rec.basis_hari_pertahun = oTreasury.LProduk.basis_hari_pertahun
    rec.act_type = fr.act_type
    rec.caption = 'View Detail Account %s' % (oTreasury.getJenisTreasury())
    if fr.act_type=='C':
      rec.caption = 'Tutup Account %s' % (oTreasury.getJenisTreasury())
      if oTreasury.status_rekening==3:
        raise Exception, "TIDAK BISA MELAKUKAN TRANSAKSI\nREKENING %s SUDAH LUNAS / TUTUP" % (oTreasury.nama_rekening)
        
    if fr.act_type=='M':
      rec.caption = 'Pembayaran Kupon %s' % (oTreasury.getJenisTreasury())
      if oTreasury.status_rekening==3:
        raise Exception, "TIDAK BISA MELAKUKAN TRANSAKSI\nREKENING %s SUDAH LUNAS / TUTUP" % (oTreasury.nama_rekening)
    
    if rec.tgl_jatuh_tempo not in [None,0] and rec.tgl_jatuh_tempo <= tgl_acc:
      rec.label_jt ='ACCOUNT SUDAH JATUH TEMPO TANGGAL '+tglIndo.tgl_indo(config,rec.tgl_jatuh_tempo,2)

    if oTreasury.status_rekening==3:
      rec.label_jt ='ACCOUNT SUDAH TUTUP TANGGAL '+tglIndo.tgl_indo(config,rec.tgl_tutup,2)

    #--historitransaksi
    uipFilter = uideflist.uipFilter; dsFilter = uipFilter.Dataset
    recFilter = dsFilter.AddRecord()
    recFilter.nomor_rekening = nomor_rekening
    recFilter.detail_level = 'D'
    recFilter.kode_jenis = oTreasury.kode_jenis
    recFilter.filter_transaction_class = 'F'

    rec.nominal_deal = abs(oTreasury.saldo)
    rec.nilai_tunai = abs(oTreasury.saldo+rec.revaluasi)

    rec.saldo_accrue = -rec.saldo_accrue or 0
    diskonto = abs(rec.revaluasi) or 0
    rec.nilai_amortisasi = abs(rec.nilai_amortisasi) or 0
    rec.revaluasi = diskonto + rec.nilai_amortisasi

    #--

  except:
    raise Exception, debug.getExcMsg()
  #if fr.act_type=='C' and oTreasury.status_rekening==3:
    #raise Exception, "TIDAK BISA MELAKUKAN TRANSAKSI\nREKENING %s SUDAH LUNAS / TUTUP" % (nomor_rekening)
#-- 

def authorize(config, params, returns):
  try :
    #rpdb2.start_embedded_debugger('000', True, True)  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    rec = params.uipTreasury.GetRecord(0)
    dsAkad = params.GetDatasetByName('uipData')
    recAkad = dsAkad.GetRecord(0)
    ph = {'recParent': rec, 'recAkad': recAkad}
    
    oTreasury = helper.GetObject("TreasuryAccount", rec.nomor_rekening)
    oTreasury = oTreasury.CastToLowestDescendant()
    
    # info user transaksi / otor
    dsInfo = params.__inputinfo
    recInfo = dsInfo.GetRecord(0)
    oTreasury.id_otorentri = recInfo.id_otorisasi
    
    oTreasury.closeTreasury(ph)
         
    #raise Exception, oAccount.nomor_ref_deal 
    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--  
  
def reject(config, params, returns):
  pass

#-- 
def authorize_kupon(config, params, returns):
  try :
    #rpdb2.start_embedded_debugger('000', True, True)  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    rec = params.uipTreasury.GetRecord(0)
    dsAkad = params.GetDatasetByName('uipData')
    recAkad = dsAkad.GetRecord(0)
    ph = {'recParent': rec, 'recAkad': recAkad}
    
    oTreasury = helper.GetObject("TreasuryAccount", rec.nomor_rekening)
    oTreasury = oTreasury.CastToLowestDescendant()
    
    # info user transaksi / otor
    dsInfo = params.__inputinfo
    recInfo = dsInfo.GetRecord(0)
    oTreasury.id_otorentri = recInfo.id_otorisasi
    
    oTreasury.marketTreasury(ph)
         
    #raise Exception, oAccount.nomor_ref_deal 
    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--  
  
def reject_kupon(config, params, returns):
  pass
    
def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000')
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  rec = params.uipTreasury.GetRecord(0)
  override_state = ''
  config.BeginTransaction()
  try :
    recOtor = otorisasi.prepareParameterOtorisasi(params)
    recOtor.kode_entri = 'TRSB001' if rec.act_type == 'C' else 'TRSB002'
    recOtor.keterangan = "%s - %s" % (rec.caption.upper(), rec.nomor_rekening.upper())
    recOtor.info1 = rec.nomor_rekening
    #recOtor.info2 = None
    recOtor.tipe_target = 0
    recOtor.id_peran = "OM"
    recOtor.nama_tabel = rec.contract_class_name
    recOtor.tipe_key = 1
    recOtor.key_string = rec.nomor_rekening
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    resOtor = app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph)
    
    #cek limit transaksi
    id_otorisasi = resOtor.FirstRecord.id_otorisasi
    transaction_amount = rec.nominal_deal
    TH = modman.getModule(config,'TransactionHelper')
    override_state = TH.TransactionHelper.CekLimitTransaksi(config, id_otorisasi, transaction_amount)
    
    isErr = 0
    errMsg = ""
    config.Commit()
  except :
    config.Rollback()
    isErr = 1
    errMsg = "%s.%s" % (str(sys.exc_info()[0]), str(sys.exc_info()[1]))
    nomor_rekening = ""
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg], ['nomor_rekening', rec.nomor_rekening], ['override_state', override_state])
#--  
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--
  