class fInputNewAccount:
  arrValidationDef = [['ujroh', 'num', 'value > 0', 'Nilai Ujroh harus lebih besar dari 0']
    , ['kolektibilitas', 'num', 'value > 0', 'Kolek Belum Dipilih']
  ]
  
  dRef = {           
     'LGolongan_Nasabah':'REF_GOLONGAN_NASABAH'
  }
    
  def __init__(self, formObj, parentForm):
    self.parentForm = parentForm
    pass
  #--
  
  def GetArrValidationDef(self):
    return self.arrValidationDef
  
  def OnEnterNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnEnterNum(sender)
  #--
  
  def OnExitNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnExitNum(sender)
  #--

  def harga_beliOnExit(self, sender):
    # procedure(sender: TrtfDBEdit)
    self.count_nilaitunai()
    
  def count_nilaitunai(self):
    uip = self.uipData
    uipParent = self.parentForm.uipTreasury
    uip.Edit()
    uip.nilai_tunai = uipParent.nominal_deal*(uip.harga_beli/100)

  def harga_baruOnExit(self, sender):
    # procedure(sender: TrtfDBEdit)
    self.count_nilaitunaisekarang()
    self.count_revaluasi()
    
  def count_nilaitunaisekarang(self):
    uip = self.uipData
    uipParent = self.parentForm.uipTreasury
    uip.Edit()
    uip.nilai_tunai_sekarang = round(uipParent.nominal_deal*(uip.harga_baru/100),2)
  
  def count_revaluasi(self):
    uip = self.uipData
    uip.Edit()
    uip.revaluasi = uip.nilai_tunai_sekarang-uip.nilai_tunai