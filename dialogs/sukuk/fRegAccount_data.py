import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.otorisasi as otorisasi
import com.ihsan.util.dbutil as dbutil
import com.ihsan.net.message as message
import sys
import com.ihsan.util.modman as modman
import rpdb2                          
#rpdb2.start_embedded_debugger('000', True, True)

modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

class record: pass
                                                                           
def FormOnSetDataEx(uideflist, params):
  # procedure(uideflist: TPClassUIDefList; params: TPClassUIDataPacket)
  config = uideflist.config
  helper = pobjecthelper.PObjectHelper(config)
  if otorisasi.loadDataOtorisasi(uideflist, params, "uipHelper;uipTreasury"): return
  uideflist.PrepareReturnDataset()
  rec = uideflist.uipTreasury.Dataset.AddRecord()
  phTgl = config.AppObject.rexecscript("core", "appinterface/coreInfo.getAccountingDay", config.AppObject.CreatePacket())
  tgl_transaksi = phTgl.FirstRecord.acc_date
  rec.date_now = tgl_transaksi
  rec.tgl_buka = tgl_transaksi
  rec.jenis_treasuryaccount = 'B'
  rec.kolektibilitas = 1
  rec.kode_entri = 'TRP004N'

  def_valuta = helper.GetObject('ParameterGlobalTreasury', 'DEF_VALUTA').nilai_parameter_string   
  def_valuta_desc = helper.GetObject('core.Currency', def_valuta).Description   

  def_cabang = helper.GetObject('ParameterGlobalTreasury', 'DEF_CABANG').nilai_parameter_string   
  def_cabang_desc = helper.GetObject('enterprise.Cabang', def_cabang).Nama_Cabang   

  rec.SetFieldByName('LCurrency.Currency_Code', def_valuta)
  rec.SetFieldByName('LCurrency.Description', def_valuta_desc)
  rec.SetFieldByName('LCabang.Kode_Cabang', def_cabang)
  rec.SetFieldByName('LCabang.Nama_Cabang', def_cabang_desc)
  
#-- 
def authorize(config, params, returns):
  try :
    #rpdb2.start_embedded_debugger('000', True, True)  
    helper = pobjecthelper.PObjectHelper(config)
    app = config.AppObject
    rec = params.uipTreasury.GetRecord(0)

    ph = {'recParent': rec}#, 'recAkad': recAkad}
    oAccount = helper.CreatePObject('TreaSuratBerharga', ph)
    
    # info user transaksi / otor
    dsInfo = params.__inputinfo
    recInfo = dsInfo.GetRecord(0)
    oAccount.id_otorentri = recInfo.id_otorisasi
    # process transaksi placing         
    oAccount.Placing(ph)
    
    #set saldo akrual awal saat pembelian
    oAccount.ujroh = oAccount.saldo_accrue or 0
    #set user input & auth
    oAccount.user_input = recInfo.user_input
    oAccount.time_input = recInfo.time_input
    oAccount.user_auth =  config.SecurityContext.initUser
    oAccount.time_auth =  config.Now() 
     
    #raise Exception, oAccount.nomor_ref_deal 
    config.FlushUpdates() # cek generate SQL
  except :
    errMsg = _dbg_excmsg()   
    raise Exception, errMsg
#--
  
def reject(config, params, returns):
  #delete infotransaksi
  dsInfo = params.__inputinfo
  recInfo = dsInfo.GetRecord(0)
  id_otorisasi = recInfo.id_otorisasi
  sSQL = '''
    DELETE FROM %(TreaInfoTransaksi)s WHERE id_otorisasi='%(id_otorisasi)s' 
  ''' % {
    'TreaInfoTransaksi': config.MapDBTableName("TreaInfoTransaksi"),
    'id_otorisasi':id_otorisasi
  }
  dbutil.runSQL(config, sSQL)
    
def save(config, params, returns):
  #rpdb2.start_embedded_debugger('000')
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  fau = helper.GetPClass('TreasuryUtils')(config)
  rec = params.uipTreasury.GetRecord(0) 
  #recAkad = params.uipData.GetRecord(0)
  override_state = ''
  config.BeginTransaction()
  try :
    #raise Exception,  
    nomor_rekening = fau.getNextAccountNumber(rec.jenis_treasuryaccount)
    rec.nomor_rekening = nomor_rekening

    recOtor = otorisasi.prepareParameterOtorisasi(params)
    recOtor.kode_entri = rec.kode_entri
    recOtor.keterangan = "Penempatan Inv. %s - %s" % (rec.nama_rekening, rec.seri_SB)
    recOtor.info1 = nomor_rekening
    #recOtor.info2 = None
    recOtor.tipe_target = 0
    recOtor.id_peran = "OM"
    recOtor.nama_tabel = rec.contract_class_name
    recOtor.tipe_key = 1
    recOtor.key_string = nomor_rekening
    
    ph = app.CreatePacket()
    ph.Packet.AcquireAnotherPacket(params)
    resOtor = app.rexecscript("enterprise", "api/otorisasi.storeDataPacket", ph)
    
    #cek limit transaksi
    id_otorisasi = resOtor.FirstRecord.id_otorisasi
    transaction_amount = rec.nominal_deal #+ recAkad.ujroh
    TH = modman.getModule(config,'TransactionHelper')
    override_state = TH.TransactionHelper.CekLimitTransaksi(config, id_otorisasi, transaction_amount)
    
    isErr = 0
    errMsg = ""
    config.Commit()
  except :
    config.Rollback()
    id_otorisasi = 0
    isErr = 1
    errMsg = "%s.%s" % (str(sys.exc_info()[0]), str(sys.exc_info()[1]))
    nomor_rekening = ""
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg], ['nomor_rekening', nomor_rekening], ['override_state', override_state], ['id_otorisasi', id_otorisasi])
#--

def cetakValidasi(config, params, returns):
  #rpdb2.start_embedded_debugger('000')
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  recTrea = params.uipTreasury.GetRecord(0)
  #recAkad = params.uipData.GetRecord(0)
  
  #accountinfo
  id_otorisasi = recTrea.id_otorisasi
  nominal = recTrea.nominal_deal
  kode_cabang = recTrea.GetFieldByName('LCabang.kode_cabang')
  kode_valuta = recTrea.GetFieldByName('LCurrency.Currency_Code')
  datenow = config.Now()
  user_input = config.SecurityContext.UserID
  
  #getaccount 
  kode_produk = recTrea.GetFieldByName('LProduk.kode_produk')
  rekening_debet, nama_rekening_debet = getAccount(config, 'tr_nominal', kode_produk)
  rekening_kredit, nama_rekening_kredit = getAccount(config, 'tr_giro_bi', kode_produk)
  
  config.BeginTransaction()
  try :                                                                                                       
    #save info transaksi
    rec = {
      'Id_Otorisasi':id_otorisasi,
      'Nama_Rekening_Debet': nama_rekening_debet,
      'Nomor_Rekening_Debet': rekening_debet,
      'Nama_Rekening_Kredit': nama_rekening_kredit,
      'Nomor_Rekening_Kredit': rekening_kredit,
      'Nominal_Transaksi': nominal,
      'Nomor_Ref_Deal': nomor_ref_deal,
      'Kode_Cabang': kode_cabang,
      'Kode_Valuta': kode_valuta,
      'User_Info': user_input,  
      'Date_Info': datenow,  
    }
    oTran = helper.CreatePObject('TreaInfoTransaksi', rec)
    
    #Cetak validasi
    CV = modman.getModule(config,'scripts#validasi_treasury')   
    filename = CV.GenerateValidasiFile(oTran)
    sw = returns.AddStreamWrapper()
    sw.LoadFromFile(filename)                       
    sw.MIMEType = config.AppObject.GetMIMETypeFromExtension(filename)
    
    isErr = 0
    errMsg = ""
    config.Commit()
  except :
    config.Rollback()
    isErr = 1
    errMsg = "%s.%s" % (str(sys.exc_info()[0]), str(sys.exc_info()[1]))
  #--
  returns.CreateValues(['isErr', isErr], ['errMsg', errMsg])
#--

def getAccount(config, classgroup, kode_produk):                                                    
  sSQL = '''
    SELECT account_code, account_name FROM %(ACCOUNT)s a
    INNER JOIN %(GLINTERFACE)s b ON a.account_code = b.kode_account
    INNER JOIN %(DETILTRANSAKSICLASS)s c ON b.kode_interface = c.kode_tx_class
    WHERE a.is_detail = 'T' AND c.classgroup = '%(classgroup)s'
    AND b.kode_produk = '%(kode_produk)s'
  ''' % {
        'ACCOUNT': config.MapDBTableName('core.ACCOUNT'),
        'GLINTERFACE': config.MapDBTableName('GLINTERFACE'),
        'DETILTRANSAKSICLASS': config.MapDBTableName('core.DETILTRANSAKSICLASS'),
        'classgroup': classgroup,
        'kode_produk': kode_produk,
  }
  q = config.CreateSQL(sSQL).RawResult
  
  #getData
  account_code = q.account_code
  account_name = q.account_name
  
  return account_code, account_name 
