class fRegAccount :
  # TO USE THIS FORM:
  # call self.defineUIFromCode
  # [call self.defineAuthorizeOperation()] -- optional from authorization framework
  # call self.activate
  # call self.FormContainer.Show()
  
  kode_entri = 'TRP004'  
  eqv_rate='';period_count='';amount_check='';spr_rate='';rep_period=''
  
  # format : fieldname, type:str|num, acceptedCondition, errMsg
  arrDefValidation = [ ['LCounterpart.kode_counterpart', 'str', 'len(value) > 0', 'Counterpart harus dipilih'] 
      , ['LProduk.kode_produk' , 'str', 'len(value) > 0', 'Produk harus dipilih']
      , ['LCurrency.Currency_Code', 'str', 'len(value) > 0', 'Kode Valuta harus diisi'    ]
      , ['LCabang.Kode_Cabang'     , 'str', 'len(value) > 0', 'Cabang harus diisi'      ]
      , ['nominal_deal'     , 'num', 'value > 0', 'Nominal harus diisi dan lebih besar dari 0']
      , ['tgl_buka'      , 'date', 'value > 0', 'Tanggal Buka Harus diisi'               ]
      , ['tgl_settlement'      , 'date', 'value > 0', 'Tanggal Settlement Harus diisi'               ]
      #, ['jangka_waktu'     , 'num', 'value > 0', 'Jangka Waktu harus diisi dan lebih besar dari 0']
      , ['tgl_jatuh_tempo'      , 'date', 'value > 0', 'Tanggal Jatuh Tempo Harus diisi'               ]
      , ['eqv_rate_real', 'num', 'value > 0', 'Coupon Rate harus diisi dan lebih besar dari 0']
  ]
  
  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    pass
    
 
  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.kode_entri = 'TRP004N'
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False
      
  def defineUIFromCode(self, kode_entri):
    self.kode_entri = kode_entri
    formObject = self.FormObject; app = formObject.ClientApplication; userApp = app.UserAppObject
    #--
    self.kode_entri = kode_entri
    self.authorizeMode = False
  #--
  
  def activate(self):
    #setFormView
    #self.SetFormView()
    pass

  # parsing kondisi validasi dengan memperhitungkan token :parameter_name untuk kondisi yang terkait dengan variabel lain
  def parseCondition(self, uipart, defValidation) :
    TOKEN_PARAM = ':'
    elmt = defValidation
    fname = elmt[0]; ftype = elmt[1] ; cond = elmt[2] ; errMsg = elmt[3]
    arrCondWords = cond.split(' ') ; idx = 0 ; value = ''
    for word in arrCondWords :
      # check token params
      if word[0] == TOKEN_PARAM :
        paramFieldName = word[1:] #without first char
        #get param value
        tmpval = uipart.GetFieldValue(paramFieldName) or ""
        if isinstance(tmpval, str) :
          arrCondWords[idx] = ''' '%s' ''' % tmpval
        else :
          arrCondWords[idx] = str(tmpval)
      else :
        pass # currently do nothing
              
      if ftype == 'str' :
        value = uipart.GetFieldValue(fname) or ""
        value = value.strip()
      elif ftype == 'num' :
        tmpval = uipart.GetFieldValue(fname)
        value = -999 if tmpval == None else tmpval          
      else : raise Exception, "doValidation : unknown type %s" % (ftype)         
      idx = idx + 1
    # rebuild cond
    cond = " ".join(arrCondWords)
    return cond, value
  #--
      
  def getValidationMsg(self):
    app = self.FormObject.ClientApplication
    userApp = app.UserAppObject 
    uip = self.uipTreasury
    # generic form
    arrValidation = []
    
    arrDefValidation = self.arrDefValidation
    arrValidation += arrDefValidation
      
    arrErrMsg = userApp.doValidation(uip, arrValidation)
    
    return arrErrMsg
  #--
  
  def OnEnterNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnEnterNum(sender)
  #--
  
  def OnExitNum(self, sender):
    form = sender.OwnerForm
    uapp = form.ClientApplication.UserAppObject
    uapp.OnExitNum(sender)
  #--

  def AsDateTime(self, dateTuple):
    modDateTime = self.FormObject.ClientApplication.ModDateTime
    tanggal = modDateTime.EncodeDate(dateTuple[0], dateTuple[1], dateTuple[2])
    return tanggal      

  def LProdukOnExit(self, sender):
    uip = self.uipTreasury
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_produk;nama_produk;tipe_counterpart;basis_hari_pertahun'

    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupTreaProduk", nField, lsData, 
      None,
      {
        'jenis_treasuryaccount':uip.jenis_treasuryaccount,
        'tipe_treasury':'P'
      })
    uip.Edit()
    uip.tipe_counterpart = uip.GetFieldValue(nField+'.tipe_counterpart')
    uip.basis_hari_pertahun = uip.GetFieldValue(nField+'.basis_hari_pertahun')
    
    return res
    
  def LCounterpartOnExit(self, sender):
    uip = self.uipTreasury
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'kode_counterpart;nama_counterpart'

    res = uapp.checkStdLookup(sender, nField, lsData )      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupCounterpart", nField, lsData, 
      None,
      {
        'tipe_counterpart':uip.tipe_counterpart
      })

    return res

  def LValutaOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject 
    nField = sender.Name
    lsData = 'Currency_Code;Description'

    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res
      
    res = uapp.stdLookup(sender, "parameter@lookupCurrency", nField, lsData)
    return res

  def LCabangOnExit(self, sender):
    uapp = self.FormObject.ClientApplication.UserAppObject
    nField = sender.Name
    lsData = 'kode_cabang;nama_cabang'
    
    res = uapp.checkStdLookup(sender, nField, lsData)      
    if res != None:
      return res

    res = uapp.stdLookup(sender, "parameter@lookupCabang", nField, lsData)
    return res

  def jangka_waktuOnExit(self, sender):
    app = self.FormObject.ClientApplication
    uip = self.uipTreasury
    if uip.jangka_waktu == None:
      app.ShowMessage('Jangka Waktu harus diisi..!!')
      self.pForm_jangka_waktu.SetFocus()
    else:
      opendate=self.AsDateTime(uip.tgl_buka)  
      uip.tgl_jatuh_tempo = opendate + uip.jangka_waktu
  
  def count_cash(self, sender):
    uip = self.uipTreasury
    uip.Edit()
    bidvolume = (uip.nilai_nav or 1)
    uip.n_akru = uip.saldo_accrue / bidvolume
    uip.cash_proceeds = ((uip.saldo_accrue or 0) + (uip.nilai_tunai or 0)) or 1
    uip.n_cash = uip.cash_proceeds / bidvolume
    uip.n_netto = uip.nilai_tunai / bidvolume

  def nilai_tunaiOnExit(self, sender):
    # procedure(sender: TrtfDBEdit)
    uip = self.uipTreasury
    uip.Edit()
    uip.harga_beli = (uip.nilai_tunai / (uip.nominal_deal or 1))*100
    uip.revaluasi = uip.nilai_tunai - uip.nominal_deal
    self.panel1_revaluasi.ControlCaption = 'Permium'
    if uip.revaluasi < 0:
      self.panel1_revaluasi.ControlCaption = 'Diskonto'
    
    #self.count_cash(sender)
    #pass  
    
  def FormAfterProcessServerData(self, formobj, operationid, datapacket):
    # function(formobj: TrtfForm; operationid: integer; datapacket: TPClassUIDataPacket):boolean
    uipTrea = self.uipTreasury
    self.FormObject.PyFormObject.mInput.GetPage(1).TabVisible = False #Override
      
  
  # save to server
  def saveFasilitas(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    uipHelper = self.uipHelper
    
    #set data untuk pengecekan
    uipTreasury = self.uipTreasury
    uipTreasury.Edit()
    
    arrErrMsg = self.getValidationMsg()
    if len(arrErrMsg) > 0 :
      errMsg = "\n".join(arrErrMsg)
      app.ShowMessage(errMsg)
    else : 
      dlg_msg = 'Apakah anda yakin?'    
      
      dlg = app.ConfirmDialog(dlg_msg) 
      if dlg:
        # ambil dari frame                
        form.CommitBuffer()
        params = form.GetDataPacket()
                
        resp = form.CallServerMethod('save', params)    
        status = resp.FirstRecord
        if status.isErr:
          app.ShowMessage('Error :' + status.errMsg)
        else :
          #cek limit transaksi
          if status.override_state=='F':
            app.ShowMessage('Transaksi Melebihi Limit User, Override transaksi Untuk Selesaikan Entri transaksi..!')
            
          app.ShowMessage('%s berhasil dibuat. Nomor rekening = %s\r\nEfektif setelah otorisasi' % (uipTreasury.caption, status.nomor_rekening))    
          sender.ExitAction = 1
        
          #Cetak Validasi
          uipTreasury.Edit()
          uipTreasury.id_otorisasi = status.id_otorisasi
          #self.cetakValidasi()
        #--
      #--
    #--
  #--
     
  def cetakValidasi(self):
    form = self.FormObject
    app = form.ClientApplication
    form.CommitBuffer()
    params = form.GetDataPacket()

    #cetak Validasi    
    cv = form.CallServerMethod('cetakValidasi', params)    
    status = cv.FirstRecord
    
    if status.isErr:
      app.ShowMessage('Error : %s' + status.errMsg)
      return
    
    try:       
      app.ShowMessage("Masukkan Slip Transaksi ke Printer") 
      oPrint = self.FormObject.ClientApplication.GetClientClass('PrintLib','PrintLib')()
      oPrint.doProcess(app, cv.packet, 2)
    except:
      app.ShowMessage("Proses cetak slip gagal. Silahkan cek koneksi ke printer.")
  #--

  def setRekBank(self, cust, nField):
    uip = self.uipTreasury
    uip.SetFieldValue(nField+'.nomor_rekening', cust['nomor_rekening'])
    uip.SetFieldValue(nField+'.nama_rekening', cust['nama_rekening'])
    #'''
    
  def LRekBankOnBeforeLookup(self, sender, linkui):
    # function(sender: TrtfDBLookupEdit; linkui: TrtfLinkUIElmtSetting): boolean
    uapp = self.FormObject.ClientApplication.UserAppObject
    dRekSumber = uapp.lookupRekBank()
    if dRekSumber != None:
      self.setRekBank(dRekSumber, sender.Name)
    #--
    return 0
  #--
