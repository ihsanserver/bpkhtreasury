class fCreateDepCollateralAccount:
  
  _cols = {
          'LFinCollateralAsset.nomor_rekening':'LFinCollateralAsset.nomor_rekening',
          'LFinCollateralAsset.bukti_kepemilikan':'LFinCollateralAsset.bukti_kepemilikan',
          'LFinCollateralAsset.pemilik_agunan':'nama_nasabah',
          'LFinCollateralAsset.valuation':'valuation',
          'LFinCollateralAsset.market_value':'market_value',
          'LFinCollateralAsset.liquidation_value':'liquidation_value',
          'LFinCollateralAsset.value_use':'used_value',
          'jenis_agunan':'jenis_agunan',
          'LJenis_Agunan.reference_code':'kode_agunan',
          'LJenis_Agunan.reference_desc':'keterangan_agunan',
          'LGolPenerbit.reference_code':'kode_penerbit',
          'LGolPenerbit.reference_desc':'keterangan_penerbit',
          'golongan_penerbit':'golongan_penerbit',
          'legal_binding':'legal_binding',
          'Lref_jenis_pengikatan.reference_code':'kode_ref',
          'Lref_jenis_pengikatan.reference_desc':'keterangan_ref',
          'ref_jenis_pengikatan':'ref_jenis_pengikatan',
          'tanggal_pengikatan':'tanggal_pengikatan',
          'paripasu':'paripasu',
          'paripasu_market_value':'paripasu_market_value',
          'paripasu_liquidation_value':'paripasu_liquidation_value',
          'is_ppap_deduction':'is_ppap_deduction'
        }
  
  _cols_2 = {
          'tanggal_pengikatan':'tanggal_pengikatan',
          'LFinCollateralAsset.nomor_rekening':'nomor_rekening',
          'LFinCollateralAsset.bukti_kepemilikan':'bukti_kepemilikan',
          'LFinCollateralAsset.pemilik_agunan':'pemilik_agunan',
          'Lref_jenis_pengikatan.reference_desc':'ref_jenis_pengikatan',
          'legal_binding':'legal_binding',
          'paripasu':'paripasu',
          'paripasu_market_value':'paripasu_market_value',
          'paripasu_liquidation_value':'paripasu_liquidation_value'
        }
  
  data = {'fMode':''}
        
  kode_entri = ''
  
  dictMapping = {
      'TRLINK': {
        'contract_class_name': "TreasuryAccount",
        'caption': "Link agunan",
        'subframe_id': "",
        'contract_type': "",
        'contract_uip_name': "uipTreaAcc"
      }
    }

  def __init__(self, formObj, parentForm):
    self.authorizeMode = False
    self.parentForm = parentForm
    self.app = formObj.ClientApplication
    pass
  #--   
  
  # save to server
  def saveTreaCollateralAccount(self, sender):
    form = self.FormObject
    app = form.ClientApplication
    #uipHelper = self.uipHelper
    
    uipTreaAcc = self.uipTreaAcc
    uipTreaCollateralAccount = self.uipTreaCollateralAccount
    
    uipTreaCollateralAccount.First()
    '''
    while not uipTreaCollateralAccount.eof :
      uipTreaCollateralAccount.Edit()
      if uipTreaCollateralAccount.GetFieldValue('LFinCollateralAsset.dasar_nilai') == 'P' :
        uipTreaCollateralAccount.pct_use = uipTreaCollateralAccount.value_pct_use
      if uipTreaCollateralAccount.GetFieldValue('LFinCollateralAsset.dasar_nilai') == 'N' :
        uipTreaCollateralAccount.value_use = uipTreaCollateralAccount.value_pct_use
      uipTreaCollateralAccount.Next()
    '''
    
    form.CommitBuffer()
    params = form.GetDataPacket()
    resp = form.CallServerMethod('save', params)    
    status = resp.FirstRecord
    if status.isErr:
      app.ShowMessage('Error :' + status.errMsg)
    else:
      app.ShowMessage('Berhasil disimpan')    
      sender.ExitAction = 1

  def uipTreaCollateralAccountBeforeDelete(self, uip):
    # procedure(uip: TrtfPClassUI)
    #norek = uip.GetFieldValue('LFinCollateralAsset.Nomor_Rekening')
    grid = self.uipDelete
    grid.Append()
    #grid.nomor_rekening = norek
    for col_name in self._cols_2:
      grid.SetFieldValue(self._cols_2[col_name], uip.GetFieldValue(col_name))
    grid.Post()
    
    pass
    
  def defineUIFromCode(self, kode_entri):
    self.kode_entri = kode_entri
    formObject = self.FormObject; app = formObject.ClientApplication; userApp = app.UserAppObject
    entri = self.dictMapping.get(kode_entri, None)
    if entri == None:
      raise Exception, "Entry code %s not found" % kode_entri
    userApp.clientAttrUtil.transferAttributes([
      'contract_class_name', 'caption', 'subframe_id', 
      'contract_type', 'contract_uip_name'
      ], self, entri)
    if self.parentForm != None:
      self.parentForm.FormObject.Caption = self.caption
    else:
      self.FormObject.Caption = self.caption
    #--
    self.kode_entri = kode_entri
    self.authorizeMode = False
  #--

  def defineAuthorizeOperation(self, kode_entri, id_otorisasi):
    self.authorizeMode = True
    self.kode_entri = kode_entri
    self.id_otorisasi = id_otorisasi
    self.FormObject.SetAllControlsReadOnly()
    self.pAction.Visible = False
    
  def activate(self):
    formObj = self.FormObject; app = formObj.ClientApplication
    #app.ShowMessage('activate() called')
    if self.authorizeMode:
      ph = app.CreateValues(['operation', 'view_authorize'], ['id_otorisasi', self.id_otorisasi])
    else:
      self.setDefaultFormData()
      ph = None
    systemContext = formObj.SystemContext
    if systemContext != "":
      subframe_id = "%s://%s" % (systemContext, self.subframe_id)
    else:
      subframe_id = self.subframe_id 


  def bAddOnClick(self, sender):
    grid = self.uipTreaCollateralAccount
    self.OpenFormDetail(grid, 'new')
  
  def bEditOnClick(self, sender):
    grid = self.uipTreaCollateralAccount
    for col_name in self._cols:
      self.data[self._cols[col_name]]=grid.GetFieldValue(col_name)
      
    self.OpenFormDetail(grid, 'edit')

  def bDeleteOnClick(self, sender):
    grid = self.uipTreaCollateralAccount
    dlg = self.app.ConfirmDialog('Yakin akan menghapus data?')
    if dlg:
      grid.Delete()

    
  def OpenFormDetail(self, grid, fMode):
    uip = self.uipTreaAcc
    form = self.app.CreateForm('fAddCollAccount','fAddCollAccount',0,None,None)
    self.data['fMode'] = fMode
    self.data['norek_treaAccount'] = uip.nomor_rekening
    self.data['jenis_treasuryaccount'] = uip.jenis_treasuryaccount

    ph = form.Show(self.data)
    if ph  <> 0:
      res = ph.FirstRecord
      self.IsiGridDetail(res, grid, fMode)

  
  def IsiGridDetail(self, res, grid, fMode):
    if fMode =='new':
      grid.Append()
    else:
      grid.Edit()

    for col_name in self._cols:
      grid.SetFieldValue(col_name, res.GetFieldByName(self._cols[col_name]))
      
    grid.Post()