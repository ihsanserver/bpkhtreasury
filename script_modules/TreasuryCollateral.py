import com.ihsan.foundation.pobject as pobject
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.modman as modman
import com.ihsan.net.message as message
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.customidgenAPI as customidgenAPI                                                            
import com.ihsan.util.dbutil as dbutil


#mod_rekening = modman.getRefModule(appserver.ActiveConfig, "core", "mod_rekening")

def dbg(msg):
  message.send_udp(msg + "\n", 'localhost', 9123)

class TreaCollateralAccount(pobject.PObject):
    # Static variables
    pobject_classname = 'TreaCollateralAccount'
    pobject_keys      = ['norek_treaaccount', 'norek_fincollateralasset'] 
    
    def OnCreate(self, param):
      config = self.Config
      helper = self.Helper 
      app = config.AppObject
      #app.ConCreate('out')

      recSrc = param['TreaCollateralAccount']
      oRecSrc = atutil.GeneralObject(recSrc)
      oAccount = param['oTreasuryAccount']

      self.norek_treaaccount = oAccount.nomor_rekening
      self.LTreaAccount = oAccount
      self.pct_use = 0
      atutil.transferAttributes(helper, 
        [ 'norek_fincollateralasset=LFinCollateralAsset.Nomor_Rekening',
          'legal_binding*',
          'ref_jenis_pengikatan*',
          'tanggal_pengikatan*',
          'is_ppap_deduction*',
          'paripasu*',
          'paripasu_liquidation_value*',
          'jenis_agunan*',
          'golongan_penerbit*'            
        ], self, recSrc
      )
      LTreaAccount = self.LTreaAccount 
      LTreaAccount.coll_deduct_recalc = 'T'
      oCollateral = self.LFinCollateralAsset
      #raise Exception, self.norek_fincollateralasset
      oCollateral.colasset_status_update = 'B'
      #oCollateral.calcParipasu()
      #if oCollateral.asset_type not in ('J','H'):
        #app.ConWriteln('qq')
        #app.ConRead('')

      #oCollateral.addRemoveUsage(oRecSrc.pct_use, oRecSrc.value_use)

      #self.config.FlushUpdates()

    def UpdatePengikatan(self, param):
      config = self.Config
      helper = self.Helper      
      recSrc = param['FinCollateralAccount']
      oRecSrc = atutil.GeneralObject(recSrc)
      # set fields with simple / direct 1-on-1 assignment from record with same field name
      old_pct_use = self.pct_use or 0 
      old_value_use = self.value_use or 0 
      new_pct_use = oRecSrc.pct_use or 0 
      new_value_use = oRecSrc.value_use or 0
      atutil.transferAttributes(helper,
        [ 'legal_binding*',
          'ref_jenis_pengikatan*',
          'tanggal_pengikatan*',
          'is_ppap_deduction*',
          'paripasu*',
          'paripasu_liquidation_value*',
          'jenis_agunan*',
          'golongan_penerbit*' 
        ], self, recSrc
      )
      oAccount = self.LFinAccount 
      oAccount.coll_deduct_recalc = 'T'
      oCollateral = self.LFinCollateralAsset
      #oCollateral.addRemoveUsage(new_pct_use - old_pct_use, new_value_use - old_value_use)
      
      #if oCollateral.asset_type not in ('J','H'): 
      #oCollateral.calcParipasu() 

      #cvalue = oAccount.collateral_value
      #oAccount.collateral_value = cvalue + (new_value_use - old_value_use)
      
    def ModifyValue(self, pct_use, value_use):
      helper = self.Helper
      old_pct_use = self.pct_use 
      old_value_use = self.value_use 
      self.pct_use = pct_use
      self.value_use = value_use
      oAccount = self.LFinAccount
      oAccount.coll_deduct_recalc = 'T'
      # untuk cash collateral internal, sesuaikan blok saldo sebesar nilai penggunaan
      oCollateral = self.LFinCollateralAsset
      oCollateral.addRemoveUsage(pct_use - old_pct_use, value_use - old_value_use)
      #if oCollateral.IsA('FinColIntDeposito'):
      #  oDepCollateral = oCollateral.CastToLowestDescendant()
      #  if value_use > old_value_use:
      #    oDepCollateral.holdOrReleaseBalance(oAccount, 'C', value_use - old_value_use)
      #  else:
      #    oDepCollateral.holdOrReleaseBalance(oAccount, 'D', old_value_use - value_use)
        #--
      #--
      #self.config.FlushUpdates()
      
    def OnDelete(self):
      helper = self.Helper
      #user_blokir = self.config.SecurityContext.UserID      
      oAccount = self.LFinAccount
      oAccount.coll_deduct_recalc = 'T'
      oCollateral = self.LFinCollateralAsset

      #oCollateral.addRemoveUsage(-self.pct_use, -self.value_use)
      #if oCollateral.asset_type not in ('J','H'): 
      #  oCollateral.calcParipasu()
          
      #--
#--