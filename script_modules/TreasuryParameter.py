import com.ihsan.foundation.pobject as pobject
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.modman as modman
import com.ihsan.net.message as message
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.dbutil as dbutil

#mod_rekening = modman.getRefModule(appserver.ActiveConfig, "core", "mod_rekening")

def dbg(msg):
  message.send_udp(msg + "\n", 'localhost', 9123)

   
class TreaCounterpart(pobject.PObject):
  # Static variables
  pobject_classname = 'TreaCounterpart'
  pobject_keys      = ['kode_counterpart']

  d_dict={'A':'Bank Umum', 'B':'Bank Central', 'C':'Korporat'}
  trfAttribut = [
        'tipe_counterpart*',
        'nama_counterpart=*LMemberRTGS.nama_member',
        'nilai_limit*',
        'is_unlimited*',
        'limit_used*',      
        'remark_counterpart*',
        'status*'
      ]
      
  def OnCreate(self, param):    
    trfAttribut = self.trfAttribut
    trfAttribut.append('kode_counterpart=*LMemberRTGS.kode_member')      

    self.doSave(param, trfAttribut)
  
  def Edit(self, param):
    self.doSave(param, self.trfAttribut)

  def doSave(self, param, trfAttribut):
    config = self.Config
    helper = self.Helper      
    atutil.transferAttributes(helper, self.trfAttribut, self, param)
#--

class TreaProduk(pobject.PObject):
  # Static variables
  pobject_classname = 'TreaProduk'
  pobject_keys      = ['kode_produk']

  trfAttribut = [
        'nama_produk*'
        ,'jenis_treasuryaccount=*LJenisTreasury.jenis_treasuryaccount'
        ,'kode_pof=*LPoolOfFund.kode_Pof'
        ,'basis_hari_pertahun*'
        ,'tipe_treasury*'
        ,'tipe_counterpart*'
        ,'tipe_fasbis*'
        ,'sumber_dana*'
        ,'jenis_instrumen=*LJenisInstrumen.refdata_id'
      ]
      
  def OnCreate(self, param):    
    trfAttribut = self.trfAttribut
    trfAttribut.append('kode_produk')      

    self.doSave(param, trfAttribut)
  
  def Edit(self, param):
    self.doSave(param, self.trfAttribut)

  def doSave(self, param, trfAttribut):
    config = self.Config
    helper = self.Helper      
    atutil.transferAttributes(helper, self.trfAttribut, self, param)

  def VerifiedGLInterfaceProduk(self):
    config = self.Config
    dParam = {
      'detiltransaksiclass': config.MapDBTableName('core.detiltransaksiclass'),
      'account': config.MapDBTableName('core.account'),
      'glinterface': config.MapDBTableName('glinterface'),
      'kode_produk': self.product_code, 'classparameter': self.contracttype_code
    }
    sSQL=''' 
      SELECT DISTINCT 
        b.kode_tx_class, b.classgroup, b.acc_type, b.deskripsi, c.account_code, c.account_type 
      FROM {detiltransaksiclass} b 
      LEFT JOIN {glinterface} a ON a.kode_interface=b.kode_tx_class AND a.kode_produk='{kode_produk}'
      LEFT JOIN {account} c ON a.kode_account=c.account_code
      WHERE b.acc_type<>'N' and b.classparameter='{classparameter}'
      ORDER BY kode_tx_class 
    '''.format(**dParam)
    rSQL = config.CreateSQL(sSQL).RawResult
    rSQL.First()   
    lsTxClass=[]
    while not rSQL.Eof:
      if rSQL.account_code in [None, '']:
        lsTxClass.append('kode tx class %s belum dibuat mappingnya pada GLInterface' % (rSQL.kode_tx_class))
      elif rSQL.acc_type <> rSQL.account_type:      
        lsTxClass.append('kode tx class %s, Tipe Account pada GL Interface tidak sesuai dengan ketentuan' % rSQL.kode_tx_class)

      rSQL.Next()
    #--endwhile
    
    if len(lsTxClass)>0:
      raise Exception, "\n ".join(lsTxClass)
    #--
              
  def EditGLInterface(self, param):  
    config = self.Config
    helper = self.Helper      
    uipGrid = param['recGL']
    kode_produk = self.kode_produk
    cei_list ='('
    for i in range(0, uipGrid.RecordCount):
      recDetil = uipGrid.GetRecord(i)
      sql = """
        SELECT Id_GLInterface FROM %(glinterface)s WHERE kode_produk='%(kode_produk)s' and kode_interface='%(kode_interface)s'
      """ % {
          'glinterface': config.MapDBTableName('glinterface'),
          'kode_produk': kode_produk, 'kode_interface':recDetil.kode_tx_class
        }
      rs = config.CreateSQL(sql).RawResult
      Id_GLInterface = rs.Id_GLInterface
      
      if recDetil.status_gl == 'T': 
        oTx = helper.GetObject("GLInterface", Id_GLInterface)
        if oTx.IsNull:
          oTx = helper.CreatePObject("GLInterface")
          
        #if recDetil.gl_account in (None, '', ' '):
        #  raise Exception, 'Kode account untuk produk %s - tx_class % tidak boleh kosong!' % (kode_produk, recDetil.kode_tx_class)
          
        oTx.KODE_ACCOUNT   = recDetil.gl_account 
        oTx.KODE_INTERFACE = recDetil.kode_tx_class
        oTx.KODE_PRODUK    = kode_produk
          
        cei_list += str(oTx.Id_GLInterface)+','
        
    cei_list += '0)'
    
    # Delete cost element
    sSQL ="DELETE FROM %s WHERE ID_GLINTERFACE not in %s AND KODE_PRODUK='%s' " % (config.MapDBTableName("GLINTERFACE"),cei_list,kode_produk)
    config.ExecSQL(sSQL)
#--

class GLInterface(pobject.PObject):
  # Static variables
  pobject_classname = 'GLInterface'
  pobject_keys      = ['Id_GLInterface']
#---

class ParameterGlobalTreasury(pobject.PObject):
  # Static variables
  pobject_classname = 'ParameterGlobalTreasury'
  pobject_keys      = ['kode_parameter']

  trfAttribut = [ 'tipe_parameter*',
        'deskripsi*',
        'nilai_parameter*',
        'nilai_parameter_tanggal*',
        'nilai_parameter_string*'
      ]

  def OnCreate(self, param):
    config = self.Config
    helper = self.Helper
    #trfAttribut = self.trfAttribut
    self.trfAttribut.append('kode_parameter')
    self.Edit(param)
    self.status = 'T' 
   
  def Edit(self, param):
    config = self.Config
    helper = self.Helper
    app = config.AppObject
    app.ConCreate('out')
          
    mlu = config.ModLibUtils
    recSrc = param['datapaket']
    atutil.transferAttributes(helper, self.trfAttribut, self, recSrc)
    self.config.FlushUpdates()
  #--
  
#---
      