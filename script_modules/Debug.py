import sys
import traceback
import os
import com.ihsan.foundation.pobject as pobject

__DEBUG_IS_ENABLED = 1
__DEBUG_DEF_LEVEL  = 4

# mendapatkan error message secara lebih detail, 
# digunakan utk mempermudah develpment karena kita bisa mengetahui di file mana dan line berapa error tsb terjadi.
# level 3 : tampilkan file name & line number
# level 4 : tampilkan traceback / stacktrace
def getExcMsg(__DEBUG_IS_ENABLED = 1, LEVEL=4) :
  #LEVEL = 4
  #LEVEL = __DEBUG_DEF_LEVEL  # use default, ignore argument, for PRODUCTION CODE uncomment above row, comment this row, or use LEVEL = 1 or LEVEL = 0
  exc_tb = sys.exc_info()[2] ; msg = ''
  if __DEBUG_IS_ENABLED :
    msg += "\r\n DEVELOPMENT MODE, DEBUG ENABLED, DEBUG LEVEL = %d \r\n" % LEVEL;
    msg += "TO HIDE THIS VERBOSE DEBUG MESSAGE, EDIT : Debug.getExcMsg : \r\n"
    if LEVEL >= 0 :
      msg += str(sys.exc_info()[0]) + "\r\n"
    if LEVEL >= 1 :
      msg += str(sys.exc_info()[1]) + "\r\n"
    if LEVEL >= 2 :
      fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]      
      msg += '--> ' + fname + "\r\n"
    if LEVEL >= 3 :
      slineno = '|--> LINE NUMBER : ' + str(exc_tb.tb_lineno)
      msg += slineno + "\r\n"
    if LEVEL >= 4 :
      msg += "\r\n"
      msg += "----- TRACEBACK ----- \n"
      formatted_lines = traceback.format_exc().splitlines()
      for line in formatted_lines : 
        msg += str(line) + "\r\n"
      #for  
  return msg
#--    