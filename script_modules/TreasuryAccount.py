import sys
import com.ihsan.foundation.pobject as pobject
import com.ihsan.foundation.mobject as mobject
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.customidgenAPI as customidgenAPI                                                            
import com.ihsan.util.modman as modman
import com.ihsan.net.message as message
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.lib.textreport as report
import copy
import calendar
from decimal import Decimal, ROUND_HALF_UP

modman.loadStdModules(globals(), ['libs'])

class NameSpaceHelper: pass # dummy class to store this module's namespace

gHelper = pobjecthelper.PObjectHelper(appserver.ActiveConfig)
gFinJournal = modman.getModule(appserver.ActiveConfig, 'TreasuryJournal')

EPSILON = 0.01

def dbg(msg):
  message.send_udp(msg + "\n", 'localhost', 9898)

class PendingTransactionJournal(pobject.PObject):
    # Static variables
    pobject_classname = 'PendingTransactionJournal'
    pobject_keys      = ['id_transaksi']
    
    def OnCreate(self, oTransaction, holdJournal = False):
      self.id_transaksi = oTransaction.id_transaksi
      self.session_id = self.Config.SecurityContext.SessionID
      self.config.FlushUpdates()
      if not holdJournal:
        try:
          # raise Exception, "Masuk ke buat jurnal"
          periodHelper = self.Helper.CreateObject('core.PeriodHelper')
          #tgl_journal = periodHelper.GetAccountingDate()
          tgl_journal = oTransaction.GetAsTDateTime('tanggal_transaksi')
          #raise Exception, tgl_journal
          sTglJournal = self.config.FormatDateTime('dd-MMM-yyyy', tgl_journal)
          gFinJournal.createTreasuryJournal(self.config, sTglJournal, self.id_transaksi)
        finally:
          self.config.ResetCache()
        #--
      #--
    #-- def OnCreate
      
    def sendToAccounting(self):
      config = self.Config
      app = config.AppObject
      if self.flag_sent == 'T':
        return
      id_transaksi = self.id_transaksi
      config.BeginTransaction()
      try:
        isErr = 0
        errmsg = '' 
        try:
          if self.flag_sent == 'F' or self.flag_sent == None or self.flag_sent == '':
            phRes = app.rexecscript('remote_core', 'appinterface/RemoteJournal', app.CreateValues(['id_transaksi', id_transaksi]))
            fr = phRes.FirstRecord
            isErr = not fr.status
            errmsg = fr.errmsg
            if not isErr: self.flag_sent = 'T'
          #--
        except:
          isErr = 1
          errmsg = str(sys.exc_info()[1])
        self.is_err = int(isErr)
        self.err_message = errmsg
        config.Commit()
      except:
        config.Rollback()
        raise
        
      if isErr:
        raise Exception, errmsg
        
      dParams = {'filter': 'ptj.id_transaksi = %d' % id_transaksi}
      config.BeginTransaction()
      try:
        sSQL = '''
          INSERT INTO completedtransactionjournal
          (
            id_transaksi,
            journal_no
          )
          SELECT id_transaksi, journal_no FROM
          pendingtransactionjournal ptj
          WHERE 
          %(filter)s
        ''' % dParams
        dbutil.runSQL(config, sSQL)
        self.Delete()
        config.Commit()
      except:
        config.Rollback()
        raise
      #--
    #-- send to accounting
      
    def sendAllToAccounting(self, currentSessionOnly = True):
      config = self.Config; mlu = config.ModLibUtils
      helper = self.Helper
      app = config.AppObject
      app.ConCreate('out')
      if currentSessionOnly:
        allSession = 0
      else:
        allSession = 1
        
      q = config.CreateSQL('''
        select id_transaksi
        from pendingtransactionjournal where
        (session_id = %s or 1 = %d) and (flag_sent is NULL or flag_sent = 'F') order by id_transaksi
      ''' % (mlu.QuotedStr(config.SecurityContext.SessionID), allSession)).RawResult
      errorOccured = False
      while not q.Eof:
        id_transaksi = q.id_transaksi
        o = helper.GetObject('PendingTransactionJournal', id_transaksi)
        try:
          o.sendToAccounting()
          app.ConWriteln('Create accounting entry for id %d success' % id_transaksi, 'out')
        except:
          errorOccured = True
          errmsg = str(sys.exc_info()[1])
          app.ConWriteln('Create accounting entry for id %d failed: %s' % (id_transaksi, errmsg), 'out')
        q.Next()
      #-- while

      app.ConWriteln('---------------------------')
      app.ConWriteln('Process completed')      
      app.ConRead('Press Enter to continue') 
      
    #-- def
#-- class      
    

class FAUtils(mobject.MObject):
    
  def mobject_init(self):
    self.utils = TreasuryUtils(self.Config)
    self.utils.Helper = self.Helper
  

class TreasuryUtils:
  DICT_KODE_JENIS={
    'D':'TRD', #deposito
    'F':'TRF', #FASBIS
    'S':'TRS', #SIMA
    'B':'TRB', #sukuk
    'R':'TRR', #reverse repo
    'A':'TRA', #reksadana, #reverse repo
    'C':'TRC', #bankaccount
    'H':'TRH'  #hajjaccount
  }
  #raise Exception, DICT_KODE_JENIS
    
  def __init__(self, config):
    self.config = config
    #self.helper = self.Helper
    
  def getCustomId(self,idName,idCode):
    config = self.config
    customid = customidgenAPI.custom_idgen(config)
    customid.PrepareGetID(idName, idCode)
    new_seq = customid.GetLastID()
    customid.Commit()    
    return new_seq
  #--

  def getNextAccountNumber(self,cif):
    idCode = self.DICT_KODE_JENIS[cif]
    new_seq = self.getCustomId('TreasuryAccount',idCode)
    
    norek = '%s%s' % (idCode, str( int(new_seq) ).zfill(10))
    #raise Exception, norek
    return norek
  #--
    
  def get_kode_entri(self,cparam,cgroup):
    row = self.config.CreateSQL("""SELECT KODE_ENTRI FROM %s WHERE CLASSPARAMETER='%s' AND CLASSGROUP='%s' """ % (self.config.MapDBTableName('core.DETILTRANSGROUPCLASS'),cparam,cgroup)).RawResult      
    return row.KODE_ENTRI
    
  def get_tx(self,kode_jenis,c_group):
    row = self.config.CreateSQL("""select kode_tx_class from %s WHERE kode_jenis='%s' and classgroup='%s' """ % (self.config.MapDBTableName('core.DetilTransaksiClass'),kode_jenis,c_group)).RawResult      
    return row.kode_tx_class
    
  # hilangkan jurnal row dengan amount = 0
  def removeZeroAmountFromJournal(self, lsource):
    lcleaned = []
    for row in lsource :
      nilai = row['amount']
      if float(abs(nilai)) > 0.01 :
        lcleaned.append(row)
    return lcleaned
    
  #rounded 2 digit
  def to_decimal(self, val):
    b=Decimal(format(val, '.2f'))
    #output = Decimal(b.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))
    output = round(val,2)
    
    return output
    
  #----  
#--
    
class TreasuryAccount(gHelper.GetPClass("core.RekeningTransaksi")):
    # Static variables
    pobject_classname = 'TreasuryAccount'
    pobject_keys      = ['nomor_rekening']
    
    kode_tx_class = None
        
    dictInitValues = {
      'Saldo' : 0.0, 
      'Saldo_Hari_Lalu' : 0.0,
      'Saldo_POD' : 0.0, 
      'Status_Rekening' : 1,
      'eqv_rate_gross': 0.0, 
      'eqv_rate_real': 0.0, 
      'harga_baru' : 0.0, 
      'harga_beli' : 0.0,
      'jangka_waktu': 1,
      'kolektibilitas': 1,
      'nilai_amortisasi' : 0.0, 
      'nilai_tunai' : 0.0, 
      'nilai_tunai_sekarang' : 0, 
      'nisbah' : 0.0, 
      'nominal_deal' : 0,
      'ppap_umum' : 0.0,
      'ppap_khusus' : 0.0,
      'saldo_accrue': 0.0,
      'accrue_day': 0,
      'saldo_biaya': 0.0, 
      'saldo_margin' : 0.0,
      'ujroh': 0.0,
      'revaluasi': 0.0,
    }
    
    listTransfer =['jangka_waktu*'
      ,'kode_produk=*LProduk.kode_produk' 
      ,'kode_counterpart=*LCounterpart.kode_counterpart' 
      ,'kode_cabang=*LCabang.Kode_Cabang' 
      ,'Kode_Valuta=*LCurrency.Currency_Code'
      ,'tgl_buka*' 
      ,'tgl_jatuh_tempo*' 
      ,'nominal_deal*'
      ,'keterangan*'
      ,'eqv_rate_real*', 'eqv_rate_gross*'
      ,'nisbah*'
      ,'rating_agency=*Llembaga_pemeringkat.refdata_id'
      ,'ranking_value=*Lperingkat_debitur.refdata_id'
      ,'rating_date*'
      ,'jenis_akad*'
      ,'model_baghas*'
      ,'hubungan_bank=*Lhubungan_bank.refdata_id'
      ,'status_nasabah=*Lstatus.refdata_id'
      ,'kategori_portofolio=*LKategori_Portofolio.refdata_id'
      ,'norek_banksrc=*LBankAccount.Nomor_Rekening'
      ,'metode_pengukuran*'                                  
      ,'periode_imbalan*'                                  
      ,'additional_fitur*'                                  
      ,'status_instrumen*'
      ,'negara_lawan=*Lnegara_lawan.refdata_id'
      ,'nama_rekening*'
      , 'nilai_nav*'
    ]
    
    def OnCreate(self, param):
      config = self.Config
      helper = self.Helper
      recSrc = param['recParent']
      
      oRek = helper.GetObject('TreasuryAccount', recSrc.nomor_rekening)
      if not oRek.IsNull:
        fau = helper.GetPClass('TreasuryUtils')(config)
        recSrc.nomor_rekening = fau.getNextAccountNumber(recSrc.jenis_treasuryaccount)
        
      for initField, initValue in self.dictInitValues.iteritems():
        self.SetFieldByName(initField, initValue)
      
      self.ApplyChanges()
      oGRec = atutil.GeneralObject(recSrc)
      
      listTransfer = self.listTransfer
      listTransfer.append('nomor_rekening')
      
      atutil.transferAttributes(helper, listTransfer, self, recSrc)
      self.Config.FlushUpdates()
    #--
    
    def Edit(self, param):
      config = self.Config
      helper = self.Helper      
      recSrc = param['recParent']
      
      listTransfer = self.listTransfer
                          
      atutil.transferAttributes(helper, listTransfer, self, recSrc)
      self.Config.FlushUpdates()
      
    
    def ProcessTransGroupDetails(self, transgroup, detailTrans):
      config = self.Config; mlu = config.ModLibUtils
      helper = self.Helper
      
      if len(detailTrans) == 0:
        return
      
      # default account code selisih
      acc_code_selisih = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultAccountSelisih') 
      
      oProduct = self.LProduk
      if oProduct.IsNull:
        product_code_filter = 'is null'
        product_code = '<undefined>'
      else:
        kode_produk = oProduct.kode_produk  
        kode_produk_filter = '= %s' % mlu.QuotedStr(kode_produk)
        
      dictMap = {}
      for dt in detailTrans:
        if dt.kode_account in (None,''):
          kode_tx_class = dt.kode_tx_class
          kode_account = dictMap.get(kode_tx_class, None)
          if kode_account == None:
            sSQL = '''
                select * from %s
                where kode_produk %s AND kode_interface = %s 
              ''' % (config.MapDBTableName('glinterface'), kode_produk_filter, mlu.QuotedStr(kode_tx_class))
            #raise Exception, config.GetHomeDir()
            #raise Exception, sSQL 
            q = config.CreateSQL(sSQL).RawResult
            if q.Eof:
              #  raise Exception, "GLInterface kode_produk %s dan kode_interface %s tidak ditemukan" % (product_code, kode_tx_class)
              # kode_account = '211070700027' # '103130005040' # temporary default  103130005040
              kode_account = acc_code_selisih
              kode_rc = ''
            else: 
              kode_account = q.kode_account
              if kode_account == None or kode_account == '':
                kode_account = acc_code_selisih
                #kode_account = '211070700027' #'103130005040' # temporary default
              kode_rc = ''
            dictMap[kode_tx_class] = kode_account
          else:
            pass
          
          dt.kode_account = kode_account
          
        if dt.kode_tx_class in (None,'') and dt.keterangan=='Pengakuan Amortisasi Biaya':
          dt.nomor_rekening = '%s-%s-%s' % (dt.kode_account,dt.kode_cabang,dt.kode_valuta)
          dt.ID_DETILTRANSGROUP = None
          dt.kode_jurnal = '11'
      #--
    #--

    def processDetilTransaksiUmum(self, id_group_dtu):
      config = self.Config; mlu = config.ModLibUtils
      helper = self.Helper
      acc_code_selisih = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultAccountSelisih')
      config.FlushUpdates() 
      dbutil.runSQL(config,
        '''
          UPDATE %(detiltransaksiumum)s dtu
          SET 
            (kode_account, rc_code) = 
            (
              SELECT 
                coalesce(gli.kode_account, %(acc_code_selisih)s) as kode_account, 
                kode_rc 
              FROM 
                %(glinterface)s gli, %(finaccount)s fa
              WHERE
                gli.kode_produk = fa.product_code AND
                fa.nomor_rekening = %(nomor_rekening)s AND
                gli.kode_interface = dtu.kode_tx_class
            )
          WHERE
            dtu.id_group = %(id_group)d AND (dtu.override_account is NULL or dtu.override_account = 'F') 
        ''' % {
          'detiltransaksiumum': config.MapDBTableName('core.detiltransaksiumum'),
          'acc_code_selisih': mlu.QuotedStr(acc_code_selisih),
          'glinterface': config.MapDBTableName('glinterface'),
          'treasuryaccount': config.MapDBTableName('treasuryaccount'),
          'nomor_rekening': mlu.QuotedStr(self.nomor_rekening),
          'id_group': int(id_group_dtu)   
        }
      )
      dbutil.runSQL(config,
        '''
          UPDATE %(detiltransaksiumum)s dtu
          SET
            kode_cabang = %(kode_cabang)s, kode_valuta = %(kode_valuta)s 
          WHERE
            dtu.id_group = %(id_group)d AND (dtu.override_cabang is NULL or dtu.override_cabang = 'F') 
        ''' % {
          'detiltransaksiumum': config.MapDBTableName('core.detiltransaksiumum'),
          'kode_cabang': mlu.QuotedStr(self.kode_cabang),
          'kode_valuta': mlu.QuotedStr(self.kode_valuta),
          'id_group': int(id_group_dtu)   
        }
      )
      config.ResetCache()
      
      pass
    #-- def processDetilTransaksiUmum
 
    def getReverseAmount(self, parTxn):
      config = self.Config
      fau = FinAccountUtils(config)
      tx_payment = fau.get_tx(self.kode_jenis, 'pembayaran')
      transaction_amount = 0
      for i in range(0, parTxn.RecordCount):
        rec_txn = parTxn.GetRecord(i)
        if rec_txn.kode_tx_class in [tx_payment, '']:
          transaction_amount += rec_txn.nilai_mutasi
      return transaction_amount
    #--
    
    def ReverseTxn(self, parameters):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
            
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_trx = periodHelper.GetAccountingDate()
      
      # CREATE TRANSACTION
      rec = parameters.uipData.GetRecord(0)
      parTxn = parameters.uipTxn
      kode_entri = 'FRVS01' 
      keterangan = 'Rev. Txn %s - %s' % (rec.jenis_transaksi,rec.jurnal_no)

      oTx = FinAccount.initTransaksi(self, kode_entri, tgl_trx, keterangan)

      kode_jenis = self.kode_jenis
      fau = FinAccountUtils(config)
      tx_payment = fau.get_tx(kode_jenis, 'pembayaran')
      tx_arear = fau.get_tx(kode_jenis, 'tunggakan')
      entries = []
      tx_class = ''
      is_arrear = 'F'
      for i in range(0, parTxn.RecordCount):
        rec_txn = parTxn.GetRecord(i)
        _sign = 'D' if rec_txn.jenis_mutasi=='C' else 'C'
        tx_class = rec_txn.kode_tx_class

        if tx_class == tx_arear:
          is_arrear = 'T'
           
        if tx_class not in [tx_payment, '']:
          entries.append({'sign': _sign, 'amount': rec_txn.nilai_mutasi, 'kode_tx_class': tx_class, 'kode_account':rec_txn.kode_account})
          #rev detil biaya di peragaan
          if rec.kode_transaksi == 'FAC01':
             self.ReverseBiaya(rec_txn)
        elif tx_class =='':
          dtrx = self.Helper.GetObject('core.RekeningLiabilitas', rec_txn.nomor_rekening)
          if dtrx.IsNull:
            entries.append({'sign': _sign, 'amount': rec_txn.nilai_mutasi, 'kode_tx_class': None, 'kode_account':rec_txn.kode_account})
          else:
            oDT = dtrx.BuatTransaksiManual('core.DetilTransaksi', _sign, rec_txn.nilai_mutasi);
            oDT.LTransaksi = oTx
            oDT.keterangan = 'Rev. Txn %s ' % rec_txn.keterangan
            oDT.tanggal_transaksi = oTx.tanggal_transaksi
            oDT.kode_jurnal = '11'
            oDT.Proses()
          
        else:
          #create detil transaksi rekening liability
          LPaymentSrc = self.LPaymentSrc
          if LPaymentSrc.IsNull:
            raise Exception, "Rekening sumber pembayaran tidak ada.."
          oDT = LPaymentSrc.BuatTransaksiManual('core.DetilTransaksi', 'C', rec_txn.nilai_mutasi);
          oDT.LTransaksi = oTx
          oDT.keterangan = oTx.keterangan
          oDT.tanggal_transaksi = oTx.tanggal_transaksi
          oDT.kode_jurnal = '11'
          oDT.Proses()
                       
      entries = fau.removeZeroAmountFromJournal(entries)
      #raise Exception, entries
      oDetTxGroupCost = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupCost.ProcessDetails()
      #--
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      oSced = self.LActiveSchedule
      if not oSced.IsNull:
        #raise Exception,'QQ'
        oSced.id_otorentri = self.id_otorentri
        oSced.ReverseSchedule(rec, is_arrear)
      
      oTx.is_reverse_allowed='F'
      #reverse
      revTx = self.Helper.GetObject('core.Transaksi', rec.id_transaksi)
      if revTx.IsNull:
        revTx = self.Helper.GetObject('core.HistTransaksi', rec.id_transaksi)
        
      revTx.is_reversed = 'T'
      revTx.is_reverse_allowed='F'
      #raise Exception,'OK__'
      return oDetTxGroupCost 
    #--

    #upddate by BG set user input dan user otorisasi #20150216 
    def initTransaksiEOD(self, kode_entri, tgl_transaksi, keterangan):
      helper = self.Helper ; config = self.Config      
      # CREATE TRANSACTION 
      oTx = helper.CreatePObject("core.Transaksi", kode_entri)
      oTx.SetStandardInfo(tgl_transaksi, keterangan)
      oTx.user_input     = 'sysFIN'
      oTx.SetStatusOtorisasi(1)     
      oTx.user_otorisasi = 'sysFIN'
      
      return oTx 

    #upddate by BG set user input dan user otorisasi #20150216 
    def initTransaksi(self, kode_entri, tgl_transaksi, keterangan):
      helper = self.Helper ; config = self.Config      
      # CREATE TRANSACTION 
      oTx = helper.CreatePObject("core.Transaksi", kode_entri)
      oTx.SetStandardInfo(tgl_transaksi, keterangan)
      if self.id_otorentri>0: 
        Otr = helper.GetObject('enterprise.OtorEntri', self.id_otorentri)
        oTx.terminal_input = Otr.terminal_input    
        oTx.tanggal_input  = Otr.tgl_input    
        oTx.jam_input      = Otr.tgl_input    
        oTx.user_input     = Otr.user_input
      
      oTx.SetStatusOtorisasi(1)
      return oTx
      
    def initTransaksiRekLiability(self, oTx, mutasi, amount):
      helper = self.Helper
      config = self.Config
      
      no_account = helper.GetObject('ParameterGlobalTreasury', 'DEF_REKENING').nilai_parameter_string 
      ldestacc = helper.GetObject('core.RekeningTransaksi', no_account)
        
      oDT = ldestacc.BuatTransaksiManual('core.DetilTransaksi', mutasi, amount)
      oDT.LTransaksi = oTx
      oDT.keterangan = oTx.keterangan
      oDT.tanggal_transaksi = oTx.tanggal_transaksi
      oDT.Proses() 

    #close Treasury
    def closeTreasury (self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      recA = params['recAkad']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TRCLOSE'
      keterangan = 'PENUTUPAN ACCOUNT - %s' % (self.nomor_rekening)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'penutupan')
      
      #update adjustment pendapatan Fasbis/Sbis
      if self.jenis_treasuryaccount in ['F']:
        self.adjustment_pendapatan  = recA.adjustment_pendapatan
      
      if self.jenis_treasuryaccount in ['F', 'D']:  
        nominal_deal  = self.nominal_deal 
        nom_ujroh     = recA.ujroh
        amount        = nominal_deal+nom_ujroh
        mutasi        = 'D'
        
        if self.jenis_treasuryaccount in ['D']:
          amount      = nominal_deal
        
        if recA.tipe_treasury in ['B']:
          mutasi = 'C'
          
        #create detil transaksi rekening liability  
        TreasuryAccount.initTransaksiRekLiability(self, oTx, mutasi, amount)
      #--endif
      
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      #update tgl tutup dan status rekening
      self.closing_date = tgl_transaksi
      self.tgl_tutup = tgl_transaksi
      self.Status_Rekening = 3
      
      return oDetTx
    #----   
      
    #close Treasury
    def getJenisTreasury(self):
      helper = self.Helper
      config = self.Config
      row = config.CreateSQL("""SELECT * FROM jenistreasury WHERE jenis_treasuryaccount='%s' """ % (self.jenis_treasuryaccount)).RawResult 
      
      return row.keterangan     
    #----    
                                                     
    #journal trx 
    def getTrxJournal(self,param,jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = FinAccountUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      lgr = []
      if jns_jurnal =='finish_AYDA': # transaksi penyelesaian AYDA
        ayda_balance = param.AYDA_balance
        nilai_jual_aktiva = param.nilai_jual_aktiva
        laba_ayda = nilai_jual_aktiva + ayda_balance
        s_sign='C'; s_kj='ayda_laba' 
        if laba_ayda < 0:
          s_sign='D'; s_kj='ayda_biaya'
          laba_ayda = -laba_ayda
        
        #journal input ayda
        lgr += [
        #reverse ayda balance
          {'sign': s_sign, 'amount': laba_ayda, 'kode_tx_class':  fau.get_tx(kode_jenis,s_kj ) },
          {'sign': 'C', 'amount': -ayda_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_aktiva' ) },
        #reverse PPANP
          {'sign': 'D', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_cadangan' ) },
          {'sign': 'C', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdp_rev_ppap' ) }
        ]

      elif jns_jurnal =='ppanp_AYDA': #transaksi pencadangan AYDA
        lgr += [
        #create PPANP
          {'sign': 'D', 'amount': -(self.ayda_balance+self.reserved_loss_balance), 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_by_cadangan' ) },
          {'sign': 'C', 'amount': -(self.ayda_balance+self.reserved_loss_balance), 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_cadangan' ) }
        ]

      elif jns_jurnal =='delete_AYDA': #transaksi pencadangan AYDA
        lgr += [
        #delete AYDA
          {'sign': 'D', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_cadangan' ) },
          {'sign': 'C', 'amount': -self.ayda_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_aktiva' ) },
          {'sign': 'C', 'amount': self.ayda_balance+self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_by_cadangan' ) }
        ]

      elif jns_jurnal =='penalty_process': # transaksi denda ta'wid & ta'zir        
        lgr += [
          {'sign': 'C', 'amount': param.penalty_tawidh, 'kode_tx_class':  fau.get_tx(kode_jenis,'denda_tawidh' ) },
          {'sign': 'C', 'amount': param.penalty_tazir, 'kode_tx_class':  fau.get_tx(kode_jenis,'denda_tazir' ) }
        ]
      elif jns_jurnal =='recovery_wo': # transaksi recovery_wo       
        lgr += [
          {'sign': 'C', 'amount': param.recovery_pokok, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off' ) },
          {'sign': 'C', 'amount': param.recovery_margin, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off_margin' ) }
        ]

      elif jns_jurnal =='hapus_tagih': # transaksi hapus_tagih       
        lgr += [
          {'sign': 'C', 'amount': -self.write_off_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off' ) },
          {'sign': 'C', 'amount': -self.write_off_margin_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off_margin' ) }
        ]

      elif jns_jurnal =='eff_amortize': # transaksi denda amortisasi sisa biaya        
        lgr += [
          {'sign': 'D', 'amount': param, 'kode_tx_class':  fau.get_tx(kode_jenis,'deferred_balance' ) }
        ]

      elif jns_jurnal=='reverse_PPAP':
        #checking ppap yang terbentuk umum atau khusus
        pdp_codes='pdp_rev_ppap_umum' if (self.reserved_common_balance>0.01 or self.reserved_loss_balance==0) else 'pdp_rev_ppap'
        lgr += [
        #rerevse PPAP
          {'sign': 'D', 'amount': self.reserved_common_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum' ) }
          ,{'sign': 'C', 'amount': self.reserved_common_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdp_rev_ppap_umum' ) }
          ,{'sign': 'D', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus' ) }
          ,{'sign': 'C', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdp_rev_ppap' ) }
        ]
        
      #raise Exception,lgr
      entries = fau.removeZeroAmountFromJournal(lgr)
      return entries
    #----
#--

#Bank Account    
class BankAccount(TreasuryAccount):
    # Static variables
    pobject_classname = 'BankAccount'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '37'
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper 
      self.kode_jenis = 'TRC'
      TreasuryAccount.OnCreate(self, param)

      config.FlushUpdates()

    def TrxBankAccount(self, param): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      helper = self.Helper                                                                                               
      config = self.Config
      mlu = config.ModLibUtils
      rec = param['rec']
      rec2 = param['rec2']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      kode_entri = rec.kode_entri
      keterangan = rec.keterangan

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      
      entries = self.getTrxJournal(param, 'trx_bank')
      #raise Exception, entries
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
            
      return oTx
    #--
      
    def TrxBankAccountRekLawan(self, param, oTx):
      rec = param['rec']
      rec2 = param['rec2']
      #raise Exception, oTx.tanggal_transaksi
      if rec.rek_lawan=='B': #bank account
        kode_entri = rec.kode_entri
        keterangan = rec.keterangan
        entries = self.getTrxJournal(param, 'trx_bank_lawan')
        #raise Exception, entries
        oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
        oDetTx.ProcessDetails()

      elif rec.rek_lawan=='L': #rek liabilities
        j_mutasi = 'C' if rec.jenis_mutasi=='D' else 'D' 
        dtrx = self.Helper.GetObject('core.RekeningLiabilitas', rec2.nomor_rekening_liab)
        oDT = dtrx.BuatTransaksiManual('core.DetilTransaksi', j_mutasi, rec.nominal)
        oDT.LTransaksi = oTx
        oDT.keterangan = rec.keterangan
        oDT.tanggal_transaksi = oTx.tanggal_transaksi
        oDT.kode_jurnal = '11'
        oDT.Proses()

      elif rec.rek_lawan=='G': #GL account
        j_mutasi = 'C' if rec.jenis_mutasi=='D' else 'D'
        dGLAcc = {
          'no_gl' : rec2.GetFieldByName('Kode_GL.account_code'),
          'cabang' : rec2.GetFieldByName('kode_cabang_gl.Kode_Cabang'), 
          'valuta' : 'IDR'
        }
        norek_gl = "%(no_gl)s-%(cabang)s-%(valuta)s" % dGLAcc
        #raise Exception, norek_gl
        dtrx = self.Helper.GetObject('core.GLAccount', norek_gl)
        oDT = dtrx.BuatTransaksiManual('core.DetilTransaksi', j_mutasi, rec.nominal)
        oDT.LTransaksi = oTx
        oDT.keterangan = rec.keterangan
        oDT.tanggal_transaksi = oTx.tanggal_transaksi
        oDT.kode_jurnal = '11'
        oDT.Proses()
    #--
    
    def TrxBankAccountJournal(self, oTx): 
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)      
    #--

    def TrxBankAccount(self, rec, oTx):
      kode_entri = rec.kode_entri
      keterangan = rec.keterangan
      entries = self.getTrxJournal(rec, 'trx_bank_2')
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()

    #bankaccount
    def getTrxJournal(self, param, jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = TreasuryUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal =='trx_bank': 
        rec = param['rec']
        nominal_trx  = rec.nominal
          
        lgr += [
            {'sign': rec.jenis_mutasi, 'amount': nominal_trx, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
        ]
 
      elif jns_jurnal =='trx_bank_2': 
        nominal_trx  = param.nominal_beli
          
        lgr += [
            {'sign': param.jenis_mutasi, 'amount': nominal_trx, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
        ]
 
      elif jns_jurnal =='trx_bank_lawan': 
        rec = param['rec']
        j_mutasi = 'C' if rec.jenis_mutasi=='D' else 'D'
        lgr += [
            {'sign': j_mutasi, 'amount': rec.nominal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
        ]
        
                  
      entries = fau.removeZeroAmountFromJournal(lgr)
      return entries
#--

#Hajj Account    
class HajjAccount(TreasuryAccount):
    # Static variables
    pobject_classname = 'HajjAccount'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '38'
    
    dictInitValues = {
      'Saldo_lunas' : 0.0, 
      'Saldo_tunda' : 0.0, 
      'Saldo_manfaat' : 0.0, 
    }
    
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    lstransfer =[ 'nama_ibu_kandung*'
      , 'tempat_lahir*'
      , 'tanggal_lahir*'
      , 'jenis_kelamin*'
      , 'jenis_identitas*'
      , 'nomor_identitas*'
      , 'nomor_hp*'
      , 'email*'
      , 'alamat_rumah*'
      , 'alamat_rt*'
      , 'alamat_rw*'
      , 'alamat_kelurahan*'
      , 'alamat_kecamatan*'
      , 'alamat_kota*'
      , 'alamat_provinsi*'
      , 'alamat_kodepos*'
    ]
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper 
      lstransfer = self.lstransfer
      self.kode_jenis = 'TRH'
      TreasuryAccount.OnCreate(self, param)

      for initField, initValue in self.dictInitValues.iteritems():
        self.SetFieldByName(initField, initValue)

      recSrc = param['recParent']      
      atutil.transferAttributes(helper, lstransfer, self, recSrc)          
      config.FlushUpdates()
    #---
    
    def TrxBankAccount(self, param): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      helper = self.Helper                                                                                               
      config = self.Config
      mlu = config.ModLibUtils
      rec = param['rec']
      rec2 = param['rec2']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      kode_entri = rec.kode_entri
      keterangan = rec.keterangan

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      
      entries = self.getTrxJournal(param, 'trx_bank')
      #raise Exception, entries
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
            
      return oTx
    #--
      
    def TrxBankAccountRekLawan(self, param, oTx):
      rec = param['rec']
      rec2 = param['rec2']
      #raise Exception, oTx.tanggal_transaksi
      if rec.rek_lawan=='B': #bank account
        kode_entri = rec.kode_entri
        keterangan = rec.keterangan
        entries = self.getTrxJournal(param, 'trx_bank_lawan')
        #raise Exception, entries
        oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
        oDetTx.ProcessDetails()

      elif rec.rek_lawan=='L': #rek liabilities
        j_mutasi = 'C' if rec.jenis_mutasi=='D' else 'D' 
        dtrx = self.Helper.GetObject('core.RekeningLiabilitas', rec2.nomor_rekening_liab)
        oDT = dtrx.BuatTransaksiManual('core.DetilTransaksi', j_mutasi, rec.nominal)
        oDT.LTransaksi = oTx
        oDT.keterangan = rec.keterangan
        oDT.tanggal_transaksi = oTx.tanggal_transaksi
        oDT.kode_jurnal = '11'
        oDT.Proses()

      elif rec.rek_lawan=='G': #GL account
        j_mutasi = 'C' if rec.jenis_mutasi=='D' else 'D'
        dGLAcc = {
          'no_gl' : rec2.GetFieldByName('Kode_GL.account_code'),
          'cabang' : rec2.GetFieldByName('kode_cabang_gl.Kode_Cabang'), 
          'valuta' : 'IDR'
        }
        norek_gl = "%(no_gl)s-%(cabang)s-%(valuta)s" % dGLAcc
        #raise Exception, norek_gl
        dtrx = self.Helper.GetObject('core.GLAccount', norek_gl)
        oDT = dtrx.BuatTransaksiManual('core.DetilTransaksi', j_mutasi, rec.nominal)
        oDT.LTransaksi = oTx
        oDT.keterangan = rec.keterangan
        oDT.tanggal_transaksi = oTx.tanggal_transaksi
        oDT.kode_jurnal = '11'
        oDT.Proses()
    #--
    
    def TrxBankAccountJournal(self, oTx): 
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)      
    #--

    #bankaccount
    def getTrxJournal(self, param, jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = TreasuryUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal =='trx_sa': #setor_awal 
        rec = param['rec']
        nominal_trx  = rec.nominal
          
        lgr += [
            {'sign': 'C', 'amount': nominal_trx, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
        ]
 
      elif jns_jurnal =='trx_sl': #setor_lunas
        rec = param['rec']
        nominal_trx  = rec.nominal
          
        lgr += [
            {'sign': 'C', 'amount': nominal_trx, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_lunas')}
        ]
 
      elif jns_jurnal =='trx_nm': #setor_lunas
        rec = param['rec']
        nominal_trx  = rec.nominal
          
        lgr += [
            {'sign': 'C', 'amount': nominal_trx, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_mannfaat')}
        ]
 
      elif jns_jurnal =='trx_tunda': #utang tunda 
        lgr += [
            {'sign': 'D', 'amount': self.saldo, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
            {'sign': 'D', 'amount': self.saldo_lunas, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_lunas')},
            {'sign': 'C', 'amount': self.saldo+self.saldo_lunas, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_tunda')}
        ]
        
                  
      entries = fau.removeZeroAmountFromJournal(lgr)
      return entries
#--

#Akad FASBIS / SBIS    
class TreaFasbis(TreasuryAccount):
    # Static variables
    pobject_classname = 'TreaFasbis'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '32'
    
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    lstransfer =[ 'tipe_baghas*'
        , 'tipe_fasbis*' 
        , 'kolektibilitas*'
        , 'ujroh*'
        , 'saldo_accrue*'
        , 'accrue_day*'
        , 'adjustment_pendapatan*'
        , 'nomor_ref_deal*' 
    ]
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper 
      lstransfer = self.lstransfer
      self.kode_jenis = 'TRF'
      TreasuryAccount.OnCreate(self, param)
      recSrc = param['recAkad']      
      atutil.transferAttributes(helper, lstransfer, self, recSrc)          
      config.FlushUpdates()
  
    def doSave(self, param, lstransfer):
      config = self.Config; helper = self.Helper
      recSrc = param['recAkad']
      TreasuryAccount.Edit(self, param)      
      atutil.transferAttributes(helper, lstransfer, self, recSrc)          
      config.FlushUpdates()
    #--       

    def Placing(self, param): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      helper = self.Helper                                                                                               
      config = self.Config
      mlu = config.ModLibUtils
      recP = param['recParent']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      kode_entri = recP.kode_entri  
      keterangan = 'PENEMPATAN FASBIS %s' % self.nomor_rekening

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      entries = self.getTrxJournal(param, 'penempatan')
      
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTx
    #--
    
    #Edit Account Treasury
    def editTreasury (self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
           
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TREDIT'
      keterangan = 'EDIT ACCOUNT FASBIS/SBIS - %s' % (self.nomor_rekening)
      
      #simpan perubahan
      lstransfer = self.lstransfer
      self.doSave(params, lstransfer)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'edit')

      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTx
    #---- 
    
    #fasbis
    def getTrxJournal(self, param, jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = TreasuryUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal =='penempatan': # penempatan fasbis
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = recP.nominal_deal 
        nom_ujroh     = recA.ujroh
        nom_akru      = recA.saldo_accrue
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
        
        if recA.kolektibilitas in [1]:
          nom_ppap_umum = recA.nom_ppap
        else:
          nom_ppap_khusus = recA.nom_ppap
          
        ## mengurangi pokok piutang
        lgr += [
            {'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
            {'sign': 'C', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
        ]
        
        # jurnal ckpn jika ppap > 0
        if nom_ppap_umum > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
              {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')}
          ]
        
        if nom_ppap_khusus > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
              {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')}
          ]

        # jurnal akru jika ppap > 0
        if nom_akru > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_akru, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_pymad')},
              {'sign': 'C', 'amount': nom_akru, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_accrue')}
          ]

      elif jns_jurnal =='penutupan': # penutupan fasbis     
        recA = param['recAkad']
        nominal_deal  = self.nominal_deal 
        nom_ujroh     = recA.ujroh
        nom_akru      = -self.saldo_accrue
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0 
        
        ## mengurangi pokok piutang
        lgr += [
            #{'sign': 'D', 'amount': nominal_deal+nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
            {'sign': 'C', 'amount': nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_profit')},
            {'sign': 'C', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
        ]
        
        # jurnal ckpn jika ppap > 0
        if nom_ppap_umum > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
              {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_umum')},
          ]

        if nom_ppap_khusus > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
              {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_khusus')},
          ]

        # jurnal reverse akru jika ppap > 0
        if nom_akru > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_akru, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_accrue')},
              {'sign': 'C', 'amount': nom_akru, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_pymad')},
          ]
            
      elif jns_jurnal =='edit': # edit fasbis     
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = self.nominal_deal
        nom_ujroh     = self.ujroh
        nom_akru      = -self.saldo_accrue
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
        
        ## Selisih perubahan data
        selisih_deal  = recP.nominal_deal-self.nominal_deal
        selisih_ujroh = recA.ujroh-self.ujroh 
        
        if selisih_deal >= 0: 
          lgr += [
              {'sign': 'D', 'amount': selisih_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              {'sign': 'C', 'amount': selisih_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
          ]
          
        if selisih_deal < 0:
          lgr += [
              {'sign': 'D', 'amount': selisih_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              {'sign': 'C', 'amount': selisih_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
          ]
                  
      entries = fau.removeZeroAmountFromJournal(lgr)
      return entries
      
#Akad Deposito Antar Bank   
class TreaDeposito(TreasuryAccount):
    # Static variables
    pobject_classname = 'TreaDeposito'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '31'
    
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    lstransfer =[ 'nomor_bilyet*'
          , 'is_ARO*' 
          , 'ujroh*' 
          , 'nilai_tunai*'
          , 'kolektibilitas*'
          , 'jenis_operasional=*Ljenis_operasional.refdata_id'
          , 'sandi_bank=*LSandi_Bank.refdata_id'
    ]
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      self.kode_jenis = 'TRD'
      TreasuryAccount.OnCreate(self, param)
      recSrc = param['recAkad']      
      atutil.transferAttributes(helper, self.lstransfer, self, recSrc)          
      config.FlushUpdates()
    
    def doSave(self, param, lstransfer):
      config = self.Config; helper = self.Helper
      TreasuryAccount.Edit(self, param)
      recSrc = param['recAkad']      
      atutil.transferAttributes(helper, self.lstransfer, self, recSrc)          
      config.FlushUpdates()
    #--       

    def Placing(self, param): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      helper = self.Helper                                                                                               
      config = self.Config
      mlu = config.ModLibUtils
      recP = param['recParent']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      kode_entri = recP.kode_entri
      amount = recP.nominal_deal
      keterangan = 'PENEMPATAN DEPOSITO ANTAR BANK %s' % self.nomor_rekening

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      entries = self.getTrxJournal(param, 'penempatan')      
      #create detil transaksi rekening liability  
      #TreasuryAccount.initTransaksiRekLiability(self, oTx, 'C', amount)      
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      oBank = helper.GetObject("BankAccount", recP.GetFieldByName('LBankAccount.Nomor_Rekening'))
      class ParRecord(object):pass        
      _parPayment = ParRecord()
      _parPayment.nominal_beli = amount        
      _parPayment.kode_entri = kode_entri        
      _parPayment.keterangan = keterangan        
      _parPayment.jenis_mutasi = 'C'
      
      oBank.TrxBankAccount(_parPayment, oTx) 
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTx
    #--
    
    def Borrowing(self, param): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      helper = self.Helper                                                                                               
      config = self.Config
      mlu = config.ModLibUtils
      recP = param['recParent']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      kode_entri = recP.kode_entri
      amount = recP.nominal_deal
      keterangan = 'PENERBITAN DEPOSITO ANTAR BANK %s' % self.nomor_rekening

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      entries = self.getTrxJournal(param, 'penerbitan')
      
      #create detil transaksi rekening liability  
      TreasuryAccount.initTransaksiRekLiability(self, oTx, 'D', amount)

      #raise Exception, entries
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTx
    #--
    
    def editTreasury (self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      recP = params['recParent']
           
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TREDIT' 
      keterangan = 'EDIT ACCOUNT DEPOSITO - %s' % (self.nomor_rekening)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'edit')
      #raise Exception, entries
      
      
      # selisih perubahan nominal deal
      amount  = recP.nominal_deal-self.nominal_deal
      
      #create detil transaksi rekening liability  
      TreasuryAccount.initTransaksiRekLiability(self, oTx, 'C', amount)

      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      #simpan perubahan
      self.doSave(params, self.lstransfer)
      
      return oDetTx

    def paybahasTreasury (self, params):
      helper = self.Helper
      config = self.Config
      recA = params['recAkad']
      mlu = config.ModLibUtils
           
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TRPAYBAHAS'
      keterangan = 'PEMBAYARAN BAGI HASIL DEPOSITO - %s' % (self.nomor_rekening)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'paybahas')
      #raise Exception, entries

      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      # selisih pembayaran bagi hasil
      nom_baghas  = recA.amount-recA.saldo_margin
      
      #create detil transaksi rekening liability  
      TreasuryAccount.initTransaksiRekLiability(self, oTx, 'D', nom_baghas)
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      #simpan perubahan
      self.doSave(params, self.lstransfer)
      
      return oDetTx
      
    
    #deposito
    def getTrxJournal(self, param, jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = TreasuryUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal =='penempatan': # penempatan deposito antar bank
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = recP.nominal_deal 
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
        
        if recA.kolektibilitas in [1]:
          nom_ppap_umum = recA.nom_ppap
        else:
          nom_ppap_khusus = recA.nom_ppap
        
        ## mengurangi pokok piutang
        if nominal_deal > 0:
          lgr += [
              {'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              #{'sign': 'C', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
          ]
        
        # jurnal ckpn jika ppap > 0
        if nom_ppap_umum > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
              {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
          ]
        
        if nom_ppap_khusus > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
              {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
          ]

      elif jns_jurnal =='penutupan': #penutupan deposito antar bank     
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = recP.nominal_deal 
        nom_ujroh     = recA.ujroh-recA.saldo_margin
        nom_ppap_umum = self.ppap_umum
        nom_ppap_khusus = self.ppap_khusus
        
        ## mengurangi pokok piutang
        if recA.tipe_treasury in ['P']:
          lgr += [       
              {'sign': 'C', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              #{'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},            
              #{'sign': 'C', 'amount': nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_profit')}
          ]
        else:
          lgr += [       
              {'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal_brw')},
              #{'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},            
              #{'sign': 'C', 'amount': nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_profit')}
          ]
        
        # jurnal ckpn jika ppap > 0
        if nom_ppap_umum > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
              {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_umum')},
          ]

        if nom_ppap_khusus > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
              {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_khusus')},
          ]
           
      elif jns_jurnal =='edit': # edit deposito     
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = self.nominal_deal
        nom_ujroh     = self.ujroh        
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
        
        if recA.kolektibilitas in [1]:
          nom_ppap_umum = recA.nom_ppap
        else:
          nom_ppap_khusus = recA.nom_ppap   
        
        ## Selisih perubahan data
        selisih_deal  = recP.nominal_deal-nominal_deal
        selisih_ujroh = recA.ujroh-nom_ujroh
        selisih_ppap_umum = nom_ppap_umum-self.ppap_umum
        selisih_ppap_khusus = nom_ppap_khusus-self.ppap_khusus     
        
        ## mengurangi pokok piutang
        if selisih_deal > 0: 
          lgr += [
              {'sign': 'D', 'amount': selisih_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              #{'sign': 'C', 'amount': selisih_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
          ]
          
        if selisih_deal < 0:
          lgr += [
              {'sign': 'D', 'amount': abs(selisih_deal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
              #{'sign': 'C', 'amount': abs(selisih_deal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
          ]
      
        if recA.kolektibilitas in [1]:
          if nom_ppap_khusus > 0:
            lgr += [
                {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
                {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_khusus')},
            ]
            
          if selisih_ppap_umum > 0:
            lgr += [
                {'sign': 'D', 'amount': selisih_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
                {'sign': 'C', 'amount': selisih_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
            ]
          else:
            lgr += [
                {'sign': 'D', 'amount': abs(selisih_ppap_umum), 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
                {'sign': 'C', 'amount': abs(selisih_ppap_umum), 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
            ]         
        
        else :    
          if nom_ppap_umum > 0:
            lgr += [
                {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
                {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_umum')},
            ]
  
          if selisih_ppap_khusus > 0:
            lgr += [
                {'sign': 'D', 'amount': selisih_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
                {'sign': 'C', 'amount': selisih_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
            ]
          else:
            lgr += [
                {'sign': 'D', 'amount': abs(selisih_ppap_khusus), 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
                {'sign': 'C', 'amount': abs(selisih_ppap_khusus), 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
            ]
            
        
      elif jns_jurnal =='paybahas': # bayar bagi hasil deposito     
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = self.nominal_deal
        nom_ujroh     = self.ujroh
        saldo_ujroh   = self.saldo_margin
        
        
        ## selisih pembayaran bagi hasil
        nom_baghas  = recA.amount-saldo_ujroh
        
        ## mengurangi pokok piutang
        if nom_baghas >= 0: 
          lgr += [
              #{'sign': 'D', 'amount': nom_baghas, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
              {'sign': 'C', 'amount': nom_baghas, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_profit')}
          ]
        #raise Exception, lgr     
      # -- endif placing
      
      elif jns_jurnal =='penerbitan': #penerbitan deposito     
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = recP.nominal_deal 
        nom_ujroh     = recA.ujroh
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
              
        if recA.kolektibilitas in [1]:
          nom_ppap_umum = recA.nom_ppap
        else:
          nom_ppap_khusus = recA.nom_ppap
        
                
        ## mengurangi pokok piutang
        if nominal_deal > 0:
          lgr += [
              #{'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
              {'sign': 'C', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal_brw')}
          ]
        
        # jurnal ckpn jika ppap > 0
        if nom_ppap_umum > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
              {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
          ]
        
        if nom_ppap_khusus > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
              {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
          ]
          
      entries = fau.removeZeroAmountFromJournal(lgr)
      return entries

#Akad SIMA   
class TreaSima(TreasuryAccount):
    # Static variables
    pobject_classname = 'TreaSima'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '33'
    
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    lstransfer =[ 'nomor_sertifikat*' 
          , 'ujroh*' 
          , 'jenis_sima*'
          , 'tgl_bayar_baghas*'
          , 'underlying_asset*'
          , 'kolektibilitas*'
          , 'saldo_baghas*'    
          , 'golongan_nasabah=*LGolongan_Nasabah.refdata_id'
    ]
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      self.kode_jenis = 'TRS'
      TreasuryAccount.OnCreate(self, param)
      recSrc = param['recAkad']       
      atutil.transferAttributes(helper, self.lstransfer, self, recSrc)          
      config.FlushUpdates()
    
    def doSave(self, param, lstransfer):
      config = self.Config; helper = self.Helper
      TreasuryAccount.Edit(self, param)
      recSrc = param['recAkad']      
      atutil.transferAttributes(helper, self.lstransfer, self, recSrc)          
      config.FlushUpdates()
    #--       

    def Placing(self, param): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      helper = self.Helper                                                                                               
      config = self.Config
      mlu = config.ModLibUtils
      recP = param['recParent']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      kode_entri = recP.kode_entri
      keterangan = 'PENEMPATAN SIMA %s' % self.nomor_rekening

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      #app.ConWriteln(jns_ayda)
      entries = self.getTrxJournal(param, 'penempatan')

      #raise Exception, entries
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTx
    #--
    
    def Borrowing(self, param): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      helper = self.Helper                                                                                               
      config = self.Config
      mlu = config.ModLibUtils
      recP = param['recParent']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      kode_entri = recP.kode_entri
      keterangan = 'PENERBITAN SIMA %s' % self.nomor_rekening

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      #app.ConWriteln(jns_ayda)
      entries = self.getTrxJournal(param, 'penerbitan')

      #raise Exception, entries
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTx
    #--
    
    def editTreasury (self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
           
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TREDIT'
      keterangan = 'EDIT ACCOUNT SIMA - %s' % (self.nomor_rekening)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'edit')
      #raise Exception, entries

      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      #simpan perubahan
      self.doSave(params, self.lstransfer)
      
      return oDetTx
          
    def paybahasTreasury (self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
           
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TRPAYBAHAS'
      keterangan = 'PEMBAYARAN BAGI HASIL SIMA - %s' % (self.nomor_rekening)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'paybahas')
      #raise Exception, entries

      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      #simpan perubahan
      self.doSave(params, self.lstransfer)
      
      return oDetTx
      
    
    #sima
    def getTrxJournal(self, param, jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = TreasuryUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal =='penempatan': # penempatan sima
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = recP.nominal_deal 
        nom_ujroh     = recA.ujroh
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
        
        if recA.kolektibilitas in [1]:
          nom_ppap_umum = recA.nom_ppap
        else:
          nom_ppap_khusus = recA.nom_ppap
        
        ## mengurangi pokok piutang
        if nominal_deal > 0:
          lgr += [
              {'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              {'sign': 'C', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
          ]
        
        # jurnal ckpn jika ppap > 0
        if nom_ppap_umum > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
              {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
          ]
        
        if nom_ppap_khusus > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
              {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
          ]

      elif jns_jurnal =='penutupan': # penutupan sima
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = recP.nominal_deal
        nom_ppap_umum = self.ppap_umum
        nom_ppap_khusus = self.ppap_khusus
        
         
        ## mengurangi pokok piutang
        if recA.tipe_treasury in ['P']:
          #Sima Placing
          nom_ujroh     = recA.ujroh-self.saldo_margin
          lgr += [
              {'sign': 'D', 'amount': nominal_deal+nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
              {'sign': 'C', 'amount': nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_profit')},
              {'sign': 'C', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
          ]
        else:
          #SIMA Borrowing
          saldo_accrue  = self.saldo_accrue
          saldo_biaya   = self.saldo_biaya  
          nom_ujroh     = recA.ujroh+self.saldo_biaya
          
          lgr += [
              {'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              {'sign': 'D', 'amount': nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_biaya')},
              {'sign': 'D', 'amount': saldo_accrue, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_accrue')},
              {'sign': 'C', 'amount': nominal_deal+nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
          ]
        
        # jurnal ckpn jika ppap > 0
        if nom_ppap_umum > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
              {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_umum')},
          ]

        if nom_ppap_khusus > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
              {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_khusus')},
          ]
           
      elif jns_jurnal =='edit': # edit sima     
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = self.nominal_deal
        nom_ujroh     = self.ujroh        
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
        
        if recA.kolektibilitas in [1]:
          nom_ppap_umum = recA.nom_ppap
        else:
          nom_ppap_khusus = recA.nom_ppap   
        
        ## Selisih perubahan data
        selisih_deal  = recP.nominal_deal-nominal_deal
        selisih_ujroh = recA.ujroh-nom_ujroh
        selisih_ppap_umum = nom_ppap_umum-self.ppap_umum
        selisih_ppap_khusus = nom_ppap_khusus-self.ppap_khusus  
        
        ## mengurangi pokok piutang
        if recA.tipe_treasury in ['P']:
          if selisih_deal >= 0: 
            lgr += [
                {'sign': 'D', 'amount': selisih_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
                {'sign': 'C', 'amount': selisih_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
            ]
            
          if selisih_deal < 0:
            lgr += [
                {'sign': 'D', 'amount': abs(selisih_deal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
                {'sign': 'C', 'amount': abs(selisih_deal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
            ]
            
        else: #Sima Borrowing
          if selisih_deal >= 0: 
            lgr += [
                {'sign': 'D', 'amount': selisih_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
                {'sign': 'C', 'amount': selisih_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal_brw')}
            ]
            
          if selisih_deal < 0:
            lgr += [
                {'sign': 'D', 'amount': abs(selisih_deal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal_brw')},
                {'sign': 'C', 'amount': abs(selisih_deal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
            ]
                                        
      
        if recA.kolektibilitas in [1]:
          if nom_ppap_khusus >= 0:
            lgr += [
                {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
                {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_khusus')},
            ]
            
          if selisih_ppap_umum >= 0:
            lgr += [
                {'sign': 'D', 'amount': selisih_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
                {'sign': 'C', 'amount': selisih_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
            ]
          else:
            lgr += [
                {'sign': 'D', 'amount': abs(selisih_ppap_umum), 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
                {'sign': 'C', 'amount': abs(selisih_ppap_umum), 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
            ]         
        
        else :    
          if nom_ppap_umum >= 0:
            lgr += [
                {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
                {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_umum')},
            ]
  
          if selisih_ppap_khusus >= 0:
            lgr += [
                {'sign': 'D', 'amount': selisih_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
                {'sign': 'C', 'amount': selisih_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
            ]
          else:
            lgr += [
                {'sign': 'D', 'amount': abs(selisih_ppap_khusus), 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
                {'sign': 'C', 'amount': abs(selisih_ppap_khusus), 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
            ]
            
      elif jns_jurnal =='paybahas': # bayar bagi hasil sima     
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = self.nominal_deal
        nom_ujroh     = self.ujroh
        saldo_margin   = self.saldo_margin
        saldo_accrue   = self.saldo_accrue
        
        if recA.tipe_treasury in ['P']: 
          #SIMA Placing
          ## selisih pembayaran bagi hasil
          nom_baghas  = recA.amount-saldo_margin
        
          if nom_baghas > 0: 
            lgr += [
                {'sign': 'D', 'amount': nom_baghas, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
                {'sign': 'C', 'amount': nom_baghas, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_profit')}
            ]
        else: 
          #SIMA Borrowing pembayaran bagi hasil
          nom_baghas  = recA.amount
      
          lgr += [                                                                               
              {'sign': 'D', 'amount': nom_baghas-saldo_accrue, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_biaya')},
              {'sign': 'D', 'amount': saldo_accrue, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_accrue')},
              {'sign': 'C', 'amount': nom_baghas, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
          ]   
          
      elif jns_jurnal =='penerbitan': #penerbitan sima     
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = recP.nominal_deal 
        nom_ujroh     = recA.ujroh
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
              
        if recA.kolektibilitas in [1]:
          nom_ppap_umum = recA.nom_ppap
        else:
          nom_ppap_khusus = recA.nom_ppap
        
                
        ## mengurangi pokok piutang
        if nominal_deal > 0:
          lgr += [
              {'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
              {'sign': 'C', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal_brw')}
          ]
        
        # jurnal ckpn jika ppap > 0
        if nom_ppap_umum > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
              {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
          ]
        
        if nom_ppap_khusus > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
              {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
          ]
          
      entries = fau.removeZeroAmountFromJournal(lgr)
      return entries 

#Akad Surat Berharga   
class TreaSuratBerharga(TreasuryAccount):
    # Static variables
    pobject_classname = 'TreaSuratBerharga'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '34'     
    
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    lstransfer =[ 'nomor_ref_deal*' 
          , 'ujroh*' 
          , 'tipe_SB*'
          , 'klasifikasi_SB*'
          , 'kode_instrumen*'
          , 'tgl_settlement*'
          , 'tipe_kupon*'   
          , 'tgl_bayar_berikutnya*', 'tgl_bayar_terakhir*'
          , 'seri_SB*'     
          , 'nominal_pajak*'
          , 'kolektibilitas*'
          , 'harga_beli*'   
          #, 'golongan_nasabah=*LGolongan_Nasabah.refdata_id'
          #, 'harga_baru'                                    
          , 'nilai_tunai*'
          #, 'revaluasi*'
    ]
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      self.kode_jenis = 'TRB'
      TreasuryAccount.OnCreate(self, param)
      recSrc = param['recParent']       
      atutil.transferAttributes(helper, self.lstransfer, self, recSrc)          
      config.FlushUpdates()
    
    def doSave(self, param, lstransfer):
      config = self.Config; helper = self.Helper
      TreasuryAccount.Edit(self, param)
      recSrc = param['recParent']      
      atutil.transferAttributes(helper, self.lstransfer, self, recSrc)          
      config.FlushUpdates()
    #--       

    def Placing(self, param): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      helper = self.Helper                                                                                               
      config = self.Config
      mlu = config.ModLibUtils
      recP = param['recParent']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      kode_entri = 'TRP004N'
      keterangan = 'PENEMPATAN SURAT BERHARGA SERI %s NO.REF:%s' % (self.seri_SB, self.nomor_ref_deal)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      #app.ConWriteln(jns_ayda)
      entries = self.getTrxJournal(param, 'penempatan')
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      oBank = helper.GetObject("BankAccount", recP.GetFieldByName('LBankAccount.Nomor_Rekening'))
      class ParRecord(object):pass        
      _parPayment = ParRecord()
      _parPayment.nominal_beli = (recP.nilai_tunai or 0)+(recP.saldo_accrue or 0)        
      _parPayment.kode_entri = kode_entri        
      _parPayment.keterangan = keterangan        
      _parPayment.jenis_mutasi = 'C'
      
      oBank.TrxBankAccount(_parPayment, oTx)
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTx
    #--
       
    def editTreasury (self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
           
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TREDIT'
      keterangan = 'EDIT ACCOUNT SURAT BERHARGA - %s' % (self.nomor_rekening)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'edit')
      #raise Exception, entries

      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      #simpan perubahan
      self.doSave(params, self.lstransfer)
      
      return oDetTx  
    
    def marketTreasury (self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
           
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TRMARKET'
      keterangan = 'MARK TO MARKET SURAT BERHARGA - %s' % (self.nomor_rekening)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'market')
      #raise Exception, entries

      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      #simpan perubahan
      self.doSave(params, self.lstransfer)
      
      return oDetTx 
    
    #SURATBERHARGA
    def getTrxJournal(self, param, jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = TreasuryUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal =='penempatan': # penempatan surat berharga
        rec = param['recParent']
        nominal_deal  = rec.nominal_deal or 0 
        nilai_tunai   = rec.nilai_tunai or 0
        nom_akru      = rec.saldo_accrue or 0
        nom_pajak     = rec.nominal_pajak

        disc_perm       = nominal_deal-nilai_tunai
        diskonto = 0 if disc_perm < 0 else disc_perm
        premium  = 0 if disc_perm > 0 else -disc_perm
        
        #nominal pajak, harga beli
        cash_proceed = nilai_tunai + nom_akru + nom_pajak
        
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
        
        if rec.kolektibilitas in [1]:
          nom_ppap_umum = rec.nom_ppap
        else:
          nom_ppap_khusus = rec.nom_ppap
        
        ## mengurangi pokok piutang
        lgr += [
            {'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
            {'sign': 'D', 'amount': nom_akru, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_pymad')},
            {'sign': 'D', 'amount': premium, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_premium')},
            {'sign': 'C', 'amount': diskonto, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_diskonto')},
            #{'sign': 'C', 'amount': nom_pajak, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_pajak')}
        ]
        
        # jurnal ckpn jika ppap > 0
        if nom_ppap_umum > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
              {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
          ]
        
        if nom_ppap_khusus > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
              {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
          ]

      elif jns_jurnal =='market': # mark to market SURATBERHARGA
        recP = param['recParent']
        recA = param['recAkad']
        nom_beli = recA.nilai_tunai
        nom_baru = recA.nilai_tunai_sekarang
        nominal = +(nom_baru-nom_beli)
        
        if nominal > 0:
          lgr += [
              {'sign': 'D', 'amount': nominal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_market')},
              {'sign': 'C', 'amount': nominal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_biaya')}
          ]
        else:
          lgr += [
              {'sign': 'D', 'amount': abs(nominal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_biaya')},
              {'sign': 'C', 'amount': abs(nominal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_market')}
          ]
        
      elif jns_jurnal =='edit': # edit SURATBERHARGA             
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = self.nominal_deal
        nom_ujroh     = self.ujroh        
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
        nom_pajak = self.nominal_pajak
        nominal = nominal_deal*(self.harga_beli/100)
        
        nominal_baru = recP.nominal_deal*(recA.harga_beli/100)
        
        if recA.kolektibilitas in [1]:
          nom_ppap_umum = recA.nom_ppap
        else:
          nom_ppap_khusus = recA.nom_ppap   
        
        ## Selisih perubahan data
        selisih_nominal = nominal_baru-nominal
        selisih_ujroh = recA.ujroh-nom_ujroh 
        selisih_pajak = recA.nominal_pajak-nom_pajak
        selisih_ppap_umum = nom_ppap_umum-self.ppap_umum
        selisih_ppap_khusus = nom_ppap_khusus-self.ppap_khusus  
        
        ## mengurangi pokok piutang
        if recA.klasifikasi_SB in ['A', 'T']:
          if selisih_nominal > 0:
            lgr += [
                {'sign': 'D', 'amount': selisih_nominal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
                {'sign': 'D', 'amount': selisih_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_pymad')},
                {'sign': 'C', 'amount': selisih_nominal-selisih_pajak+selisih_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
                {'sign': 'C', 'amount': selisih_pajak, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_pajak')}
            ]
                                                               
        if recA.kolektibilitas in [1]:
          if nom_ppap_khusus >= 0:
            lgr += [
                {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
                {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_khusus')},
            ]
            
          if selisih_ppap_umum >= 0:
            lgr += [
                {'sign': 'D', 'amount': selisih_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
                {'sign': 'C', 'amount': selisih_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
            ]
          else:
            lgr += [
                {'sign': 'D', 'amount': abs(selisih_ppap_umum), 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
                {'sign': 'C', 'amount': abs(selisih_ppap_umum), 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
            ]         
        
        else :    
          if nom_ppap_umum >= 0:
            lgr += [
                {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
                {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_umum')},
            ]
  
          if selisih_ppap_khusus >= 0:
            lgr += [
                {'sign': 'D', 'amount': selisih_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
                {'sign': 'C', 'amount': selisih_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
            ]
          else:
            lgr += [
                {'sign': 'D', 'amount': abs(selisih_ppap_khusus), 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
                {'sign': 'C', 'amount': abs(selisih_ppap_khusus), 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
            ]
                           
      entries = fau.removeZeroAmountFromJournal(lgr)
      return entries          
      
#Akad Reverse Repo 
class TreaReverseRepo(TreasuryAccount):
    # Static variables
    pobject_classname = 'TreaReverseRepo'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '35'     
    
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    lstransfer =[ 'nomor_ref_deal*' 
          , 'ujroh*'                                    
          , 'nilai_tunai'
    ]
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      self.kode_jenis = 'TRR'
      TreasuryAccount.OnCreate(self, param)
      recSrc = param['recAkad']       
      atutil.transferAttributes(helper, self.lstransfer, self, recSrc)          
      config.FlushUpdates()
    
    def doSave(self, param, lstransfer):
      config = self.Config; helper = self.Helper
      TreasuryAccount.Edit(self, param)
      recSrc = param['recAkad']      
      atutil.transferAttributes(helper, self.lstransfer, self, recSrc)          
      config.FlushUpdates()
    #--       

    def Placing(self, param): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      helper = self.Helper                                                                                               
      config = self.Config
      mlu = config.ModLibUtils
      recP = param['recParent']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      kode_entri = recP.kode_entri
      keterangan = 'PENEMPATAN REVERSE REPO %s' % self.nomor_rekening

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      #app.ConWriteln(jns_ayda)
      entries = self.getTrxJournal(param, 'penempatan')

      #raise Exception, entries
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTx
    #--
       
    def editTreasury (self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
           
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TREDIT'
      keterangan = 'EDIT ACCOUNT REVERSE REPO - %s' % (self.nomor_rekening)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'edit')
      #raise Exception, entries

      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      #simpan perubahan
      self.doSave(params, self.lstransfer)
      
      return oDetTx  
    
    #REVERSEREPO
    def getTrxJournal(self, param, jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = TreasuryUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal =='penempatan': # penempatan ReverseRepo
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = recP.nominal_deal 
        nom_ujroh     = recA.ujroh
        
        ## mengurangi pokok piutang:
        if nominal_deal > 0:
          lgr += [
              {'sign': 'D', 'amount': nominal_deal+nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              {'sign': 'C', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
              {'sign': 'C', 'amount': nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_pymad')}
          ]
        
      elif jns_jurnal =='edit': # edit ReverseRepo             
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = self.nominal_deal
        nom_ujroh     = self.ujroh       
        
        ## Selisih perubahan data
        selisih_nominal = recP.nominal_deal-nominal_deal
        selisih_ujroh   = recA.ujroh-nom_ujroh  
        
        ## mengurangi pokok piutang
        if selisih_nominal >= 0:
          lgr += [
              {'sign': 'D', 'amount': selisih_nominal+selisih_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              {'sign': 'C', 'amount': selisih_nominal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
              {'sign': 'C', 'amount': selisih_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_pymad')}
          ]
        if selisih_nominal < 0 or selisih_ujroh < 0:
          lgr += [
              {'sign': 'D', 'amount': abs(selisih_nominal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
              {'sign': 'D', 'amount': abs(selisih_ujroh), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_pymad')},
              {'sign': 'C', 'amount': abs(selisih_nominal+selisih_ujroh), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
          ]
      
      elif jns_jurnal =='penutupan': # tutup ReverseRepo             
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = self.nominal_deal
        nom_ujroh     = self.ujroh
        
        ## Selisih pendapatan
        selisih_ujroh = nom_ujroh-self.saldo_margin       
        
        ## mengurangi pokok piutang
        lgr += [
            {'sign': 'D', 'amount': nominal_deal+nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
            {'sign': 'C', 'amount': nominal_deal+nom_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
        ]
        
        ##sisa amortisasi
        lgr += [
            {'sign': 'D', 'amount': selisih_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_pymad')},
            {'sign': 'C', 'amount': selisih_ujroh, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_profit')}
        ]
                                                                                              
      entries = fau.removeZeroAmountFromJournal(lgr)
      return entries     

#Reksadana 
class TreaReksadana(TreasuryAccount):
    pobject_classname = 'TreaReksadana'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '36'     
    
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    lstransfer =[ 
          'nomor_ref_deal*'                                
          , 'nilai_nav*'                              
          , 'harga_beli*'                               
          , 'kolektibilitas*' 
    ]
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      self.kode_jenis = 'TRA'
      TreasuryAccount.OnCreate(self, param)
      recSrc = param['recAkad']       
      atutil.transferAttributes(helper, self.lstransfer, self, recSrc)          
      config.FlushUpdates()
    
    def doSave(self, param, lstransfer):
      config = self.Config; helper = self.Helper
      TreasuryAccount.Edit(self, param)
      recSrc = param['recAkad']      
      atutil.transferAttributes(helper, self.lstransfer, self, recSrc)          
      config.FlushUpdates()
    #--       

    def Placing(self, param): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      helper = self.Helper                                                                                               
      config = self.Config
      mlu = config.ModLibUtils
      recP = param['recParent']
      
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      kode_entri = recP.kode_entri                                         
      keterangan = 'PENEMPATAN REKSADANA %s' % self.nomor_rekening

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      #app.ConWriteln(jns_ayda)
      entries = self.getTrxJournal(param, 'penempatan')
     
      #raise Exception, entries
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      oBank = helper.GetObject("BankAccount", recP.GetFieldByName('LBankAccount.Nomor_Rekening'))
      class ParRecord(object):pass        
      _parPayment = ParRecord()
      _parPayment.nominal_beli = recP.nominal_deal        
      _parPayment.kode_entri = kode_entri        
      _parPayment.keterangan = keterangan        
      _parPayment.jenis_mutasi = 'C'
      
      oBank.TrxBankAccount(_parPayment, oTx)      
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTx
    #--
       
    def editTreasury (self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
           
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TREDIT'
      keterangan = 'EDIT ACCOUNT REKSADANA - %s' % (self.nomor_rekening)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'edit')
      #raise Exception, entries

      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      #simpan perubahan
      self.doSave(params, self.lstransfer)
      
      return oDetTx
    
    def marketTreasury (self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
           
      # CREATE TRANSACTION
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      kode_entri = 'TRA001'
      keterangan = 'MARK TO MARKET REKSADANA - %s' % (self.nomor_rekening)

      oTx = TreasuryAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      entries = self.getTrxJournal(params, 'market')

      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTx.ProcessDetails()
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTx  
    
    #REKSADANA
    def getTrxJournal(self, param, jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = TreasuryUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal =='penempatan': # penempatan REKSADANA
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = recP.nominal_deal
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
        
        if recA.kolektibilitas in [1]:
          nom_ppap_umum = recA.nom_ppap
        else:
          nom_ppap_khusus = recA.nom_ppap
                
        ## mengurangi pokok piutang:
        if nominal_deal > 0:
          lgr += [
              {'sign': 'D', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              #{'sign': 'C', 'amount': nominal_deal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
          ]
        
        # jurnal ckpn jika ppap > 0
        if nom_ppap_umum > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
              {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
          ]
        
        if nom_ppap_khusus > 0:
          lgr += [
              {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
              {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
          ]
        
      elif jns_jurnal =='market': # mark to market reksadana
        rec = param['rec']
        nominal =  (rec.nilai_nav_new - self.nilai_nav) * self.harga_beli
        
        if nominal > 0:
          lgr += [
              {'sign': 'D', 'amount': nominal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              {'sign': 'C', 'amount': nominal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_biaya')}
          ]
        else:
          lgr += [
              {'sign': 'D', 'amount': abs(nominal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_biaya')},
              {'sign': 'C', 'amount': abs(nominal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
          ]
      
      elif jns_jurnal =='edit': # edit REKSADANA             
        recP = param['recParent']
        recA = param['recAkad']
        nominal_deal  = self.nominal_deal 
        nom_ppap_umum = self.ppap_umum or 0
        nom_ppap_khusus = self.ppap_khusus or 0
        
        if recA.kolektibilitas in [1]:
          nom_ppap_umum = recA.nom_ppap
        else:
          nom_ppap_khusus = recA.nom_ppap
        
        ## Selisih perubahan data
        selisih_nominal = recP.nominal_deal-nominal_deal
        selisih_ppap_umum = nom_ppap_umum-self.ppap_umum
        selisih_ppap_khusus = nom_ppap_khusus-self.ppap_khusus  
        
        # Perubahan Nominal
        if selisih_nominal > 0:
          lgr += [
              {'sign': 'D', 'amount': selisih_nominal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')},
              {'sign': 'C', 'amount': selisih_nominal, 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')}
          ]
        if selisih_nominal < 0:
          lgr += [
              {'sign': 'D', 'amount': abs(selisih_nominal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_giro_bi')},
              {'sign': 'C', 'amount': abs(selisih_nominal), 'kode_tx_class':  fau.get_tx(kode_jenis,'tr_nominal')}
          ]
          
        # Perubahan ppap
        if recA.kolektibilitas in [1]:
          if nom_ppap_khusus >= 0:
            lgr += [
                {'sign': 'D', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
                {'sign': 'C', 'amount': nom_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_khusus')},
            ]
            
          if selisih_ppap_umum >= 0:      
            lgr += [
                {'sign': 'D', 'amount': selisih_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
                {'sign': 'C', 'amount': selisih_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
            ]
          else:
            lgr += [
                {'sign': 'D', 'amount': abs(selisih_ppap_umum), 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
                {'sign': 'C', 'amount': abs(selisih_ppap_umum), 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_umum')},
            ]         
        
        else :    
          if nom_ppap_umum >= 0:
            lgr += [
                {'sign': 'D', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum')},
                {'sign': 'C', 'amount': nom_ppap_umum, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdpt_rev_ppap_umum')},
            ]
  
          if selisih_ppap_khusus >= 0:
            lgr += [
                {'sign': 'D', 'amount': selisih_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
                {'sign': 'C', 'amount': selisih_ppap_khusus, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
            ]
          else:
            lgr += [
                {'sign': 'D', 'amount': abs(selisih_ppap_khusus), 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus')},
                {'sign': 'C', 'amount': abs(selisih_ppap_khusus), 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus')},
            ]
      
                                                                                                            
      entries = fau.removeZeroAmountFromJournal(lgr)
      return entries

class TreaInfoTransaksi(pobject.PObject):
    # static variable
    pobject_classname = 'TreaInfoTransaksi'
    pobject_keys = ['id_otorisasi']
    
    trfAttribut = [
      'Id_Otorisasi',
      'Nama_Rekening_Debet*',
      'Nama_Rekening_Kredit*',
      'Nomor_Rekening_Debet*',
      'Nomor_Rekening_Kredit*',
      'Nominal_Transaksi*',
      'Nomor_Ref_Deal*',
      'Kode_Cabang*',
      'Kode_Valuta*', 
      'User_Info*',
      'Date_Info*',
    ]
      
    def OnCreate(self, param):  
      trfAttribut = self.trfAttribut 
      self.doSave(param, trfAttribut)
    
    def doSave(self, param, trfAttribut):
      config = self.Config
      helper = self.Helper      
      atutil.transferAttributes(helper, self.trfAttribut, self, param)
      self.Config.FlushUpdates()
#--         