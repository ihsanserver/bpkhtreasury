import sys
import com.ihsan.foundation.pobject as pobject
import com.ihsan.foundation.mobject as mobject
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.customidgenAPI as customidgenAPI                                                            
import com.ihsan.util.modman as modman
import com.ihsan.net.message as message
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.lib.textreport as report
import copy
import calendar
from decimal import Decimal, ROUND_HALF_UP

modman.loadStdModules(globals(), ['libs', 'FinCalculator'])

class NameSpaceHelper: pass # dummy class to store this module's namespace

gHelper = pobjecthelper.PObjectHelper(appserver.ActiveConfig)
gFinJournal = modman.getModule(appserver.ActiveConfig, 'FinJournal')

EPSILON = 0.01

def dbg(msg):
  message.send_udp(msg + "\n", 'localhost', 9898)

class PendingTransactionJournal(pobject.PObject):
    # Static variables
    pobject_classname = 'PendingTransactionJournal'
    pobject_keys      = ['id_transaksi']
    
    def OnCreate(self, oTransaction, holdJournal = False):
      self.id_transaksi = oTransaction.id_transaksi
      self.session_id = self.Config.SecurityContext.SessionID
      self.config.FlushUpdates()
      if not holdJournal:
        try:
          # raise Exception, "Masuk ke buat jurnal"
          periodHelper = self.Helper.CreateObject('core.PeriodHelper')
          #tgl_journal = periodHelper.GetAccountingDate()
          tgl_journal = oTransaction.GetAsTDateTime('tanggal_transaksi')
          #raise Exception, tgl_journal
          sTglJournal = self.config.FormatDateTime('dd-MMM-yyyy', tgl_journal)
          gFinJournal.createFinancingJournal(self.config, sTglJournal, self.id_transaksi)
        finally:
          self.config.ResetCache()
        #--
      #--
    #-- def OnCreate
      
    def sendToAccounting(self):
      config = self.Config
      app = config.AppObject
      if self.flag_sent == 'T':
        return
      id_transaksi = self.id_transaksi
      config.BeginTransaction()
      try:
        isErr = 0
        errmsg = '' 
        try:
          if self.flag_sent == 'F' or self.flag_sent == None or self.flag_sent == '':
            phRes = app.rexecscript('remote_core', 'appinterface/RemoteJournal', app.CreateValues(['id_transaksi', id_transaksi]))
            fr = phRes.FirstRecord
            isErr = not fr.status
            errmsg = fr.errmsg
            if not isErr: self.flag_sent = 'T'
          #--
        except:
          isErr = 1
          errmsg = str(sys.exc_info()[1])
        self.is_err = int(isErr)
        self.err_message = errmsg
        config.Commit()
      except:
        config.Rollback()
        raise
        
      if isErr:
        raise Exception, errmsg
        
      dParams = {'filter': 'ptj.id_transaksi = %d' % id_transaksi}
      config.BeginTransaction()
      try:
        sSQL = '''
          INSERT INTO completedtransactionjournal
          (
            id_transaksi,
            journal_no
          )
          SELECT id_transaksi, journal_no FROM
          pendingtransactionjournal ptj
          WHERE 
          %(filter)s
        ''' % dParams
        dbutil.runSQL(config, sSQL)
        self.Delete()
        config.Commit()
      except:
        config.Rollback()
        raise
      #--
    #-- send to accounting
      
    def sendAllToAccounting(self, currentSessionOnly = True):
      config = self.Config; mlu = config.ModLibUtils
      helper = self.Helper
      app = config.AppObject
      app.ConCreate('out')
      if currentSessionOnly:
        allSession = 0
      else:
        allSession = 1
        
      q = config.CreateSQL('''
        select id_transaksi
        from pendingtransactionjournal where
        (session_id = %s or 1 = %d) and (flag_sent is NULL or flag_sent = 'F') order by id_transaksi
      ''' % (mlu.QuotedStr(config.SecurityContext.SessionID), allSession)).RawResult
      errorOccured = False
      while not q.Eof:
        id_transaksi = q.id_transaksi
        o = helper.GetObject('PendingTransactionJournal', id_transaksi)
        try:
          o.sendToAccounting()
          app.ConWriteln('Create accounting entry for id %d success' % id_transaksi, 'out')
        except:
          errorOccured = True
          errmsg = str(sys.exc_info()[1])
          app.ConWriteln('Create accounting entry for id %d failed: %s' % (id_transaksi, errmsg), 'out')
        q.Next()
      #-- while

      app.ConWriteln('---------------------------')
      app.ConWriteln('Process completed')      
      app.ConRead('Press Enter to continue') 
      
    #-- def
#-- class      
    

class FinFacility(pobject.PObject):
    # Static variables
    pobject_classname = 'FinFacility'
    pobject_keys      = ['facility_no']

    
    def OnCreate(self, param):
      #self.
      kd_akad={
        'A':'MRH',
        'B':'MSY',
        'C':'MDB',
        'I': 'Kafalah'
      }
                          
      config = self.Config
      helper = self.Helper      
      recSrc = param['FinFacility']
      self.id_otorentri = param['id_otorentri']
      
      # set other fields w/ non simple assignment
      if not atutil.checkHasAttribute(recSrc, 'facility_no') or ((recSrc.facility_no or "") == ""):
        fau = FinAccountUtils(config)
        cif = recSrc['LCustomer.Nomor_Nasabah']
        kdProd = kd_akad[recSrc['contracttype_code']]
        self.facility_no = fau.getNextFacilityNumber(cif+kdProd)
      else:
        self.facility_no = recSrc.facility_no
      default_valuta = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultCurrency')
      default_pof = '000'
      oUser = helper.GetObject('enterprise.User', config.SecurityContext.InitUser)
      default_cabang = oUser.kode_cabang
      dictInitValues = {
        'total_facility_used' : 0.0,
        'locked' : 'F',
        'currency_code': default_valuta,
        'kode_pof': default_pof,
        'kode_cabang': default_cabang,
        'dropping_model': 'A'
      } 
      for initField, initValue in dictInitValues.iteritems():
        self.SetFieldByName(initField, initValue)

      atutil.transferAttributes(helper, 
        [ 'no_nasabah=LNasabah.Nomor_Nasabah', 
          'NasabahGroup=*LNasabahGroup.Nomor_Nasabah', 
          'AMCode=*LAccountManager.AgentCode',
          'legal_binding', 'contract_date', 'dropping_due_date*', 'facility_due_date', 'contract_no', 'Currency_Code=*LCurrency.Currency_Code',
          'kode_pof=*LPoolOfFund.kode_pof','Kode_Cabang=*LCabang.Kode_Cabang', 'targeted_eqv_rate', 'total_facility_limit', 'sifat_fasilitas',
          'dropping_model*', 'contracttype_code', 'notary_name*', 'opening_date', 'add_contract_date', 'add_contract_no',
          'reviewer=*LReviewer.refdata_id', 'approver=*LApprover.refdata_id', 'kode_notaris=*LNotaris.kode_notaris'
        ], self, recSrc
      )
      self.status = 'N'  # new
      self.status_update = 'F'
      
      # create dummy account to maintain facility limit and usage
      self.createCommitmentAccount()
      #self.openFacility()
      self.config.FlushUpdates()
    #--
  
    def createCommitmentAccount(self, customLimit = False):
      dParamFinAccount = {
        'tgl_akad': timeutil.stdDate(self.Config, self.contract_date), 
        'adm_fee': 0.0, 
        'description': 'COMMITMENT ACCOUNT FAS %s' % self.facility_no, 
        'legal_binding': self.legal_binding, 
        'other_fee': 0.0, 
        'period_count': 0, 
        'contract_number': self.contract_no, 
        'targeted_eqv_rate': self.targeted_eqv_rate, 
        'LCustomer.Nomor_Nasabah': self.no_nasabah, 
        'LNasabahGroup.Nomor_Nasabah': self.NasabahGroup, 
        'LAccountManager.AgentCode': self.AMCode, 
        #'LPerusahaanAsuransi.Nama_Singkat': None,
        #'LPerusahaanAsuransi2.Nama_Singkat': None,
        'due_date': self.facility_due_date,
        'kode_asuransi': None,
        'kode_asuransi2': None,
        'LPaymentSrc.nomor_rekening': None,
        'notary_name': self.notary_name,         
        'opening_date': self.opening_date,
        'add_contract_date': self.add_contract_date,
        'add_contract_no': self.add_contract_no, 
        'reviewer': self.reviewer,
        'approver': self.approver 
      }
      param = {'FinAccount': dParamFinAccount} 
      oAccount = self.Helper.CreatePObject("FinAccount", param)
      self.LCommitmentAccount = oAccount
      oAccount.kode_valuta = self.currency_code
      oAccount.kode_cabang =  self.Kode_Cabang 
      oAccount.is_commitmentaccount = 'T'
      oAccount.LFinFacility = self
      oAccount.contracttype_code = 'W' # waad / commitment
      oAccount.dropping_model = self.dropping_model
      oAccount.kode_jenis = 'WAD'
      oAccount.financing_model = 'C' # continous
      if not customLimit:
        self.setNewCommitment(self.total_facility_limit)
      return oAccount
    #--
    
    def transactCommitment(self, sign, amount, keterangan = None, oTx = None ): 
      # CREATE TRANSACTION
      config = self.Config
      oFinAccount = self.LCommitmentAccount
      if keterangan == None:
        keterangan = "FASILITAS %s" % self.facility_no
      #if sign == 'D' and oFinAccount.saldo < amount:
      #  raise Exception, 'Limit fasilitas %s tidak mencukupi' % self.facility_no
      oldTx = oTx  
      if oTx == None:    
        periodHelper = self.Helper.CreateObject('core.PeriodHelper')
        oTx = self.initTransaksi('', periodHelper.GetAccountingDate(), keterangan)

      if sign == 'C':
        kode_entri = 'FTF001'
      else:
        kode_entri = 'FTF002'
      
      
      akad=self.contracttype_code; _sifat=self.sifat_fasilitas
      '''
      if akad=='B': #akad musyarakah
        acc_code = '9111003' if _sifat=='C' else '9111004' 
      elif akad=='C': #akad mudharabah
        acc_code = '9111001' if _sifat=='C' else '9111002' 
      elif akad=='I': #akad kafalah
        acc_code = '9220001'
      else: # selain itu
        acc_code = '9111006'
      '''
      
        
      #get acc code waad from glinterfacefacility
      sf_ = 1 if _sifat=='C' else 2
      sql = """
        SELECT kode_account, cost_element_id FROM %(glinterfacefacility)s WHERE contracttype_code='%(akad)s' and kode_interface='19000' and interface_type='1' order by cost_element_id
      """ % {
          'glinterfacefacility': config.MapDBTableName('glinterfacefacility'),
          'akad': akad
        }
      rs = config.CreateSQL(sql).RawResult
      acc_code = '9111006'
      while not rs.Eof:
        acc_code = rs.kode_account
        if sf_ == rs.cost_element_id:
          acc_code = rs.kode_account
        
        rs.Next() 

      #raise Exception, acc_code
        
      oDetTx = oFinAccount.CreateTransactionGroup(kode_entri, keterangan, oTx, [
            {'sign': sign, 'amount': amount, 'kode_tx_class': '19000', 'kode_account':acc_code}
          ]  
      )
      oDetTx.ProcessDetails()
      if oldTx == None:
        self.Helper.CreatePObject('PendingTransactionJournal', oTx)
        #oTx.CreateJournal()
      
      self.last_update = periodHelper.GetAccountingDate()
    #--
    
    def setNewCommitment(self, newCommitment):
      oFinAccount = self.LCommitmentAccount
      oFinAccount.LockRow()
      self.LockRow()
      
      prev_balance = oFinAccount.saldo
      if newCommitment > prev_balance:
        sign = "C"
        amount = newCommitment - prev_balance
      else:
        sign = "D"
        amount = prev_balance - newCommitment
      #--
      self.transactCommitment(sign, amount)
      self.total_facility_used = self.total_facility_limit - oFinAccount.saldo
    #--
    
    def notifyDropping(self, oAccount, amount_dropping):
      if self.dropping_model in ('A', 'O') and self.drop_counter >= 1:
        raise Exception, 'Fasilitas %s sudah pernah dropping sebelumnya' % oAccount.nomor_rekening      
      if ((self.total_facility_limit-self.total_facility_used) - amount_dropping) < -0.1:
        raise Exception, 'Fasilitas %s melebihi limit!' % oAccount.nomor_rekening      
      self.drop_counter = (self.drop_counter or 0) + 1
      self.total_facility_used = self.total_facility_used + amount_dropping
      self.transactCommitment('D', amount_dropping, 'DROPPING %s' % oAccount.nomor_rekening)
      
    def notifyPayment(self, oAccount, amount_principal_payment):
      if self.dropping_model == 'R':
        self.total_facility_used = self.total_facility_used - amount_principal_payment
        self.transactCommitment('C', amount_principal_payment, 'PAYMENT %s' % oAccount.nomor_rekening)
      pass
    
    def notifyReversePayment(self, oAccount, amount_principal_payment):
      if self.dropping_model == 'R':
        self.total_facility_used = self.total_facility_used + amount_principal_payment
        self.transactCommitment('D', amount_principal_payment, 'REVERSE PAYMENT %s' % oAccount.nomor_rekening)
      pass
    
    def closeFacility(self):
      helper = self.Helper
      config = self.Config
      periodHelper = helper.CreateObject('core.PeriodHelper')
      tgl_tutup = periodHelper.GetAccountingDate()                   
      oFinAccount = helper.GetObject('FinAccount',self.nomor_rekening )
      oFinAccount.id_otorentri = self.id_otorentri

      sisa_amount = oFinAccount.saldo
      #raise Exception, sisa_amount
      self.transactCommitment('D', sisa_amount, 'Tutup Fasilitas %s' % self.facility_no)
      self.total_facility_used  = 0
      self.total_facility_limit = 0
      
      #realized sisa biaya amortisasi
      oFinAccount.RealizedRemainCost()

      oFinAccount.status_rekening =3
      oFinAccount.closing_date = tgl_tutup

      self.status = 'T'
      self.closing_date = tgl_tutup
      self.last_update = tgl_tutup
      #self.facility_due_date = tgl_tutup
    #--
      
    def editFacility(self,params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils             
      periodHelper = helper.CreateObject('core.PeriodHelper')
      tgl_trx = periodHelper.GetAccountingDate()                   
      
      self.dropping_due_date = params.dropping_due_date 
      self.facility_due_date = params.facility_due_date
      self.AMCode            = params.GetFieldByName('LAccountManager.AgentCode')                      
      self.notary_name       = params.notary_name
      self.kode_notaris      = params.GetFieldByName('LNotaris.kode_notaris')
      self.dropping_model    = params.dropping_model
      self.add_contract_no   = params.add_contract_no
      self.add_contract_date = params.add_contract_date
      self.contract_date     = params.contract_date
      self.contract_no       = params.contract_no
      self.locked            = params.locked
      
      dbutil.runSQL(config,
        '''
          UPDATE %(finaccount)s
          SET
            dropping_model = %(dm)s 
          WHERE
            facility_no =%(f_no)s 
        ''' % {
          'finaccount': config.MapDBTableName('finaccount'),
          'dm': mlu.QuotedStr(self.dropping_model),
          'f_no': mlu.QuotedStr(self.facility_no)
        }
      )      
      #jika topup fasilitas
      if params.top_up_facility > 0:
        self.transactCommitment('C', params.top_up_facility, 'TOP UP Plafond Fasilitas %s' % self.nomor_rekening)
        self.total_facility_limit += params.top_up_facility
      elif params.top_up_facility < 0:
        self.transactCommitment('D', -params.top_up_facility, 'Penurunan Plafond Fasilitas %s' % self.nomor_rekening)
        self.total_facility_limit += params.top_up_facility
    
      self.last_update = tgl_trx
    #--    
      
    def FacilityLoanActive(self):
      config = self.Config
      mlu = config.ModLibUtils
      #cek fasilitas boleh tutup atau tidak
      sql = """SELECT count(f.nomor_rekening) as c_openAccount FROM %(rekeningtransaksi)s rt, %(finaccount)s f 
                WHERE rt.nomor_rekening = f.nomor_rekening and f.contracttype_code<>'W' and  rt.status_rekening = '1' and
                f.facility_no = %(facility_no)s 
      """ % {
          'finaccount': config.MapDBTableName('finaccount'),
          'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
          'facility_no': mlu.QuotedStr(self.facility_no)
        }
      rs = config.CreateSQL(sql).RawResult
      if not rs.Eof:
        c_openAccount = rs.c_openAccount
      
      return c_openAccount or 0

    #upddate by BG set user input dan user otorisasi #20150216 
    def initTransaksi(self, kode_entri, tgl_transaksi, keterangan):
      helper = self.Helper ; config = self.Config      
      # CREATE TRANSACTION 
      oTx = helper.CreatePObject("core.Transaksi", kode_entri)
      oTx.SetStandardInfo(tgl_transaksi, keterangan)
      if self.id_otorentri>0: 
        Otr = helper.GetObject('enterprise.OtorEntri', self.id_otorentri)
        oTx.terminal_input = Otr.terminal_input    
        oTx.tanggal_input  = Otr.tgl_input    
        oTx.jam_input      = Otr.tgl_input    
        oTx.user_input     = Otr.user_input
      
      oTx.SetStatusOtorisasi(1)
      return oTx 

    def reposition(self, params): 
      helper = self.Helper ; config = self.Config
      #RuleTransaction = modman.getModule(self.Config, "RuleTransaction")
      oFinWaad = self.LCommitmentAccount
      amount_rep = oFinWaad.saldo
      self.transactCommitment('D', amount_rep, 'Reposisi old %s' % oFinWaad.nomor_rekening)
      oFinWaad.kode_cabang = params.GetFieldByName('LCabangTujuan.Kode_Cabang')
      self.kode_cabang = params.GetFieldByName('LCabangTujuan.Kode_Cabang')
      
      ls_finacc = self.Ls_FinAccount
      ls_finacc.First()
      ent =[]
      while not ls_finacc.EndOfList:
        oF = ls_finacc.CurrentElement
        oFinAcc = helper.GetObject('FinAccount', oF.nomor_rekening)#ls_finacc.CurrentElement
        oFinAcc = oFinAcc.CastToLowestDescendant() 

        if oFinAcc.contracttype_code <> 'W' and not oFinAcc.isRepaymentCompleted() and not oFinAcc.isWriteOff() and not oFinAcc.isAYDA():
          if oFinAcc.isDroppingDone() or oFinAcc.contracttype_code == 'I':
            oFinAcc = oFinAcc.CastToLowestDescendant()
            oFinAcc.reposition(params)
          else:
            oFinAcc.kode_cabang = self.kode_cabang
          #ent.append(oF.nomor_rekening)
          
        ls_finacc.Next()
      #kr in new branch   
      self.transactCommitment('C', amount_rep, 'Reposisi new %s' % oFinWaad.nomor_rekening)

#--

class FinMurabahahFacility(FinFacility):
    # Static variables
    pobject_classname = 'FinMurabahahFacility'
    pobject_keys      = ['facility_no']
#--

class FinMusyarakahFacility(FinFacility):
    # Static variables
    pobject_classname = 'FinMusyarakahFacility'
    pobject_keys      = ['facility_no']
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      FinFacility.OnCreate(self, param)
      recSrc = param['FinMusyarakahFacility']
      # set fields with simple / direct 1-on-1 assignment from record with same field name

      config.FlushUpdates()
#--

class FinMudharabahFacility(FinMusyarakahFacility):
    # Static variables
    pobject_classname = 'FinMudharabahFacility'
    pobject_keys      = ['facility_no']
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      FinFacility.OnCreate(self, param)
      recSrc = param['FinMudharabahFacility']
      # set fields with simple / direct 1-on-1 assignment from record with same field name

      config.FlushUpdates()

    def createCommitmentAccount(self):
      pass
#--

class FinQardhFacility(FinFacility):
    # Static variables
    pobject_classname = 'FinQardhFacility'
    pobject_keys      = ['facility_no']

class FAUtils(mobject.MObject):
    
  def mobject_init(self):
    self.utils = FinAccountUtils(self.Config)
    self.utils.Helper = self.Helper
  

class FinAccountUtils:
  DICT_FACILITY_TYPE_MAP = {
    'A': 'Murabahah waad',
    'B': 'Musyarakah',
    'C': 'Mudharabah',
    'G': 'PRKS',
    'I': 'Kafalah'
  }
  
  DICT_KODE_JENIS={
    'A':'MBH',
    'B':'MSY',
    'C':'MDB',
    'D':'QRD',
    'E':'MMQ',
    'F':'IJR',
    'G':'PRK',
    'H':'ISH',
    'I':'KAF'
  }
    
  def __init__(self, config):
    self.config = config
    #self.helper = self.Helper
    
  def getCustomId(self,idName,idCode):
    config = self.config
    customid = customidgenAPI.custom_idgen(config)
    customid.PrepareGetID(idName, idCode)
    new_seq = customid.GetLastID()
    customid.Commit()    
    return new_seq
  #--

  def getNextAccountNumber(self,cif):
    #row = self.config.CreateSQL("""select %(seq_finaccount)s.NEXTVAL as nextnum from dual""" % dbutil.mapDBTableNames(self.config, ['seq_finaccount'])).RawResult      
    #norek = 'F' + str( int(row.nextnum) ).zfill(10)
    #idCode = '%s%s' % (cif, 'MRH') 
    idCode = cif
    new_seq = self.getCustomId('FinAccount',idCode)
    
    norek = '%s%s' % (idCode, str( int(new_seq) ).zfill(3))
    #raise Exception, norek
    return norek
  #--
    
  def getNextFacilityNumber(self,idCode):
    #row = self.config.CreateSQL("""select %(seq_finfacility)s.NEXTVAL as nextnum from dual""" % dbutil.mapDBTableNames(self.config, ['seq_finfacility'])).RawResult      
    #return 'FAS' + str( int(row.nextnum) ).zfill(7)
    new_seq = self.getCustomId('FinAccount',idCode)
    
    norek = '%s%s' % (idCode, str( int(new_seq) ).zfill(2))
    #raise Exception, norek
    return norek
    
  def get_kode_entri(self,cparam,cgroup):
    row = self.config.CreateSQL("""SELECT KODE_ENTRI FROM %s WHERE CLASSPARAMETER='%s' AND CLASSGROUP='%s' """ % (self.config.MapDBTableName('core.DETILTRANSGROUPCLASS'),cparam,cgroup)).RawResult      
    return row.KODE_ENTRI
    
  def get_tx(self,kode_jenis,c_group):
    row = self.config.CreateSQL("""select kode_tx_class from %s WHERE kode_jenis='%s' and classgroup='%s' """ % (self.config.MapDBTableName('core.DetilTransaksiClass'),kode_jenis,c_group)).RawResult      
    return row.kode_tx_class
    
  def get_account_code(self,product_code,cost_element_id):
    sSQL ="""select account_code,amortization_account_code 
             from %s WHERE product_code='%s' and cost_element_id=%s 
          """ % (self.config.MapDBTableName('FinProductCost'),product_code,cost_element_id)
    row = self.config.CreateSQL(sSQL).RawResult
    _gac = [ row.account_code, row.amortization_account_code ]      
    return _gac
    
  def get_glfacility(self, rec):
    if rec[0]=='1':
      add_where =""" and kode_interface='%s' and interface_type='1' """ % (rec[3])
    elif rec[0]=='2':
      add_where =""" and cost_element_id=%s and interface_type='2' """ % (rec[3])

    sSQL =""" select kode_account from %s WHERE kode_jenis='%s' and dropping_model='%s' %s """ % (self.config.MapDBTableName('glinterfacefacility'),rec[1],rec[2],add_where)
          
    #raise Exception, sSQL
    row = self.config.CreateSQL(sSQL).RawResult
    return row.kode_account
    
  def mapFacilityType(self, ft):
    res = self.DICT_FACILITY_TYPE_MAP.get(ft, None)
    if res == None: raise Exception, "Unknown facility_type %s" % ft
    return res 
    
  # hilangkan jurnal row dengan amount = 0
  def removeZeroAmountFromJournal(self, lsource):
    lcleaned = []
    for row in lsource :
      nilai = row['amount']
      if float(abs(nilai)) > 0.01 :
        lcleaned.append(row)
    return lcleaned
    
  #rounded 2 digit
  def to_decimal(self, val):
    b=Decimal(format(val, '.2f'))
    #output = Decimal(b.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))
    output = round(val,2)
    
    return output
    
  #----  

#--
    
class FinAccount(gHelper.GetPClass("core.RekeningCustomer")):
    # Static variables
    pobject_classname = 'FinAccount'
    pobject_keys      = ['nomor_rekening']
    
    kode_tx_class = None
    
    def OnCreate(self, param):
      #self.                          
      config = self.Config
      helper = self.Helper      
      recSrc = param['FinAccount']
      # set other fields w/ non simple assignment
      default_valuta = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultCurrency')
      if not atutil.checkHasAttribute(recSrc, 'nomor_rekening') or ((recSrc.nomor_rekening or "") == ""):
        fau = FinAccountUtils(config)
        #self.Nomor_Rekening = fau.getNextAccountNumber(recSrc.GetFieldByName('LCustomer.Nomor_Nasabah'))
        if atutil.checkHasAttribute(recSrc, 'LCustomer.Nomor_Nasabah'):
          self.Nomor_Rekening = fau.getNextAccountNumber(recSrc['LCustomer.Nomor_Nasabah'])
        elif atutil.checkHasAttribute(recSrc, 'lfinfacility.no_nasabah'):                                                                     
          self.Nomor_Rekening = fau.getNextAccountNumber(recSrc['lfinfacility.no_nasabah'])
        else:
          raise Exception, 'OnCreate FinAccount getNextAccountNumber attribute not defined!'
      else:
        self.Nomor_Rekening = recSrc.nomor_rekening
      oUser = helper.GetObject('enterprise.User', config.SecurityContext.InitUser)
      default_cabang = oUser.kode_cabang
      dictInitValues = {
        'automatic_payment': 'F',
        'Saldo' : 0.0, 
        'income_deferred_balance' : 0.0, 
        'Saldo_Hari_Lalu' : 0.0,
        'Saldo_POD' : 0.0, 
        'Status_Rekening' : 1,
        'period_unit': 'M', 
        'reserve_eval': 'T', 
        'arrear_balance' : 0.0, 
        'arrear_period' : 0.0,
        'profit_balance': 0.0,
        'profit_arrear_balance': 0.0,
        'collateral_to_balance' : 0.0, 
        'drop_counter' : 0, 
        'overall_col_level' : 0, 
        'eval_arrear_period' : 0, 
        'is_external': 'F',
        'payment_balance' : 0.0, 
        'performance_col_level' : 0, 
        'prospect_col_level' : 0, 
        'repayment_col_level' : 0,
        'manual_col_level': 0,
        'reserved_loss_balance' : 0.0,
        'reserved_common_balance': 0.0,
        'reserved_manual_balance': 0.0, 
        'total_facility_used' : 0.0,
        'collateral_value': 0.0,
        'collateral_cash_value': 0.0,
        'write_off_status': 'F',
        'write_off_balance': 0.0,
        'restructure_counter': 0,

        'penalty_lastapply': None,
        'penalty_tmp_balance': 0.0,
        'penalty_due_balance': 0.0,
        
        #'dropping_amount': 0.0,
        #'predicted_margin_amount': 0.0,
        
        ## default values that is to be overriden by parameters
        'kode_valuta': default_valuta,
        'kode_cabang': default_cabang,
        'col_model': 'R', # repayment-only collateral evaluation 
        'dropping_model': 'A', # all at once dropping model
        'financing_model': 'T', # term-based financing 
        'contracttype_code': 'A', # murabahah
        'adm_fee': 0.0,
        'description': '',                                                                                                                               
        'other_fee': 0.0,
        'penalty_type': 'N',
        'penalty_fix': 0.0,
        'penalty_gp': 0,
        'penalty_maxp': 0,
        'penalty_percentage': 0.0,
        'insurance_fee': 0.0,
        'insurance_fee2': 0.0,
        'notary_name': '',                                                                                                                               
        'limited_for_enterprises': 'F',                                                                                                                               
        'limited_without_bc': 'F',                                                                                                                               
        'notary_fee': 0.0,
        'penalty_tawidh': 0.0,
        'penalty_tazir': 0.0,
        'recovery_wo_status': 'F'
      } 
      for initField, initValue in dictInitValues.iteritems():
        self.SetFieldByName(initField, initValue)
      
      self.ApplyChanges()
      oGRec = atutil.GeneralObject(recSrc)
      if oGRec.hasAttribute('LFinFacility.facility_no'):
        facility_no = oGRec.getAttrByName('LFinFacility.facility_no')
        self.facility_no = facility_no     
        oFacility = self.Helper.GetObject("FinFacility", facility_no)
        #raise Exception, facility_no
        if oFacility.status == 'T':
          raise Exception, "Fasilitas sudah ditutup"
        oFacility.status = "G"
        facilityAttrs = \
          [ 
            'Nomor_Nasabah=no_nasabah',
            'NasabahGroup',
            'contracttype_code',
            'nama_rekening=@core.Nasabah:no_nasabah->nama_nasabah',
            'kode_valuta=currency_code',
            'kode_cabang',
            'kode_pof',                
            #'tgl_akad=contract_date', 
            'contract_number=contract_no', 
            'legal_binding*', 
            'AMCode', 
            'dropping_due_date', 
            'reviewer*', 
            'approver*',
            'kode_notaris*'
          ]
        atutil.transferAttributes(helper, facilityAttrs, self, oFacility)
        listTransfer = \
          [ 
            'automatic_payment*','tgl_akad','adm_fee*','limited_without_bc*','limited_for_enterprises*',
            'linked_with_bank_group*','linked_with_bank*','opening_date', 
            'description*','norek_costpayment=*LCostPayment.Nomor_Rekening', 
            'other_fee*','period_unit*','is_executing*' , 
            'period_count','penalty_type*','penalty_fix*','automatic_penalty_debit*', 
            #'insurance_company=LPerusahaanAsuransi.Nama_Singkat', 'kode_asuransi' ,
            #'insurance_company2=LPerusahaanAsuransi2.Nama_Singkat', 'kode_asuransi2' ,
            'tanggal_aktifitas_terakhir=#now','targeted_eqv_rate',
            'norek_paymentsrc=*LPaymentSrc.Nomor_Rekening',
            'insurance_fee*','insurance_fee2*', 'notary_name*', 'notary_fee*',
            'insurance_issued_date*','insurance_due_date*','insurance_issued_date2*','insurance_due_date2*'
            ,'external_account_no*','external_customer_no*','payment_by_payroll*'  
          ]
        if oGRec.hasAttribute('LFinProduct.product_code'):
          listTransfer.extend([
            'product_code=LFinProduct.product_code',
            'col_model=@FinProduct:LFinProduct.product_code->default_col_model',
            'dropping_model=@FinProduct:LFinProduct.product_code->dropping_model', 
            #'financing_model=@FinProduct:LFinProduct.product_code->financing_model',
            #'penalty_type=@FinProduct:LFinProduct.product_code->penalty_type',
            #'penalty_fix=@FinProduct:LFinProduct.product_code->penalty_fix',
            'penalty_gp=@FinProduct:LFinProduct.product_code->penalty_gp',
            'penalty_maxp=@FinProduct:LFinProduct.product_code->penalty_maxp',
            'penalty_percentage=@FinProduct:LFinProduct.product_code->penalty_percentage',
          ])
        #--
      else:          
        if oGRec.hasAttribute('LCustomer.Nomor_Nasabah'):  
          no_nasabah = 'Nomor_Nasabah=LCustomer.Nomor_Nasabah'
        else:                                                      
          no_nasabah = 'lfinfacility.no_nasabah'
           
        listTransfer = \
          [ 
            'automatic_payment*','tgl_akad', 'opening_date', 'adm_fee*', 'description*','limited_without_bc*','limited_for_enterprises*','is_executing*',
            'linked_with_bank_group*','linked_with_bank*','NasabahGroup=*LNasabahGroup.Nomor_Nasabah', 
            'legal_binding', 'other_fee*', 'period_count','norek_costpayment=*LCostPayment.Nomor_Rekening', 
            'contract_number*', 'targeted_eqv_rate','period_unit*','penalty_type*','penalty_fix*','automatic_penalty_debit*', 
            no_nasabah, 
            'AMCode=*LAccountManager.AgentCode', 
            #'insurance_company=LPerusahaanAsuransi.Nama_Singkat', 'kode_asuransi' ,
            #'insurance_company2=LPerusahaanAsuransi2.Nama_Singkat', 'kode_asuransi2' ,
            'tanggal_aktifitas_terakhir=#now',
            'kode_cabang=*LCabang.kode_cabang',
            'norek_paymentsrc=*LPaymentSrc.Nomor_Rekening',
            'nama_rekening=@core.Nasabah:LCustomer.Nomor_Nasabah->nama_nasabah',
            'insurance_fee*', 'insurance_fee2*', 'notary_name*', 'notary_fee*',
            'reviewer=*LReviewer.refdata_id','approver=*LApprover.refdata_id','kode_notaris=*LNotaris.kode_notaris',
            'dropping_due_date*','insurance_issued_date*','insurance_due_date*','insurance_issued_date2*','insurance_due_date2*'
            ,'external_account_no*','external_customer_no*','payment_by_payroll*'  
          ]
        #--
        if oGRec.hasAttribute('LFinProduct.product_code'):
          listTransfer.extend([
            'product_code=LFinProduct.product_code',
            'kode_valuta=@FinProduct:LFinProduct.product_code->currency_code',
            'col_model=@FinProduct:LFinProduct.product_code->default_col_model',
            'dropping_model=@FinProduct:LFinProduct.product_code->dropping_model', 
            #'financing_model=@FinProduct:LFinProduct.product_code->financing_model',
            'contracttype_code=@FinProduct:LFinProduct.product_code->contracttype_code'
          ])
        #--
      
      atutil.transferAttributes(helper, listTransfer, self, recSrc)
      self.period_count_initial = self.period_count 
      self.period_count_awal = self.period_count 
      self.targeted_eqv_rate_awal = self.targeted_eqv_rate
      
      if oGRec.hasAttribute('LFinProduct.product_code'): 
        oProduct = self.LFinProduct
        if oProduct.IsNull or (oProduct.kode_pof or "") == "":
          kode_pof = config.SysVarIntf.GetStringSysVar('PARAMETERS', 'default_pool_of_fund')
        else:
          kode_pof = oProduct.kode_pof
        self.kode_pof = kode_pof
        default_valuta = self.Config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultCurrency')
        self.currency_code_pl = default_valuta
        self.eff_is_peg_IEIR = oProduct.eff_is_peg_IEIR or 'F'
        self.eff_is_applied = oProduct.eff_is_applied or 'F'
      self.Config.FlushUpdates()
    #--
    
    def EditFinAccount(self, param):
      config = self.Config
      helper = self.Helper      
      recSrc = param['FinAccount']
      listTransfer = [ 
        'automatic_payment*',
        'description*',
        'limited_without_bc*',
        'limited_for_enterprises*',
        'is_executing*',
        'linked_with_bank_group*',
        'linked_with_bank*',
        'penalty_type*',
        'penalty_fix*',
        'automatic_penalty_debit*', 
        'AMCode=*LAccountManager.AgentCode', 
        'norek_paymentsrc=*LPaymentSrc.Nomor_Rekening',
        'reviewer=*LReviewer.refdata_id',
        'approver=*LApprover.refdata_id', 
        'kode_notaris=*LNotaris.kode_notaris', 
        'tgl_akad', 
        'dropping_due_date*',
        'notary_name*',
        'contract_number*',
        'add_contract_no*',
        'add_contract_date*'
        ,'external_account_no*','external_customer_no*','payment_by_payroll*'
      ]
      atutil.transferAttributes(helper, listTransfer, self, recSrc)
      
      config.FlushUpdates()
      

    def ProcessTransGroupDetails(self, transgroup, detailTrans):
      config = self.Config; mlu = config.ModLibUtils
      helper = self.Helper
      
      if len(detailTrans) == 0:
        return
      
      # default account code selisih
      acc_code_selisih = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultAccountSelisih') 
      
      oProduct = self.LFinProduct
      if oProduct.IsNull:
        product_code_filter = 'is null'
        product_code = '<undefined>'
      else:
        product_code = oProduct.product_code  
        product_code_filter = '= %s' % mlu.QuotedStr(product_code)
        
      dictMap = {}
      for dt in detailTrans:
        if dt.kode_account in (None,''):
          kode_tx_class = dt.kode_tx_class
          kode_account = dictMap.get(kode_tx_class, None)
          if kode_account == None:
            sSQL = '''
                select * from %s
                where kode_produk %s AND kode_interface = %s 
              ''' % (config.MapDBTableName('financingsys.glinterface'), product_code_filter, mlu.QuotedStr(kode_tx_class))
            #raise Exception, config.GetHomeDir()
            #raise Exception, sSQL 
            q = config.CreateSQL(sSQL).RawResult
            if q.Eof:
              #  raise Exception, "GLInterface kode_produk %s dan kode_interface %s tidak ditemukan" % (product_code, kode_tx_class)
              # kode_account = '211070700027' # '103130005040' # temporary default  103130005040
              kode_account = acc_code_selisih
              kode_rc = ''
            else: 
              kode_account = q.kode_account
              if kode_account == None or kode_account == '':
                kode_account = acc_code_selisih
                #kode_account = '211070700027' #'103130005040' # temporary default
              kode_rc = q.kode_rc
            dictMap[kode_tx_class] = kode_account
          else:
            pass
          
          dt.kode_account = kode_account
          
        if dt.kode_tx_class in (None,'') and dt.keterangan=='Pengakuan Amortisasi Biaya':
          dt.nomor_rekening = '%s-%s-%s' % (dt.kode_account,dt.kode_cabang,dt.kode_valuta)
          dt.ID_DETILTRANSGROUP = None
          dt.kode_jurnal = '11'
      #--
    #--

    def processDetilTransaksiUmum(self, id_group_dtu):
      config = self.Config; mlu = config.ModLibUtils
      helper = self.Helper
      acc_code_selisih = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultAccountSelisih')
      config.FlushUpdates() 
      dbutil.runSQL(config,
        '''
          UPDATE %(detiltransaksiumum)s dtu
          SET 
            (kode_account, rc_code) = 
            (
              SELECT 
                coalesce(gli.kode_account, %(acc_code_selisih)s) as kode_account, 
                kode_rc 
              FROM 
                %(glinterface)s gli, %(finaccount)s fa
              WHERE
                gli.kode_produk = fa.product_code AND
                fa.nomor_rekening = %(nomor_rekening)s AND
                gli.kode_interface = dtu.kode_tx_class
            )
          WHERE
            dtu.id_group = %(id_group)d AND (dtu.override_account is NULL or dtu.override_account = 'F') 
        ''' % {
          'detiltransaksiumum': config.MapDBTableName('core.detiltransaksiumum'),
          'acc_code_selisih': mlu.QuotedStr(acc_code_selisih),
          'glinterface': config.MapDBTableName('financingsys.glinterface'),
          'finaccount': config.MapDBTableName('financingsys.finaccount'),
          'nomor_rekening': mlu.QuotedStr(self.nomor_rekening),
          'id_group': int(id_group_dtu)   
        }
      )
      dbutil.runSQL(config,
        '''
          UPDATE %(detiltransaksiumum)s dtu
          SET
            kode_cabang = %(kode_cabang)s, kode_valuta = %(kode_valuta)s 
          WHERE
            dtu.id_group = %(id_group)d AND (dtu.override_cabang is NULL or dtu.override_cabang = 'F') 
        ''' % {
          'detiltransaksiumum': config.MapDBTableName('core.detiltransaksiumum'),
          'kode_cabang': mlu.QuotedStr(self.kode_cabang),
          'kode_valuta': mlu.QuotedStr(self.kode_valuta),
          'id_group': int(id_group_dtu)   
        }
      )
      config.ResetCache()
      
      pass
    #-- def processDetilTransaksiUmum
     
    def getPricingModel(self): # virtual method, should return pricing method, default is 'effective' == '1'
      return '1'
      
    def CostTransaction(self, parameters):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
            
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_trx = periodHelper.GetAccountingDate()
      
      # CREATE TRANSACTION
      kode_entri = 'FAC01' 
      keterangan = 'CREATE BIAYA %s-%s' % (self.nomor_rekening, self.nama_rekening)

      oTx = self.initTransaksi(kode_entri, tgl_trx, keterangan)
            
      #finacc = self.CastToLowestDescendant()
      #raise Exception,self.kode_jenis
      kode_jenis = self.kode_jenis
      fau = FinAccountUtils(config)
      list_cost = self.Ls_FinAccCost
      list_cost.First()
      entries = []
      while not list_cost.EndOfList:
        oCost = list_cost.CurrentElement
        amount = oCost.cost_amount
        if oCost.is_procced <> 'T' and amount>0:
          
          #get kode_tx_class, acc_code
          if self.contracttype_code=='W':
            kode_jenis=fau.DICT_KODE_JENIS[parameters.contracttype_code]
            self.norek_costpayment = parameters.GetFieldByName('LCostPayment.Nomor_Rekening')
            if oCost.LFinCostElement.is_amortized=='T': 
              kode_tx_class = fau.get_tx(kode_jenis,'deferred_balance')
              _pars=['1', kode_jenis, parameters.dropping_model, kode_tx_class]
              oCost.start_amortize_date=tgl_trx 
              oCost.end_amortize_date = parameters.facility_due_date
            elif oCost.LFinCostElement.is_amortized=='F': 
              kode_tx_class = fau.get_tx(kode_jenis,'pdp_adm')
              _pars=['2', kode_jenis, parameters.dropping_model, oCost.cost_element_id]
              
            #raise Exception, _pars
            acc_code = fau.get_glfacility(_pars)
            #raise Exception, acc_code
            _ket = '%s %s - %s' % (oCost.LFinCostElement.cost_element_name, self.facility_no, self.nama_rekening)
            #--
          
          else:
            if oCost.LFinCostElement.is_amortized=='T': 
              c_group='deferred_balance'
              acc_code = None
              oCost.start_amortize_date=tgl_trx 
            elif oCost.LFinCostElement.is_amortized=='F': 
              c_group='pdp_adm'
              _gac = fau.get_account_code(self.product_code, oCost.cost_element_id)
              acc_code = _gac[0]
              
            kode_tx_class = fau.get_tx(kode_jenis,c_group)
            _ket = '%s %s - %s' % (oCost.LFinCostElement.cost_element_name, self.nomor_rekening, self.nama_rekening)
            #--
          
          #create detil transaksi financing
          entries.append({'sign': 'C', 'amount': amount, 'kode_tx_class': kode_tx_class, 'keterangan':_ket, 'kode_account':acc_code})
        
          #raise Exception, '%s * %s' % (kode_jenis,kode_tx_class)
          #create detil transaksi rekening liability
          LCostPayment = self.LCostPayment
          if LCostPayment.IsNull:
            raise Exception, "Cost Payment account not defined"
          oDT = LCostPayment.BuatTransaksiManual('core.DetilTransaksi', 'D', amount);
          oDT.LTransaksi = oTx
          oDT.keterangan = _ket
          oDT.id_parameter_transaksi = '20'
          oDT.tanggal_transaksi = oTx.tanggal_transaksi
          oDT.kode_jurnal = '11'
          oDT.Proses()
        
          oCost.is_procced = 'T' 

        list_cost.Next()
        
      #raise Exception, entries
      oDetTxGroupCost = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupCost.ProcessDetails()
      #--
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTxGroupCost 
         
    def updateEndAmortizeDate(self, jns='new'):
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_trx = periodHelper.GetAccountingDate()
      list_cost = self.Ls_FinAccCost
      list_cost.First()
      oSchedule=self.LActiveSchedule
      while not list_cost.EndOfList:
        oCost = list_cost.CurrentElement
        if oCost.LFinCostElement.is_amortized=='T' and oCost.cost_amortized < oCost.cost_amount:
          if jns =='new':
            oCost.end_amortize_date = oSchedule.getLastPaymentDate()          
          elif jns =='added' and oCost.end_amortize_date in (0,None):
            oCost.end_amortize_date = oSchedule.getLastPaymentDate()          
        
        list_cost.Next()
      
    def prosesAYDA(self, parameters, jns_ayda):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      app = config.AppObject
      app.ConCreate('out')
      app.ConWriteln('Inisialisasi proses TUTUP...')
      fau = FinAccountUtils(config)
            
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_trx = periodHelper.GetAccountingDate()
      
      # CREATE TRANSACTION
      oTx = FinAccount.initTransaksi(self, parameters.kode_entri, tgl_trx, parameters.keterangan)
      #oTx = helper.CreatePObject("core.Transaksi", parameters.kode_entri)
      #oTx.SetStandardInfo(tgl_trx, parameters.keterangan)
      #oTx.status_otorisasi = 1
            
      app.ConWriteln(jns_ayda)
      entries = self.getTrxJournal(parameters,jns_ayda, self.kode_jenis)
        
      app.ConWriteln(str(entries))
      #app.ConRead('')
      entries = fau.removeZeroAmountFromJournal(entries)
      oDetTxGroupCost = self.CreateTransactionGroup(parameters.kode_entri, parameters.keterangan, oTx, entries)  
      oDetTxGroupCost.ProcessDetails()
      #--            
      if jns_ayda =='input_AYDA':
        self.ayda_flag  = 'T'
        self.ayda_date = parameters.ayda_date
        #oFinAcc.AutomaticUnLink()
        self.RealizedRemainCost()
        self.status_rekening = 9
      elif jns_ayda =='finish_AYDA':        
        self.ayda_flag  = 'F'
        self.status_rekening = 3
        laba_ayda = parameters.nilai_jual_aktiva+parameters.ayda_balance
        #amount = -(parameters.ayda_balance+laba_ayda)
        amount = parameters.nilai_jual_aktiva  
        app.ConWriteln('amount debet rek '+str(amount))

        #create detil transaksi rekening liability
        no_account = parameters.GetFieldByName('LDebet_Account.Nomor_Rekening')
        ldestacc = helper.GetObject('core.RekeningTransaksi', no_account)

        oDT = ldestacc.BuatTransaksiManual('core.DetilTransaksi', 'D', amount);
        oDT.LTransaksi = oTx
        oDT.keterangan = oTx.keterangan
        oDT.tanggal_transaksi = oTx.tanggal_transaksi
        oDT.kode_jurnal = '11'
        oDT.Proses()
      elif jns_ayda =='ppanp_AYDA':
        pass
        #self.ayda_flag  = 'F'
        #self.status_rekening = 3      
      elif jns_ayda =='delete_AYDA':
        self.ayda_flag  = 'F'
        self.status_rekening = 3      
      elif jns_ayda =='hapus_buku':
        self.write_off_status  = 'T'
        self.RealizedRemainCost()
        self.status_rekening = 8      
        self.write_off_deleted  = 'F'
        self.write_off_date = parameters.tgl_transaksi     
      elif jns_ayda =='recovery_wo':  
        self.recovery_wo_status  = 'T'
        if (parameters.recovery_pokok + self.write_off_balance) == 0.0 and (parameters.recovery_margin + self.write_off_margin_balance) == 0.0:
          self.repayment_col_level = 1
      elif jns_ayda =='hapus_tagih':
        self.write_off_deleted  = 'T'
        self.write_off_deleted_date = parameters.tgl_transaksi     
      else:
        raise Exception, 'jenis trx tutup tidak terdefinisi'

      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      app.ConWriteln('OK')
      #raise Exception, 'Under Construction'

      return oDetTxGroupCost      
    #--    
  
    def dropping(self, parameters): # virtual method to detailed in descendant classes #FINACCOUNT
      if self.drop_counter > 0 and self.dropping_model in ('A', 'O'):
        raise Exception, "Rekening ini sudah pernah dropping sebelumnya"
      self.drop_counter = self.drop_counter + 1 
      self.overall_col_level = 1 
      self.performance_col_level = 1 
      self.prospect_col_level = 1 
      self.repayment_col_level = 1
      self.so_col_level = 1
      self.manual_col_level = 1
      self.coll_deduct_recalc = 'T'
      self.col_eval_required ='T'
      oFacility = self.LFinFacility
      if not oFacility.IsNull:
        oFacility.id_otorentri = None if self.contracttype_code=='G' else self.id_otorentri
        oFacility.notifyDropping(self, parameters.amount_dropping)
      self.dropping_amount = self.loan_amount
      
      # hitung nilai IEIR
      self.createIEIR()
        
    def createIEIR(self):
      config = self.config
      
      list_cost = self.Ls_FinAccCost
      amortize_cost = 0
      while not list_cost.EndOfList:
        oCost = list_cost.CurrentElement
        if oCost.LFinCostElement.is_amortized=='T': 
          amortize_cost += oCost.cost_amount
        list_cost.Next()

      q = self.Config.CreateSQL("""
          SELECT sum(PRINCIPAL_AMOUNT) as sum_pokok, sum(PROFIT_AMOUNT + MUKASAH_AMOUNT) as sum_margin, max(INITIAL_PRINCIPAL_AMT) as initial_pokok 
          FROM %(FINPAYMENTSCHEDDETAIL)s a, %(FINPAYMENTSCHEDULE)s b 
          WHERE a.ID_SCHEDULE=%(id_schedule)s AND a.ID_SCHEDULE=b.ID_SCHEDULE AND a.SEQ_NUMBER>0
          GROUP BY TO_CHAR(a.SCHED_DATE,'YYYYMM')
          ORDER BY TO_CHAR(a.SCHED_DATE,'YYYYMM')
        """ % {
          'FINPAYMENTSCHEDULE': config.MapDBTableName('FINPAYMENTSCHEDULE'),
          'FINPAYMENTSCHEDDETAIL': config.MapDBTableName('FINPAYMENTSCHEDDETAIL'),
          'id_schedule': self.id_schedule
        }).RawResult
      
      cashInSeries =[]
      initialCashFlow = -(q.initial_pokok or 0) + amortize_cost 
      while not q.Eof :
        cashInSeries.append(q.sum_pokok+q.sum_margin)
          
        q.Next()
      #--end:while
      ieir = 0.0#libs.calculateEIR(initialCashFlow, cashInSeries)        
      self.eff_ieir = ieir                               #nilai IEIR
      self.eff_initial_cash_flow = initialCashFlow
      self.eff_balance = initialCashFlow #total pokok ditambah total biaya yang diamortisasi
      self.eff_income_balance = 0.0
      self.eff_correction_balance = 0                    # 
      self.eff_check_IEIR = 'F'                          # dropping default F diset 'T' ketika ubah jadwal
      #self.eff_is_peg_IEIR = 'F'                         # diambil dari produk
      #raise Exception,  self.eff_balance
    
    def disburse(self, kode_entri, parameters): #disburse-finacc
      helper = self.Helper
      config = self.Config
      if self.is_external == 'T':
        norek_tujuan = parameters.norek_tujuan
      else:
        norek_tujuan = parameters.GetFieldByName('LDisbursalDest.Nomor_Rekening')
      
      parameters = atutil.GeneralObject(parameters)
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_transaksi = periodHelper.GetAccountingDate()
      if not parameters.hasAttribute('keterangan'):
        keterangan = 'PENCAIRAN %s' % self.nomor_rekening
      else:
        keterangan = parameters.keterangan
      nomor_rekening = parameters.nomor_rekening 
      
      oTx = FinAccount.initTransaksi(self, kode_entri, tgl_transaksi, keterangan)
      #raise Exception, 'txclass %s' % self.base_kode_tx_class 
      entries = [ 
          {'sign': 'D', 'amount': parameters.disbursal_amount, 'kode_tx_class': '%s011' % self.base_kode_tx_class},
        ]
      oDetTxGroupDropping = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)
      oDetTxGroupDropping.ProcessDetails()
      
      ldestacc = helper.GetObject('core.RekeningTransaksi', norek_tujuan)
      oDT = ldestacc.BuatTransaksiManual('core.DetilTransaksi', 'C', parameters.disbursal_amount)      
      oDT.LTransaksi = oTx
      oDT.keterangan = oTx.keterangan
      oDT.tanggal_transaksi = oTx.tanggal_transaksi
      oDT.kode_jurnal = '11'
      oDT.Proses()
      
      if parameters.hasAttribute('LDevelover.Nomor_Rekening'):
        self.TrfToDevelover(parameters, oTx, parameters.disbursal_amount)
           
      finacc = helper.GetObject('FinAccount', nomor_rekening)
      finacc.LDisbursalDest = ldestacc
      finacc.norek_disbursaldest = norek_tujuan 
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTxGroupDropping
    #-- disburse-finacc
    
    def TrfToDevelover(self, parameters, oTx, drop_amount):
      helper = self.Helper
      config = self.Config
      if parameters.getAttrByName('LDevelover.Nomor_Rekening') not in (None,''):
        norek = [
          [parameters.getAttrByName('LDisbursalDest.Nomor_Rekening'), 'D'],
          [parameters.getAttrByName('LDevelover.Nomor_Rekening'), 'C']
        ]
        
        for i in norek:
          norek_tujuan=i[0]
          mnemonic=i[1]
          
          ldestacc = helper.GetObject('core.RekeningTransaksi', norek_tujuan)
          oDT = ldestacc.BuatTransaksiManual('core.DetilTransaksi', mnemonic, drop_amount)       
          oDT.LTransaksi = oTx
          oDT.keterangan = 'PEMBAYARAN A.N. %s' % self.Nama_Rekening #oTx.keterangan
          oDT.tanggal_transaksi = oTx.tanggal_transaksi
          oDT.kode_jurnal = '11'
          oDT.Proses()
      #-- disburse-finacc
      
    def getLoanAmount(self): # virtual method
      raise Exception, "getLoanAmount() not defined in this class"
      
    def createShiftedSchedule(self, tgl_angsur_awal, new_tenor, editScheduleType): 
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')

      oSchedule = self.LActiveSchedule
      
      if oSchedule.IsNull:
        raise Exception, "Tidak ada jadwal angsuran aktif"
      seq = oSchedule.getCurrentPaymentSeq()
      
      tgl_angsur_awal = int(round(timeutil.stdDate(self.Config, tgl_angsur_awal)))
      
      oTmpSchedule = self.LTmpDroppingSchedule
      ## UTK SEMENTARA SAAT INI SELALU HAPUS JADWAL TEMPORARY EXISTING 
      #if oTmpSchedule.IsNull:
      #  bDelExisting = False
      #  bCreateNew = True
      #else:
      #  ex_tgl_angsur_awal = int(round(timeutil.stdDate(self.Config, oTmpSchedule.first_install_date)))
      #  bDelExisting = tgl_angsur_awal != ex_tgl_angsur_awal
      #  bCreateNew = bDelExisting
      
      ## UTK SEMENTARA SAAT INI SELALU HAPUS JADWAL TEMPORARY EXISTING
      bCreateNew = True
      bDelExisting = True
        
      if bDelExisting:
        if not oTmpSchedule.IsNull: oTmpSchedule.Delete()
        
      tgl_angsur_awal = tgl_angsur_awal / 1.0
      if bCreateNew:
        oTmpSchedule = self.Helper.CreatePObject("FinPaymentSchedule")
        oTmpSchedule.shiftFromExistingSchedule(self, tgl_angsur_awal, oSchedule, seq, new_tenor, editScheduleType)
        
        self.LTmpDroppingSchedule = oTmpSchedule        
        oTmpSchedule.setAsTemporary()       
        
      #--
      self.Config.FlushUpdates()
      return oTmpSchedule
    #--
            
    def createOrGetTemporarySchedule(self, tgl_angsur_awal, infoSchedule):
      classname = self.ClassName.lower()
      amount = self.getLoanAmount()
      period = self.period_count
      eq_rate = self.targeted_eqv_rate
      
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      
      tgl_dropping = int(round(periodHelper.GetAccountingDate()))
      tgl_angsur_awal = int(round(timeutil.stdDate(self.Config, tgl_angsur_awal)))
      
      oTmpSchedule = self.LTmpDroppingSchedule
      ## UTK SEMENTARA SAAT INI SELALU HAPUS JADWAL TEMPORARY EXISTING KARENA USER BISA UBAH2 MUKASAH
      bCreateNew = True
      bDelExisting = True
      
      ## UTK SEMENTARA SAAT INI SELALU HAPUS JADWAL TEMPORARY EXISTING KARENA USER BISA UBAH2 MUKASAH      
      #if oTmpSchedule.IsNull:
      #  bDelExisting = False
      #  bCreateNew = True
      #else:
      #  ex_tgl_dropping = int(round(timeutil.stdDate(self.Config, oTmpSchedule.dropping_date)))
      #  ex_tgl_angsur_awal = int(round(timeutil.stdDate(self.Config, oTmpSchedule.first_install_date)))
      #  bDelExisting = (tgl_dropping != ex_tgl_dropping) or (tgl_angsur_awal != ex_tgl_angsur_awal)
      #  bCreateNew = bDelExisting        
        
      if bDelExisting:
        if not oTmpSchedule.IsNull: oTmpSchedule.Delete()
        #pass
        
      reschedule = infoSchedule['reschedule']
      if reschedule:
        oSchedule = self.LActiveSchedule
        seq = oSchedule.getCurrentPaymentSeq()

        amount = infoSchedule['sisa_pokok']
        period = int(infoSchedule['new_tenor'] - seq+1)
        eq_rate = infoSchedule['eqv_rate']
          
      tgl_dropping = tgl_dropping / 1.0
      tgl_angsur_awal = tgl_angsur_awal / 1.0
      if bCreateNew:
        install = libs.installment(period)
        #install.runSimulation(self.getPricingModel(), amount, self.targeted_eqv_rate)
        install.runSimulation(infoSchedule['pricing_model'], amount, eq_rate)
        total_margin = install.margin

        oTmpSchedule = self.Helper.CreatePObject("FinPaymentSchedule")
        if classname in ('finmurabahahaccount','finistishnaaccount') :
          mukasah_rate = float(infoSchedule['mukasah_rate'])
          mukasah_seq_limit = int(infoSchedule['mukasah_seq_limit'])          
        
          mksh_install = None
          if mukasah_rate > 0:
            n_rate = eq_rate - mukasah_rate      
            mksh_install = libs.installment(period)
            mksh_install.runSimulation(infoSchedule['pricing_model'], amount, n_rate)

          oDropDetailSched = oTmpSchedule.initFromSimulationWithMukasahPct(self, tgl_dropping, tgl_angsur_awal, install, mukasah_rate, mukasah_seq_limit, False, mksh_install,reschedule)
        elif classname in ('finijarahaccount','finqardhaccount') :
          oDropDetailSched = oTmpSchedule.initFromSimulation(self, tgl_dropping, tgl_angsur_awal, install, False, reschedule) # tanpa mukasah
          #oDropDetailSched = oTmpSchedule.initFromSimulationWithMukasahPct(self, tgl_dropping, tgl_angsur_awal, install, mukasah_rate, mukasah_seq_limit, False, mksh_install,reschedule)
        elif classname in ['finmusyarakahaccount', 'finmudharabahaccount'] :
          nisbah_bank = self.profit_share
          if infoSchedule['is_balloon_payment'] == 'T':
            install.isBalloonPayment()
          oDropDetailSched = oTmpSchedule.initFromMusyarakahSimulation(self, tgl_dropping, tgl_angsur_awal, install, nisbah_bank, False,reschedule)
        elif classname == 'finmmqaccount' :
          oDropDetailSched = oTmpSchedule.initFromMMQSimulation(self, tgl_dropping, infoSchedule['pricing_model'], tgl_angsur_awal, False,reschedule, period, amount, eq_rate)
        elif classname == 'finprkaccount' :
          oDropDetailSched = oTmpSchedule.initFromZeroSimulation(self, tgl_dropping, tgl_angsur_awal, install, False)
        else :
          raise Exception, "createOrGetTemporarySchedule : must be customized for account type : %s" % classname
        self.LTmpDroppingSchedule = oTmpSchedule
        oTmpSchedule.setAsTemporary()
      #--
      self.Config.FlushUpdates()
      return oTmpSchedule
    #--
    
    # method virtual, harus diimplementasikan pada masing2 akad, jika tidak ada, implementasikan dengan return [] #list kosong untuk akad ybs
    def customizedRescheduleDetails(self): # finacc
      pass
      #classname = self.CastToLowestDescendant().ClassName.lower()
      #raise Exception, "customizedReschedule must be implemented for %s " % classname
    
    #create jadwal dari grid.
    def schedFromGrid(self, params, editschedule_type='edit-schedule'): # finacc
      helper = self.Helper
      config = self.Config
      if editschedule_type=='edit-schedule':
        recInput = params.uipRes.GetRecord(0)
      elif editschedule_type=='dropping':
        recInput = params.uipDropping.GetRecord(0)
      recSched = params.uipJadwal
      app = config.AppObject      
      app.ConCreate('out')
      
      finacc = self.CastToLowestDescendant()
      classname = self.ClassName.lower()
      oTmpSchedule = helper.CreatePObject("FinPaymentSchedule")
      _seq_begin = 1
      if editschedule_type=='edit-schedule':
        oSchedule = self.LActiveSchedule
        seq = oSchedule.getCurrentPaymentSeq()
        oTmpSchedule.getExistingSchedule(self, oSchedule, recInput.tgl_mulai_angsur, seq)
        _seq_begin = seq

      akru_balance = 0
      if classname in ['finmurabahahaccount','finistishnaaccount']:
        akru_balance = finacc.profit_accrue_balance or 0 
      elif classname in ['finijarahaccount']:
        akru_balance = finacc.lease_profit_recv or 0 

      #app.ConWrite(str(akru_balance))
      app.ConWriteln('Inisialisasi Proses Upload Jadwal Angsuran\n')
      _margin_new = 0;_max_seq=1;_max_date=0 
      for i in range(0, recSched.RecordCount):
        _schd = recSched.GetRecord(i)
        
        oDet = helper.CreatePObject("FinPaymentSchedDetail")
        oDet.LFinPaymentSchedule = oTmpSchedule
        oDet.seq_number = _seq_begin
        oDet.sched_date = _schd.sched_date
        oDet.principal_amount = _schd.principal_amount
        oDet.profit_amount = _schd.profit_amount
        _margin_new += _schd.profit_amount
        oDet.principal_outstanding = _schd.principal_outstanding
        
        if akru_balance > 0.01:
          akru_amount = akru_balance if akru_balance<_schd.profit_amount else _schd.profit_amount
          oDet.accrued_amount = akru_amount
          akru_balance -= akru_amount
        
        #app.ConWriteln("%s -- %s" % (_schd.seq_number,oDet.accrued_amount))
        if self.contracttype_code in ['A','H']:
          oDet.mukasah_amount = _schd.mukasah_amount
        
        elif self.contracttype_code in ['D']:
          oDet.profit_amount = 0 # untuk qardh tidak ada margin          

        elif self.contracttype_code in ['B','C','G']:
          oDet.profit_amount_whole = _schd.profit_amount_whole      
          oDet.profit_ratio        = _schd.profit_ratio 

        #elif self.contracttype_code in ['G']:
        #  oDet.profit_amount_whole = _schd.profit_amount_whole      
          
        _max_seq = _seq_begin if _seq_begin > _max_seq else _max_seq  
        _max_date = _schd.sched_date if _schd.sched_date > _max_date else _max_date  

        _seq_begin += 1
        #app.ConWriteln(self.contracttype_code)         
        #app.ConWriteln(str(_schd.profit_amount_whole))         
      
      #app.ConRead('')
      oTmpSchedule.initial_profit_amt = _margin_new
      finacc.LTmpDroppingSchedule = oTmpSchedule
      oTmpSchedule.period = _max_seq
      self.period_count = _max_seq
      self.due_date     = _max_date
      
      #raise Exception, str(_max_seq)+' '+str(self.period_count)
      #--     
    
    def reschedule(self, parameters): 
      helper = self.Helper
      config = self.Config
      #parameters = atutil.GeneralObject(parameters)
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      
      tgl_restruktur = parameters.tgl_restruktur
      #-- kalau sudah fix, pakai yang ini saja
      tgl_accounting = periodHelper.GetAccountingDate()  
      tgl_mulai_angsur = parameters.tgl_mulai_angsur
      
      #raise Exception, tgl_mulai_angsur
      if parameters.editschedule_type != 'cust-schedule' :
        if int(round(timeutil.stdDate(config, tgl_restruktur))) > int(round(timeutil.stdDate(config, tgl_mulai_angsur))):
          raise Exception, "Tanggal angsur awal harus >= tanggal reschedule"        
      else :
        pass # jika cust schedule tidak ada validasi tanggal
              
      ## ASSIGN PAYMENT SCHEDULE
      oldSchedule = self.LActiveSchedule
      tempSchedule = self.LTmpDroppingSchedule
      if tempSchedule.IsNull:
        raise Exception, "Jadwal angsur baru belum dibuat"
      if int(round(timeutil.stdDate(config, tgl_mulai_angsur))) != int(round(timeutil.stdDate(config, tempSchedule.first_install_date))):
        raise Exception, "Tanggal jadwal angsur manual berbeda dengan tanggal angsur awal"
      self.LTmpDroppingSchedule = None
      oldSchedule.is_active ='F'

      if self.contracttype_code in ('A','H'):
        self.mukasah_rate = parameters.rate_mukasah
        self.max_dpd_mukasah = parameters.max_dpd_mukasah
        tempSchedule.mukasah_seq_limit = parameters.mukasah_seq_limit

      #raise Exception, parameters.pricing_model 
      self.pricing_model = parameters.pricing_model
      self.LActiveSchedule = tempSchedule
      tempSchedule.setAsActive()
      self.due_date = tempSchedule.getLastPaymentDate()
      
      # SET PARAMETER KOLEKTIBILITAS DAN INFO RESCHEDULE
      #self.restructure_counter = (self.restructure_counter or 0) + 1
      self.restructure_last_date = tgl_restruktur
      #self.eval_arrear_period = 3
      _period = tempSchedule.getMaxSeq()
      self.arrear_period = 0 # clear arrears
      tempSchedule.period = _period
      #raise Exception, tempSchedule.period
      #if self.contracttype_code<>'G':
      self.targeted_eqv_rate = parameters.new_eqv_rate
      self.period_count = _period
      # CREATE TRANSACTION
      #oTx = helper.CreatePObject("core.Transaksi", parameters.kode_entri)
      #oTx.SetStandardInfo(tgl_accounting, parameters.keterangan)
      #oTx.status_otorisasi = 1

      #lstDetails = self.customizedRescheduleDetails()
            
      #oDetTxGroup = self.CreateTransactionGroup(parameters.kode_entri, parameters.keterangan, oTx, lstDetails, '.TxRestructure')
      
      #oDetTxGroup.restructure_info = parameters.keterangan
      #oDetTxGroup.approval_ref = parameters.no_referensi
      #oDetTxGroup.restructure_date = tgl_restruktur
      #oDetTxGroup.restructure_counter = self.restructure_counter
      
      #oDetTxGroup.ProcessDetails()
      #oTx.CreateJournal()
      #self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      #self.period_count = self.LActiveSchedule.period     
      self.eff_check_IEIR = 'T'                          # dropping default F diset 'T' ketika ubah jadwal
      # evaluasi kolektibilitas --> leave it to zul
      pass
      
    #def updateCollectibility(self, parameters): # virtual method to be implemented in descendant classes
      #raise Exception, "Method updateCollectibility() not implemented in this class"
    #  pass
    
    def updateCollectibility(self, parameters):
      #RuleTransaction = modman.getModule(self.Config, "RuleTransaction")
      helper = self.Helper ; config = self.Config
      recSrc = parameters
      atutil.transferAttributes(helper, 
        [ 'prospect_col_level', 'prospect_col_desc',
          'performance_col_level', 'performance_col_desc', 
          'repayment_col_level', 'manual_col_level',
          'overall_col_level','expired_manual_col' 
        ],
        self, recSrc
      )
      
      #so_col_level = self.getSoColLevel()
      
      #new_socollevel = max([so_col_level,parameters.overall_col_level])
      #set aux fields
      self.col_eval_required = 'T'
      self.reserve_eval = 'T'
      self.so_col_level = recSrc.manual_col_level
      
      #insert to history kolektibilitas
      oHC = helper.CreatePObject('FinHistCollect', {'HistColect':recSrc})
      
      pass
    #--
    
    # menentukan level kolektibilitas baru tanpa rule sql diambil dari proses pod
    def analyzeColLevel_new(self, param):
      config = self.config
      mlu = config.ModLibUtils
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      oToday = periodHelper.GetToday()
      float_oToday = oToday.GetDate()
      strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)
      d_params ={
          'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
          'finpaymentschedule': config.MapDBTableName('finpaymentschedule'),
          'finpaymentscheddetail': config.MapDBTableName('finpaymentscheddetail'),
          'finaccount': config.MapDBTableName('finaccount'),
          'finproduct': config.MapDBTableName('finproduct'),
          'date': mlu.QuotedStr(strDate),
          'norek': mlu.QuotedStr(self.nomor_rekening)
      }      
      sql = ''' 
        SELECT
          fa.nomor_rekening,
          round( sum(
            case 
              when psd.seq_number = 0 then psd.principal_amount + psd.profit_amount
              when months_between(to_date(%(date)s), psd.SCHED_DATE + case when fp.arrear_grace_period IS NULL then 0 else fp.arrear_grace_period end) >=  9 then psd.principal_amount + psd.profit_amount
              else 0.0
            end
          ),2) as zcol5,
          round( sum(
            case 
              when psd.seq_number = 0 then psd.principal_amount + psd.profit_amount
              when months_between(to_date(%(date)s), psd.SCHED_DATE + case when fp.arrear_grace_period IS NULL then 0 else fp.arrear_grace_period end) >=  6 then psd.principal_amount + psd.profit_amount
              else 0.0
            end
          ),2) as zcol4,
          round( sum(
            case 
              when psd.seq_number = 0 then psd.principal_amount + psd.profit_amount
              when months_between(to_date(%(date)s), psd.SCHED_DATE + case when fp.arrear_grace_period IS NULL then 0 else fp.arrear_grace_period end) >=  3 then psd.principal_amount + psd.profit_amount
              else 0.0
            end
          ),2) as zcol3,
          round( sum(
            case 
              when psd.seq_number = 0 then psd.principal_amount + psd.profit_amount
              when psd.SCHED_DATE + (case when fp.arrear_grace_period IS NULL then 0 else fp.arrear_grace_period end) <= to_date(%(date)s) then psd.principal_amount + psd.profit_amount
              else 0.0
            end
          ),2) as zcol2,
    			round(months_between(to_date(%(date)s), min(case when PSD.SEQ_NUMBER=1 then psd.SCHED_DATE end)),5) as first_payment_age
        FROM
          %(finaccount)s fa,
          %(finproduct)s fp,
          %(rekeningtransaksi)s rt,
          %(finpaymentscheddetail)s psd
        WHERE
          rt.status_rekening = 1 and
          fa.nomor_rekening = rt.nomor_rekening and
          fa.product_code = fp.product_code and
          psd.ID_SCHEDULE = fa.ID_SCHEDULE and
          fa.NOMOR_REKENING=%(norek)s
        GROUP BY fa.nomor_rekening
      ''' % d_params
      
      #raise Exception,sql
      rs = config.CreateSQL(sql).RawResult
      
      #raise Exception,rs.zcol3
      
      classname = self.ClassName.lower() 
      outs_balance = self.saldo+self.arrear_balance
      if classname == "finijarahaccount" :       
        outs_balance = -(self.getPrincipalOutstanding()+self.getMarginOutstanding())

      outs_balance = round(outs_balance,2)
      if rs.first_payment_age < 0:
        repayment_col_level = 1
      elif rs.first_payment_age >= 0 and rs.first_payment_age<3:
        if outs_balance >= rs.zcol2:
          repayment_col_level = 1  
        else:
          repayment_col_level = 2  
      elif rs.first_payment_age >= 0 and rs.first_payment_age<6:
        if outs_balance >= rs.zcol2: 
          repayment_col_level = 1 
        elif outs_balance >= rs.zcol3: 
          repayment_col_level = 2 
        else: 
          repayment_col_level = 3 
      elif rs.first_payment_age >= 0 and rs.first_payment_age<9:
        if outs_balance >= rs.zcol2: 
          repayment_col_level = 1 
        elif outs_balance >= rs.zcol3: 
          repayment_col_level = 2 
        elif outs_balance >= rs.zcol4: 
          repayment_col_level = 3 
        else: 
          repayment_col_level = 4 
      else:
        if outs_balance >= rs.zcol2: 
          repayment_col_level = 1 
        elif outs_balance >= rs.zcol3: 
          repayment_col_level = 2 
        elif outs_balance >= rs.zcol4: 
          repayment_col_level = 3 
        elif outs_balance >= rs.zcol5: 
          repayment_col_level = 4 
        else: 
          repayment_col_level = 5 

      #raise Exception, '%s %s' % (round(outs_balance,2)>=rs.zcol2, repayment_col_level)
      lcol = []
      if self.col_model == 'C' :
        lcol = [repayment_col_level, param.prospect_col_level, param.performance_col_level, param.manual_col_level]
      else : # 'R'
        lcol = [repayment_col_level, param.manual_col_level]
      tmp_col_level = max(lcol) # kolektibilitas sementara, mempertimbangkan manual col, belum melibatkan so col level 
      #hitung calc single obligor col level
      so_col_level = self.getSoColLevel()
       
      overall_col_level = max([ tmp_col_level, so_col_level ]) # kolektibilitas akhir, melibatkan so col level
      if param.manual_col_level <>0 and param.manual_col_level < overall_col_level: 
        overall_col_level = param.manual_col_level      

      return repayment_col_level, tmp_col_level, overall_col_level 
    #--
    
    def getSoColLevel(self):
      config = self.config
      sql = '''     
          select rc.nomor_nasabah,		
            max(fa.repayment_col_level) as repayment_col_level,		
            max(fa.performance_col_level) as performance_col_level,		
            max(fa.prospect_col_level) as prospect_col_level,		
            max(fa.manual_col_level) as manual_col_level		
          from		
            %(rekeningcustomer)s rc,		
            %(finaccount)s fa		
          where 		
            fa.nomor_rekening = rc.nomor_rekening and		
            rc.nomor_nasabah = '%(no_nasabah)s' and 		
            rc.nomor_rekening <> '%(no_rek)s'
          group by rc.nomor_nasabah 		
      ''' % {
        'rekeningcustomer': config.MapDBTableName('core.rekeningcustomer'),
        'finaccount': config.MapDBTableName('finaccount'),
        'no_nasabah': self.nomor_nasabah,
        'no_rek': self.nomor_rekening
      }
      rs = self.Config.CreateSQL(sql).RawResult
      
      so_col_level = 1
      if not rs.Eof:
        lcoll=[rs.repayment_col_level,rs.performance_col_level,rs.prospect_col_level,rs.manual_col_level]
        so_col_level = max(lcoll)
      
      return so_col_level  
      
      
    # mendapatkan saldo / outstanding aktual
    def getActualOutstanding(self):
      config = self.Config
      t_act = 0
      sql = ''' select saldo from %s where nomor_rekening = '%s' ''' % ( config.MapDBTableName('core.rekeningtransaksi'), self.nomor_rekening)
      rs = self.Config.CreateSQL(sql).RawResult
      if not rs.Eof:
        t_act = rs.saldo
      t_act = t_act if (t_act != None) else 0
      return t_act
    #--
         
    def getCalcPPAInfo(self, params):
      prm = modman.getModule(self.Config, "RuleParameter")
      saldo             = self.getActualOutstanding()
      nilai_agunan      = self.collateral_value
      outstanding_pokok = 0
      classname = self.ClassName.lower() 
      if classname in ("finmurabahahaccount","finistishnaaccount") :       
        outstanding_pokok =  saldo + self.mmd_balance + self.arrear_balance + self.profit_arrear_balance
      elif classname in ["finmusyarakahaccount", "finmudharabahaccount", "finmmqaccount", "finprkaccount"] :
        outstanding_pokok =  saldo + self.arrear_balance
      elif classname == "finqardhaccount" :
        outstanding_pokok =  saldo        
      elif classname == "finijarahaccount" :
        #outstanding_pokok =  self.lease_principal_recv + self.lease_profit_recv        
        outstanding_pokok =  0       
      else : raise Exception, "finaccount.getCalcPPAInfo must be customized for " + classname
              
      ppap_base         = -1 * (outstanding_pokok + nilai_agunan)
      #cash collateral tidak sebagai pengurang PPAP
      #ppap_umum_base    = -1 * (outstanding_pokok + self.collateral_cash_value) if self.overall_col_level==1 else 0
      ppap_umum_base    = -1 * outstanding_pokok if self.overall_col_level==1 else 0
      
      ppap_umum_minimum = prm.TransactionParameter.getPPAMinPercentage(self) * ppap_umum_base
      persentase_col    = prm.TransactionParameter.getPPAPercentage(self, self.overall_col_level)   
      ppap_minimum      = persentase_col * ppap_base
      
      delta_ppap_umum   = 0 ; delta_ppap_khusus = 0 ; delta_ppap_manual = 0
      
      COND_PPAP_UMUM_BLM_CUKUP   = (ppap_umum_minimum > self.reserved_common_balance) # Berarti pencadangan PPAP umum belum memenuhi
      # cek kecukupan PPA minimum
      if COND_PPAP_UMUM_BLM_CUKUP : # Berarti pencadangan PPAP umum belum memenuhi
        delta_ppap_umum = ppap_umum_minimum - self.reserved_common_balance	
      else : # Berarti terjadi kelebihan PPAP umum
        delta_ppap_umum = self.reserved_common_balance  - ppap_umum_minimum
                  
      # cek kecukupan PPA khusus
      current_ppap      = self.reserved_loss_balance + ppap_umum_minimum
      #raise Exception, current_ppap
      COND_PPAP_KHUSUS_BLM_CUKUP = (current_ppap < ppap_minimum) # berarti terjadi kekurangan PPAP khusus        
      if COND_PPAP_KHUSUS_BLM_CUKUP : # berarti terjadi kekurangan PPAP khusus
        delta_ppap_khusus = ppap_minimum - current_ppap
      else : # berarti terjadi kelebihan PPAP khusus
        if ppap_minimum >= ppap_umum_minimum :
          delta_ppap_khusus = current_ppap - ppap_minimum
        else :
          delta_ppap_khusus = self.reserved_loss_balance
          
      # PPA Adjustment
      new_ppap_manual = params.new_ppap_manual if params != None else 0.0
      current_ppap_manual = self.reserved_manual_balance
      COND_NEW_PPAP_MANUAL_BIGGER = (new_ppap_manual > current_ppap_manual) 
      if COND_NEW_PPAP_MANUAL_BIGGER :
        delta_ppap_manual = new_ppap_manual - current_ppap_manual
      else :
        delta_ppap_manual = current_ppap_manual - new_ppap_manual

      info = {}    
      info['saldo'                     ] = saldo                     
      info['nilai_agunan'              ] = nilai_agunan              
      info['outstanding_pokok'         ] = outstanding_pokok         
      info['ppap_base'                 ] = ppap_base                 
      info['ppap_umum_base'            ] = ppap_umum_base            
      info['ppap_umum_minimum'         ] = ppap_umum_minimum         
      info['persentase_col'            ] = persentase_col            
      info['ppap_minimum'              ] = ppap_minimum              
      info['current_ppap'              ] = current_ppap              
      info['delta_ppap_umum'           ] = delta_ppap_umum           
      info['delta_ppap_khusus'         ] = delta_ppap_khusus
      info['delta_ppap_manual'         ] = delta_ppap_manual         
      info['COND_PPAP_UMUM_BLM_CUKUP'  ] = COND_PPAP_UMUM_BLM_CUKUP   
      info['COND_PPAP_KHUSUS_BLM_CUKUP'] = COND_PPAP_KHUSUS_BLM_CUKUP
      info['COND_NEW_PPAP_MANUAL_BIGGER'] = COND_NEW_PPAP_MANUAL_BIGGER       
            
      return info
    #--
    
    def calculatePPA_obs(self, parameters):
      RuleTransaction = modman.getModule(self.Config, "RuleTransaction")
      helper = self.Helper ; config = self.Config
      
      rule = RuleTransaction.RuleFacade()                                                       
      rule.setup(self, ['pembentukan_ppa'], parameters)
      rule.execTransactionJournal()      
    #--
    
    def calculatePPA(self, parameters): # tanpa rule transaction
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
            
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      float_oToday = periodHelper.GetAccountingDate()
      mm = int(config.FormatDateTime('mm', float_oToday))
      yr = int(config.FormatDateTime('yyyy', float_oToday))
      y=yr-1 if mm==1 else yr 
      m=12 if mm==1 else mm-1         
      d = a = calendar.monthrange(y,m)[1]
      
      tgl_trx = config.ModDateTime.EncodeDate(y,m,d)
      # CREATE TRANSACTION
      kode_entri = parameters.kode_entri 
      #keterangan = 'Hitung PPAP %s' % self.nomor_rekening

      oTx = FinAccount.initTransaksi(self, kode_entri, tgl_trx, parameters.keterangan)

      kode_jenis = self.kode_jenis
      fau = FinAccountUtils(config)
      entries = []

      info = self.getCalcPPAInfo(parameters)
      delta_ppap_umum            = info['delta_ppap_umum'           ]
      delta_ppap_khusus          = info['delta_ppap_khusus'         ]
      delta_ppap_manual          = info['delta_ppap_manual'         ]
      COND_PPAP_UMUM_BLM_CUKUP   = info['COND_PPAP_UMUM_BLM_CUKUP'  ]
      COND_PPAP_KHUSUS_BLM_CUKUP = info['COND_PPAP_KHUSUS_BLM_CUKUP']
      COND_NEW_PPAP_MANUAL_BIGGER= info['COND_NEW_PPAP_MANUAL_BIGGER']
                    
      # cek kecukupan PPA minimum
      if COND_PPAP_UMUM_BLM_CUKUP : #Berarti pencadangan PPAP umum belum memenuhi
        entries += [ 
          { 'sign': 'C', 'amount': delta_ppap_umum, 'kode_tx_class': fau.get_tx(kode_jenis, 'ppap_umum')}
          ,{'sign': 'D', 'amount': delta_ppap_umum, 'kode_tx_class': fau.get_tx(kode_jenis, 'by_ppap'), 'kode_subtx_class': 'A'}
        ]	
      else : # Berarti terjadi kelebihan PPAP umum
        entries += [ 
          { 'sign': 'D', 'amount': delta_ppap_umum, 'kode_tx_class': fau.get_tx(kode_jenis, 'ppap_umum')}
          ,{'sign': 'C', 'amount': delta_ppap_umum, 'kode_tx_class': fau.get_tx(kode_jenis, 'pdp_rev_ppap_umum'), 'kode_subtx_class': 'A'}
        ]
                  
      # cek kecukupan PPA khusus
      if COND_PPAP_KHUSUS_BLM_CUKUP : # berarti terjadi kekurangan PPAP khusus
        entries += [ 
          { 'sign': 'C', 'amount': delta_ppap_khusus, 'kode_tx_class': fau.get_tx(kode_jenis, 'ppap_khusus')}
          #,{'sign': 'D', 'amount': delta_ppap_khusus, 'kode_tx_class': fau.get_tx(kode_jenis, 'beban_ppap'), 'kode_subtx_class': 'B'}
          ,{'sign': 'D', 'amount': delta_ppap_khusus, 'kode_tx_class': fau.get_tx(kode_jenis, 'by_ppap_khusus')}
        ]
      else : # berarti terjadi kelebihan PPAP khusus
        entries += [ 
            { 'sign': 'D', 'amount': delta_ppap_khusus, 'kode_tx_class': fau.get_tx(kode_jenis, 'ppap_khusus')}
            #,{'sign': 'C', 'amount': delta_ppap_khusus, 'kode_tx_class': fau.get_tx(kode_jenis, 'beban_ppap'), 'kode_subtx_class': 'C'}
            ,{'sign': 'C', 'amount': delta_ppap_khusus, 'kode_tx_class': fau.get_tx(kode_jenis, 'pdp_rev_ppap')}  
        ]
          
      # ppap manual 
      if COND_NEW_PPAP_MANUAL_BIGGER :
        by_codes='by_ppap' if self.overall_col_level==1 else 'by_ppap_khusus'
        entries += [
          { 'sign': 'C', 'amount': delta_ppap_manual, 'kode_tx_class': fau.get_tx(kode_jenis, 'ppap_adjustment')}
          ,{'sign': 'D', 'amount': delta_ppap_manual, 'kode_tx_class': fau.get_tx(kode_jenis, by_codes)} 
        ]
      else :
        pdp_codes='pdp_rev_ppap_umum' if self.overall_col_level==1 else 'pdp_rev_ppap'
        entries += [
          { 'sign': 'D', 'amount': delta_ppap_manual, 'kode_tx_class': fau.get_tx(kode_jenis, 'ppap_adjustment')}
          ,{'sign': 'C', 'amount': delta_ppap_manual, 'kode_tx_class': fau.get_tx(kode_jenis, pdp_codes)}
        ]
        
      
      entries = fau.removeZeroAmountFromJournal(entries)
      #raise Exception, entries
      oDetTxGroupCost = self.CreateTransactionGroup(kode_entri, parameters.keterangan, oTx, entries)  
      oDetTxGroupCost.ProcessDetails()
      #--
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTxGroupCost 

    def getReverseAmount(self, parTxn):
      config = self.Config
      fau = FinAccountUtils(config)
      tx_payment = fau.get_tx(self.kode_jenis, 'pembayaran')
      transaction_amount = 0
      for i in range(0, parTxn.RecordCount):
        rec_txn = parTxn.GetRecord(i)
        if rec_txn.kode_tx_class in [tx_payment, '']:
          transaction_amount += rec_txn.nilai_mutasi
      return transaction_amount
    #--
    
    def ReverseTxn(self, parameters):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
            
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_trx = periodHelper.GetAccountingDate()
      
      # CREATE TRANSACTION
      rec = parameters.uipData.GetRecord(0)
      parTxn = parameters.uipTxn
      kode_entri = 'FRVS01' 
      keterangan = 'Rev. Txn %s - %s' % (rec.jenis_transaksi,rec.jurnal_no)

      oTx = FinAccount.initTransaksi(self, kode_entri, tgl_trx, keterangan)

      kode_jenis = self.kode_jenis
      fau = FinAccountUtils(config)
      tx_payment = fau.get_tx(kode_jenis, 'pembayaran')
      tx_arear = fau.get_tx(kode_jenis, 'tunggakan')
      entries = []
      tx_class = ''
      is_arrear = 'F'
      for i in range(0, parTxn.RecordCount):
        rec_txn = parTxn.GetRecord(i)
        _sign = 'D' if rec_txn.jenis_mutasi=='C' else 'C'
        tx_class = rec_txn.kode_tx_class

        if tx_class == tx_arear:
          is_arrear = 'T'
           
        if tx_class not in [tx_payment, '']:
          entries.append({'sign': _sign, 'amount': rec_txn.nilai_mutasi, 'kode_tx_class': tx_class, 'kode_account':rec_txn.kode_account})
          #rev detil biaya di peragaan
          if rec.kode_transaksi == 'FAC01':
             self.ReverseBiaya(rec_txn)
        elif tx_class =='':
          dtrx = self.Helper.GetObject('core.RekeningLiabilitas', rec_txn.nomor_rekening)
          if dtrx.IsNull:
            entries.append({'sign': _sign, 'amount': rec_txn.nilai_mutasi, 'kode_tx_class': None, 'kode_account':rec_txn.kode_account})
          else:
            oDT = dtrx.BuatTransaksiManual('core.DetilTransaksi', _sign, rec_txn.nilai_mutasi);
            oDT.LTransaksi = oTx
            oDT.keterangan = 'Rev. Txn %s ' % rec_txn.keterangan
            oDT.tanggal_transaksi = oTx.tanggal_transaksi
            oDT.kode_jurnal = '11'
            oDT.Proses()
          
        else:
          #create detil transaksi rekening liability
          LPaymentSrc = self.LPaymentSrc
          if LPaymentSrc.IsNull:
            raise Exception, "Rekening sumber pembayaran tidak ada.."
          oDT = LPaymentSrc.BuatTransaksiManual('core.DetilTransaksi', 'C', rec_txn.nilai_mutasi);
          oDT.LTransaksi = oTx
          oDT.keterangan = oTx.keterangan
          oDT.tanggal_transaksi = oTx.tanggal_transaksi
          oDT.kode_jurnal = '11'
          oDT.Proses()
                       
      entries = fau.removeZeroAmountFromJournal(entries)
      #raise Exception, entries
      oDetTxGroupCost = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupCost.ProcessDetails()
      #--
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      oSced = self.LActiveSchedule
      if not oSced.IsNull:
        #raise Exception,'QQ'
        oSced.id_otorentri = self.id_otorentri
        oSced.ReverseSchedule(rec, is_arrear)
      
      oTx.is_reverse_allowed='F'
      #reverse
      revTx = self.Helper.GetObject('core.Transaksi', rec.id_transaksi)
      if revTx.IsNull:
        revTx = self.Helper.GetObject('core.HistTransaksi', rec.id_transaksi)
        
      revTx.is_reversed = 'T'
      revTx.is_reverse_allowed='F'
      #raise Exception,'OK__'
      return oDetTxGroupCost 
    #--
      
    def ReverseBiaya(self, recD):
      config = self.config
      sql = '''     
        SELECT FC.FINACCCOST_ID,SRC.KETERANGAN, FC.COST_AMOUNT, COST_AMORTIZED, SRC.KETERANGAN_2
        FROM %(finacccost)s fc
          INNER JOIN (
          	SELECT FINACCCOST_ID, COST_ELEMENT_NAME||' '||NOMOR_REKENING keterangan
          	, COST_ELEMENT_NAME||' %(facility_no)s' keterangan_2
          	FROM %(finacccost)s a, %(fincostelement)s b
          	WHERE a.COST_ELEMENT_ID=b.COST_ELEMENT_ID
          ) src ON FC.FINACCCOST_ID=SRC.FINACCCOST_ID
        WHERE (SRC.KETERANGAN='%(keterangan)s' OR SRC.KETERANGAN_2='%(keterangan)s')
        	AND FC.COST_AMOUNT=%(cost_amount)s
        	AND nvl(COST_AMORTIZED,0)=0 		
      ''' % {
        'finacccost': config.MapDBTableName('finacccost'),
        'fincostelement': config.MapDBTableName('fincostelement'),
        'keterangan': recD.keterangan.split(' - ')[0],
        'cost_amount': recD.nilai_mutasi,
        'facility_no': self.facility_no
      }
      #raise Exception, sql
      rs = self.Config.CreateSQL(sql).RawResult
      FAC = self.Helper.GetObject('FinAccCost', rs.FINACCCOST_ID)
      if not FAC.IsNull:
        FAC.Delete()
    #--
            
    def reposition(self, parameters): 
      RuleTransaction = modman.getModule(self.Config, "RuleTransaction")
      helper = self.Helper ; config = self.Config      
      ruleakad = RuleTransaction.RuleFacade.ruleAkadFactory(config, self.ClassName)
      ruleakad.setup(self, parameters)
      dictAddOnParam = {
        'arrReposisiRawValues' : ruleakad.getRawValuesReposisi()  
      }
      ruleRvr = RuleTransaction.RuleFacade()                  
      ruleRvr.setup(self, ['reposisi_reverse'], parameters, dictAddOnParam)                                                                    
      ruleRvr.execTransactionJournal(False) # do not create journal before complete
      oTx = ruleRvr.getTransactionObj()
      
      # ubah kode cabang
      #origin = recparam.GetFieldByName('LCabangAsal.Kode_Cabang')
      dest = parameters.GetFieldByName('LCabangTujuan.Kode_Cabang')
      self.Kode_Cabang = dest
      self.ApplyChanges()
             
      # apply di cabang 
      ruleDest = RuleTransaction.RuleFacade()
      ruleDest.setup_nonTrx(self, ['reposisi_applytodestination'], parameters, dictAddOnParam)      
      ruleDest.setTransactionObj(oTx) # using exising transaction object, 1 transaction, dua transactionGroup
      ruleDest.execTransactionJournal(True) # now create journal since it's complete
      #oTx2.Delete()#edit Gemma 26Sep13
      
      #raise Exception, str(dictAddOnParam)                        
    #--
    
    def ProdSwitch(self, parameters):
      config = self.config
      app = config.AppObject
      fau = FinAccountUtils(config)
      app.ConCreate('out')
      prod_lama = parameters.GetFieldByName('LProduct.product_code')
      prod_baru = parameters.GetFieldByName('LFinProduct_New.product_code')

      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_trx = periodHelper.GetAccountingDate()
      
      # CREATE TRANSACTION
      kode_entri = parameters.kode_entri 
      keterangan = 'Ganti Produk Loan %s [dr %s ke %s ]' % (self.nomor_rekening,prod_lama,prod_baru)

      oTx = self.initTransaksi(kode_entri, tgl_trx, keterangan)

      sql = '''     
        SELECT 
          b.KODE_TX_CLASS,KODE_ACCOUNT,b.DESKRIPSI,ACCUM_FIELD_NAME,CLASSGROUP,KODE_PRODUK
        FROM %(glinterface)s a
          INNER JOIN %(detiltransaksiclass)s b ON a.KODE_INTERFACE=b.KODE_TX_CLASS
        WHERE KODE_PRODUK in ('%(prod_lama)s','%(prod_baru)s')
        AND ACCUM_FIELD_NAME is not null and b.CLASSGROUP<>'pendapatan'
        ORDER BY KODE_PRODUK, a.KODE_INTERFACE
      ''' % {
        'glinterface': config.MapDBTableName('glinterface'),
        'detiltransaksiclass': config.MapDBTableName('core.detiltransaksiclass'),
        'prod_lama': prod_lama,
        'prod_baru': prod_baru
      }
      #app.ConWriteln(sql)
      q = config.CreateSQL(sql).RawResult
      tx_awal = {}
      tx_baru = {}
      _desc   = {}
      while not q.Eof:
        n_mutasi=0
        if q.ACCUM_FIELD_NAME not in ['',None]:
          _mutasi = eval('self.'+q.ACCUM_FIELD_NAME)
          n_mutasi = fau.to_decimal(_mutasi)

        tx_code = q.KODE_TX_CLASS
        classgroup = q.CLASSGROUP
        _desc [classgroup] = q.DESKRIPSI
        if q.KODE_PRODUK == prod_lama:
          _sign = 'D' if n_mutasi > 0 else 'C'
          tx_awal[classgroup] = {'sign': _sign, 'amount': abs(n_mutasi), 'kode_tx_class': tx_code, 'kode_account':q.KODE_ACCOUNT }
        elif q.KODE_PRODUK == prod_baru:
          _sign = 'C' if n_mutasi > 0 else 'D'
          tx_baru[classgroup] = {'sign': _sign, 'amount': abs(n_mutasi), 'kode_tx_class': tx_code, 'kode_account':q.KODE_ACCOUNT }
        
        q.Next()
              
      entries=[]
      app.ConWriteln('entry txn..!!!')  
      for ls in tx_awal:
      	acc_awal = tx_awal[ls]['kode_account']
      	acc_baru = tx_baru[ls]['kode_account']
      	if acc_awal<>acc_baru:
          entries +=[ tx_awal[ls],tx_baru[ls] ]
          #app.ConWriteln('----'+_desc[ls]+'----')
          #app.ConWriteln(str(tx_awal[ls]))
          #app.ConWriteln(str(tx_baru[ls]))
          
      #raise Exception, entries
      app.ConWriteln(str(entries))
      entries = fau.removeZeroAmountFromJournal(entries)
      app.ConWriteln(str(entries))
      oDetTxGroupCost = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupCost.ProcessDetails()
      #--
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      self.product_code = prod_baru 
      #app.ConRead('')
      #raise Exception, 'UNDER CONSTRACTION!!'
      return oDetTxGroupCost 
      
    def getPenaltyValue(self, dpd, angsuran):
      penalty_fix = self.penalty_fix
      _dt = (penalty_fix*dpd*angsuran)/100000
      _tawid=0;_tazir=0;_bp=0
      if dpd >= 1:    
        oProduct = self.LFinProduct
        ls_penalty = oProduct.Ls_FinPenalty
        sisa_dpd = dpd        
        while not ls_penalty.EndOfList:
          oPenalty = ls_penalty.CurrentElement
          #if oPenalty.dpd_bottom <= dpd and oPenalty.dpd_up >= dpd:
          #  biaya_harian = oPenalty.daily_penalty_cost
          #  break
          if sisa_dpd > 0:
            _selisih =(oPenalty.dpd_up-oPenalty.dpd_bottom)+1
            _jml =sisa_dpd if sisa_dpd<_selisih else _selisih
            _bp +=_jml*oPenalty.daily_penalty_cost
            sisa_dpd -=_jml
          else:
            break 
           
          ls_penalty.Next()
                  
        if _dt>_bp:
          _tawid = _bp
          _tazir = _dt-_bp
        else:
          _tawid = _dt
        
      return {'_tazir':_tazir,'_tawid':_tawid}
    
    def writeOff(self, parameters):
      RuleTransaction = modman.getModule(self.Config, "RuleTransaction")
      helper = self.Helper ; config = self.Config      
      rule = RuleTransaction.RuleFacade()
      rule.setup(self, ['writeoff'], parameters)                                                                   
      rule.execTransactionJournal()
      
      self.write_off_status  = 'T'
    #--
    
    def writeOffFinal(self, parameters):
      RuleTransaction = modman.getModule(self.Config, "RuleTransaction")
      helper = self.Helper ; config = self.Config      
      rule = RuleTransaction.RuleFacade()
      rule.setup(self, ['writeoff_final'], parameters)                                                                   
      rule.execTransactionJournal()
    #--
    
    def getWriteOffPaymentInfo(self):
      byr_ppap = 0.0 ; byr_ppap_umum = 0.0 ; byr_ppap_khusus = 0.0
      nominal_bayar  = min([self.payment_balance, -1 * self.write_off_balance]) # pilih nominal payment_balance maksimum sebesar tunggakan write off
      reserved_loss_balance = self.reserved_loss_balance
      reserved_common_balance = self.reserved_common_balance
      
      total_ppap = self.reserved_loss_balance + reserved_common_balance
      total_non_ppap = -1 * (self.write_off_balance + total_ppap)
      if nominal_bayar > total_non_ppap : 
        byr_ppap = nominal_bayar - total_non_ppap
      else : 
        byr_ppap = 0.0
      if byr_ppap > 0:
        if byr_ppap > reserved_loss_balance :
          byr_ppap_umum = byr_ppap - reserved_loss_balance
          byr_ppap_khusus = reserved_loss_balance
        else :
          byr_ppap_umum = 0.0
          byr_ppap_khusus = byr_ppap
      info = {
        'nominal_bayar'    : nominal_bayar   
        ,'total_ppap'      : total_ppap      
        ,'total_non_ppap'  : total_non_ppap  
        ,'byr_ppap'        : byr_ppap        
        ,'byr_ppap_umum'   : byr_ppap_umum   
        ,'byr_ppap_khusus' : byr_ppap_khusus 
      }
      return info
    #--
    
    def writeOffPayment(self, parameters):
      RuleTransaction = modman.getModule(self.Config, "RuleTransaction")
      helper = self.Helper ; config = self.Config
      
      #self.deductSourcePaymentAccount()
      self.deductSourcePaymentAccount(parameters.nominal_bayar)
      
      self.checkPaymentBalanceAdqt(parameters)
            
      rule = RuleTransaction.RuleFacade()
      rule.setup(self, ['writeoff_payment'], parameters)                                                                   
      rule.execTransactionJournal()
    #--
    
    def deductSourcePaymentAccount(self, payment = None, nomor_rekening = None, amount_deduct = None, kode_entri = None, kode_tx_class = None, fmt_keterangan = None, cust_keterangan = None):
      config = self.Config; mlu = config.ModLibUtils
      helper = self.Helper
      
      #raise Exception, "stopped"
      if nomor_rekening == None:
        oPaymentSrc = self.LPaymentSrc
      else:
        oPaymentSrc = helper.GetObject("core.RekeningTransaksi", nomor_rekening)
        
      if oPaymentSrc.IsNull:
        raise Exception, "Account sumber pembayaran tidak ditemukan"
        
      if amount_deduct == None:
        if payment != None : # ambil dari parameter
          payment_amount = payment
        else : # jika tidak ada ambil dari cicilan berikutnya  
          payment_amount = self.getNextPaymentAmount()         
        #amount_deduct = (payment_amount - self.payment_balance - self.penalty_tmp_balance) if payment_amount > self.payment_balance + self.penalty_tmp_balance else 0
        amount_deduct = (payment_amount - self.payment_balance) if payment_amount > self.payment_balance else 0
        #raise Exception,"payment_amount:" + str(payment_amount)  + " paymentbal:" + str(self.payment_balance) + " penalty bal:" + str(self.penalty_tmp_balance) + " amount deduct :" + str(amount_deduct)
                
      if amount_deduct <= 0.01:
        return None
        
      # FIX ZH : mendapatkan kode entri, utk kelas2 yg inheritance, misalnya : mudharabah seharusnya MDB bukan MSY
      ### SOMEHOW THIS DOEST WORK 
      ###finacc = self.CastToLowestDescendant()
      ###kode_jenis = finacc.kode_jenis ## kode_jenis masih memberikan MSY utk MDB ??
      
      ## FIX2 ZH : ubah ke SQL :
      kode_jenis = None
      q = self.config.CreateSQL(
        '''select kode_jenis from %(rekeningtransaksi)s where nomor_rekening = %(norek)s ''' % {
           'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'), 'norek': mlu.QuotedStr(self.nomor_rekening) 
        } 
      ).RawResult
      if q.Eof:
        raise Exception, "nomor rekening %s tidak terdaftar di core.rekeningtransaksi" % (self.nomor_rekening)
      kode_jenis = q.kode_jenis       
      #raise Exception, kode_jenis  ## untuk mudharabah seharusnya MDB bukan MSY         
        
      if kode_entri == None: # try to search kode_entri  
        q = self.config.CreateSQL(
          '''
             SELECT kode_entri FROM %(detiltransgroupclass)s
             WHERE classgroup=%(classgroup)s AND subsystem_code = 'FN' AND classparameter='debet_src'
          ''' % {
            'detiltransgroupclass': config.MapDBTableName('core.detiltransgroupclass'),
            'classgroup': mlu.QuotedStr(kode_jenis)
          } 
        ).RawResult
        if q.Eof:
          raise Exception, "Kode entri 'debet_src' tidak terdaftar untuk jenis rekening %s" % kode_jenis
        kode_entri = q.kode_entri
      #--
      
      if kode_tx_class == None: # try to search kode tx class 'pembayaran'
        q = self.config.CreateSQL(
          '''
             SELECT kode_tx_class FROM %(detiltransaksiclass)s
             WHERE kode_jenis=%(kode_jenis)s AND classgroup='pembayaran'
          ''' % {
            'detiltransaksiclass': config.MapDBTableName('core.detiltransaksiclass'),
            'kode_jenis': mlu.QuotedStr(kode_jenis)
          } 
        ).RawResult
        if q.Eof:
          raise Exception, "Kode transaksi 'pembayaran' tidak terdaftar untuk jenis rekening %s" % kode_jenis
        kode_tx_class = q.kode_tx_class
      #raise Exception, kode_tx_class         
      #-- 
        
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      #raise Exception, kode_entri # kode entri
      
      if cust_keterangan == None:
        if fmt_keterangan == None:
          keterangan = 'PEMBAYARAN PEMBIAYAAN %s' % self.nomor_rekening
        else:
          keterangan = fmt_keterangan % self.nomor_rekening
      else:
        keterangan = cust_keterangan
         
      if self.id_otorentri==0:
        oTx = self.initTransaksiEOD(kode_entri, periodHelper.GetAccountingDate(), keterangan)
      else:
        oTx = self.initTransaksi(kode_entri, periodHelper.GetAccountingDate(), keterangan)
      entries = [
          {'sign': 'C', 'amount': amount_deduct, 'kode_tx_class': kode_tx_class}
        ]
      
      oSchd = self.LActiveSchedule
      oTx.nomor_referensi = "%s_%s" % (self.nomor_rekening, oSchd.active_seq)
      #raise Exception,"payment_amount:" + str(payment_amount)  + " paymentbal:" + str(self.payment_balance) + " penalty bal:" + str(self.penalty_tmp_balance) + " amount deduct :" + str(amount_deduct)
      
      oDetTxGroup = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)
      oDetTxGroup.ProcessDetails()
      oDT = oPaymentSrc.BuatTransaksiManual('core.DetilTransaksi', 'D', amount_deduct);
      oDT.LTransaksi = oTx
      oDT.keterangan = oTx.keterangan
      oDT.tanggal_transaksi = oTx.tanggal_transaksi
      oDT.kode_jurnal = '11'
      oDT.Proses()
      
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      return amount_deduct
      #oTx.CreateJournal()
      #--
      
    #upddate by BG set user input dan user otorisasi #20150216 
    def initTransaksiEOD(self, kode_entri, tgl_transaksi, keterangan):
      helper = self.Helper ; config = self.Config      
      # CREATE TRANSACTION 
      oTx = helper.CreatePObject("core.Transaksi", kode_entri)
      oTx.SetStandardInfo(tgl_transaksi, keterangan)
      oTx.user_input     = 'sysFIN'
      oTx.SetStatusOtorisasi(1)     
      oTx.user_otorisasi = 'sysFIN'
      
      return oTx 

    #upddate by BG set user input dan user otorisasi #20150216 
    def initTransaksi(self, kode_entri, tgl_transaksi, keterangan):
      helper = self.Helper ; config = self.Config      
      # CREATE TRANSACTION 
      oTx = helper.CreatePObject("core.Transaksi", kode_entri)
      oTx.SetStandardInfo(tgl_transaksi, keterangan)
      if self.id_otorentri>0: 
        Otr = helper.GetObject('enterprise.OtorEntri', self.id_otorentri)
        oTx.terminal_input = Otr.terminal_input    
        oTx.tanggal_input  = Otr.tgl_input    
        oTx.jam_input      = Otr.tgl_input    
        oTx.user_input     = Otr.user_input
      
      oTx.SetStatusOtorisasi(1)
      return oTx 

    
    def getPrincipalOutstanding(self): # finacc
      #raise Exception, self.saldo
      return -(self.saldo + self.arrear_balance) 
    #--
    
    def getMarginOutstanding(self): # virtual method
      return 0.0
    #--
      
    def IsConfidential(self): # virtual method
      helper = self.Helper ; config = self.Config
      mlu = config.ModLibUtils
      sSQL="""
    		select * from %(finproduct)s pr
    		LEFT JOIN %(confidentialaccess)s ca ON pr.id_cfgroup=ca.id_cfgroup 
    			AND ca.id_peran in (SELECT id_peran FROM %(listperanuser)s WHERE id_user=%(id_user)s)
    		where 
          pr.product_code = %(product_code)s
          and (nvl(pr.id_cfgroup,0)=0 or nvl(ca.id_cfgroup,0)>0)
        """ % {
          'finproduct': config.MapDBTableName('finproduct'),
          'confidentialaccess': config.MapDBTableName('core.confidentialaccess'), 
          'listperanuser': config.MapDBTableName('enterprise.listperanuser'), 
          'id_user': mlu.QuotedStr(config.SecurityContext.InitUser),
          'product_code': mlu.QuotedStr(self.product_code)
        }
      #raise Exception, sSQL      
      q = config.CreateSQL(sSQL).RawResult
      
      isConfi = False
      if q.Eof:
        isConfi = True
    
      return isConfi
    #--
      
    def getFlatRate(self): # virtual method
      return 0.0
    #--
      
    def getTenorMonth(self):
      config = self.Config
      tenor_pinjaman = self.period_count
      sql='''
        SELECT round(months_between(max(sched_date), min(sched_date))+1,0) as tenorMonth
        FROM %s WHERE ID_SCHEDULE=%s AND SEQ_NUMBER>0
      ''' % (config.MapDBTableName("finpaymentscheddetail"), self.id_schedule) 
      rs = config.CreateSQL(sql).RawResult
      if not rs.Eof:
        tenor_pinjaman = rs.tenorMonth or self.period_count_awal
      
      return tenor_pinjaman
    #--
      
    def getWriteOffTotalPrincipalOustanding(self):
      raise Exception, "getWriteOffTotalPrincipalOustanding must be implemented in %s " % self.ClassName
    
    # sudah lunas atau belum
    def isRepaymentCompleted(self):
      if self.isDroppingDone() and self.id_schedule not in [None, 0]: ## belum dropping / punya schedule
        sched = self.LActiveSchedule
        res = (self.Status_Rekening == 3 or sched.completion_status == 'T') # TODO pastikan status_rekening
      else :
        res = False
     
      return res

    # sudah lunas tetapi flag masih belum berubah
    def isSchedCompleted(self):
      if self.isDroppingDone() and self.id_schedule not in [None, 0]: ## belum dropping / punya schedule
        sched = self.LActiveSchedule
        sql='''
          SELECT seq_number FROM %s WHERE ID_SCHEDULE=%s AND nvl(realized,'F')='F'
        ''' % (self.Config.MapDBTableName("finpaymentscheddetail"), self.id_schedule) 
        rs = self.Config.CreateSQL(sql).RawResult
        res = not rs.Eof
        os_loan = self.saldo + self.arrear_balance
        if abs(os_loan)<=0.1:
          res = False
      else :
        res = False
     
      return res
      
    # sudah lunas atau belum
    def isAydaDueDate(self):
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl = periodHelper.GetAccountingDate()
      y,m,d = self.config.ModLibUtils.DecodeDate(tgl)
      tgl_today = '%s%s%s' % (str(d).zfill(2),str(m).zfill(2),y)
      config = self.config
      sql = ''' SELECT NOMOR_REKENING, round(MONTHS_BETWEEN(TO_DATE('%s', 'DDMMYYYY'), AYDA_DATE),4) as month_b FROM %s WHERE NOMOR_REKENING='%s'
       ''' % (tgl_today, config.MapDBTableName("finaccount"), self.nomor_rekening)
      rs = self.Config.CreateSQL(sql).RawResult
            
      return rs.month_b < 12
      
    def isAYDAHadPPANP(self):
      #raise Exception, self.ayda_balance+self.reserved_loss_balance  >= 0.0
      return self.ayda_balance+self.reserved_loss_balance >= 0.0
      
    # is payment balance adequate vs amount
    def isPaymentBalanceAdqt(self, paymentAmount):
      delta = (self.payment_balance - paymentAmount)
      if (delta >= 0) :
        return True
      else : 
        return ( abs(delta) < EPSILON )   
    #--
      
    # get current nisbah
    def getCurrentNisbah(self):
      config = self.Config
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl = periodHelper.GetAccountingDate()
      strDate = config.FormatDateTime('yyyymm', tgl)
      nisbah = 0
      if self.id_schedule not in [None, 0]:
        sql='''
          SELECT b.SCHED_DATE, decode(nvl(d.PROFIT_RATIO,0), 0, b.PROFIT_RATIO, d.PROFIT_RATIO) as current_rate 
          FROM FINPAYMENTSCHEDULE a
          LEFT JOIN finpaymentscheddetail b ON a.ID_SCHEDULE = b.ID_SCHEDULE AND a.ACTIVE_SEQ = b.SEQ_NUMBER
          LEFT JOIN (
          	SELECT ID_SCHEDULE,max(SEQ_NUMBER) as max_seq 
            FROM FINPAYMENTSCHEDDETAIL WHERE to_char(SCHED_DATE, 'YYYYMM')='%(strDate)s' GROUP BY ID_SCHEDULE 
          ) c ON a.ID_SCHEDULE=c.ID_SCHEDULE
          LEFT JOIN finpaymentscheddetail d ON a.ID_SCHEDULE = d.ID_SCHEDULE AND c.max_seq = d.SEQ_NUMBER
          WHERE a.ID_SCHEDULE=%(id_schedule)s
        ''' % {'id_schedule' :self.id_schedule, 'strDate': strDate} 
        rs = config.CreateSQL(sql).RawResult
        nisbah = rs.current_rate
     
      return nisbah
      
    # memeriksa apakah payment balance mencukupi atau tidak untuk melakukan transaksi pembayaran
    def checkPaymentBalanceAdqt(self, params):
      payment = 0.0
      try :
        payment = params.payment
      except :
        pass
      try :  
        payment = params.total_bayar
      except :
        pass
      #raise Exception, "payment bal =%.2f   ;  payment = %.2f" % (self.payment_balance, payment)
      if not self.isPaymentBalanceAdqt(payment) :
        raise Exception, "finacc : %s : Titipan / Payment Balance = %.2f tidak mencukupi utk pembayaran %.2f, delta : %f" % (self.Nomor_Rekening, self.payment_balance, payment, self.payment_balance - payment)   
    #--
        
    # sudah dropping atau belum
    def isDroppingDone(self):
      return ( self.drop_counter > 0 )
      #return self.id_schedule != None
      
    # siap dropping atau belum
    def isCollateralInputDone(self):
      config = self.config
      sql = ''' select
      case fp.use_collateral 
        when 'F' then 1
        else (select  count(*) as coll_count from %s f  where f.norek_finaccount = '%s')
      end as coll_count 
      from %s fa, %s fp 
      where fa.nomor_rekening = '%s' and
      fa.product_code = fp.product_code
       ''' % (config.MapDBTableName("fincollateralaccount"),
              self.nomor_rekening,
              config.MapDBTableName("finaccount"),
              config.MapDBTableName("finproduct"),
              self.nomor_rekening)
      rs = self.Config.CreateSQL(sql).RawResult
      if not rs.Eof:
        coll_count = rs.coll_count
      if coll_count == 0 :
        return 0
      else : return 1
        
    def is_need_findata_complete(self):
      sql = ''' 
          SELECT NEED_FINDATA_COMPLETE
          FROM FINACCOUNT a
          LEFT JOIN FINPRODUCT c on a.product_code=c.product_code
          WHERE a.NOMOR_REKENING='%s'
      ''' % (self.nomor_rekening)
      rs = self.Config.CreateSQL(sql).RawResult
      
      return rs.NEED_FINDATA_COMPLETE

    def isCustDataDone(self):
      sql = ''' select count(*) as cust_data_count from fincustadditionaldata where nomor_nasabah = '%s' ''' % (self.nomor_nasabah)
      rs = self.Config.CreateSQL(sql).RawResult
      if not rs.Eof:
        cust_data_count = rs.cust_data_count
      if cust_data_count == 0 :
        return 0
      else : return 1
        
    def isAccDataDone(self):
      sql = ''' select count(*) as acc_data_count from finaccadditionaldata where nomor_rekening = '%s' ''' % (self.nomor_rekening)
      rs = self.Config.CreateSQL(sql).RawResult
      if not rs.Eof:
        acc_data_count = rs.acc_data_count
      if acc_data_count == 0 :
        return 0
      else : return 1
      
    def isScheduleDetailOk(self):
      # cek jumlah angsuran
      res = 1 # OK
      idsch = self.LActiveSchedule.id_schedule
      sql = ''' select count(*) as sched_count from finpaymentscheddetail where id_schedule = %d and seq_number > 0 ''' % (idsch)
      rs = self.Config.CreateSQL(sql).RawResult
      count_sch = 0
      if not rs.Eof:
        count_sch = rs.acc_data_count
      if count_sch != self.period_count : 
        res = -1
      return res      
               
    # jika account sudah dropping tapi belum memiliki dropping balance, utk menangani account2 yg dibuat sblm fitur pencairan
    def isDroppingValid(self):
      return ( self.dropping_balance > 0 )
      
    # sudah dicairkan atau belum
    def isDisbursalDone(self):
      #return ( self.LDisbursalDest <> None )
      return ( self.norek_disbursaldest != '' )
      
    # AYDA atau tidak ?
    def isAYDA(self):
      return self.AYDA_flag == 'T'

    def isKolMacet(self):      
      return self.overall_col_level == 5

    # write off atau tidak ?
    def isWriteOff(self):
      return self.write_off_status == 'T'
      
    # write off atau tidak ?
    def isHapusTagih(self):
      return self.write_off_deleted == 'T'
      
    def isWriteOffFinalized(self):
      return abs(self.write_off_balance) > EPSILON
      
    def hasPaymentBalance(self):
      return self.payment_balance != 0.0
      
    def isPaymentBalanceAdequate(self, amount):
      return self.payment_balance >= amount
      
    def getNextPaymentAmount(self):
      raise Exception, "getNextPaymentAmount() is not implemented"
        
    def getTazirAmt(self):
      idsch = self.LActiveSchedule.id_schedule
      sql = ''' select sum(penalty_tazir) sum_tazir from finpaymentscheddetail where id_schedule = %d and seq_number > 0 and realized='T' ''' % (idsch)
      rs = self.Config.CreateSQL(sql).RawResult
      
      return rs.sum_tazir or 0
    
    def getTawidhAmt(self):
      idsch = self.LActiveSchedule.id_schedule
      sql = ''' select sum(penalty_tawidh) sum_tawidh from finpaymentscheddetail where id_schedule = %d and seq_number > 0 and realized='T' ''' % (idsch)
      rs = self.Config.CreateSQL(sql).RawResult
      
      return rs.sum_tawidh or 0
    
    def AutomaticUnLink(self):
      lst = self.Ls_FinCollateralAccount
      lst.First()
      while not lst.EndOfList:
        oFCA = lst.CurrentElement
        oColl = self.Helper.GetObject('FinCollateralAsset', oFCA.norek_fincollateralasset )
        oDT = self.Helper.GetObjectByInstance('FinCollateralAccount', oFCA).CastToLowestDescendant()
        oDT.Delete()
        #hitung paripasu
        oColl.calcParipasu()
        oColl.colasset_status_update = 'B' if oColl.isLinked() else 'R'
        lst.Next()
      
      #update paripasu AfterUnlink
      self.AfterUnLink()

    def AfterUnLink(self):
      if self.Ls_FinCollateralAccount.EndOfList:
        self.collateral_value = 0
        self.collateral_cash_value = 0
        self.collateral_gross_value = 0

      self.reserve_eval = 'T'
      
    #mengakui sisa amortisasi biaya nasabah
    def RealizedRemainCost(self):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      app = config.AppObject
      app.ConCreate('out')
      app.ConWriteln('Inisialisasi proses Amortisasi Biaya...')
      app.ConWriteln('saldo 1: %s' % self.saldo)
      fau = FinAccountUtils(config)

      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_trx = periodHelper.GetAccountingDate()

      lst = self.Ls_FinAccCost
      lst.First()
      entries=[]
      _aCost = 0
      while not lst.EndOfList:
        oFAC = lst.CurrentElement
        unAmortize = oFAC.cost_amount-oFAC.cost_amortized    
        if oFAC.LFinCostElement.is_amortized == 'T' and unAmortize > 0:
          app.ConWriteln('amor '+oFAC.LFinCostElement.cost_element_name)
          
          if self.contracttype_code=='W':
            oFF = self.Helper.GetObject('FinFacility', self.facility_no )
            kode_jenis = fau.DICT_KODE_JENIS[oFF.contracttype_code]
            drop_model = oFF.dropping_model
            kode_entri = fau.get_kode_entri('eff_amortize',kode_jenis)

            kode_tx_class = fau.get_tx(kode_jenis,'deferred_balance')
            _pars=['1', kode_jenis, drop_model, kode_tx_class]
            acc_code = fau.get_glfacility(_pars)
            entries.append({'sign': 'D', 'amount': unAmortize, 'kode_tx_class': kode_tx_class, 'kode_account':acc_code})

            _pars=['2', kode_jenis, drop_model, oFAC.cost_element_id]
            acc_code = fau.get_glfacility(_pars)
            entries.append({'sign': 'C', 'amount': unAmortize, 'kode_tx_class': None, 'kode_account':acc_code})              
            #--
          else:
            kode_entri = fau.get_kode_entri('eff_amortize',self.kode_jenis)
            entries += self.getTrxJournal(unAmortize,'eff_amortize', self.kode_jenis)
  
            _gac = fau.get_account_code(self.product_code, oFAC.cost_element_id)
            acc_code = _gac[0]
            entries.append({'sign': 'C', 'amount': unAmortize, 'kode_tx_class': None, 'kode_account':acc_code})

          oFAC.cost_amortized += unAmortize
          oFAC.end_amortize_date = tgl_trx
          _aCost += unAmortize
          
        lst.Next()

      app.ConWriteln(str(entries))
      if len(entries) > 0:  
        # CREATE TRANSACTION
        keterangan='Pengakuan Amortisasi Biaya'
        oTx = FinAccount.initTransaksi(self, kode_entri, tgl_trx, keterangan)
                    
        app.ConWriteln(str(entries))
        
        entries = fau.removeZeroAmountFromJournal(entries)
        oDetTxGroupCost = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
        #app.ConWriteln('saldo 3: %s' % self.saldo)
        oDetTxGroupCost.ProcessDetails()
  
        #oTx.CreateJournal()
        self.Helper.CreatePObject('PendingTransactionJournal', oTx)
        app.ConWriteln('OK')
        #app.ConWriteln('saldo 4: %s' % self.saldo)
        #app.ConRead('')
        self.saldo -= round(_aCost,2)
        #raise Exception, self.saldo
  
        return oDetTxGroupCost      
    #--    

    
    #close FinAccount
    def closeFinAcc (self):
      helper = self.Helper
      config = self.Config
      #hapus link agunan      
      self.AutomaticUnLink()
      self.RealizedRemainCost()
      #oFinAcc.due_date = oFinAcc.last_repayment
      self.closing_date = self.last_repayment

      self.Status_Rekening = 3
      
      dbutil.runSQL(config, ''' update %(finpaymentscheddetail)s set REALIZED='T', penalty_tawidh=0, penalty_tazir=0 where ID_SCHEDULE=%(idsch)s 
      --AND nvl(realized,'F')='F' 
      ''' % { 'finpaymentscheddetail': config.MapDBTableName('finpaymentscheddetail'),
              'idsch': self.id_schedule } 
      )      
      oSchedule = self.LActiveSchedule
      if not oSchedule.IsNull:
        oSchedule.completion_status = 'T'
        
      #if self.contracttype_code=='F':     
        #oFinAcc = finacc.CastToLowestDescendant()
        #if oFinAcc.is_imbt in ['T', None]:
      #  self.reverseAktiva()
      #--
      
    #journal trx 
    def getTrxJournal(self,param,jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = FinAccountUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      lgr = []
      if jns_jurnal =='finish_AYDA': # transaksi penyelesaian AYDA
        ayda_balance = param.AYDA_balance
        nilai_jual_aktiva = param.nilai_jual_aktiva
        laba_ayda = nilai_jual_aktiva + ayda_balance
        s_sign='C'; s_kj='ayda_laba' 
        if laba_ayda < 0:
          s_sign='D'; s_kj='ayda_biaya'
          laba_ayda = -laba_ayda
        
        #journal input ayda
        lgr += [
        #reverse ayda balance
          {'sign': s_sign, 'amount': laba_ayda, 'kode_tx_class':  fau.get_tx(kode_jenis,s_kj ) },
          {'sign': 'C', 'amount': -ayda_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_aktiva' ) },
        #reverse PPANP
          {'sign': 'D', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_cadangan' ) },
          {'sign': 'C', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdp_rev_ppap' ) }
        ]

      elif jns_jurnal =='ppanp_AYDA': #transaksi pencadangan AYDA
        lgr += [
        #create PPANP
          {'sign': 'D', 'amount': -(self.ayda_balance+self.reserved_loss_balance), 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_by_cadangan' ) },
          {'sign': 'C', 'amount': -(self.ayda_balance+self.reserved_loss_balance), 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_cadangan' ) }
        ]

      elif jns_jurnal =='delete_AYDA': #transaksi pencadangan AYDA
        lgr += [
        #delete AYDA
          {'sign': 'D', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_cadangan' ) },
          {'sign': 'C', 'amount': -self.ayda_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_aktiva' ) },
          {'sign': 'C', 'amount': self.ayda_balance+self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_by_cadangan' ) }
        ]

      elif jns_jurnal =='penalty_process': # transaksi denda ta'wid & ta'zir        
        lgr += [
          {'sign': 'C', 'amount': param.penalty_tawidh, 'kode_tx_class':  fau.get_tx(kode_jenis,'denda_tawidh' ) },
          {'sign': 'C', 'amount': param.penalty_tazir, 'kode_tx_class':  fau.get_tx(kode_jenis,'denda_tazir' ) }
        ]
      elif jns_jurnal =='recovery_wo': # transaksi recovery_wo       
        lgr += [
          {'sign': 'C', 'amount': param.recovery_pokok, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off' ) },
          {'sign': 'C', 'amount': param.recovery_margin, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off_margin' ) }
        ]

      elif jns_jurnal =='hapus_tagih': # transaksi hapus_tagih       
        lgr += [
          {'sign': 'C', 'amount': -self.write_off_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off' ) },
          {'sign': 'C', 'amount': -self.write_off_margin_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off_margin' ) }
        ]

      elif jns_jurnal =='eff_amortize': # transaksi denda amortisasi sisa biaya        
        lgr += [
          {'sign': 'D', 'amount': param, 'kode_tx_class':  fau.get_tx(kode_jenis,'deferred_balance' ) }
        ]

      elif jns_jurnal=='reverse_PPAP':
        #checking ppap yang terbentuk umum atau khusus
        pdp_codes='pdp_rev_ppap_umum' if (self.reserved_common_balance>0.01 or self.reserved_loss_balance==0) else 'pdp_rev_ppap'
        lgr += [
        #rerevse PPAP
          {'sign': 'D', 'amount': self.reserved_common_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum' ) }
          ,{'sign': 'C', 'amount': self.reserved_common_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdp_rev_ppap_umum' ) }
          ,{'sign': 'D', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus' ) }
          ,{'sign': 'C', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'pdp_rev_ppap' ) }
          #reverse ppap_adjustment
          ,{'sign': 'D', 'amount': self.reserved_manual_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_adjustment' ) }
          ,{'sign': 'C', 'amount': self.reserved_manual_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,pdp_codes ) }  
        ]
        
      #raise Exception,lgr
      return lgr
      
    # update data periode tunggakan di pembiayaan, jika ada
    def updateArrearPeriode(self, num_payment):
      if self.arrear_period > 0:
        self.arrear_period -= num_payment if num_payment <= self.arrear_period else self.arrear_period

    def OnDelete(self):
      pass
    #----
#--
    
class FinMurabahahAccount(FinAccount):
    # Static variables
    pobject_classname = 'FinMurabahahAccount'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '11'
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      self.kode_jenis = 'MBH'
      self.financing_model = 'T'
      FinAccount.OnCreate(self, param)
      recSrc = param['FinMurabahahAccount']
      # set fields with simple / direct 1-on-1 assignment from record with same field name
      atutil.transferAttributes(helper, 
        [ 'base_price', 
          'pricing_model', 
          'urbun' 
        ],
        self, recSrc
      )          
      dictInitValues = {
        #'automatic_payment': 'T',
        'discount_balance' : 0.0, 
        'initial_base_balance' : 0.0, 
        'initial_profit_balance' : 0.0, 
        'MMD_balance' : 0.0, 
        'profit_accrue_balance': 0.0, 
        'profit_balance': 0.0, 
        'profit_pending_balance': 0.0, 
        'write_off_balance': 0.0
      }
      for initField, initValue in dictInitValues.iteritems():
        self.SetFieldByName(initField, initValue)
      self.total_facility_limit = self.base_price - self.urbun
      self.loan_amount = self.base_price - self.urbun
      config.FlushUpdates()
    #--       

    def getPricingModel(self): # virtual method
      return self.pricing_model

    def getPrincipalOutstanding(self): # murabahah
      return - (self.saldo + self.arrear_balance + self.mmd_balance + self.profit_arrear_balance)
    #--
    def getMarginOutstanding(self): # murabahah
      return self.mmd_balance + self.profit_arrear_balance
    #--

    def getWriteOffTotalPrincipalOustanding(self):
      return self.saldo + self.mmd_balance + self.profit_accrue_balance + self.arrear_balance
    
    def getWriteOffTotalMarginOutstanding(self):
      return self.mmd_balance + self.profit_accrue_balance       
      
    def getLoanAmount(self): # virtual method
      return self.base_price - self.urbun
      
    def ProcessUIData(self, recData):
      pass
      
    def OnDelete(self):
      pass
      
    # murabahah
    def dropping(self, parameters): 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      # tgl_angsur_awal: TDateTime
      # jenis_jadwal_angsur: string (D, T, C)                                                        
      # id_schedule_tmp: integer (optional)
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      
      #do pencairan ke rekening
      pencairan = self.disburse(parameters)
      
      parameters = atutil.GeneralObject(parameters)
      
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      if self.is_external == 'F':
        if int(round(timeutil.stdDate(config, tgl_dropping))) > int(round(timeutil.stdDate(config, parameters.tgl_angsur_awal))):
          raise Exception, "Tanggal angsur awal harus >= tanggal dropping"
      
      ## CREATE PAYMENT SCHEDULE
      tempSchedule = self.LTmpDroppingSchedule
      if parameters.jenis_jadwal_angsur in ("T"):
        if not tempSchedule.IsNull:
          self.LTmpDroppingSchedule = None
          
        #--
        install = libs.installment(self.period_count, self.Config)
        if parameters.hasAttribute('LParameterTiering.tiering_code'):
          tiering_code = parameters.getAttrByName('LParameterTiering.tiering_code') 
        else:
          tiering_code = None 
        install.runSimulation(self.pricing_model, parameters.amount_dropping, self.targeted_eqv_rate, tiering_code)
        # simulasi untuk mukasah
        #installMukasah = libs.installment(self.period_count)
        #installMukasah.runSimulation(self.pricing_model, parameters.amount_dropping, parameters.mukasah_rate) #TODO : rate mukasah bagusnya dari form dropping atau pas create account ?
        mksh_install = None
        mukasah_rate = parameters.mukasah_rate
        if mukasah_rate > 0:
          n_rate = self.targeted_eqv_rate - mukasah_rate      
          mksh_install = libs.installment(self.period_count, self.Config)
          mksh_install.runSimulation(self.pricing_model, parameters.amount_dropping, n_rate)
                           
        total_margin = install.margin
        oPaymentSchedule = helper.CreatePObject("FinPaymentSchedule")
        oPaymentSchedule.mukasah_seq_limit = parameters.mukasah_seq_limit 
        #oDropDetailSched = oPaymentSchedule.initFromSimulation(self, tgl_dropping, parameters.tgl_angsur_awal, install, True) # tanpa mukasah
        oDropDetailSched = oPaymentSchedule.initFromSimulationWithMukasahPct(self, tgl_dropping, parameters.tgl_angsur_awal, install, mukasah_rate, parameters.mukasah_seq_limit, True, mksh_install)
        self.LActiveSchedule = oPaymentSchedule
        oSchedule = oPaymentSchedule
      else:
        if tempSchedule.IsNull:
          raise Exception, "Jadwal angsur manual belum dibuat"

        tempSchedule.genericInitSimulation(self, tgl_dropping, parameters.tgl_angsur_awal, 0)
        tempSchedule.initial_principal_amt = parameters.amount_dropping

        if int(round(timeutil.stdDate(config, parameters.tgl_angsur_awal))) != int(round(timeutil.stdDate(config, tempSchedule.first_install_date))):
          raise Exception, "Tanggal jadwal angsur manual berbeda dengan tanggal angsur awal"
        if self.is_external == 'F':
          if int(round(timeutil.stdDate(config, tgl_dropping))) != int(round(timeutil.stdDate(config, tempSchedule.dropping_date))) :
            raise Exception, "Tanggal jadwal angsur manual berbeda dengan tanggal angsur awal"

        total_margin = tempSchedule.initial_profit_amt
        oDropDetailSched = tempSchedule.createDroppingEntry()
        self.LTmpDroppingSchedule = None
        self.LActiveSchedule = tempSchedule
        tempSchedule.setAsActive()
        oSchedule = tempSchedule
      #--
      
      self.initial_base_balance = -oSchedule.initial_principal_amt
      self.initial_profit_balance = -oSchedule.initial_profit_amt
      self.predicted_margin_amount = oSchedule.initial_profit_amt

      self.due_date = oSchedule.getLastPaymentDate()
      self.max_dpd_mukasah = parameters.max_dpd_mukasah
      
      # CREATE TRANSACTION
      if not parameters.hasAttribute('kode_entri'):
        kode_entri = 'FTD01' 
      else:
        kode_entri = parameters.kode_entri

      if not parameters.hasAttribute('keterangan'):
        keterangan = 'DROPPING %s' % self.nomor_rekening
      else:
        keterangan = parameters.keterangan

      oTx = FinAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      if self.base_kode_tx_class=='18':
        oFinAgent = self.LFinAgent
        oFinAgent.AgentUsedPlafond += parameters.amount_dropping
        if oFinAgent.AgentPlafond < oFinAgent.AgentUsedPlafond:
          raise Exception, 'Plafond Agent Sudah Mencapai Limit..!!'  

        entries = self.DroppingTermin(parameters.amount_dropping,total_margin)
        if len(entries) == 0:
          raise Exception, 'Termin Belum Dibuat.!!'

        if self.feeAmount > 0:
          entries += [ {'sign': 'D', 'amount': self.feeAmount, 'kode_tx_class': '%s022' % self.base_kode_tx_class} ]

          #cr 
          feeDev = helper.GetObject('core.RekeningTransaksi', oFinAgent.RekDropping)
          oDT = feeDev.BuatTransaksiManual('core.DetilTransaksi', 'C', self.feeAmount)       
          oDT.LTransaksi = oTx
          oDT.keterangan = 'Fee Develover Istishna Loan %s' % self.nomor_rekening 
          oDT.tanggal_transaksi = oTx.tanggal_transaksi
          oDT.kode_jurnal = '11'
          oDT.Proses()
        
        #raise Exception, entries
      else: 
        entries = [
            {'sign': 'D', 'amount': parameters.amount_dropping + total_margin, 'kode_tx_class': '%s001' % self.base_kode_tx_class}, 
            {'sign': 'C', 'amount': parameters.amount_dropping, 'kode_tx_class': '%s011' % self.base_kode_tx_class}, 
            {'sign': 'C', 'amount': total_margin, 'kode_tx_class': '%s002' % self.base_kode_tx_class}
          ]

      oDetTxGroupDropping = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupDropping.ProcessDetails()
      
      oDropDetailSched.realize(oDetTxGroupDropping)
      
      FinAccount.dropping(self, parameters)
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      self.dropping_date = tgl_dropping
      return oDetTxGroupDropping 
    #--
    
    def disburse(self, parameters): #disburse-mbh 
      classname = self.CastToLowestDescendant().ClassName.lower()
      if classname in ('finmurabahahaccount','finistishnaaccount') :
        kode_entri = 'FTDB01'
      else : raise Exception, "murabahah based account - disburse must be customized for %s" % classname
      return FinAccount.disburse(self, kode_entri, parameters)    
    #--disburse-mbh
    
    def DroppingTermin(self, harga_beli, total_margin):
      return None
      
    def customizeRescheduleDetails(self): # mbh murabahah
      accrue = self.profit_accrue_balance
      if abs(accrue) > EPSILON:
        lstDetails.append({'sign': 'D', 'amount': accrue, 'kode_tx_class': '%s004' % self.base_kode_tx_class})
        lstDetails.append({'sign': 'C', 'amount': accrue, 'kode_tx_class': '%s002' % self.base_kode_tx_class})
      
      pending = self.profit_pending_balance  
      if abs(pending) > EPSILON:
        lstDetails.append({'sign': 'D', 'amount': pending, 'kode_tx_class': '%s006' % self.base_kode_tx_class})
        
      arrear = self.arrear_balance
      if abs(arrear) > EPSILON:
        lstDetails.append({'sign': 'C', 'amount': -arrear, 'kode_tx_class': '%s010' % self.base_kode_tx_class})
        lstDetails.append({'sign': 'D', 'amount': -arrear, 'kode_tx_class': '%s001' % self.base_kode_tx_class})
        
      return lstDetails
    #--
    
    # return dictionary with keys :
    # 'total_pokok', 'total_margin', 'total_bayar', 'mukasah'  
    def getPartialRepaymentInfo(self, jml_bln, pct_diskon):
      info = {'total_pokok' : 0, 'total_margin' : 0, 'mukasah' : 0, 'total_bayar' : 0 }
      oSched = self.LActiveSchedule
      if jml_bln > 0 :
        seqStart = oSched.getCurrentPaymentSeq()
        info = oSched.getPartialRepaymentInfo(seqStart, jml_bln, pct_diskon)
      return info
      
    # return dictionary with keys :
    # 'sisa_pokok', 'sisa_margin', 'bln_margin' , 'sisa_margin_hrs_dibayar', 'total_bayar', 'mukasah'  
    def getAcceleratedRepaymentInfo(self, bln_margin):
      finacc = self.CastToLowestDescendant()
      classname = finacc.ClassName.lower()
      sisa_pokok  = self.getPrincipalOutstanding() #-1 * (self.saldo + self.mmd_balance) 
      sisa_margin = self.getMarginOutstanding()
      sisa_margin_hrs_dibayar = 0
       
      oSched = self.LActiveSchedule
      currentSeq = oSched.getCurrentPaymentSeq()
      _tazir,_tawidh = oSched.getPenaltyAmount()
      
      if classname in ('finmurabahahaccount','finistishnaaccount') :
        bln_mukasah = self.period_count - oSched.active_seq - bln_margin + 1
        mukasah = oSched.getMukasah(bln_mukasah)
        margin_stl_mukasah = sisa_margin - mukasah
        info = {
          'sisa_pokok'    : sisa_pokok 
          , 'sisa_margin' : sisa_margin 
          , 'bln_margin'  : bln_margin
          , 'sisa_margin_hrs_dibayar' : margin_stl_mukasah
          , 'total_bayar' : sisa_pokok + margin_stl_mukasah 
          , 'mukasah' : mukasah, 'classname' : classname   
        }
      else :
        if bln_margin > 0 :
          sisa_margin_hrs_dibayar = oSched.getMarginHrsDibayar(currentSeq, bln_margin) 
        info = {
          'sisa_pokok'    : sisa_pokok 
          , 'sisa_margin' : sisa_margin 
          , 'bln_margin'  : bln_margin
          , 'sisa_margin_hrs_dibayar' : sisa_margin_hrs_dibayar
          , 'total_bayar' : sisa_pokok + sisa_margin_hrs_dibayar 
          , 'mukasah' : 0
          , 'classname' : classname  
        }
        
      info['penalty_tazir']  = _tazir 
      info['penalty_tawidh'] = _tawidh 

      return info
    
    def getNextPaymentAmount(self): # murabahah
      oSchedule = self.LActiveSchedule
      if oSchedule.IsNull: raise Exception, "Tidak ada jadwal angsuran"
      if oSchedule.completion_status == 'T': raise Exception, "Jadwal angsur sudah lunas"
      oSchedDetail = oSchedule.findDetailBySeq(oSchedule.active_seq)
      #return oSchedDetail.principal_amount + oSchedDetail.profit_amount
      return oSchedDetail.getNominalCicilan()

    #mrbh
    def getTrxJournal(self, param,jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = FinAccountUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal =='payment': # Manual Payment
        payment = copy.copy(param.payment)
        bayarPokok  = param.principal_amount 
        bayarMargin = param.profit_amount
        proporsiMukasah = param.mukasah_amount
        total_bayar = bayarPokok + bayarMargin
        tunggakan = -round(self.arrear_balance,2) # fixed by IK 22 August, abs resilience 
        tunggMargin = round(self.profit_arrear_balance,2) # fixed by IK 22 August, abs resilience
        tunggPokok = round(tunggakan-tunggMargin,2) #tunggakan pokok
        tunggMargin_debet = bayarMargin if bayarMargin<tunggMargin else tunggMargin #margin jt yang didebet    
        tunggakan_debet = round(tunggMargin_debet+min(tunggPokok,bayarPokok),2) #pitang jt yang didebet
        pitang_debet = round(total_bayar - tunggakan_debet,2)
        mmd_debet = round(bayarMargin - tunggMargin_debet,2)    
        ## mengurangi pokok piutang
        lgr += [{'sign': 'D', 'amount': payment, 'kode_tx_class': fau.get_tx(kode_jenis,'pembayaran') }]
        
        #book to tampungan jika agen wan prestasi
        if hasattr(param, 'ish_tampungan'):
          _proses = True
          lgr.append( {'sign': 'C', 'amount': param.ish_tampungan, 'kode_tx_class':  fau.get_tx(kode_jenis,'tampungan_angsuran')} )
          payment -= param.ish_tampungan
        else:
          tampungan = (self.saldo+self.arrear_balance)+payment
          if tampungan > 0.01:
            lgr.append( {'sign': 'C', 'amount': tampungan, 'kode_tx_class':  fau.get_tx(kode_jenis,'tampungan_angsuran')} )      
            _proses = False
          else:
            _proses = True

        if _proses:
          if proporsiMukasah > 0 :
            lgr.append( {'kode_tx_class' :  fau.get_tx(kode_jenis,'mukasah'), 'sign' : 'D', 'amount' :	proporsiMukasah } )      
          
          if self.arrear_balance < 0 : # Cek Tunggakan   
            # ada tunggakan: piutang murabahah jatuh tempo vs payment  
            lgr += [{'sign': 'C', 'amount': tunggakan_debet, 'kode_tx_class': fau.get_tx(kode_jenis,'tunggakan') }]                

          lgr += [{'sign': 'C', 'amount': pitang_debet, 'kode_tx_class':  fau.get_tx(kode_jenis,'outstanding') }]
          #--                  
  
          ## reverse akru vs yadit 
          if self.profit_accrue_balance > 0 : #_CD['IS_ACCRUE_INC_EXIST']
            akru = self.profit_accrue_balance # fixed by IK 22 August, abs resilience 
            bayarAkru = bayarMargin if akru > bayarMargin else akru     
            
            lgr += [
                {'sign': 'D', 'amount': bayarAkru, 'kode_tx_class':  fau.get_tx(kode_jenis,'pendapatan_akru')},
                {'sign': 'C', 'amount': bayarAkru, 'kode_tx_class':  fau.get_tx(kode_jenis,'yadit_mmd')}
            ]
  
          # book pendapatan, MMD vs pendapatan murabahah
          if tunggMargin > 0.0:
            #mutasiTunggMargin = min(tunggMargin, bayarMargin)
            lgr.append({'sign': 'D', 'amount': tunggMargin_debet, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan_margin')})
            
          lgr.append({'sign': 'D', 'amount': mmd_debet, 'kode_tx_class':  fau.get_tx(kode_jenis,'margin_ditangguhkan')} )
          lgr.append( {'sign': 'C', 'amount': bayarMargin, 'kode_tx_class':  fau.get_tx(kode_jenis,'pendapatan')} )
          
          #--
          
          if self.profit_pending_balance < 0: #_CD['IS_MARGIN_PENYELESAIAN_EXIST'] 
            nominal_mmd = bayarMargin if (bayarMargin < -self.profit_pending_balance) else -self.profit_pending_balance 
            # Untuk kol 3, 4, 5 akan terdapat Pdpt margin dalam penyelesaian (6xxx single entry). Pembayaran akan mengurangi catatan single entry tersebut
            lgr.append( {'sign': 'C', 'amount': nominal_mmd, 'kode_tx_class':  fau.get_tx(kode_jenis,'margin_penyelesaian')} )
            #self.profit_pending_balance = self.profit_pending_balance - nominal_mmd ## off balance sheet ?
                      
        #raise Exception, lgr
      
      # -- endif payment
      
      elif jns_jurnal =='pelunasan_dipercepat': # Manual pelunasan_dipercepat
        payment = copy.copy(param.payment)
        mukasah = param.mukasah_amount        
        profit_accrue = self.profit_accrue_balance # fixed by IK 22 August, abs resilience
        profit_pending = -self.profit_pending_balance # fixed by IK 22 August, abs resilience
        tunggakan = -self.arrear_balance # fixed by IK 22 August, abs resilience
        mmd_jt = self.profit_arrear_balance # fixed by IK 22 August, abs resilience
        mmd = self.mmd_balance # fixed by IK 22 August, abs resilience
        #margin = self.getMarginOutstanding() # fixed by IK 22 August, abs resilience
        saldo = -self.saldo # fixed by IK 22 August, abs resilience
        
        lgr += [ 
          {'kode_tx_class' : fau.get_tx(kode_jenis, 'pembayaran'), 'sign' : 'D', 'amount' :	payment },
          {'kode_tx_class' : fau.get_tx(kode_jenis, 'mukasah'), 'sign' : 'D', 'amount' :	mukasah }
        ] 
        # col 3 : anggap ada perpindahan kol ke kol 1
        if self.overall_col_level > 2 :
          lgr.append( {'sign': 'C', 'amount': profit_pending, 'kode_tx_class': fau.get_tx(kode_jenis, 'margin_penyelesaian')} ) # off balance sheet?
                                   
        # sudah kol 1
        lgr += [ 
          {'kode_tx_class' : fau.get_tx(kode_jenis, 'tunggakan'), 'sign' : 'C', 'amount' :	tunggakan}
          ,{ 'kode_tx_class' : fau.get_tx(kode_jenis, 'outstanding'), 'sign' : 'C', 'amount' :	saldo}

          ,{'kode_tx_class' : fau.get_tx(kode_jenis, 'pendapatan_akru'), 'sign' : 'D', 'amount' :	profit_accrue }
          ,{'kode_tx_class' : fau.get_tx(kode_jenis, 'yadit_mmd'), 'sign' : 'C', 'amount' :	profit_accrue }

          ,{'kode_tx_class' : fau.get_tx(kode_jenis, 'tunggakan_margin'), 'sign' : 'D', 'amount' :	mmd_jt }
          ,{'kode_tx_class' : fau.get_tx(kode_jenis, 'margin_ditangguhkan'), 'sign' : 'D', 'amount' :	mmd }
          ,{'kode_tx_class' : fau.get_tx(kode_jenis, 'pendapatan'), 'sign' : 'C', 'amount' :	mmd_jt+mmd }
        ] 
        lgr += FinAccount.getTrxJournal(self,param,'reverse_PPAP')                                                                          
        
      elif jns_jurnal =='input_AYDA': # transaksi input AYDA
        os_pokok = self.getPrincipalOutstanding() # sudah dinegatifkan
        nilai_jaminan = param.AYDA_balance # dari parameter, positif
        nilai_AYDA = nilai_jaminan if nilai_jaminan <= os_pokok else os_pokok
        biaya_AYDA = (os_pokok - nilai_jaminan) if (nilai_jaminan < os_pokok) else 0
        
        #journal input ayda
        lgr += [
        #create ayda balance
          {'sign': 'D', 'amount': os_pokok, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_aktiva' ) },
          #{'sign': 'D', 'amount': biaya_AYDA, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_biaya' ) },
        #clear loan balance
          {'sign': 'C', 'amount': -self.saldo, 'kode_tx_class':  fau.get_tx(kode_jenis,'outstanding') },
          {'sign': 'D', 'amount': self.mmd_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'margin_ditangguhkan' ) },
        #reverse Akru jika ada  
          {'sign': 'D', 'amount': self.profit_accrue_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'pendapatan_akru' ) },
          {'sign': 'C', 'amount': self.profit_accrue_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'yadit_mmd' ) },
        #reverse Tunggakan            
          {'sign': 'D', 'amount': self.profit_arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan_margin' ) },
          {'sign': 'C', 'amount': -self.arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan' ) },
          {'sign': 'C', 'amount': -self.profit_pending_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'margin_penyelesaian' ) },
        #Create PPANP lancar 1% by yayan 09dec14
          {'sign': 'D', 'amount': os_pokok*0.01, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_by_cadangan' ) },
          {'sign': 'C', 'amount': os_pokok*0.01, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_cadangan' ) }
        ]        
        lgr += FinAccount.getTrxJournal(self,param,'reverse_PPAP')                                                                          
      
      elif jns_jurnal =='hapus_buku': # transaksi Write Off Hapus Buku
        os_pokok = round(self.getPrincipalOutstanding(),2)                                           
        os_margin = round(self.getMarginOutstanding(),2)
        total_ppap  = round(self.reserved_common_balance+self.reserved_loss_balance+self.reserved_manual_balance,2)
        bentuk_reverse_ppap = os_pokok - total_ppap
        
        #journal input ayda
        lgr += [
        # rerevse PPAP
          {'sign': 'D', 'amount': self.reserved_common_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum' ) },
          {'sign': 'D', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus' ) },
          {'sign': 'D', 'amount': self.reserved_manual_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_adjustment' ) },
        # bentuk tambahan ppap
          #{'sign': 'C', 'amount': bentuk_reverse_ppap, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus' ) },
          {'sign': 'D', 'amount': bentuk_reverse_ppap, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus' ) },
        #clear loan balance
          {'sign': 'C', 'amount': -self.saldo, 'kode_tx_class':  fau.get_tx(kode_jenis,'outstanding') },
          {'sign': 'D', 'amount': self.mmd_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'margin_ditangguhkan' ) },
        #reverse Akru jika ada  
          {'sign': 'D', 'amount': self.profit_accrue_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'pendapatan_akru' ) },
          {'sign': 'C', 'amount': self.profit_accrue_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'yadit_mmd' ) },
        #reverse Tunggakan            
          {'sign': 'D', 'amount': self.profit_arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan_margin' ) },
          {'sign': 'C', 'amount': -self.arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan' ) },
          {'sign': 'C', 'amount': -self.profit_pending_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'margin_penyelesaian' ) },
        # Create Write Off  
          {'sign': 'D', 'amount': os_pokok, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off' ) },
          {'sign': 'D', 'amount': os_margin, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off_margin' ) }
        ]        
      else:
        lgr = FinAccount.getTrxJournal(self,param,jns_jurnal)
        
      return lgr
      
    def PrintBaseTransaction(self):
        config = self.Config
        
        corporate = self.Helper.CreateObject('Corporate')
        info = corporate.GetCabangInfo(self.kode_cabang)
        branchName = info.Nama_Cabang
        
        
        fha = self.pricing_model
        if fha == '1': fhaket = 'EFEKTIF'
        elif fha == '2': fhaket = 'SLIDING'
        elif fha == '4': fhaket = 'AN REST'
        elif fha == '5': fhaket = 'PROGRES'
        elif fha == '6': fhaket = 'FLAT'
        
        col = self.overall_col_level
        if col == 1: col_desc = 'Lancar'
        elif col == 2: col_desc = 'Dalam Perhatian Khusus'
        elif col == 3: col_desc = 'Kurang Lancar'
        elif col == 4: col_desc = 'Diragukan'
        elif col == 5: col_desc = 'Macet'
        else : col_desc = ''
        
        os_pokok = - (self.saldo + self.arrear_balance + self.MMD_balance + self.profit_accrue_balance)
        os_margin = self.MMD_balance + self.profit_accrue_balance
        
        tunggakan_pokok = -(self.arrear_balance + self.profit_arrear_balance)
        tunggakan_margin = self.profit_arrear_balance
        
        reportsource = '%s\\%s' % (corporate.GetUserHomeDir(), 'rpt_base_transaction.txt')
        rpt = report.textreport(reportsource)
        try:
            rpt.p_column({50: 'DATA DAN TRANSAKSI PEMBIAYAAN MURABAHAH'})
            bank_name = self.Config.SysVarIntf.GetStringSysVar('COMPANYINFO', 'NAME')
            rpt.p_column({58: bank_name})
            rpt.p_line('=' * 125)
            rpt.p_line('Nomor Cabang    : %s - %s' % (self.kode_cabang, branchName))
            rpt.p_column({
                1 : 'Nomor Kartu     : '+self.nomor_rekening })
            rpt.p_column({
                1 : 'Nama Nasabah    : '+self.nama_rekening })
            rpt.p_column({
                1: 'Kode Produk PD   : '+self.product_code+' - '+self.LFinProduct.product_name ,
                42: 'Jangka Waktu  : '+str(self.period_count)+' Bulan'})
            rpt.p_column({
                1 : 'Tanggal Akad    : '+rpt.fm_dmy(self.tgl_akad),
                42: 'Tgl. J. Tempo : '+rpt.fm_dmy(self.due_date)})
            rpt.p_column({
                1 : 'Harga Perolehan : '+rpt.fm_float(self.base_price),
                42: 'Uang Muka     : '+rpt.fm_float(self.urbun)})
            rpt.p_column({
                1 : 'Margin          : '+rpt.fm_float(self.initial_profit_balance),
                42: 'Harga Jual    : '+rpt.fm_float(self.base_price - self.urbun)})
            rpt.p_column({
                1 : 'Jml Angs./Bulan : '+rpt.fm_float(self.monthly_installment),
                42: 'Tgl Angs Brkt : '})
            rpt.p_column({
                1 : 'Margin Ekv Rate : '+str(self.targeted_eqv_rate),               
                42: 'Pricing       : '+ fhaket})
            rpt.p_column({
                1 : 'Nomor Rek. Angs : '+self.norek_paymentsrc,
                42: 'Kolek         : '+str(self.manual_col_level)+' - '+str(col_desc)})
            rpt.p_line('-------------------- POKOK ------------- MARGIN ------------- JUMLAH ----------')
            rpt.p_column({
                1 : 'Outstanding     : '+rpt.fm_float(os_pokok),
                40: rpt.fm_float(os_margin),
                60: rpt.fm_float(os_pokok + os_margin)})
            rpt.p_column({
                1 : 'Tunggakan       : '+rpt.fm_float(tunggakan_pokok),
                40: rpt.fm_float(tunggakan_margin),
                60: rpt.fm_float(tunggakan_pokok + tunggakan_margin)})
            rpt.p_column({
                1 : 'Tgl Pemb Akhir  : '+rpt.fm_dmy(self.due_date)})
            rpt.p_header('Halaman : <<PAGE:3>>', {'view_infirstpage': 0, 'formatted': 1})
            rpt.p_header('-' * 125)
            rpt.p_header(rpt.fm_column({
                1  : 'Tgl Trans', 
                14 : 'KdTrx',
                21 : 'No Interface',
                34 : 'Keterangan',
                72 : 'Mutasi',
                92 : 'Jumlah Transaksi',
                112: 'User Input'}))
            rpt.p_header('-' * 125)
            
            sql = """
            select c.tanggal_transaksi, kode_entri, kode_tx_class, a.keterangan, jenis_mutasi,  
                nilai_mutasi, c.user_input  from %(detiltransaksi)s a, %(detiltransgroup)s b , %(transaksi)s c  
                where a.nomor_rekening = '%(norek)s' 
                and a.id_detiltransgroup = b.id_detiltransgroup  
                and a.id_transaksi = c.id_transaksi  
                order by tanggal_transaksi
            """ % {'detiltransaksi' : config.MapDBTableName('core.detiltransaksi'), 
              'detiltransgroup': config.MapDBTableName('core.detiltransgroup'),
              'transaksi' : config.MapDBTableName('core.transaksi'),  
              'norek' : self.nomor_rekening}
            q = config.CreateSQL(sql)
            q.active = 1; res = q.rawresult
            
            while not res.Eof:
                rpt.p_column({
                    1  : rpt.fm_dmy(res.tanggal_transaksi),
                    14 : res.kode_entri,
                    21 : res.kode_tx_class,
                    34 : res.keterangan[:35],
                    72 : res.jenis_mutasi,
                    90 : rpt.fm_float(res.nilai_mutasi),
                    112: res.user_input})
                        
                res.Next()
            
            rpt.p_line('')
            rpt.p_line('*** Akhir Record ***')
        finally:
            rpt.close()
        
        return reportsource      
        #-- 
        
    def PrintArrears(self):
        config = self.Config
        
        corporate = self.Helper.CreateObject('Corporate')
        
        info2 = corporate.GetCabangInfo(self.kode_cabang)
        branchName = info2.Nama_Cabang
        
        #nmvalt = kiblatinfo.GetCurrencyName(config, self.mlkmu)
        #branchName = kiblatinfo.BranchInfo(config, self.mlkdc)
        
        reportsource = '%s\\%s' % (corporate.GetUserHomeDir(), 'rpt_arrears.txt')
        #reportsource = config.userhomedirectory + base.REPORT_ARREARS
        rpt = report.textreport(reportsource)
        try:
            rpt.set_format({'pagelength': 0, 'topmargin': 0})
            bank_name = self.Config.SysVarIntf.GetStringSysVar('COMPANYINFO', 'NAME')
            rpt.p_line(bank_name)
            rpt.p_line('No. Cabang : %s - %s' % (self.kode_cabang, branchName))
            rpt.p_line('DAFTAR HISTORI TUNGGAKAN')
            rpt.p_line('')
            rpt.p_line('Nomor Kartu  : ' + self.nomor_rekening)
            rpt.p_line('Nama Nasabah : ' + self.nama_rekening)
            rpt.p_line('Mata Uang    : %s '  % (self.kode_valuta))
            rpt.p_line('-' * 79)
            rpt.p_column({
                2   : 'Tanggal', 
                17  : 'Angsuran',
                37  : 'Angsuran', 
                58  : 'Jumlah', 
                72  : 'Angsuran'
            })
            rpt.p_column({
                2   : 'Angsuran', 
                18  : 'Pokok', 
                38  : 'Margin',
                57  : 'Angsuran',
                75  : 'ke'
            })
            rpt.p_line('-' * 79)
            
            q = config.CreateSQL("select b.sched_date, b.principal_amount, b.profit_amount, b.principal_amount + b.profit_amount as total_amount, seq_number from finpaymentschedule a, finpaymentscheddetail b  \
                where a.id_schedule = b.id_schedule and nomor_rekening = '%s' and a.is_active = 'T' \
                and seq_number <> 0 and seq_number >= a.active_seq and seq_number < a.normative_seq "\
                %(self.nomor_rekening))
            q.active = 1; res = q.rawresult
            
            while not res.Eof:
                rpt.p_column({
                    2  : rpt.fm_dmy(res.sched_date),
                    10 : rpt.fm_float(res.principal_amount),
                    30 : rpt.fm_float(res.profit_amount),
                    51 : rpt.fm_float(res.total_amount),
                    75 : str(res.seq_number)})
                        
                res.Next()
            
            rpt.p_line('')
            rpt.p_line('*** Akhir Record ***')
        finally:
            rpt.close()
        
        return reportsource
        #--
    
    def PrintCard(self):
        config = self.Config

        oSchedule = self.LActiveSchedule
        if oSchedule.IsNull:
          raise Exception, "Pembiayaan ini belum memiliki jadwal angsuran"
        
        corporate = self.Helper.CreateObject('Corporate')
        
        info2 = corporate.GetCabangInfo(self.kode_cabang)
        branchName = info2.Nama_Cabang
        
        if self.LCustomer.jenis_nasabah == 'I' :
           sql = """SELECT 
                    alamat_rumah_jalan 
                    FROM %(nasabahindividu)s n, %(individu)s i 
                    WHERE n.id_individu = i.id_individu and n.nomor_nasabah = '%(nomornasabah)s' """ %{ 'nasabahindividu' : config.MapDBTableName('core.nasabahindividu'), 'individu' : config.MapDBTableName('core.individu'), 'nomornasabah' : self.nomor_nasabah} 
           q1 = config.CreateSQL(sql)
           q1.active = 1; res_alamat = q1.rawresult
           while not res_alamat.Eof:
              alamat_rumah = res_alamat.alamat_rumah_jalan
              res_alamat.Next()
        elif self.LCustomer.jenis_nasabah == 'K' :
           q1 = config.CreateSQL("SELECT \
                    alamat_rumah_jalan \
                    FROM %s n\
                    WHERE n.nomor_nasabah = '%s' " %(config.MapDBTableName('core.nasabahkorporat'), self.nomor_nasabah))
           q1.active = 1; res_alamat = q1.rawresult
           while not res_alamat.Eof:
              alamat_rumah = res_alamat.alamat_rumah_jalan
              res_alamat.Next()
              
        create_date = config.Now()
        create_user = config.SecurityContext.InitUser


        reportsource = '%s\\%s' % (corporate.GetUserHomeDir(), 'rpt_card.txt')
        rpt = report.textreport(reportsource)
        try:
            totalamount = self.LActiveSchedule.initial_principal_amt + self.LActiveSchedule.initial_profit_amt 
            rpt.p_column({33: 'KARTU PEMBIAYAAN MURABAHAH'})
            bank_name = self.Config.SysVarIntf.GetStringSysVar('COMPANYINFO', 'NAME')
            rpt.p_column({35: bank_name})
            rpt.p_line('=' * 100)
            rpt.p_column({
                1   : branchName,
                60 : 'Paraf : ',
                70 : 'Maker ',
                80 : 'Checker  Approval',
            })
            rpt.p_line('-' * 100)
            rpt.p_column({3   : 'Nomor ', 15  : 'Nama ', 35  : 'Tanggal ', 48  : 'Angsuran ',
                60 : 'Jatuh ', 78 : 'Jangka ', 90 : 'Debet'})
            rpt.p_column({1   : 'Pembiayaan', 14  : 'Debitur', 33  : 'Diberikan',
                48  : 'Pertama', 60 : 'Tempo', 78 : 'Waktu', 88 : 'Rekening'})
            rpt.p_column({1   : self.nomor_rekening, 14  : self.nama_rekening, 33  : rpt.fm_dmy(self.dropping_date),
                48  : rpt.fm_dmy(self.due_date), 60 : rpt.fm_dmy(self.due_date),
                78 : str(self.period_count), 88 : self.norek_paymentsrc})
            rpt.p_line('\n')
            rpt.p_column({1   : 'Alamat Rumah', 24  : 'Margin Setara', 50  : 'Jumlah Nominal',
                75 : 'Jumlah Margin'})
            rpt.p_column({1   : alamat_rumah, 24  : rpt.fm_float(self.targeted_eqv_rate, 2, 6) + ' %', 50  : rpt.fm_float(self.LActiveSchedule.initial_principal_amt, 0, 14),
                75 : rpt.fm_float(self.LActiveSchedule.initial_profit_amt, 0, 13)})
            rpt.p_line('\n')
            rpt.p_column({1   : 'Alamat Surat', 24  : 'Account Officer', 50 : 'Nomor Akad'})
            rpt.p_column({1   : '' , 24  : ''  , 50 : self.contract_number})
            rpt.p_line('\n')
            rpt.p_column({1   : 'Tanggal', 20  : 'Jatuh ', 37  : 'Plafond', 60 : 'Nilai',
                75 : 'Margin', 90 : 'Pokok'})
            rpt.p_column({3   : 'Akad', 18  : 'Tempo Akad', 36  : 'Pembiayaan',
                60 : 'Jaminan', 75 : '', 90 : ''})
            rpt.p_column({1  : rpt.fm_dmy(self.tgl_akad), 18  : rpt.fm_dmy(self.due_date),
                33  : rpt.fm_float(self.base_price, 0, 14), 55 : rpt.fm_float(self.collateral_value, 0, 14), 
                70 : rpt.fm_float(self.LActiveSchedule.initial_profit_amt, 0, 14), 85 : rpt.fm_float(self.LActiveSchedule.initial_principal_amt, 0, 14)})
            rpt.p_line('\n')
            rpt.p_line('')
            rpt.p_header('HALAMAN <<PAGE:3>>', {'view_infirstpage': 0, 'formatted': 1})
            rpt.p_header('-' * 110)
            rpt.p_header(rpt.fm_column({1   : 'Ang', 7  : 'Tanggal', 20  : 'Jumlah', 36  : 'Bagian Pokok', 52  : 'Bagian Margin',
                68  : 'Sisa Pokok', 84  : 'Sisa Margin', 100  : 'Sisa Angs'}))
            rpt.p_column({64  : rpt.fm_float(self.LActiveSchedule.initial_principal_amt, 0, 14), 80  : rpt.fm_float(self.LActiveSchedule.initial_profit_amt, 0, 14),
                96  : rpt.fm_float(totalamount, 0, 14)})
            rpt.p_header('-' * 110)

            q = config.CreateSQL("SELECT \
                    id_scheddetail,\
                    seq_number,\
                    sched_date,\
                    principal_amount, \
                    profit_amount,\
                    realized,\
                    id_detiltransgroup \
                    FROM finpaymentscheddetail sdd \
                    WHERE sdd.id_schedule = '%s' and seq_number <> 0 \
                    ORDER BY id_scheddetail, seq_number" %(self.id_schedule))
            q.active = 1; res = q.rawresult
            
            #~ ON EVERY ROW
            sp = self.LActiveSchedule.initial_principal_amt
            sm = self.LActiveSchedule.initial_profit_amt
            ts = sp + sm
            n = self.period_count
            i=1
            
            while not res.Eof:
                sp = sp - res.principal_amount
                sm = sm - res.profit_amount
                ts = sp + sm
                tp = res.principal_amount + res.profit_amount
                
                rpt.p_column({
                    1   : '%3d' % (i),
                    7   : rpt.fm_dmy(res.sched_date),
                    16  : rpt.fm_float(tp, 0, 14),
                    32  : rpt.fm_float(res.principal_amount, 0, 14),
                    48  : rpt.fm_float(res.profit_amount, 0, 14),
                    64  : rpt.fm_float(sp, 0, 14),
                    80  : rpt.fm_float(sm, 0, 14),
                    96 : rpt.fm_float(ts, 0, 14)
                })
                leftrow = 66 - rpt.get_row()
                if (i < n) and ((leftrow == 3) or ((leftrow == 4) and (i % 12) == 0)):
                    if leftrow == 4:
                        rpt.p_line('')
                    rpt.p_line('=' * 110)
                    rpt.p_line('Bersambung...')
                    rpt.p_line('=' * 110)
                    rpt.p_column({
                        39  : 'USER-ID CETAK : ' ,
                        68  : 'TGL. CETAK : '
                    })
                elif (i % 12) == 0:
                    rpt.p_line('')
                i=i+1
                res.Next()
            #~ end while
                        
            rpt.p_line('=' * 110)
            rpt.p_column({
                1   : 'TOTAL PEMBIAYAAN :',
                19  : rpt.fm_float(totalamount, 0, 16),
                36  : rpt.fm_float(self.LActiveSchedule.initial_principal_amt, 0, 16),
                52  : rpt.fm_float(self.LActiveSchedule.initial_profit_amt, 0, 16)
            })
            rpt.p_line('=' * 110)
            rpt.p_column({
                39  : 'USER-ID CETAK : ', #+  create_user  ,
                68  : 'TGL. CETAK : ' #+ rpt.fm_dmy(create_date)
            })            
            
            rpt.p_line('*** Akhir Record ***')
        finally:
            rpt.close()
        
        return reportsource
        #--
#--    

class FinIjarahAccount(FinAccount):
    # Static variables
    pobject_classname = 'FinIjarahAccount'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '16'
    
    #kode_jenis = 'IJRH'

    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      self.kode_jenis = 'IJR' 
      self.financing_model = 'T'
      FinAccount.OnCreate(self, param)
      recSrc = param['FinIjarahAccount']
      # set fields with simple / direct 1-on-1 assignment from record with same field name
      atutil.transferAttributes(helper, 
        [ 'initial_asset_value', 
          'pricing_model'
        ],
        self, recSrc
      )
      
      self.max_eqv_rate = self.targeted_eqv_rate
      self.max_period_count = self.period_count
                
      dictInitValues = {
        #'automatic_payment': 'T',
        'depreciation_balance': 0.0,
        'income_accrue_balance': 0.0,
        'income_pending_balance': 0.0,
        'leasing_price': 0.0,
        'depr_method': 'E',
        'predicted_profit': 0.0,
        'lease_principal_recv': 0.0,
        'lease_profit_recv': 0.0,
        'reserved_loss_recv': 0.0
      }
      for initField, initValue in dictInitValues.iteritems():
        self.SetFieldByName(initField, initValue)
      self.total_facility_limit = self.initial_asset_value
      self.loan_amount = self.initial_asset_value
      self.setFlatRate()
      config.FlushUpdates()
    #--       
      
    def ProcessUIData(self, recData):
      pass
    
    def setFlatRate(self):
      config = self.Config; helper = self.Helper
      if self.pricing_model in ('6',6):
        effRate = self.targeted_eqv_rate
        nPeriod = self.period_count
        fl_rate = libs.effectiveToFlatRate(effRate, nPeriod)
        self.targeted_eqv_rate_flat = fl_rate  
    
    #ijarah
    def reschedule(self, parameters):
      newSch = self.LTmpDroppingSchedule
      _period = newSch.getMaxSeq()      
      self.leasing_price = newSch.getLeasingPrice()
      #catat max_rate & max_tenor
      self.max_eqv_rate = parameters.new_eqv_rate if parameters.new_eqv_rate>self.max_eqv_rate else self.max_eqv_rate
      self.max_period_count = _period if _period>self.max_period_count else self.max_period_count
      self.setFlatRate()
      FinAccount.reschedule(self, parameters)       

    def getPrincipalOutstanding(self):
      res = self.getPokokMargin()
      sisa_pokok = 0
      if not res.Eof:
        sisa_pokok = res.sisa_pokok or 0
      
      return sisa_pokok
      
    def getMarginOutstanding(self):
      res = self.getPokokMargin()
      sisa_margin = 0
      if not res.Eof:
        sisa_margin = res.sisa_margin or 0
      return sisa_margin

    def getPokokMargin(self):
      #raise Exception, self.id_schedule
      sql = """
          select sum(profit_amount) as sisa_margin, sum(principal_amount) as sisa_pokok 
          from %s a
          where a.id_schedule = %d and nvl(a.realized,'F')='F' and a.seq_number>0 
        """ % (self.config.MapDBTableName('finpaymentscheddetail'),self.id_schedule)
      res = self.config.CreateSQL(sql).RawResult
      
      return res

    def OnDelete(self):
      pass
      
    def getLoanAmount(self): # virtual method
      return self.initial_asset_value
      
    # reverse aktiva ijarah dengan akum. depresiasi
    def reverseAktiva(self, id_otorisasi=0): 
      helper = self.Helper
      config = self.Config

      kode_jenis = self.kode_jenis
      kode_entri = 'FPA06'
      keterangan = 'Rev. Aktiva and Depr.'
      
      #diaktifkan kembali untuk reverse..
      self.status_rekening = 1
      
      fau = FinAccountUtils(config)
      periodHelper = helper.CreateObject('core.PeriodHelper')
      tgl_trx = periodHelper.GetAccountingDate()
      
      if id_otorisasi==0: 
        oTx = FinAccount.initTransaksiEOD(self, kode_entri, tgl_trx, keterangan)
      else: 
        oTx = FinAccount.initTransaksi(self, kode_entri, tgl_trx, keterangan)
      
      pendapatan_aktiva = self.saldo + self.depreciation_balance  
      entries = [
        {'sign': 'C', 'amount': -self.saldo, 'kode_tx_class': fau.get_tx(kode_jenis,'nilai_perolehan') },
        {'sign': 'D', 'amount': self.depreciation_balance, 'kode_tx_class': fau.get_tx(kode_jenis,'akum_depresiasi') }
      ]

      if pendapatan_aktiva < 0:
        entries += [{'sign': 'D', 'amount': -pendapatan_aktiva, 'kode_tx_class': fau.get_tx(kode_jenis,'rugi_penjualan') }]
      else:
        entries += [{'sign': 'C', 'amount': pendapatan_aktiva, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan_penjualan') }]

      oDetTxGroupDropping = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupDropping.ProcessDetails()
      
      #oTx.CreateJournal()
      helper.CreatePObject('PendingTransactionJournal', oTx)
      
      #nonaktifkan untuk seterusnya..
      self.status_rekening = 3
      
      return oDetTxGroupDropping 
      
    # ijarah
    def dropping(self, parameters): 
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      
      parameters = atutil.GeneralObject(parameters)
      
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      
      norek_tujuan = parameters.getAttrByName('LDisbursalDest.Nomor_Rekening')         
      lAccDroppingDest = helper.GetObject('core.RekeningLiabilitas', norek_tujuan)
      if lAccDroppingDest.IsNull :
        raise Exception, "rekening tujuan dropping tidak ada"

      ## CREATE PAYMENT SCHEDULE
      tempSchedule = self.LTmpDroppingSchedule
      if parameters.jenis_jadwal_angsur in ("T"):
        if not tempSchedule.IsNull:
          self.LTmpDroppingSchedule = None
        #--
        install = libs.installment(self.period_count, self.Config)
        if parameters.hasAttribute('LParameterTiering.tiering_code'):
          tiering_code = parameters.getAttrByName('LParameterTiering.tiering_code') 
        else:
          tiering_code = None 
        install.runSimulation(self.pricing_model, parameters.amount_dropping, self.targeted_eqv_rate, tiering_code)
                 
        total_margin = install.margin
        oPaymentSchedule = helper.CreatePObject("FinPaymentSchedule")
        oDropDetailSched = oPaymentSchedule.initFromSimulation(self, tgl_dropping, parameters.tgl_angsur_awal, install, True, False) # tanpa mukasah
        #oDropDetailSched = oPaymentSchedule.initFromSimulationWithMukasahPct(self, tgl_dropping, parameters.tgl_angsur_awal, install, mukasah_rate, parameters.mukasah_seq_limit, True, mksh_install)
        self.LActiveSchedule = oPaymentSchedule
        oSchedule = oPaymentSchedule
      else:
        if tempSchedule.IsNull:
          raise Exception, "Jadwal angsur manual belum dibuat"

        tempSchedule.genericInitSimulation(self, tgl_dropping, parameters.tgl_angsur_awal, 0)
        tempSchedule.initial_principal_amt = parameters.amount_dropping

        if int(round(timeutil.stdDate(config, parameters.tgl_angsur_awal))) != int(round(timeutil.stdDate(config, tempSchedule.first_install_date))):
          raise Exception, "Tanggal jadwal angsur manual berbeda dengan tanggal angsur awal"
        if self.is_external == 'F':
          if int(round(timeutil.stdDate(config, tgl_dropping))) != int(round(timeutil.stdDate(config, tempSchedule.dropping_date))) :
            raise Exception, "Tanggal jadwal angsur manual berbeda dengan tanggal angsur awal"
        total_margin = tempSchedule.initial_profit_amt
        oDropDetailSched = tempSchedule.createDroppingEntry()
        self.LTmpDroppingSchedule = None
        self.LActiveSchedule = tempSchedule
        tempSchedule.setAsActive()
        oSchedule = tempSchedule
      #--
      
      #self.leasing_price = oSchedule.findDetailBySeq(1)
      self.predicted_margin_amount = oSchedule.initial_profit_amt
      self.PREDICTED_PROFIT = oSchedule.initial_profit_amt
      self.due_date = oSchedule.getLastPaymentDate()
      
      newSch = self.LActiveSchedule      
      self.leasing_price = newSch.getLeasingPrice()
      # CREATE TRANSACTION
      if not parameters.hasAttribute('kode_entri'):
        kode_entri = 'FTD06' 
      else:
        kode_entri = parameters.kode_entri

      if not parameters.hasAttribute('keterangan'):
        keterangan = 'DROPPING %s' % self.nomor_rekening
      else:
        keterangan = parameters.keterangan

      oTx = FinAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      
      entries = [
          {'sign': 'D', 'amount': parameters.amount_dropping, 'kode_tx_class': '16001'}
        ]

      oDetTxGroupDropping = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupDropping.ProcessDetails()
      
      oDropDetailSched.realize(oDetTxGroupDropping)

      oDT = lAccDroppingDest.BuatTransaksiManual('core.DetilTransaksi', 'C', parameters.amount_dropping);
      oDT.LTransaksi = oTx
      oDT.keterangan = oTx.keterangan
      oDT.tanggal_transaksi = oTx.tanggal_transaksi
      oDT.kode_jurnal = '11'
      oDT.Proses()  
      
      if parameters.hasAttribute('LDevelover.Nomor_Rekening'):
        FinAccount.TrfToDevelover(self, parameters, oTx, parameters.amount_dropping)

      FinAccount.dropping(self, parameters)      
      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      self.dropping_date = tgl_dropping
      self.last_depr_date = tgl_dropping
      self.end_depr_date = oSchedule.getLastPaymentDate()
      oSchedule.accrue_seq = 1
      self.setFlatRate()
      return oDetTxGroupDropping 
    #--
    
    #ijarah
    def getTrxJournal(self, param,jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = FinAccountUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal in ['payment','pelunasan_dipercepat']: # Manual Payment
        payment = copy.copy(param.payment)
        bayarPokok  = param.principal_amount 
        bayarMargin = param.profit_amount

        piutang_sewa_pokok = -self.lease_principal_recv # fixed by IK 22 August, abs resilience
        piutang_sewa_margin = -self.lease_profit_recv # fixed by IK 22 August, abs resilience
        sisa_pokok = 0;sisa_margin=0
        ## mengurangi pokok piutang
        lgr += [{'sign': 'D', 'amount': payment, 'kode_tx_class': fau.get_tx(kode_jenis,'pembayaran') }]
        if self.arrear_balance < -0.01 :
          # ada tunggakan: selesaikan tunggakan terlebih dahulu
          tunggakan = -self.arrear_balance # fixed by IK 22 August, abs resilience
          bayarTunggakan = copy.copy(bayarPokok)
          #sisaTunggakan = 0       
          if (tunggakan < bayarTunggakan) :
            bayarTunggakan = tunggakan
          
          bayarPokok = bayarPokok - bayarTunggakan         
      
          lgr += [
            {'sign': 'C', 'amount': bayarTunggakan, 'kode_tx_class': fau.get_tx(kode_jenis,'tunggakan') }
          ]

        if self.profit_arrear_balance < -0.01 :
          # ada tagian kontijensi kol > 2
          t_margin = -self.profit_arrear_balance 
          bayar_t_margin = copy.copy(bayarMargin)
          #sisaTunggakan = 0       
          if (t_margin < bayar_t_margin) :
            bayar_t_margin = t_margin

          bayarMargin -= bayar_t_margin         
      
          lgr += [
            {'sign': 'C', 'amount': bayar_t_margin, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan') },
            {'sign': 'C', 'amount': bayar_t_margin, 'kode_tx_class': fau.get_tx(kode_jenis,'tagihan_ujrah') }
          ]

        # tidak ada tunggakan:piutang murabahah vs payment
        bayar_sewa_pokok = bayarPokok
        bayar_sewa_margin = bayarMargin
        if (piutang_sewa_pokok < bayar_sewa_pokok):
          bayar_sewa_pokok = piutang_sewa_pokok
          sisa_pokok = bayarPokok - piutang_sewa_pokok 
          
        if (piutang_sewa_margin < bayar_sewa_margin):
          bayar_sewa_margin = piutang_sewa_margin
          sisa_margin = bayarMargin - piutang_sewa_margin 
          
        
        lgr += [
          {'sign': 'C', 'amount': bayar_sewa_pokok, 'kode_tx_class': fau.get_tx(kode_jenis,'piutang_sewa_pokok') },
          {'sign': 'C', 'amount': bayar_sewa_margin, 'kode_tx_class': fau.get_tx(kode_jenis,'piutang_sewa_margin') }
        ]
        #lra.append( {'sign': 'D', 'amount': payment} )
        if sisa_pokok > 0 or sisa_margin > 0: 
          lgr += [
            {'sign': 'C', 'amount': sisa_pokok+sisa_margin, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan') }
          ]
          
        if jns_jurnal =='pelunasan_dipercepat':
          lgr += FinAccount.getTrxJournal(self,param,'reverse_PPAP')                                                                          
                  
        #raise Exception, lgr
      # -- endif payment 
           
      elif jns_jurnal =='reverse_yadit':
        yadit_p = -round(self.lease_principal_recv,4) 
        yadit_m = -round(self.lease_profit_recv,4)
         
        #journal input ayda
        #reverse yadit req by yayan 14dec14
        if yadit_p > 0:
          lgr += [
            {'sign': 'D', 'amount': yadit_p, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan') },
            {'sign': 'C', 'amount': yadit_p, 'kode_tx_class': fau.get_tx(kode_jenis,'piutang_sewa_pokok') }
          ]
        if yadit_m > 0:
          lgr += [
            {'sign': 'D', 'amount': yadit_m, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan') },
            {'sign': 'C', 'amount': yadit_m, 'kode_tx_class': fau.get_tx(kode_jenis,'piutang_sewa_margin') }
          ]
                    
      elif jns_jurnal in ['penjualan_aktiva']: # Manual penjualan_aktiva
        pendapatan_penjualan = abs(param.pendapatan_aktiva)
        saldo  = param.saldo 
        depr_balance = param.depreciation_balance
        netto_aktiva = param.netto_aktiva
        pdpt_sewa = param.pdpt_sewa
        payment = netto_aktiva + param.pendapatan_aktiva+pdpt_sewa

        lgr += self.getTrxJournal(param,'reverse_yadit')
          
        #book transaction 
        lgr += [
          {'sign': 'D', 'amount': payment, 'kode_tx_class': fau.get_tx(kode_jenis,'pembayaran') },
          {'sign': 'C', 'amount': saldo, 'kode_tx_class': fau.get_tx(kode_jenis,'nilai_perolehan') },
          {'sign': 'D', 'amount': depr_balance, 'kode_tx_class': fau.get_tx(kode_jenis,'akum_depresiasi') },
          #{'sign': 'C', 'amount': pdpt_sewa, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan') } 
          {'sign': 'C', 'amount': pdpt_sewa, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan_penjualan') } # req by yayan 14dec14
        ]
        if param.pendapatan_aktiva < 0:
          lgr += [{'sign': 'D', 'amount': pendapatan_penjualan, 'kode_tx_class': fau.get_tx(kode_jenis,'rugi_penjualan') }]
        else:
          lgr += [{'sign': 'C', 'amount': pendapatan_penjualan, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan_penjualan') }]
          
        lgr += FinAccount.getTrxJournal(self,param,'reverse_PPAP') 
        
        #raise Exception, lgr                                                                            
      elif jns_jurnal =='input_AYDA': # transaksi input AYDA
        asset_ijr = -round(self.saldo,4)                     
        piutang_jt = -round(self.arrear_balance,4)
        depr_balance = round(self.depreciation_balance,4)
        nilai_AYDA = asset_ijr-depr_balance+piutang_jt
        
        lgr += self.getTrxJournal(param,'reverse_yadit')
          
        lgr += [
        #create ayda balance
          {'sign': 'D', 'amount': asset_ijr-depr_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_aktiva' ) },
          
        #clear loan balance
          {'sign': 'D', 'amount': depr_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'akum_depresiasi') },
          {'sign': 'C', 'amount': asset_ijr, 'kode_tx_class':  fau.get_tx(kode_jenis,'nilai_perolehan') },

        #clear tunggakan
          {'sign': 'D', 'amount': piutang_jt, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_aktiva' ) },
          {'sign': 'C', 'amount': piutang_jt, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan' ) },
          
        #Create PPANP lancar 1% by yayan 09dec14
          {'sign': 'C', 'amount': nilai_AYDA*0.01, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_cadangan' ) },
          {'sign': 'D', 'amount': nilai_AYDA*0.01, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_by_cadangan' ) }
        ]
        lgr += FinAccount.getTrxJournal(self,param,'reverse_PPAP')                                                                          
              
      elif jns_jurnal =='hapus_buku': # transaksi Write Off Hapus Buku
        asset_ijr = - round(self.saldo,4)                     
        piutang_jt = - round(self.arrear_balance,4)
        total_ppap  = round((self.reserved_common_balance+self.reserved_loss_balance+self.reserved_manual_balance),4)
        depr_balance = round(self.depreciation_balance,4)
        bentuk_reverse_ppap = asset_ijr - depr_balance - total_ppap

        lgr += self.getTrxJournal(param,'reverse_yadit')

        lgr += [
        #clear loan balance
          {'sign': 'D', 'amount': depr_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'akum_depresiasi' ) },
          #{'sign': 'D', 'amount': asset_ijr-depr_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'rugi_write_off' ) },
          {'sign': 'C', 'amount': asset_ijr, 'kode_tx_class':  fau.get_tx(kode_jenis,'nilai_perolehan') },
          {'sign': 'D', 'amount': asset_ijr-depr_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off' ) },
        #clear piutang
          {'sign': 'D', 'amount': piutang_jt, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus' ) },
          #{'sign': 'D', 'amount': piutang_jt-total_ppap, 'kode_tx_class':  fau.get_tx(kode_jenis,'rugi_write_off' ) },
          {'sign': 'C', 'amount': piutang_jt, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan') },
          {'sign': 'D', 'amount': piutang_jt, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off' ) },
        #create WO margin
          {'sign': 'C', 'amount': -self.profit_arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tagihan_ujrah' ) },
          {'sign': 'D', 'amount': -self.profit_arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off_margin' ) },

        # rerevse PPAP
          {'sign': 'D', 'amount': self.reserved_common_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum' ) },
          {'sign': 'D', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus' ) },
          {'sign': 'D', 'amount': self.reserved_manual_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_adjustment' ) },
        # bentuk tambahan ppap
          #{'sign': 'C', 'amount': bentuk_reverse_ppap, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus' ) },
          {'sign': 'D', 'amount': bentuk_reverse_ppap, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus' ) }
        ]        
        #lgr += FinAccount.getTrxJournal(self,param,'reverse_PPAP') 

        #raise Exception, lgr
                           
      else:
        lgr = FinAccount.getTrxJournal(self,param,jns_jurnal)

      
      return lgr
          
#-- 
   
class FinQardhAccount(FinMurabahahAccount):
    # Static variables
    pobject_classname = 'FinQardhAccount'
    pobject_keys      = ['nomor_rekening']
    
    financing_model = 'T'
    finmurabahah_type = 'Q'

    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      FinAccount.OnCreate(self, param)
      self.kode_jenis = 'QRD'
      recSrc = param['FinQardhAccount']
      # set fields with simple / direct 1-on-1 assignment from record with same field name
      atutil.transferAttributes(helper, 
        [ 'base_price', 
          'pricing_model',
          'qardh_due_date'
        ],
        self, recSrc
      )
      dictInitValues = {
        'discount_balance' : 0.0, 
        'initial_base_balance' : 0.0, 
        'initial_profit_balance' : 0.0, 
        'MMD_balance' : 0.0, 
        'profit_accrue_balance': 0.0, 
        'profit_balance': 0.0, 
        'profit_pending_balance': 0.0, 
        'write_off_balance': 0.0,
        
        'daily_fine_amount' : 0.0        
      }
      for initField, initValue in dictInitValues.iteritems():
        self.SetFieldByName(initField, initValue)
      self.total_facility_limit = self.base_price
      self.loan_amount = self.base_price
      config.FlushUpdates()
    #--       
      
    def ProcessUIData(self, recData):
      pass
      
    def OnDelete(self):
      pass

    #qardh
    def dropping(self, parameters):   # qardh
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      # tgl_angsur_awal: TDateTime
      # jenis_jadwal_angsur: string (D, T, C)                                                        4
      # id_schedule_tmp: integer (optional)
      helper = self.Helper
      config = self.Config
      
      parameters = atutil.GeneralObject(parameters)
      
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate() 
      if int(round(timeutil.stdDate(config, tgl_dropping))) > int(round(timeutil.stdDate(config, parameters.tgl_angsur_awal))):
        raise Exception, "Tanggal angsur awal harus >= tanggal dropping"
      
      ## CREATE PAYMENT SCHEDULE
      tempSchedule = self.LTmpDroppingSchedule
      if tempSchedule.IsNull:
        raise Exception, "Jadwal angsur manual belum dibuat"

      tempSchedule.genericInitSimulation(self, tgl_dropping, parameters.tgl_angsur_awal, 0)
      tempSchedule.initial_principal_amt = parameters.amount_dropping

      if int(round(timeutil.stdDate(config, parameters.tgl_angsur_awal))) != int(round(timeutil.stdDate(config, tempSchedule.first_install_date))):
        raise Exception, "Tanggal jadwal angsur manual berbeda dengan tanggal angsur awal"
      if self.is_external == 'F':
        if int(round(timeutil.stdDate(config, tgl_dropping))) != int(round(timeutil.stdDate(config, tempSchedule.dropping_date))) :
          raise Exception, "Tanggal jadwal angsur manual berbeda dengan tanggal angsur awal"

      #total_margin = tempSchedule.initial_profit_amt
      oDropDetailSched = tempSchedule.createDroppingEntry()
      self.LTmpDroppingSchedule = None
      self.LActiveSchedule = tempSchedule
      tempSchedule.setAsActive()
      oSchedule = tempSchedule
    #--

      #self.predicted_margin_amount = 0.0
      self.due_date = self.LActiveSchedule.getLastPaymentDate()
      
      norek_tujuan = parameters.getAttrByName('LDisbursalDest.Nomor_Rekening')         
      lAccDroppingDest = helper.GetObject('core.RekeningLiabilitas', norek_tujuan)
      if lAccDroppingDest.IsNull :
        raise Exception, "rekening tujuan dropping tidak ada"

      # CREATE TRANSACTION
      if not parameters.hasAttribute('kode_entri'):
        kode_entri = 'FTD03' 
      else:
        kode_entri = parameters.kode_entri

      if not parameters.hasAttribute('keterangan'):
        keterangan = 'DROPPING %s' % self.nomor_rekening
      else:
        keterangan = parameters.keterangan

      oTx = FinAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      
      entries = [
          {'sign': 'D', 'amount': parameters.amount_dropping, 'kode_tx_class': '14001'}
        ]
         
      oDetTxGroupDropping = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)
      oDetTxGroupDropping.ProcessDetails()
      
      oDropDetailSched.realize(oDetTxGroupDropping)
        
      oDT = lAccDroppingDest.BuatTransaksiManual('core.DetilTransaksi', 'C', parameters.amount_dropping);
      oDT.LTransaksi = oTx
      oDT.keterangan = oTx.keterangan
      oDT.tanggal_transaksi = oTx.tanggal_transaksi
      oDT.kode_jurnal = '11'
      oDT.Proses()  
      #--      
     
      if parameters.hasAttribute('LDevelover.Nomor_Rekening'):
        FinAccount.TrfToDevelover(self, parameters, oTx, parameters.amount_dropping)

      FinAccount.dropping(self, parameters)

      self.dropping_date = tgl_dropping
      
      oDetTxGroupDropping.ProcessDetails()
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      pass
    #--
    
    def disburse(self, parameters): #disburse-qrd 
      classname = self.CastToLowestDescendant().ClassName.lower()
      if classname == 'finqardhaccount' :
        kode_entri = 'FTDB03'
      else : raise Exception, "qardh based account - disburse must be customized for %s" % classname
      return FinAccount.disburse(self, kode_entri, parameters)    
    #--disburse-qrd

    # info denda qardh
    def getFineInfo(self, tglCheck) :
      config = self.Config
      deltaDays = timeutil.stdDate(config, tglCheck) - timeutil.stdDate(config, self.qardh_due_date)  
      fine_amount = 0 if (deltaDays < 1) else (self.daily_fine_amount or 0.0) * deltaDays
      return {'fine_days' : deltaDays, 'total_fine_amount': fine_amount}
      
#-- QARDH    
        
class FinAgent(pobject.PObject):
    # Static variables
    pobject_classname = 'FinAgent'
    pobject_keys      = ['FinAgentId']

    attrlist = [ 'agentname','agentaddress*',
          'rekescrow=LRekEscrow.nomor_rekening','rekdropping=LRekDropping.nomor_rekening',
          'agentplafond','agentusedplafond',
        ]
    def OnCreate(self, param):
      #self.                          
      config = self.Config
      helper = self.Helper      
      recSrc = param['FinAgent']
      # set other fields w/ non simple assignment
      self.attrlist.append('FinAgentId')
      atutil.transferAttributes(helper, 
        self.attrlist, self, recSrc
      )
      # create dummy account to maintain facility limit and usage
      self.config.FlushUpdates()

    def Edit(self, param):
      #self.                          
      config = self.Config
      helper = self.Helper      
      recSrc = param['FinAgent']
      # set other fields w/ non simple assignment
      atutil.transferAttributes(helper, 
        self.attrlist, self, recSrc
      )

      # create dummy account to maintain facility limit and usage
      self.config.FlushUpdates()


#--

class DroppTiering(pobject.PObject):
    # Static variables
    pobject_classname = 'DroppTiering'
    pobject_keys      = ['DroppTieringId']

    attrlist = [ 'seqTiering*','TieringDate*','droppAmount*','DroppPercentage*'
        ]
    def OnCreate(self, recSrc):
      #self.                          
      config = self.Config
      helper = self.Helper      
      #recSrc = param['DroppTiering']
      # set other fields w/ non simple assignment
      self.attrlist.append('DroppTieringId')
      atutil.transferAttributes(helper, 
        self.attrlist, self, recSrc
      )
      # create dummy account to maintain facility limit and usage
      self.config.FlushUpdates()

    def Edit(self, recSrc):
      #self.                          
      config = self.Config
      helper = self.Helper      
      #recSrc = param['DroppTiering']
      # set other fields w/ non simple assignment
      atutil.transferAttributes(helper, 
        self.attrlist, self, recSrc
      )

      # create dummy account to maintain facility limit and usage
      self.config.FlushUpdates()


#--

class FinIstishnaAccount(FinMurabahahAccount):
    # Static variables
    pobject_classname = 'FinIstishnaAccount'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '18'
    
    financing_model = 'T'
    finmurabahah_type = 'H'

    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      FinAccount.OnCreate(self, param)
      self.kode_jenis = 'ISH'
      recSrc = param['FinIstishnaAccount']
      # set fields with simple / direct 1-on-1 assignment from record with same field name
      atutil.transferAttributes(helper, 
        [ 'finagentid=*LFinAgent.FinAgentId','FeeAmount','base_price', 
          'pricing_model','urbun' ],
        self, recSrc
      )
      dictInitValues = {
        #'automatic_payment': 'T',
        'discount_balance' : 0.0, 
        'initial_base_balance' : 0.0, 
        'initial_profit_balance' : 0.0, 
        'MMD_balance' : 0.0, 
        'profit_accrue_balance': 0.0, 
        'profit_balance': 0.0, 
        'profit_pending_balance': 0.0, 
        'write_off_balance': 0.0, 
        'stored_balance': 0.0
      }
      for initField, initValue in dictInitValues.iteritems():
        self.SetFieldByName(initField, initValue)
      self.total_facility_limit = self.base_price - self.urbun
      self.loan_amount = self.base_price - self.urbun

      self.CreateTermin(param['DroppTiering'])          

      config.FlushUpdates()
    #--       
      
    def CreateTermin(self,recDet):
      for i in range(0, recDet.RecordCount):
        recDetil = recDet.GetRecord(i)
        if recDetil.DroppTieringId in (None,0):                                           
          oDetail = self.Helper.CreatePObject('DroppTiering', recDetil)
          oDetail.DroppStatus ='F'
          oDetail.droppAmount = (self.base_price - self.urbun)*(oDetail.DroppPercentage/100)
          oDetail.FinAgentId = self.FinAgentId
          oDetail.nomor_rekening = self.nomor_rekening
        else:                                           
          oDetail = self.Helper.GetObject('DroppTiering',recDetil.DroppTieringId)
          oDetail.Edit(recDetil)
                  
    def pctRealizedTermin(self):
      ls_tiering = self.Ls_DroppTiering
      ls_tiering.First()
      _pct=0
      while not ls_tiering.EndOfList:
        oTiering = ls_tiering.CurrentElement
        if oTiering.DroppStatus=='T':
          _pct += oTiering.DroppPercentage
        ls_tiering.Next()
        
      return _pct
            
    def amountRealizedTermin(self):
      ls_tiering = self.Ls_DroppTiering
      ls_tiering.First()
      _pokok=0;_margin=0
      while not ls_tiering.EndOfList:
        oTiering = ls_tiering.CurrentElement
        if oTiering.DroppStatus=='T':
          _pokok += oTiering.DroppAmount
          _margin += oTiering.MIDAmount

        ls_tiering.Next()
        
      return _pokok, _margin
            
    def getNextTermin(self):
      config = self.Config
      mlu = config.ModLibUtils
      sSQL='''
        SELECT DroppTieringId FROM %(DROPPTIERING)s WHERE NOMOR_REKENING =%(nomor_rekening)s AND DROPPSTATUS='F' AND rownum=1 ORDER BY SEQTIERING
      '''% { 'DROPPTIERING' : config.MapDBTableName('DROPPTIERING'), 
             'nomor_rekening' :mlu.QuotedStr(self.nomor_rekening)
          } 
      q = config.CreateSQL(sSQL).rawresult
      dtid = 0
      if not q.Eof:
        dtid = q.DroppTieringId      
        
      return dtid
            
    def nextDropTermin(self, params):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      kode_entri='FTDT07'; keterangan='Dropping Termin Istishna ke %i' % params.seqTermin      
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_trx = periodHelper.GetAccountingDate()
      
      oTx = FinAccount.initTransaksi(self, kode_entri, tgl_trx, keterangan)
      
      entries = self.DroppingTermin(params.harga_beli, params.initial_profit_balance)
            
      oDetTxGroupCost = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupCost.ProcessDetails()
      #--      
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      
      return oDetTxGroupCost 
    
    def getTerminProporsional(self, termin_amount):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      sql = """
        SELECT  
        	SEQ_NUMBER, principal_amount, profit_amount
        FROM 
        	%(finpaymentscheddetail)s psd
        WHERE 
        	psd.ID_SCHEDULE=%(id_schedule)s AND SEQ_NUMBER>0 
        ORDER BY SEQ_NUMBER asc
      """ % { 'finpaymentscheddetail' : config.MapDBTableName('finpaymentscheddetail'), 
              'id_schedule' :self.id_schedule} 
      q = config.CreateSQL(sql).rawresult
      q.First()
      ePokok,eMargin = self.amountRealizedTermin()
      max_piutang = termin_amount
      akum_angs = 0;akum_pokok = 0;akum_margin = 0
      while not q.Eof:
        akum_angs += q.principal_amount + q.profit_amount
        if max_piutang>=akum_angs:
          akum_pokok += q.principal_amount
          akum_margin += q.profit_amount
        
        q.Next()
        
      return  akum_pokok-ePokok, akum_margin-eMargin
    
    def DroppingTermin(self, harga_beli, total_margin):
      config = self.Config
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()

      DroppTieringId = self.getNextTermin()
      
      entries=[]
      if DroppTieringId <> 0:
        oTermin = self.Helper.GetObject('DroppTiering',DroppTieringId)
        _pctRT = self.pctRealizedTermin()
        _realPct = _pctRT+oTermin.DroppPercentage
        pctTermin = _realPct/100
        _pokok = harga_beli * pctTermin
        _margin = total_margin * pctTermin
        
        DroppAmount,MIDAmount = self.getTerminProporsional(_pokok+_margin)
        #raise Exception, "%s - %s" % (DroppAmount,MIDAmount)
        entries = [
          #pembentukan Asset istishna dlm penyelesaian
          {'sign': 'D', 'amount': harga_beli*(oTermin.DroppPercentage/100), 'kode_tx_class': '%s071' % self.base_kode_tx_class}, 
          {'sign': 'C', 'amount': harga_beli*(oTermin.DroppPercentage/100), 'kode_tx_class': '%s011' % self.base_kode_tx_class},
          #pembentukan piutang Istisahna 
          {'sign': 'D', 'amount': DroppAmount+MIDAmount, 'kode_tx_class': '%s001' % self.base_kode_tx_class}, 
          {'sign': 'C', 'amount': DroppAmount, 'kode_tx_class': '%s072' % self.base_kode_tx_class}, 
          {'sign': 'C', 'amount': MIDAmount, 'kode_tx_class': '%s002' % self.base_kode_tx_class}
        ]
        if self.stored_balance > 0:
          total_dropping_ish = DroppAmount+MIDAmount
          sql = """
            SELECT  
            	SEQ_NUMBER,PRINCIPAL_AMOUNT,REALIZED_MARGIN,REALIZED_MUKASAH,(PRINCIPAL_AMOUNT+REALIZED_MARGIN) total_payment,
            	%(stored_balance)s-sum((PRINCIPAL_AMOUNT+REALIZED_MARGIN-REALIZED_MUKASAH)) OVER (PARTITION BY ID_SCHEDULE ORDER BY PSD.SEQ_NUMBER DESC) as sisa_tampungan
            FROM 
            	%(finpaymentscheddetail)s psd
            WHERE 
            	psd.ID_SCHEDULE=%(id_schedule)s AND
            	REALIZED='T' AND SEQ_NUMBER>0 
            ORDER BY SEQ_NUMBER DESC
          """ % { 'stored_balance':self.stored_balance, 
                  'finpaymentscheddetail' : config.MapDBTableName('finpaymentscheddetail'), 
                  'id_schedule' :self.id_schedule} 
          q = config.CreateSQL(sql).rawresult
          q.First()
          s_pokok=0;s_mukasah=0;s_margin=0
          while not q.Eof:
            total_dropping_ish -= q.total_payment
            if q.sisa_tampungan >=0 and total_dropping_ish >=0:
              s_pokok   += q.principal_amount
              s_margin  += q.realized_margin
              s_mukasah += q.realized_mukasah
            q.Next()
                      
          entries += [
            #cr piutang dari tampungan 
            {'sign': 'D', 'amount': s_pokok+s_margin-s_mukasah, 'kode_tx_class': '%s073' % self.base_kode_tx_class}, 
            {'sign': 'D', 'amount': s_mukasah, 'kode_tx_class': '%s005' % self.base_kode_tx_class}, 
            {'sign': 'C', 'amount': s_pokok+s_margin, 'kode_tx_class': '%s001' % self.base_kode_tx_class},

            #pengakuan pendapatan dari tampungan 
            {'sign': 'D', 'amount': s_margin, 'kode_tx_class': '%s002' % self.base_kode_tx_class}, 
            {'sign': 'C', 'amount': s_margin, 'kode_tx_class': '%s003' % self.base_kode_tx_class}
          ]
        
        if _realPct ==100:
          entries += [
              #jurnal balik termin 100%
              {'sign': 'D', 'amount': harga_beli, 'kode_tx_class': '%s072' % self.base_kode_tx_class}, 
              {'sign': 'C', 'amount': harga_beli, 'kode_tx_class': '%s071' % self.base_kode_tx_class},
          ]
          

        oTermin.DroppStatus = 'T'
        oTermin.TieringDate = tgl_dropping
        oTermin.DroppAmount = DroppAmount
        oTermin.MIDAmount   = MIDAmount 
        
        self.TrfEscrowToOpr(harga_beli * (oTermin.DroppPercentage/100))
        
      return entries
      

    def TrfEscrowToOpr(self, drop_amount):
      helper = self.Helper
      config = self.Config
      
      periodHelper = helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()
      keterangan = 'Dropping Termin Istishna %s' % self.nomor_rekening
      oTx = FinAccount.initTransaksi(self, 'FTDT07', tgl_dropping, keterangan)

      oFinAgent = self.LFinAgent
      norek = [
        [oFinAgent.RekEscrow, 'D'], [oFinAgent.RekDropping, 'C']
      ]
      
      for i in norek:
        norek_tujuan=i[0]
        mnemonic=i[1]
        
        ldestacc = helper.GetObject('core.RekeningTransaksi', norek_tujuan)
        oDT = ldestacc.BuatTransaksiManual('core.DetilTransaksi', mnemonic, drop_amount)       
        oDT.LTransaksi = oTx
        oDT.keterangan = oTx.keterangan
        oDT.tanggal_transaksi = oTx.tanggal_transaksi
        oDT.kode_jurnal = '11'
        oDT.Proses()
    #-- disburse-finacc

      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      

    def ProcessUIData(self, recData):
      pass
      
    def OnDelete(self):
      pass
      
    def getPrincipalOutstanding(self): # istishna
      os_pokok = -(self.saldo + self.arrear_balance + self.mmd_balance + self.profit_arrear_balance)
      
      return os_pokok + self.PrincipalUnrealizedTermin()
    #--
    def getMarginOutstanding(self): # istishna
      os_margin = self.mmd_balance + self.profit_arrear_balance
      return os_margin + self.MarginUnrealizedTermin()
    #--

    # istishna
    def PrincipalUnrealizedTermin(self):
      sSQL = """ 
        select sum(DroppAmount) as pokok from %s b 
        where b.droppstatus='T' and b.nomor_rekening='%s'   
      """ % (self.config.MapDBTableName('dropptiering'),self.nomor_rekening) 
      row = self.config.CreateSQL(sSQL).RawResult      
      
      return self.loan_amount - row.pokok
    #--

    # istishna
    def MarginUnrealizedTermin(self):
      sSQL = """ 
        select sum(MIDAmount) margin from %s b 
        where b.droppstatus='T' and b.nomor_rekening='%s'   
      """ % (self.config.MapDBTableName('dropptiering'),self.nomor_rekening) 
      row = self.config.CreateSQL(sSQL).RawResult
      
      return -(self.initial_profit_balance + row.margin)
      
    def getUnrealizedTermin(self):  
      sSQL = """ 
        select sum(dropppercentage) t_pct from %s b 
        where b.droppstatus='F' and b.nomor_rekening='%s'   
      """ % (self.config.MapDBTableName('dropptiering'),self.nomor_rekening) 
      row = self.config.CreateSQL(sSQL).RawResult      
      return row.t_pct       

    def getLoanAmount(self):
      return self.base_price - self.urbun
      

#-- istishna    
        
class FinMusyarakahAccount(FinAccount):
    # Static variables
    pobject_classname = 'FinMusyarakahAccount'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '12'
    
    #kode_jenis = 'MSY'
    
    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      FinAccount.OnCreate(self, param)
      self.kode_jenis = 'MSY'
      self.financing_model = 'T'
      recSrc = param['FinMusyarakahAccount']
      # set fields with simple / direct 1-on-1 assignment from record with same field name
      dictInitValues = {
        'targeted_income' : 'F',
        'income_target' : 0.0,
        'profit_share_model' : 'P',
        'review_cycle': 12,
        'self_capital': 0.0,
        'total_projected_income': 0.0 ,
        'repayment_margin_mo': 1
      }
      for initField, initValue in dictInitValues.iteritems():
        self.SetFieldByName(initField, initValue)
      oGen = atutil.GeneralObject(recSrc)
      atutil.transferAttributes(helper, 
        [ 
          'total_capital', 
          'self_capital*', 
          'profit_share_model*',
          'profit_share',
          'review_cycle*',
          'due_date*',
          'next_repricing_date*',
          'repricing_period*',
          'spread_rate*',
          'FinAgentId=*LFinAgent.FinAgentId'
        ],
        self, recSrc
      )
      self.loan_amount = self.total_capital - self.self_capital 
      config.FlushUpdates()
    #--
    
    def getPricingModel(self): # virtual method
      return '1'

    def getLoanAmount(self):
      return self.total_capital - self.self_capital
      
    # musy
    def getMarginOutstanding(self):
      return (self.total_projected_income or 0.0) - self.profit_balance
    
    def getNextPaymentAmount(self):
      return self.income_target
    
    #musyarakah       
    def dropping(self, parameters): # musyarakah 
      # parameters is either dictionary or record with the following fields:
      # keterangan: string
      # tgl_angsur_awal: TDateTime
      # id_schedule_tmp: integer (mandatory)
      helper = self.Helper
      config = self.Config
      
      norek_tujuan = parameters.GetFieldByName('LDisbursalDest.Nomor_Rekening')         
      lAccDroppingDest = helper.GetObject('core.RekeningLiabilitas', norek_tujuan)
      if lAccDroppingDest.IsNull :
        raise Exception, "rekening tujuan dropping tidak ada"
      
      parameters = atutil.GeneralObject(parameters)
      
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate() 
      if int(round(timeutil.stdDate(config, tgl_dropping))) > int(round(timeutil.stdDate(config, parameters.tgl_angsur_awal))):
        raise Exception, "Tanggal angsur awal harus >= tanggal dropping"
      
      ## CREATE PAYMENT SCHEDULE
      tempSchedule = self.LTmpDroppingSchedule
      if tempSchedule.IsNull:
        raise Exception, "Jadwal angsur manual belum dibuat"

      tempSchedule.genericInitSimulation(self, tgl_dropping, parameters.tgl_angsur_awal, 0)
      tempSchedule.initial_principal_amt = parameters.amount_dropping

      if int(round(timeutil.stdDate(config, parameters.tgl_angsur_awal))) != int(round(timeutil.stdDate(config, tempSchedule.first_install_date))):
        raise Exception, "Tanggal jadwal angsur manual berbeda dengan tanggal angsur awal"
      if int(round(timeutil.stdDate(config, tgl_dropping))) != int(round(timeutil.stdDate(config, tempSchedule.dropping_date))):
        raise Exception, "Tanggal jadwal angsur manual berbeda dengan tanggal angsur awal"
      total_margin = tempSchedule.initial_profit_amt
      self.total_projected_income = total_margin
      
      oDropDetailSched = tempSchedule.createDroppingEntry()
      self.LTmpDroppingSchedule = None
      self.LActiveSchedule = tempSchedule
      tempSchedule.setAsActive()
      oSchedule = tempSchedule

      if self.due_date in [None, 0.0]:
        self.due_date = self.LActiveSchedule.getLastPaymentDate()
      lpaymentacc = self.LPaymentSrc
      if lpaymentacc.IsNull:
        raise Exception, "Payment account not defined"

      # CREATE TRANSACTION
      oTx = FinAccount.initTransaksi(self, parameters.kode_entri, tgl_dropping, parameters.keterangan)
            
      ljournal = [
          {'sign': 'D', 'amount': parameters.amount_dropping, 'kode_tx_class': '%s001' % self.base_kode_tx_class} 
      ]
      oDetTxGroupDropping = self.CreateTransactionGroup(parameters.kode_entri, parameters.keterangan, oTx, ljournal)
      oDetTxGroupDropping.ProcessDetails()

      oDropDetailSched.realize(oDetTxGroupDropping)
                    
      oDT = lAccDroppingDest.BuatTransaksiManual('core.DetilTransaksi', 'C', parameters.amount_dropping); # dropping musyarakah
      oDT.LTransaksi = oTx
      oDT.keterangan = oTx.keterangan
      oDT.tanggal_transaksi = oTx.tanggal_transaksi
      oDT.kode_jurnal = '11'
      oDT.Proses()
      
      if self.ClassName == 'FinMMQAccount':
        sum_mukasah = self.LActiveSchedule.getMukasahAmount()
        if self.FinAgentId not in [None, 0]:
          # db rek nasabah, cr rek. opr developer
          dropp_amount = parameters.amount_dropping-sum_mukasah
          oDT = lAccDroppingDest.BuatTransaksiManual('core.DetilTransaksi', 'D', parameters.amount_dropping); # dropping musyarakah
          oDT.LTransaksi = oTx
          oDT.keterangan = oTx.keterangan+' PB TO DEVELOPER'
          oDT.tanggal_transaksi = oTx.tanggal_transaksi
          oDT.kode_jurnal = '11'
          oDT.Proses()
          
          dtid = self.getNextTermin()
          drop_pct = 100
          if dtid<>0:
            oTahapan = helper.GetObject('DroppTiering',dtid)
            drop_pct = oTahapan.DroppPercentage
            oTahapan.DroppStatus = 'T'
            oTahapan.TieringDate = oTx.tanggal_transaksi
            ket_cair_opr = 'TAHAP 1 %s %% %s' % (drop_pct, oTx.keterangan)
            
          opr_amount = dropp_amount * (drop_pct/100)
          tampungan = dropp_amount - opr_amount
  
          LDevOpr = self.LFinAgent.LRekDropping
          LDevEscrow = self.LFinAgent.LRekEscrow
  
          #cr ke oprasional untuk pencairan tahap 1 jika bertahap / 100%
          oDT = LDevOpr.BuatTransaksiManual('core.DetilTransaksi', 'C', opr_amount); # dropping musyarakah
          oDT.LTransaksi = oTx
          oDT.keterangan = ket_cair_opr
          oDT.tanggal_transaksi = oTx.tanggal_transaksi
          oDT.kode_jurnal = '11'
          oDT.Proses()
          
          #sisa pencairan berikutnya ditampung direk escrow 
          if tampungan> 0.1:
            oDT = LDevEscrow.BuatTransaksiManual('core.DetilTransaksi', 'C', tampungan); # dropping musyarakah
            oDT.LTransaksi = oTx
            oDT.keterangan = 'TAMPUNGAN '+oTx.keterangan
            oDT.tanggal_transaksi = oTx.tanggal_transaksi
            oDT.kode_jurnal = '11'
            oDT.Proses()
          
          # cr subsidi ke rek escrow Developer
          if sum_mukasah>0.1:
            oDT = LDevEscrow.BuatTransaksiManual('core.DetilTransaksi', 'C', sum_mukasah); # dropping musyarakah
            oDT.LTransaksi = oTx
            oDT.keterangan = 'SUBSIDI '+oTx.keterangan
            oDT.tanggal_transaksi = oTx.tanggal_transaksi
            oDT.kode_jurnal = '11'
            oDT.Proses() 
        #--
      else:
        if parameters.hasAttribute('LDevelover.Nomor_Rekening'):
          FinAccount.TrfToDevelover(self, parameters, oTx, parameters.amount_dropping)

        self.pricing_model = parameters.pricing_model
        self.is_balloon_payment = parameters.is_balloon_payment
            
      FinAccount.dropping(self, parameters)

      self.dropping_date = tgl_dropping
      self.predicted_margin_amount = total_margin
      
      #raise Exception, self.is_balloon_payment
            
      oDetTxGroupDropping.ProcessDetails()
      #oTx.CreateJournal()
      self.Helper.CreatePObject('PendingTransactionJournal', oTx)
      pass
    #--
    
    def disburse(self, parameters): #disburse-msy
      classname = self.CastToLowestDescendant().ClassName.lower()
      if classname == 'finmusyarakahaccount' :
        kode_entri = 'FTDB02'
      else : raise Exception, "musyarakah based account - disburse must be customized for %s" % classname
      FinAccount.disburse(self, kode_entri, parameters)    
    #--disburse-msy
    
    def customizedRescheduleDetails(self): #msy musyarakah
      # do nothing
      return []
    
    def ProcessUIData(self, recData):
      pass
      
    def OnDelete(self):
      pass
      
    def getAcceleratedRepaymentInfo(self):
      sisa_pokok  = -1 * self.saldo 
      sisa_margin = 0 ; 
      sisa_margin_hrs_dibayar = -self.profit_arrear_balance
      oSched = self.LActiveSchedule
      currentSeq = oSched.getCurrentPaymentSeq()  
      _tazir,_tawidh = oSched.getPenaltyAmount()
      info = {
        'sisa_pokok'    : sisa_pokok 
        , 'sisa_margin' : sisa_margin 
        , 'bln_margin'  : 0
        , 'sisa_margin_hrs_dibayar' : sisa_margin_hrs_dibayar
        , 'total_bayar' : sisa_pokok + sisa_margin_hrs_dibayar 
        , 'mukasah' : 0
        , 'penalty_tazir' : _tazir
        , 'penalty_tawidh' : _tawidh 
      }
      
      return info
      
    def getWriteOffTotalPrincipalOustanding(self):
      return self.saldo + self.arrear_balance
      
    # menentukan level kolektibilitas
    def analyzeColLevel_new(self, param):
      #raise Exception, 'XXX'
      modC = modman.getModule(self.Config, "RuleCollectibility")
      rule = modC.RuleFacade()
      rule.setup(self, param)
      repayment_col_level = rule.calcRepaymentColl()
      
      lcol = []
      if self.col_model == 'C' :
        lcol = [repayment_col_level, param.prospect_col_level, param.performance_col_level, param.manual_col_level]
      else : # 'R'
        lcol = [repayment_col_level, param.manual_col_level]
      tmp_col_level = max(lcol) # kolektibilitas sementara, mempertimbangkan manual col, belum melibatkan so col level  
      overall_col_level = max([ tmp_col_level, param.so_col_level ]) # kolektibilitas akhir, melibatkan so col level      
      return repayment_col_level, tmp_col_level, overall_col_level 
      
    #--
    
    #musy
    def getTrxJournal(self, param, jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = FinAccountUtils(config)
      if self.ClassName == 'FinMudharabahAccount':
        kode_jenis = 'MDB'
      elif self.ClassName == 'FinMusyarakahAccount':
        kode_jenis = 'MSY'
      if self.ClassName == 'FinMMQAccount':
        kode_jenis = 'MMQ'
      if self.ClassName == 'FinPRKAccount':
        kode_jenis = 'PRK'
      
      lgr = []

      if jns_jurnal in ['payment','pelunasan_dipercepat']: # Manual Payment
        payment = copy.copy(param.payment) or 0
        bayarPokok  = param.principal_amount or 0 
        bayarMargin = param.profit_amount or 0
        #acrue_balance = self.margin_accrued_balance or 0
        tunggakanMargin = -self.profit_arrear_balance or 0
        if self.arrear_balance < 0 :
          # ada tunggakan: selesaikan tunggakan terlebih dahulu
          tunggakan = -self.arrear_balance
          bayarTunggakan = copy.copy(bayarPokok)
          sisaBayarPokok = 0       
          if (tunggakan < bayarTunggakan) :
            bayarTunggakan = tunggakan
            sisaBayarPokok = bayarPokok - bayarTunggakan         
      
          lgr += [
            {'sign': 'D', 'amount': bayarTunggakan, 'kode_tx_class': fau.get_tx(kode_jenis,'pembayaran') },
            {'sign': 'C', 'amount': bayarTunggakan, 'kode_tx_class': fau.get_tx(kode_jenis,'tunggakan') }
          ]
          if sisaBayarPokok > 0 : # jika masih ada kelebihan, bayarkan pokok
            lgr += [
              { 'sign': 'D', 'amount': sisaBayarPokok, 'kode_tx_class': fau.get_tx(kode_jenis,'pembayaran') }
              ,{'sign': 'C', 'amount': sisaBayarPokok, 'kode_tx_class': fau.get_tx(kode_jenis,'outstanding') }
            ]
            
        else :
          # tidak ada tunggakan:piutang murabahah vs payment
          lgr += [
            {'sign': 'D', 'amount': bayarPokok, 'kode_tx_class': fau.get_tx(kode_jenis,'pembayaran') },
            {'sign': 'C', 'amount': bayarPokok, 'kode_tx_class': fau.get_tx(kode_jenis,'outstanding') }
          ]
          #lra.append( {'sign': 'D', 'amount': payment} )
        
        # bayar margin
        sisa_margin = bayarMargin 
        if tunggakanMargin>0:
          bt_margin = tunggakanMargin if tunggakanMargin < bayarMargin else bayarMargin
          sisa_margin = bayarMargin - bt_margin 
          lgr += [
            { 'sign': 'D', 'amount': bt_margin, 'kode_tx_class': fau.get_tx(kode_jenis,'pembayaran') }
            ,{'sign': 'C', 'amount': bt_margin, 'kode_tx_class': fau.get_tx(kode_jenis,'tunggakan_margin') }
            # balik pendapatan yadit vs pendapatan
            ,{ 'sign': 'D', 'amount': bt_margin, 'kode_tx_class': fau.get_tx(kode_jenis,'pdpt_yadit') }
            ,{'sign': 'C', 'amount': bt_margin, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan') }
          ]
          
        #raise Exception, 'tm:%s - bm:%s - sm:%s' % (tunggakanMargin, bayarMargin, sisa_margin)
        if sisa_margin> 0:
          lgr += [
            { 'sign': 'D', 'amount': sisa_margin, 'kode_tx_class': fau.get_tx(kode_jenis,'pembayaran') }
            ,{'sign': 'C', 'amount': sisa_margin, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan') }
          ]
        
        if jns_jurnal =='pelunasan_dipercepat':
          lgr += FinAccount.getTrxJournal(self,param,'reverse_PPAP')                                                                          
      
      elif jns_jurnal in ['realisasi_gp']: # realisasi gross profit
        realisasiMargin  = param.profit_amount 
        lgr += [
          { 'sign': 'D', 'amount': realisasiMargin, 'kode_tx_class': fau.get_tx(kode_jenis,'tunggakan_margin') }
          ,{'sign': 'C', 'amount': realisasiMargin, 'kode_tx_class': fau.get_tx(kode_jenis,'pdpt_yadit') } #akan diubah nanti
          #,{'sign': 'C', 'amount': realisasiMargin, 'kode_tx_class': fau.get_tx(kode_jenis,'pendapatan') }
        ]
      
      elif jns_jurnal =='input_AYDA': # transaksi input AYDA
        os_pokok = self.getPrincipalOutstanding()
        nilai_jaminan = param.AYDA_balance
        nilai_AYDA = nilai_jaminan if nilai_jaminan <= os_pokok else os_pokok
        biaya_AYDA = (os_pokok - nilai_jaminan) if (nilai_jaminan < os_pokok) else 0
        
        #journal input ayda
        lgr += [
        #create ayda balance
          {'sign': 'D', 'amount': os_pokok, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_aktiva' ) },
          #{'sign': 'D', 'amount': biaya_AYDA, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_biaya' ) },
        #clear loan balance
          {'sign': 'C', 'amount': -self.saldo, 'kode_tx_class':  fau.get_tx(kode_jenis,'outstanding') },
          {'sign': 'C', 'amount': -self.arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan' ) },
        #Create PPANP lancar 1% by yayan 09dec14
          {'sign': 'D', 'amount': os_pokok*0.01, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_by_cadangan' ) },
          {'sign': 'C', 'amount': os_pokok*0.01, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_cadangan' ) }
        ]
        lgr += FinAccount.getTrxJournal(self,param,'reverse_PPAP')                                                                          
        
      elif jns_jurnal =='hapus_buku': # transaksi Write Off Hapus Buku
        os_pokok = self.getPrincipalOutstanding()                     
        os_margin = self.getMarginOutstanding()
        total_ppap  = self.reserved_common_balance+self.reserved_loss_balance+self.reserved_manual_balance
        bentuk_reverse_ppap = os_pokok-total_ppap
        
        #journal input ayda
        lgr += [
        # rerevse PPAP
          {'sign': 'D', 'amount': self.reserved_common_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum' ) },
          {'sign': 'D', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus' ) },
          {'sign': 'D', 'amount': self.reserved_manual_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_adjustment' ) },
        # bentuk tambahan by ppap
          #{'sign': 'C', 'amount': bentuk_reverse_ppap, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus' ) },
          {'sign': 'D', 'amount': bentuk_reverse_ppap, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus' ) },
        #clear loan balance
          {'sign': 'C', 'amount': -self.saldo, 'kode_tx_class':  fau.get_tx(kode_jenis,'outstanding') },
          {'sign': 'C', 'amount': -self.arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan' ) },
        # Create Write Off  
          {'sign': 'D', 'amount': os_pokok, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off' ) },
          {'sign': 'D', 'amount': os_margin, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off_margin' ) }
        ]
      else:
        lgr = FinAccount.getTrxJournal(self,param,jns_jurnal)

      #raise Exception, lgr
      return lgr

    def PrintBaseTransaction(self):
        config = self.Config
        
        corporate = self.Helper.CreateObject('Corporate')
        info = corporate.GetCabangInfo(self.kode_cabang)
        branchName = info.Nama_Cabang
        
        fha = self.profit_share_model
        if fha == 'P': fhaket = 'Profit Sharing'
        elif fha == 'R': fhaket = 'Revenue Sharing'
        
        #os_pokok = -(self.saldo+self.MMD_balance-self.profit_accrue_balance)
        #os_margin = self.MMD_balance-self.profit_accrue_balance
        
        #tunggakan_pokok = -(self.arrear_balance + self.profit_pending_balance)
        #tunggakan_margin = self.profit_pending_balance
        
        reportsource = '%s\\%s' % (corporate.GetUserHomeDir(), 'rpt_base_transaction.txt')
        rpt = report.textreport(reportsource)
        try:
            rpt.p_column({50: 'DATA DAN TRANSAKSI PEMBIAYAAN MURABAHAH'})
            bank_name = self.Config.SysVarIntf.GetStringSysVar('COMPANYINFO', 'NAME')
            rpt.p_column({58: bank_name})
            rpt.p_line('=' * 125)
            rpt.p_line('Nomor Cabang    : %s - %s' % (self.kode_cabang, branchName))
            rpt.p_column({
                1 : 'Nomor Kartu     : '+self.nomor_rekening,
                42: 'Nama Nasabah : '+self.nama_rekening})
            rpt.p_column({
                1 : 'No. Fasilitas   : '+self.facility_no,
                40: 'Kode Produk PD : '+self.product_code+' - '+self.LFinProduct.product_name})
            rpt.p_column({
                1 : 'Sifat Fas       : ',
                42: 'Jangka Waktu : '+str(self.period_count)+' Bulan'})
            rpt.p_column({
                1 : 'Tanggal Akad    : '+rpt.fm_dmy(self.tgl_akad),
                31: 'Tgl. Realisasi : ',
                60: 'Tgl. J. Tempo : '+rpt.fm_dmy(self.due_date)})
            rpt.p_column({
                1 : 'Harga Perolehan : '+rpt.fm_float(self.income_target),
                45: 'Uang Muka : -'})
            rpt.p_column({
                1 : 'Margin          : '+rpt.fm_float(self.profit_share),
                44: 'Harga Jual : -'})
            rpt.p_column({
                1 : 'Jml Angs./Bulan : -',
                41: 'Tgl Angs Brkt : '})
            rpt.p_column({
                1 : 'Margin Ekv Rate : '+str(self.targeted_eqv_rate),               
                35: 'Flag Pemby Angs : ',
                68: 'Pricing : '+ fhaket})
            rpt.p_column({
                1 : 'Nomor Rek. Angs : '+self.norek_paymentsrc,
                68: 'Kolek   : '+str(self.manual_col_level)+' - '+str(self.manual_col_desc)})
            rpt.p_line('-------------------- POKOK ------------- MARGIN ------------- JUMLAH ----------')
            rpt.p_column({
                1 : 'Outstanding  : ',
                40: '',
                60: ''})
            rpt.p_column({
                1 : 'Tunggakan       : ',
                40: '',
                60: ''})
            rpt.p_column({
                1 : 'Tgl Pemb Akhir  : '+rpt.fm_dmy(self.due_date),
                40: rpt.fm_dmy(self.due_date)})
            rpt.p_header('Halaman : <<PAGE:3>>', {'view_infirstpage': 0, 'formatted': 1})
            rpt.p_header('-' * 125)
            rpt.p_header(rpt.fm_column({
                1  : 'Tgl Trans', 
                14 : 'KdTrx',
                21 : 'No Interface',
                34 : 'Keterangan',
                72 : 'Mutasi',
                92 : 'Jumlah Transaksi',
                112: 'User Input'}))
            rpt.p_header('-' * 125)
            
            
            sql = """select c.tanggal_transaksi, kode_entri, kode_tx_class, a.keterangan, jenis_mutasi,  
                nilai_mutasi, c.user_input  from  %(detiltransaksi)s a, %(detiltransgroup)s b , %(transaksi)s c  
                where a.nomor_rekening = '%(norek)s' 
                and a.id_detiltransgroup = b.id_detiltransgroup  
                and a.id_transaksi = c.id_transaksi  
                order by tanggal_transaksi
            """ % { 'detiltransaksi' : config.MapDBTableName('core.detiltransaksi'), 
              'detiltransgroup': config.MapDBTableName('core.detiltransgroup'),
              'transaksi' : config.MapDBTableName('core.transaksi'), 'norek' : self.nomor_rekening}
                
            q = config.CreateSQL(sql)
            q.active = 1; res = q.rawresult
            
            while not res.Eof:
                rpt.p_column({
                    1  : rpt.fm_dmy(res.tanggal_transaksi),
                    14 : res.kode_entri,
                    21 : res.kode_tx_class,
                    34 : res.keterangan[:35],
                    72 : res.jenis_mutasi,
                    90 : rpt.fm_float(res.nilai_mutasi),
                    112: res.user_input})
                        
                res.Next()
            
            rpt.p_line('')
            rpt.p_line('*** Akhir Record ***')
        finally:
            rpt.close()
        
        return reportsource      
        #-- 
        
    def PrintArrears(self):
        config = self.Config
        
        corporate = self.Helper.CreateObject('Corporate')
        
        info2 = corporate.GetCabangInfo(self.kode_cabang)
        branchName = info2.Nama_Cabang
        
        #nmvalt = kiblatinfo.GetCurrencyName(config, self.mlkmu)
        #branchName = kiblatinfo.BranchInfo(config, self.mlkdc)
        
        reportsource = '%s\\%s' % (corporate.GetUserHomeDir(), 'rpt_arrears.txt')
        #reportsource = config.userhomedirectory + base.REPORT_ARREARS
        rpt = report.textreport(reportsource)
        try:
            rpt.set_format({'pagelength': 0, 'topmargin': 0})
            bank_name = self.Config.SysVarIntf.GetStringSysVar('COMPANYINFO', 'NAME')
            rpt.p_line(bank_name)
            rpt.p_line('No. Cabang : %s - %s' % (self.kode_cabang, branchName))
            rpt.p_line('DAFTAR HISTORI TUNGGAKAN')
            rpt.p_line('')
            rpt.p_line('Nomor Kartu  : ' + self.nomor_rekening)
            rpt.p_line('Nama Nasabah : ' + self.nama_rekening)
            rpt.p_line('Mata Uang    : %s '  % ('-'))
            rpt.p_line('-' * 79)
            rpt.p_column({
                2   : 'Tanggal', 
                17  : 'Angsuran',
                37  : 'Angsuran', 
                58  : 'Jumlah', 
                72  : 'Angsuran'
            })
            rpt.p_column({
                2   : 'Angsuran', 
                18  : 'Pokok', 
                38  : 'Margin',
                57  : 'Angsuran',
                75  : 'ke'
            })
            rpt.p_line('-' * 79)
            
            #q = config.CreateSQL("select   from pbscore.detiltransaksi a, pbscore.detiltransgroup b   \
            #    where a.nomor_rekening = '%s' \
            #     %(self.nomor_rekening))
            #q.active = 1; res = q.rawresult
            
            rpt.p_line('')
            rpt.p_line('*** Akhir Record ***')
        finally:
            rpt.close()
        
        return reportsource
        #--
    
    def PrintCard(self):
        config = self.Config

        oSchedule = self.LActiveSchedule                                                     
        if oSchedule.IsNull:
          raise Exception, "Pembiayaan ini belum memiliki jadwal angsuran"
        
        corporate = self.Helper.CreateObject('Corporate')
        
        info2 = corporate.GetCabangInfo(self.kode_cabang)
        branchName = info2.Nama_Cabang

        reportsource = '%s\\%s' % (corporate.GetUserHomeDir(), 'rpt_card.txt')
        rpt = report.textreport(reportsource)
        try:
            totalamount = self.LActiveSchedule.initial_principal_amt + self.LActiveSchedule.initial_profit_amt 
            rpt.p_column({33: 'KARTU PEMBIAYAAN MURABAHAH'})
            bank_name = self.Config.SysVarIntf.GetStringSysVar('COMPANYINFO', 'NAME')
            rpt.p_column({35: bank_name})
            rpt.p_line('=' * 100)
            rpt.p_column({
                1   : branchName,
                60 : 'Paraf : ',
                70 : 'Maker ',
                80 : 'Checker  Approval',
            })
            rpt.p_line('-' * 100)
            rpt.p_column({3   : 'Nomor ', 15  : 'Nama ', 35  : 'Tanggal ', 48  : 'Angsuran ',
                60 : 'Jatuh ', 78 : 'Jangka ', 90 : 'Debet'})
            rpt.p_column({1   : 'Pembiayaan', 14  : 'Debitur', 33  : 'Diberikan',
                48  : 'Pertama', 60 : 'Tempo', 78 : 'Waktu', 88 : 'Rekening'})
            rpt.p_column({1   : self.nomor_rekening, 14  : self.nama_rekening, 33  : rpt.fm_dmy(self.dropping_date),
                48  : rpt.fm_dmy(self.due_date), 60 : rpt.fm_dmy(self.due_date),
                78 : str(self.period_count), 88 : self.norek_paymentsrc})
            rpt.p_line('\n')
            rpt.p_column({1   : 'Alamat Rumah', 24  : 'Margin Setara', 50  : 'Jumlah Nominal',
                75 : 'Jumlah Margin', 90 : 'Segmentasi'})
            rpt.p_column({1   : '', 24  : rpt.fm_float(self.targeted_eqv_rate, 2, 6) + ' %', 50  : rpt.fm_float(self.LActiveSchedule.initial_principal_amt, 0, 14),
                75 : rpt.fm_float(self.LActiveSchedule.initial_profit_amt, 0, 13), 90 : ''})
            rpt.p_line('\n')
            rpt.p_column({1   : 'Alamat Surat', 24  : 'Account Officer', 50 : 'Nomor Akad', 75 : 'Tujuan Pembiayaan'})
            rpt.p_column({1   : '', 24  : self.LCustomer.alamat_surat_jalan, 50 : '-', 75 : ''})
            rpt.p_line('\n')
            rpt.p_column({1   : 'Tanggal', 20  : 'Jatuh ', 37  : 'Plafond', 60 : 'Nilai',
                75 : 'Margin', 90 : 'Pokok'})
            rpt.p_column({3   : 'Akad', 18  : 'Tempo Akad', 36  : 'Pembiayaan',
                60 : 'Jaminan', 75 : '', 90 : ''})
            rpt.p_column({1  : rpt.fm_dmy(self.tgl_akad), 18  : rpt.fm_dmy(self.due_date),
                33  : rpt.fm_float(self.income_target, 0, 14), 55 : rpt.fm_float(self.collateral_value, 0, 14), 
                70 : rpt.fm_float(self.LActiveSchedule.initial_profit_amt, 0, 14), 85 : rpt.fm_float(self.LActiveSchedule.initial_principal_amt, 0, 14)})
            rpt.p_line('\n')
            rpt.p_column({1   : 'Catatan : ', 34  : 'Kelompok ', 71 : 'Kelompok ',})
            rpt.p_column({1   : '1', 34  : 'Perusahaan ', 71 : 'Debitur'})
            rpt.p_line('')
            rpt.p_header('HALAMAN <<PAGE:3>>', {'view_infirstpage': 0, 'formatted': 1})
            rpt.p_header('-' * 110)
            rpt.p_header(rpt.fm_column({1   : 'Ang', 7  : 'Tanggal', 20  : 'Jumlah', 36  : 'Bagian Pokok', 52  : 'Bagian Margin',
                68  : 'Sisa Pokok', 84  : 'Sisa Margin', 100  : 'Sisa Angs'}))
            rpt.p_column({64  : rpt.fm_float(self.LActiveSchedule.initial_principal_amt, 0, 14), 80  : rpt.fm_float(self.LActiveSchedule.initial_profit_amt, 0, 14),
                96  : rpt.fm_float(totalamount, 0, 14)})
            rpt.p_header('-' * 110)

            q = config.CreateSQL("SELECT \
                    id_scheddetail,\
                    seq_number,\
                    sched_date,\
                    principal_amount, \
                    profit_amount,\
                    realized,\
                    id_detiltransgroup \
                    FROM finpaymentscheddetail sdd \
                    WHERE sdd.id_schedule = '%s' and seq_number <> 0 \
                    ORDER BY id_scheddetail, seq_number" %(self.id_schedule))
            q.active = 1; res = q.rawresult
            
            #~ ON EVERY ROW
            sp = self.LActiveSchedule.initial_principal_amt
            sm = self.LActiveSchedule.initial_profit_amt
            ts = sp + sm
            n = self.period_count
            i=1
            
            while not res.Eof:
                sp = sp - res.principal_amount
                sm = sm - res.profit_amount
                ts = sp + sm
                tp = res.principal_amount + res.profit_amount
                
                rpt.p_column({
                    1   : '%3d' % (i),
                    7   : rpt.fm_dmy(res.sched_date),
                    16  : rpt.fm_float(tp, 0, 14),
                    32  : rpt.fm_float(res.principal_amount, 0, 14),
                    48  : rpt.fm_float(res.profit_amount, 0, 14),
                    64  : rpt.fm_float(sp, 0, 14),
                    80  : rpt.fm_float(sm, 0, 14),
                    96 : rpt.fm_float(ts, 0, 14)
                })
                leftrow = 66 - rpt.get_row()
                if (i < n) and ((leftrow == 3) or ((leftrow == 4) and (i % 12) == 0)):
                    if leftrow == 4:
                        rpt.p_line('')
                    rpt.p_line('=' * 110)
                    rpt.p_line('Bersambung...')
                    rpt.p_line('=' * 110)
                    rpt.p_column({
                        39  : 'USER-ID CETAK : ' ,
                        68  : 'TGL. CETAK : ',
                        93 : 'JAM CETAK : '
                    })
                elif (i % 12) == 0:
                    rpt.p_line('')
                i=i+1
                res.Next()
            #~ end while
                        
            rpt.p_line('=' * 110)
            rpt.p_column({
                1   : 'TOTAL PEMBIAYAAN :',
                19  : rpt.fm_float(totalamount, 0, 16),
                36  : rpt.fm_float(self.LActiveSchedule.initial_principal_amt, 0, 16),
                52  : rpt.fm_float(self.LActiveSchedule.initial_profit_amt, 0, 16)
            })
            rpt.p_line('=' * 110)
            rpt.p_column({
                39  : 'USER-ID CETAK : ',
                68  : 'TGL. CETAK : ',
                93 : 'JAM CETAK : '
            })            
            
            rpt.p_line('*** Akhir Record ***')
        finally:
            rpt.close()
        
        return reportsource
        #--  
#-- musyarakah

class FinMMQAccount(FinMusyarakahAccount):
    # Static variables
    pobject_classname = 'FinMMQAccount'
    #pobject_keys      = ['nomor_rekening']
    
    kode_jenis = 'MMQ'
    financing_model = 'T'
    base_kode_tx_class = '15'

    def OnCreate(self, param):
      #import rpdb2; rpdb2.start_embedded_debugger('000', True, True)
      mmqCalc = FinCalculator.PBSMMQCalculator() #FinCalculator.MMQCalculator() 
      config = self.Config; helper = self.Helper
      #param['FinMusyarakahAccount'] = param['FinMMQAccount']
      #FinMusyarakahAccount.OnCreate(self, param)
      #recSrc = param['FinMMQAccount']
      
      config = self.Config; helper = self.Helper
      recSrc = param['FinMMQAccount']
      param['FinMusyarakahAccount'] = recSrc # copy musyarakah parameter
      FinMusyarakahAccount.OnCreate(self, param) # call parent constructor
      self.kode_jenis = 'MMQ'
      
      oGen = atutil.GeneralObject(recSrc)  
      
      M = self.total_capital
      t = self.period_count
      rate = self.targeted_eqv_rate
      uangMuka = self.self_capital #M - B
      B = self.total_capital - uangMuka #modal bank
       
      A = mmqCalc.CalcCicilanBulanan(t, rate, B)
      #f, P = mmqCalc.GetCicilanPokokDanSewa(M, uangMuka, t, A)
      P = mmqCalc.CalcSewaBulanan(M, uangMuka, rate)
      f = A - P  
      
      self.principal_installment = f  
      self.income_target = P * t ##f + self.profit_share /100 * P # profit share dalam persen ?
      self.targeted_income = 'T'
      self.automatic_payment = 'T'
      self.loan_amount = self.total_capital - self.self_capital 
      
      if self.FinAgentId is not [None, 0]:
        self.TahapanDropping(param['DroppTiering'])          

      config.FlushUpdates()
    #--       
      
    def TahapanDropping(self,recDet):
      for i in range(0, recDet.RecordCount):
        recDetil = recDet.GetRecord(i)
        if recDetil.DroppTieringId in (None,0):                                           
          oDetail = self.Helper.CreatePObject('DroppTiering', recDetil)
          oDetail.DroppStatus ='F'
          oDetail.droppAmount = self.loan_amount*(oDetail.DroppPercentage/100)
          oDetail.FinAgentId = self.FinAgentId
          oDetail.nomor_rekening = self.nomor_rekening
        else:                                           
          oDetail = self.Helper.GetObject('DroppTiering',recDetil.DroppTieringId)
          oDetail.Edit(recDetil)
    #--       

    def ProcessUIData(self, recData):
      pass
      
    def OnDelete(self):   
      pass
      
    def getMarginOutstanding(self):
      return (self.total_projected_income or 0.0) - self.profit_balance
      
    
    def getNextTermin(self):
      config = self.Config
      mlu = config.ModLibUtils
      sSQL='''
        SELECT DroppTieringId FROM %(DROPPTIERING)s WHERE NOMOR_REKENING =%(nomor_rekening)s AND DROPPSTATUS='F' AND rownum=1 ORDER BY droppseq
      '''% { 'DROPPTIERING' : config.MapDBTableName('DROPPTIERING'), 
             'nomor_rekening' :mlu.QuotedStr(self.nomor_rekening)
          } 
      q = config.CreateSQL(sSQL).rawresult
      dtid = 0
      if not q.Eof:
        dtid = q.DroppTieringId      
        
      return dtid
            
    def setTieringAccount(self, recDropping, recTiering):
      helper = self.Helper
      config = self.Config
      if recTiering.RecordCount>0:
        sSQL=''' select * from %s where nomor_rekening='%s' ''' % (config.MapDBTableName("FinParameterTiering"),self.nomor_rekening)
        q = config.CreateSQL(sSQL).RawResult
        t_code = None
        if not q.Eof:
          t_code = q.tiering_code      

        ph = {'recData': recDropping, 'recDetail': recTiering, 'recDeleted':None}
        if t_code==None:
          tier_ = recDropping.GetFieldByName('LParameterTiering.tiering_code')
          new_seq = getCustomId(config, 'tiering_sched', tier_)
          tiering_code = '%s_%s' % (tier_, str(int(new_seq)).zfill(5))
    
          oTiering = helper.CreatePObject("FinParameterTiering", ph)
          oTiering.tiering_code = tiering_code
          oTiering.description  = recDropping.GetFieldByName('LParameterTiering.description')
          oTiering.nomor_rekening = self.nomor_rekening
        else:
          oTiering = helper.GetObject("FinParameterTiering", t_code)
          sSQL=''' delete from %s where tiering_code='%s' ''' % (config.MapDBTableName("FinParameterTieringDetail"),t_code)
          dbutil.runSQL(config, sSQL)
        
        oTiering.listTiering(ph, 1)
        
    def setNextRepricing(self, tgl_repicing, _seq):
      self.repricing_date = tgl_repicing
      oProd = self.LFinProduct
      oSchd = self.LActiveSchedule
      nseq = _seq - 1 + oProd.repricing_period
      if _seq < oProd.fixed_period:
        nseq = oProd.fixed_period
        
      oSchDetail = oSchd.findDetailBySeq(nseq)

      if oSchDetail not in [None]:
        self.next_repricing_date = oSchDetail.sched_date           
    #--

    #MMQ
    def getTrxJournal(self, param,jns_jurnal, kj=None):  #virtual method
      config = self.config
      fau = FinAccountUtils(config)
      kode_jenis = self.kode_jenis if kj==None else kj
      
      lgr = []
      if jns_jurnal =='payment': # Manual Payment
        payment = param.payment
        bayarPokok  = param.principal_amount 
        bayarMargin = param.profit_amount - param.mukasah_amount
        proporsiMukasah = param.mukasah_amount
        total_bayar = bayarPokok + bayarMargin
        tunggakan = -round(self.arrear_balance,2) # fixed by IK 22 August, abs resilience 
        tunggakan_debet = bayarPokok if bayarPokok<tunggakan else tunggakan
        pitang_debet = round(bayarPokok - tunggakan_debet,2)
        ket = param.keterangan
        ## mengurangi pokok piutang
        lgr += [{'sign': 'D', 'amount': payment, 'kode_tx_class': fau.get_tx(kode_jenis,'pembayaran') }]
        
        if self.arrear_balance < 0 : # Cek Tunggakan   
          # ada tunggakan: piutang murabahah jatuh tempo vs payment  
          lgr += [{'sign': 'C', 'amount': tunggakan_debet, 'kode_tx_class': fau.get_tx(kode_jenis,'tunggakan') }]                

        lgr += [{'sign': 'C', 'amount': pitang_debet, 'kode_tx_class':  fau.get_tx(kode_jenis,'outstanding') }]
        #--                  

        lgr.append( {'sign': 'C', 'amount': bayarMargin, 'kode_tx_class':  fau.get_tx(kode_jenis,'pendapatan')} )
        if proporsiMukasah > 0 :
          lgr.append( {'kode_tx_class' :  fau.get_tx(kode_jenis,'pendapatan'), 'sign' : 'C', 'amount' :	proporsiMukasah, 'keterangan': ket+' [SUBSIDI]' } )      
                
        #--        
      # -- endif payment
      
      elif jns_jurnal =='pelunasan_dipercepat': # Manual pelunasan_dipercepat
        payment = copy.copy(param.payment)
        mukasah = param.mukasah_amount        
        profit_accrue = self.profit_accrue_balance # fixed by IK 22 August, abs resilience
        profit_pending = self.profit_pending_balance # fixed by IK 22 August, abs resilience
        tunggakan = -self.arrear_balance # fixed by IK 22 August, abs resilience
        mmd_jt = self.profit_arrear_balance # fixed by IK 22 August, abs resilience
        mmd = self.mmd_balance # fixed by IK 22 August, abs resilience
        #margin = self.getMarginOutstanding() # fixed by IK 22 August, abs resilience
        saldo = -self.saldo # fixed by IK 22 August, abs resilience
        
        lgr += [ 
          {'kode_tx_class' : fau.get_tx(kode_jenis, 'pembayaran'), 'sign' : 'D', 'amount' :	payment },
          {'kode_tx_class' : fau.get_tx(kode_jenis, 'mukasah'), 'sign' : 'D', 'amount' :	mukasah }
        ] 
        # col 3 : anggap ada perpindahan kol ke kol 1
        if self.overall_col_level > 2 :
          lgr.append( {'sign': 'C', 'amount': profit_pending, 'kode_tx_class': fau.get_tx(kode_jenis, 'margin_penyelesaian')} ) # off balance sheet?
                                   
        # sudah kol 1
        lgr += [ 
          {'kode_tx_class' : fau.get_tx(kode_jenis, 'tunggakan'), 'sign' : 'C', 'amount' :	tunggakan}
          ,{ 'kode_tx_class' : fau.get_tx(kode_jenis, 'outstanding'), 'sign' : 'C', 'amount' :	saldo}

          ,{'kode_tx_class' : fau.get_tx(kode_jenis, 'pendapatan_akru'), 'sign' : 'D', 'amount' :	profit_accrue }
          ,{'kode_tx_class' : fau.get_tx(kode_jenis, 'yadit_mmd'), 'sign' : 'C', 'amount' :	profit_accrue }

          ,{'kode_tx_class' : fau.get_tx(kode_jenis, 'tunggakan_margin'), 'sign' : 'D', 'amount' :	mmd_jt }
          ,{'kode_tx_class' : fau.get_tx(kode_jenis, 'margin_ditangguhkan'), 'sign' : 'D', 'amount' :	mmd }
          ,{'kode_tx_class' : fau.get_tx(kode_jenis, 'pendapatan'), 'sign' : 'C', 'amount' :	mmd_jt+mmd }
        ] 
        lgr += FinAccount.getTrxJournal(self,param,'reverse_PPAP')                                                                          
        
      elif jns_jurnal =='input_AYDA': # transaksi input AYDA
        os_pokok = self.getPrincipalOutstanding() # sudah dinegatifkan
        nilai_jaminan = param.AYDA_balance # dari parameter, positif
        nilai_AYDA = nilai_jaminan if nilai_jaminan <= os_pokok else os_pokok
        biaya_AYDA = (os_pokok - nilai_jaminan) if (nilai_jaminan < os_pokok) else 0
        
        #journal input ayda
        lgr += [
        #create ayda balance
          {'sign': 'D', 'amount': os_pokok, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_aktiva' ) },
          #{'sign': 'D', 'amount': biaya_AYDA, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_biaya' ) },
        #clear loan balance
          {'sign': 'C', 'amount': -self.saldo, 'kode_tx_class':  fau.get_tx(kode_jenis,'outstanding') },
          {'sign': 'D', 'amount': self.mmd_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'margin_ditangguhkan' ) },
        #reverse Akru jika ada  
          {'sign': 'D', 'amount': self.profit_accrue_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'pendapatan_akru' ) },
          {'sign': 'C', 'amount': self.profit_accrue_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'yadit_mmd' ) },
        #reverse Tunggakan            
          {'sign': 'D', 'amount': self.profit_arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan_margin' ) },
          {'sign': 'C', 'amount': -self.arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan' ) },
          {'sign': 'C', 'amount': -self.profit_pending_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'margin_penyelesaian' ) },
        #Create PPANP lancar 1% by yayan 09dec14
          {'sign': 'D', 'amount': os_pokok*0.01, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_by_cadangan' ) },
          {'sign': 'C', 'amount': os_pokok*0.01, 'kode_tx_class':  fau.get_tx(kode_jenis,'ayda_cadangan' ) }
        ]        
        lgr += FinAccount.getTrxJournal(self,param,'reverse_PPAP')                                                                          
      
      elif jns_jurnal =='hapus_buku': # transaksi Write Off Hapus Buku
        os_pokok = round(self.getPrincipalOutstanding(),2)                                           
        os_margin = round(self.getMarginOutstanding(),2)
        total_ppap  = round(self.reserved_common_balance+self.reserved_loss_balance+self.reserved_manual_balance,2)
        bentuk_reverse_ppap = os_pokok - total_ppap
        
        #journal input ayda
        lgr += [
        # rerevse PPAP
          {'sign': 'D', 'amount': self.reserved_common_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_umum' ) },
          {'sign': 'D', 'amount': self.reserved_loss_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus' ) },
          {'sign': 'D', 'amount': self.reserved_manual_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_adjustment' ) },
        # bentuk tambahan ppap
          #{'sign': 'C', 'amount': bentuk_reverse_ppap, 'kode_tx_class':  fau.get_tx(kode_jenis,'ppap_khusus' ) },
          {'sign': 'D', 'amount': bentuk_reverse_ppap, 'kode_tx_class':  fau.get_tx(kode_jenis,'by_ppap_khusus' ) },
        #clear loan balance
          {'sign': 'C', 'amount': -self.saldo, 'kode_tx_class':  fau.get_tx(kode_jenis,'outstanding') },
          {'sign': 'D', 'amount': self.mmd_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'margin_ditangguhkan' ) },
        #reverse Akru jika ada  
          {'sign': 'D', 'amount': self.profit_accrue_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'pendapatan_akru' ) },
          {'sign': 'C', 'amount': self.profit_accrue_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'yadit_mmd' ) },
        #reverse Tunggakan            
          {'sign': 'D', 'amount': self.profit_arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan_margin' ) },
          {'sign': 'C', 'amount': -self.arrear_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'tunggakan' ) },
          {'sign': 'C', 'amount': -self.profit_pending_balance, 'kode_tx_class':  fau.get_tx(kode_jenis,'margin_penyelesaian' ) },
        # Create Write Off  
          {'sign': 'D', 'amount': os_pokok, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off' ) },
          {'sign': 'D', 'amount': os_margin, 'kode_tx_class':  fau.get_tx(kode_jenis,'write_off_margin' ) }
        ]        
      else:
        lgr = FinAccount.getTrxJournal(self,param,jns_jurnal)
        
      return lgr
      
#-- mmq         

class FinCustAdditionalData(pobject.PObject):
    # Static variables
    pobject_classname = 'FinCustAdditionalData'
    pobject_keys      = ['nomor_nasabah']
   
    ls_Attr = [ 
      'locked*',
      'status_update*',
      'golongan_debitur=*Lgolongan_debitur.refdata_id',
      'SID_DIN*',
      'SID_ID_Debitur*',
      'melanggar_BMPK*',
      'melampaui_BMPK*',
      'rating_debitur*',
      'lembaga_rating*',
      'go_public*',
      'lembaga_pemeringkat=*Llembaga_pemeringkat.refdata_id',
      'peringkat_debitur=*Lperingkat_debitur.refdata_id',
      'linked_with_bank*',
      'add_field1*',
      'add_field2*',
      'add_field3*',
      'tanggal_pemeringkat*',
      'hub_dengan_bank=*Lhub_dengan_bank.refdata_id',
      'add_dati2=*LDati2.refdata_id',
      'status_sid=*LStatus_sid.refdata_id',
      'cust_group_id=*LFinCustGroups.cust_group_id'
      ]

    def OnCreate(self, param=None):
      config = self.Config
      helper = self.Helper
      if param <> None:      
        recSrc = param['FinCustAdditionalData']
        # set other fields w/ non simple assignment
        attr = self.ls_Attr
        attr.append('nomor_nasabah')
        atutil.transferAttributes(helper, 
          attr, self, recSrc
        )
        # create dummy account to maintain facility limit and usage
      self.config.FlushUpdates()

    def EditData(self, param):
      config = self.Config
      helper = self.Helper      
      # set other fields w/ non simple assignment
      atutil.transferAttributes(helper, 
        self.ls_Attr, self, param
      )
      # create dummy account to maintain facility limit and usage
      self.config.FlushUpdates()
      
    def InsertMember(self, group_code, no_nasabah):
      config = self.Config
      helper = self.Helper
      oDetail = helper.GetObject('FinCustGroupLink', {'cust_group_id':group_code,'nomor_nasabah':no_nasabah})
      if oDetail.IsNull:
        oDetail = helper.CreatePObject('FinCustGroupLink')
        oDetail.cust_group_id = group_code 
        oDetail.nomor_nasabah = no_nasabah 
  
#--

class FinAccAdditionalData(pobject.PObject):
    # Static variables
    pobject_classname = 'FinAccAdditionalData'
    pobject_keys      = ['nomor_rekening']
    
    ls_Attr = [ 
      'add_field1*',
      'add_field2*',
      'add_field3*',
      'add_field4*',
      'add_field5*',
      'add_field6*',
      'bagian_yang_dijamin*',
      'data_restruktur*',
      'golongan_piutang=*Lgolongan_piutang.refdata_id',
      'jenis_fasilitas=*Ljenis_fasilitas.refdata_id',
      'jenis_penggunaan=*Ljenis_penggunaan.refdata_id',
      'jenis_piutang=*Ljenis_piutang.refdata_id',
      'kategori_portofolio=*Lkategori_portofolio.refdata_id',
      'kategori_usaha=*Lkategori_usaha.refdata_id',
      'keterangan_SID*',
      'kondisi_debitur*',
      'lokasi_proyek=*Llokasi_proyek.refdata_id',
      'orientasi_penggunaan=*Lorientasi_penggunaan.refdata_id',
      'penjamin=*Lpenjamin.refdata_id',
      'permasalahan_debitur*',
      'plafond_paripasu*',
      'restruktur_ke*',
      'sebab_macet=*Lsebab_macet.refdata_id',
      'sektor_ekonomi=*Lsektor_ekonomi.refdata_id',
      'SID_ID_Fasilitas*',
      'sifat_investasi=*Lsifat_investasi.refdata_id',
      'sifat_piutang=*Lsifat_piutang.refdata_id',
      'status_piutang=*Lstatus_piutang.refdata_id',
      'status_update*',
      'tgl_restruktur_akhir*',
      'tgl_restruktur_awal*',
      'tujuan_penggunaan=*Ltujuan_penggunaan.refdata_id',
      'restructuretype=*LRestructureType.refdata_id',
      'takeover=*LTakeOver.refdata_id'
      ]
    
    def OnCreate(self, param):
      #self.                          
      config = self.Config
      helper = self.Helper      
      recSrc = param['FinAccAdditionalData']
     
      attr = self.ls_Attr
      attr.append('nomor_rekening')
      #raise Exception, attr
      atutil.transferAttributes(helper, 
        attr, self, recSrc
      )
      # create dummy account to maintain facility limit and usage
      self.config.FlushUpdates()

    def EditData(self, param):
      config = self.Config
      helper = self.Helper
      old_fp = self.plafond_paripasu      
      new_fp = param.plafond_paripasu      
      # set other fields w/ non simple assignment
      atutil.transferAttributes(helper, 
        self.ls_Attr, self, param
      )
      # create dummy account to maintain facility limit and usage
      if old_fp <> new_fp:
        self.HitungUlangParipasu()
      self.config.FlushUpdates()
      
    def HitungUlangParipasu(self):
      config = self.Config
      helper = self.Helper      
      q = config.CreateSQL("""
          SELECT * FROM %(FinCollateralAccount)s a WHERE a.norek_finaccount='%(norek)s'
        """ % {
          'FinCollateralAccount': config.MapDBTableName('FinCollateralAccount'),
          'norek': self.nomor_rekening
      }).RawResult
      
      while not q.Eof :
        oColl = helper.GetObject('FinCollateralAsset', q.norek_fincollateralasset )
        #hitung paripasu
        oColl.calcParipasu()
        q.Next()
#--

class FinMudharabahAccount(FinMusyarakahAccount):
    # Static variables
    pobject_classname = 'FinMudharabahAccount'
    pobject_keys      = ['nomor_rekening']
    base_kode_tx_class = '13'
    
    #kode_jenis = 'MDB'

    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      recSrc = param['FinMudharabahAccount']
      param['FinMusyarakahAccount'] = recSrc # copy musyarakah parameter
      FinMusyarakahAccount.OnCreate(self, param) # call parent constructor
      self.kode_jenis = 'MDB'
      self.financing_model = 'T'
      oGen = atutil.GeneralObject(recSrc)
      config.FlushUpdates()
    #--
      
    def ProcessUIData(self, recData):
      pass
      
    def OnDelete(self):
      pass
      
#-- mudharabah

class FinAccCost(pobject.PObject):
  # Static variables
  pobject_classname = 'FinAccCost'
  pobject_keys      = ['finacccost_id']

  def OnCreate(self, param):
    self.save(param)
    #raise Exception, 'B'
        
  def save(self, param):  #self.                          
    config = self.Config
    helper = self.Helper      
    recSrc = param['FinCost']
    #raise Exception,recSrc.GetFieldByName('LFinCostElement.cost_element_id')
    atutil.transferAttributes(helper, 
          [ 'cost_element_id=*cost_element_id','finacccost_id*','nomor_rekening*','cost_amount*', ], self, recSrc
    )
    self.cost_amortized=0
    ##ita
    '''    
    oFinAccount = helper.GetObject('FinAccount', self.nomor_rekening)
    oFinAccount.CastToLowestDescendant()
    if oFinAccount.kode_jenis == 'PRK':
      periodHelper = helper.CreateObject('core.PeriodHelper')
      self.start_amortize_date = periodHelper.GetAccountingDate()
      self.end_amortize_date = oFinAccount.LActiveSchedule.getLastPaymentDate()
      self.is_procced = 'T'
    #--
    '''
    # create dummy account to maintain facility limit and usage
    self.config.FlushUpdates()

  def edit(self, param):  #self.                          
    config = self.Config
    helper = self.Helper      
    recSrc = param['FinCost']
    #raise Exception,recSrc.GetFieldByName('LFinCostElement.cost_element_id')
    atutil.transferAttributes(helper, 
          [ 'cost_element_id=*cost_element_id','nomor_rekening*','cost_amount*', ], self, recSrc
    )
    # create dummy account to maintain facility limit and usage
    self.config.FlushUpdates()

    
# UNUSED CODES
'''
      q = self.config.CreateSQL('SELECT * FROM pbscore.detiltransaksi WHERE id_transaksi = %d' % oTransaction.id_transaksi).RawResult
      d = []
      while not q.Eof:
        a = {}
        a['kode_account'] = q.kode_account
        a['nomor_rekening'] = q.nomor_rekening
        a['kode_tx_class'] = q.kode_tx_class
        a['jenis_mutasi'] = q.jenis_mutasi
        a['nilai_mutasi'] = q.nilai_mutasi
        d.append(a)
        q.Next()
      self.config.SendDebugMsg('JOURNAL DETAILS\r\n%s' % str(d))
'''



class FinPRKAccount(FinAccount):
    # Static variables
    pobject_classname = 'FinPRKAccount'
    pobject_keys      = ['nomor_rekening']
    
    #kode_jenis = 'PRK'
    financing_model = 'T'

    def OnCreate(self, param):
      config = self.Config; helper = self.Helper
      
      FinAccount.OnCreate(self, param)              
      self.is_revolving = 'T'                       
      self.kode_jenis = 'PRK'
      
      recSrc = param['FinPRKAccount']
      # set fields with simple / direct 1-on-1 assignment from record with same field name
      atutil.transferAttributes(helper, 
        [ 'income_target', 'share_value', 'projection_amount'
        ],
        self, recSrc
      )
      dictInitValues = {
        'commitment_balance' : 0.0, 
        'profit_balance' : 0.0 
        #,'share_value' : 0.0
      }
      for initField, initValue in dictInitValues.iteritems():
        self.SetFieldByName(initField, initValue)
                
      oRek = helper.GetObject('core.Giro', self.norek_paymentsrc)
      if not oRek.IsNull:
        if oRek.HasAccountPRK():  
          raise Exception, 'Nomor rekening %s sudah memiliki fasilitas PRK yang aktif' % self.norek_paymentsrc  
                               
      decday = int(helper.GetObject('ParameterGlobal', 'PRK_TGL_DC').nilai_Parameter)   
      bghday = int(helper.GetObject('ParameterGlobal', 'PRK_TGL_ANGS').nilai_Parameter)   
      periodHelper = self.Helper.CreateObject('core.PeriodHelper')
      tgl_journal = periodHelper.GetAccountingDate()        
      tp_tgl_journal = y, m, d = config.ModLibUtils.DecodeDate(tgl_journal)
      tp_tgl_journal = tp_tgl_journal.__add__((0,0,0,0,0,0))    
      #y,m,d = libs.addMonthUnits(tp_tgl_journal, self.period_count)[:3]
      tgl_journal_th = config.ModLibUtils.EncodeDate(y,m,d)
      tp_tgl_angsur_awal = libs.addMonthUnits(tp_tgl_journal, 1) #tgl di bulan berikutnya
      tgl_angsur_awal = config.ModLibUtils.EncodeDate(tp_tgl_angsur_awal[0],tp_tgl_angsur_awal[1],bghday)

      self.last_create_provisi_date = self.opening_date = tgl_journal
      if d >= decday:
        self.next_create_provisi_date = config.ModLibUtils.EncodeDate(tp_tgl_angsur_awal[0], tp_tgl_angsur_awal[1], decday)
      else:                  
        self.next_create_provisi_date = config.ModLibUtils.EncodeDate(y, m, decday)
                                                    
      p = {
        'nomor_rekening': self.norek_paymentsrc,
        'nomor_rekening_prk': self.nomor_rekening,
        'income_target': self.income_target,
        'tgl_journal' : tgl_journal,        
        'tgl_journal_th' : tgl_journal_th
        }
        
      oRegPRK = self.Helper.CreatePObject('core.RegisterGiroPRK', p)

      install = libs.installment(self.period_count)
      install.runSimulation(self.getPricingModel(), self.income_target, self.targeted_eqv_rate)
      total_margin = install.margin
      oTmpSchedule = self.Helper.CreatePObject("FinPaymentSchedule")
      oDropDetailSched = oTmpSchedule.initFromZeroSimulation(self, tgl_journal, tgl_angsur_awal, install, False)
      self.LActiveSchedule = oTmpSchedule
      self.drop_counter += 1                 
      self.total_facility_limit = self.income_target
      if self.due_date in [None, 0.0]:
        self.due_date = self.LActiveSchedule.getLastPaymentDate()

      config.FlushUpdates()     
    #--       

    def EntryMGPDetail(self, param):
      config = self.Config
      helper = self.Helper      
      uipDetil = param['recDetail']
      # Delete penalty detail
      sSQL ="DELETE FROM %s WHERE nomor_rekening='%s' " % (config.MapDBTableName("PRK_MGP"),self.nomor_rekening)
      config.ExecSQL(sSQL)       
      for i in range(0, uipDetil.RecordCount):
        recDetil = uipDetil.GetRecord(i)
        oTx = helper.CreatePObject("PRK_MGP")
        oTx.nomor_rekening = self.nomor_rekening 
          
        oTx.AddInfo(recDetil)
    #--       

    def GetProyeksiBlnIni(self, SRR):
      config = self.Config
      dParam={
        'prk_mgp' : config.MapDBTableName("PRK_MGP")
        , 'nomor_rekening' : self.nomor_rekening
        , 'srr' : SRR
      }
      sSQL = '''
        SELECT nomor_rekening, Max(mgp_amount) proj_bln_ini, Max(profit_amount) baghas_bln_ini 
        FROM {prk_mgp} a WHERE a.nomor_rekening='{nomor_rekening}'
        AND max_value<={srr}
        GROUP BY nomor_rekening
      '''.format(**dParam)
      rQ = config.CreateSQL(sSQL).RawResult
      
      return rQ.proj_bln_ini
  
    def getLoanAmount(self): # virtual method
      return self.income_target
      
    def PayInstallment(self, Nominal):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      
      periodHelper = helper.CreateObject('core.PeriodHelper')
      tgl = periodHelper.GetAccountingDate()   
      self.total_facility_used -= Nominal

      ldestacc = helper.GetObject('core.RekeningTransaksi', self.norek_paymentsrc)
      
      # CREATE TRANSACTION
      kode_entri = 'FTPP07' 
      keterangan = 'PENURUNAN SALDO POKOK PRK %s' % self.nomor_rekening

      #oTx = FinAccount.initTransaksi(self, kode_entri, tgl, keterangan)
      oTx = helper.CreatePObject("core.Transaksi", kode_entri)
      oTx.SetStandardInfo(tgl, keterangan)
      oTx.status_otorisasi = 1      
         
      entries = [ 
          {'sign': 'C', 'amount': Nominal, 'kode_tx_class': '17001'}
        ]
        
      oDetTxGroupDropping = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupDropping.ProcessDetails()      
                     
      oDT = ldestacc.BuatTransaksiManual('core.DetilTransaksi', 'D', Nominal);
      oDT.LTransaksi = oTx
      oDT.keterangan = oTx.keterangan
      oDT.tanggal_transaksi = oTx.tanggal_transaksi
      oDT.kode_jurnal = '11'
      oDT.Proses()  
      #--            
      helper.CreatePObject('PendingTransactionJournal', oTx)            
            
    def dropping(self, parameters): #PRK
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      
      parameters = atutil.GeneralObject(parameters)
      
      periodHelper = helper.CreateObject('core.PeriodHelper')
      tgl_dropping = periodHelper.GetAccountingDate()      
            
      ldestacc = helper.GetObject('core.Giro', self.norek_paymentsrc)
      amount_dropping = parameters.amount_dropping
      #-- code sudah dipindah ke S_Transaksi : GetTunggakanBaghasPRK
      #saldo = ldestacc.GetSaldoEfektif()
      #profit_arrear = self.LActiveSchedule.GetProfitArrearPRK()      
      #deduct_val = saldo - profit_arrear
      #if deduct_val <= 0:         
      #  amount_dropping = parameters.amount_dropping + saldo
      #else:                                                 
      #  amount_dropping = parameters.amount_dropping + saldo - deduct_val                                          
                                          
      #raise Exception, 'saldo=%s profit_arrear=%s deduct_val=%s parameters.amount_dropping=%s' % (saldo, profit_arrear, deduct_val, parameters.amount_dropping)
        
      self.dropping_date = tgl_dropping     
      self.LDisbursalDest = ldestacc
      self.norek_disbursaldest =  self.norek_paymentsrc    
      self.loan_amount += amount_dropping 
      self.total_facility_used += amount_dropping
      
      # CREATE TRANSACTION
      kode_entri = 'FTD007' 
      keterangan = 'DROPPING PRK %s' % self.nomor_rekening

      #oTx = FinAccount.initTransaksi(self, kode_entri, tgl_dropping, keterangan)
      oTx = helper.CreatePObject("core.Transaksi", kode_entri)
      oTx.SetStandardInfo(tgl_dropping, keterangan)
      oTx.status_otorisasi = 1      
         
      entries = [
          {'sign': 'D', 'amount': amount_dropping, 'kode_tx_class': '17001'}
        ]
        
      oDetTxGroupDropping = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupDropping.ProcessDetails()      
                                                           
      FinAccount.dropping(self, parameters)  
      #FinAccount.CostTransaction(self, parameters)
      
      oDT = ldestacc.BuatTransaksiManual('core.DetilTransaksi', 'C', amount_dropping);
      oDT.LTransaksi = oTx
      oDT.keterangan = oTx.keterangan
      oDT.tanggal_transaksi = oTx.tanggal_transaksi
      oDT.kode_jurnal = '11'
      oDT.Proses()  
      #--            
      helper.CreatePObject('PendingTransactionJournal', oTx)
      
    def PayBaghas(self, Nominal):
      helper = self.Helper
      config = self.Config
      mlu = config.ModLibUtils
      
      periodHelper = helper.CreateObject('core.PeriodHelper')
      tgl = periodHelper.GetAccountingDate()   
      ldestacc = helper.GetObject('core.RekeningTransaksi', self.norek_paymentsrc)
      
      # CREATE TRANSACTION
      kode_entri = 'FPBH01' 
      keterangan = 'PEMBAYARAN BAGHAS PRK %s' % self.nomor_rekening

      #oTx = FinAccount.initTransaksi(self, kode_entri, tgl, keterangan)
      oTx = helper.CreatePObject("core.Transaksi", kode_entri)
      oTx.SetStandardInfo(tgl, keterangan)
      oTx.status_otorisasi = 1      
         
      entries = [ 
          {'sign': 'C', 'amount': Nominal, 'kode_tx_class': '17006'}
        ]
        
      oDetTxGroupDropping = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)  
      oDetTxGroupDropping.ProcessDetails()      
                     
      oDT = ldestacc.BuatTransaksiManual('core.DetilTransaksi', 'D', Nominal);
      oDT.LTransaksi = oTx
      oDT.keterangan = oTx.keterangan
      oDT.tanggal_transaksi = oTx.tanggal_transaksi
      oDT.kode_jurnal = '11'
      oDT.Proses()  
      #--            
      helper.CreatePObject('PendingTransactionJournal', oTx)
      
      oSched = self.LActiveSchedule
      oSched.PaymentBaghasPRK(Nominal)
      
      self.col_eval_repayment = 'T'
      
    def GetSaldoRata(self, startdate, enddate):
      config = self.Config
      app = config.AppObject
      app.ConCreate('out')
      y,m,d = config.ModLibUtils.DecodeDate(startdate)
      sDate = '%s-%s-%s' % (y,m,d)
      y,m,d = config.ModLibUtils.DecodeDate(enddate)
      eDate = '%s-%s-%s' % (y,m,d)
      dTables = {
        'histdetiltransaksi': config.MapDBTableName('core.histdetiltransaksi'),
        'histtransaksi': config.MapDBTableName('core.histtransaksi'),
        'detiltransaksi': config.MapDBTableName('core.detiltransaksi'),
        'transaksi': config.MapDBTableName('core.transaksi'),
        'sDate' : sDate,                                                        
        'eDate' : eDate,
        'nomrek' : self.nomor_rekening,
        'kode_tx_class' : '17001',
      } 
      sSQL = '''
        with t as  
          (  
            select date '%(sDate)s' d1, date '%(eDate)s' d2 from dual  
          )
        select sum(saldo) as sumsaldo from
        (
          select   
            (d1 + i) as tanggal,  
            sum(decode(jenis_mutasi, 'D', nilai_mutasi, -nilai_mutasi)) as saldo  
          from t,   
            xmltable ('for $i in 0 to xs:int(D) return $i' passing xmlelement(d, d2-d1) columns i integer path '.'),  
            %(detiltransaksi)s h,  %(transaksi)s t 
          where  
            h.id_transaksi = t.id_transaksi 
            and t.tanggal_transaksi <= d1 + i 
            and nomor_rekening = '%(nomrek)s'
            and kode_tx_class = '%(kode_tx_class)s'
          group by (d1 + i)
          --order by (d1 + i)

          union 
          
          select   
            (d1 + i) as tanggal,  
            sum(decode(jenis_mutasi, 'D', nilai_mutasi, -nilai_mutasi)) as saldo  
          from t,   
            xmltable ('for $i in 0 to xs:int(D) return $i' passing xmlelement(d, d2-d1) columns i integer path '.'),  
            %(histdetiltransaksi)s h,  %(histtransaksi)s t 
          where  
            h.id_transaksi = t.id_transaksi 
            and t.tanggal_transaksi <= d1 + i 
            and nomor_rekening = '%(nomrek)s'
            and kode_tx_class = '%(kode_tx_class)s'
          group by (d1 + i)
          --order by (d1 + i)
          )      
      ''' % dTables
      #app.ConWriteln(sSQL)
      #app.ConRead('')
      rSQL = config.CreateSQL(sSQL).RawResult
      
      return rSQL.sumsaldo or 0.0

    def GetSaldoAt(self, enddate):
      config = self.Config
      y,m,d = config.ModLibUtils.DecodeDate(enddate)
      eDate = '%s-%s-%s' % (y,str(m).zfill(2),d)
      dTables = {
        'detiltransaksi': config.MapDBTableName('core.detiltransaksi'),
        'transaksi': config.MapDBTableName('core.transaksi'),
        'histdetiltransaksi': config.MapDBTableName('core.histdetiltransaksi'),
        'histtransaksi': config.MapDBTableName('core.histtransaksi'),                                
        'tgl' : eDate,
        'nomrek' : self.nomor_rekening,
        'kode_tx_class' : '17001'      
      } 
      sSQL = '''
        select sum(saldo) as saldo from (
          select   
            sum(decode(jenis_mutasi, 'D', nilai_mutasi, -nilai_mutasi)) as saldo  
          from   
            %(detiltransaksi)s h,  %(transaksi)s t 
          where  
            h.id_transaksi = t.id_transaksi 
            and t.tanggal_transaksi <= to_date('%(tgl)s', 'yyyy-mm-dd') 
            and nomor_rekening = '%(nomrek)s'
            and kode_tx_class = '%(kode_tx_class)s'               

          UNION
          
          select   
            sum(decode(jenis_mutasi, 'D', nilai_mutasi, -nilai_mutasi)) as saldo  
          from   
            %(histdetiltransaksi)s h,  %(histtransaksi)s t 
          where  
            h.id_transaksi = t.id_transaksi 
            and t.tanggal_transaksi <= to_date('%(tgl)s', 'yyyy-mm-dd') 
            and nomor_rekening = '%(nomrek)s'
            and kode_tx_class = '%(kode_tx_class)s' 
        )              
      ''' % dTables
      rSQL = config.CreateSQL(sSQL).RawResult
      
      return rSQL.saldo or 0.0
      
    def ProcessUIData(self, recData):
      pass
      
    def OnDelete(self):
      pass
#--   
    def TunggakBaghas(self):
      config = self.Config
      sSQL = '''
        select sum(profit_amount_whole) - sum(realized_margin) as profit_arrear
        from finaccount a, finpaymentscheddetail b
        where a.id_schedule = b.id_schedule
        and a.nomor_rekening = '%s'            
      ''' % self.nomor_rekening
      rSQL = config.CreateSQL(sSQL).RawResult
      arrear = rSQL.profit_arrear if not rSQL.Eof else 0
      
      return arrear > 0 

class Kafalah(FinAccount):
    # static variable
    pobject_classname = 'Kafalah' # AS DEFINED IN METADATA
    pobject_keys = ['nomor_rekening'] # AS DEFINED IN METADATA
    
    def OnCreate(self,param):
      config = self.Config
      helper = self.Helper           
      FinAccount.OnCreate(self, param)      
      recSrc = param['Kafalah']
      #nomor kafalah tidak auto generate req pak ridwan bcas 27jan15
      #self.no_kafalah = self.getNextKafalahNumber()
      atutil.transferAttributes(helper, 
        ['nama_penerima','alamat1_penerima', 'alamat2_penerima', 'tgl_penerbitan', 'keterangan1', 'keterangan2', 'tgl_mulai_berlaku',
        'nilai_kafalah','jenis_kafalah','tgl_berakhir', 'tgl_klaim_akhir',
        'nama_proyek','lokasi_proyek' , 'nilai_proyek','no_kafalah*'          
        ], self, recSrc
      )
      
      self.currency_code= 'IDR'      
      self.status_kafalah= 'A'  
      self.is_commitmentaccount = 'T'
      self.kode_jenis = 'KAF'
      self.financing_model = 'C' # continous
      
      #raise Exception, FinAccount.id_otorentri
      #self.transactOpenKafalah(self.nilai_kafalah) 
    #--

    def transactOpenKafalah(self,nilai_kafalah=0, keterangan = None, oTx = None):
      kode_entri = 'FTBG01'
      
      nilai_kafalah = self.nilai_kafalah
      #raise Exception, nilai_kafalah
      if keterangan == None:
        keterangan = "Pembukaan Bank Garansi %s" % self.nomor_rekening
      oldTx = oTx  
      if oTx == None:              
        #oTx = FinAccount.initTransaksi(self, kode_entri, periodHelper.GetAccountingDate(), keterangan)
        periodHelper = self.Helper.CreateObject('core.PeriodHelper')
        tgl_trx = periodHelper.GetAccountingDate()
        oTx = self.initTransaksi(kode_entri, tgl_trx, keterangan)
        #oTx = self.Helper.CreatePObject("core.Transaksi", kode_entri)
        #oTx.status_otorisasi = 1
        #oTx.SetStandardInfo(periodHelper.GetAccountingDate(), keterangan)      

      entries = [              
            {'sign': 'C', 'amount': nilai_kafalah, 'kode_tx_class': '19011'}
        ]
        
      oDetTx = self.CreateTransactionGroup(kode_entri, keterangan, oTx, entries)
      oDetTx.ProcessDetails()
                
      self.dropping_date = tgl_trx
      if oldTx == None:
        self.Helper.CreatePObject('PendingTransactionJournal', oTx)
        #oTx.CreateJournal()
      
    #--
    def transactCloseKafalah(self, nilai_kafalah, keterangan = None, oTx = None):
      # CREATE TRANSACTION   
      kode_entri = 'FTBG02'
      
      if keterangan == None:
        keterangan = "Penutupan Bank Garansi %s" % self.nomor_rekening
      oldTx = oTx 
      periodHelper = self.Helper.CreateObject('core.PeriodHelper') 
      if oTx == None:    
        #oTx = FinAccount.initTransaksi(self, kode_entri, periodHelper.GetAccountingDate(), keterangan)
        tgl_trx = periodHelper.GetAccountingDate()
        oTx = self.initTransaksi(kode_entri, tgl_trx, keterangan)
        #oTx = self.Helper.CreatePObject("core.Transaksi", kode_entri)
        #oTx.status_otorisasi = 1
        #
        #oTx.SetStandardInfo(periodHelper.GetAccountingDate(), keterangan)      
      
      entries = [
            {'sign': 'D', 'amount': nilai_kafalah, 'kode_tx_class': '19011'}
        ]
      oDetTx = self.CreateTransactionGroup( kode_entri, keterangan, oTx,entries)
      
      oDetTx.ProcessDetails()

      oFacility = self.LFinFacility
      if not oFacility.IsNull:
        oFacility.id_otorentri = self.id_otorentri
        oFacility.notifyPayment(self, nilai_kafalah)
          
      if oldTx == None:
        self.Helper.CreatePObject('PendingTransactionJournal', oTx)
        #oTx.CreateJournal()

      self.closeFinAcc()
      #self.status_rekening = 3
      #self.closing_date = periodHelper.GetAccountingDate()

    #--
    def getNextKafalahNumber(self):
      row = self.config.CreateSQL("""select %(seq_kafalah)s.NEXTVAL as nextnum from dual""" % dbutil.mapDBTableNames(self.config, ['seq_kafalah'])).RawResult      
      return 'BG' + str( int(row.nextnum) ).zfill(9)
          
    def ProcessUIData(self, recData):
      pass
      
    def OnDelete(self):
      pass
      
    def getLoanAmount(self): 
      return self.nilai_kafalah

    def isRepaymentCompleted(self):
      res = (self.Status_Rekening == 3 or self.status_kafalah == 'T') # TODO pastikan status_rekening
     
      return res
      
            
class KafalahEvent(pobject.PObject):
    # static variable
    pobject_classname = 'KafalahEvent' # AS DEFINED IN METADATA
    pobject_keys = ['id_kafalahevent'] # AS DEFINED IN METADATA
    def OnCreate(self, param=None):
      pass
      # do any necessary initalization here
    #--
#--


          