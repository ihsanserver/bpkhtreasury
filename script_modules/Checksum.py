import sys
import os
import com.ihsan.foundation.pobject as pobject
import types

#TABEL dari MB

def GetCRC16Table(config):
    
    CRC16Table = {}
    for idx in range(256):
      CRC16Table[idx] = 0
      

    dsCRC = config.CreateSQL("select idx, crc_value from %s order by idx" % config.MapDBTableName("core.crctable")).rawresult
    while not dsCRC.Eof :
      CRC16Table[dsCRC.idx] = int(dsCRC.crc_value)
      dsCRC.Next()

    return CRC16Table  

def CalculateCRC16(config, strData):
    
    CRC16Table = GetCRC16Table(config)
    CRC16 = 0

    UpdateString = 0
    lengthData = len(strData)
    for idx in range(lengthData):
        # convert to decimal
        decData = ord(strData[idx])

        # Calculate CRC
        lngValue1 = ((CRC16 & (0xFF00)) / 256) & long(0xFF)
        lngValue1 = CRC16Table[lngValue1]
        lngValue2 = ((CRC16 & long(0xFF)) * long(256)) & long(0xFF00)
        lngValue3 = decData & long(0xFF)
        
        try:
          CRC16 = lngValue1 ^ lngValue2 ^ lngValue3
        except :
          raise Exception,  (idx, lngValue1)
        
    #-- end for
    raise Exception, CRC16    
    return CRC16

#def IsDataValid(self, strData, CheckSum):
#    return str(self.GenerateCheckSum(strData)) == CheckSum

def GenerateCheckSum(config, strData):
    #CheckSum = CalculateCRC16(config, strData)
    CheckSum = getChecksum(config, strData)
    
    return str(CheckSum).zfill(5) 
    
def getChecksum(config, fdata, usedate=False, usetime=False, tablefile=None):
  # Fungsi Checksum dari Mobile Banking 
  app = config.AppObject

  dsCRC = config.CreateSQL("select idx, crc_value from %s order by idx" % config.MapDBTableName("core.crctable")).RawResult
  DEFAULT_CRC_TABLE = []
  while not dsCRC.Eof :
    DEFAULT_CRC_TABLE += [int(dsCRC.crc_value)]
    dsCRC.Next()
  
  mdata = str(fdata).rstrip()
  if usedate:
    mdata+=usedate
  if usetime:
    mdata+=usetime
  if tablefile and os.path.exists(tablefile):
    with open(tablefile, 'r') as f:
      CTABLE  = [int(line) for line in f]
  else:
    CTABLE = DEFAULT_CRC_TABLE
  mdata = mdata.strip()
  try:
    for bitke in range(len(mdata)):
      if bitke==0:
        crcawal = '0'*16  # crc awal 0000000000000000
      # shr 8 bit
      #app.ConWriteln(crcawal)
      procbin = crcawal[:8]
      # ubah ke decimal
      procdec = int(procbin, 2)
      # lookup pada table
      crcdec = CTABLE[procdec]
      # hasilnya ubah ke binary
      crcbin = str(bin(crcdec))[2:]
      modcrc = '0'*(16-len(crcbin.strip())) + crcbin.strip()
      # didapat x1
      newcrc = (modcrc + '0'*16 )[:16]
      # shr 8 bit
      procbin = crcawal[8:]
      # ambil bitke, ubah ke binary
      dke = mdata[bitke]
      dkedec = ord(dke)
      dkebin = str(bin(dkedec))[2:]
      moddke = '0'*(8-len(dkebin.strip())) + dkebin.strip()
      newdke = (moddke + '0'*8)[:8]
      # gabungkan menjadi x2
      newcomp = procbin+newdke
      res = ''
      # x1 xor x2 untuk mendapat hasil checksum
      for i in range(16):
        if newcrc[i]==newcomp[i]:
          res+='0'
        else:
          res+='1'
      crcawal = (res + '0'*16)[:16]
      hasil = int(res,2)
  except:
    #app.ConRead('error')
    pass
  # concat 3 digit akhir checksum pada kanan data
  return str(hasil).zfill(5)
  