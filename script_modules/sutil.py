# DIALECT VARS
D_ORACLE     = 0x01
D_SQLSERVER  = 0x02 

# GLOBALS
D_DIALECT    = D_ORACLE 

def setDialectToORACLE():
  global D_DIALECT
  D_DIALECT = D_ORACLE
  
def toDateFromString(sdate, dialect=D_DIALECT):
  if dialect == D_ORACLE:
    return "to_date({sdate!r}, 'dd-mm-yyyy')".format(sdate=sdate)
  
  if dialect == D_SQLSERVER:
    return "convert(datetime, {sdate!r}, 105)".format(sdate=sdate)

def toDate(config, adate, dialect=D_DIALECT):
  sdate = config.FormatDateTime('dd-mm-yyyy', adate)
  return toDateFromString(sdate, dialect)
    