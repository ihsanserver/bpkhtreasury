import string, sys
import types, re
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.modman as modman
import datetime

dict_none = ['n/a','N/A','NULL','',0]

def ValidateDataString(data, jenis=0):
  sValidValue = ''
  if data in dict_none:
    data = None

  if data!=None:
    try:
      sValidValue = data
      if type(data) == types.IntType or type(data) == types.FloatType:
        sValidValue = str(int(data))
    except:
      pass
      
  if jenis == 1:
    sValidValue = re.sub(r'[^\w]', '', sValidValue)
    
  return sValidValue.strip()  

def ValidateDataInt(data):
  sValidValue = 0
  if data in dict_none:
    data = None

  if data!=None:
    try:
      'sValidValue = int(data)'
      'if type(data) == types.StringType:'
      sValidValue = int(data)
    except:
      pass
      
  return sValidValue  

def ValidateDataFloat(data):
  sValidValue = 0.0
  if data in dict_none:
    data = None

  if data!=None:
    try:
      'sValidValue = int(data)'
      'if type(data) == types.StringType:'
      sValidValue = float(data)
    except:
      pass
      
  return sValidValue  

def ValidateDataNumber(config, data):
  sValidValue = 0.0
  if data not in dict_none:
    sValidValue = data
      
  return sValidValue  
  
def to_Float(data):
  if data!=None:
    try:
      sValidValue = data
      if type(data) == types.IntType or type(data) == types.FloatType:
        sValidValue = '%0.4f'%data
        #print sValidValue
      else:
        tData=data.replace(',','').replace('-','')
        sData=tData.strip()
        sValidValue = 0.0 if sData=='' else tData
        #print 'else'
      #print 'OK'
    except:
      pass
      
  return sValidValue  
  
def DateFloatToStr(data, config=None):
  sValidValue = ''
  if data in dict_none:
    data = None

  if data!=None:
    try:
      if type(data) == types.FloatType and config not in [None]:
        y,m,d = config.ModDateTime.DecodeDate(data)
        
        sValidValue = "%02d-%02d-%d" % (d,m,y)
        
        datetime.strptime(sValidValue, '%d-%m-%Y')
    except:
      pass
      
  return sValidValue
  
def validate_date(d, formatdate):
  try:
    datetime.datetime.strptime(d, '%Y/%m/%d')
  except ValueError:
    raise ValueError("Format tanggal salah, seharusnya [%s]" % formatdate)

def to_date(data, jenis=0, config=None):
  sValidValue = 'null'
  if type(data) <> types.FloatType and type(data) <> types.IntType:
    data=data.strip()
    
  if data in dict_none:
    data = None

  if data!=None:
    #try:
    has_value = 'F'
    if type(data) == types.FloatType and config not in [None]:
      formatdate = 'YYYYMMDD'
      y,m,d = config.ModDateTime.DecodeDate(data)
    else:
      if jenis ==0: #YYYYMMDD
        formatdate = 'YYYYMMDD'
        y = data[:4]; m=data[4:6]; d=data[6:]or'01'
      elif jenis == 1:
        formatdate = 'DD-MM-YYYY'
        d,m,y = data.split('-')
      elif jenis == 2: #DD/MM/YYYY
        formatdate = 'DD/MM/YYYY'
        d,m,y = data.split('/')
      elif jenis == 3:
        formatdate = 'YYYY-MM-DD'
        y,m,d = data.split('-')
      elif jenis ==4:
        data = str(int(data))
        y = data[:4]; m=data[4:6]; d=data[6:]or'07'
        formatdate = 'YYYYMM'
      elif jenis ==5:
        has_value = 'T'
        trdata = data.strip()
        if len(trdata)<>9:
          sValidValue = 'null'
        else:
          data = data.upper()
          sValidValue = "TO_DATE('%s','DD-MON-RR')" % data
      elif jenis == 6:
        m,d,y = data.split('/')
        formatdate = 'MM/DD/YYYY'

    if has_value == 'F' and d not in ('',None) and m not in ('',None) and y not in ('',None) :
      date_val = '%s/%s/%s' % (y,m,d)
      valData = validate_date(date_val, formatdate) 

      sValidValue = "TO_DATE('%s','YYYY/MM/DD')" % date_val
    #except:
    #  pass
      
  return sValidValue

def ValidateImp(Nilai, dType, config=None):

  if dType in ['S']:
    mlu = config.ModLibUtils
    nVal = mlu.QuotedStr(ValidateDataString(Nilai)).upper()
  elif dType == 'F':
    nVal = ValidateDataFloat(Nilai)
  elif dType == 'P': # data %
    nVal = float(Nilai or 0.0)*100.0
  elif dType == 'I':
    nVal = ValidateDataInt(Nilai)
  elif dType == 'D':
    nVal = to_date(Nilai,2,config)  
  elif dType == 'T':
    nVal = to_date(Nilai,3,config)  
  elif dType == 'N':
    nVal = ValidateDataNumber (config, Nilai)
  else:
    nVal = Nilai
    
  return nVal
#---
    
def CreateUploadBatch(config, tplcode, u_file_name):
  mlu = config.ModLibUtils
  helper = pobjecthelper.PObjectHelper(config) 
  periodHelper = helper.CreateObject('core.PeriodHelper')
  tgl_trx = periodHelper.GetAccountingDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', tgl_trx)
  
  sqlutil = modman.getModule(config, 'sqlutil')
  

  dParam = {
    'tplcode': mlu.QuotedStr(tplcode),
    'u_date': sqlutil.toDateTime(config, config.Now()), #mlu.QuotedStr(strDate),
    'u_input': mlu.QuotedStr(config.SecurityContext.UserID),
    'u_file_name': mlu.QuotedStr(u_file_name),
    'u_kode_cabang' : mlu.QuotedStr(config.SecurityContext.GetUserInfo()[4]),
  }
  
  rs = config.CreateSQL("select seq_uploadbatch.nextval as id_batch from dual").RawResult
  rs.First()
  dParam['id_batch'] = rs.id_batch

  sSQL='''
    insert into uploadbatch (
      id_batch, upload_date,tplcode,upload_file_name,user_input,upload_state, kode_cabang
    ) values
    (
      %(id_batch)s, %(u_date)s, %(tplcode)s, %(u_file_name)s, %(u_input)s, 'U', %(u_kode_cabang)s
    )
  ''' % dParam
  dbutil.runSQL(config,sSQL)
  
  return dParam['id_batch']
  
def GenCekNullCondition(field_name, dType):
  # dType
  # S = String
  # N = Number
  # else F = Float

  if dType=='S':
    _cn = "nvl({field_name}, '')='' "
  elif dType=='N': #boleh diidi 0
    _cn = "{field_name} is null "
  else:
    _cn = "nvl({field_name}, 0)=0 "
        
  return _cn.format(field_name = field_name)
  
def genRequiredColTypes(_cols, _reqCols):
  _reqColTypes = {}
  for col_def in _cols.values() :
    if col_def[0] in _reqCols :
      _reqColTypes[col_def[0]] = col_def[1]
    
  return _reqColTypes 

def ValidateRequiredField(config, dcReqFields, sqlparams, debugmode = False):
  # params 
  # Fungsi untuk validasi generik untuk field yang mandatory
  # config = DAF config object
  # dcReqFields = Dictionary mandatory field dengan ketentuan berikut --> { fieldname : fieldtype ,}
  # sqlparams = Dictionary variable string sql dengan ketentuan key berikut init harus ada
  #             id_batch : id batch upload yang akan di proses
  #             tmptable : nama tabel temporary penampung data migrasi
  #             primary_fieldname : nama field primary key
  
  app = config.AppObject
  
  for fieldname, fieldtype in dcReqFields.iteritems() :
    sqlparams['cekNull'] = GenCekNullCondition(fieldname , fieldtype)
    sqlparams['field_name'] = fieldname    
    
    sSQL = """
       insert into errorbatch (err_id, id_batch, line_number, primarycode, error_code, additional_desc) 
       select seq_errorbatch.nextval, {id_batch}, baris, {primary_fieldname}, 'G01', 'Field {field_name} tidak boleh kosong (Baris ' || baris || ' )'
       from {tmptable}
       where id_batch = {id_batch} 
         and {cekNull}
      """.format(**sqlparams)
    if debugmode:
      app.ConWriteln('Execute... ' + sSQL)
    dbutil.runSQL(config, sSQL)
  # end for
  
def ExcecuteSQLFlow(config, dcSQLSources, LsSQLFlow, sqlparams, debugmode = False):
  app = config.AppObject
  for flowname in LsSQLFlow :        
    app.ConWriteln( ">> %s " % flowname)
    sSQL = dcSQLSources[flowname].format(**sqlparams)
    if debugmode :
      app.ConWriteln('Execute... ' + sSQL)
      app.ConRead('')
    dbutil.runSQL(config, sSQL)