import os
import sys
#import rpdb2; rpdb2.start_embedded_debugger("000")

class lookup:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.kode_tx_class
    self.kode_jenis = fr.kode_jenis
    
  def initQueryObject(self, rqsql):
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT kode_tx_class, deskripsi
      FROM %s
      ''' % (self.config.MapDBTableName('core.DetilTransaksiClass'))
    rqsql.WHEREClause = '''
       kode_jenis = %s AND (kode_tx_class LIKE %s or upper(deskripsi) LIKE upper(%s)) 
    ''' % (mlu.QuotedStr(self.kode_jenis), mlu.QuotedStr('%%%s%%' % self.key), mlu.QuotedStr('%%%s%%' % self.key))
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("kode_tx_class;deskripsi")
    rqsql.keyFieldName = "kode_tx_class"
    rqsql.setBaseOrderFieldNames("kode_tx_class")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_tx_class'
            Visible = True
            Width = 60
          end
          item
            FieldName = 'deskripsi'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
#-- lookup tx_code

class lookup_2:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.kode_tx_class
    self.kode_jenis = fr.kode_jenis
    
  def initQueryObject(self, rqsql):
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT kode_tx_class, deskripsi
      FROM %s
      ''' % (self.config.MapDBTableName('core.DetilTransaksiClass'))
    rqsql.WHEREClause = '''
       kode_jenis in (%s, 'WAD') AND (kode_tx_class LIKE %s or upper(deskripsi) LIKE upper(%s)) 
    ''' % (mlu.QuotedStr(self.kode_jenis), mlu.QuotedStr('%%%s%%' % self.key), mlu.QuotedStr('%%%s%%' % self.key))
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("kode_tx_class;deskripsi")
    rqsql.keyFieldName = "kode_tx_class"
    rqsql.setBaseOrderFieldNames("kode_tx_class")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_tx_class'
            Visible = True
            Width = 60
          end
          item
            FieldName = 'deskripsi'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
#-- lookup tx_code

