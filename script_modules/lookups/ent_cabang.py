import os
import sys

class lookupCabang_obs:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.kode_cabang
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    #oUser = config.CreatePObjImplProxy("enterprise.User")
    #oUser.Key = config.SecurityContext.InitUser
    rqsql.SELECTFROMClause = '''
      SELECT 
        c.kode_cabang,
        c.nama_cabang
      FROM %(list_cabang_diizinkan)s lc, %(cabang)s c
    ''' % {
      'list_cabang_diizinkan': config.MapDBTableName("enterprise.ListCabangDiizinkan"),
      'cabang': config.MapDBTableName("enterprise.Cabang")
    }
    rqsql.WHEREClause = '''
      (lc.kode_cabang = c.kode_cabang 
      AND ( upper(c.kode_cabang) LIKE upper(%(kode_cabang)s) OR upper(c.nama_cabang) LIKE upper(%(kode_cabang)s) ) 
      AND lc.id_user = %(id_user)s) 
    ''' %  {
      'kode_cabang': mlu.QuotedStr('%%%s%%' % self.key_value),
      'id_user': mlu.QuotedStr(config.SecurityContext.InitUser)
    }                                
    rqsql.setAltOrderFieldNames("c.kode_cabang")
    rqsql.keyFieldName = "c.kode_cabang"
    rqsql.setBaseOrderFieldNames("c.kode_cabang")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_cabang'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'nama_cabang'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
  
class lookupCabang:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.kode_cabang    
    
    sSQL = '''
      SELECT b.tipe_akses_cabang FROM %(userapp)s b WHERE b.id_user='%(id_user)s' 
    ''' % {
      'userapp': config.MapDBTableName("enterprise.userapp"),
      'id_user':config.SecurityContext.InitUser
    }
    rSQL = config.CreateSQL(sSQL).RawResult
    self.tipe_akses = rSQL.tipe_akses_cabang
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils

    sJoinType = 'INNER JOIN'
    if self.tipe_akses == 'S': sJoinType = 'LEFT JOIN'

    rqsql.SELECTFROMClause = '''
      SELECT 
        c.kode_cabang,
        c.nama_cabang
      FROM %(cabang)s c
      %(sJoinType)s %(list_cabang_diizinkan)s lc ON lc.kode_cabang = c.kode_cabang AND lc.id_user = %(id_user)s
    ''' % {
      'list_cabang_diizinkan': config.MapDBTableName("enterprise.ListCabangDiizinkan"),
      'cabang': config.MapDBTableName("enterprise.Cabang"),'sJoinType':sJoinType,
      'id_user': mlu.QuotedStr(config.SecurityContext.InitUser)
    }
    rqsql.WHEREClause = '''
      ( upper(c.kode_cabang) LIKE upper(%(kode_cabang)s) OR upper(c.nama_cabang) LIKE upper(%(kode_cabang)s) )
      AND c.status_aktif = 'T' 
    ''' %  {
      'kode_cabang': mlu.QuotedStr('%%%s%%' % self.key_value)
    }                                
    rqsql.setAltOrderFieldNames("c.kode_cabang")
    rqsql.keyFieldName = "c.kode_cabang"
    rqsql.setBaseOrderFieldNames("c.kode_cabang")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_cabang'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'nama_cabang'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
  
class lookupCabang_trx:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.kode_cabang    
    
    sSQL = '''
      SELECT b.tipe_akses_cabang FROM %(userapp)s b WHERE b.id_user='%(id_user)s' 
    ''' % {
      'userapp': config.MapDBTableName("enterprise.userapp"),
      'id_user':config.SecurityContext.InitUser
    }
    rSQL = config.CreateSQL(sSQL).RawResult
    self.tipe_akses = rSQL.tipe_akses_cabang
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils

    sJoinType = 'LEFT JOIN'
    rqsql.SELECTFROMClause = '''
      SELECT 
        c.kode_cabang,
        c.nama_cabang
      FROM %(cabang)s c
      %(sJoinType)s %(list_cabang_diizinkan)s lc ON lc.kode_cabang = c.kode_cabang AND lc.id_user = %(id_user)s
    ''' % {
      'list_cabang_diizinkan': config.MapDBTableName("enterprise.ListCabangDiizinkan"),
      'cabang': config.MapDBTableName("enterprise.Cabang"),'sJoinType':sJoinType,
      'id_user': mlu.QuotedStr(config.SecurityContext.InitUser)
    }
    rqsql.WHEREClause = '''
      ( upper(c.kode_cabang) LIKE upper(%(kode_cabang)s) OR upper(c.nama_cabang) LIKE upper(%(kode_cabang)s) )
      AND c.status_aktif = 'T' 
    ''' %  {
      'kode_cabang': mlu.QuotedStr('%%%s%%' % self.key_value)
    }                                
    rqsql.setAltOrderFieldNames("c.kode_cabang")
    rqsql.keyFieldName = "c.kode_cabang"
    rqsql.setBaseOrderFieldNames("c.kode_cabang")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_cabang'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'nama_cabang'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
    