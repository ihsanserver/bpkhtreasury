import os
import sys
import com.ihsan.util.dbutil as dbutil

class lookupAllAccount:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT
        l.nomor_rekening, rt.nama_rekening, l.nama_singkat,        
        case when l.status_restriksi='T' then 9999999999 else rt.saldo end saldo 
      FROM %(rekeningliabilitas)s l, %(rekeningtransaksi)s rt
    ''' % dbutil.mapDBTableNames(config, ['core.rekeningtransaksi', 'core.rekeningliabilitas'])
    rqsql.WHEREClause = '''
      l.nomor_rekening=rt.nomor_rekening
      AND l.jenis_rekening_liabilitas in ('G', 'T','P')
      and rt.status_rekening=1
      and (upper(l.nomor_rekening) like upper(%(nomor_rekening)s) or upper(rt.nama_rekening) LIKE upper(%(nomor_rekening)s))
    ''' % {
      'nomor_rekening': mlu.QuotedStr('%%%s%%' % self.key_value)
    }
    rqsql.setAltOrderFieldNames("l.nomor_rekening")
    rqsql.keyFieldName = "l.nomor_rekening"
    rqsql.setBaseOrderFieldNames("l.nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'saldo'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
  #--

class lookupAccount:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    if fr.Dataset.IsFieldExist('nomor_nasabah'):
      self.nomor_nasabah = fr.nomor_nasabah
    elif fr.Dataset.IsFieldExist('no_nasabah'):
      self.nomor_nasabah = fr.no_nasabah
    else:
      raise Exception, "Field nomor_nasabah or no_nasabah was not found"
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    sSQL = '''
      SELECT
        l.nomor_rekening, 
        rt.nama_rekening,
        l.nama_singkat,
        case when l.status_restriksi='T' then 9999999999 else rt.saldo end saldo 
      FROM %(rekeningtransaksi)s rt, %(rekeningliabilitas)s l
    ''' % dbutil.mapDBTableNames(config, ['core.rekeningtransaksi', 'core.rekeningliabilitas'])
    
    rqsql.SELECTFROMClause = sSQL
    wSQL = '''
      (
        l.nomor_nasabah=%(nomor_nasabah)s OR 
        EXISTS (SELECT 1 FROM %(relasinasabah)s rn WHERE l.nomor_nasabah=rn.nomor_nasabah_relasi AND rn.nomor_nasabah=%(nomor_nasabah)s)
        OR l.jenis_rekening_liabilitas='P'
      )  
      AND l.nomor_rekening=rt.nomor_rekening
      and rt.status_rekening=1
      AND l.jenis_rekening_liabilitas in ('G', 'T', 'P')
      AND (upper(l.nomor_rekening) LIKE upper(%(nomor_rekening)s) OR upper(rt.nama_rekening) LIKE upper(%(nomor_rekening)s) )
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%%%s%%' % self.key_value),
      'nomor_nasabah': mlu.QuotedStr(self.nomor_nasabah),
      'relasinasabah': config.MapDBTableName('core.relasinasabah')
    }
    #raise Exception, sSQL+wSQL
    rqsql.WHEREClause = wSQL                                
    rqsql.setAltOrderFieldNames("l.nomor_rekening")
    rqsql.keyFieldName = "l.nomor_rekening"
    rqsql.setBaseOrderFieldNames("l.nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'saldo'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
  #--
  
class lookupPaymentSrc:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    if fr.Dataset.IsFieldExist('nomor_nasabah'):
      self.nomor_nasabah = fr.nomor_nasabah
    elif fr.Dataset.IsFieldExist('no_nasabah'):
      self.nomor_nasabah = fr.no_nasabah
    else:
      raise Exception, "Field nomor_nasabah or no_nasabah was not found"
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    sSQL = '''
      SELECT
        l.nomor_rekening, 
        rt.nama_rekening,
        l.nama_singkat,
        case when l.status_restriksi='T' then 9999999999 else rt.saldo end saldo 
      FROM %(rekeningtransaksi)s rt, %(rekeningliabilitas)s l
    ''' % dbutil.mapDBTableNames(config, ['core.rekeningtransaksi', 'core.rekeningliabilitas'])
    
    rqsql.SELECTFROMClause = sSQL
    wSQL = '''
      (
        l.nomor_nasabah=%(nomor_nasabah)s OR 
        EXISTS (SELECT 1 FROM %(relasinasabah)s rn WHERE l.nomor_nasabah=rn.nomor_nasabah_relasi AND rn.nomor_nasabah=%(nomor_nasabah)s)
      )  
      AND l.nomor_rekening=rt.nomor_rekening
      and rt.status_rekening=1
      AND l.jenis_rekening_liabilitas in ('G', 'T')
      AND upper(l.nomor_rekening) LIKE upper(%(nomor_rekening)s)
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%%%s%%' % self.key_value),
      'nomor_nasabah': mlu.QuotedStr(self.nomor_nasabah),
      'relasinasabah': config.MapDBTableName('core.relasinasabah')
    }
    #raise Exception, sSQL+wSQL
    rqsql.WHEREClause = wSQL                                
    rqsql.setAltOrderFieldNames("l.nomor_rekening")
    rqsql.keyFieldName = "l.nomor_rekening"
    rqsql.setBaseOrderFieldNames("l.nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'saldo'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
  #--
  
class lookupAccount_2:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    self.col_type  = fr.col_type
    if fr.Dataset.IsFieldExist('nomor_nasabah'):
      self.nomor_nasabah = fr.nomor_nasabah
    elif fr.Dataset.IsFieldExist('no_nasabah'):
      self.nomor_nasabah = fr.no_nasabah
    else:
      raise Exception, "Field nomor_nasabah or no_nasabah was not found"
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT
        l.nomor_rekening, l.nama_singkat,
        rt.nama_rekening,
        case when l.status_restriksi='T' then 9999999999 else rt.saldo end saldo 
      FROM %(rekeningliabilitas)s l, %(rekeningtransaksi)s rt
    ''' % dbutil.mapDBTableNames(config, ['core.rekeningtransaksi', 'core.rekeningliabilitas'])
    rqsql.WHEREClause = '''
      l.nomor_nasabah=%(nomor_nasabah)s
      AND l.nomor_rekening=rt.nomor_rekening
      and rt.status_rekening=1
      AND l.jenis_rekening_liabilitas = %(col_type)s
      AND l.nomor_rekening LIKE %(nomor_rekening)s
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%s%%' % self.key_value),
      'nomor_nasabah': mlu.QuotedStr(self.nomor_nasabah),
      'col_type': mlu.QuotedStr(self.col_type)
    }                                
    rqsql.setAltOrderFieldNames("nomor_rekening")
    rqsql.keyFieldName = "nomor_rekening"
    rqsql.setBaseOrderFieldNames("nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'saldo'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
  #--

class lookupGiro:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening

  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT
        l.nomor_rekening, nsb.nama_nasabah as nama_singkat
      FROM %(rekeningtransaksi)s rt, %(rekeningliabilitas)s l, %(nasabah)s nsb 
    ''' % dbutil.mapDBTableNames(config, ['core.rekeningliabilitas', 'core.nasabah', 'core.rekeningtransaksi'])

    rqsql.WHEREClause = '''
      l.nomor_nasabah = nsb.nomor_nasabah
      and rt.nomor_rekening = l.nomor_rekening
      AND l.jenis_rekening_liabilitas ='G'    
      and rt.status_rekening=1
      AND (upper(l.nomor_rekening) LIKE upper(%(nomor_rekening)s)  
      OR upper(nsb.nama_nasabah) LIKE upper(%(nama_singkat)s))          
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%s%%' % self.key_value),
      'nama_singkat': mlu.QuotedStr('%s%%' % self.key_value)
    }
    rqsql.setAltOrderFieldNames("nomor_rekening;nama_singkat")
    rqsql.keyFieldName = "nomor_rekening"
    rqsql.setBaseOrderFieldNames("nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          >
      end
    '''
  #--  #--
  #--
  
     
class lookupGiroPRK:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    if fr.Dataset.IsFieldExist('nomor_nasabah'):
      self.nomor_nasabah = fr.nomor_nasabah
    elif fr.Dataset.IsFieldExist('no_nasabah'):
      self.nomor_nasabah = fr.no_nasabah
    else:
      raise Exception, "Field nomor_nasabah or no_nasabah was not found"
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT
        l.nomor_rekening, l.nama_singkat,
        rt.nama_rekening,
        rt.saldo  
      FROM %(rekeningliabilitas)s l, %(rekeningtransaksi)s rt
    ''' % dbutil.mapDBTableNames(config, ['core.rekeningtransaksi', 'core.rekeningliabilitas'])
    rqsql.WHEREClause = '''
      l.nomor_nasabah=%(nomor_nasabah)s
      AND l.nomor_rekening=rt.nomor_rekening
      and rt.status_rekening=1
      AND l.jenis_rekening_liabilitas in ('G')
      AND (upper(l.nomor_rekening) LIKE %(nomor_rekening)s OR upper(rt.nama_rekening) LIKE %(nomor_rekening)s )
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%s%%' % self.key_value.upper()),
      'nomor_nasabah': mlu.QuotedStr(self.nomor_nasabah)
    }                                
    rqsql.setAltOrderFieldNames("l.nomor_rekening")
    rqsql.keyFieldName = "l.nomor_rekening"
    rqsql.setBaseOrderFieldNames("l.nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'saldo'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
  #--
  
  
class lookupDeposito:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    self.nomor_nasabah = fr.nomor_nasabah
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT
        rt.nomor_rekening,
        rt.nama_rekening,
        rt.saldo
      FROM %(deposito)s dp, %(rekeningtransaksi)s rt, %(rekeningliabilitas)s rl, %(nasabah)s nsb
    ''' % dbutil.mapDBTableNames(config, ['core.deposito', 'core.rekeningtransaksi', 'core.nasabah', 'core.rekeningliabilitas'])
    rqsql.WHEREClause = '''
      rl.nomor_nasabah=%(nomor_nasabah)s    
      AND rl.nomor_rekening = rt.nomor_rekening
      AND rt.nomor_rekening = dp.nomor_rekening
      and rt.status_rekening=1
      AND nsb.nomor_nasabah = rl.nomor_nasabah
      AND rl.is_blokir <> 'T'
      AND dp.disposisi_nominal = 'A'
      AND rt.status_rekening <> '3'  
      AND rl.nomor_rekening LIKE %(nomor_rekening)s
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%s%%' % self.key_value),
      'nomor_nasabah': mlu.QuotedStr(self.nomor_nasabah)
    }                                
    rqsql.setAltOrderFieldNames("nomor_rekening")
    rqsql.keyFieldName = "nomor_rekening"
    rqsql.setBaseOrderFieldNames("nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'saldo'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
  #--
#-- 

class lookupDeposito_without_restrict:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    self.nomor_nasabah = fr.nomor_nasabah
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT
        rt.nomor_rekening,
        rt.nama_rekening,
        rt.saldo
      FROM %(deposito)s dp, %(rekeningtransaksi)s rt, %(rekeningliabilitas)s rl, %(nasabah)s nsb
    ''' % dbutil.mapDBTableNames(config, ['core.deposito', 'core.rekeningtransaksi', 'core.nasabah', 'core.rekeningliabilitas'])
    rqsql.WHEREClause = '''
      rl.nomor_rekening = rt.nomor_rekening
      AND rt.nomor_rekening = dp.nomor_rekening
      and rt.status_rekening=1
      AND nsb.nomor_nasabah = rl.nomor_nasabah
      AND rl.is_blokir <> 'T'
      AND dp.disposisi_nominal = 'A'
      AND rt.status_rekening <> '3'  
      AND ( rl.nomor_rekening LIKE upper(%(nomor_rekening)s) or upper(rt.nama_rekening) LIKE upper(%(nomor_rekening)s) )
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%%%s%%' % self.key_value),
      'nomor_nasabah': mlu.QuotedStr(self.nomor_nasabah)
    }                                
    rqsql.setAltOrderFieldNames("nomor_rekening")
    rqsql.keyFieldName = "nomor_rekening"
    rqsql.setBaseOrderFieldNames("nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'saldo'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
  #--
#-- 

class lookupDeposito2:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    self.nomor_nasabah = fr.nomor_nasabah
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT
        rt.nomor_rekening,
        nsb.nama_nasabah as nama_rekening,
        rl.saldo_deposito
      FROM %(deposito)s dp, %(rekeningtransaksi)s rt, %(rekeningliabilitas)s rl, %(nasabah)s nsb
    ''' % dbutil.mapDBTableNames(config, ['core.deposito', 'core.rekeningtransaksi', 'core.nasabah', 'core.rekeningliabilitas'])
    rqsql.WHEREClause = '''
      upper(rl.nomor_nasabah)=upper(%(nomor_nasabah)s)    
      AND rl.nomor_rekening = rt.nomor_rekening
      AND rt.nomor_rekening = dp.nomor_rekening
      AND nsb.nomor_nasabah = rl.nomor_nasabah
      and rt.status_rekening=1
      AND rl.is_blokir <> 'T'
      AND dp.disposisi_nominal = 'A'
      AND rt.status_rekening <> '3'
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%s%%' % self.key_value),
      'nomor_nasabah': mlu.QuotedStr(self.nomor_nasabah)
    }                                
    rqsql.setAltOrderFieldNames("nomor_rekening")
    rqsql.keyFieldName = "nomor_rekening"
    rqsql.setBaseOrderFieldNames("nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'saldo_deposito'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
  #--
#--  
   
  
class lookupDisbursalAccount:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT
        l.nomor_rekening, rt.nama_rekening, l.nama_singkat, n.nama_nasabah 
      FROM %(rekeningtransaksi)s rt, %(rekeningliabilitas)s l, %(nasabah)s n
    ''' % (dbutil.mapDBTableNames(config, ['core.rekeningtransaksi', 'core.rekeningliabilitas', 'core.nasabah']))
    rqsql.WHEREClause = '''
      ( upper(l.nomor_rekening) LIKE upper(%(nomor_rekening)s)
      OR upper(rt.nama_rekening) LIKE upper(%(nomor_rekening)s) ) 
      and rt.status_rekening=1
      AND l.nomor_rekening = rt.nomor_rekening
      AND l.nomor_nasabah = n.nomor_nasabah
      AND l.jenis_rekening_liabilitas IN ('G', 'T', 'P')
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%%%s%%' % self.key_value)
    }                                
    rqsql.setAltOrderFieldNames("l.nomor_rekening")
    rqsql.keyFieldName = "l.nomor_rekening"
    rqsql.setBaseOrderFieldNames("l.nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          >
      end
    '''
  #--  #--
  #--

class lookupDisbursalAccount_full:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT
        l.nomor_rekening, rt.nama_rekening, l.nama_singkat, n.nama_nasabah 
      FROM %(rekeningtransaksi)s rt, %(rekeningliabilitas)s l, %(nasabah)s n
    ''' % (dbutil.mapDBTableNames(config, ['core.rekeningtransaksi', 'core.rekeningliabilitas', 'core.nasabah']))
    rqsql.WHEREClause = '''
      upper(l.nomor_rekening) = upper(%(nomor_rekening)s)
      and rt.status_rekening=1
      AND l.nomor_rekening = rt.nomor_rekening
      AND l.nomor_nasabah = n.nomor_nasabah
      AND l.jenis_rekening_liabilitas IN ('G', 'T', 'P')
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%s%%' % self.key_value)
    }                                
    rqsql.setAltOrderFieldNames("l.nomor_rekening")
    rqsql.keyFieldName = "l.nomor_rekening"
    rqsql.setBaseOrderFieldNames("l.nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          >
      end
    '''
  #--  #--
  #--

class lookupAccount_GL_SAV_CA:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT a.NOMOR_REKENING,b.DESKRIPSI,a.NAMA_REKENING 
      FROM %(rekeningtransaksi)s a
      LEFT JOIN %(jenisrekeningtransaksi)s b ON a.KODE_JENIS=b.KODE_JENIS
    ''' % (dbutil.mapDBTableNames(config, ['core.rekeningtransaksi', 'core.jenisrekeningtransaksi']))
    rqsql.WHEREClause = '''
      upper(a.nomor_rekening) LIKE upper(%(nomor_rekening)s)
      AND a.kode_jenis IN ('GL','CA','SAV','RAB')
      and a.status_rekening=1
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%s%%' % self.key_value)
    }                                
    rqsql.setAltOrderFieldNames("a.nomor_rekening")
    rqsql.keyFieldName = "a.nomor_rekening"
    rqsql.setBaseOrderFieldNames("a.nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          >
      end
    '''
  #--  #--
  #--
 
class lookupNasabah:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_nasabah
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    sSQL = '''
      SELECT distinct
        n.nomor_nasabah, 
        decode(n.jenis_nasabah,'I','Individu','K','Korporat','') tipe_nasabah,
        n.nama_nasabah 
      FROM %(nasabah)s n
        inner join %(rekeningcustomer)s l on n.nomor_nasabah=l.nomor_nasabah
        inner join %(finaccount)s fa on l.nomor_rekening=fa.nomor_rekening
    ''' % dbutil.mapDBTableNames(config, ['core.nasabah', 'core.rekeningcustomer', 'finaccount'])
    
    rqsql.SELECTFROMClause = sSQL
    wSQL = '''
      (upper(n.nomor_nasabah) LIKE upper(%(nomor_nasabah)s) OR upper(n.nama_nasabah) LIKE upper(%(nomor_nasabah)s) )
    ''' %  {
      'nomor_nasabah':  mlu.QuotedStr('%%%s%%' % self.key_value)
    }
    #raise Exception, sSQL+wSQL
    rqsql.WHEREClause = wSQL                                
    rqsql.setAltOrderFieldNames("n.nomor_nasabah")
    rqsql.keyFieldName = "n.nomor_nasabah"
    rqsql.setBaseOrderFieldNames("n.nomor_nasabah")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_nasabah'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_nasabah'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'tipe_nasabah'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
  #--
  
class lookupNasabahPlus:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_nasabah
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    sSQL = '''
      SELECT distinct
        n.nomor_nasabah, 
        decode(n.jenis_nasabah,'I','Individu','K','Korporat','') tipe_nasabah,
        n.nama_nasabah, nvl(c.os_pokok,0) as Limit_Nom_Tarik_Tunai
        , nvl(c.jml_account,0) jml_account  
      FROM %(nasabah)s n
        left join (
      		select 
      			rc.NOMOR_NASABAH, sum(decode(rt.status_rekening,1,1,0)) jml_account, 
      			sum(nvl(decode(fa.contracttype_code,'I',nilai_kafalah,fa.dropping_amount),0)) plafond,
      			sum(round(
      				case 
      					when fa.contracttype_code in ('F') then 
      						(fa.dropping_amount - nvl(pai.principal_payment_ijr,0)) 
      					when fa.contracttype_code in ('I') then 
      						decode(RT.STATUS_REKENING,1,nilai_kafalah,0) 
      					when fa.contracttype_code in ('A','H') then 
      						-(rt.saldo + fa.arrear_balance + mrb.mmd_balance + fa.profit_arrear_balance) 
      					else 
      						fa.dropping_amount 
      				end
      			,2 )) as os_pokok,
      			count(1) jml_nsb
      		from %(rekeningtransaksi)s rt
      			INNER join finaccount fa on rt.NOMOR_REKENING=fa.NOMOR_REKENING
      			LEFT JOIN finmurabahahaccount mrb ON fa.nomor_rekening=mrb.nomor_rekening
      			LEFT JOIN (
      				select id_schedule,sum(principal_amount) as principal_payment_ijr,sum(realized_margin) as profit_payment_ijr 
      				from finpaymentscheddetail where seq_number>0 and realized='T'
      				group by id_schedule
      			) pai on pai.id_schedule=fa.id_schedule   	
      			LEFT JOIN kafalah kaf ON fa.nomor_rekening=kaf.nomor_rekening
      			inner join %(rekeningcustomer)s rc on fa.NOMOR_REKENING=rc.NOMOR_REKENING
      		where fa.contracttype_code <>'W' --and RT.STATUS_REKENING=1
      		group by rc.nomor_nasabah
      	) c on n.nomor_nasabah=c.nomor_nasabah	        
        
    ''' % dbutil.mapDBTableNames(config, ['core.nasabah', 'core.rekeningcustomer', 'core.rekeningtransaksi', 'finaccount'])
    
    rqsql.SELECTFROMClause = sSQL
    wSQL = '''
      (upper(n.nomor_nasabah) LIKE upper(%(nomor_nasabah)s) OR upper(n.nama_nasabah) LIKE upper(%(nomor_nasabah)s) )
    ''' %  {
      'nomor_nasabah':  mlu.QuotedStr('%%%s%%' % self.key_value)
    }
    #raise Exception, sSQL+wSQL
    rqsql.WHEREClause = wSQL                                
    rqsql.setAltOrderFieldNames("n.nomor_nasabah")
    rqsql.keyFieldName = "n.nomor_nasabah"
    rqsql.setBaseOrderFieldNames("n.nomor_nasabah")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_nasabah'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_nasabah'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'tipe_nasabah'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'Limit_Nom_Tarik_Tunai'
            Title.Caption = 'Expossure'
            Visible = True
            Width = 120
          end
          item
            FieldName = 'jml_account'
            Title.Caption = 'Jml Rek. Pembiayaan'
            Visible = True
            Width = 100
          end
          >
      end
    '''
  #--  #--
  #--
  
class lookupGroup:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.group_name
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    sSQL = '''
      SELECT distinct
        n.* 
      FROM %(fincustgroups)s n
    ''' % dbutil.mapDBTableNames(config, ['fincustgroups'])
    
    rqsql.SELECTFROMClause = sSQL
    wSQL = '''
      upper(n.group_name) LIKE upper(%(group_name)s)
    ''' %  {
      'group_name':  mlu.QuotedStr('%%%s%%' % self.key_value)
    }
    #raise Exception, sSQL+wSQL
    rqsql.WHEREClause = wSQL                                
    rqsql.setAltOrderFieldNames("n.group_name")
    rqsql.keyFieldName = "n.group_name"
    rqsql.setBaseOrderFieldNames("n.group_name")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'group_name'
            Title.Caption = 'Nama Group'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'group_description'
            Title.Caption = 'Keterangan'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'group_parent'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
  #--
  
   