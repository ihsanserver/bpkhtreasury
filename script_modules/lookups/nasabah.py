import sys
import os
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.dbutil as dbutil

class lookup_nasabah:
  def __init__(self, config, fr):
    self.config = config
    self.nomor_nasabah = fr.nomor_nasabah
    self.nama_nasabah = fr.nama_nasabah
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    rqsql.SELECTFROMClause = '''
    select distinct
    cna.*
    
    from finaccount fa
    inner join {rekeningcustomer} crc on crc.nomor_rekening = fa.nomor_rekening
    left join fincustadditionaldata cad on cad.nomor_nasabah = crc.nomor_nasabah
    left join (select * from fincollateralaccount fca
                inner join fincollateralasset cas on fca.norek_fincollateralasset = cas.nomor_rekening)
                fas on fas.norek_finaccount = fa.nomor_rekening
    inner join {rekeningtransaksi} crt on crt.nomor_rekening = fa.nomor_rekening
    inner join {nasabah} cna on cna.nomor_nasabah = crc.nomor_nasabah
    inner join {nasabahindividu} cni on cni.nomor_nasabah = crc.nomor_nasabah
    inner join {individu} ci on ci.id_individu = cni.id_individu
    left join {enum_int} esa on esa.enum_value = crt.status_rekening and esa.enum_name = 'eStatusRekening'
    left join enum_varchar eat on eat.enum_value = fas.asset_type and eat.enum_name = 'eColAssetType'
    left join (select rd.* from {referencedata} rd
               inner join {referencetype} rt on rt.reftype_id = rd.reftype_id
               where upper(rt.reference_name) = upper('ref_sektorusaha'))
               erd on erd.refdata_id = fa.sektor_usaha
    '''.format(**{
      'nasabah': config.MapDBTableName('core.nasabah'),
      'nasabahindividu': config.MapDBTableName('core.nasabahindividu'),
      'rekeningcustomer': config.MapDBTableName('core.rekeningcustomer'),
      'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
      'individu': config.MapDBTableName('core.individu'),
      'enum_int': config.MapDBTableName('core.enum_int'),
      'referencedata': config.MapDBTableName('enterprise.referencedata'),
      'referencetype': config.MapDBTableName('enterprise.referencetype')
    })
    rqsql.WHEREClause = '''
    (
      UPPER(cna.nomor_nasabah) LIKE '%%%(nomor_nasabah)s%%'
      OR UPPER(cna.nama_nasabah) LIKE '%%%(nomor_nasabah)s%%'
    )
    ''' % {
      'nomor_nasabah': str(self.nomor_nasabah).upper()
    }
    #--
    
    #rqsql.GROUPBYClause = "GROUP BY acc.account_code, acc.account_name, acc.account_type" 
    rqsql.setAltOrderFieldNames("cna.nomor_nasabah;cna.nama_nasabah")
    rqsql.keyFieldName = "cna.nomor_nasabah"
    rqsql.setBaseOrderFieldNames("cna.nomor_nasabah")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_nasabah'
            Title.Caption = 'Nomor Nasabah'
            Width = 70
          end
          item
            FieldName = 'nama_nasabah'
            Title.Caption = 'Nama Nasabah'
            Width = 200
          end
          item
            FieldName = 'alamat_surat_jalan'
            Title.Caption = 'Alamat'
            Width = 400
          end
          >
      end
    '''
  #--
#--