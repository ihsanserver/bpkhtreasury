import os
import sys

class lookupJenisTreasury:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.kode_jenis   
        
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils

    rqsql.SELECTFROMClause = '''
      SELECT * FROM %(jenistreasury)s
    ''' % {
      'jenistreasury': config.MapDBTableName("jenistreasury")
    }
    rqsql.WHEREClause = '''
      ( upper(keterangan) LIKE upper(%(key_value)s) OR upper(kode_jenis) LIKE upper(%(key_value)s))
    ''' %  {
      'key_value': mlu.QuotedStr('%%%s%%' % self.key_value)
    }                                
    
    rqsql.setAltOrderFieldNames("kode_jenis")
    rqsql.keyFieldName = "kode_jenis"
    rqsql.setBaseOrderFieldNames("kode_jenis")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'jenis_treasuryaccount'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'kode_jenis'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'keterangan'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
  
class lookupJenisTreasuryAccrual:
  def __init__(self, config, fr):
    self.config = config
    self.jenis_treasuryaccount = fr.jenis_treasuryaccount
    self.key_value = fr.kode_jenis   
        
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils

    rqsql.SELECTFROMClause = '''
      SELECT * FROM %(jenistreasury)s
    ''' % {
      'jenistreasury': config.MapDBTableName("jenistreasury")
    }
    rqsql.WHEREClause = '''
       jenis_treasuryaccount = '%(jenis_treasuryaccount)s' AND
      ( upper(keterangan) LIKE upper(%(key_value)s) OR upper(kode_jenis) LIKE upper(%(key_value)s))
    ''' %  {
      'key_value': mlu.QuotedStr('%%%s%%' % self.key_value),
      'jenis_treasuryaccount': self.jenis_treasuryaccount
    }                                
    
    rqsql.setAltOrderFieldNames("kode_jenis")
    rqsql.keyFieldName = "kode_jenis"
    rqsql.setBaseOrderFieldNames("kode_jenis")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'jenis_treasuryaccount'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'kode_jenis'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'keterangan'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
  
class lookupTreaProdukAccrual:
  def __init__(self, config, fr):
    self.config = config
    self.tipe_fasbis         = fr.tipe_fasbis
    self.jenis_treasuryaccount = fr.jenis_treasuryaccount
    self.keyword               = fr.kode_produk
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    rqsql.SELECTFROMClause = '''
    SELECT
      p.*
    FROM %(treaproduk)s p
    ''' % {
      'treaproduk': config.MapDBTableName('treaproduk')
    }
    rqsql.WHEREClause = '''
    p.jenis_treasuryaccount = '%(jenis_treasuryaccount)s' 
    AND p.tipe_fasbis='%(tipe_fasbis)s'
    AND ( UPPER(p.kode_produk) LIKE '%%%(keyword)s%%' OR UPPER(p.nama_produk) LIKE '%%%(keyword)s%%' )
    ''' % {
      'keyword': str(self.keyword).upper()
      ,'jenis_treasuryaccount' : self.jenis_treasuryaccount
      ,'tipe_fasbis' : self.tipe_fasbis
    }
    #-- 
    
    #rqsql.GROUPBYClause = "GROUP BY acc.account_code, acc.account_name, acc.account_type" 
    rqsql.setAltOrderFieldNames("kode_produk")
    rqsql.keyFieldName = "kode_produk"
    rqsql.setBaseOrderFieldNames("kode_produk")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_produk'
            Title.Caption = 'Kode Produk'
            Width = 70
          end
          item
            FieldName = 'nama_produk'
            Title.Caption = 'Nama Produk'
            Width = 250
          end
          item
            FieldName = 'basis_hari_pertahun'
            Title.Caption = 'Basis Hari Pertahun'
            Width = 90
          end
          item
            FieldName = 'tipe_counterpart'
            visible = False
          end
          item
            FieldName = 'tipe_fasbis'
            visible = False
          end
          >
      end
    '''
  #--
#--
  
class lookupTreaProduk:
  def __init__(self, config, fr):
    self.config = config
    self.tipe_treasury         = fr.tipe_treasury
    self.jenis_treasuryaccount = fr.jenis_treasuryaccount
    self.keyword               = fr.kode_produk
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    rqsql.SELECTFROMClause = '''
    SELECT
      p.*
    FROM %(treaproduk)s p
    ''' % {
      'treaproduk': config.MapDBTableName('treaproduk')
    }
    rqsql.WHEREClause = '''
    p.jenis_treasuryaccount = '%(jenis_treasuryaccount)s' 
    AND p.tipe_treasury='%(tipe_treasury)s'
    AND ( UPPER(p.kode_produk) LIKE '%%%(keyword)s%%' OR UPPER(p.nama_produk) LIKE '%%%(keyword)s%%' )
    ''' % {
      'keyword': str(self.keyword).upper()
      ,'jenis_treasuryaccount' : self.jenis_treasuryaccount
      ,'tipe_treasury' : self.tipe_treasury
    }
    #-- 
    #raise Exception, rqsql.WHEREClause
    #rqsql.GROUPBYClause = "GROUP BY acc.account_code, acc.account_name, acc.account_type" 
    rqsql.setAltOrderFieldNames("kode_produk")
    rqsql.keyFieldName = "kode_produk"
    rqsql.setBaseOrderFieldNames("kode_produk")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_produk'
            Title.Caption = 'Kode Produk'
            Width = 70
          end
          item
            FieldName = 'nama_produk'
            Title.Caption = 'Nama Produk'
            Width = 250
          end
          item
            FieldName = 'basis_hari_pertahun'
            Title.Caption = 'Basis Hari Pertahun'
            Width = 90
          end
          item
            FieldName = 'tipe_counterpart'
            visible = False
          end
          item
            FieldName = 'tipe_fasbis'
            visible = False
          end
          >
      end
    '''
  #--
#--

class lookupAllTreaProduk:
  def __init__(self, config, fr):
    self.config = config
    self.jenis_treasuryaccount = fr.jenis_treasuryaccount
    self.keyword = fr.kode_produk
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    rqsql.SELECTFROMClause = '''
    SELECT
      p.*
    FROM %(treaproduk)s p
    ''' % {
      'treaproduk': config.MapDBTableName('treaproduk')
    }
    rqsql.WHEREClause = '''
    p.jenis_treasuryaccount = '%(jenis_treasuryaccount)s'
    AND ( UPPER(p.kode_produk) LIKE '%%%(keyword)s%%' OR UPPER(p.nama_produk) LIKE '%%%(keyword)s%%' )
    ''' % {
      'keyword': str(self.keyword).upper()
      ,'jenis_treasuryaccount' : self.jenis_treasuryaccount
    }
    #--
    
    #rqsql.GROUPBYClause = "GROUP BY acc.account_code, acc.account_name, acc.account_type" 
    rqsql.setAltOrderFieldNames("kode_produk")
    rqsql.keyFieldName = "kode_produk"
    rqsql.setBaseOrderFieldNames("kode_produk")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_produk'
            Title.Caption = 'Kode Produk'
            Width = 70
          end
          item
            FieldName = 'nama_produk'
            Title.Caption = 'Nama Produk'
            Width = 250
          end
          item
            FieldName = 'jenis_treasuryaccount'
            Title.Caption = 'Jenis Treasury'
            Width = 80
          end
          item
            FieldName = 'basis_hari_pertahun'
            Title.Caption = 'Basis Hari Pertahun'
            Width = 90
          end
          item
            FieldName = 'tipe_counterpart'
            visible = False
          end
          item
            FieldName = 'tipe_fasbis'
            visible = False
          end
          >
      end
    '''
  #--
#--

class lookupCounterpart:
  def __init__(self, config, fr):
    self.config = config
    self.tipe_counterpart = fr.tipe_counterpart
    self.keyword          = fr.kode_counterpart
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    rqsql.SELECTFROMClause = '''
    SELECT
      p.*
    FROM treacounterpart p
    ''' 
    rqsql.WHEREClause = '''
    p.tipe_counterpart = '%(tipe_counterpart)s'
    AND p.status = 'T'
    AND ( UPPER(p.kode_counterpart) LIKE '%%%(keyword)s%%' OR UPPER(p.nama_counterpart) LIKE '%%%(keyword)s%%' )
    ''' % {
      'keyword': str(self.keyword).upper()
      ,'tipe_counterpart' : self.tipe_counterpart
    }
    #--
    
    #rqsql.GROUPBYClause = "GROUP BY acc.account_code, acc.account_name, acc.account_type" 
    rqsql.setAltOrderFieldNames("kode_counterpart")
    rqsql.keyFieldName = "kode_counterpart"
    rqsql.setBaseOrderFieldNames("kode_counterpart")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_counterpart'
            Title.Caption = 'Kode Counterpart'
            Width = 70
          end
          item
            FieldName = 'nama_counterpart'
            Title.Caption = 'Nama Counterpart'
            Width = 250
          end
          >
      end
    '''
  #--
#--

class lookupGLInterface:
  def __init__(self, config, fr):
    self.config = config
    self.kode_produk = fr.kode_produk
    self.keyword     = fr.kode_interface
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    rqsql.SELECTFROMClause = '''
    SELECT
      a.kode_interface, a.kode_produk, a.kode_account
      , c.account_name
      , b.deskripsi
    FROM glinterface a
    inner join detiltransaksiclass b on a.kode_interface=b.kode_tx_class
    inner join account c on a.kode_account=c.account_code
    ''' 
    rqsql.WHEREClause = '''
    a.kode_produk = '%(kode_produk)s'
    AND ( UPPER(a.kode_interface) LIKE '%%%(keyword)s%%' OR UPPER(b.deskripsi) LIKE '%%%(keyword)s%%' )
    ''' % {
      'keyword': str(self.keyword).upper()
      ,'kode_produk' : self.kode_produk
    }
    #--
    
    #rqsql.GROUPBYClause = "GROUP BY acc.account_code, acc.account_name, acc.account_type" 
    rqsql.setAltOrderFieldNames("kode_interface")
    rqsql.keyFieldName = "kode_interface"
    rqsql.setBaseOrderFieldNames("kode_interface")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_interface'
            Title.Caption = 'Kode Interface'
            Width = 70
          end
          item
            FieldName = 'deskripsi'
            Title.Caption = 'Deskripsi'
            Width = 180
          end
          item
            FieldName = 'kode_account'
            Title.Caption = 'Kode Account'
            Width = 90
          end
          item
            FieldName = 'account_name'
            Title.Caption = 'Nama Account GL'
            Width = 270
          end
          >
      end
    '''
  #--
#--

class lookupAllCounterpart:
  def __init__(self, config, fr):
    self.config = config
    self.keyword = fr.kode_counterpart
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    rqsql.SELECTFROMClause = '''
    SELECT
      p.*
    FROM treacounterpart p
    ''' 
    rqsql.WHEREClause = '''
    p.status = 'T'
    AND ( UPPER(p.kode_counterpart) LIKE '%%%(keyword)s%%' OR UPPER(p.nama_counterpart) LIKE '%%%(keyword)s%%' )
    ''' % {
      'keyword': str(self.keyword).upper()
    }
    #--
    
    #rqsql.GROUPBYClause = "GROUP BY acc.account_code, acc.account_name, acc.account_type" 
    rqsql.setAltOrderFieldNames("kode_counterpart")
    rqsql.keyFieldName = "kode_counterpart"
    rqsql.setBaseOrderFieldNames("kode_counterpart")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_counterpart'
            Title.Caption = 'Kode Counterpart'
            Width = 70
          end
          item
            FieldName = 'nama_counterpart'
            Title.Caption = 'Nama Counterpart'
            Width = 250
          end
          >
      end
    '''
  #--
#--

class lookupMemberRTGS:
  def __init__(self, config, fr):
    self.config = config
    self.keyword = fr.kode_member
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    rqsql.SELECTFROMClause = '''
    SELECT
      kode_member, nama_member
    FROM %(memberRTGS)s
    ''' % {                                        
      'memberRTGS': config.MapDBTableName("core.MemberRTGS")
    }
    rqsql.WHEREClause = '''
    status = 'A' AND ( UPPER(kode_member) LIKE '%%%(keyword)s%%' OR UPPER(nama_member) LIKE '%%%(keyword)s%%' )
    ''' % {                                        
      'keyword': str(self.keyword).upper()
    }
    #--
    
    #rqsql.GROUPBYClause = "GROUP BY acc.account_code, acc.account_name, acc.account_type" 
    rqsql.setAltOrderFieldNames("kode_member")
    rqsql.keyFieldName = "kode_member"
    rqsql.setBaseOrderFieldNames("kode_member")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_member'
            Title.Caption = 'Kode Counterpart'
            Width = 70
          end
          item
            FieldName = 'nama_member'
            Title.Caption = 'Nama Counterpart'
            Width = 250
          end
          >
      end
    '''
  #--
#--

class lookupCurrency:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.Currency_Code
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT 
        Currency_Code,
        Description
      FROM %(Currency)s c
    ''' % {
      'Currency': config.MapDBTableName("core.Currency")
    }
    rqsql.WHEREClause = '''
      upper(Currency_Code) LIKE upper(%s) or upper(Description) LIKE upper(%s)  
    ''' % (
        mlu.QuotedStr('%%%s%%' % self.key_value),
        mlu.QuotedStr('%%%s%%' % self.key_value)
      )
                                    
    rqsql.setAltOrderFieldNames("Currency_Code")
    rqsql.keyFieldName = "Currency_Code"
    rqsql.setBaseOrderFieldNames("Currency_Code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'currency_code'
            Title.Caption = 'Kode Currency'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'Description'
            Title.Caption = 'Nama Currency'
            Visible = True
            Width = 150
          end
          >
      end
    '''
  #-- lookup Currency
 
class lookupCabang:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.kode_cabang    
    
    sSQL = '''
      SELECT b.tipe_akses_cabang FROM %(userapp)s b WHERE b.id_user='%(id_user)s' 
    ''' % {
      'userapp': config.MapDBTableName("enterprise.userapp"),
      'id_user':config.SecurityContext.InitUser
    }
    rSQL = config.CreateSQL(sSQL).RawResult
    self.tipe_akses = rSQL.tipe_akses_cabang
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils

    sJoinType = 'INNER JOIN'
    if self.tipe_akses == 'S': sJoinType = 'LEFT JOIN'

    rqsql.SELECTFROMClause = '''
      SELECT 
        c.kode_cabang,
        c.nama_cabang
      FROM %(cabang)s c
      %(sJoinType)s %(list_cabang_diizinkan)s lc ON lc.kode_cabang = c.kode_cabang AND lc.id_user = %(id_user)s
    ''' % {
      'list_cabang_diizinkan': config.MapDBTableName("enterprise.ListCabangDiizinkan"),
      'cabang': config.MapDBTableName("enterprise.Cabang"),'sJoinType':sJoinType,
      'id_user': mlu.QuotedStr(config.SecurityContext.InitUser)
    }
    rqsql.WHEREClause = '''
      ( upper(c.kode_cabang) LIKE upper(%(kode_cabang)s) OR upper(c.nama_cabang) LIKE upper(%(kode_cabang)s) )
      AND c.status_aktif = 'T' 
    ''' %  {
      'kode_cabang': mlu.QuotedStr('%%%s%%' % self.key_value)
    }                                
    rqsql.setAltOrderFieldNames("c.kode_cabang")
    rqsql.keyFieldName = "c.kode_cabang"
    rqsql.setBaseOrderFieldNames("c.kode_cabang")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_cabang'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'nama_cabang'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
  
 