import os
import sys
#import rpdb2; rpdb2.start_embedded_debugger("000")

class lookupProject:
  def __init__(self, config, fr):
    self.config = config
    self.account_type = fr.account_type
    self.project_no = fr.project_no or ''
    
  def initQueryObject(self, rqsql):
    mlu = self.config.ModLibUtils
    if self.account_type in  ['I','X'] :
      rqsql.SELECTFROMClause = '''SELECT
        p.project_no, p.name, p.description
        FROM Project p'''
      rqsql.WHEREClause = ''' 
         lower(p.project_no) like '%{project_no}%'
         or lower(p.name) like '%{project_no}%'
         or lower(p.description) like '%{project_no}%'
         '''.format(project_no = self.project_no.lower())
      rqsql.GROUPBYClause = " "
      rqsql.setAltOrderFieldNames("project_no;name")
      rqsql.keyFieldName = "project_no"
      rqsql.setBaseOrderFieldNames("project_no")
    else :
      rqsql.SELECTFROMClause = '''SELECT
        '0000' as project_no, 'DEFAULT' as name, 'DEFAULT' as description
        FROM Dual'''
      rqsql.WHEREClause = '''1 = 1'''
      rqsql.GROUPBYClause = " "
      rqsql.setAltOrderFieldNames("project_no;name")
      rqsql.keyFieldName = "project_no"
      rqsql.setBaseOrderFieldNames("project_no")
      
    rqsql.columnSetting = '''
    object TColumnsWrapper
      Columns = <
        item
          Expanded = False
          FieldName = 'project_no'
          Title.Caption = 'Kode RC'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'name'
          Title.Caption = 'Nama RC'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'description'
          Title.Caption = 'Keterangan'
          Width = 200
          Visible = True
        end
      >
    end
  '''
      
  #--
#-- class lookupBranch


class lookupGLAccount:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.account_code
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT 
        account_code,
        account_name
      FROM %(Account)s c
    ''' % {
      'Account': config.MapDBTableName("core.Account")
    }
    rqsql.WHEREClause = '''
      c.is_detail='T' AND
      (upper(account_code) LIKE upper(%s) or upper(account_name) LIKE upper(%s))  
    ''' % (
        mlu.QuotedStr('%%%s%%' % self.key_value),
        mlu.QuotedStr('%%%s%%' % self.key_value)
      )
                                    
    rqsql.setAltOrderFieldNames("account_code")
    rqsql.keyFieldName = "account_code"
    rqsql.setBaseOrderFieldNames("account_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'account_code'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'account_name'
            Visible = True
            Width = 200
          end
          >
      end
    '''
  #-- lookup lookupGLAccount

class lookupSandi:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.kode_1
    self.kategori = fr.kategori.upper()
    #raise Exception, self.kategori
    
  def initQueryObject(self, rqsql):
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
       SELECT   
        kode_1 as Sandi,
        keterangan_1 as Keterangan,
        id, kode_1, keterangan_1
       FROM Sandi
      '''
    rqsql.WHEREClause = ''' kategori=%s and 
          ( upper(kode_1) LIKE upper(%s) or upper(keterangan_1) LIKE upper(%s) ) ''' % (
        mlu.QuotedStr(self.kategori),
        mlu.QuotedStr('%%%s%%' % self.key_value),
        mlu.QuotedStr('%%%s%%' % self.key_value)
      )
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("kode_1;keterangan_1")
    rqsql.keyFieldName = "kode_1"
    rqsql.setBaseOrderFieldNames("kode_1")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'sandi'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'keterangan'
            Visible = True
            Width = 200
          end
          >
      end
    '''
  #--
#--  lookupSandi

class lookupSandi_2:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.kode_1
    self.kategori = fr.kategori.upper()
    self.lkode = fr.lkode.upper()
    
     
  def initQueryObject(self, rqsql):
    mlu = self.config.ModLibUtils
    where = ''' kategori=%s and 
          ( upper(kode_1) LIKE upper(%s) or upper(keterangan_1) LIKE upper(%s) ) 
          %s ''' % (
        mlu.QuotedStr(self.kategori),
        mlu.QuotedStr('%%%s%%' % self.key_value),
        mlu.QuotedStr('%%%s%%' % self.key_value),
        self.lkode
      ) 
      
    rqsql.SELECTFROMClause = '''
       SELECT   
        kode_1 as Sandi,
        keterangan_1 as Keterangan,
        id, kode_1, keterangan_1
       FROM Sandi
      '''
    rqsql.WHEREClause = where
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("kode_1;keterangan_1")
    rqsql.keyFieldName = "kode_1"
    rqsql.setBaseOrderFieldNames("kode_1")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'sandi'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'keterangan'
            Visible = True
            Width = 200
          end
          >
      end
    '''
  #--
#--  lookupSandi

  
class lookupFinAccount:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
       SELECT   
        a.nomor_rekening,
        b.nama_rekening,
        a.product_code,
        a.contracttype_code,
        c.description
       FROM %(FA)s a
       LEFT OUTER JOIN %(RT)s b ON (a.nomor_rekening=b.nomor_rekening)
       LEFT OUTER JOIN %(CT)s c ON (a.contracttype_code=c.contracttype_code)
      '''  % {
           'FA': config.MapDBTableName("FinAccount"),
           'RT': config.MapDBTableName("core.RekeningTransaksi"),
           'CT': config.MapDBTableName("ContractType")
      }
                                                     
    rqsql.WHEREClause = '''
      a.contracttype_code not in ('W')
      AND ( upper(a.nomor_rekening) LIKE upper(%s) 
      OR upper(b.nama_rekening) LIKE upper(%s) )  
    ''' % (
        mlu.QuotedStr('%%%s%%' % self.key_value),
        mlu.QuotedStr('%%%s%%' % self.key_value)
      )
                                    
    rqsql.setAltOrderFieldNames("nomor_rekening")
    rqsql.keyFieldName = "nomor_rekening"
    rqsql.setBaseOrderFieldNames("nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Visible = True
            Width = 100
          end
          item
            FieldName = 'nama_rekening'
            Title.Caption = 'Nama Nasabah'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'description'
            Title.Caption = 'Akad'
            Visible = True
            Width = 100
          end
          item
            FieldName = 'product_code'
            Title.Caption = 'Kode Produk'
            Visible = True
            Width = 90
          end
          >
      end
    '''
  #-- lookup FinAppraisalAgent
  
class lookupAM:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.AgentCode
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT 
        AgentCode,
        AgentName
      FROM %(AM)s c
    ''' % {
      'AM': config.MapDBTableName("core.FundingAgent")
    }
    rqsql.WHEREClause = '''
      statusactive = 'T' and 
      ( upper(AgentCode) LIKE upper(%s) or upper(AgentName) LIKE upper(%s) )  
    ''' % (
        mlu.QuotedStr('%%%s%%' % self.key_value),
        mlu.QuotedStr('%%%s%%' % self.key_value)
      )
                                    
    rqsql.setAltOrderFieldNames("AgentName;AgentCode")
    rqsql.keyFieldName = "AgentCode"
    rqsql.setBaseOrderFieldNames("AgentName;AgentCode")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'AgentCode'
            Title.Caption = 'Kode AO'
            Visible = True
            Width = 100
          end
          item
            FieldName = 'AgentName'
            Title.Caption = 'Nama AO'
            Visible = True
            Width = 150
          end
          >
      end
    '''
  #-- lookup FinAppraisalAgent
  
class lookupAM_am:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.AMCode
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT 
        AMCode,
        EmployeeName
      FROM %(AM)s c
    ''' % {
      'AM': config.MapDBTableName("core.AccountManager")
    }
    rqsql.WHEREClause = '''
      upper(AMCode) LIKE upper(%s) or upper(EmployeeName) LIKE upper(%s)  
    ''' % (
        mlu.QuotedStr('%%%s%%' % self.key_value),
        mlu.QuotedStr('%%%s%%' % self.key_value)
      )
                                    
    rqsql.setAltOrderFieldNames("EmployeeName;AMCode")
    rqsql.keyFieldName = "AMCode"
    rqsql.setBaseOrderFieldNames("EmployeeName;AMCode")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'AMCode'
            Title.Caption = 'Kode AO'
            Visible = True
            Width = 100
          end
          item
            FieldName = 'EmployeeName'
            Title.Caption = 'Nama AO'
            Visible = True
            Width = 150
          end
          >
      end
    '''
  #-- lookup FinAppraisalAgent
  
class lookupTreaAccount:
  def __init__(self, config, fr):
    self.config = config
    self.kode_jenis = fr.kode_jenis
    self.key_value = fr.nomor_rekening
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT 
        a.nomor_rekening, b.nama_rekening
        , a.kode_produk, a.kode_counterpart
        , b.kode_cabang, b.kode_valuta
        , c.kode_instrumen
        , d.nama_counterpart 
        , b.saldo
        , c.tgl_settlement
      FROM %(treasuryaccount)s a
        INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
        LEFT JOIN %(treacounterpart)s d ON a.kode_counterpart=d.kode_counterpart
        left JOIN %(treasuratberharga)s c ON a.nomor_rekening=c.nomor_rekening
      '''  % {
           'treasuryaccount': config.MapDBTableName("treasuryaccount"),
           'rekeningtransaksi': config.MapDBTableName("core.rekeningtransaksi"),
           'treacounterpart': config.MapDBTableName("treacounterpart"),
           'treasuratberharga': config.MapDBTableName("treasuratberharga")
      }

    rqsql.WHEREClause = '''
      b.kode_jenis = %(kode_jenis)s
      AND ( 
        upper(a.nomor_rekening) LIKE upper(%(key_value)s) 
        OR upper(b.nama_rekening) LIKE upper(%(key_value)s) 
        OR upper(c.kode_instrumen) LIKE upper(%(key_value)s) 
      )  
    ''' % {
        'kode_jenis': mlu.QuotedStr('%s' % self.kode_jenis),
        'key_value': mlu.QuotedStr('%%%s%%' % self.key_value)
      }
                                    
    #raise Exception, rqsql.WHEREClause
    rqsql.setAltOrderFieldNames("nomor_rekening")
    rqsql.keyFieldName = "nomor_rekening"
    rqsql.setBaseOrderFieldNames("nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekening'
            Title.Caption = 'Generated Key'
            Visible = True
            Width = 100
          end
          item
            FieldName = 'nama_rekening'
            Title.Caption = 'Nama Rekening'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'nama_counterpart'
            Title.Caption = 'Counterpart'
            Visible = True
            Width = 100
          end
          item
            FieldName = 'kode_instrumen'
            Title.Caption = 'Kode Instrumen'
            Visible = True
            Width = 100
          end
          item
            FieldName = 'kode_produk'
            Title.Caption = 'Kode Produk'
            Visible = True
            Width = 90
          end
          item
            FieldName = 'kode_cabang'
            Title.Caption = 'Kode Cabang'
            Visible = True
            Width = 90
          end
          item
            FieldName = 'kode_valuta'
            Title.Caption = 'Kode Valuta'
            Visible = True
            Width = 90
          end
          item
            FieldName = 'saldo'
            Title.Caption = 'Saldo'
            Visible = True
            Width = 90
          end
          item
            FieldName = 'tgl_settlement'
            Title.Caption = 'Tgl. Settlement'
            Visible = True
            Width = 90
          end
          >
      end
    '''
  #-- lookup FinAppraisalAgent
  
 