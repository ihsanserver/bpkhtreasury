import os
import sys

class lookupParameter:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.kode_entri
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT 
        c.kode_entri,
        c.nama_entri
      FROM %(OtorParameter)s
    ''' % {
      'OtorParameter': config.MapDBTableName("OtorParameter")
    }
    rqsql.WHEREClause = '''
      c.kode_entri LIKE %(kode_entri)s 
    ''' %  {
      'kode_entri': mlu.QuotedStr('%s%%' % self.key_value)
    }                                
    rqsql.setAltOrderFieldNames("kode_entri")
    rqsql.keyFieldName = "kode_entri"
    rqsql.setBaseOrderFieldNames("kode_entri")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_entri'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_entri'
            Visible = True
            Width = 150
          end
          >
      end
    '''
  #--