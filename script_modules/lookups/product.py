import os
import sys
import com.ihsan.util.dbutil as dbutil
#import rpdb2; rpdb2.start_embedded_debugger("000")

class lookupFinGroups:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.group_code
    #self.contract_type = fr.contract_type
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    sSQL = ''' SELECT * FROM %(FinCustGroups)s ''' % dbutil.mapDBTableNames(config, ['financing.FinCustGroups'])
    rqsql.SELECTFROMClause = sSQL
    rqsql.WHEREClause = '''
       ( upper(group_code) LIKE upper(%s) OR upper(group_name) LIKE  upper(%s) )
    ''' % (mlu.QuotedStr('%%%s%%' % self.key), mlu.QuotedStr('%%%s%%' % self.key))

    #--
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("group_code;group_name")
    rqsql.keyFieldName = "group_code"
    rqsql.setBaseOrderFieldNames("group_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'group_code'
            Title.Caption = 'Kode Group'
            Visible = True
            Width = 90
          end
          item
            FieldName = 'group_name'
            Title.Caption = 'Nama Group'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'group_description'
            Title.Caption = 'Keterangan'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'group_parent'
            Title.Caption = 'CIF Parent'
            Visible = True
            Width = 110
          end
          item
            FieldName = 'cust_group_id'
            Visible = False
            Width = 110
          end
          >
      end
    '''
  #--
#-- lookupFinGroups

class lookupProduct_cfd:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.product_code
    self.contract_type = fr.contract_type
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    dParams = {
      'finproduct':config.MapDBTableName("financing.finproduct"), 
      'confidentialaccess':config.MapDBTableName("core.confidentialaccess"), 
      'listperanuser':config.MapDBTableName("enterprise.listperanuser"), 
      'id_user': mlu.QuotedStr(config.SecurityContext.InitUser),
    }
    sSQL = '''SELECT
      pr.product_code, pr.product_name, pr.currency_code, pr.special_flag, pr.kode_pof, pr.contracttype_code,
      pr.targeted_eqv_rate, pr.penalty_type, 
      nvl(pr.is_restricted,'F') is_restricted,
      nvl(pr.id_cfgroup,0) id_cfgroup

      ,amount_limit_max, amount_limit_min, default_period_count,
      min_eqv_rate,max_eqv_rate,min_period_count,max_period_count
      ,repricing_period_min, repricing_period_max
      FROM %(finproduct)s pr
      	LEFT JOIN %(confidentialaccess)s ca ON pr.id_cfgroup=ca.id_cfgroup AND ca.id_peran in (SELECT id_peran FROM %(listperanuser)s WHERE id_user=%(id_user)s)
      ''' % dParams
    rqsql.SELECTFROMClause = sSQL
    rqsql.WHEREClause = '''
       special_flag is null AND status_product = 'T' 
       AND ( upper(product_code) LIKE upper(%s) OR upper(product_name) LIKE  upper(%s) )
       AND (nvl(pr.id_cfgroup,0)=0 or nvl(ca.id_cfgroup,0)>0)
    ''' % (mlu.QuotedStr('%%%s%%' % self.key), mlu.QuotedStr('%%%s%%' % self.key))

    if self.contract_type not in ['','ALL']:
      rqsql.WHEREClause += ''' AND contracttype_code = '%s' ''' % self.contract_type
    #--
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("product_code;product_name")
    rqsql.keyFieldName = "product_code"
    rqsql.setBaseOrderFieldNames("product_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'product_code'
            Title.Caption = 'Kode Produk'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'product_name'
            Title.Caption = 'Nama Produk'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
#-- lookupCurrency

class lookupProduct:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.product_code
    self.contract_type = fr.contract_type
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    sSQL = '''SELECT
      product_code, product_name, currency_code, special_flag, kode_pof, contracttype_code,
      targeted_eqv_rate, penalty_type, 
      nvl(is_restricted,'F') is_restricted,
      nvl(id_cfgroup,0) id_cfgroup

      ,amount_limit_max, amount_limit_min, default_period_count,
      min_eqv_rate,max_eqv_rate,min_period_count,max_period_count
      ,repricing_period_min, repricing_period_max
      FROM %(FinProduct)s
      ''' % dbutil.mapDBTableNames(config, ['financing.FinProduct'])
    rqsql.SELECTFROMClause = sSQL
    rqsql.WHEREClause = '''
       special_flag is null AND status_product = 'T' 
       AND ( upper(product_code) LIKE upper(%s) OR upper(product_name) LIKE  upper(%s) )
    ''' % (mlu.QuotedStr('%%%s%%' % self.key), mlu.QuotedStr('%%%s%%' % self.key))

    if self.contract_type not in ['','ALL']:
      rqsql.WHEREClause += ''' AND contracttype_code = '%s' ''' % self.contract_type
    #--
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("product_code;product_name")
    rqsql.keyFieldName = "product_code"
    rqsql.setBaseOrderFieldNames("product_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'product_code'
            Title.Caption = 'Kode Produk'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'product_name'
            Title.Caption = 'Nama Produk'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
#-- lookupCurrency

class lookupProduct_2:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.product_code
    self.contract_type = fr.contract_type
    self.dropping_model = fr.dropping_model
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    d = {}
    rqsql.SELECTFROMClause = '''SELECT
      product_code, product_name, currency_code, special_flag, kode_pof, contracttype_code,
      targeted_eqv_rate, penalty_type,  
      nvl(is_restricted,'F') is_restricted,
      nvl(id_cfgroup,0) id_cfgroup
      FROM %(FinProduct)s
      ''' % dbutil.mapDBTableNames(config, ['financing.FinProduct'])
    rqsql.WHEREClause = '''
       special_flag is null 
       AND status_product='T' 
       AND (
            contracttype_code='%s' AND dropping_model='%s' AND contracttype_code in ('B','C') OR
            contracttype_code='%s' AND contracttype_code not in ('B','C') 
       )
       AND (upper(product_code) LIKE upper(%s) OR upper(product_name) LIKE  upper(%s) )
    ''' % (self.contract_type, self.dropping_model, self.contract_type, mlu.QuotedStr('%%%s%%' % self.key), mlu.QuotedStr('%%%s%%' % self.key))
    
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("product_code;product_name")
    rqsql.keyFieldName = "product_code"
    rqsql.setBaseOrderFieldNames("product_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'product_code'
            Title.Caption = 'Kode Produk'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'product_name'
            Title.Caption = 'Nama Produk'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
#-- lookupCurrency

class lookupProduct_2_cfd:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.product_code
    self.contract_type = fr.contract_type
    self.dropping_model = fr.dropping_model
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    dParams = {
      'finproduct':config.MapDBTableName("financing.finproduct"), 
      'confidentialaccess':config.MapDBTableName("core.confidentialaccess"), 
      'listperanuser':config.MapDBTableName("enterprise.listperanuser"), 
      'id_user': mlu.QuotedStr(config.SecurityContext.InitUser),
    }
    rqsql.SELECTFROMClause = '''SELECT
      pr.product_code, pr.product_name, pr.currency_code, pr.special_flag, pr.kode_pof, pr.contracttype_code,
      pr.targeted_eqv_rate, pr.penalty_type,  
      nvl(pr.is_restricted,'F') is_restricted,
      nvl(pr.id_cfgroup,0) id_cfgroup
      FROM %(finproduct)s pr
      	LEFT JOIN %(confidentialaccess)s ca ON pr.id_cfgroup=ca.id_cfgroup AND ca.id_peran in (SELECT id_peran FROM %(listperanuser)s WHERE id_user=%(id_user)s)
      ''' % dParams
    rqsql.WHEREClause = '''
       pr.special_flag is null 
       AND pr.status_product='T' 
       AND (
            pr.contracttype_code='%s' AND pr.dropping_model='%s' AND contracttype_code in ('B','C') OR
            pr.contracttype_code='%s' AND pr.contracttype_code not in ('B','C') 
       )
       AND (upper(pr.product_code) LIKE upper(%s) OR upper(pr.product_name) LIKE  upper(%s) )
       AND (nvl(pr.id_cfgroup,0)=0 or nvl(ca.id_cfgroup,0)>0)
    ''' % (self.contract_type, self.dropping_model, self.contract_type, mlu.QuotedStr('%%%s%%' % self.key), mlu.QuotedStr('%%%s%%' % self.key))
    
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("product_code;product_name")
    rqsql.keyFieldName = "product_code"
    rqsql.setBaseOrderFieldNames("product_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'product_code'
            Title.Caption = 'Kode Produk'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'product_name'
            Title.Caption = 'Nama Produk'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
#-- lookupCurrency

class lookupKonfidential:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.cf_code
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    sSQL = '''SELECT * FROM %(ConfidentialGroup)s
      ''' % dbutil.mapDBTableNames(config, ['core.ConfidentialGroup'])
    rqsql.SELECTFROMClause = sSQL
    rqsql.WHEREClause = '''
       upper(cf_code) LIKE upper(%s) OR upper(description) LIKE  upper(%s) 
    ''' % (mlu.QuotedStr('%%%s%%' % self.key), mlu.QuotedStr('%%%s%%' % self.key))
    #--
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("cf_code;description")
    rqsql.keyFieldName = "cf_code"
    rqsql.setBaseOrderFieldNames("cf_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'cf_code'
            Title.Caption = 'Kode Konfidential'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'description'
            Title.Caption = 'Keterangan'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
#-- lookupKonfidential

class getPoolOfFund:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.kode_pof or ''   
    userinfo = config.SecurityContext.GetUserInfo()
    self.company_code = userinfo[5]
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    
    rqsql.SELECTFROMClause = '''
    SELECT
      p.kode_pof,
      p.nama_pof,
      p.currency_code
    FROM 
      %(pooloffund)s p
      --inner join %(company)s co on co.company_code = p.company_code
    ''' % {
      'pooloffund': config.MapDBTableName('core.PoolOfFund'),
      'company': config.MapDBTableName('enterprise.Company'),
    }
    
    rqsql.WHEREClause = '''
    ROWNUM <= 30
    AND (
      UPPER(p.kode_pof) LIKE %(key_value)s 
      OR UPPER(p.nama_pof) LIKE %(key_value)s
    )
    --AND p.company_code = '%(CompanyCode)s'
    ''' %  {
      'key_value': mlu.QuotedStr('%%%s%%' % str(self.key_value).upper()),
      'CompanyCode':self.company_code,
    }                                
    
    rqsql.setAltOrderFieldNames("kode_pof")
    rqsql.keyFieldName = "kode_pof"
    rqsql.setBaseOrderFieldNames("kode_pof")
    
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            Expanded = False
            FieldName = 'kode_pof'
            Title.Caption = 'Kode POF'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nama_pof'
            Title.Caption = 'Nama POF'
            Width = 300
            Visible = True
          end
        >
      end
    '''
  #--
#--