import os
import sys
import com.ihsan.util.dbutil as dbutil
#import rpdb2; rpdb2.start_embedded_debugger("000")

class lookup:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.currency_code
    
  def initQueryObject(self, rqsql):
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''SELECT
      currency_code, full_name
      FROM %(Currency)s
      ''' % dbutil.mapDBTableNames(self.config, ['core.Currency'])
    rqsql.WHEREClause = "upper(currency_code) LIKE upper(%s)" % mlu.QuotedStr('%%%s%%' % self.key)
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("currency_code;full_name")
    rqsql.keyFieldName = "currency_code"
    rqsql.setBaseOrderFieldNames("currency_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'currency_code'
            Title.Caption = 'Kode Currency'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'full_name'
            Title.Caption = 'Nama Currency'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
#-- lookupCurrency

class lookupPoolOfFund:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.kode_pof
    self.currency_code = fr.currency_code or ""
    
  def initQueryObject(self, rqsql):
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''SELECT
      kode_pof, nama_pof
      FROM %(PoolOfFund)s
      ''' % dbutil.mapDBTableNames(self.config, ['core.PoolOfFund'])
    rqsql.WHEREClause = "currency_code = %s AND upper(kode_pof) LIKE upper(%s)" % (mlu.QuotedStr(self.currency_code), mlu.QuotedStr('%%%s%%' % self.key))
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("kode_pof;nama_pof")
    rqsql.keyFieldName = "kode_pof"
    rqsql.setBaseOrderFieldNames("kode_pof")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'kode_pof'
            Title.Caption = 'Kode PoF'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'nama_pof'
            Title.Caption = 'Nama PoF'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
#-- lookupCurrency