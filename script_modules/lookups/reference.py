import sys
import os
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.dbutil as dbutil

class refJoinMapCat:
  def __init__(self, config, fr):
    self.config = config
    self.reference_code = fr.reference_code
    self.reference_name = fr.reference_name
    self.nama_kategori = fr.nama_kategori

    self.addCond = ''
    if fr.Dataset.IsFieldExist('addCond'):
      self.addCond = fr.addCond
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    rqsql.SELECTFROMClause = '''
    SELECT
      d.*,
      k.nama_kategori,
      t.reference_name
    FROM 
      %(referencedata)s d
      INNER JOIN %(referencetype)s t ON t.reftype_id = d.reftype_id
      INNER JOIN %(refmapreport)s r ON r.reftype_id = t.reftype_id
      INNER JOIN (
        SELECT
          x.*,
          CASE x.kolom_detil 
            WHEN 'sandi_lbus' THEN 'B'
            WHEN 'sandi_lsmk' THEN 'M'
            WHEN 'sandi_sid' THEN 'S'
            ELSE '' 
          END AS report_type
        FROM %(refmapkategori)s x
      ) k ON k.kode_kategori = r.kode_map_kategori
    ''' % {
      'referencedata': config.MapDBTableName('enterprise.referencedata'),
      'referencetype': config.MapDBTableName('enterprise.referencetype'),
      'refmapreport': config.MapDBTableName('enterprise.refmapreport'),
      'refmapkategori': config.MapDBTableName('enterprise.refmapkategori')
    }
    rqsql.WHEREClause = '''
    r.report_type = k.report_type
    AND (
      UPPER(d.reference_code) LIKE '%%%(reference_code)s%%'
      OR UPPER(d.reference_desc) LIKE '%%%(reference_code)s%%'
    )
    ''' % {
      'reference_code': str(self.reference_code).upper()
    }
    if self.reference_name != '':
      rqsql.WHEREClause += ''' AND UPPER(t.reference_name) = UPPER(%(reference_name)s) ''' % {
        'reference_name': mlu.QuotedStr(self.reference_name)
      }
    #--
    if self.nama_kategori != '':
      rqsql.WHEREClause += ''' AND UPPER(k.nama_kategori) = UPPER(%(nama_kategori)s) ''' % {
        'nama_kategori': mlu.QuotedStr(self.nama_kategori)
      }
    #--
    
    if self.addCond != '':
      rqsql.WHEREClause += self.addCond
    #--
    
    #rqsql.GROUPBYClause = "GROUP BY acc.account_code, acc.account_name, acc.account_type" 
    rqsql.setAltOrderFieldNames("reference_code;reference_desc;refdata_id")
    rqsql.keyFieldName = "refdata_id"
    rqsql.setBaseOrderFieldNames("reference_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'refdata_id'
            Visible = False
          end
          item
            FieldName = 'reftype_id'
            Visible = False
          end
          item
            FieldName = 'reference_code'
            Title.Caption = 'Kode Ref'
            Width = 70
          end
          item
            FieldName = 'reference_desc'
            Title.Caption = 'Keterangan'
            Width = 450
          end
          >
      end
    '''
  #--
#--

class lookup_refdata:
  def __init__(self, config, fr):
    self.config = config
    self.reference_code = fr.reference_code
    self.reference_name = fr.reference_name
    self.nama_kategori = fr.nama_kategori
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    rqsql.SELECTFROMClause = '''
    SELECT
      d.*,
      t.reference_name
    FROM 
      %(referencedata)s d
      INNER JOIN %(referencetype)s t ON t.reftype_id = d.reftype_id
    ''' % {
      'referencedata': config.MapDBTableName('enterprise.referencedata'),
      'referencetype': config.MapDBTableName('enterprise.referencetype')
    }
    rqsql.WHEREClause = '''
    (
      UPPER(d.reference_code) LIKE '%%%(reference_code)s%%'
      OR UPPER(d.reference_desc) LIKE '%%%(reference_code)s%%'
    )
    ''' % {
      'reference_code': str(self.reference_code).upper()
    }
    #--
    if self.nama_kategori != '':
      rqsql.WHEREClause += ''' AND UPPER(t.reference_name) = UPPER(%(nama_kategori)s) ''' % {
        'nama_kategori': mlu.QuotedStr(self.nama_kategori)
      }
    #--
    
    #rqsql.GROUPBYClause = "GROUP BY acc.account_code, acc.account_name, acc.account_type" 
    rqsql.setAltOrderFieldNames("reference_code;reference_desc;refdata_id")
    rqsql.keyFieldName = "refdata_id"
    rqsql.setBaseOrderFieldNames("reference_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'refdata_id'
            Visible = False
          end
          item
            FieldName = 'reftype_id'
            Visible = False
          end
          item
            FieldName = 'reference_code'
            Title.Caption = 'Kode Ref'
            Width = 70
          end
          item
            FieldName = 'reference_desc'
            Title.Caption = 'Keterangan'
            Width = 450
          end
          >
      end
    '''
  #--
#--


class lookup_Enumerasi:
  def __init__(self, config, fr):
    self.config = config
    self.e_type      = fr.e_type
    self.description = fr.description
    self.enum_name   = fr.enum_name
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = config.ModLibUtils
    enumTable = config.MapDBTableName('financing.enum_varchar')
    if e_type=='I':
      enumTable = config.MapDBTableName('financing.enum_int')
    rqsql.SELECTFROMClause = '''
      SELECT d.* FROM %(enumTable)s d
    ''' % { 'enumTable': enumTable }
    rqsql.WHEREClause = '''
      d.enum_name = '%(enum_name)s' and UPPER(d.description) LIKE UPPER('%%%(description)s%%')
    ''' % {
      'enum_name': self.enum_name,
      'description': self.description
    }
    #--

    rqsql.setAltOrderFieldNames("description")
    rqsql.keyFieldName = "description"
    rqsql.setBaseOrderFieldNames("description")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'enum_value'
            Title.Caption = 'Kode Enum'
            Width = 60
          end
          item
            FieldName = 'description'
            Title.Caption = 'Keterangan'
            Width = 350
          end
          >
      end
    '''
  #--
#--

class lookupUser:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.id_user    
    
    sSQL = '''
      SELECT b.tipe_akses_cabang FROM %(userapp)s b WHERE b.id_user='%(id_user)s' 
    ''' % {
      'userapp': config.MapDBTableName("enterprise.userapp"),
      'id_user':config.SecurityContext.InitUser
    }
    rSQL = config.CreateSQL(sSQL).RawResult
    self.tipe_akses = rSQL.tipe_akses_cabang
    self.def_user = config.SecurityContext.InitUser
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils

    rqsql.SELECTFROMClause = '''
      SELECT ID_USER,NAMA_USER,NOMOR_KARYAWAN FROM %(userapp)s
    ''' % {
      'userapp': config.MapDBTableName("enterprise.userapp")
    }
    rqsql.WHEREClause = '''
      ( upper(id_user) LIKE upper(%(id_user)s) OR upper(nama_user) LIKE upper(%(id_user)s) )
    ''' %  {
      #'id_user': mlu.QuotedStr('%%%s%%' % self.key_value)
      'id_user': mlu.QuotedStr('%%%s%%' % self.def_user)
    }                                
    rqsql.setAltOrderFieldNames("id_user")
    rqsql.keyFieldName = "id_user"
    rqsql.setBaseOrderFieldNames("id_user")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'id_user'
            Visible = True
            Width = 100
          end
          item
            FieldName = 'nama_user'
            Visible = True
            Width = 250
          end
          >
      end
    '''
  #--
  
class lookupTieringParams:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.tiering_code
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    sSQL = '''SELECT * FROM %(finparametertiering)s
      ''' % dbutil.mapDBTableNames(config, ['finparametertiering'])
    rqsql.SELECTFROMClause = sSQL
    rqsql.WHEREClause = '''
       nomor_rekening IS NULL and
       (upper(tiering_code) LIKE upper(%s) OR upper(description) LIKE  upper(%s)) 
    ''' % (mlu.QuotedStr('%%%s%%' % self.key), mlu.QuotedStr('%%%s%%' % self.key))
    #--
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("tiering_code;description")
    rqsql.keyFieldName = "tiering_code"
    rqsql.setBaseOrderFieldNames("tiering_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'tiering_code'
            Title.Caption = 'Kode Tiering'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'description'
            Title.Caption = 'Keterangan'
            Visible = True
            Width = 450
          end
          item
            FieldName = 'period_count'
            Title.Caption = 'Tenor'
            Visible = True
            Width = 70
          end
          >
      end
    '''
  #--
#-- lookupKonfidential

class lookupTieringParams_2:
  def __init__(self, config, fr):
    self.config = config
    self.key = fr.tiering_code
    self.norek = fr.nomor_rekening
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    dParam = {
      'finparametertiering': config.MapDBTableName('finparametertiering'),
      'keyw': mlu.QuotedStr('%%%s%%' % self.key),
      'norek': mlu.QuotedStr(self.norek)
    }
    sSQL = '''SELECT * FROM %(finparametertiering)s ''' % dParam
    rqsql.SELECTFROMClause = sSQL
    rqsql.WHEREClause = '''
       nvl(nomor_rekening, %(norek)s) = %(norek)s and
       (upper(tiering_code) LIKE upper(%(keyw)s) OR upper(description) LIKE  upper(%(keyw)s)) 
    ''' % dParam
    #--
    rqsql.GROUPBYClause = ""
    rqsql.setAltOrderFieldNames("tiering_code;description")
    rqsql.keyFieldName = "tiering_code"
    rqsql.setBaseOrderFieldNames("tiering_code")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'tiering_code'
            Title.Caption = 'Kode Tiering'
            Visible = True
            Width = 70
          end
          item
            FieldName = 'description'
            Title.Caption = 'Keterangan'
            Visible = True
            Width = 450
          end
          >
      end
    '''
  #--
#-- lookupKonfidential
  