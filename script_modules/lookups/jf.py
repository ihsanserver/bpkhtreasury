import os
import sys
#import rpdb2; rpdb2.start_embedded_debugger("000")

class channelingAgent():
  def __init__(self, config, fr):
    self.config = config
    self.keyword = fr.keyword
  #--
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    
    sTblName = 'jfchannelingagent'
    sKeyFieldName = 'agentcode'
    
    rqsql.SELECTFROMClause = '''
      SELECT 
        m.agentcode,
        m.agentname,
        m.is_active
      FROM %(tblName)s m
      ''' % {'tblName': config.MapDBTableName(sTblName)}
    
    rqsql.WHEREClause = '''
      (UPPER(m.agentcode) LIKE UPPER(%(keyword)s)
      OR UPPER(m.agentname) LIKE UPPER(%(keyword)s)) 
      ''' %  {'keyword': mlu.QuotedStr('%%%s%%' % self.keyword)}
    
    rqsql.setAltOrderFieldNames(sKeyFieldName)
    rqsql.keyFieldName = sKeyFieldName
    rqsql.setBaseOrderFieldNames(sKeyFieldName)
  #--
#--