import os
import sys
import com.ihsan.util.dbutil as dbutil

class lookupFinAccount:
  def __init__(self, config, fr):
    self.config = config
    self.key_value = fr.nomor_rekening
    #--
    #if len(fr.nomor_rekening) < 8:
    #  raise Exception, "Input nomor rekening kurang panjang (min 8)"
    
  def initQueryObject(self, rqsql):
    config = self.config
    mlu = self.config.ModLibUtils
    rqsql.SELECTFROMClause = '''
      SELECT
        fa.nomor_rekening,
        ns.nama_nasabah as nama_rekening,
        ct.contracttype_code,
        fa.product_code,
        ct.description, 
        ns.nomor_nasabah
      FROM %(rekeningcustomer)s cs,
      %(rekeningtransaksi)s rt,
      finaccount fa,
      contracttype ct,
      %(nasabah)s ns 
    ''' % dbutil.mapDBTableNames(config, ['core.rekeningcustomer', 'core.nasabah', 'core.rekeningtransaksi'])
    
    rqsql.WHEREClause = '''
      fa.nomor_rekening = rt.nomor_rekening AND
      rt.nomor_rekening = cs.nomor_rekening AND
      cs.nomor_nasabah = ns.nomor_nasabah AND
      ct.contracttype_code = fa.contracttype_code 
      AND rt.status_rekening = 1
      AND fa.nomor_rekening LIKE %(nomor_rekening)s
    ''' %  {
      'nomor_rekening': mlu.QuotedStr('%s%%' % self.key_value)
    }                                
    rqsql.setAltOrderFieldNames("fa.nomor_rekening;ns.nama_nasabah")
    rqsql.keyFieldName = "fa.nomor_rekening"
    rqsql.setBaseOrderFieldNames("fa.nomor_rekening")
    rqsql.columnSetting = '''
      object TColumnsWrapper
        Columns = <
          item
            FieldName = 'nomor_rekeninig'
            Visible = True
            Width = 80
          end
          item
            FieldName = 'nama_rekening'
            Visible = True
            Width = 150
          end
          item
            FieldName = 'product_code'
            Visible = True
            Width = 80
          end
          >
      end
    '''
  #--  #--
#--  
    