import sys
import os
import com.ihsan.foundation.pobject as pobject

### TABLE ALIASES :
### FA = FINACCOUNT FIN
### RT = REKENINGTRANSAKSI CORE
### RC = REKENINGCUSTOMER CORE
### N = NASABAH CORE
### FS = FINPAYMENTSCHEDULE FIN
### FSD = FINPAYMENTSCHEDDETAIL FIN
### MBH = FINMURABAHAHACCOUNT
### QRD = FINQARDHACCOUNT
### MSY = FINMUSYARAKAHACCOUNT                                

class RptSqlHelper :
  def getJoinedAccountTbl(self, config):
    
    sql = '''
     %(FINACCOUNT)s FA 
     LEFT OUTER JOIN %(FINMURABAHAHACCOUNT)s  MBH ON FA."NOMOR_REKENING" = MBH."NOMOR_REKENING"
     LEFT OUTER JOIN %(FINMUSYARAKAHACCOUNT)s MSY ON FA."NOMOR_REKENING" = MSY."NOMOR_REKENING"
     LEFT OUTER JOIN %(FINQARDHACCOUNT)s QRD ON FA."NOMOR_REKENING" = QRD."NOMOR_REKENING" 
     LEFT OUTER JOIN %(FINIJARAHACCOUNT)s IJR ON FA."NOMOR_REKENING" = IJR."NOMOR_REKENING" 
     LEFT OUTER JOIN %(KAFALAH)s KAF ON FA."NOMOR_REKENING" = KAF."NOMOR_REKENING" 
    ''' % {
    'FINACCOUNT': config.MapDBTableName('financing.FINACCOUNT'),
    'FINMUSYARAKAHACCOUNT': config.MapDBTableName('financing.FINMUSYARAKAHACCOUNT'),
    'FINQARDHACCOUNT': config.MapDBTableName('financing.FINQARDHACCOUNT'), 
    'FINMURABAHAHACCOUNT': config.MapDBTableName('financing.FINMURABAHAHACCOUNT'),
    'FINIJARAHACCOUNT': config.MapDBTableName('financing.FINIJARAHACCOUNT'),
    'KAFALAH': config.MapDBTableName('financing.KAFALAH')
    }
    return sql

  def getJoinedAccountTblDaily(self, date, config):
    mlu = config.ModLibUtils
    tanggal = mlu.QuotedStr(str(date))
    sql = '''
     (SELECT * FROM %(CKNOM_BASE_DAILY)s WHERE TANGGAL = to_date(%(tanggal)s, 'YYYY-MM-DD')) FA1
     LEFT OUTER JOIN %(FINACCOUNT)s FA ON FA1."NOMOR_REKENING" = FA."NOMOR_REKENING" 
     LEFT OUTER JOIN %(FINMURABAHAHACCOUNT)s  MBH ON FA."NOMOR_REKENING" = MBH."NOMOR_REKENING"
     LEFT OUTER JOIN %(FINMUSYARAKAHACCOUNT)s MSY ON FA."NOMOR_REKENING" = MSY."NOMOR_REKENING"
     LEFT OUTER JOIN %(FINQARDHACCOUNT)s QRD ON FA."NOMOR_REKENING" = QRD."NOMOR_REKENING" 
    ''' % {
    'CKNOM_BASE_DAILY': config.MapDBTableName('tmp.CKNOM_BASE_DAILY'),
    'FINACCOUNT': config.MapDBTableName('financing.FINACCOUNT'),
    'FINMUSYARAKAHACCOUNT': config.MapDBTableName('financing.FINMUSYARAKAHACCOUNT'),
    'FINQARDHACCOUNT': config.MapDBTableName('financing.FINQARDHACCOUNT'), 
    'FINMURABAHAHACCOUNT': config.MapDBTableName('financing.FINMURABAHAHACCOUNT'),
    'tanggal':tanggal
    }
    return sql
    
    
  def getJoinedFacilityTbl(self,config):
    sql = '''INNER JOIN %(FINFACILITY)s FF ON FA."FACILITY_NO" = FF."FACILITY_NO"''' % {
    'FINFACILITY': config.MapDBTableName('financing.FINFACILITY')
    }
    return sql
    
  def getScheduleTbl(self,config):
    sql = '''
    LEFT JOIN %(FINPAYMENTSCHEDULE)s FS ON FA."ID_SCHEDULE" = FS."ID_SCHEDULE"
    LEFT JOIN %(FINPAYMENTSCHEDDETAIL)s FSD ON FS."ID_SCHEDULE" = FSD."ID_SCHEDULE"
    AND FS."ACTIVE_SEQ" = FSD."SEQ_NUMBER"
    ''' % {
    'FINPAYMENTSCHEDULE': config.MapDBTableName('financing.FINPAYMENTSCHEDULE'),
    'FINPAYMENTSCHEDDETAIL': config.MapDBTableName('financing.FINPAYMENTSCHEDDETAIL')
    }
    return sql
    
  def getSchedByDate(self,config):
    phTgl = config.AppObject.rexecscript("core", "appinterface/coreInfo.getAccountingDay", config.AppObject.CreatePacket())
    tgl_transaksi = phTgl.FirstRecord.acc_date
    strDate = config.FormatDateTime('yyyymm', tgl_transaksi)
    sql = '''
    LEFT JOIN (
      SELECT ID_SCHEDULE, max(PROFIT_RATIO) nisbah_bank FROM %(FINPAYMENTSCHEDDETAIL)s a 
      WHERE to_char(a.SCHED_DATE, 'YYYYMM')='%(strDate)s' GROUP BY ID_SCHEDULE
    ) PSD ON FA."ID_SCHEDULE" = PSD."ID_SCHEDULE"  
    ''' % {
    'FINPAYMENTSCHEDDETAIL': config.MapDBTableName('financing.FINPAYMENTSCHEDDETAIL'),'strDate': strDate
    }
    return sql
    
  def getOtherTbl(self,config):
    sql = '''
     INNER JOIN %(REKENINGTRANSAKSI)s  RT ON FA."NOMOR_REKENING" = RT."NOMOR_REKENING"
     INNER JOIN %(REKENINGCUSTOMER)s  RC ON RT."NOMOR_REKENING" = RC."NOMOR_REKENING"
     INNER JOIN %(NASABAH)s  N ON RC."NOMOR_NASABAH" = N."NOMOR_NASABAH"
    '''% {
    'REKENINGTRANSAKSI': config.MapDBTableName('core.REKENINGTRANSAKSI'),
    'REKENINGCUSTOMER': config.MapDBTableName('core.REKENINGCUSTOMER'),
    'NASABAH': config.MapDBTableName('core.NASABAH')
    }
    return sql
    
  def getOtherTbl2(self,config):
    sql = '''
     INNER JOIN %(NASABAH)s  N ON FI."NO_NASABAH" = N."NOMOR_NASABAH"
    '''% {
    'NASABAH': config.MapDBTableName('core.NASABAH')
    }
    return sql

  def getCollateralTbl(self,config):
    sql = '''
     INNER JOIN %(FINCOLLATERALACCOUNT)s CA ON CA."NOREK_FINACCOUNT" = FA."NOMOR_REKENING"
     INNER JOIN %(FINCOLLATERALASSET)s CO ON CO."NOMOR_REKENING" = CA."NOREK_FINCOLLATERALASSET"
       LEFT JOIN %(ENUM_VARCHAR)s EV ON CO."ASSET_TYPE" = EV."ENUM_VALUE" AND EV."ENUM_NAME"='eColAssetType'
       LEFT JOIN %(ENUM_VARCHAR)s GAT ON CO."GROUP_ASSET_TYPE" = GAT."ENUM_VALUE" AND GAT."ENUM_NAME"='eColAssetType'
    '''% {
    'FINCOLLATERALACCOUNT': config.MapDBTableName('financing.FINCOLLATERALACCOUNT'),
    'FINCOLLATERALASSET': config.MapDBTableName('financing.FINCOLLATERALASSET'),
    'ENUM_VARCHAR': config.MapDBTableName('financing.ENUM_VARCHAR')
    }
    return sql
        
  def getTransaksiTbl(self,config):
    sql = '''
      inner join %(detiltransaksi)s DT on FA.nomor_rekening = DT.nomor_rekening
      left outer join %(detiltransaksiclass)s DTC ON DT.kode_tx_class = DTC.kode_tx_class
      inner join %(transaksi)s TX on DT.id_transaksi = TX.id_transaksi
    '''% {
          'detiltransaksi': config.MapDBTableName('core.DetilTransaksi'), 
          'transaksi': config.MapDBTableName('core.Transaksi'), 
          'detiltransaksiclass': config.MapDBTableName('core.detiltransaksiclass')
       }
              
    return sql
    
  def getHistTransaksiTbl(self,config):
    sql = '''
      inner join %(histdetiltransaksi)s DT on FA.nomor_rekening = DT.nomor_rekening
      left outer join %(detiltransaksiclass)s DTC ON DT.kode_tx_class = DTC.kode_tx_class
      inner join %(histtransaksi)s TX on DT.id_transaksi = TX.id_transaksi
    '''% {
          'histdetiltransaksi': config.MapDBTableName('core.HistDetilTransaksi'), 
          'histtransaksi': config.MapDBTableName('core.HistTransaksi'), 
          'detiltransaksiclass': config.MapDBTableName('core.detiltransaksiclass')
       }
              
    return sql

  def getJoinedAccountManagerTbl(self,config):
    sql = '''LEFT OUTER JOIN %(FundingAgent)s AM ON FA.AMCODE = AM.AGENTCODE ''' % {
    'FundingAgent': config.MapDBTableName('core.FundingAgent')
    }
    return sql
    
  def getJoinedProductTbl(self,config):
    sql = '''LEFT OUTER JOIN %(FinProduct)s FP ON FA.PRODUCT_CODE = FP.PRODUCT_CODE ''' % {
    'FinProduct': config.MapDBTableName('financing.FinProduct')
    }
    return sql
    
    
  def dateFloatToStr(self, data, config=None): # return YYYY-MM-DD
    from datetime import datetime
    import types
    
    sValidValue = ''
    if data!=None:
      try:
        if type(data) == types.FloatType and config not in [None]:
          y,m,d = config.ModDateTime.DecodeDate(data)
          
          sValidValue = "%04d-%02d-%02d" % (y,m,d)
          
          datetime.strptime(sValidValue, '%Y-%m-%d')
      except:
        sValidValue = ''    
    return sValidValue
#--

  def getFacilityTbl(self, config):
    
    sql = '''
     %(FINFACILITY)s FI 
    ''' % {
    'FINFACILITY': config.MapDBTableName('financing.FINFACILITY')
    }
    return sql

class RptPastDueHelper (RptSqlHelper) :
    
  def getColPastDuePrincipal_bck(self):
    sql = """
    CASE 
     WHEN FA."CONTRACTTYPE_CODE"='A' THEN
       CASE WHEN FA."ARREAR_BALANCE" < 0 THEN
         CASE WHEN FA."REPAYMENT_COL_LEVEL" IN(0,1,2) THEN -1 * (FA."ARREAR_BALANCE"+FA."PROFIT_ARREAR_BALANCE")
         ELSE -1*(FA."ARREAR_BALANCE"+FA."PROFIT_ARREAR_BALANCE") END
       ELSE 0  END      
     WHEN FA."CONTRACTTYPE_CODE"='D' THEN 
       CASE WHEN FA."ARREAR_BALANCE" < 0 THEN
         CASE WHEN FA."REPAYMENT_COL_LEVEL" IN(0,1,2) THEN-1 * (FA."ARREAR_BALANCE"+FA."PROFIT_ARREAR_BALANCE")
         ELSE-1*(FA."ARREAR_BALANCE"+FA."PROFIT_ARREAR_BALANCE") END
       ELSE 0  END     
     WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G') THEN (FA."ARREAR_BALANCE") 
     ELSE 0 
     END 
    """
    return sql

  def getColPastDuePrincipal(self):
    sql = """
    CASE 
     WHEN FA."CONTRACTTYPE_CODE" in ('A','H','D') THEN
       CASE WHEN FA."ARREAR_BALANCE" < 0 THEN -1 * (FA."ARREAR_BALANCE"+FA."PROFIT_ARREAR_BALANCE") ELSE 0  END      
     WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G','F') THEN -(FA."ARREAR_BALANCE") 
     ELSE 0 
     END 
    """
    return sql
     
  def getColPastDueMargin_bck(self):
    sql = """
     CASE 
     WHEN FA."CONTRACTTYPE_CODE"='A' THEN
       CASE WHEN FA."ARREAR_BALANCE" < 0 THEN 
         CASE WHEN FA."REPAYMENT_COL_LEVEL" IN(0,1,2) THEN MBH."PROFIT_ACCRUE_BALANCE"
         ELSE MBH."PROFIT_PENDING_BALANCE" END
       ELSE 0  END  
     WHEN FA."CONTRACTTYPE_CODE"='D' THEN 
       CASE WHEN FA."ARREAR_BALANCE" < 0 THEN 
         CASE WHEN FA."REPAYMENT_COL_LEVEL" IN(0,1,2) THEN QRD."PROFIT_ACCRUE_BALANCE"
         ELSE QRD."PROFIT_PENDING_BALANCE" END 
       ELSE 0  END
     WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G') THEN 0 
     ELSE 0 
     END
    """
    return sql
    
  def getColPastDueMargin(self):
    sql = """
     CASE 
     WHEN FA."CONTRACTTYPE_CODE" in ('A','D','H') THEN
       CASE WHEN FA."ARREAR_BALANCE" < 0 THEN FA."PROFIT_ARREAR_BALANCE" ELSE 0  END  
     WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G') THEN -FA."PROFIT_ARREAR_BALANCE" 
     ELSE 0 
     END
    """
    return sql
    
  def getColDurationDesc(self):  
    sql = """
    CASE 
     WHEN FA."ARREAR_PERIOD" < 90 THEN '1 S/D 90'
     WHEN FA."ARREAR_PERIOD" >= 90 AND FA."ARREAR_PERIOD" < 180 THEN '90 S/D 180'
     WHEN FA."ARREAR_PERIOD" >= 180 AND FA."ARREAR_PERIOD" < 270 THEN '180 S/D 270'      
     WHEN FA."ARREAR_PERIOD" >= 270 AND FA."ARREAR_PERIOD" < 360 THEN '270 S/D 360'
     ELSE 'LEBIH DARI 360' 
     END
    """
    return sql
    
  def getSql(self, config, pastDueType, params) :
    mlu = config.ModLibUtils
    dsql = {'pastDuePrincipal': self.getColPastDuePrincipal(), 'pastDueMargin': self.getColPastDueMargin(), 'durationGroup': self.getColDurationDesc()
    , 'accounts' : self.getJoinedAccountTbl(config), 'otherTables' : self.getOtherTbl(config), 'schedTables' : self.getScheduleTbl(config)
    , 'valuta' : mlu.QuotedStr(params['valuta']), 'branchcode': mlu.QuotedStr(params['branchcode']) }
    if pastDueType in ['by-ao', 'by-duration', 'by-col'] :
      if pastDueType == 'by-ao' : dsql['orderby'] = 'ORDER BY AMCODE, CUST_NO, LOAN_NO '         
      elif pastDueType == 'by-col' : dsql['orderby'] = 'ORDER BY COL, CUST_NO, LOAN_NO '         
      else : dsql['orderby'] = 'ORDER BY DURATION_GROUP , CUST_NO, LOAN_NO '
      sql = '''SELECT
         RT.KODE_CABANG AS KODE_CABANG,
         %(pastDuePrincipal)s AS PAST_DUE_PRINCIPAL, 
         %(pastDueMargin)s AS PAST_DUE_MARGIN, 
         RT."NAMA_REKENING" AS LOAN_HOLDER,
         RC."NOMOR_NASABAH" AS CUST_NO,
         FA."NOMOR_REKENING" AS LOAN_NO,
         FS."ACTIVE_SEQ" AS SEQ,
         FSD."SCHED_DATE" AS DUE_DATE,  
         FA."DUE_DATE" AS MATURITY_DATE,
         FA."ARREAR_PERIOD" AS DURATION,
         FA."AMCODE" AS AMCODE,
         RT."KODE_VALUTA" AS KODE_VALUTA,
         FA."CONTRACTTYPE_CODE" AS KODE_AKAD,
         FA."OVERALL_COL_LEVEL" AS COL,
         1 AS COUNT,
         %(durationGroup)s as DURATION_GROUP
      FROM
         %(accounts)s
         %(schedTables)s
         %(otherTables)s          
      '''
    elif pastDueType == 'by-loan-holder' :
      loanSqlHlp = RptLoanOutstandingHelper() 
      dsql['orderby'] = 'ORDER BY CUST_NO, LOAN_NO '
      dsql['facilityTbl'] = self.getJoinedFacilityTbl(config)
      dsql['loanOutstd'] = loanSqlHlp.getColLoanOutstd()  
      sql = '''SELECT
         RT.KODE_CABANG AS KODE_CABANG,
         %(pastDuePrincipal)s AS PAST_DUE_PRINCIPAL,
         --(FA."ARREAR_BALANCE") AS PAST_DUE_PRINCIPAL, 
         %(pastDueMargin)s AS PAST_DUE_MARGIN,
         FF."TOTAL_FACILITY_LIMIT" AS FACILITY_AMT,
         %(loanOutstd)s AS LOAN_OUTSTD, 
         RT."NAMA_REKENING" AS LOAN_HOLDER,
         RC."NOMOR_NASABAH" AS CUST_NO,
         FA."NOMOR_REKENING" AS LOAN_NO,
         FS."ACTIVE_SEQ" AS SEQ,
         FSD."SCHED_DATE" AS DUE_DATE,
         FA."DUE_DATE" AS MATURITY_DATE,
         FA."ARREAR_PERIOD" AS DURATION,
         FA."AMCODE" AS AMCODE,
         RT."KODE_VALUTA" AS KODE_VALUTA,
         FA."CONTRACTTYPE_CODE" AS KODE_AKAD,
         FA."OVERALL_COL_LEVEL" AS COL,
         1 AS COUNT
      FROM
         %(accounts)s
         %(facilityTbl)s
         %(schedTables)s
         %(otherTables)s          
      '''    
    else : raise Exception, "getSql must be customized for rptType '%s'" % pastDueType
    sql += '''
    WHERE
           (1 = 1)
           AND RT.KODE_VALUTA = %(valuta)s
           AND RT.KODE_CABANG = %(branchcode)s
           AND RT.STATUS_REKENING <> 3
           AND FS.COMPLETION_STATUS <> 'T'
           AND FA.is_external = 'F'
    %(orderby)s
    '''       
    sql = sql % dsql
    ### FSD.ARREAR = 'T'
    #raise Exception, sql
    return sql  
#--

class RptLoanOutstandingHelper(RptSqlHelper) :
  def getColLoanOutstd(self): ## outstanding pokok
    sql ='''
    CASE 
      WHEN FA."CONTRACTTYPE_CODE"='A' THEN 
        -(RT.SALDO + FA.ARREAR_BALANCE)        
      WHEN FA."CONTRACTTYPE_CODE"='D' THEN 
        -(RT.SALDO + FA.ARREAR_BALANCE)  
      WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G') THEN 
        -(RT.SALDO + FA.ARREAR_BALANCE) 
      WHEN FA."CONTRACTTYPE_CODE" IN ('F') THEN 
        (IJR.INITIAL_ASSET_VALUE - IJR.DEPRECIATION_BALANCE)-FA.ARREAR_BALANCE 
      WHEN FA."CONTRACTTYPE_CODE" IN ('I') THEN 
        nilai_kafalah 
      ELSE 0 
    END
    '''
    return sql
    
  def getColOutstdMargin(self):
    sql = '''CASE 
      WHEN FA."CONTRACTTYPE_CODE"='A' THEN 
        MBH.MMD_BALANCE + FA.PROFIT_ARREAR_BALANCE         
      WHEN FA."CONTRACTTYPE_CODE"='D' THEN 
        QRD.MMD_BALANCE + FA.PROFIT_ARREAR_BALANCE  
      WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G') THEN 
        --MSY.TOTAL_PROJECTED_INCOME - FA.PROFIT_BALANCE
        0 
      WHEN FA."CONTRACTTYPE_CODE" IN ('F') THEN 
        FA.PREDICTED_MARGIN_AMOUNT - FA.PROFIT_BALANCE
      ELSE 0 
    END
    '''
    return sql
    
  def getProfitAccrue(self):
    sql = '''CASE 
      WHEN FA."CONTRACTTYPE_CODE"='A' THEN 
        MBH.PROFIT_ACCRUE_BALANCE         
      WHEN FA."CONTRACTTYPE_CODE"='D' THEN 
        QRD.PROFIT_ACCRUE_BALANCE  
      WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G','F') THEN 
        --MSY.TOTAL_PROJECTED_INCOME - FA.PROFIT_BALANCE
        0 
      ELSE 0 
    END
    '''
    return sql
  #--

  def whereLoGlobal(self):
    sCondition = '''
    AND RT.STATUS_REKENING <> 3 AND FA.CONTRACTTYPE_CODE<>'W'    
    '''    
    return sCondition
  
  def whereLoOpenToDay(self):
    sCondition = '''
    AND TRUNC(fa.opening_date) = TRUNC(SYSDATE)
    AND RT.STATUS_REKENING <> 3
    AND FA.CONTRACTTYPE_CODE<>'W'     
    '''    
    return sCondition
               
  def whereLoClosedToday(self):
    sCondition = '''
    AND (
      TRUNC(fa.closing_date) = TRUNC(SYSDATE)
      OR                                     
      TRUNC(fa.ayda_date) = TRUNC(SYSDATE)
      OR                                     
      TRUNC(fa.writeoff_date) = TRUNC(SYSDATE)
    )
    AND FA.CONTRACTTYPE_CODE<>'W' 
    '''    
    return sCondition
               
  def whereLoLancar(self):
    sCondition = '''
    AND overall_col_level = 1
    AND RT.STATUS_REKENING <> 3 
    AND FA.CONTRACTTYPE_CODE<>'W'    
    '''    
    return sCondition
                             
  def whereLoTunggak(self):
    sCondition = '''
    AND overall_col_level > 1
    AND RT.STATUS_REKENING <> 3 
    AND FA.CONTRACTTYPE_CODE<>'W'    
    '''    
    return sCondition
    
  def whereLoWO(self):
    sCondition = '''
    AND write_off_status = 'T'
    AND FA.CONTRACTTYPE_CODE<>'W'     
    '''    
    return sCondition
        
  def whereLoRecoveryWO(self):
    sCondition = '''          
    AND recovery_wo_status = 'T'  
    AND FA.CONTRACTTYPE_CODE<>'W'  
    '''    
    return sCondition

  def whereLoDetailAYDA(self):
    sCondition = '''          
    AND ayda_flag = 'T'  
    AND FA.CONTRACTTYPE_CODE<>'W'  
    '''    
    return sCondition            

  #--
    
  def getSql(self, config, rptType, params) :
    mlu = config.ModLibUtils
    pastDueHlp = RptPastDueHelper()
    phTgl = config.AppObject.rexecscript("core", "appinterface/coreInfo.getAccountingDay", config.AppObject.CreatePacket())
    tgl_transaksi = phTgl.FirstRecord.acc_date
    strDate = config.FormatDateTime('dd-mmm-yyyy', tgl_transaksi)
    
    dsql = {
      'pastDuePrincipal' : pastDueHlp.getColPastDuePrincipal(), 
      'pastDueMargin'    : pastDueHlp.getColPastDueMargin(), 
      'outstdPokok'      : self.getColLoanOutstd(), 
      'outstdMargin'     : self.getColOutstdMargin(), 
      'profitAccrue'     : self.getProfitAccrue(), 
      'accounts'         : self.getJoinedAccountTbl(config),
      'otherTables'      : self.getOtherTbl(config),
      'schedTables'      : self.getScheduleTbl(config), 
      'valuta'           : mlu.QuotedStr(params['valuta']),
      'branchcode'       : mlu.QuotedStr(params['branchcode']),
      'accountmanager'   : self.getJoinedAccountManagerTbl(config), 
      'finproduct'       : self.getJoinedProductTbl(config),
      'SchedByDate'      : self.getSchedByDate(config)
    }
    
    if rptType == 'by-margin' :    
      dsql['orderby'] = 'ORDER BY KODE_CABANG, MARGIN_RATE, CUST_NO, LOAN_NO '
    elif rptType == 'by-loan-holder':
      dsql['orderby'] = 'ORDER BY KODE_CABANG, NAMA_REKENING, LOAN_NO '
    elif rptType == 'by-loan-type':
      dsql['orderby'] = 'ORDER BY KODE_CABANG, PROD_CODE, NAMA_REKENING, LOAN_NO '
    elif rptType == 'by-loan-type-k':
      dsql['orderby'] = 'ORDER BY KODE_CABANG, PROD_CODE, CUST_NO, LOAN_NO '
    elif rptType == 'by-ao':
      dsql['orderby'] = 'ORDER BY KODE_CABANG, AMCODE, PROD_CODE, CUST_NO, LOAN_NO '  
    elif rptType == 'lo-open-to-day':
      dsql['orderby'] = 'ORDER BY KODE_CABANG, CUST_NO, LOAN_NO '
    elif rptType == 'lo-closed-ayda' :
      dsql['orderby'] = 'ORDER BY KODE_CABANG, CUST_NO, LOAN_NO '
    elif rptType == 'lo-lancar' :
      dsql['orderby'] = 'ORDER BY KODE_CABANG, CUST_NO, LOAN_NO '
    elif rptType == 'lo-tunggak' :
      dsql['orderby'] = 'ORDER BY KODE_CABANG, CUST_NO, LOAN_NO '
    elif rptType == 'lo-write-off' :
      dsql['orderby'] = 'ORDER BY KODE_CABANG, CUST_NO, LOAN_NO '
    elif rptType == 'lo-recovery-wr-off' :
      dsql['orderby'] = 'ORDER BY KODE_CABANG, CUST_NO, LOAN_NO '
    elif rptType == 'lo-detail-ayda' :
      dsql['orderby'] = 'ORDER BY KODE_CABANG, CUST_NO, LOAN_NO '
    elif rptType == 'all':
      dsql['orderby'] = 'ORDER BY KODE_CABANG, PROD_CODE'  
    else : raise Exception, "getSql must be customized for rptType '%s'" % rptType       
    
    if rptType == 'lo-open-to-day':
      dsql['conditions'] = self.whereLoOpenToDay()
    elif rptType == 'lo-closed-ayda' :
      dsql['conditions'] = self.whereLoClosedToday()
    elif rptType == 'lo-lancar' :
      dsql['conditions'] = self.whereLoLancar()
    elif rptType == 'lo-tunggak' :
      dsql['conditions'] = self.whereLoTunggak()
    elif rptType == 'lo-write-off' :
      dsql['conditions'] = self.whereLoWO()
    elif rptType == 'lo-recovery-wr-off' :
      dsql['conditions'] = self.whereLoRecoveryWO()    
    elif rptType == 'lo-detail-ayda' :
      dsql['conditions'] = self.whereLoDetailAYDA()
    else:
      dsql['conditions'] = self.whereLoGlobal()
    #--
    
    sql = '''SELECT DISTINCT
       RT.KODE_CABANG AS KODE_CABANG,
       %(pastDuePrincipal)s AS PAST_DUE_PRINCIPAL, 
       %(pastDueMargin)s AS PAST_DUE_MARGIN,
       %(outstdPokok)s AS OUTSTD_PRINCIPAL,
       %(outstdMargin)s AS OUTSTD_MARGIN, 
       %(profitAccrue)s AS PROFIT_ACCRUE, 
       RT."NAMA_REKENING" AS LOAN_HOLDER,
       RC."NOMOR_NASABAH" AS CUST_NO,
       case when FA.contracttype_code in ('B','C') then 
         (case when nvl(PSD.nisbah_bank,0) = 0 then FSD.profit_ratio else PSD.nisbah_bank end) 
       else FA."TARGETED_EQV_RATE" end AS MARGIN_RATE,             
       FA."NOMOR_REKENING" AS LOAN_NO,
       FA."PRODUCT_CODE" AS PROD_CODE,
       FA."PRODUCT_CODE" || ' - ' || FP.PRODUCT_NAME AS PROD_NAME,
       FA."DROPPING_DATE" AS START_DATE, 
       FA."DUE_DATE" AS MATURITY_DATE,
       FA."AMCODE" || ' - '|| AM.AGENTNAME AS AMCODE,
       RT."KODE_VALUTA" AS KODE_VALUTA,
       FA."CONTRACTTYPE_CODE" AS KODE_AKAD,
       FA."REPAYMENT_COL_LEVEL" AS KOL,
       FA."OVERALL_COL_LEVEL" AS OVERALL_KOL,
       FSD."PENALTY_COUNTER" AS PENALTY_COUNTER,
       FSD."SCHED_DATE" AS FIRST_PAST_DUE_DATE,
       nvl(FA."RESERVED_COMMON_BALANCE", 0.0) + nvl(FA."RESERVED_LOSS_BALANCE", 0.0) AS PPAP, 
       1 AS COUNT
    FROM
       %(accounts)s
       %(schedTables)s
       %(SchedByDate)s
       %(otherTables)s
       %(accountmanager)s
       %(finproduct)s
    WHERE
       1 = 1
       AND RT.KODE_VALUTA = %(valuta)s
       AND RT.KODE_CABANG = %(branchcode)s
       AND FA.is_external = 'F'
       %(conditions)s
    %(orderby)s
    '''       
    sql = sql % dsql
    #raise Exception, sql
    return sql 
#--


  def getSqlRekap(self, config, rptType, params) :
    mlu = config.ModLibUtils
    pastDueHlp = RptPastDueHelper()
    
    dsql = {
      'pastDuePrincipal' : pastDueHlp.getColPastDuePrincipal(), 
      'pastDueMargin'    : pastDueHlp.getColPastDueMargin(), 
      'outstdPokok'      : self.getColLoanOutstd(), 
      'outstdMargin'     : self.getColOutstdMargin(), 
      'accounts'         : self.getJoinedAccountTbl(config),
      'otherTables'      : self.getOtherTbl(config),
      'schedTables'      : self.getScheduleTbl(config), 
      'valuta'           : mlu.QuotedStr(params['valuta']),
      'branchcode'       : mlu.QuotedStr(params['branchcode']),
      'accountmanager'   : self.getJoinedAccountManagerTbl(config), 
      'finproduct'       : self.getJoinedProductTbl(config),
    }
    
    if rptType == 'by-margin':
      dsql['groupfield'] = '''FA."TARGETED_EQV_RATE"'''
    elif rptType == 'by-loan-holder' :
      dsql['groupfield'] = '''RT."NAMA_REKENING"''' 
    elif rptType == 'by-loan-type' :
      dsql['groupfield'] = '''FA."PRODUCT_CODE" || ' - ' || FP.PRODUCT_NAME'''
    elif rptType == 'by-col' :
      dsql['groupfield'] = '''FA."OVERALL_COL_LEVEL"'''
    #--
    
    if rptType == 'by-col':
      dsql['condition'] = '''AND FA."OVERALL_COL_LEVEL" > 1'''
    else:
      dsql['condition'] = ''
      
    sql = '''SELECT DISTINCT
       sum(%(pastDuePrincipal)s) AS PAST_DUE_PRINCIPAL,
       --sum(FA.ARREAR_BALANCE) AS PAST_DUE_PRINCIPAL, 
       sum(%(pastDueMargin)s) AS PAST_DUE_MARGIN,
       sum(%(outstdPokok)s) AS OUTSTD_PRINCIPAL,
       sum(%(outstdMargin)s) AS OUTSTD_MARGIN, 
       %(groupfield)s AS GROUPFIELD,
       count(1) as COUNT
    FROM
       %(accounts)s
       %(schedTables)s
       %(otherTables)s
       %(accountmanager)s
       %(finproduct)s
    WHERE
       1 = 1
       AND RT.KODE_VALUTA = %(valuta)s
       AND RT.KODE_CABANG = %(branchcode)s
       AND FA.is_external = 'F'
       AND RT.STATUS_REKENING <> 3
       %(condition)s
    group by %(groupfield)s
    order by %(groupfield)s
    '''       
    sql = sql % dsql
    #raise Exception, sql
    return sql 
#--
class RptMaturityProfileHelper(RptSqlHelper) :
  def getColMaturity(self, date):
    dsql = {'date':date} 
    sql = '''sum(FSD.principal_amount + FSD.profit_amount) as total,
      sum(
        case 
          when months_between(to_date(%(date)s, 'YYYY-MM-DD'), FSD.SCHED_DATE) <=  1 
            then FSD.principal_amount + FSD.profit_amount
          else 0.0
        end
      ) as mo0_1,
      sum(
        case 
          when months_between(to_date(%(date)s, 'YYYY-MM-DD'), FSD.SCHED_DATE) > 1 AND months_between(to_date(%(date)s, 'YYYY-MM-DD'), FSD.SCHED_DATE) <= 3 
            then FSD.principal_amount + FSD.profit_amount
          else 0.0
        end
      ) as mo1_3,
      sum(
        case 
          when months_between(to_date(%(date)s, 'YYYY-MM-DD'), FSD.SCHED_DATE) > 3 AND months_between(to_date(%(date)s, 'YYYY-MM-DD'), FSD.SCHED_DATE) <= 6
            then FSD.principal_amount + FSD.profit_amount
          else 0.0
        end
      ) as mo3_6,
      sum(
        case
          when months_between(to_date(%(date)s, 'YYYY-MM-DD'), FSD.SCHED_DATE) > 6 AND months_between(to_date(%(date)s, 'YYYY-MM-DD'), FSD.SCHED_DATE) <= 12
            then FSD.principal_amount + FSD.profit_amount
          else 0.0
        end
      ) as mo6_12,
      sum(
        case
          when months_between(to_date(%(date)s, 'YYYY-MM-DD'), FSD.SCHED_DATE) > 12
            then FSD.principal_amount + FSD.profit_amount
          else 0.0
        end
      ) as mo12_n
    ''' % dsql
    return sql
  
  def getSqlPart(self, isMature, config, rptType, params):
    mlu = config.ModLibUtils
    colismatured = 1 if isMature else 0
    date = self.dateFloatToStr(params['tgl_report'], config)
    cond = "FSD.ARREAR = 'T' " if isMature else "FSD.ARREAR <> 'T' " 
    date = mlu.QuotedStr(date) 
    dsql = {'date': date, 
      'maturitycols': self.getColMaturity(date),
      'colismatured': colismatured, 'cond' : cond,  
      'valuta' : mlu.QuotedStr(params['valuta']), 'branchcode': mlu.QuotedStr(params['branchcode']),
      'FINPAYMENTSCHEDULE': config.MapDBTableName('financing.FINPAYMENTSCHEDULE'),
      'FINACCOUNT': config.MapDBTableName('financing.FINACCOUNT'),
      'FINPAYMENTSCHEDDETAIL': config.MapDBTableName('financing.FINPAYMENTSCHEDDETAIL'),
      'FINPRODUCT': config.MapDBTableName('financing.FINPRODUCT'),
      'REKENINGTRANSAKSI': config.MapDBTableName('core.REKENINGTRANSAKSI'),
      'REKENINGCUSTOMER': config.MapDBTableName('core.REKENINGCUSTOMER')
      }
    if rptType == 'detail' : 
      sql = '''
      SELECT
        RT.KODE_CABANG AS BRANCHCODE,
        FP."PRODUCT_NAME" AS PROD_NAME,
        FA."PRODUCT_CODE" AS PROD_CODE,
        FA."NOMOR_REKENING" AS LOAN_NO,
        RC."NOMOR_NASABAH" AS CUST_NO,
        RT."NAMA_REKENING" AS CUST_NAME,
        %(colismatured)s AS IS_MATURE,
        %(maturitycols)s
      FROM
           %(FINPAYMENTSCHEDULE)s FS INNER JOIN %(FINACCOUNT)s FA ON FS."ID_SCHEDULE" = FA."ID_SCHEDULE"
           INNER JOIN %(FINPAYMENTSCHEDDETAIL)s FSD ON FS."ID_SCHEDULE" = FSD."ID_SCHEDULE"
           INNER JOIN %(FINPRODUCT)s FP ON FA."PRODUCT_CODE" = FP."PRODUCT_CODE"
           INNER JOIN %(REKENINGTRANSAKSI)s RT ON FA."NOMOR_REKENING" = RT."NOMOR_REKENING"
           INNER JOIN %(REKENINGCUSTOMER)s RC ON RT."NOMOR_REKENING" = RC."NOMOR_REKENING"
      WHERE
           (1 = 1)
           AND %(cond)s
           AND FSD.SEQ_NUMBER > 0
           AND RT.KODE_VALUTA = %(valuta)s
           AND RT.KODE_CABANG = %(branchcode)s
           AND FA.is_external = 'F'
      GROUP BY
           RT.KODE_CABANG,
           FP."PRODUCT_NAME", 
           FA."PRODUCT_CODE",
           FA."NOMOR_REKENING", 
           RC."NOMOR_NASABAH",
           RT."NAMA_REKENING"        
      '''
    elif rptType == 'rekap':
      sql = '''
      SELECT
        RT.KODE_CABANG AS BRANCHCODE,
        FP."PRODUCT_NAME" AS PROD_NAME,
        FA."PRODUCT_CODE" AS PROD_CODE,
        %(colismatured)s AS IS_MATURE,
        %(maturitycols)s
      FROM
           %(FINPAYMENTSCHEDULE)s  FS INNER JOIN %(FINACCOUNT)s FA ON FS."ID_SCHEDULE" = FA."ID_SCHEDULE"
           INNER JOIN %(FINPAYMENTSCHEDDETAIL)s FSD ON FS."ID_SCHEDULE" = FSD."ID_SCHEDULE"
           INNER JOIN %(FINPRODUCT)s FP ON FA."PRODUCT_CODE" = FP."PRODUCT_CODE"
           INNER JOIN %(REKENINGTRANSAKSI)s RT ON FA."NOMOR_REKENING" = RT."NOMOR_REKENING"
           INNER JOIN %(REKENINGCUSTOMER)s RC ON RT."NOMOR_REKENING" = RC."NOMOR_REKENING"
      WHERE
           (1 = 1)
           AND %(cond)s
           AND FSD.SEQ_NUMBER > 0
           AND RT.KODE_VALUTA = %(valuta)s
           AND RT.KODE_CABANG = %(branchcode)s
           AND FA.is_external = 'F'
      GROUP BY
           RT.KODE_CABANG,
           FP."PRODUCT_NAME", 
           FA."PRODUCT_CODE"      
      '''
    sql = sql % dsql
    #raise Exception, sql       
    return sql
    
  def getSql(self, config, rptType, params) :
    dsql = {'unmature': self.getSqlPart(False,  config, rptType, params)
    , 'mature': self.getSqlPart(True,  config, rptType, params)}
    if rptType == 'detail' :
      sql = '''SELECT * FROM 
      (
         %(unmature)s
         UNION
         %(mature)s           
      )
      ORDER BY
           PROD_CODE, CUST_NO, LOAN_NO, IS_MATURE
      ''' % dsql
    elif rptType == 'rekap' :
      sql = '''SELECT * FROM 
      (
         %(unmature)s
         UNION
         %(mature)s           
      )
      ORDER BY
           PROD_CODE
      ''' % dsql
    else : raise Exception, "getSql muist be defined fpr rptType %s" % rptType  
    return sql
    
class RptCollateralHelper(RptSqlHelper) :
  def getSql(self, config, rptType, params) :
    mlu = config.ModLibUtils
    pastDueHlp = RptPastDueHelper()
    dsql = {
      'accounts' : self.getJoinedAccountTbl(config)
      , 'otherTables' : self.getOtherTbl(config)
      , 'schedTables' : self.getScheduleTbl(config)
      , 'collateralTables' : self.getCollateralTbl(config)
      , 'valuta' : mlu.QuotedStr(params['valuta'])
      , 'branchcode': mlu.QuotedStr(params['branchcode'])
    }
    if rptType in ('by-collateral_code', 'by-collateral_type'):    
      dsql['orderby'] = 'ORDER BY ASSET_TYPE,nomor_jaminan,LOAN_HOLDER '
    elif rptType == 'all' :    
      dsql['orderby'] = 'ORDER BY LOAN_NO'
    else : raise Exception, "getSql must be customized for rptType '%s'" % rptType       
    sql = '''SELECT
       RT.KODE_CABANG AS KODE_CABANG,
       CO.NOMOR_REKENING as nomor_jaminan,                 
       CO.ASSET_TYPE as AT,
       CO.LIQUIDATION_VALUE,
       CO.VALUATION,
       CO.MARKET_VALUE,
       
       CA.paripasu as paripasu_legal,
       CA.paripasu_liquidation_value as paripasu_taksasi,
       CA.paripasu_market_value as paripasu_pasar,
       
       --EV."ENUM_DESCRIPTION" AS ASSET_TYPE,
       CASE WHEN GAT.ENUM_DESCRIPTION is null then EV.ENUM_DESCRIPTION 
         ELSE GAT.ENUM_DESCRIPTION ||' - '||EV.ENUM_DESCRIPTION END as ASSET_TYPE,

       CO.BUKTI_KEPEMILIKAN AS BUKTI_KEPEMILIKAN,
       CO.value_use  AS COLLATERAL_AMOUNT,
       RT."NAMA_REKENING" AS LOAN_HOLDER,
       RC."NOMOR_NASABAH" AS CUST_NO,
       FA."TARGETED_EQV_RATE" AS MARGIN_RATE,             
       FA."NOMOR_REKENING" AS LOAN_NO,
       FA."LOAN_AMOUNT" AS LOAN_AMOUNT,
       FS."DROPPING_DATE" AS START_DATE, 
       FA."DUE_DATE" AS MATURITY_DATE,
       FA."AMCODE" AS AMCODE,
       RT."KODE_VALUTA" AS KODE_VALUTA,
       FA."CONTRACTTYPE_CODE" AS KODE_AKAD,
       CO.LAST_APPRAISAL_TYPE,
       CO.LAST_VALUATION_DATE,
       1 AS COUNT
    FROM
       %(accounts)s
       %(schedTables)s
       %(otherTables)s
       %(collateralTables)s
    WHERE
      (CO.group_code ='' OR CO.group_code is null)
      AND RT.KODE_CABANG = %(branchcode)s
    %(orderby)s
    '''       
    sql = sql % dsql
    #raise Exception, sql
    return sql 
#--

  def getSqlRekap(self, config, rptType, params) :
    mlu = config.ModLibUtils
    pastDueHlp = RptPastDueHelper()
    dsql = {'asset_type' : self.getAssetType() , 'collateral_account' : self.getCollateralAmountUse(), 'accounts' : self.getJoinedAccountTbl(config)
    , 'otherTables' : self.getOtherTbl(config), 'schedTables' : self.getScheduleTbl(config), 'collateralTables' : self.getCollateralTbl(config)
    , 'valuta' : mlu.QuotedStr(params['valuta']), 'branchcode': mlu.QuotedStr(params['branchcode'])}

    if rptType in ('by-code'):
      dsql['groupfield'] = ''' EV."ENUM_DESCRIPTION" '''    
    else : raise Exception, "getSql must be customized for rptType '%s'" % rptType
           
    sql = '''SELECT
       %(groupfield)s AS GROUPFIELD,
       SUM(CO.LIQUIDATION_VALUE) AS LIQUIDATION_VALUE,
       SUM(CO.VALUATION) AS VALUATION,
       SUM(CO.MARKET_VALUE) AS MARKET_VALUE,
       COUNT(1) AS COUNT
    FROM
       %(accounts)s
       %(schedTables)s
       %(otherTables)s
       %(collateralTables)s
                           
    GROUP BY  %(groupfield)s
    ORDER BY %(groupfield)s
    '''       
    sql = sql % dsql
    #raise Exception, sql
    return sql 
#--

class RptTrPembiayaanHelper(RptSqlHelper) :
 
  def getSql(self, config, rptType, params) :
    mlu = config.ModLibUtils
    pastDueHlp = RptPastDueHelper()
    dsql = { 'accounts' : self.getJoinedAccountTbl(config), 'valuta' : mlu.QuotedStr(params['valuta']), 'branchcode': mlu.QuotedStr(params['branchcode']),
      'otherTables' : self.getOtherTbl(config),  'transaksiTables' : self.getTransaksiTbl(config)
    }
    if rptType == 'tr_pembiayaan' :    
      dsql['orderby'] = 'order by loan_no, id_transaksi' 
    else : raise Exception, "getSql must be customized for rptType '%s'" % rptType       
    sql = '''
    SELECT
      RT.NAMA_REKENING AS LOAN_HOLDER,
      RC.NOMOR_NASABAH AS CUST_NO,         
      FA.NOMOR_REKENING AS LOAN_NO,
      TX.tanggal_transaksi,
      TX.keterangan,
      DT.jenis_mutasi,
      DTC.deskripsi,
      DT.nilai_mutasi,
      DT.kode_valuta,      
      TX.journal_no,
      TX.id_transaksi
    FROM
      %(accounts)s  
      %(otherTables)s 
      %(transaksiTables)s
    WHERE
      (1 = 1)
      AND ( 'ALL'=%(valuta)s or RT.KODE_VALUTA = %(valuta)s )
      AND ( 'ALL'=%(branchcode)s or RT.KODE_CABANG = %(branchcode)s )
      AND FA.is_external = 'F'
    %(orderby)s
    '''       
    sql = sql % dsql
    #raise Exception, sql
    return sql 
#--

class RptHistTrPembiayaanHelper(RptSqlHelper) :
 
  def getSql(self, config, rptType, params) :
    mlu = config.ModLibUtils
    pastDueHlp = RptPastDueHelper()
    dsql = { 'accounts' : self.getJoinedAccountTbl(config), 'valuta' : mlu.QuotedStr(params['valuta']), 'branchcode': mlu.QuotedStr(params['branchcode']),
      'otherTables' : self.getOtherTbl(config),  'histTransaksiTables' : self.getHistTransaksiTbl(config)
    }
    if rptType == 'hist_tr_pembiayaan' :                              
      dsql['orderby'] = 'order by loan_no, id_transaksi' 
    else : raise Exception, "getSql must be customized for rptType '%s'" % rptType       
    sql = '''
    SELECT
      RT.NAMA_REKENING AS LOAN_HOLDER,
      RC.NOMOR_NASABAH AS CUST_NO,         
      FA.NOMOR_REKENING AS LOAN_NO,
      TX.tanggal_transaksi,
      TX.keterangan,
      DT.jenis_mutasi,
      DTC.deskripsi,
      DT.nilai_mutasi,
      DT.kode_valuta,      
      TX.journal_no,
      TX.id_transaksi
    FROM
      %(accounts)s  
      %(otherTables)s 
      %(histTransaksiTables)s
    WHERE
           (1 = 1)
           AND RT.KODE_VALUTA = %(valuta)s
           AND RT.KODE_CABANG = %(branchcode)s
           AND RT.STATUS_REKENING <> 3
           AND FA.is_external = 'F'
    %(orderby)s
    '''       
    sql = sql % dsql
    #raise Exception, sql
    return sql 
#--

class RptFacilityOutstandingHelper(RptSqlHelper) :
  def getType(self): ## outstanding pokok
    sql ='''
    CASE 
      WHEN FI.CONTRACTTYPE_CODE='A' THEN 
        'Murabahah Waad' 
      WHEN FI.CONTRACTTYPE_CODE='B' THEN 
        'Musyarakah'
      WHEN FI.CONTRACTTYPE_CODE='C' THEN 
        'Mudharabah'
      WHEN FI.CONTRACTTYPE_CODE='D' THEN
        'Qardh'                                                                   
    END
    '''
    return sql  
  def getColLoanOutstd(self): ## outstanding pokok
    sql ='''
    CASE 
      WHEN FA."CONTRACTTYPE_CODE"='A' THEN 
        RT.SALDO + FA.ARREAR_BALANCE + MBH.MMD_BALANCE + FA.PROFIT_ARREAR_BALANCE       
      WHEN FA."CONTRACTTYPE_CODE"='D' THEN 
        RT.SALDO + FA.ARREAR_BALANCE + QRD.MMD_BALANCE + FA.PROFIT_ARREAR_BALANCE 
      WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G') THEN 
        RT.SALDO + FA.ARREAR_BALANCE 
      ELSE 0 
    END
    '''
    return sql
    
  def getColOutstdMargin(self):
    sql = '''CASE 
      WHEN FA."CONTRACTTYPE_CODE"='A' THEN 
        MBH.MMD_BALANCE + FA.PROFIT_ARREAR_BALANCE        
      WHEN FA."CONTRACTTYPE_CODE"='D' THEN 
        QRD.MMD_BALANCE + FA.PROFIT_ARREAR_BALANCE 
      WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G') THEN 
        MSY.TOTAL_PROJECTED_INCOME - FA.PROFIT_BALANCE 
      ELSE 0 
    END
    '''
    return sql
    
  def getSql(self, config, rptType, params) :
    mlu = config.ModLibUtils
    pastDueHlp = RptPastDueHelper()
    dsql = {'accounts' : self.getJoinedAccountTbl(config),'type' : self.getType() ,'facility' : self.getFacilityTbl(config), 'otherTables' : self.getOtherTbl2(config)
    , 'valuta' : mlu.QuotedStr(params['valuta']), 'branchcode': mlu.QuotedStr(params['branchcode']) }
    if rptType == 'by-type' :    
      dsql['orderby'] = 'ORDER BY KODE_CABANG, TYPE, CUST_NO, FACILITY_NO '
    else : raise Exception, "getSql must be customized for rptType '%s'" % rptType       
    sql = '''SELECT
       FI.KODE_CABANG AS KODE_CABANG,
       %(type)s AS TYPE,
       N.NAMA_NASABAH AS facility_holder,
       FI.NO_NASABAH AS cust_no,
       FI.facility_no AS facility_no,             
       FI.total_facility_limit AS total_facility_limit,
       FI.total_facility_used AS total_facility_used,
       FI.total_facility_limit - FI.total_facility_used AS total_facility_unused, 
       FI.dropping_due_date AS tgl_kelonggaran_tarik,
       FI.facility_due_date AS tgl_jt_facility,
       1 AS COUNT
    FROM
       %(facility)s
       %(otherTables)s
    WHERE
           (1 = 1)
           AND FI.CURRENCY_CODE = %(valuta)s
           AND FI.KODE_CABANG = %(branchcode)s
           AND FI.STATUS <> 'T' 
           AND not exists (select 1 from  %(accounts)s  where FA.FACILITY_NO = FI.FACILITY_NO and FA.is_external = 'T')
    %(orderby)s
    '''       
    sql = sql % dsql
    #raise Exception, sql
    return sql 

#--


class RptLoanOutstandingKonsolidasiHelper(RptSqlHelper) :
  def getColLoanOutstd(self): ## outstanding pokok
    sql ='''
    CASE 
      WHEN FA."CONTRACTTYPE_CODE" in ('A','H') THEN 
        RT.SALDO + FA.ARREAR_BALANCE + MBH.MMD_BALANCE + FA.PROFIT_ARREAR_BALANCE        
      WHEN FA."CONTRACTTYPE_CODE"='D' THEN 
        RT.SALDO + FA.ARREAR_BALANCE + QRD.MMD_BALANCE + FA.PROFIT_ARREAR_BALANCE  
      WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G') THEN 
        RT.SALDO + FA.ARREAR_BALANCE 
      ELSE 0 
    END
    '''
    return sql
    
  def getColOutstdMargin(self):
    sql = '''CASE 
      WHEN FA."CONTRACTTYPE_CODE" in ('A','H') THEN 
        MBH.MMD_BALANCE + FA.PROFIT_ARREAR_BALANCE        
      WHEN FA."CONTRACTTYPE_CODE"='D' THEN 
        QRD.MMD_BALANCE + FA.PROFIT_ARREAR_BALANCE  
      WHEN FA."CONTRACTTYPE_CODE" IN('B','C','E','G') THEN 
        --MSY.TOTAL_PROJECTED_INCOME - FA.PROFIT_BALANCE
        0 
      ELSE 0 
    END
    '''
    return sql
    
  def getSql(self, config, rptType, params) :
    mlu = config.ModLibUtils
    pastDueHlp = RptPastDueHelper()
    dsql = {'pastDuePrincipal': pastDueHlp.getColPastDuePrincipal(), 'pastDueMargin': pastDueHlp.getColPastDueMargin()
    , 'outstdPokok' : self.getColLoanOutstd(), 'outstdMargin': self.getColOutstdMargin()
    , 'accounts' : self.getJoinedAccountTbl(config), 'otherTables' : self.getOtherTbl(config), 'schedTables' : self.getScheduleTbl(config)
    , 'valuta' : mlu.QuotedStr(params['valuta']), 'branchcode': mlu.QuotedStr(params['branchcode']), 'accountmanager' : self.getJoinedAccountManagerTbl(config)
    , 'finproduct': self.getJoinedProductTbl(config) }
    sql =''
    if rptType == 'by-loan-type-k':
      dsql['orderby'] = 'ORDER BY KODE_CABANG, PROD_CODE '
      sql = '''
        SELECT
           RT.KODE_CABANG AS KODE_CABANG,
           FA."PRODUCT_CODE" AS PROD_CODE,
           FP."PRODUCT_NAME" AS PROD_NAME,
           RT."KODE_VALUTA" AS KODE_VALUTA,
           -sum(RT.SALDO + FA.ARREAR_BALANCE ) AS OS_PIUTANG,
           sum(MBH.MMD_BALANCE) as OS_MARGIN, 
           -sum(FA."ARREAR_BALANCE") as PD_PIUTANG,
           -sum(FA."ARREAR_BALANCE"+FA.PROFIT_ARREAR_BALANCE) as PD_POKOK,
           sum(FA."PROFIT_ARREAR_BALANCE") as PD_MARGIN ,
           count (FA."PRODUCT_CODE") as count
        FROM
          %(accounts)s
          %(otherTables)s
          %(finproduct)s
        where 
           RT.KODE_VALUTA = %(valuta)s
           AND RT.KODE_CABANG = %(branchcode)s
           AND RT.STATUS_REKENING <> 3
           AND FA."CONTRACTTYPE_CODE" in ('A','D','H')
        group by RT.KODE_CABANG,  FA.PRODUCT_CODE, FP.PRODUCT_NAME, RT."KODE_VALUTA"
        
        UNION
              
        SELECT
          RT.KODE_CABANG AS KODE_CABANG,
          FA."PRODUCT_CODE" AS PROD_CODE,
          FP."PRODUCT_NAME" AS PROD_NAME,
          RT."KODE_VALUTA" AS KODE_VALUTA,
          -sum(RT.SALDO + FA.ARREAR_BALANCE) AS OS_PIUTANG,
          0 as OS_MARGIN, 
          -sum(FA."ARREAR_BALANCE") as PD_PIUTANG,
          -sum(FA."ARREAR_BALANCE"-FA.PROFIT_ARREAR_BALANCE) as PD_POKOK,
          -sum(FA."PROFIT_ARREAR_BALANCE") as PD_MARGIN ,
          count (FA."PRODUCT_CODE") as count
        FROM
          %(accounts)s
          %(otherTables)s
          %(finproduct)s
        where 
          RT.KODE_VALUTA = %(valuta)s
          AND RT.KODE_CABANG = %(branchcode)s
          AND RT.STATUS_REKENING <> 3
          AND FA."CONTRACTTYPE_CODE" IN('B','C','E','G','F')
        group by RT.KODE_CABANG,  FA.PRODUCT_CODE, FP.PRODUCT_NAME, RT."KODE_VALUTA"
      '''       
      sql = sql % dsql
    else : raise Exception, "getSql must be customized for rptType '%s'" % rptType       
    #raise Exception, sql
    return sql 
    
  def getSql2(self, config, rptType, params) :
    mlu = config.ModLibUtils
    pastDueHlp = RptPastDueHelper()
    date = self.dateFloatToStr(params['tanggal'], config)
    dsql = {'pastDuePrincipal': pastDueHlp.getColPastDuePrincipal(), 'pastDueMargin': pastDueHlp.getColPastDueMargin()
    , 'outstdPokok' : self.getColLoanOutstd(), 'outstdMargin': self.getColOutstdMargin()
    , 'accounts' : self.getJoinedAccountTblDaily(date,config), 'otherTables' : self.getOtherTbl(config), 'schedTables' : self.getScheduleTbl(config)
    , 'valuta' : mlu.QuotedStr(params['valuta']), 'branchcode': mlu.QuotedStr(params['branchcode']), 'accountmanager' : self.getJoinedAccountManagerTbl(config),
    'FINPRODUCT': config.MapDBTableName('financing.FINPRODUCT') }
    if rptType == 'by-loan-type-k':
      dsql['orderby'] = 'ORDER BY KODE_CABANG, PROD_CODE '
    else : raise Exception, "getSql must be customized for rptType '%s'" % rptType       
    sql = '''
      SELECT
             RT.KODE_CABANG AS KODE_CABANG,
             FA."PRODUCT_CODE" AS PROD_CODE,
             FP."PRODUCT_NAME" AS PROD_NAME,
             RT."KODE_VALUTA" AS KODE_VALUTA,
            sum(FA1.P_SALDO + FA1.P_ARREAR_BALANCE )       
             AS OS_PIUTANG,
          sum(FA1.P_MMD_BALANCE)
          as OS_MARGIN, 
      sum(-1 * (FA1."P_ARREAR_BALANCE"))
       as PD_PIUTANG,
         count (FA."PRODUCT_CODE") as count
        FROM
        %(accounts)s
        %(otherTables)s
        INNER JOIN  %(FINPRODUCT)s FP ON FA.product_code = FP.product_code
      where 
           RT.KODE_VALUTA = %(valuta)s
           AND RT.KODE_CABANG = %(branchcode)s
                 AND RT.STATUS_REKENING <> 3
                 AND FA."CONTRACTTYPE_CODE"='A'
      group by RT.KODE_CABANG,  FA.PRODUCT_CODE, FP.PRODUCT_NAME, RT."KODE_VALUTA"
      
      UNION
      
      SELECT
             RT.KODE_CABANG AS KODE_CABANG,
             FA."PRODUCT_CODE" AS PROD_CODE,
             FP."PRODUCT_NAME" AS PROD_NAME,
             RT."KODE_VALUTA" AS KODE_VALUTA,
            sum(FA1.P_SALDO + FA1.P_ARREAR_BALANCE  )       
             AS OS_PIUTANG,
          sum(FA1.P_MMD_BALANCE)
          as OS_MARGIN, 
      sum(-1 * (FA1."P_ARREAR_BALANCE"))
       as PD_PIUTANG,
         count (FA."PRODUCT_CODE") as count
        FROM
        %(accounts)s
        %(otherTables)s
        INNER JOIN  %(FINPRODUCT)s FP ON FA.product_code = FP.product_code
      where 
           RT.KODE_VALUTA = %(valuta)s
           AND RT.KODE_CABANG = %(branchcode)s
                 AND RT.STATUS_REKENING <> 3
                 AND FA."CONTRACTTYPE_CODE"='D'
      group by RT.KODE_CABANG,  FA.PRODUCT_CODE, FP.PRODUCT_NAME, RT."KODE_VALUTA"
      
      UNION
      
      SELECT
             RT.KODE_CABANG AS KODE_CABANG,
             FA."PRODUCT_CODE" AS PROD_CODE,
             FP."PRODUCT_NAME" AS PROD_NAME,
             RT."KODE_VALUTA" AS KODE_VALUTA,
            sum(FA1.P_SALDO + FA1.P_ARREAR_BALANCE)       
             AS OS_PIUTANG,
          0
          as OS_MARGIN, 
      sum(FA1."P_ARREAR_BALANCE")
       as PD_PIUTANG,
         count (FA."PRODUCT_CODE") as count
        FROM
        %(accounts)s
        %(otherTables)s
        INNER JOIN  %(FINPRODUCT)s FP ON FA.product_code = FP.product_code
      where 
           RT.KODE_VALUTA = %(valuta)s
           AND RT.KODE_CABANG = %(branchcode)s
                 AND RT.STATUS_REKENING <> 3
                 AND FA."CONTRACTTYPE_CODE" IN('B','C','E','G')
      group by RT.KODE_CABANG,  FA.PRODUCT_CODE, FP.PRODUCT_NAME, RT."KODE_VALUTA"
    '''       
    sql = sql % dsql
    #raise Exception, sql
    return sql 
    
  def dateFloatToStr(self, data, config=None): # return YYYY-MM-DD
    from datetime import datetime
    import types
    
    sValidValue = ''
    if data!=None:
      try:
        if type(data) == types.FloatType and config not in [None]:
          y,m,d = config.ModDateTime.DecodeDate(data)
          
          sValidValue = "%04d-%02d-%02d" % (y,m,d)
          
          datetime.strptime(sValidValue, '%Y-%m-%d')
      except:
        sValidValue = ''    
    return sValidValue
#-- 