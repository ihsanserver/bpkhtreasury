# corporate.py

import com.ihsan.foundation.mobject as mobject
import com.ihsan.net.socketclient as socket
import os, sys

class record:
    pass

class BatchProcess(mobject.MObject):
    
    def mobject_init(self, parameter):
        info = parameter.info.GetRecord(0)
        
        self.main_pid = info.main_pid
        self.step_id  = info.step_id
        
        # prepare connection
        destination = info.notify_host, info.notify_port
        self.tcp = socket.TCPClient_STX_ETX(destination)        
        
        self.params = parameter.parameters
    
    def Notify_OK(self):
        self.Notify(1, "")
    
    def Notify_Error(self):
        errInfo = str(sys.exc_info()[0]) + "." + str(sys.exc_info()[1])
        self.Notify(2, errInfo)
        
    def Notify(self, status, errInfo):
        sMessage = "|".join([
            str(self.main_pid),
            str(self.Config.SecurityContext.pid),
            str(self.step_id),
            str(status),
            errInfo
        ])
        
        tcp = self.tcp
        tcp.Send(sMessage)
        sReply = tcp.Receive()
        replies = sReply.split("|")
        if replies[0] == "0":
            raise Exception, "Notify service error:" +  replies[1]
    #--def
            
class Corporate(mobject.MObject):
    
    def mobject_init(self, bNotLogin = 0):
        self.HasLogin = not bNotLogin
        self.AutoResponseError = 0
        self.Ls_LimitTransaksi = None

        # Load User Info
        UserId = self.Config.SecurityContext.UserId
        User = self.Helper.GetObject('Enterprise.User', UserId)
        if User.isnull:
          raise Exception, 'Enterprise : Data user tidak ditemukan'
          
        self.User = User
        self.LoginContext = User
            
    def SetAutoResponseError(self):
        self.AutoResponseError = 1
        
    def SendRequest(self, request):        
        message = str(request.__dict__)
        
        app = self.Config.AppObject
        ph = app.CreateValues(['data', message])
        ph = app.rexecscript('enterprise', 'request_handler', ph)
        r_message = ph.FirstRecord.data
        
        response = record()        
        response.__dict__ = eval(r_message)
        
        if self.AutoResponseError and response.Is_Err:
            raise Exception, 'Enterprise:' +  response.Err_Message
        #--
    
        return response
    
    def Login(self, userid, password):
        request = record()
        request.Function_Code = 'LOGIN'
        request.userid = userid
        request.password = password
        
        resp = self.SendRequest(request)
        warn = response['warn']
        warnMsg = response['warnMsg']
        
        return warn, warnMsg
        
    def BeforeLogout(self):
        request = record()
        request.Function_Code = 'LOGOUT'
        
        return self.SendRequest(request)
        
    def OnGetUserInfo(self, userid, userinfo):
        request = record()
        request.Function_Code = 'ONGETUSERINFO'
        request.userid = userid
        
        resp = self.SendRequest(request)
        
    def GetUserHomeDir(self):
        config = self.Config
        
        user_dir = config.UserHomeDirectory
        if user_dir[-1:] != os.sep:
          user_dir = user_dir + os.sep
          
        return user_dir
        
    def FunctionAccess(self, id_fungsi):
        request = record()
        request.Function_Code = 'AKSES'
        request.Id_User = self.Config.SecurityContext.UserID
        request.Id_Fungsi = id_fungsi
    
        return self.SendRequest(request)        
    
    def MultiFunctionAccess(self, list_id_fungsi):
        request = record()
        request.Function_Code = 'MULTIAKSES'
        request.Id_User = self.Config.SecurityContext.UserID
        request.List_Id_Fungsi = list_id_fungsi
    
        return self.SendRequest(request)        
    
    def DualControlCheck(self, userid, password, id_fungsi):
        if self.Config.SecurityContext.UserID == userid.upper():
            resp = record()
            resp.Is_Err = 1
            resp.Err_Message = 'User dual control tidak boleh sama dengan user active!'
            
            return resp
            
        request = record()
        request.Function_Code = 'DUALCONTROL'
        request.Id_User = userid
        request.Password = password
        request.Id_Fungsi = id_fungsi
    
        return self.SendRequest(request)        
    
    def OverrideCheck(self, userid, password, id_fungsi, limit_check=0, limit_value=0.0):
        if self.Config.SecurityContext.UserID == userid.upper():
            raise Exception, 'User override tidak boleh sama dengan user active!'
            
        request = record()
        request.Function_Code = 'OVERRIDE'
        request.Id_User = userid
        request.Password = password
        request.Id_Fungsi = id_fungsi
        request.Limit_Check = limit_check
        request.Limit_Value = limit_value

        return self.SendRequest(request)        
    
    def GetUserInfo(self, userid):
        request = record()
        request.Function_Code = 'USER'
        request.Id_User = userid
    
        return self.SendRequest(request)        
    
    def GetCabangInfo(self, kode_cabang):
        request = record()
        request.Function_Code = 'CABANG'
        request.Kode_Cabang = kode_cabang
    
        return self.SendRequest(request)        
    
    def GetFungsiInfo(self, id_fungsi):
        request = record()
        request.Function_Code = 'FUNGSI'
        request.Id_Fungsi = id_fungsi
    
        return self.SendRequest(request)        
    
    def GetDepartemenInfo(self, kode_departemen):
        request = record()
        request.Function_Code = 'DEPARTEMEN'
        request.Kode_Departemen = kode_departemen
    
        return self.SendRequest(request)        
    
    def GetKaryawanInfo(self, nomor_karyawan):
        request = record()
        request.Function_Code = 'KARYAWAN'
        request.Nomor_Karyawan = nomor_karyawan
    
        return self.SendRequest(request)        
    
    def GetJabatanInfo(self, kode_jabatan):
        request = record()
        request.Function_Code = 'JABATAN'
        request.Kode_Jabatan = kode_jabatan
    
        return self.SendRequest(request)        
    
    def GetTerminalInfo(self, ip_terminal):
        request = record()
        request.Function_Code = 'TERMINAL'
        request.IP_Terminal = ip_terminal
    
        return self.SendRequest(request)        
    
    def CheckLimitTransaksi(self, nominal):
        limits = self.Ls_LimitTransaksi
      
        if limits == None:
          user = self.Helper.GetObject('Enterprise.User', 
            self.Config.SecurityContext.UserID)
          limits = user.GetListLimitTransaksi()
          self.Ls_LimitTransaksi = limits
        #--
        
        if not limits.has_key('T'):
            raise Exception, 'Enterprise. User tidak memiliki limit transaksi'
        #--
            
        limit = limits['T']
        
        return (nominal <= limit[0])
    
    def CheckLimitOtorisasi(self, nominal):
        limits = self.Ls_LimitTransaksi
      
        if limits == None:
          user = self.Helper.GetObject('Enterprise.User', 
            self.Config.SecurityContext.UserID)
          limits = user.GetListLimitTransaksi()
          self.Ls_LimitTransaksi = limits
        #--
        
        if not limits.has_key('O'):
            raise Exception, 'Enterprise. User tidak memiliki limit otorisasi transaksi'
        #--
        
        limit = limits['O']
        
        return (nominal <= limit[0])