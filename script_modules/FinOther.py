import com.ihsan.foundation.pobject as pobject
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.modman as modman
import com.ihsan.net.message as message
import com.ihsan.util.attrutil as atutil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.dbutil as dbutil

#mod_rekening = modman.getRefModule(appserver.ActiveConfig, "core", "mod_rekening")

def dbg(msg):
  message.send_udp(msg + "\n", 'localhost', 9123)

class FinInsurance(pobject.PObject):
  # Static variables
  pobject_classname = 'FinInsurance'
  pobject_keys      = ['insurance_id']
  
  def OnCreate(self, param):
    self.Save(param)
    pass
    
  def Save(self, param):
    #self.
    config = self.Config
    helper = self.Helper      
    recSrc = param['recFinInsurance']
      
    oUser = helper.GetObject('enterprise.User', config.SecurityContext.initUser)
        
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'insurance_id*',
        'insurance_fee*',
        'insurance_name=LPerusahaanAsuransi.Nama_Singkat',
        'insurance_type*',
        'insurance_kind*',
        'insurance_state*',
        'branch_name=*LCabang.kode_cabang',
        'due_date*',
        'issued_date*',
        'policy_number*',
        'nomor_rekening*',
        'insurance_OrderDate*',
        'shipping_date_SPPA*',
        'tgl_target_pengajuan*',
        'tgl_terima_polis*',
        'JenisMaskapai*',
        'banker_clause*',
        'alih_ke_syariah*',
        'payment_status*',
        'insurance_coverage*',
        'nilai_pertanggungan*',
        'insurance_add_info*'
      ], self, recSrc
    )
    #recSpec = param['recFinInsurance']
    #raise Exception, 'AA'
    #atutil.transferAttributes(helper, self.SPEC_ATTR, self, recSpec)
    #self.applyCustomAttributes(recSpec)
    #self.applyNoBuktiKepemilikan() # virtual method
    #self.applyFirstAppraisal()
    
  def Edit(self, param):
    #self.
    config = self.Config
    helper = self.Helper      
    recSrc = param['recFinInsurance']
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'insurance_fee*',
        'insurance_name=*LPerusahaanAsuransi.Nama_Singkat',
        'insurance_type*',
        'insurance_kind*',
        'insurance_state*',
        'branch_name=*LCabang.kode_cabang',
        'due_date*',
        'issued_date*',
        'policy_number*',
        'nomor_rekening*',
        'insurance_OrderDate*',
        'shipping_date_SPPA*',
        'tgl_target_pengajuan*',
        'tgl_terima_polis*',
        'JenisMaskapai*',
        'banker_clause*',
        'alih_ke_syariah*',
        'payment_status*',
        'insurance_coverage*',
        'nilai_pertanggungan*',
        'insurance_add_info*'
      ], self, recSrc
    )
    #raise Exception, self.branch_name
    #recSpec = param['recFinInsurance']
    #atutil.transferAttributes(helper, self.SPEC_ATTR, self, recSpec)
    
  def Delete(self):
    config = self.Config
    helper = self.Helper      
    self.insurance_state = 'D'
    self.insurance_add_info = self.nomor_rekening
    self.nomor_rekening = None

    
class FinCostElement(pobject.PObject):
  # Static variables
  pobject_classname = 'FinCostElement'
  pobject_keys      = ['cost_element_id']
  
  def OnCreate(self, param):
    self.Save(param)
    pass
    
  def Save(self, param):
    #self.
    config = self.Config
    helper = self.Helper      
    recSrc = param['recData']
      
    oUser = helper.GetObject('enterprise.User', config.SecurityContext.initUser)
    self.date_create = config.Now()
        
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'cost_element_id*',
        'cost_element_name*',
        'cost_description*',
        'is_amortized*'
        #'account_code=LAccount.account_code',
        #'amortization_account_code=LAmortizationAccount.account_code'
      ], self, recSrc
    )
  #--
    
  def Edit(self, param):
    #self.
    config = self.Config
    helper = self.Helper      
    recSrc = param['recData']                                                   
    self.date_modified = config.Now()
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'cost_element_name*',
        'cost_description*',
        'is_amortized*'
        #'account_code=LAccount.account_code',
        #'amortization_account_code=LAmortizationAccount.account_code'
      ], self, recSrc
    )
  #--

  def CostElementCheck(self):
    config = self.Config
    dParams = {
     'finproductcost': config.MapDBTableName("finproductcost"),
     'finacccost': config.MapDBTableName("finacccost"), 
     'idt': self.cost_element_id
    }
    sSQL = '''select count(1) jml_prod from %(finproductcost)s where cost_element_id=%(idt)s''' % dParams 
    q1 = config.CreateSQL(sSQL).RawResult

    sSQL = '''select count(1) jml_acc from %(finacccost)s where cost_element_id=%(idt)s''' % dParams 
    q2 = config.CreateSQL(sSQL).RawResult
    
    ret = (q1.jml_prod or 0)+(q2.jml_acc or 0)
    
    return ret  
  #--
  
  def Hapus(self, param):
    #self.
    config = self.Config
    helper = self.Helper      
    recSrc = param['recData']                                                   
    # Delete cost element
    sSQL ="DELETE FROM %s WHERE cost_element_id = %s " % (config.MapDBTableName("FinCostElement"),recSrc.cost_element_id)
    dbutil.runSQL(config, sSQL) 
    
    
class PenaltyTemplate(pobject.PObject):
  # Static variables
  pobject_classname = 'PenaltyTemplate'
  pobject_keys      = ['id_penalty_template']
      
  def OnCreate(self, param):
    #self.
    config = self.Config
    helper = self.Helper      
    recSrc = param['recData']
      
    oUser = helper.GetObject('enterprise.User', config.SecurityContext.initUser)               
    self.date_create = config.Now()
    
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'id_penalty_template*',
        'penalty_type*',
        'description*',
        'phone_cost*',
        'transport_cost*',
        'salary_cost*',
        'salary_cost_2*'
      ], self, recSrc
    )
    
  def Edit(self, param):
    #self.
    config = self.Config
    helper = self.Helper      
    recSrc = param['recData']                                                   
    self.date_modified = config.Now()
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'penalty_type*',
        'description*',
        'phone_cost*',
        'transport_cost*',
        'salary_cost*',
        'salary_cost_2*'
      ], self, recSrc
    )

  def EntryTemplateDetail(self, param):
    config = self.Config
    helper = self.Helper      
    uipDetil = param['recDetail']
    cei_list ='('
    for i in range(0, uipDetil.RecordCount):
      recDetil = uipDetil.GetRecord(i)
      id_penalty_template=self.id_penalty_template
      id_penalty_detail = recDetil.id_penalty_detail
      if id_penalty_detail not in [None,0]:
        oTx = helper.GetObject("PenaltyTemplateDetail", id_penalty_detail)
      else:
        oTx = helper.CreatePObject("PenaltyTemplateDetail")
        oTx.id_penalty_template = id_penalty_template 
        
      cei_list += str(oTx.id_penalty_detail)+','
      oTx.AddInfo(recDetil)
        
    cei_list += '0)'
    
    # Delete penalty detail
    sSQL ="DELETE FROM %s WHERE id_penalty_detail not in %s " % (config.MapDBTableName("PenaltyTemplateDetail"),cei_list)
    config.ExecSQL(sSQL)       
      
  def PenaltyCheck(self):
    config = self.Config
    sSQL = '''select count(1) jml_prod from %(finproduct)s where id_penalty_template=%(idt)s
    ''' % {'finproduct': config.MapDBTableName("finproduct"), 'idt': self.id_penalty_template} 
    res = config.CreateSQL(sSQL).RawResult
    res.First()
    
    return res.jml_prod or 0
  #--
  
  def Hapus(self, param):
    config = self.Config
    recSrc = param['recData']                                                   
    # Delete cost element
    sSQL ="DELETE FROM %s WHERE id_penalty_template = %s " % (config.MapDBTableName("PenaltyTemplateDetail"),recSrc.id_penalty_template)
    dbutil.runSQL(config, sSQL) 

    sSQL ="DELETE FROM %s WHERE id_penalty_template = %s " % (config.MapDBTableName("PenaltyTemplate"),recSrc.id_penalty_template)
    dbutil.runSQL(config, sSQL) 
    
class PenaltyTemplateDetail(pobject.PObject):
  # Static variables
  pobject_classname = 'PenaltyTemplateDetail'
  pobject_keys      = ['id_penalty_detail']
      
  def AddInfo(self, param):
    #self.
    config = self.Config
    helper = self.Helper      
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'dpd_bottom*',
        'dpd_up*',
        'phone_frequency*',
        'visit_frequency*',
        'bucket_seq*',
        'max_amount_bucket*',
        'daily_penalty_cost*'
      ], self, param
    )
    #recSpec = param['recFinInsurance']
    #atutil.transferAttributes(helper, self.SPEC_ATTR, self, recSpec)
    

class NOTARIS(pobject.PObject):
  # Static variables
  pobject_classname = 'NOTARIS'
  pobject_keys      = ['kode_notaris']

  def OnCreate(self, param):    
    config = self.Config
    helper = self.Helper      
    recSrc = param['recNotaris']
    atutil.transferAttributes(helper,
      [
        'kode_notaris*',
        'nama_notaris*',
        'alamat_notaris*',  
        'telepon*',                       
      ], self, recSrc
    )
  
  
  def Edit(self, param):
    config = self.Config
    helper = self.Helper      
    recSrc = param['recNotaris']
    atutil.transferAttributes(helper,
      [
        'nama_notaris*',  
        'alamat_notaris*', 
        'telepon*',
      ], self, recSrc
    )

class NotarisData(pobject.PObject):
  # Static variables
  pobject_classname = 'NotarisData'
  pobject_keys      = ['ID_NotarisData']

  def OnCreate(self, param):    
    config = self.Config
    helper = self.Helper      
    recSrc = param['recNotaris']
    atutil.transferAttributes(helper,
      [
        'ID_NotarisData',
        'kode_notaris=*LNotaris.kode_notaris',
        'nomor_nasabah=*LNasabah.nomor_nasabah',
        'jenis_dokumen*',                       
        'StatusCoverNote*',                     
        'TglCoverNote*',
        'TglJTCoverNote*'
      ], self, recSrc
    )
  
  
  def Edit(self, param):
    config = self.Config
    helper = self.Helper      
    recSrc = param['recNotaris']
    atutil.transferAttributes(helper,
      [
        'kode_notaris=*LNotaris.kode_notaris',
        'nomor_nasabah=*LNasabah.nomor_nasabah',
        'jenis_dokumen*',                       
        'StatusCoverNote*',                     
        'TglCoverNote*',                     
        'TglJTCoverNote*',
      ], self, recSrc
    )
 
class ToBeObtained(pobject.PObject):
  # Static variables
  pobject_classname = 'ToBeObtained'
  pobject_keys      = ['ID_TBO']

  def OnCreate(self, param):    
    config = self.Config
    helper = self.Helper      
    recSrc = param['recTBO']
    atutil.transferAttributes(helper,
      [
        'ID_TBO',
        'agentcode=*LAccOff.AgentCode',
        'nomor_nasabah=*LNasabah.nomor_nasabah',
        'tgl_tbo*',
        'tgl_jt_tbo*'
      ], self, recSrc
    )
  
  
  def Edit(self, param):
    config = self.Config
    helper = self.Helper      
    recSrc = param['recTBO']
    atutil.transferAttributes(helper,
      [
        'agentcode=*LAccOff.AgentCode',
        'nomor_nasabah=*LNasabah.nomor_nasabah',
        'tgl_tbo*',
        'tgl_jt_tbo*',
      ], self, recSrc
    )

class ToBeObtainedDetail(pobject.PObject):
  # Static variables
  pobject_classname = 'ToBeObtainedDetail'
  pobject_keys      = ['ID_TBODetail']

  def OnCreate(self, param):    
    config = self.Config
    helper = self.Helper      
    recSrc = param['recTBODetail']
    atutil.transferAttributes(helper,
      [
        'ID_TBODetail',
        'nama_dokumen',
        'keterangan',
      ], self, recSrc
    )
  
  
  def Edit(self, param):
    config = self.Config
    helper = self.Helper      
    recSrc = param['recTBODetail']
    atutil.transferAttributes(helper,
      [
        'nama_dokumen',
        'keterangan',
      ], self, recSrc
    )
    
    
class GLInterfaceFacility(pobject.PObject):
  # Static variables
  pobject_classname = 'GLInterfaceFacility'
  pobject_keys      = ['GLFacility_ID']

  d_dict={'MBH':'A','MSY':'B','MDB':'C','PRK':'G','KAF':'I'}
  trfAttribut = [
        'contracttype_code*',
        'kode_jenis',
        'dropping_model',
        'kode_account=*LAccount.account_code',
        'kode_interface=*LInterface.kode_tx_class',
        'cost_element_id=*LFinCostElement.cost_element_id',
        'Interface_Type'
      ]
      
  def OnCreate(self, param):    
    trfAttribut = self.trfAttribut
    trfAttribut.append('GLFacility_ID')      

    self.doSave(param, trfAttribut)
  
  def Edit(self, param):
    self.doSave(param, self.trfAttribut)

  def doSave(self, param, trfAttribut):
    config = self.Config
    helper = self.Helper      
    atutil.transferAttributes(helper, self.trfAttribut, self, param)
    self.contracttype_code = self.d_dict[self.kode_jenis]
    if self.Interface_Type=='1' and self.kode_interface=='19000':
      self.cost_element_id = param.sifat_fas


class HistRiskRating(pobject.PObject):
  # Static variables
  pobject_classname = 'HistRiskRating'
  pobject_keys      = ['RiskRatingId']

  trfAttribut = [
    'CRF*',
    'ERF*',
    'RatingDate*',
    'JenisPembiayaan*',
    'nomor_nasabah*',
    'CRF_score*',
    'ERF_score*'
  ]
      
  def OnCreate(self, param):    
    config = self.Config
    helper = self.Helper
    trfAttribut = self.trfAttribut
    trfAttribut.append('RiskRatingId') 
    #raise Exception, param.nomor_nasabah     
    atutil.transferAttributes(helper, trfAttribut, self, param)
    self.is_current = 'F'
    #update current rating
    #raise Exception, self.nomor_nasabah     
    self.updateCurrentRating()
  #---     
  
  def Edit(self, param):
    config = self.Config
    helper = self.Helper
    atutil.transferAttributes(helper, self.trfAttribut, self, param)
    #raise Exception, self.nomor_nasabah
  #---     
    
  def updateCurrentRating(self):
    config = self.Config
    mlu = config.ModLibUtils
    dParam = {
      'HistRiskRating': config.MapDBTableName('HistRiskRating'),
      'no_cif': mlu.QuotedStr(self.nomor_nasabah)
    }
    dbutil.runSQL(config, ''' update %(HistRiskRating)s set is_current='F' where nomor_nasabah=%(no_cif)s ''' % dParam) 
    sSQL = '''
      merge INTO %(HistRiskRating)s x USING (
        SELECT a.nomor_nasabah, Max(riskratingid) max_id 
        FROM %(HistRiskRating)s a
          INNER JOIN (  
            SELECT nomor_nasabah, Max(ratingdate) last_date, Count(1) jml FROM %(HistRiskRating)s GROUP BY nomor_nasabah
          ) b ON a.nomor_nasabah=b.nomor_nasabah AND a.ratingdate=b.last_date
        where a.nomor_nasabah=%(no_cif)s
        GROUP BY a.nomor_nasabah 
      ) y ON (x.riskratingid=y.max_id)
      WHEN matched THEN UPDATE SET 
        x.is_current = 'T'
    ''' % dParam
             
    dbutil.runSQL(config, sSQL) 
  #---     
      
class FinCustGroups(pobject.PObject):
  # Static variables
  pobject_classname = 'FinCustGroups'
  pobject_keys      = ['cust_group_id']

  trfAttribut = [
    'group_code*',
    'group_name*',
    'group_description*',
    'group_parent=*LNasabah.Nomor_Nasabah',
    #'group_expossure*',
    'BMPK*'
  ]
      
  def OnCreate(self, param):    
    config = self.Config
    helper = self.Helper
    #trfAttribut = self.trfAttribut
    self.trfAttribut.append('cust_group_id')
    self.Edit(param) 
    #atutil.transferAttributes(helper, trfAttribut, self, param)
  #---     
  
  def InsertMember(self, group_id, no_nasabah):
    config = self.Config
    helper = self.Helper
    oDetail = helper.GetObject('FinCustGroupLink', {'cust_group_id':group_id,'nomor_nasabah':no_nasabah})
    if oDetail.IsNull:
      oDetail = helper.CreatePObject('FinCustGroupLink')
      oDetail.cust_group_id = group_id 
      oDetail.nomor_nasabah = no_nasabah 

    oCIF = helper.GetObject('FinCustAdditionalData', no_nasabah)
    if oCIF.IsNull:
      oCIF = helper.CreatePObject('FinCustAdditionalData')
      oCIF.nomor_nasabah = no_nasabah
    
    if oCIF.cust_group_id in [None, 0]:
      oCIF.cust_group_id = group_id

    return oDetail
    
  def Edit(self, param):
    config = self.Config
    helper = self.Helper
    app = config.AppObject
    app.ConCreate('out')
          
    recSrc = param['FinCustGroups']
    recSD = param['GroupLink']
    
    atutil.transferAttributes(helper, self.trfAttribut, self, recSrc)

    group_id = self.cust_group_id
    self.group_expossure =0
    app.ConWriteln( "Insert parent group.." )
    oDetail = self.InsertMember(group_id, self.group_parent)
    if oDetail.description in [None, '']:
      oDetail.description = 'Group Parent'   

    app.ConWriteln( "Insert member group dimulai...." )
    c_list = [self.group_parent] 
    for i in range(0, recSD.RecordCount):
      recDetil = recSD.GetRecord(i)      
      no_nasabah = recDetil.GetFieldByName('LMember.Nomor_Nasabah')
      
      oDetail = self.InsertMember(group_id, no_nasabah)
      oDetail.description   = recDetil.description 
  
      c_list.append("'%s'" % no_nasabah)
      
      if (i % 50) == 0:
        app.ConWriteln( "%s Detil Data Berhasil diproses..." % (i) )

    no_list = " , ".join(c_list)
    # Delete group member
    sSQL ="DELETE FROM %s WHERE nomor_nasabah not in ( %s ) and cust_group_id=%s " % (config.MapDBTableName("FinCustGroupLink"),no_list,group_id)
    #app.ConWriteln(sSQL)
    config.ExecSQL(sSQL) 
    
    # update default groups @ cust additional data
    sSQL = '''
      UPDATE %(fincustadditionaldata)s a SET a.CUST_GROUP_ID=NULL
      WHERE 
        nvl(a.CUST_GROUP_ID,0)<> 0 and
        NOT EXISTS 
        (
        SELECT 1 FROM %(fincustgrouplink)s b WHERE a.CUST_GROUP_ID=b.CUST_GROUP_ID AND a.NOMOR_NASABAH=b.NOMOR_NASABAH
        )
    ''' % dbutil.mapDBTableNames(config, ['fincustadditionaldata','fincustgrouplink'])     
    config.ExecSQL(sSQL) 

    app.ConWriteln( "selesai...." )
    #app.ConRead('')
    #raise Exception, 'AA'
  #---     
    
class FinCustGroupLink(pobject.PObject):
  # Static variables
  pobject_classname = 'FinCustGroupLink'
  pobject_keys      = ['cust_group_id','nomor_nasabah']


class FinHistCollect(pobject.PObject):
  # Static variables
  pobject_classname = 'FinHistCollect'
  pobject_keys      = ['FinHistCollectId']

  attrlist = [ 'FinHistCollectId*',
        'nomor_rekening',
        'updatedate=tgl_check',
        'oldcolvalue=old_overall_col_level',
        'newcolvalue=overall_col_level'
      ]

  def OnCreate(self, param):
    config = self.Config
    helper = self.Helper      
    recSrc = param['HistColect']
    atutil.transferAttributes(helper, 
      self.attrlist, self, recSrc
    )
    # create dummy account to maintain facility limit and usage
    self.config.FlushUpdates()
#--

class MonthlyGP(pobject.PObject):
  # Static variables
  pobject_classname = 'MonthlyGP'
  pobject_keys      = ['mgp_id']
      
  def OnCreate(self, param):
    config = self.Config
    helper = self.Helper      
    recSrc = param['recData']
      
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'mgp_id*',
        'mgp_description*',
        'mgp_plafond*',
        'mgp_eqv_rate*',
        'mgp_nisbah*',
        'mgp_multiple_tier*',
        'mgp_status*'
      ], self, recSrc
    )
    
  def Edit(self, param):
    config = self.Config
    helper = self.Helper      
    recSrc = param['recData']                                                   
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'mgp_description*',
        'mgp_plafond*',
        'mgp_eqv_rate*',
        'mgp_nisbah*',
        'mgp_multiple_tier*',
        'mgp_status*'
      ], self, recSrc
    )

  def EntryMGPDetail(self, param):
    config = self.Config
    helper = self.Helper      
    uipDetil = param['recDetail']
    # Delete penalty detail
    sSQL ="DELETE FROM %s WHERE mgp_id=%s " % (config.MapDBTableName("MGPDetail"),self.mgp_id)
    config.ExecSQL(sSQL)       
    for i in range(0, uipDetil.RecordCount):
      recDetil = uipDetil.GetRecord(i)
      oTx = helper.CreatePObject("MGPDetail")
      oTx.mgp_id = self.mgp_id 
        
      oTx.AddInfo(recDetil)
        
        
  def Hapus(self, param):
    config = self.Config
    recSrc = param['recData']                                                   
    # Delete cost element
    sSQL ="DELETE FROM %s WHERE id_penalty_template = %s " % (config.MapDBTableName("PenaltyTemplateDetail"),recSrc.id_penalty_template)
    dbutil.runSQL(config, sSQL) 

    sSQL ="DELETE FROM %s WHERE id_penalty_template = %s " % (config.MapDBTableName("PenaltyTemplate"),recSrc.id_penalty_template)
    dbutil.runSQL(config, sSQL) 
    
class MGPDetail(pobject.PObject):
  # Static variables
  pobject_classname = 'MGPDetail'
  pobject_keys      = ['mgpdetail_id']
      
  def AddInfo(self, param):
    #self.
    config = self.Config
    helper = self.Helper      
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'seq_no*',
        'max_value*',
        'min_value*',
        'mgp_amount*',
        'profit_amount*'
      ], self, param
    )
    #recSpec = param['recFinInsurance']
    #atutil.transferAttributes(helper, self.SPEC_ATTR, self, recSpec)
    
class PRK_MGP(pobject.PObject):
  # Static variables
  pobject_classname = 'PRK_MGP'
  pobject_keys      = ['mgpdetail_id']
      
  def AddInfo(self, param):
    #self.
    config = self.Config
    helper = self.Helper      
    # set fields with simple / direct 1-on-1 assignment from record with same field name
    atutil.transferAttributes(helper,
      [
        'seq_no*',
        'max_value*',
        'min_value*',
        'mgp_amount*',
        'profit_amount*'
      ], self, param
    )
    #recSpec = param['recFinInsurance']
    #atutil.transferAttributes(helper, self.SPEC_ATTR, self, recSpec)
   

class ParameterGlobal(pobject.PObject):
  # Static variables
  pobject_classname = 'ParameterGlobal'
  pobject_keys      = ['kode_parameter']

  trfAttribut = [ 'tipe_parameter*',
        'deskripsi*',
        'nilai_parameter*',
        'nilai_parameter_tanggal*',
        'nilai_parameter_string*'
      ]

  def OnCreate(self, param):
    config = self.Config
    helper = self.Helper
    #trfAttribut = self.trfAttribut
    self.trfAttribut.append('kode_parameter')
    self.Edit(param)
    self.status = 'T' 
   
  def Edit(self, param):
    config = self.Config
    helper = self.Helper
    app = config.AppObject
    app.ConCreate('out')
          
    mlu = config.ModLibUtils
    recSrc = param['datapaket']
    atutil.transferAttributes(helper, self.trfAttribut, self, recSrc)
    self.config.FlushUpdates()
  #--
  
#---
      