import sys
import os
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.intrdbutil as dbutil
import copy

def FixDetilTransaksiValas(config, dParams):
  
  sSQL = '''
    UPDATE {detiltransaksi} dt
    set  kode_kurs = 'TT_JUAL'
       , nilai_kurs_manual = (
           SELECT d1.kurs_tengah_bi 
           FROM {kurshistory} d1,
              (SELECT currency_code, MAX(history_date) as maxdate
              FROM {kurshistory} WHERE history_date <= dt.tanggal_transaksi
              GROUP BY currency_code) d2
            WHERE d1.currency_code = d2.currency_code
              AND d1.history_date = d2.maxdate 
              and dt.kode_valuta = d1.currency_code
        )
    WHERE dt.kode_valuta <> {default_valuta}
      and exists (
        select 1 from {srctable} ptj
        where ptj.id_transaksi = dt.id_transaksi
        )
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  
  sSQL = '''
    UPDATE {detiltransaksi} dt
    SET 
      nilai_kurs_manual = 1,
      nilai_ekuivalen = nilai_mutasi
    WHERE dt.kode_valuta = {default_valuta} AND 
    EXISTS(
      select 1 from
        {srctable} ptj
      where ptj.id_transaksi = dt.id_transaksi
      AND {filter}
    )
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
def createTreasuryJournal(config, sDate, idTransaksiOrFilter = None, srcTable = None):
  # Fungsi ini bisa digunakan dengan tiga cara:
  # 1. Hanya parameter config dan sDate: akan dipilih seluruh data dari financingsys.pendingtransactionjournal yang flag_sent IS NULL
  # 2. Dengan parameter config, sDate dan idTransaksiOrFilter (integer): akan menjurnal id_transaksi tertentu yang ada di tabel financingsys.pendingtransactionjournal
  # 3. Dengan parameter config, sDate, idTransaksiOrFilter (string), srcTable (string): akan menjurnal dari srcTable dengan kriteria idTransaksiOrFilter 
  
  mlu = config.ModLibUtils

  
  default_valuta = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultCurrency')
  acc_code_selisih = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultAccountSelisih')
  head_branch = '000'
  acc_code_rak = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultAccountRAKOtomatis')
  current_user = config.SecurityContext.InitUser 
  dParams = {
    'detiltransaksi': config.MapDBTableName('core.detiltransaksi'),
    'transaksi': config.MapDBTableName('core.transaksi'),
    'account': config.MapDBTableName('core.account'), 
    'accountinstance': config.MapDBTableName('core.accountinstance'),
    'accountingday': config.MapDBTableName('core.accountingday'),
    'journal': config.MapDBTableName('core.journal'),
    'journalitem': config.MapDBTableName('core.journalitem'),    
    'masterkurs': config.MapDBTableName('core.masterkurs'),
    'rpvaccountdetail': config.MapDBTableName('core.rpvaccountdetail'),
    'kurshistory' : config.MapDBTableName('core.kurshistory'),
    'treasuryaccount': config.MapDBTableName('treasuryaccount'),
    'glinterface': config.MapDBTableName('glinterface'),
    'seq_treajournalno': config.MapDBTableName('seq_treajournalno'),
    'seq_treajournalitemno': config.MapDBTableName('seq_treajournalitemno'),
    'date': mlu.QuotedStr(sDate),
    'default_valuta': mlu.QuotedStr(default_valuta),
    'head_branch': mlu.QuotedStr(head_branch),
    'acc_code_rak': mlu.QuotedStr(acc_code_rak),
    'acc_code_selisih': mlu.QuotedStr(acc_code_selisih),
    'current_user': mlu.QuotedStr(current_user)    
  }
  
  if idTransaksiOrFilter == None:
    dParams.update({'filter': 'ptj.flag_sent IS NULL'})
    dParams.update({'srctable': config.MapDBTableName('pendingtransactionjournal')})
    dParams.update({'resulttable': config.MapDBTableName('completedtransactionjournal')})
  else:
    if srcTable == None:
      dParams.update({'filter': 'ptj.id_transaksi = %d' % idTransaksiOrFilter})
      dParams.update({'srctable': config.MapDBTableName('pendingtransactionjournal')})
      dParams.update({'resulttable': config.MapDBTableName('completedtransactionjournal')})
    else:
      dParams.update({'filter': idTransaksiOrFilter})
      dParams.update({'srctable': config.MapDBTableName(srcTable)})
      

  # SET ACCOUNT DATA DI FINANCING
  sSQL = '''
    MERGE INTO {detiltransaksi} target
    USING (
      SELECT                                        
        dt.id_detil_transaksi,
        case
          when gli.kode_account is null then {acc_code_selisih}
          else gli.kode_account
        end as kode_account
      FROM
        {detiltransaksi} dt,
        {srctable} ptj,
        {treasuryaccount} fa,
        {glinterface} gli
      WHERE
        ptj.id_transaksi = dt.id_transaksi
        AND {filter}
        AND dt.nomor_rekening = fa.nomor_rekening
        AND (fa.kode_produk = gli.kode_produk AND gli.kode_interface = dt.kode_tx_class 
        OR fa.kode_produk IS NULL AND gli.kode_interface = dt.kode_tx_class
        )
    ) updrecs
    ON (target.id_detil_Transaksi = updrecs.id_detil_transaksi)
    WHEN MATCHED THEN
      UPDATE SET target.kode_account = updrecs.kode_account 
      WHERE target.kode_account IS NULL
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  sSQL = '''
    MERGE INTO {detiltransaksi} d 
    using (
      select tr.id_transaksi, tr.tanggal_transaksi
      from {transaksi} tr
      inner join {srctable} ptj on ptj.id_transaksi = tr.id_transaksi
    ) t on (t.id_transaksi = d.id_transaksi)
    when matched then update 
    set tanggal_transaksi =  t.tanggal_transaksi
    where ( d.tanggal_transaksi <> t.tanggal_transaksi
            OR d.tanggal_transaksi IS null)              
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  # ADJUST & FIX  UP DATA KURS DAN EKIVALEN
  
  #-- adjust kode_kurs jadi 'NR' untuk akun yg tidak di reval
  sSQL = '''
    UPDATE {detiltransaksi} dt
    SET 
      kode_kurs = 'NR'
    WHERE dt.kode_valuta <> {default_valuta} AND 
    EXISTS(
      select 1 from
        {srctable} ptj
      where ptj.id_transaksi = dt.id_transaksi
      AND {filter}
    )
    and exists (
        select 1
        from {account} a
        where a.is_disable_reval_curr = 'T'
          and a.account_code = dt.kode_account
      )
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  #FixDetilTransaksiValas(config, dParams)
  
  sSQL = '''
    UPDATE {detiltransaksi} dt
    SET 
      nilai_ekuivalen = nilai_mutasi * nilai_kurs_manual
    WHERE dt.kode_valuta <> {default_valuta} AND 
    EXISTS(
      select 1 from
        {srctable} ptj
      where ptj.id_transaksi = dt.id_transaksi
      AND {filter}
    )
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  # CHECK ACCOUNTINGDAY AVAILABILITY
  sSQL = '''
    SELECT datevalue, periode_status FROM {accountingday} accd
    WHERE datevalue = TO_DATE({date})
  '''.format(**dParams)
  q = config.CreateSQL(sSQL).RawResult
  if q.Eof:
    raise Exception, "Accounting day %s not found" % sDate
  
  
  # CEK VALIDITAS DATA
  sSQL = '''
    SELECT dt.id_detil_transaksi, 
      dt.id_transaksi,
      dt.nomor_rekening,
      dt.jenis_mutasi,
      dt.nilai_mutasi,
      dt.kode_tx_class,
      t.nomor_seri
    FROM {detiltransaksi} dt, {transaksi} t, {srctable} ptj
    WHERE 
    dt.id_transaksi = t.id_transaksi
    AND ptj.id_transaksi = t.id_transaksi
    AND {filter}
    AND (
      dt.kode_account IS NULL
      OR
      dt.kode_cabang IS NULL
      OR
      dt.kode_valuta IS NULL
      OR
      (
        dt.kode_valuta = {default_valuta}
        AND
        (dt.nilai_kurs_manual <= 0 OR dt.nilai_kurs_manual IS NULL)
      )
    )
  '''.format(**dParams)
  # raise Exception, sSQL
  q = config.CreateSQL(sSQL).RawResult
  if not q.Eof:
    raise Exception, "Ada data transaksi tidak lengkap. nomor_rekening=%s id_detil_transaksi=%d" % (q.nomor_rekening, q.id_detil_transaksi)
  
  # CEK ACCOUNTINSTANCE
  
  # CEK ACCOUNTINSTANCE selain expense income 
  sSQL = '''
    SELECT dt.id_detil_transaksi, 
      dt.id_transaksi,
      dt.nomor_rekening,
      dt.kode_account,
      dt.kode_cabang,
      dt.kode_valuta
    FROM {detiltransaksi} dt, {transaksi} t, {srctable} ptj, 
      {account} a 
    WHERE 
    dt.id_transaksi = t.id_transaksi
    AND ptj.id_transaksi = t.id_transaksi
    AND a.account_code = dt.kode_account
    AND a.account_type not in ('I','X')
    AND {filter}
    AND NOT EXISTS (
      select 1 
      FROM {accountinstance} ai
      WHERE ai.account_code = dt.kode_account
        AND ai.branch_code = dt.kode_cabang
        AND ai.currency_code = dt.kode_valuta        
    )
  '''.format(**dParams)
  q = config.CreateSQL(sSQL).RawResult

  if not q.Eof:
    raise Exception, "Account instance untuk account=%s valuta=%s cabang=%s tidak ada" % (q.kode_account, q.kode_valuta, q.kode_cabang)
  
  # CEK ACCOUNTINSTANCE untuk expense income
  # cek menggunakan valuta default  
  sSQL = '''
    SELECT dt.id_detil_transaksi, 
      dt.id_transaksi,
      dt.nomor_rekening,
      dt.kode_account,
      dt.kode_cabang,
      dt.kode_valuta
    FROM {detiltransaksi} dt, {transaksi} t, {srctable} ptj, 
      {account} a      
    WHERE 
    dt.id_transaksi = t.id_transaksi
    AND ptj.id_transaksi = t.id_transaksi
    AND a.account_code = dt.kode_account
    AND a.account_type in ('I','X')
    AND {filter}
    AND NOT EXISTS (
      select 1 
      FROM {accountinstance} ai
      WHERE ai.account_code = dt.kode_account
        AND ai.branch_code = dt.kode_cabang
        AND ai.currency_code = {default_valuta}
    )
  '''.format(**dParams)
  q = config.CreateSQL(sSQL).RawResult

  if not q.Eof:
    raise Exception, "Account instance untuk account=%s valuta=%s cabang=%s tidak ada" % (q.kode_account, q.kode_valuta, q.kode_cabang)
  
  # CEK BALANCING
  sSQL = '''
    SELECT id_transaksi, amount_debit, amount_credit FROM (
      SELECT  
        dt.id_transaksi,
        SUM(
        case
          when dt.jenis_mutasi = 'D' then round(dt.nilai_mutasi * dt.nilai_kurs_manual,2)
          else 0.0
        end) as amount_debit,
        SUM(
        case
          when dt.jenis_mutasi = 'C' then round(dt.nilai_mutasi * dt.nilai_kurs_manual,2)
          else 0.0
        end) as amount_credit
      FROM {detiltransaksi} dt, {transaksi} t, {srctable} ptj,
        {account} acc
      WHERE 
      dt.id_transaksi = t.id_transaksi
      AND ptj.id_transaksi = t.id_transaksi
      AND {filter}
      AND dt.kode_account = acc.account_code
      AND acc.account_type <> 'M'
      AND (t.is_sudah_dijurnal = 'F' OR t.is_sudah_dijurnal IS NULL)
      GROUP BY dt.id_transaksi
    )
    WHERE abs(amount_debit - amount_credit) > 0.05  
  '''.format(**dParams)
  q = config.CreateSQL(sSQL).RawResult   
  if not q.Eof:      
    ### TODO : sementara utk development dikomentari    
    dbginfo = {'debit' : copy.copy(q.amount_debit), 'kredit': copy.copy(q.amount_credit), 'variance': (q.amount_debit-q.amount_credit)}
    raise Exception, "Transaksi %d tidak balanced ,dbginfo = %s" % (q.id_transaksi, str(dbginfo))
    pass
  
  # CREATE JOURNAL NUMBERS
  sSQL = '''
    MERGE INTO {srctable} target
    USING (
      SELECT
        ptj.id_transaksi,
        'TRE.' || accd.fl_accountingyear || '.' || 
        lpad(to_char(accd.fl_accountingperiode), 2, '0') || '.' as prefix_journal_no
      FROM
      {srctable} ptj,
      {transaksi} t,
      {accountingday} accd
      WHERE
        accd.datevalue = TO_DATE({date})
        AND t.id_transaksi = ptj.id_transaksi
        AND (t.is_sudah_dijurnal = 'F' or t.is_sudah_dijurnal IS NULL)
        AND {filter}
      ) upd
    ON (upd.id_transaksi = target.id_transaksi)
    WHEN MATCHED THEN UPDATE SET journal_no = upd.prefix_journal_no || to_char({seq_treajournalno}.nextval) 
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  # INSERT JOURNAL
  sSQL = '''
    INSERT INTO {journal} (
      journal_no,
      journal_date_posting,
      description,
      userid_create,
      userid_posting,
      is_posted,
      journal_type,
      journal_date,
      lastitemno,
      is_partlychecked,
      branch_code,
      transaction_date,
      journal_state,
      serial_no,
      periode_Code,
      fl_accountingperiode,
      fl_accountingyear,
      reference_no
    )
    SELECT
      ptj.journal_no,
      t.tanggal_transaksi,
      t.keterangan,
      t.user_input as userid_create,
      {current_user},
      'T',
      'TRE',
      t.tanggal_transaksi,
      0,
      'T',
      t.kode_cabang_transaksi,
      t.tanggal_transaksi,
      'C',
      '',
      accd.periode_code,
      accd.fl_accountingperiode,
      accd.fl_accountingyear,
      t.Nomor_Referensi
    FROM
      {srctable} ptj,
      {accountingday} accd,
      {transaksi} t
    WHERE
      {filter}
      AND ptj.journal_no IS NOT NULL
      AND ptj.id_transaksi = t.id_transaksi
      AND accd.datevalue = TO_DATE({date})
      AND (t.is_sudah_dijurnal = 'F' or t.is_sudah_dijurnal IS NULL)  
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  # INSERT JOURNAL ITEM ASSET LIABILITY EQUITY
  sSQL = '''
    INSERT INTO {journalitem} (
      fl_journal,
      journalitem_no,
      fl_account,
      amount_debit,
      amount_credit,
      description,
      branch_code,
      valuta_code,
      fl_project_no,
      userid_create,
      userid_posting,
      userid_check,
      DatePosting,
      DateCreate,
      JournalItemType,
      Kode_Kurs,
      Nilai_Kurs,
      subaccountcode,
      AccountInstance_ID
      )
    SELECT
      ptj.journal_no,
      {seq_treajournalitemno}.nextval,
      dt.kode_account,
      case when
        dt.jenis_mutasi = 'D' then dt.nilai_mutasi
        else 0.0
      end as amount_debit,
      case when
        dt.jenis_mutasi = 'C' then dt.nilai_mutasi
        else 0.0
      end as amount_credit,
      dt.keterangan,
      dt.kode_cabang,
      dt.kode_valuta,
      NULL,
      t.user_input,
      t.user_input,
      t.user_input,
      t.tanggal_transaksi,
      t.tanggal_input,
      'N',
      dt.kode_kurs,
      dt.nilai_kurs_manual,
      dt.nomor_rekening,
      ai.accountinstance_id
    FROM
      {srctable} ptj
      inner join {transaksi} t on ptj.id_transaksi = t.id_transaksi      
      inner join {detiltransaksi} dt on dt.id_transaksi = t.id_transaksi
      inner join {accountinstance} ai on ai.account_code = dt.kode_account AND ai.branch_code = dt.kode_cabang AND ai.currency_code = dt.kode_valuta
      inner join {account} a on a.account_code = ai.account_code
    WHERE
      (t.is_sudah_dijurnal = 'F' or t.is_sudah_dijurnal IS NULL)
      AND {filter}
      and a.account_type in ('A','L','E','M')
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  # INSERT JOURNAL ITEM INCOME DAN EXPENSE
  sSQL = '''
    INSERT INTO {journalitem} (
      fl_journal,
      journalitem_no,
      fl_account,
      amount_debit,
      amount_credit,
      description,
      branch_code,
      valuta_code,
      fl_project_no,
      userid_create,
      userid_posting,
      userid_check,
      DatePosting,
      DateCreate,
      JournalItemType,
      Kode_Kurs,
      Nilai_Kurs,
      subaccountcode,
      AccountInstance_ID
      )
    SELECT
      ptj.journal_no,
      {seq_treajournalitemno}.nextval,
      dt.kode_account,
      case when
        dt.jenis_mutasi = 'D' then dt.nilai_mutasi * dt.nilai_kurs_manual
        else 0.0
      end as amount_debit,
      case when
        dt.jenis_mutasi = 'C' then dt.nilai_mutasi * dt.nilai_kurs_manual
        else 0.0
      end as amount_credit,
      dt.keterangan,
      dt.kode_cabang,
      {default_valuta},
      NULL,
      t.user_input,
      t.user_input,
      t.user_input,
      t.tanggal_transaksi,
      t.tanggal_input,
      'N',
      dt.kode_kurs,
      1.0,
      dt.nomor_rekening,
      ai.accountinstance_id
    FROM
      {srctable} ptj
      inner join {transaksi} t on ptj.id_transaksi = t.id_transaksi 
      inner join {detiltransaksi} dt on dt.id_transaksi = t.id_transaksi
      inner join {accountinstance} ai on ai.account_code = dt.kode_account AND ai.branch_code = dt.kode_cabang AND ai.currency_code = {default_valuta}
      inner join {account} a on a.account_code = ai.account_code
    WHERE
      (t.is_sudah_dijurnal = 'F' or t.is_sudah_dijurnal IS NULL)
      AND {filter}
      and a.account_type in ('I','X')
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  # INSERT JURNAL RPV VALAS (REVERSE MUTATION TYPE)
  # generate berdasarkan akun asset , liability atau equity di detiltransaksi yang valutanya valas
  # akun rpv diambil dari tabel rpvaccountdetail menggunakan relasi masterkurs sesuai kode_kurs detiltransaksi
  # jenis mutasi dibalik debet dan kreditnya
  sSQL = '''
    INSERT INTO {journalitem} (
      fl_journal,
      journalitem_no,
      fl_account,
      amount_debit,
      amount_credit,
      description,
      branch_code,
      valuta_code,
      fl_project_no,
      userid_create,
      userid_posting,
      userid_check,
      DatePosting,
      DateCreate,
      JournalItemType,
      Kode_Kurs,
      Nilai_Kurs,    
      subaccountcode,
      AccountInstance_ID
      )
    SELECT
      ptj.journal_no,
      {seq_treajournalitemno}.nextval,
      ai.account_code,
      case when
        dt.jenis_mutasi = 'C' then dt.nilai_mutasi
        else 0.0
      end as amount_debit,
      case when
        dt.jenis_mutasi = 'D' then dt.nilai_mutasi
        else 0.0
      end as amount_credit,
      dt.keterangan,
      ai.branch_code,
      ai.currency_code,
      NULL,
      t.user_input,
      t.user_input,
      t.user_input,
      t.tanggal_input,
      t.tanggal_input,
      'N',
      dt.kode_kurs,
      dt.nilai_kurs_manual,
      dt.nomor_rekening,
      ai.accountinstance_id
    FROM
      {srctable} ptj
      inner join {transaksi} t on ptj.id_transaksi = t.id_transaksi      
      inner join {detiltransaksi} dt on dt.id_transaksi = t.id_transaksi
      inner join {account} a on a.account_code = dt.kode_account
      inner join {masterkurs} m on m.kode_kurs = dt.kode_kurs
      inner join {rpvaccountdetail} v on v.rpv_name = m.rpv_name and v.branch_code = dt.kode_cabang and v.currency_code = dt.kode_valuta
      inner join {accountinstance} ai on ai.accountinstance_id = v.accountinstance_valas
    WHERE 
      (t.is_sudah_dijurnal = 'F' or t.is_sudah_dijurnal IS NULL)
      AND {filter}
      AND a.account_type in ('A','L','E')
      and dt.kode_valuta <> {default_valuta}
  '''.format(**dParams)
  
  dbutil.runSQL(config, sSQL)
  
  # INSERT JURNAL RPV IDR (AMOUNT EKUIVALEN)
  # generate berdasarkan akun asset , liability atau equity di detiltransaksi yang valutanya valas
  # akun rpv diambil dari tabel rpvaccountdetail menggunakan relasi masterkurs sesuai kode_kurs detiltransaksi
  # amountnya di buat ekuivalen dalan IDR
  sSQL = '''
    INSERT INTO {journalitem} (
      fl_journal,
      journalitem_no,
      fl_account,
      amount_debit,
      amount_credit,
      description,
      branch_code,
      valuta_code,
      fl_project_no,
      userid_create,
      userid_posting,
      userid_check,
      DatePosting,
      DateCreate,
      JournalItemType,
      Kode_Kurs,
      Nilai_Kurs,
      subaccountcode,
      AccountInstance_ID
      )
    SELECT
      ptj.journal_no,
      {seq_treajournalitemno}.nextval,
      ai.account_code,
      case when
        dt.jenis_mutasi = 'D' then dt.nilai_mutasi * nilai_kurs_manual
        else 0.0
      end as amount_debit,
      case when
        dt.jenis_mutasi = 'C' then dt.nilai_mutasi * nilai_kurs_manual
        else 0.0
      end as amount_credit,
      dt.keterangan,
      ai.branch_code,
      ai.currency_code,
      NULL,
      t.user_input,
      t.user_input,
      t.user_input,
      t.tanggal_input,
      t.tanggal_input,
      'N',
      dt.kode_kurs,
      1.0,
      dt.nomor_rekening,
      ai.accountinstance_id
    FROM
      {srctable} ptj
      inner join {transaksi} t on ptj.id_transaksi = t.id_transaksi      
      inner join {detiltransaksi} dt on dt.id_transaksi = t.id_transaksi
      inner join {account} a on a.account_code = dt.kode_account
      inner join {masterkurs} m on m.kode_kurs = dt.kode_kurs
      inner join {rpvaccountdetail} v on v.rpv_name = m.rpv_name and v.branch_code = dt.kode_cabang and v.currency_code = dt.kode_valuta
      inner join {accountinstance} ai on ai.accountinstance_id = v.accountinstance_primer
    WHERE
      (t.is_sudah_dijurnal = 'F' or t.is_sudah_dijurnal IS NULL)
      AND {filter}
      AND a.account_type in ('A','L','E')
      AND dt.kode_valuta <> {default_valuta}
  '''.format(**dParams)
  
  dbutil.runSQL(config, sSQL)
  
  
  # BALANCING RAK Otomatis, sementara hanya berlaku untuk default currency
  sSQL = '''
    INSERT INTO {journalitem} (
    fl_journal,
    journalitem_no,
    fl_account,
    amount_debit,
    amount_credit,
    description,
    branch_code,
    valuta_code,
    fl_project_no,
    userid_create,
    userid_posting,
    userid_check,
    DatePosting,
    DateCreate,
    JournalItemType,
    Kode_Kurs,
    Nilai_Kurs,
    AccountInstance_ID
    )
    SELECT 
      ubitems.fl_journal,
      {seq_treajournalitemno}.nextval,
      {acc_code_rak}, 
      case when ubitems.amount_credit > ubitems.amount_debit then ubitems.amount_credit - ubitems.amount_debit else 0.0 end  , -- reverse credit and debit amount
      case when ubitems.amount_debit > ubitems.amount_credit then ubitems.amount_debit - ubitems.amount_credit else 0.0 end,
      'TRANSAKSI RAK OTOMATIS',
      ubitems.branch_code,
      ubitems.valuta_code,
      NULL,
      t.user_input,
      t.user_input,
      t.user_input,
      t.tanggal_input,
      t.tanggal_input,
      'N',
      NULL,
      1,
      ai.accountinstance_id 
    FROM 
      (
        SELECT
          ptj.id_transaksi,  
          ji.fl_journal,
          ji.valuta_code,
          ji.branch_code,
          SUM(amount_debit) as amount_debit,
          SUM(amount_credit) as amount_credit
        FROM 
          {journalitem} ji,
          {account} acc, 
          {srctable} ptj
        WHERE 
        ptj.journal_no = ji.fl_journal
        AND acc.account_code = ji.fl_account
        AND acc.account_type <> 'M'
        AND {filter}
        GROUP BY ptj.id_transaksi, 
        ji.fl_journal, ji.valuta_code, ji.branch_code
      ) ubitems,
      {transaksi} t,
      {accountinstance} ai
    WHERE 
      abs(ubitems.amount_debit - ubitems.amount_credit) > 0.001
      AND ubitems.id_transaksi = t.id_transaksi
      AND ai.branch_code = ubitems.branch_code
      AND ai.account_code = {acc_code_rak}
      AND ai.currency_code = ubitems.valuta_code
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  # UPDATE transactions status and journal link
  sSQL = '''
    MERGE INTO {transaksi} target
    USING (
      SELECT * FROM {srctable} ptj
      WHERE
      {filter}
    ) upd
    ON (
      upd.id_transaksi = target.id_transaksi
      AND (target.is_sudah_dijurnal = 'F' or target.is_sudah_dijurnal IS NULL) 
    )
    WHEN MATCHED THEN
    UPDATE SET target.journal_no = upd.journal_no,
    target.is_sudah_dijurnal = 'T'
  '''.format(**dParams)
  dbutil.runSQL(config, sSQL)
  
  if dParams.has_key('resulttable'):
    # MOVE TO completedtransactionjournal table  
    sSQL = '''
      INSERT INTO {resulttable}
      (
        id_transaksi,
        journal_no
      )
      SELECT id_transaksi, journal_no FROM
      {srctable} ptj
      WHERE
      ptj.journal_no IS NOT NULL AND 
      {filter}
    '''.format(**dParams)
    dbutil.runSQL(config, sSQL)
    sSQL = '''
      DELETE FROM {srctable} ptj
      WHERE 
      ptj.journal_no IS NOT NULL AND
      {filter}
    '''.format(**dParams)
    dbutil.runSQL(config, sSQL)
  #--
#--  
  