# dictionary key is kode_jenis in rekening transaksi

def cols_setting(akad, report):
  vQry ='''
      item
        Expanded = False
        FieldName = '%s'
        Title.Caption = '%s'
        Width = %s
        Visible = True
      end
    '''

  _cols = ex_cols(akad, report)
  lView=[]
  for i in _cols:
    ls = i[0].split(";")
    fName = ls[0]
    tCaption = ls[0]
    if len(ls)>1:
      tCaption = ls[1]
    t_view = vQry % (fName, tCaption, i[2])
    lView.append(t_view)
 
  _dict = {
    '_vGrid':" ".join(lView)
  } 

  
  CS = '''
    object TColumnsWrapper
      Columns = <
        %(_vGrid)s
        >
    end
  ''' % _dict

  return CS
  
def ex_cols(akad, report):
  if akad.upper() in ('ALL'): #All Akad
    if report.upper() in ('A'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',130)
        ,("nama_produk;Produk",'S',90)
        ,("tgl_buka;Tgl. Deal",'D',70)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',90)
        ,("jml_hari;Jml. Hari",'I',70)
        #,("nominal_deal;Nominal",'F',110)
        ,("saldo;Nominal",'F',110)
        ,("ujroh;Ujroh",'F',110)
        ,("eqv_rate_real;Imbalan (%)",'F',100)
        #,("nominal_gross;Nominal (IDR)",'F',110)
        ,("ppap_umum;PPA UMUM",'F',110)
        ,("ppap_khusus;PPA KHUSUS",'F',110)
        ,("status;STATUS",'S',80)
      )
      
    if report.upper() in ('Z'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',130,30)
        ,("nomor_rekening;Nomor Rekening",'S',100,25)
        ,("tgl_buka;Tgl. Deal",'D',70,12)
        ,("tgl_awal;Tgl. Awal Perhitungan",'D',110,14)
        ,("tgl_accrue;Tgl. Akhir Perhitungan",'D',110,14)
        ,("accrue_day;Jml. Hari",'I',70,10) 
        ,("basis_hari_pertahun;On Basis",'S',100,10)
        ,("nominal_sbh;Nominal",'F',110,20)
        ,("imbalan;Imbalan (%)",'S',100,15) 
        ,("saldo_awal;Saldo Awal",'F',110,20)
        ,("nilai_accrue;Nilai Accrue",'F',110,20)
        ,("saldo_akhir;Saldo Akhir",'F',110,20) 
        ,("status_rekening;STATUS",'S',80,10)
      )
      
    if report.upper() in ('I'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',30)
        ,("tgl_buka;Tgl. Deal",'D',12)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',12)
        ,("jml_hari;Jml. Hari",'I',10)
        #,("nominal_deal;Nominal",'F',25) 
        ,("saldo;Nominal",'F',110)
        ,("ujroh;Ujroh",'F',25)
        ,("eqv_rate_real;Imbalan (%)",'S',10) 
        ,("status;STATUS",'S',10)
        #,("nominal_gross;Nominal (IDR)",'F',110)
      )  
              
  if akad.upper() in ('D'): #deposito
    if report.upper() in ('A'):
      EC =(
        ("nomor_rekening;GeneratedKey",'S',110)
        ,("nomor_bilyet;Nomor Bilyet",'S',110)
        ,("nama_counterpart;Nama Bank",'S',120)
        ,("nama_produk;Produk",'S',90)
        ,("tgl_buka;Tgl. Deal",'D',70)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',90)
        ,("jml_hari;Jml. Hari",'I',70)
        #,("nominal_deal;Nominal",'F',110)
        ,("saldo;Nominal",'F',110)
        ,("ujroh;Bagi Hasil",'F',110)
        ,("eqv_rate_real;Imbalan (%)",'S',90)
        ,("nisbah;Nisbah (%)",'S',90)
        ,("nilai_tunai;Nominal (IDR)",'S',110)
        ,("ppap_umum;PPA UMUM",'F',110)
        ,("ppap_khusus;PPA KHUSUS",'F',110)
        ,("status_rekening;STATUS",'S',80)
      )
    
    if report.upper() in ('I'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',30)
        ,("tgl_buka;Tgl. Deal",'D',12)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',12)
        ,("jml_hari;Jmlh Hari",'I',10)
        #,("nominal_deal;Nominal",'F',25)
        ,("saldo;Nominal",'F',110)
        ,("ujroh; Ujroh ",'F',18)
        ,("eqv_rate_real;Imbalan",'S',10)
        ,("nilai_tunai;Nominal (IDR)",'F',25)
        ,("ppap_umum;PPA UMUM",'F',20) 
        ,("ppap_khusus;PPA Khusus",'F',20)
      )
    
    if report.upper() in ('E'):
      EC =(
         ("sandi_bank;Sandi Bank",'S',120)     
        ,("hubungan_bank;Hubungan dengan Bank",'S',120)
        ,("status_bank;Status",'S',120)
        ,("jns_operasional;Jenis Operasional",'S',120)
        ,("pihak_lawan;Negara Pihak Lawan",'S',120)
        ,("jenis_instrumen;Jenis Instrumen",'S',120)
        ,("kode_valuta;Jenis Valuta",'S',120)
        ,("tgl_buka;Tgl. Deal",'D',70)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',90)
        ,("sumber_dana;Sumber Dana",'S',120)
        ,("rating_agency;Lembaga Pemeringkat",'S',120)
        ,("ranking_value;Nilai Pemeringkat",'S',120)
        ,("rating_date;Tanggal",'D',120)
        ,("jenis_akad;Jenis Akad",'S',120)
        ,("model_baghas;Metode Bagi Hasil",'S',120)
        ,("nisbah;Persentase Nisbah",'F',120)
        ,("eqv_rate_real;Awal Kontrak",'F',90)
        ,("eqv_rate_real;Bulan Laporan",'F',90)
        ,("nominal_deal;Bulan Lalu",'F',110)
        ,("nominal_debet;Debet",'F',110)
        ,("nominal_kredit;Kredit",'F',110)
        ,("nominal_other;Lainnya",'F',110)
        ,("nominal_last;Bulan Laporan",'F',110)
        ,("ujroh;Imbalan Yang diterima",'F',110)
        ,("kolektibilitas;Kualitas",'S',110)
        ,("jenis_agunan;Jenis Agunan/Jaminan",'S',110)
        ,("no_agunan;Nomor Agunan/Jaminan",'S',110)
        ,("legal_binding;Sifat",'S',110)
        ,("golongan_penerbit;Golongan Penerbit",'S',110)
        ,("penilaian_terakhir;Tgl Penilaian Terkahir",'D',110)
        ,("nilai_agunan;Jumlah",'F',110)
        ,("nilai_dijaminkan;Agunan Yang Diperhitungkan",'F',110)
        ,("bagian_dijaminkan;Bagian Dijamin",'F',110)           
        ,("ppap_umum;Individual",'F',110)
        ,("ppap_khusus;Kolektif",'F',110)
      )
  
  elif akad.upper() in ('F'): #fasbis/sbis
    if report.upper() in ('A'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',120)
        ,("nama_produk;Produk",'S',90)
        ,("tgl_buka;Tgl. Deal",'D',70)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',90)
        ,("jml_hari;Jml. Hari",'I',70)
        ,("nominal_deal;Nominal",'F',110)
        ,("ujroh;Ujroh",'F',110)
        ,("eqv_rate_real;Imbalan (%)",'S',100)
        #,("nominal_gross;Nominal (IDR)",'F',110)
        ,("ppap_umum;PPA UMUM",'F',110)
        ,("ppap_khusus;PPA KHUSUS",'F',110)
        ,("status;STATUS",'S',80)
      )
      
    if report.upper() in ('I'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',30)
        ,("tgl_buka;Tgl. Deal",'D',12)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',12)
        ,("jml_hari;Jml. Hari",'I',10)
        ,("nominal_deal;Nominal",'F',25)
        ,("ujroh;Ujroh",'F',25)
        ,("eqv_rate_real;Imbalan",'S',10)
        ,("status;STATUS",'S',10)
      )
                                
    if report.upper() in ('E'):
      EC =(
         ("jenis_instrumen;Jenis Instrumen",'S',100)
        ,("kode_valuta;Kode Valuta",'S',100)
        ,("tgl_buka;Tgl. Deal",'D',70)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',70)
        ,("sumber_dana;Sumber Dana",'S',100)
        ,("eqv_rate_real;Imbalan",'S',100)
        ,("nominal_deal;Nominal",'F',110)
      )
    
    if report.upper() in ('Z'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',130,30)
        ,("nomor_rekening;Nomor Rekening",'S',100,25)
        ,("tgl_buka;Tgl. Deal",'D',70,12)
        ,("tgl_awal;Tgl. Awal Perhitungan",'D',110,14)
        ,("tgl_accrue;Tgl. Akhir Perhitungan",'D',110,14)
        ,("accrue_day;Jml. Hari",'I',70,10) 
        ,("basis_hari_pertahun;On Basis",'S',100,10)
        ,("nominal_sbh;Nominal",'F',110,20)
        ,("imbalan;Imbalan (%)",'S',100,15) 
        ,("saldo_awal;Saldo Awal",'F',110,20)
        ,("nilai_accrue;Nilai Accrue",'F',110,20)
        ,("saldo_akhir;Saldo Akhir",'F',110,20) 
        ,("status_rekening;STATUS",'S',80,10)
      ) 
  
  if akad.upper() =='S':  #SIMA
   if report.upper() in ('A'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',120)
        ,("nama_produk;Produk",'S',90)
        ,("tgl_buka;Tgl. Deal",'D',70)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',90)
        ,("jml_hari;Jml. Hari",'I',70)
        ,("nominal_deal;Nominal",'F',110)
        ,("ujroh;Bagi Hasil",'F',110)
        ,("eqv_rate_real;Imbalan (%)",'S',90)
        ,("nisbah;Nisbah (%)",'S',90)
        ,("nomor_sertifikat;Nomor Sertifikat",'S',110)
        ,("jenis_sima;Jenis Sima",'S',110)
        ,("underlying_asset;Underlying Aset",'S',110)        
        ,("tgl_bayar_baghas;Tgl Bayar Baghas",'D',70)
        ,("ppap_umum;PPA UMUM",'F',110)
        ,("ppap_khusus;PPA KHUSUS",'F',110)
        ,("status_rekening;STATUS",'S',80)
      )
  
   if report.upper() in ('I'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',30)
        ,("tgl_buka;Tgl. Deal",'D',12)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',12)
        ,("jml_hari;Jml. Hari",'I',10)
        ,("nominal_deal;Nominal",'F',25)
        ,("ujroh;Bagi Hasil",'F',25)
        ,("eqv_rate_real;Imbalan (%)",'S',10)
        #,("nisbah;Nisbah (%)",'S',90)
        #,("nomor_sertifikat;Nomor Sertifikat",'S',110)
        #,("jenis_sima;Jenis Sima",'S',110)
        #,("underlying_asset;Underlying Aset",'S',110)        
        #,("tgl_bayar_baghas;Tgl Bayar Baghas",'D',70)
        ,("ppap_umum;PPA UMUM",'F',25)
        ,("ppap_khusus;PPA KHUSUS",'F',25)
      )
  
   if report.upper() in ('E'):
      EC =( 
         ("nomor_rekening;Nomor",'S',100)
        ,("golongan_nasabah;Golongan Nasabah",'S',120)     
        ,("hubungan_bank;Hubungan dengan Bank",'S',120)
        ,("status_bank;Status",'S',120)
        ,("pihak_lawan;Negara Pihak Lawan",'S',120)
        ,("jenis_instrumen;Jenis Instrumen",'S',120) 
        ,("additional_fitur;Fitur Tambahan",'S',120) 
        ,("status_instrumen;Status",'S',120)
        ,("kode_valuta;Jenis Valuta",'S',120)
        ,("tgl_buka;Tgl. Deal",'D',70)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',90)
        ,("sumber_dana;Sumber Dana",'S',120) 
        ,("metode_pengukuran;Metode Pengukuran",'S',120)  
        ,("kategori_portofolio;Kategori Portofolio",'S',120)
        ,("rating_agency;Lembaga Pemeringkat",'S',120)
        ,("ranking_value;Nilai Pemeringkat",'S',120)
        ,("rating_date;Tanggal",'D',120)
        ,("jenis_akad;Jenis Akad",'S',120)
        ,("model_baghas;Metode Bagi Hasil",'S',120)
        ,("nisbah;Persentase Nisbah",'F',120)
        ,("nominal_deal;Harga",'F',120) #need confirm
        ,("nominal_deal;Nominal",'F',120) #need confirm
        ,("periode_imbalan;Periode Imbalan",'S',120)
        ,("eqv_rate_real;Awal Kontrak",'F',90)
        ,("eqv_rate_real;Bulan Laporan",'F',90)
        ,("nominal_deal;Bulan Lalu",'F',110) 
        ,("nominal_debet;Debet",'F',110) #need confirm
        ,("nominal_kredit;Kredit",'F',110) #need confirm
        ,("nominal_other;Lainnya",'F',110) #need confirm
        ,("nominal_last;Bulan Laporan",'F',110) #need confirm
        ,("nominal_last;Premium Diskonto",'F',110) #need confirm        
        ,("ujroh;Imbalan Yang diterima",'F',110)
        ,("kolektibilitas;Kualitas",'S',110)
        ,("jenis_agunan;Jenis Agunan/Jaminan",'S',110)
        ,("no_agunan;Nomor Agunan/Jaminan",'S',110)
        ,("legal_binding;Sifat",'S',110)
        ,("golongan_penerbit;Golongan Penerbit",'S',110)
        ,("penilaian_terakhir;Tgl Penilaian Terkahir",'D',110)
        ,("nilai_agunan;Jumlah",'F',110)
        ,("nilai_dijaminkan;Agunan Yang Diperhitungkan",'F',110)
        ,("bagian_dijaminkan;Bagian Dijamin",'F',110)           
        ,("ppap_umum;Individual",'F',110)
        ,("ppap_khusus;Kolektif",'F',110)
      )
     
  if akad.upper() =='B': #SUKUK
    EC =(
      ("kode_instrumen;Kode Instrumen",'S',110)
      ,("nama_counterpart;Counterpart",'S',110)
      ,("nama_produk;Produk",'S',90)
      ,("kode_valuta;Valuta",'S',60)
      ,("nominal_deal;Nominal",'F',130)
      ,("rate_kurs;Rate Kurs",'F',80)
      ,("nominal_ekuivalen;Nominal Ekuivalen",'F',130)
      ,("tgl_buka;Issued Date",'D',80)
      ,("tgl_settlement;Settlement Date",'D',80)
      ,("tgl_jatuh_tempo;maturity Date",'D',80)
      ,("eqv_rate_real;Coupon Rate (%)",'F',80)
      ,("eqv_rate_gross;Yield (%)",'F',80)
      ,("nilai_tunai;Nilai Beli",'F',130)
      ,("harga_beli;Harga Beli (%)",'F',70)
      ,("s_akru;Nilai Akrual",'F',110)
      ,("premium;Premium",'F',100)
      ,("premium_amor;Amortisasi Premium",'F',100)
      ,("diskonto;diskonto",'F',100)
      ,("diskonto_amor;Amortisasi Diskonto",'F',100)
      ,("klasifikasi_SB;Klasifikasi SB",'S',80)
      ,("tipe_SB;Tipe SB",'S',80)
      #,("seri_SB;Seri SB",'S',80) 
      ,("tipe_kupon;Tipe Kupon",'S',100)
      ,("tgl_bayar_terakhir;Tgl. Kupon Terakhir",'D',90)
      ,("tgl_bayar_berikutnya;Tgl Kupon Berikutnya",'D',90)
      #,("ppap_umum;PPA UMUM",'F',110)
      ,("nomor_rekening;Generated Key",'S',80)
      ,("status_rekening;STATUS",'S',80)
    )
    #---
    if report.upper() in ('1'):
      add =(
        ("mut_akru;Mut. Akru",'F',110)
        ,("mut_amor_premium;Mut. Amor Premium",'F',110)
        ,("mut_amor_diskonto;Mut. Amor Diskonto",'F',110)
        ,("mut_kupon;Mut. Kupon",'F',110)
      )
      
      EC += add
    #---
    if report.upper() in ('2'):
      EC =(
         ("tanggal_transaksi;Tanggal Transaksi",'D',100)
        #,("kode_transaksi;Kode Transaksi",'S',70)
        ,("nama_counterpart;Nama Counterpart",'S',220)
        ,("kode_valuta;Kode Valuta",'S',70)

        ,("nomor_rekening;Generated Key",'S',100)
        ,("kode_instrumen;Kode Instrumen",'S',100)

        ,("kode_tx_class;Kode Tx Class",'S',70)
        ,("kode_account;Nomor GL",'S',70)
        #,("deskripsi;Jns Saldo",'S',70)
        ,("mutasi_tx;Nominal Transaksi",'F',130)
        ,("journal_no;Journal No",'S',90)
        ,("keterangan;Keterangan",'S',250)
      )
    #---
    if report.upper() in ('3'):
      EC =(
         ("kode_tx_class;Kode Tx",'S',70)
        ,("deskripsi;Jenis Tx",'S',170)
        ,("nama_counterpart;Nama Counterpart",'S',220)
        ,("kode_produk;Kode Produk",'S',70)
        ,("kode_valuta;Kode Valuta",'S',70)
        ,("kode_cabang;Kode cabang",'S',70)

        ,("mutasi_tx;Nominal Transaksi",'F',130)
        ,("jml_tx;Jumlah Transaksi",'S',90)
      )
  #---
  
  if akad.upper() =='R': #Reverse Repo
    if report.upper() in ('A'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',120)
        ,("tgl_buka;Tgl. Deal",'D',70)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',70)
        ,("jml_hari;Jml. Hari",'I',70)
        ,("nominal_deal;Nominal",'F',110)
        ,("ujroh;Bagi Hasil",'F',110)
        ,("eqv_rate_real;Imbalan",'F',100)
        ,("nilai_tunai;Nominal (IDR)",'F',110)
        ,("nomor_ref_deal;Nomor Referensi",'S',120)
        #,("ppap_umum;PPA UMUM",'F',110)
        #,("ppap_khusus;PPA KHUSUS",'F',110)
        ,("status_rekening;STATUS",'S',80)
      )
      
    if report.upper() in ('I'):
      EC =(
         ("nama_counterpart;Nama Bank",'S',30)
        ,("tgl_buka;Tgl. Deal",'D',12)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',12)
        ,("jml_hari;Jml. Hari",'I',10)
        ,("nominal_deal;Cash Value",'F',25)
        ,("ujroh;Bagi Hasil",'S',25)
        ,("eqv_rate_real;Imbalan",'F',10)
        ,("nilai_tunai;Nominal (IDR)",'F',25)
      )
    
    if report.upper() in ('E'):
      EC =( 
         ("nomor_ref_deal;ID",'S',80)
        ,("nomor_rekening;Nomor Rekening",'S',100)
        ,("jenis_instrumen;Jenis Instrumen",'S',120)
        ,("jenis_akad;Jenis Akad",'S',120) 
        ,("tgl_buka;Mulai",'D',70)
        ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',90)
        ,("eqv_rate_real;Persentase Imbalan ",'F',90)
        ,("nilai_tunai;Nominal",'F',120) #need confirm
        ,("sumber_dana;Sumber Dana",'S',120)
        ,("nominal_deal;Bulan Lalu",'F',120) #need confirm
        ,("nominal_deal;Bulan Laporan",'F',120) #need confirm
        ,("ujroh;Imbalan Diterima",'S',120) #need confirm
      )  
  
  if akad.upper() =='A': #Reksadana
    #if report.upper() in ('A'):
    EC =(
      ("nomor_rekening;Nomor Rekening",'S',100)
      ,("nama_rekening;Account Name",'S',120)
      ,("nama_counterpart;Nama Bank",'S',150)
      ,("tgl_buka;Tgl. Deal",'D',70)
      ,("tgl_jatuh_tempo;Tgl. Jatuh Tempo",'D',90)
      ,("nominal_deal;Nilai Perolehan",'F',120)
      ,("harga_beli;Nilai Unit",'F',110)
      ,("nilai_nav;Nav",'F',100)
      ,("saldo;Nominal (IDR)",'F',120)
      ,("saldo_biaya;Unrealized Gain",'F',120)
      #,("nilai_tunai_sekarang;Nilai Tunai Sekarang (IDR)",'F',130)
      #,("nomor_ref_deal;Nomor Referensi",'S',120)
      ,("ppap_umum;PPA UMUM",'F',110)
      ,("ppap_khusus;PPA KHUSUS",'F',110)
      ,("kolektibilitas;Kolektibilitas",'F',80)
      ,("status_rekening;STATUS",'S',80)
    )
  
    #---
    if report.upper() in ('1'):
      add =(
        ("mut_saldo;Mut. Nominal",'F',110)
        ,("mut_akru;Mut. Mark to Market",'F',110)
      )
      
      EC += add

    #---
    if report.upper() in ('2'):
      EC =(
         ("tanggal_transaksi;Tanggal Transaksi",'D',80)
        #,("kode_transaksi;Kode Transaksi",'S',70)
        ,("nama_counterpart;Nama Counterpart",'S',220)
        ,("kode_valuta;Kode Valuta",'S',70)

        ,("nomor_rekening;Generated Key",'S',100)
        ,("nama_rekening;Nama Account",'S',160)

        ,("kode_tx_class;Kode tx class",'S',70)
        ,("kode_account;Nomor GL",'S',70)
        ,("mutasi_tx;Nominal Transaksi",'F',130)
        ,("journal_no;Journal No",'S',90)
        ,("keterangan;Keterangan",'S',250)
      )

    #---
    if report.upper() in ('3'):
      EC =(
         ("kode_tx_class;Kode Tx",'S',70)
        ,("deskripsi;Jenis Tx",'S',170)
        ,("nama_counterpart;Nama Counterpart",'S',220)
        ,("kode_produk;Kode Produk",'S',70)
        ,("kode_valuta;Kode Valuta",'S',70)
        ,("kode_cabang;Kode cabang",'S',70)

        ,("mutasi_tx;Nominal Transaksi",'F',130)
        ,("jml_tx;Jumlah Transaksi",'S',90)
      )
  #---

  if akad.upper() =='C': #BankAccount
    EC =(
       ("nomor_rekening;Nomor Rekening",'S',110)
      ,("nama_rekening;Nama Rekening",'S',300)
      ,("nama_counterpart;Counterpart",'S',260)
      ,("kode_cabang;Cabang",'S',80)
      ,("nama_produk;Produk",'S',220)
      ,("kode_valuta;Kode Valuta",'S',70)
      ,("saldo;Saldo",'F',110)
      #,("tgl_buka;Tgl. Buka",'D',70)
      ,("keterangan;Keterangan",'S',120)
      ,("status_rekening;Status",'S',80)
    )
    
    #---
    if report.upper() in ('1'):
      add =(
        ("mut_debit;Mut. Debit",'F',110)
        ,("mut_kredit;Mut. Kredit",'F',110)
      )
      
      EC += add

    #---
    if report.upper() in ('2'):
      EC =(
         ("tanggal_transaksi;Tanggal Transaksi",'D',80)
        ,("kode_transaksi;Kode Transaksi",'S',70)
        ,("nama_counterpart;Nama Counterpart",'S',220)
        ,("kode_valuta;Kode Valuta",'S',70)

        ,("nomor_rekening;Generated Key",'S',100)
        ,("nama_rekening;Nama Account",'S',160)

        ,("kode_tx_class;Kode tx class",'S',70)
        ,("kode_account;Nomor GL",'S',70)
        ,("mutasi_tx;Nominal Transaksi",'F',130)
        ,("journal_no;Journal No",'S',90)
        ,("keterangan;Keterangan",'S',250)
        ,("kd_remark;No. Referensi",'S',80)
      )

    #---
    if report.upper() in ('3'):
      EC =(
         ("kode_tx_class;Kode Tx",'S',70)
        ,("deskripsi;Jenis Tx",'S',170)
        ,("nama_counterpart;Nama Counterpart",'S',220)
        ,("kode_valuta;Kode Valuta",'S',70)
        ,("kode_cabang;Kode cabang",'S',70)

        ,("mutasi_tx;Nominal Transaksi",'F',130)
        ,("jml_tx;Jumlah Transaksi",'S',90)
      )
  #---

  if akad.upper() =='H': #HajjAccount
    if report.upper() in ('A'):
      EC =(
         ("nomor_porsi;Nomor Porsi",'S',80)
        ,("nama_rekening;Nama Jamaah",'S',120)
        ,("nama_produk;Produk",'S',100)
        ,("nama_counterpart;Counterpart",'S',120)
        #,("kode_cabang;Cabang",'S',80)
        ,("kode_valuta;Kode Valuta",'S',70)
        ,("saldo;Setoran Awal",'F',110)
        ,("saldo_lunas;Setoran Lunas",'F',110)
        ,("saldo_tunda;Titipan Tunda",'F',110)
        ,("saldo_manfaat;Nilai Manfaat",'F',110)
        ,("tgl_buka;Tgl. Buka",'D',70)
        ,("keterangan;Keterangan",'S',120)
        ,("status_rekening;Status",'S',80)
        ,("nomor_rekening;Generated Key",'S',120)
      )
      
    if report.upper() in ('R'):
      EC =(
        ("nama_counterpart;Counterpart",'S',170)
        ,("kode_valuta;Kode Valuta",'S',70)
        ,("saldo;Setoran Awal",'F',110)
        ,("saldo_lunas;Setoran Lunas",'F',110)
        ,("saldo_tunda;Titipan Tunda",'F',110)
        ,("saldo_manfaat;Nilai Manfaat",'F',110)
        ,("jml_data;Jumlah Data",'I',80)
      )
    #raise Exception, EC
    if report.upper() in ('1'):
      EC =(
         ("nomor_porsi;Nomor Porsi",'S',80)
        ,("nama_rekening;Nama Jamaah",'S',120)
        ,("tgl_buka;Tgl. Buka",'D',70)
        ,("kode_produk;Produk",'S',100)
        ,("nama_counterpart;Counterpart",'S',120)
        ,("kode_valuta;Kode Valuta",'S',70)
        ,("setoran_awal;Setoran Awal",'F',110)
        ,("setoran_lunas;Setoran Lunas",'F',110)
        ,("setoran_tunda;Titipan Tunda",'F',110)
        ,("nilai_manfaat;Nilai Manfaat",'F',110)

        ,("mut_debit_sa;Mut. Debit SA",'F',110)
        ,("mut_kredit_sa;Mut. Kredit SA",'F',110)
        ,("mut_debit_sl;Mut. Debit SL",'F',110)
        ,("mut_kredit_sl;Mut. Kredit SL",'F',110)
        ,("mut_debit_td;Mut. Debit Tunda",'F',110)
        ,("mut_kredit_td;Mut. Kredit Tunda",'F',110)
        ,("mut_debit_nm;Mut. Debit NM",'F',110)
        ,("mut_kredit_nm;Mut. Kredit NM",'F',110)

        ,("no_rek_bank;Nomor Ref.",'S',120)
        ,("norek_sa;Rek. Bank",'S',120)
        ,("status_rekening;Status",'S',80)
        ,("nomor_rekening;Generated Key",'S',120)
      )
      
    if report.upper() in ('2'):
      EC =(
         ("kode_produk;Produk",'S',100)
        ,("nama_counterpart;Counterpart",'S',120)
        ,("kode_valuta;Kode Valuta",'S',70)

        ,("jml;Jumlah",'F',110)
        ,("setoran_awal;Setoran Awal",'F',110)
        ,("setoran_lunas;Setoran Lunas",'F',110)
        ,("setoran_tunda;Titipan Tunda",'F',110)
        ,("nilai_manfaat;Nilai Manfaat",'F',110)

        ,("mut_debit_sa;Mut. Debit SA",'F',110)
        ,("mut_kredit_sa;Mut. Kredit SA",'F',110)
        ,("mut_debit_sl;Mut. Debit SL",'F',110)
        ,("mut_kredit_sl;Mut. Kredit SL",'F',110)
        ,("mut_debit_td;Mut. Debit Tunda",'F',110)
        ,("mut_kredit_td;Mut. Kredit Tunda",'F',110)
        ,("mut_debit_nm;Mut. Debit NM",'F',110)
        ,("mut_kredit_nm;Mut. Kredit NM",'F',110)
      )
    if report.upper() in ('3'):
      EC =(
         ("tanggal_transaksi;Tanggal Transaksi",'D',100)
        ,("kd_remark;Kode Remark",'S',70)
        ,("kode_counterpart;Kode Bank",'S',70)
        ,("nama_counterpart;Nama bank",'S',220)
        ,("kode_valuta;Kode Valuta",'S',70)

        ,("nomor_porsi;Nomor Porsi",'S',100)
        ,("nomor_referensi;Nomor Referensi",'S',100)
        ,("nama_rekening;Nama Jamaah",'S',200)

        ,("mutasi_tx;Nominal Transaksi",'F',130)
        ,("nomor_rekening;Generated Key",'S',120)
      )
    if report.upper() in ('4'):
      EC =(
         ("kd_remark;Kode Remark",'S',70)
        ,("kode_counterpart;Kode Bank",'S',50)
        ,("nama_counterpart;Nama bank",'S',220)
        ,("kode_valuta;Kode Valuta",'S',70)

        ,("jml_tx;Jml Transaksi",'F',90)
        ,("mutasi_tx;Nominal Transaksi",'F',130)
      )

  return EC