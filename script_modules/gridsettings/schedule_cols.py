# dictionary key is kode_jenis in rekening transaksi

def cols_setting(akad):
  vQry ='''
      item
        Expanded = False
        FieldName = '%s'
        Title.Caption = '%s'
        Width = %s
        Visible = True
      end
    '''

  _cols = ex_cols(akad)
  lView=[]
  for i in _cols:
    ls = i[0].split(";")
    fName = ls[0]
    tCaption = ls[0]
    if len(ls)>1:
      tCaption = ls[1]
    t_view = vQry % (fName, tCaption, i[2])
    lView.append(t_view)
 
  _dict = {
    '_vGrid':" ".join(lView)
  } 

  
  CS = '''
    object TColumnsWrapper
      Columns = <
        %(_vGrid)s
        >
    end
  ''' % _dict

  return CS
  
def ex_cols(akad):  
  if akad.upper() in ('A','H'): #murabahah
    EC =(
       ("seq_number;Angsuran Ke-",'I',70)
      ,("tgl;Tanggal JT",'D',70)
      ,("tgl_bayar;Tgl Bayar",'D',70)
      ,("outstanding_pokok; O/S Pokok",'I',110)
      ,("angsuran_pokok;Angs Pokok",'F',110)
      ,("angsuran_margin;Angs Margin",'F',100)
      ,("mukasah",'F',100)
      ,("angsuran_total;Angs Total",'F',100) 
      ,("realisasi_margin;Bayar Margin",'F',100)
      ,("realisasi_mukasah;Realisasi Mukasah",'F',100)
      ,("realized;Status Bayar",'S',70)
      ,("DPD",'I',50)
      ,("Denda_Tawid;Denda Tawidh",'F',100)
      ,("Denda_Tazir;Denda Tazir",'F',100)
    )
  
  elif akad.upper() in ('B','C'): #musyarakah
    EC =(
       ("seq_number;Angsuran Ke-",'I',70)
      ,("tgl;Tanggal JT",'D',70)
      ,("tgl_bayar;Tgl Bayar",'D',70)
      ,("outstanding_pokok; O/S Pokok",'I',100)
      ,("angsuran_pokok;Angs Pokok",'F',100)
      ,("proyeksi_pdpt_all;Proyeksi GP",'F',100)
	    ,("nisbah_bank;Nisbah Bank",'I',60)
	    #,("nisbah_nsb",'',60)
	    ,("porsi_bank;Proyeksi Pdpt Bank",'I',100)
	    ,("realisasi_Gross_profit;Realisasi GP",'F',100)
	    ,("realisasi_nisbah;Realisasi Nisbah",'F',60)
	    ,("realisasi_pdpt;Realisasi Pdpt Bank",'F',100)
      ,("angsuran_total;Angs Total",'F',100)
      ,("realized;Status Bayar",'S',70)
      ,("PBH_VS_RBH_AKUM;RBH VS PBH",'S',70)
      ,("DPD",'I',50)
      ,("Denda_Tawid;Denda Tawidh",'F',100)
      ,("Denda_Tazir;Denda Tazir",'F',100)
    )
  
  if akad.upper() =='F':  #ijarah
    EC =(
       ("seq_number;Angsuran Ke-",'I',70)
      ,("tgl;Tanggal JT",'D',70)
      ,("tgl_bayar;Tgl Bayar",'D',70)
      ,("outstanding_pokok; O/S Pokok",'I',100)
      ,("angsuran_pokok;Angs Pokok",'F',100)
      ,("angsuran_margin;Angs Margin",'F',100)
      ,("angsuran_total;Angs Total",'F',100)
      ,("realisasi_bagi_hasil;Realisasi Bagi Hasil",'F',100)
      ,("realized;Status Bayar",'S',70)
      ,("DPD",'I',50)
      ,("Denda_Tawid;Denda Tawidh",'F',100)
      ,("Denda_Tazir;Denda Tazir",'F',100)
    )
  
  if akad.upper() =='G': #prks
    EC =(
       ("seq_number;Angsuran Ke-",'I',70)
      ,("tgl;Tanggal JT",'D',70)
      ,("tgl_bayar;Tgl Bayar",'D',70)
      ,("proyeksi_pendapatan; Proyeksi Pendapatan",'F',110)
      ,("nisbah; Nisbah",'F',50)
      ,("proyeksi_baghas; Proyeksi BagiHasil",'F',110)
      ,("srr_outstanding; Saldo Rata2 Pemakaian",'F',110)
      ,("proyeksi_bln_ini;Proyeksi Bln. Ini",'F',110)
      ,("deklarasi_pendapatan;Deklarasi Pendapatan",'F',110)
      ,("deklarasi_baghas; Deklarasi BagiHasil",'F',100)
      ,("realisasi_baghas;Realisasi BagiHasil",'F',100)
      ,("PBH_VS_RBH_AKUM;RBH VS PBH",'S',70)
      ,("realized;Status Bayar",'S',70)
      ,("penalty_counter;DPD",'I',40)
      ,("Denda_Tawid;Denda Tawidh",'F',100)
      ,("Denda_Tazir;Denda Tazir",'F',100)
    )
  
  if akad.upper() =='D': #qardh
    EC =(
       ("seq_number;Angsuran Ke-",'I',70)
      ,("tgl;Tanggal JT",'D',70)
      ,("tgl_bayar;Tgl Bayar",'D',70)
      ,("outstanding; O/S Qardh",'I',100)
      ,("angsuran;Angsuran",'F',100)
      ,("realized;Status Bayar",'S',70)
      ,("DPD",'I',50)
      ,("Denda_Tawid;Denda Tawidh",'F',100)
      ,("Denda_Tazir;Denda Tazir",'F',100)
    )
  
  if akad.upper() =='E': #MMQ
    EC =(
       ("seq_number;Angsuran Ke-",'I',70)
      ,("tgl;Tanggal JT",'D',70)  
      ,("tgl_bayar;Tgl Bayar",'D',70)
      ,("outstanding_pokok;O/S Pokok",'I',100)
      ,("angsuran_pokok;Angs Pokok",'F',100)
      ,("profit_amount;Bagi Hasil Bank",'F',100)
      ,("realized_margin;Realisasi Pendapatan", 'F',110)
      ,("angsuran_total;Angs Total",'F',100)
      ,("nisbah_bank;Kepemilikan Nasabah",'I',80)
      ,("porsi_bank",'I',70)
      #,("porsi_nasabah",'I',70)
      ,("realized;Status Bayar",'S',70)
      ,("DPD",'I',50)
      ,("Denda_Tawid;Denda Tawidh",'F',100)
      ,("Denda_Tazir;Denda Tazir",'F',100)
    )
  

  return EC