# dictionary key is kode_jenis in rekening transaksi

COLUMN_SETTINGS = {
  'MBH': '''
    object TColumnsWrapper
      Columns = <
        item
          Expanded = False
          FieldName = 'TANGGAL_TRANSAKSI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KETERANGAN'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_TRANSAKSI'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'ID_DETILTRANSGROUP'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KODE_ENTRI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NAMA_ENTRI'
          Width = 106
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KODE_VALUTA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PEMBAYARAN'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OUTSTANDING'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MARGIN_DITANGGUHKAN'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PENDAPATAN'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PENDAPATAN_AKRU'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MUKASAH'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MARGIN_PENYELESAIAN'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WRITE_OFF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PPAP_KHUSUS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TUNGGAKAN'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PPAP_UMUM'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PPAP_ADJUSTMENT'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DENDA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KWJB_DENDA'
          Visible = True
        end>
    end
  '''
}

# clone qardh from murabahah
COLUMN_SETTINGS['QRD'] = COLUMN_SETTINGS['MBH']

EXCEL_COLUMN =(
  ("TANGGAL_TRANSAKSI;TANGGAL",'D'),
  ("KETERANGAN",'S'),
  ("ID_TRANSAKSI",'I'),
  ("ID_DETILTRANSGROUP",'I'),
  ("KODE_ENTRI",'S'),
  ("NAMA_ENTRI",'S'),
  ("KODE_VALUTA",'S'),
  ("PEMBAYARAN",'F'),
  ("OUTSTANDING",'F'),
  ("MARGIN_DITANGGUHKAN",'F'),
  ("PENDAPATAN",'F'),
  ("PENDAPATAN_AKRU",'F'),
  ("MUKASAH",'F'),
  ("MARGIN_PENYELESAIAN",'F'),
  ("WRITE_OFF",'F'),
  ("PPAP_KHUSUS",'F'),
  ("TUNGGAKAN",'F'),
  ("PPAP_UMUM",'F'),
  ("PPAP_ADJUSTMENT",'F'),
  ("DENDA",'F'),
  ("KWJB_DENDA",'F')
) 