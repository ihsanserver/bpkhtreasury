# dictionary key is kode_jenis in rekening transaksi

COLUMN_SETTINGS = '''
    object TColumnsWrapper
      Columns = <
        item
          Expanded = False
          FieldName = 'TANGGAL_TRANSAKSI'
          Title.Caption = ' TANGGAL'
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KETERANGAN'
          Width = 252
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KODE_TX_CLASS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'JENIS_MUTASI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NILAI_MUTASI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KODE_VALUTA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DESKRIPSI'
          Width = 186
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'id_detil_transaksi'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'JOURNAL_NO'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'USER_INPUT'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'USER_OTORISASI'
          Width = 80
          Visible = True
        end
        >
    end
  '''
  
EXCEL_COLUMN =(
  ("TANGGAL_TRANSAKSI;TANGGAL",'D'),
  ("KETERANGAN",'S'),
  ("KODE_TX_CLASS",'S'),
  ("JENIS_MUTASI",'S'),
  ("NILAI_MUTASI",'F'),
  ("KODE_VALUTA",'S'),
  ("DESKRIPSI",'S'),
  #("NILAI_KURS",'F'),
  #("ID_DETIL_TRANSAKSI",'I'),
  ("JOURNAL_NO",'S'),
  ("USER_INPUT",'S'),
  ("USER_OTORISASI",'S')
)

'''
        item
          Expanded = False
          FieldName = 'ID_TRANSAKSI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMOR_SERI'
          Visible = True
        end        
'''