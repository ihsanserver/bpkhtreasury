# dictionary key is kode_jenis in rekening transaksi

COLUMN_SETTINGS = '''
    object TColumnsWrapper
      Columns = <
        item
          Expanded = False
          FieldName = 'TANGGAL_VALUTA'
          Title.Caption = 'TANGGAL'
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KETERANGAN_TRX'
          Title.Caption = 'DESKRIPSI'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMINAL'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KODE_TRX'
          Visible = True
        end>
    end
  '''
  
EXCEL_COLUMN =(
  ("TANGGAL_VALUTA;TANGGAL",'D'),
  ("KETERANGAN_TRX;DESKRIPSI",'S'),
  ("NOMINAL",'F'),
  ("KODE_TRX",'S')
)
