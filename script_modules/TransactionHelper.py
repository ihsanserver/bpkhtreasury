import sys
import os
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.dbutil as dbutil

class TransactionHelper :
  @staticmethod
  def getAkadName(oFinAccount):
    classname = oFinAccount.ClassName.lower()
    akad_name = ""
    if classname == 'finmurabahahaccount' : akad_name = "MURABAHAH"
    elif classname == 'finmusyarakahaccount' : akad_name = "MUSYARAKAH"
    elif classname == 'finqardhaccount' : akad_name = "QARDH"
    elif classname == 'finmudharabahaccount' : akad_name = "MUDHARABAH"
    elif classname == 'finmmqaccount' : akad_name = "MMQ" 
    elif classname == 'finijarahaccount' : akad_name = "IJR" 
    elif classname == 'finistishnaaccount' : akad_name = "ISTISHNA"      
    elif classname == 'finprkaccount' : akad_name = "PRK" 
    elif classname == 'kafalah' : akad_name = "KAFALAH" 
    else : raise Exception, "unconfigured akad_name for class %s" % classname  
    return akad_name

  @staticmethod  
  def CekLimitTransaksi(config, id_otorisasi, transaction_amount):
    dParam ={
      'otorentri': config.MapDBTableName("enterprise.otorentri"),
      'limittransaksi': config.MapDBTableName("enterprise.LimitTransaksi"),
      'id_otorisasi':id_otorisasi,
      'transaction_amount':transaction_amount
    }
    dbutil.runSQL(config, '''
      UPDATE %(otorentri)s 
      SET transaction_amount = %(transaction_amount)s, override_state='F'
      WHERE id_otorisasi=%(id_otorisasi)s
    ''' % dParam )
    
    sSQL = '''
      merge into %(otorentri)s target using
      (
        SELECT 
          ID_OTORISASI, max(CASE WHEN nvl(b.transaction_amount,0) <= nvl(a.NILAI_LIMIT,0) then 1 else 0 end) is_override
        FROM 
          %(limittransaksi)s a, %(otorentri)s b
        WHERE 
          b.id_otorisasi=%(id_otorisasi)s 
          and a.id_user=b.user_input and a.jenis_limit in ('ND','NC')
        GROUP BY ID_OTORISASI
      ) src ON (target.ID_OTORISASI=src.ID_OTORISASI)
      when matched then update set 
      	target.override_state = decode(src.is_override,0,'F',NULL)
    ''' % dParam
    dbutil.runSQL(config, sSQL)
    
    sSQL = '''
      SELECT override_state FROM %(otorentri)s WHERE id_otorisasi=%(id_otorisasi)s 
    ''' %dParam
    q = config.CreateSQL(sSQL).RawResult
    
    return q.override_state or ''
    
    
      