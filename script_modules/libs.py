''' com.ihsan.apps.ifs.libs
'''

import math
import calendar as cal
import com.ihsan.utils as utils
import com.ihsan.lib.trace as trace
import com.ihsan.util.modman as modman
import types
import calendar
           
class installment:
    def __init__(self, n, config = None):
        items = []
        for i in range(n):
            items.append(installment_item())
        
        self.items          = items
        self.duration       = n
        self.base           = 0
        self.rate           = 0
        self.installment    = 0
        self.margin         = 0
        self.pct_r          = 0
        self.config = config
    
    def getitem(self, i):
        return self.items[i-1]
                             
        
    def getTieringList(self, tiering_code):
        config = self.config
        
        mlu = config.ModLibUtils
        q = config.CreateSQL('''
            SELECT * FROM %s
            WHERE tiering_code = %s ORDER BY seq_from
          ''' %(
            config.MapDBTableName('FinParameterTieringDetail'),
            mlu.QuotedStr(tiering_code) 
          ) 
        ).RawResult
        ltiering = []
        while not q.Eof:
          ltiering.append({'seq_from': q.seq_from, 'seq_to': q.seq_to, 'rate': q.pct_value})
          q.Next()
          
        return ltiering
      
    def runSimulation(self, enumMethod, principal_amount, annual_eff_rate_pct, tiering_code = None):
        config = self.config
        if enumMethod == '1' and (tiering_code or '') != '':
          ltiering = self.getTieringList(tiering_code)
          self.efektifWithTiering(principal_amount, ltiering) 
        else:
          dictMethod = {
            '2': self.sliding,
            '1': self.efektif,
            '4': self.rest,
            '5': self.progress,
            '6': self.flat
          }
          meth = dictMethod.get(enumMethod, None)
          if meth == None:
            raise Exception, "Pricing method %s not supported" % enumMethod
          meth(principal_amount, annual_eff_rate_pct)
        #-- else
    #-- runSimulation
        
    def efektif(self, base, rate):
        self.base   = base
        self.rate   = rate
        
        n       = self.duration
        prate   = rate / 100
        mvalue  = base * prate / 12
        mrate   = prate / 12         
        multiplier  = math.pow(1 + mrate, n)
        angsuran    = mvalue / (1 - (1 / multiplier))
        angsuran    = pembulatan(angsuran)
        totalmargin = pembulatan(angsuran * n - base)

        self.installment = angsuran
        self.margin      = totalmargin
        
        efek_out = base
        for i in range(1, n):
            item = self.getitem(i)
            
            item.angs   = i
            item.nilang = angsuran            
            efek_pm     = pembulatan(efek_out * mrate)
            item.pm     = pembulatan(efek_pm)
            efek_pp     = pembulatan(angsuran - efek_pm)
            item.pp     = pembulatan(efek_pp)
            item.out    = pembulatan(efek_out)
            efek_out    = pembulatan(efek_out - efek_pp)
            item.pct_r  = prate
        #~ end of for : i == n - 1
        
        item        = self.getitem(n)
        item.angs   = n
        item.nilang = pembulatan(angsuran)
        item.out    = pembulatan(efek_out)
        item.pp     = pembulatan(efek_out)
        item.pm     = pembulatan(angsuran - efek_out)
        item.pct_r  = prate
        
        return None
        
    def isBalloonPayment(self):
        n    = self.duration
        efek_out = self.base
        for i in range(1, n):
            item = self.getitem(i)
            
            item.angs   = i 
            item.pp     = 0.0
            item.out    = efek_out
            item.nilang = item.pm            
        #~ end of for : i == n - 1
        
        item        = self.getitem(n)
        item.angs   = n
        item.out    = pembulatan(efek_out)
        item.pp     = pembulatan(efek_out)
        #item.pm     = angsuran - efek_out
        item.nilang = pembulatan(efek_out+item.pm)
        
        return None

    def efektifWithTiering(self, base, tieringArray):
        # tiering array consists dictionaries having seq_from, seq_to and rate attributes
        
        self.base   = base
        #raise Exception, tieringArray
        n = self.duration
        if self.duration <= 0:
          raise Exception, "Invalid duration %d" % self.duration
        # validate tiering parameters
        if len(tieringArray) < 1:
          raise Exception, "Invalid tiering parameter. Insufficient range"
        last_tiering = -1
        seq_from = 0
        seq_to = 0
        switch = True
        i = 1
        while i <= self.duration:
          if switch:
            last_tiering = last_tiering + 1
            if last_tiering >= len(tieringArray):
              raise Exception, "Invalid tiering parameter. Insufficient range"
            tiering = tieringArray[last_tiering]
            seq_from = tiering['seq_from']
            seq_to = tiering['seq_to']
            if seq_from > seq_to:
              raise Exception, "Invalid tiering parameter at %d -th position (%d-%d)" % (last_tiering, seq_from, seq_to) 
            if tiering['seq_from'] != i:
              raise Exception, "Invalid tiering parameter at %d -th position (%d-%d)" % (last_tiering, seq_from, seq_to)
            switch = False
          #-- if switch
          i = i + 1
          if i > seq_to:
            switch = True
          #==
        #--
        
        self.rate = tieringArray[0]['rate']
        last_tiering = -1
        seq_from = 0
        seq_to = 0
        switch = True
        i = 1
        current_base = self.base
        current_period = self.duration
        
        totalmargin = 0.0
        cumulative_margin = 0.0
        
        while i < self.duration or switch:
          if switch:
            last_tiering = last_tiering + 1
            tiering = tieringArray[last_tiering]
            seq_from = tiering['seq_from']
            seq_to = tiering['seq_to']
            rate = tiering['rate']

            prate   = rate / 100.0
            mvalue  = current_base * prate / 12.0
            mrate   = prate / 12.0
            multiplier = math.pow(1 + mrate, current_period)
            angsuran = pembulatan(mvalue / (1 - (1 / multiplier)))
            
            switch = False
          #-- if switch
          
          if i < self.duration:
            item = self.getitem(i)
            item.angs   = i
            item.nilang = angsuran            
            efek_pm     = pembulatan(current_base * mrate)
            item.pm     = efek_pm # pembayaran margin
            efek_pp     = angsuran - efek_pm 
            item.pp     = efek_pp # pembayaran pokok
            item.out    = current_base
            item.pct_r  = prate
            current_base  = current_base - efek_pp # outstanding pokok
            cumulative_margin = cumulative_margin + item.pm
            current_period = current_period - 1
            #~ end of for : i == n - 1
            i = i + 1
            if i > seq_to:
              switch = True
            #==
          #--
          
        #-- while i < self.duration or switch
        
        item        = self.getitem(self.duration)
        item.angs   = self.duration
        item.nilang = angsuran
        item.out    = current_base
        item.pp     = current_base
        item.pm     = angsuran - current_base
        item.pct_r  = prate
        cumulative_margin = cumulative_margin + item.pm
        self.margin = cumulative_margin

    def sliding(self, base, rate):
        self.base   = base
        # assume input is always effective rate
        #raise Exception, rate
        #rate = effectiveToFlatRate(rate, self.duration)
        self.rate   = rate
        
        n           = self.duration
        prate       = rate / 100
        mrate       = prate / 12
        slide_pp    = pembulatan(base / n)

        slide_out   = base
        margin      = 0.0
        for i in range(1, n):
            item = self.getitem(i)
            
            item.angs   = i
            item.pp     = slide_pp
            slide_pm    = pembulatan(slide_out * mrate)
            item.pm     = slide_pm            
            item.nilang = slide_pp + slide_pm            
            item.out    = slide_out
            slide_out   = slide_out - slide_pp
            margin      = margin + slide_pm
            item.pct_r  = prate
        #~ end of for : i == n - 1
        
        item        = self.getitem(n)
        item.angs   = n
        item.pp     = slide_out
        slide_pm    = pembulatan(slide_out * mrate)
        item.pm     = slide_pm
        item.nilang = slide_out + slide_pm
        item.out    = slide_out
        margin      = margin + slide_pm
        item.pct_r  = prate
        
        self.installment = self.getitem(1).nilang
        self.margin      = margin
        
        return None
        
    def sliding2(self, base, rate):
        self.base   = base
        # assume input is always effective rate
        #rate = effectiveToFlatRate(rate, self.duration)
        self.rate   = rate
        
        n           = self.duration
        prate       = rate / 100
        mrate       = prate / 12
        slide_pp    = pembulatan(base / n)

        slide_out   = base
        margin      = 0.0
        for i in range(1, n):
            item = self.getitem(i)
            
            item.angs   = i
            item.pp     = slide_pp
            slide_pm    = pembulatan(slide_out * mrate)
            item.pm     = slide_pm            
            item.nilang = slide_pp + slide_pm            
            item.out    = slide_out
            item.pct_r  = prate
            slide_out   = slide_out - slide_pp
            margin      = margin + slide_pm
        #~ end of for : i == n - 1
        
        item        = self.getitem(n)
        item.angs   = n
        item.pp     = slide_out
        slide_pm    = pembulatan(slide_out * mrate)
        item.pm     = slide_pm
        item.nilang = slide_out + slide_pm
        item.pct_r  = prate
        item.out    = slide_out
        margin      = margin + slide_pm
        
        self.installment = self.getitem(1).nilang
        self.margin      = margin
        
        return None

    def rest(self, base, rate):
        self.base   = base
        self.rate   = rate
        
        n       = self.duration
        prate   = rate / 100
        mvalue  = base * prate / 12
        mrate   = prate / 12
        multiplier  = math.pow(1 + prate, n / 12)
        angsuran    = mvalue / (1 - (1 / multiplier))
        angsuran    = pembulatan(angsuran)
        totalmargin = angsuran * n - base

        self.installment = angsuran
        self.margin      = totalmargin
        
        rest_out = base
        for i in range(1, n):
            item = self.getitem(i)
            
            item.angs   = i
            item.nilang = angsuran            
            rest_pm     = pembulatan(rest_out * mrate)
            item.pm     = rest_pm
            rest_pp     = angsuran - rest_pm
            item.pp     = rest_pp
            item.out    = rest_out
            item.pct_r  = prate
            rest_out    = rest_out - rest_pp
        #~ end of for : i == n - 1
        
        item        = self.getitem(n)
        item.angs   = n
        item.nilang = angsuran
        item.out    = rest_out
        item.pp     = rest_out
        item.pm     = angsuran - rest_out
        item.pct_r  = prate
        
        return None
        
    def progress(self, base, rate):
        self.base   = base
        # assume effective rate
        #rate = effectiveToFlatRate(rate, self.duration)
        self.rate   = rate
        
        n = self.duration
        prate       = rate / 100
        mrate       = prate / 12
        slide_pp    = pembulatan(base / n)

        slide_out   = base
        margin      = 0.0
        j = n
        for i in range(1, n):
            item    = self.getitem(i)
            pitem   = self.getitem(j)
            
            item.angs       = i
            item.pp         = slide_pp
            slide_pm        = pembulatan(slide_out * mrate)
            pitem.pm        = slide_pm            
            pitem.nilang    = slide_pp + slide_pm
            item.pct_r  = prate            
            item.out        = slide_out
            slide_out       = slide_out - slide_pp
            margin          = margin + slide_pm
            
            j = j - 1
        #~ end of for : i == n - 1
        
        item            = self.getitem(n)
        pitem           = self.getitem(1)
        item.angs       = n
        item.pp         = slide_out
        slide_pm        = pembulatan(slide_out * mrate)
        pitem.pm        = slide_pm
        pitem.nilang    = slide_out + slide_pm
        item.pct_r  = prate
        item.out        = slide_out
        margin          = margin + slide_pm
        
        self.installment = pitem.nilang
        self.margin      = margin
        
        return None

    def flat(self, base, rate):
        self.base   = base
        # assume effective rate
        rate = effectiveToFlatRate(rate, self.duration)
        self.rate   = rate
        
        n       = self.duration
        prate   = rate / 100
        mrate   = prate / 12
        
        flat_pp = pembulatan(base / n)
        flat_pm = pembulatan(mrate * base)
        angsuran = pembulatan(flat_pp + flat_pm)

        self.installment = angsuran
        self.margin      = pembulatan(flat_pm * n)
        
        flat_out = base
        for i in range(1, n):
            item = self.getitem(i)
            
            item.angs   = i
            item.pp     = flat_pp
            item.pm     = flat_pm
            item.nilang = angsuran
            item.out    = flat_out
            flat_out    = flat_out - flat_pp
            item.pct_r  = prate
        #~ end of for : i == n - 1
        
        item        = self.getitem(n)
        item.angs   = n
        item.pp     = flat_out
        item.pm     = flat_pm
        item.nilang = flat_out + flat_pm
        item.out    = flat_out
        item.pct_r  = prate
        
        return None

    def manual(self, base, margin):
        self.base   = base
        self.margin   = margin

        n       = self.duration
        angsuran = round((base + margin)/n, 0)
        manual_pp = pembulatan(self.base/n)
        manual_pm = pembulatan(self.margin/n)

        self.installment = angsuran

        manual_out = base
        manual_margin = margin
        for i in range(1, n):
            item = self.getitem(i)

            item.angs   = i
            item.pp     = manual_pp
            item.pm     = manual_pm
            item.nilang = manual_pp + manual_pm
            item.out    = manual_out
            #item.pct_r  = prate
            manual_out    = manual_out - manual_pp
            manual_margin = manual_margin - manual_pm
        #~ end of for : i == n - 1

        item        = self.getitem(n)
        item.angs   = n
        item.pp     = manual_out
        item.pm     = manual_margin
        item.nilang = manual_out + manual_margin
        #item.pct_r  = prate
        item.out    = manual_out

        return None
        
    def manualijarah(self, base, margin,angsuran):
        self.base   = base
        self.margin   = margin

        n       = self.duration
        #angsuran = round((base + margin)/n, 0)
        manual_pp = pembulatan(self.base/n)
        manual_pm = pembulatan(self.margin/n)

        self.installment = angsuran

        manual_out = base
        manual_margin = margin
        for i in range(1, n):
            item = self.getitem(i)

            item.angs   = i
            item.pp     = manual_pp
            item.pm     = manual_pm
            item.nilang = manual_pp + manual_pm
            item.out    = manual_out
            #item.pct_r  = prate
            manual_out    = manual_out - manual_pp
            manual_margin = manual_margin - manual_pm
        #~ end of for : i == n - 1

        item        = self.getitem(n)
        item.angs   = n
        item.pp     = manual_out
        item.pm     = manual_margin
        item.nilang = manual_out + manual_margin
        #item.pct_r  = prate
        item.out    = manual_out

        return None
        
class installment_item:
    angs    = 0
    out     = 0.0
    pp      = 0.0
    pm      = 0.0
    pct_r   = 0.0
    nilang  = 0.0
    tga     = None
    otherData = {}
    
'''
class InstallmentMMQ(installment):
  
  def implMethod(self, param):
    mmqCalc = FinCalculator.MMQCalculator()  
    cicilanBulanan = mmqCalc.CalcCicilanBulanan(n, param.marginEkvRate, param.hargaBeli)  
    sewaBulanan = cicilanBulanan - param.cicilanPengembalianPokok
    totalMargin = (sewaBulanan * n) - param.hargaBeli 
    totalAngsuran = param.hargaBeli + totalMargin
   
    dataSimulasi = mmqCalc.BuildSimulasiData(param.hargaBarang, param.uangMuka, param.marginEkvRate, param.jangkaWaktu, param.cicilanPengembalianPokok)
    
    for i in range(1, n):
      item = self.getitem(i)
      
      item.angs   = i
      item.nilang = angsuran            
      item.pm     = efek_pm
      item.pp     = efek_pp
      item.out    = efek_out
      efek_out    = efek_out - efek_pp
    #~ end of for : i == n - 1
        
        item        = self.getitem(n)
        item.angs   = n
        item.nilang = angsuran
        item.out    = efek_out
        item.pp     = efek_out
        item.pm     = angsuran - efek_out
    
           
    self.base   = base
    self.rate   = param.marginEkvRate
    self.installment = totalAngsuran
    self.margin      = totalMargin
        
    return None
  ##      
  
  def runSimulation(self, param):
    self.implMethod(param)
    
#installmentMMQ
'''  

class kosong:
    pass

def effectiveToFlatRate(effRate, nPeriod): # effRate: annual eff rate in PERCENT, nPeriod = month
  annual_flat_margin_rate = 0.0
  if effRate > 0.0:
    effRateMonthly = effRate / 1200.0
    nYear = nPeriod / 12.0
    multiplier = pow(1 + effRateMonthly, nPeriod)
    flat_margin = (effRateMonthly * nPeriod / (1 - 1 / multiplier)) - 1
    annual_flat_margin_rate = (flat_margin / nYear) * 100.0
    
  return round(annual_flat_margin_rate,4)
  '''
  effRateMonthly = effRate / 1200.0
  nYear = nPeriod / 12.0
  Df = 1 + effRateMonthly
  SumDf = 0.0
  for i in range(nPeriod):
    SumDf += Df
    Df = Df / (1 + effRateMonthly)
  return (nPeriod / SumDf  - 1) / nYear * 100
  '''
#--

def PrpporsiAngsuran(config, tgl_akad, tgl_awal, ang_margin):
  my_date = config.FormatDateTime('yyyymm', tgl_awal)  
  y = int(my_date[:4])
  m = int(my_date[4:])
  last_day = calendar.monthrange(y,m)[1]
  jml = tgl_awal - tgl_akad
  _margin = (jml/last_day)*ang_margin
  
  return round(_margin)

def calculateEIR(initialCashFlow, cashInSeries):
  # requirement:
    # initialCashFlow must be negative
    # cashInSeries must contain of all positive values
    # the sum of all values in cashInSeries must be greater or equal than -initialCashFlow
  MAX_ITERATION = 1000
  EPSILON = 0.1#1E-2
    
  def checkGuess(lowerLimit, upperLimit): # return tuple containing (guess, error, next lower, next upper)
    guess = (lowerLimit + upperLimit) / 2
    divFactor = 1 + guess
    sum = 0.0
    for s in cashInSeries:
      sum += s / divFactor
      divFactor *= (1 + guess)
    #--
    return (
      guess,
      sum + initialCashFlow, 
      guess if sum > -initialCashFlow else lowerLimit, 
      guess if sum < -initialCashFlow else upperLimit
    )
       
  if initialCashFlow > 0:
    raise Exception, 'initialCashFlow parameter must be negative'
  sum = 0.0
  for s in cashInSeries:
    if s < 0.0:
      raise Exception, 'All values in cashInSeries must be positive'
      
  for s in cashInSeries: sum += s
  if sum < -initialCashFlow: raise Exception, 'Sum of cashInSeries must be greater than initialCashFlow'
  
  lowerLimit = 0
  upperLimit = (sum / -initialCashFlow) - 1
  err = s
  itr = 0 
  while True:
    guess, err, lowerLimit, upperLimit = checkGuess(lowerLimit, upperLimit)
    if abs(err) < EPSILON:
      break 
    itr += 1
    if itr >= MAX_ITERATION:
      raise Exception, 'EIR search not convergent'
  return guess 
    
def PMT(r, n, pv):
  r=r/12/100
  return pv * (r * (1 + r)**n) / ((1 + r)**n - 1)

def getTotalMargin(r,n,pv):
	pm=PMT(r,n,pv)*n
	return pm-pv

def baloonMargin(r,n,pv,npv):
  pm=PMT(r,n,pv)*n
  tm=pm-pv
  fRate=((tm/n)/npv)*12*100
  eRate=flatToEffectiveRate(fRate,n)
  return eRate
        
def flatToEffectiveRate(annualFlatRate, nMonth):
  # example: flatToEffectiveRate(8.5, 180) will return 12.98 8.5% equivalent to 12.98% effective
  
  TRY_MAX = 1000
  EPSILON = 1E-8
  
  # f is flat margin ratio (proportion of margin to base price)
  annualFlatRate = annualFlatRate / 100.0
  f = annualFlatRate * nMonth / 12.0
  F = f + 1
  
  def R(y):
    return F * y ** (-nMonth - 1) - (nMonth + F) / y + nMonth
    
  #annualFlatRate = f / (nMonth / 12.0)
  maksAnnualFlatRate = 2 * annualFlatRate
  maksMonthFlatRate = maksAnnualFlatRate / 12.0
  yMaksEst = maksMonthFlatRate + 1
  
  ymin = ((nMonth + 1) * F / (nMonth + F)) ** (1.0 / nMonth)
  y1 = ymin
  y2 = 1 + 2 * (ymin - 1)
  delta = (yMaksEst - y2) / 10
  iTry = 0
  while R(y2) < 0 and iTry < TRY_MAX:
    y1 = y2
    y2 = y2 + delta
    iTry = iTry + 1
  #--
  if iTry >= TRY_MAX:
    raise Exception, "Iteration failed"
  
  iTry = 0
  ymid = (y1 + y2) / 2
  while math.fabs(R(ymid)) > EPSILON and iTry < TRY_MAX:
    if R(ymid) > 0:
      y2 = ymid
    else:
      y1 = ymid
    ymid = (y1 + y2) / 2
    iTry = iTry + 1
  
  if iTry >= TRY_MAX:
    raise Exception, "Iteration failed"
    
  return round((ymid - 1) * 1200.0, 4)
#--
 
def pembulatan(x, n=1, d=2):
    y = round(x*1.0/n, d)
    return y * n

def bil_real(x, n=1, d=0):
    y = round(x*1.0/n, d)
    return y * n

def SafeFloat(data):
    if data == None: return 0
    else: return data

def ConvertDate(config, adate):
    lib = config.ModLibUtils

    if adate != None:
        return lib.EncodeDate(adate[0], adate[1], adate[2])
    else:
        return None

def cv_date(config, a_date):
    return ConvertDate(config, a_date)
    
def ConvertTime(config, atime):
    lib = config.ModLibUtils

    if atime != None:
        return lib.EncodeTime(atime[3], atime[4], atime[5], 0)
    else:
        return None

def cv_time(config, a_time):
    return ConvertTime(config, a_time)
    

class forecast:
    def __init__(self, n):
        items = []
        for i in range(n):
            items.append(forecast_item())
        
        self.items          = items
        self.duration       = n
        self.base           = 0
        self.rate           = 0
        self.installment    = 0
        self.margin         = 0
    
    def getitem(self, i):
        return self.items[i-1]
        
    def sewaBJ(self, nilai, rate, sewa, rb, modal_bank, modal_nasabah):
        self.rate   = rate
        pb = rb/100.0
        
        n  = self.duration
        sn = modal_nasabah
        an = modal_nasabah/nilai
        sb = modal_bank
        ab = modal_bank/nilai
        self.base   = sb
        bb = round((sb/self.base) * sewa * pb, -1)
        bn = sewa - bb

        self.installment = sewa
        self.margin = (sewa * n) - self.base
        
        tot_pokok = 0.0
        tot_sewa  = 0.0
        for i in range(1, n):
            item = self.getitem(i)
            
            item.ke = i
            item.sn = sn
            item.an = an
            item.sb = sb
            item.ab = ab
            item.bn = bn
            item.bb = bb
            item.sw = sewa
            tot_pokok = tot_pokok + item.bn
            tot_sewa  = tot_sewa + item.sw
            
            sb = sb - bn
            sn = sn + bn
            an = sn/nilai
            ab = 1-an
            bb = round((sb/self.base) * sewa * pb, -1)
            bn = sewa - bb
            
        item    = self.getitem(n)
        item.ke = n
        item.sn = sn
        item.an = an
        item.sb = sb
        item.ab = ab
        
        item.sw = (n * sewa) - tot_sewa
        item.bn = self.base - tot_pokok
        item.bb = item.sw - item.bn
                
        return None

class forecast_item:
    ke  = 0
    sn  = 0.0
    an  = 0
    sb  = 0.0
    ab  = 0
    bn  = 0.0
    bb  = 0.0
    sw  = 0.0
    tga = None
    
def kajiBerikut(config, kajiSekarang, kodesiklus):
    if kodesiklus == 'B':
        adM = 1
    elif kodesiklus == 'S':
        adM = 6
    elif kodesiklus == 'T':
        adM = 12
    elif kodesiklus == '3':
        adM = 36
    else:
        adM = 0
        
    tgkjb = utils.AsTDateTime(config, addMonthUnits(kajiSekarang, adM))
    return tgkjb

def addMonthUnits(aDate, mUnits):
    y = mUnits/12
    m = mUnits%12
    tahun = aDate[0] + y
    bulan = aDate[1] + m
    while bulan > 12:
      bulan = bulan - 12
      tahun = tahun + 1

    lastDay = cal.monthrange(tahun, bulan)
    hari = aDate[2]
    if hari > lastDay[1]:
      hari = lastDay[1]

    rDate = (tahun, bulan, hari , aDate[3], aDate[4], aDate[5], aDate[6], aDate[7], aDate[8])

    return rDate
        
def subsMonthUnits(config,aDate,mUnits):
    utils = config.ModLibUtils
    if type(aDate) == types.FloatType:
       cDate = utils.DecodeDate(aDate)
    elif type(aDate) == types.TupleType:
       cDate = aDate
    else:
       raise Exception, 'PERINGATAN. Unknown Date Type'
              
    y = mUnits/12
    m = mUnits%12
    tahun = cDate[0] - y
    bulan = cDate[1] - m
    while bulan <= 0:
      bulan = bulan + 12
      tahun = tahun - 1
    
    lastDay = cal.monthrange(tahun, bulan)
    hari = cDate[2]
    if hari > lastDay[1]:
      hari = lastDay[1]  
    
    if type(aDate) == types.FloatType:
      rDate = utils.EncodeDate(tahun, bulan, hari)
    elif type(aDate) == types.TupleType:  
      rDate = (tahun, bulan, hari , aDate[3], aDate[4], aDate[5], aDate[6], aDate[7], aDate[8])    
    
    return rDate
            
def removeaps(s):
    return s.replace('`', '\'')
    
def QueryDateTimeFormat(config,adate):
    return config.FormatDateTimeForQuery(ConvertDate(config, adate))
    
def QueryDateFormat(config,adate):
    return "'" + config.FormatDateTime("DD-MMM-YY",ConvertDate(config, adate)) +"'"   
    
def ACCRINT(config, acq_date, st_date, lc_date, nc_date, coupon_rate, period, nominal):
  #r=r/12/100
  # ROUND(100*(C6/2)*(H6/I6),4)*10000
  kp_rate = coupon_rate / (12/period*100)
  hold_period = cv_date(config,st_date)-cv_date(config,lc_date)
  hold_period_act = cv_date(config,nc_date)-cv_date(config,lc_date)
  proporsi_akr = hold_period_act/hold_period
  
  yearfac = round(100*kp_rate*proporsi_akr, 4)*10000
  accrued_val = yearfac * (nominal/1000000)
  
  return accrued_val
