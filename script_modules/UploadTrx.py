import dafsys4
import os
import sys,types
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.customidgenAPI as customidgenAPI                                                            
import calendar

def getCols(kode_entri):
  _cols={}
  if kode_entri=='FUP01':
    _cols = {
      'id_scheddetail':'I',                           
      'nomor_rekening':'S',
      'product_code':'S',
      'profit_amount_whole':'F',
      'nisbah':'F',
      'profit_amount':'F',
      'realized_profit_amount_whole':'F',
      'realized_profit_ratio':'F',
      'realized_margin':'F'
    }
  elif kode_entri=='FUP02':
    _cols = {
      'external_customer_no':'S',
      'nomor_rekening':'S',
      'nama_rekening':'S',
      'dropping_date':'TD',
      'sched_date':'TD',
      'product_code':'S',
      'period_count':'I',
      'margin_berjalan':'F',
      'harga_beli':'F',
      'angsuran':'F',
      'principal_amount':'F',
      'profit_amount':'F',
      'mukasah_amount':'F',
      'os_pokok':'F',
      'os_total':'F',
      'initial_margin':'F',
      'initial_mukasah':'F',
      'margin_dibayar':'F',
      'mukasah_dibayar':'F',
      'keterangan':'S',
      'tglvaluta':'TD',
      'finp_id':'S',
      'tglbayar':'TD',
      'fbyr_id':'S'
      #'id_scheddetail':'I'
    }
  
  elif kode_entri=='FUP03':
    _cols = {
      'id_scheddetail':'I',
      'nomor_rekening':'S',
      'product_code':'S',
      'principal_amount':'F',
      'profit_amount_whole':'F',
      'nisbah':'F',
      'profit_amount':'F',
      'realized_profit_amount_whole':'F',
      'realized_profit_ratio':'F',
      'realized_margin':'F',
      'penalty_counter':'I',
      'penalty_tawidh':'F',
      'penalty_tazir':'F',
      'penalty_proses':'S'
    }
  
  elif kode_entri=='FUP04':
    _cols = {
      'nomor_rekening':'S',
      'product_code':'S',
      'kode_cabang':'S',
      'kolektibilitas':'I',
      'outs_pokok':'F',
      'pengurang_ppap':'F',
      'ppap_umum':'F',
      'ppap_khusus':'F',
      'ppap_adj':'F',
      'ppap_koreksi':'F'  
    }
    
  elif kode_entri=='TRU001':
    _cols = {
      'nomor_rekening':'S',
      'jenis_mutasi':'S',
      'nilai_mutasi':'F',
      'kode_cabang':'S',
      'kode_valuta':'S',
      'kode_account':'S',
      'kode_tx_class':'S' 
    }

  return _cols

def ValidateImp(Nilai, _ket, config=None):
  nVal = Nilai
  if _ket in ['S','D']:
    nVal = ValidateDataString(Nilai)
  elif _ket == 'F':
    nVal = float(Nilai or 0.0)
  elif _ket == 'P':
    nVal = float(Nilai or 0.0)*100.0
  elif _ket == 'I':
    nVal = int(Nilai or 0)
  elif _ket == 'DT' and Nilai not in [None,'',0]:
    nVal = config.ModLibUtils.DecodeDate(Nilai)
  elif _ket == 'TT' and Nilai not in [None,'',0]:
    nVal = "to_date('%s/%s/%s', 'mm/dd/yyyy')" % (Nilai[1],Nilai[2],Nilai[0])
  elif _ket == 'TD' and Nilai not in [None,'',0]:
    nVal = "to_date('%s/%s/%s', 'mm/dd/yyyy')" % (Nilai[4:6],Nilai[6:],Nilai[:4])
  else:
    nVal = Nilai
    
  return nVal
  
def ValidateDataString(data, jenis=0):
  dict_none = ['n/a','N/A','NULL','']
  sValidValue = ''
  if data in dict_none:
    data = None

  if data!=None:
    try:
      sValidValue = data
      if type(data) == types.IntType or type(data) == types.FloatType:
        sValidValue = str(int(data))
    except:
      pass

  if jenis == 1:
    sValidValue = re.sub(r'[^\w]', '', sValidValue)
    
  return sValidValue.strip()  
    
def getCustomId(config,idName,idCode):
  customid = customidgenAPI.custom_idgen(config)
  customid.PrepareGetID(idName, idCode)
  new_seq = customid.GetLastID()
  customid.Commit()    
  return new_seq
#--

def create_upl_header(config, params, upl_table):
  helper = pobjecthelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')
  mlu = config.ModLibUtils

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('Inisialisasi proses payment hrd...')

  recData = params.uipData.GetRecord(0)
  recDet = params.uipGrid

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)
  mydate = config.FormatDateTime('yyyymm', float_oToday)
  prefix = recData.kode_entri+mydate
  
  new_seq = getCustomId(config, 'upload_master',prefix)
  upload_id = '%s_%s' % (prefix, str( int(new_seq) ).zfill(4))
  
  _cols = getCols(recData.kode_entri)
  ls_col = _cols.keys()
  ls_col.sort()
  #raise Exception, upload_id
  dParam = {
    'date': mlu.QuotedStr(strDate), 
    'upload_master': config.MapDBTableName('upload_master'),
    'kode_entri': mlu.QuotedStr(recData.kode_entri),
    'user_input': mlu.QuotedStr(recData.user_input),
    'user_authorized': mlu.QuotedStr(config.SecurityContext.UserID),
    'contracttype_code': mlu.QuotedStr(recData.contracttype_code),
    'upl_table': config.MapDBTableName(upl_table),
    'upload_id': mlu.QuotedStr(upload_id),
    'c_name': ','.join(ls_col)
  }
  
  dbutil.runSQL(config, '''  
    INSERT INTO %(upload_master)s (
    	upload_id,
    	upload_date,
    	kode_entri,
    	user_input,
    	user_authorized,
    	contracttype_code
    ) values
    (
      %(upload_id)s,
      %(date)s,
      %(kode_entri)s,
      %(user_input)s,
      %(user_authorized)s,
      %(contracttype_code)s
    )     
    ''' % dParam
  )
  
  #insert detil table upload
  cl_value = '%('+')s, %('.join(ls_col)+')s'
  # insert upl_grossprofitrealization 
  for i in range(0, recDet.RecordCount):
    recDetil = recDet.GetRecord(i)
    dictParam={}
    for _rc in _cols:
      Nilai=recDetil.GetFieldByName(_rc)
      _ket = _cols[_rc]
      nVal = ValidateImp (Nilai, _ket, config)
      dictParam[_rc] = nVal
      if _cols[_rc]=='S':
        dictParam[_rc]= mlu.QuotedStr(nVal)
      elif _cols[_rc]=='F':
        dictParam[_rc]= 'round(%s, 2)' % nVal
                                                           
    dParam['c_value'] = cl_value % dictParam
    sSQL ='''  
      INSERT INTO %(upl_table)s (
      	upload_id,
      	is_process,
      	%(c_name)s      
      ) values
      (
        %(upload_id)s,'T',
        %(c_value)s
      )       
      ''' % dParam
    #app.ConWriteln(sSQL)
    #app.ConRead('')  
    dbutil.runSQL(config, sSQL)
  #---
  return upload_id
#---
  
def create_header_tu(config, params):
  helper = pobjecthelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')
  mlu = config.ModLibUtils

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('Inisialisasi proses transaksi umum...')

  recData = params.uipTrx.GetRecord(0)
  recDet = params.listMember

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)
  mydate = config.FormatDateTime('yyyymm', float_oToday)
  prefix = 'TRU.'+mydate
  
  new_seq = getCustomId(config, 'upload_master',prefix)
  upload_id = '%s.%s' % (prefix, str( int(new_seq) ).zfill(4))
  
  _cols = getCols('TRU001')
  ls_col = _cols.keys()
  ls_col.sort()
  #raise Exception, upload_id
  dsInfo = params.__inputinfo
  recInfo = dsInfo.GetRecord(0)
  Otr = helper.GetObject('enterprise.OtorEntri', recInfo.id_otorisasi)

  dParam = {
    'date': mlu.QuotedStr(strDate), 
    'transaksiumum': config.MapDBTableName('core.transaksiumum'),
    'seq_transaksi': config.MapDBTableName('core.seq_transaksi'),
    'seq_detiltransaksi': config.MapDBTableName('core.seq_detiltransaksi'),
    'seq_detiltransgroup': config.MapDBTableName('core.seq_detiltransgroup'),
    'nomor_ref': mlu.QuotedStr(recData.nomor_ref),
    'keterangan': mlu.QuotedStr(recData.keterangan),
    'kode_entri': mlu.QuotedStr(Otr.kode_entri),
    'kode_cabang': mlu.QuotedStr(Otr.kode_cabang),
    'user_input': mlu.QuotedStr(Otr.user_input),
    'user_authorized': mlu.QuotedStr(config.SecurityContext.UserID),
    'tanggal_transaksi': mlu.QuotedStr(config.FormatDateTime('dd-mmm-yyyy', recData.tanggal_transaksi)),
    'terminal_input': mlu.QuotedStr(Otr.terminal_input),
    'terminal_otorisasi': mlu.QuotedStr(config.SecurityContext.InitIP),
    'upl_table': config.MapDBTableName('core.detiltransaksiumum'),
    'upload_id': mlu.QuotedStr(upload_id),
    'c_name': ','.join(ls_col)
  }
  
  dbutil.runSQL(config, '''  
    INSERT INTO %(transaksiumum)s (
    	id_transaksi, ref_id_transaksi
      , nomor_seri
      , locked, status_transaksi_umum
      , tanggal_input
      , tanggal_transaksi
      , tanggal_otorisasi
      , nomor_referensi
      , kode_cabang_transaksi
      , user_input, user_override
      , kode_transaksi
      , terminal_input
      , terminal_otorisasi
      , keterangan
    ) values
    (
      %(seq_transaksi)s.nextval
      , %(seq_detiltransgroup)s.nextval
      , %(upload_id)s
      , 'T', 'T'
      , %(date)s
      , %(tanggal_transaksi)s
      , %(tanggal_transaksi)s
      , %(nomor_ref)s
      , %(kode_cabang)s
      , %(user_input)s, %(user_authorized)s
      , %(kode_entri)s
      , %(terminal_input)s
      , %(terminal_otorisasi)s
      , %(keterangan)s
    )     
    ''' % dParam
  )
  
  #insert detil table upload
  cl_value = '%('+')s, %('.join(ls_col)+')s'
  # insert upl_grossprofitrealization 
  for i in range(0, recDet.RecordCount):
    recDetil = recDet.GetRecord(i)
    dictParam={}
    for _rc in _cols:
      Nilai=recDetil.GetFieldByName(_rc)
      _ket = _cols[_rc]
      nVal = ValidateImp (Nilai, _ket, config)
      dictParam[_rc] = nVal
      if _cols[_rc]=='S':
        dictParam[_rc]= mlu.QuotedStr(nVal)
      elif _cols[_rc]=='F':
        dictParam[_rc]= 'round(%s, 2)' % nVal
                                                           
    dParam['c_value'] = cl_value % dictParam
    sSQL ='''  
      INSERT INTO %(upl_table)s (
        nomor_referensi
        , id_detil_transaksi_umum
        , kode_kurs
        , nilai_kurs_manual
        , %(c_name)s      
      ) values
      (
        %(upload_id)s
        , %(seq_detiltransaksi)s.nextval
        , 'BOOKING'
        , 1.0
        , %(c_value)s
      )       
      ''' % dParam
    dbutil.runSQL(config, sSQL)
  #---
  return upload_id
#---
  
def upl_trx_treasury(config, params):
  dPar = {
    'upl_table':'detiltransaksiumum', 'txn_scripts': 'scripts#upl_trx_treasury'
  }
  txn_process = create_transaksi_upload(config, params, dPar)  
#--- end
  
def upl_ppap_koreksi(config, params):
  dPar = {
    'upl_table':'upl_ppap_koreksi', 'txn_scripts': 'scripts#upl_koreksi_ppap'
  }
  txn_process = create_transaksi_upload(config, params, dPar)  
#--- end
  
def create_transaksi_upload(config, params, dPar):
  mlu = config.ModLibUtils
  #insert table upload
  upl_table = dPar['upl_table'] 
  if upl_table=='detiltransaksiumum': 
    upload_id = create_header_tu(config, params)
  else: 
    upload_id = create_upl_header(config, params, upl_table)

  app = config.AppObject
  app.ConWriteln('Inisialisasi do payment...')

#def coment(config, 'upload_id'):
  is_err = 0          
  if is_err == 0:
    txn_process = modman.getModule(config, dPar['txn_scripts'])
    txn_process.main(config, params, upload_id)
    app.ConWriteln('upload berhasil..')
  else:
    app.ConWriteln('Transaksi gagal..!!')
    #app.ConRead('press any key..')
    raise Exception, 'Transaksi gagal..!!'
    
  app.ConWriteln('selesai...')
  #app.ConRead('press any key..')
  #raise Exception, 'okok'
  app.ConCreate('out')

  
#--- end

  