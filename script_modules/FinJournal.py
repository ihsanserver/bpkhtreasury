import sys
import os
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.intrdbutil as dbutil


def createFinancingJournal(config, sDate, idTransaksiOrFilter = None, srcTable = None):
  # Fungsi ini bisa digunakan dengan tiga cara:
  # 1. Hanya parameter config dan sDate: akan dipilih seluruh data dari financingsys.pendingtransactionjournal yang flag_sent IS NULL
  # 2. Dengan parameter config, sDate dan idTransaksiOrFilter (integer): akan menjurnal id_transaksi tertentu yang ada di tabel financingsys.pendingtransactionjournal
  # 3. Dengan parameter config, sDate, idTransaksiOrFilter (string), srcTable (string): akan menjurnal dari srcTable dengan kriteria idTransaksiOrFilter 
  
  mlu = config.ModLibUtils

  default_valuta = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultCurrency')
  acc_code_selisih = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultAccountSelisih')
  head_branch = '000'
  acc_code_rak = config.SysVarIntf.GetStringSysVar('OPTION', 'DefaultAccountRAKOtomatis')
  current_user = config.SecurityContext.InitUser 
  dParams = {
    'detiltransaksi': config.MapDBTableName('core.detiltransaksi'),
    'transaksi': config.MapDBTableName('core.transaksi'),
    'account': config.MapDBTableName('core.account'), 
    'accountinstance': config.MapDBTableName('core.accountinstance'),
    'accountingday': config.MapDBTableName('core.accountingday'),
    'journal': config.MapDBTableName('core.journal'),
    'journalitem': config.MapDBTableName('core.journalitem'),
    'finaccount': config.MapDBTableName('financingsys.finaccount'),
    'glinterface': config.MapDBTableName('financingsys.glinterface'),
    'seq_finjournalno': config.MapDBTableName('financingsys.seq_finjournalno'),
    'seq_finjournalitemno': config.MapDBTableName('financingsys.seq_finjournalitemno'),
    'date': mlu.QuotedStr(sDate),
    'default_valuta': mlu.QuotedStr(default_valuta),
    'head_branch': mlu.QuotedStr(head_branch),
    'acc_code_rak': mlu.QuotedStr(acc_code_rak),
    'acc_code_selisih': mlu.QuotedStr(acc_code_selisih),
    'current_user': mlu.QuotedStr(current_user)    
  }
  
  if idTransaksiOrFilter == None:
    dParams.update({'filter': 'ptj.flag_sent IS NULL'})
    dParams.update({'srctable': config.MapDBTableName('financingsys.pendingtransactionjournal')})
    dParams.update({'resulttable': config.MapDBTableName('financingsys.completedtransactionjournal')})
  else:
    if srcTable == None:
      dParams.update({'filter': 'ptj.id_transaksi = %d' % idTransaksiOrFilter})
      dParams.update({'srctable': config.MapDBTableName('financingsys.pendingtransactionjournal')})
      dParams.update({'resulttable': config.MapDBTableName('financingsys.completedtransactionjournal')})
    else:
      dParams.update({'filter': idTransaksiOrFilter})
      dParams.update({'srctable': config.MapDBTableName(srcTable)})
      

  # SET ACCOUNT DATA DI FINANCING
  sSQL = '''
    MERGE INTO %(detiltransaksi)s target
    USING (
      SELECT                                        
        dt.id_detil_transaksi,
        case
          when gli.kode_account is null then %(acc_code_selisih)s
          else gli.kode_account
        end as kode_account
      FROM
        %(detiltransaksi)s dt,
        %(srctable)s ptj,
        %(finaccount)s fa,
        %(glinterface)s gli
      WHERE
        ptj.id_transaksi = dt.id_transaksi
        AND %(filter)s
        AND dt.nomor_rekening = fa.nomor_rekening
        AND (fa.product_code = gli.kode_produk AND gli.kode_interface = dt.kode_tx_class 
        OR fa.product_code IS NULL AND fa.contracttype_code = 'W' AND gli.kode_interface = dt.kode_tx_class
        )
    ) updrecs
    ON (target.id_detil_Transaksi = updrecs.id_detil_transaksi)
    WHEN MATCHED THEN
      UPDATE SET target.kode_account = updrecs.kode_account 
      WHERE target.kode_account IS NULL
  ''' % dParams
  dbutil.runSQL(config, sSQL)
  
  # FIX UP DATA KURS DAN EKIVALEN
  sSQL = '''
    UPDATE %(detiltransaksi)s dt
    SET 
      nilai_kurs_manual = 1,
      nilai_ekuivalen = nilai_mutasi
    WHERE dt.kode_valuta = %(default_valuta)s AND 
    EXISTS(
      select 1 from
        %(srctable)s ptj
      where ptj.id_transaksi = dt.id_transaksi
      AND %(filter)s
    )
  ''' % dParams
  dbutil.runSQL(config, sSQL)
  
  sSQL = '''
    UPDATE %(detiltransaksi)s dt
    SET 
      nilai_ekuivalen = nilai_mutasi * nilai_kurs_manual
    WHERE dt.kode_valuta <> %(default_valuta)s AND 
    EXISTS(
      select 1 from
        %(srctable)s ptj
      where ptj.id_transaksi = dt.id_transaksi
      AND %(filter)s
    )
  ''' % dParams
  dbutil.runSQL(config, sSQL)
  
  # CHECK ACCOUNTINGDAY AVAILABILITY
  sSQL = '''
    SELECT datevalue, periode_status FROM %(accountingday)s accd
    WHERE datevalue = TO_DATE(%(date)s)
  ''' % dParams
  q = config.CreateSQL(sSQL).RawResult
  if q.Eof:
    raise Exception, "Accounting day %s not found" % sDate
  
  
  # CEK VALIDITAS DATA
  sSQL = '''
    SELECT dt.id_detil_transaksi, 
      dt.id_transaksi,
      dt.nomor_rekening,
      dt.jenis_mutasi,
      dt.nilai_mutasi,
      dt.kode_tx_class,
      t.nomor_seri
    FROM %(detiltransaksi)s dt, %(transaksi)s t, %(srctable)s ptj
    WHERE 
    dt.id_transaksi = t.id_transaksi
    AND ptj.id_transaksi = t.id_transaksi
    AND %(filter)s
    AND (
      dt.kode_account IS NULL
      OR
      dt.kode_cabang IS NULL
      OR
      dt.kode_valuta IS NULL
      OR
      (
        dt.kode_valuta = %(default_valuta)s
        AND
        (dt.nilai_kurs_manual <= 0 OR dt.nilai_kurs_manual IS NULL)
      )
    )
  ''' % dParams
  # raise Exception, sSQL
  q = config.CreateSQL(sSQL).RawResult
  if not q.Eof:
    raise Exception, "Ada data transaksi tidak lengkap. nomor_rekening=%s id_detil_transaksi=%d" % (q.nomor_rekening, q.id_detil_transaksi)
  
  # CEK ACCOUNTINSTANCE
  sSQL = '''
    SELECT dt.id_detil_transaksi, 
      dt.id_transaksi,
      dt.nomor_rekening,
      dt.kode_account,
      dt.kode_cabang,
      dt.kode_valuta
    FROM %(detiltransaksi)s dt, %(transaksi)s t, %(srctable)s ptj
    WHERE 
    dt.id_transaksi = t.id_transaksi
    AND ptj.id_transaksi = t.id_transaksi
    AND %(filter)s
    AND NOT EXISTS (
      select 1 FROM %(accountinstance)s ai
      WHERE ai.account_code = dt.kode_account
      AND ai.branch_code = dt.kode_cabang
      AND ai.currency_code = dt.kode_valuta
    )
  ''' % dParams
  q = config.CreateSQL(sSQL).RawResult

  if not q.Eof:
    raise Exception, "Account instance untuk account=%s valuta=%s cabang=%s tidak ada" % (q.kode_account, q.kode_valuta, q.kode_cabang)
  
  # CEK BALANCING
  sSQL = '''
    SELECT id_transaksi, kode_valuta , amount_debit, amount_credit FROM (
      SELECT  
        dt.id_transaksi,
        dt.kode_valuta,
        SUM(
        case
          when dt.jenis_mutasi = 'D' then dt.nilai_mutasi
          else 0.0
        end) as amount_debit,
        SUM(
        case
          when dt.jenis_mutasi = 'C' then dt.nilai_mutasi
          else 0.0
        end) as amount_credit
      FROM %(detiltransaksi)s dt, %(transaksi)s t, %(srctable)s ptj,
        %(account)s acc
      WHERE 
      dt.id_transaksi = t.id_transaksi
      AND ptj.id_transaksi = t.id_transaksi
      AND %(filter)s
      AND dt.kode_account = acc.account_code
      AND acc.account_type <> 'M'
      AND (t.is_sudah_dijurnal = 'F' OR t.is_sudah_dijurnal IS NULL)
      GROUP BY dt.id_transaksi, dt.kode_valuta
    )
    WHERE abs(amount_debit - amount_credit) > 0.1  
  ''' % dParams
  q = config.CreateSQL(sSQL).RawResult   
  if not q.Eof:      
    ### TODO : sementara utk development dikomentari
    import copy    
    dbginfo = {'debit' : copy.copy(q.amount_debit), 'kredit': copy.copy(q.amount_credit), 'variance': (q.amount_debit-q.amount_credit)}
    raise Exception, "Transaksi %d tidak balanced di valuta %s ,dbginfo = %s" % (q.id_transaksi, q.kode_valuta, str(dbginfo))
    pass
  
  # CREATE JOURNAL NUMBERS
  sSQL = '''
    MERGE INTO %(srctable)s target
    USING (
      SELECT
        ptj.id_transaksi,
        'FIN.' || accd.fl_accountingyear || '.' || 
        lpad(to_char(accd.fl_accountingperiode), 2, '0') || '.' as prefix_journal_no
      FROM
      %(srctable)s ptj,
      %(transaksi)s t,
      %(accountingday)s accd
      WHERE
        accd.datevalue = TO_DATE(%(date)s)
        AND t.id_transaksi = ptj.id_transaksi
        AND (t.is_sudah_dijurnal = 'F' or t.is_sudah_dijurnal IS NULL)
        AND %(filter)s
      ) upd
    ON (upd.id_transaksi = target.id_transaksi)
    WHEN MATCHED THEN UPDATE SET journal_no = upd.prefix_journal_no || to_char(%(seq_finjournalno)s.nextval) 
  ''' % dParams
  dbutil.runSQL(config, sSQL)
  
  # INSERT JOURNAL
  sSQL = '''
    INSERT INTO %(journal)s (
      journal_no,
      journal_date_posting,
      description,
      userid_create,
      userid_posting,
      is_posted,
      journal_type,
      journal_date,
      lastitemno,
      is_partlychecked,
      branch_code,
      transaction_date,
      journal_state,
      serial_no,
      periode_Code,
      fl_accountingperiode,
      fl_accountingyear
    )
    SELECT
      ptj.journal_no,
      TO_DATE(%(date)s),
      t.keterangan,
      t.user_input as userid_create,
      %(current_user)s,
      'T',
      'FIN',
      TO_DATE(%(date)s),
      0,
      'T',
      t.kode_cabang_transaksi,
      t.tanggal_transaksi,
      'C',
      '',
      accd.periode_code,
      accd.fl_accountingperiode,
      accd.fl_accountingyear
    FROM
      %(srctable)s ptj,
      %(accountingday)s accd,
      %(transaksi)s t
    WHERE
      %(filter)s
      AND ptj.journal_no IS NOT NULL
      AND ptj.id_transaksi = t.id_transaksi
      AND accd.datevalue = TO_DATE(%(date)s)
      AND (t.is_sudah_dijurnal = 'F' or t.is_sudah_dijurnal IS NULL)  
  ''' % dParams
  dbutil.runSQL(config, sSQL)
  
  # INSERT JOURNAL ITEM
  sSQL = '''
    INSERT INTO %(journalitem)s (
      fl_journal,
      journalitem_no,
      fl_account,
      amount_debit,
      amount_credit,
      description,
      branch_code,
      valuta_code,
      fl_project_no,
      userid_create,
      userid_posting,
      userid_check,
      DatePosting,
      DateCreate,
      JournalItemType,
      Kode_Kurs,
      Nilai_Kurs,
      subaccountcode,
      AccountInstance_ID
      )
    SELECT
      ptj.journal_no,
      %(seq_finjournalitemno)s.nextval,
      dt.kode_account,
      case when
        dt.jenis_mutasi = 'D' then dt.nilai_mutasi
        else 0.0
      end as amount_debit,
      case when
        dt.jenis_mutasi = 'C' then dt.nilai_mutasi
        else 0.0
      end as amount_credit,
      dt.keterangan,
      dt.kode_cabang,
      dt.kode_valuta,
      NULL,
      t.user_input,
      t.user_input,
      t.user_input,
      t.tanggal_input,
      t.tanggal_input,
      'N',
      dt.kode_kurs,
      dt.nilai_kurs_manual,
      dt.nomor_rekening,
      ai.accountinstance_id
    FROM
      %(srctable)s ptj,
      %(detiltransaksi)s dt,
      %(transaksi)s t,
      %(accountinstance)s ai
    WHERE
      ptj.id_transaksi = t.id_transaksi
      AND %(filter)s
      AND dt.id_transaksi = t.id_transaksi
      AND ai.account_code = dt.kode_account
      AND ai.branch_code = dt.kode_cabang
      AND ai.currency_code = dt.kode_valuta
      AND (t.is_sudah_dijurnal = 'F' or t.is_sudah_dijurnal IS NULL)
  ''' % dParams
  dbutil.runSQL(config, sSQL)
  
  # BALANCING RAK Otomatis, sementara hanya berlaku untuk default currency
  sSQL = '''
    INSERT INTO %(journalitem)s (
    fl_journal,
    journalitem_no,
    fl_account,
    amount_debit,
    amount_credit,
    description,
    branch_code,
    valuta_code,
    fl_project_no,
    userid_create,
    userid_posting,
    userid_check,
    DatePosting,
    DateCreate,
    JournalItemType,
    Kode_Kurs,
    Nilai_Kurs,
    AccountInstance_ID
    )
    SELECT 
      ubitems.fl_journal,
      %(seq_finjournalitemno)s.nextval,
      %(acc_code_rak)s, 
      case when ubitems.amount_credit > ubitems.amount_debit then ubitems.amount_credit - ubitems.amount_debit else 0.0 end  , -- reverse credit and debit amount
      case when ubitems.amount_debit > ubitems.amount_credit then ubitems.amount_debit - ubitems.amount_credit else 0.0 end,
      'TRANSAKSI RAK OTOMATIS',
      ubitems.branch_code,
      ubitems.valuta_code,
      NULL,
      t.user_input,
      t.user_input,
      t.user_input,
      t.tanggal_input,
      t.tanggal_input,
      'N',
      NULL,
      1,
      ai.accountinstance_id 
    FROM 
      (
        SELECT
          ptj.id_transaksi,  
          ji.fl_journal,
          ji.valuta_code,
          ji.branch_code,
          SUM(amount_debit) as amount_debit,
          SUM(amount_credit) as amount_credit
        FROM 
          %(journalitem)s ji,
          %(account)s acc, 
          %(srctable)s ptj
        WHERE 
        ptj.journal_no = ji.fl_journal
        AND acc.account_code = ji.fl_account
        AND acc.account_type <> 'M'
        AND %(filter)s
        GROUP BY ptj.id_transaksi, 
        ji.fl_journal, ji.valuta_code, ji.branch_code
      ) ubitems,
      %(transaksi)s t,
      %(accountinstance)s ai
    WHERE 
      abs(ubitems.amount_debit - ubitems.amount_credit) > 0.001
      AND ubitems.id_transaksi = t.id_transaksi
      AND ai.branch_code = ubitems.branch_code
      AND ai.account_code = %(acc_code_rak)s
      AND ai.currency_code = ubitems.valuta_code
  ''' % dParams
  dbutil.runSQL(config, sSQL)
  
  # UPDATE transactions status and journal link
  sSQL = '''
    MERGE INTO %(transaksi)s target
    USING (
      SELECT * FROM %(srctable)s ptj
      WHERE
      %(filter)s
    ) upd
    ON (
      upd.id_transaksi = target.id_transaksi
      AND (target.is_sudah_dijurnal = 'F' or target.is_sudah_dijurnal IS NULL) 
    )
    WHEN MATCHED THEN
    UPDATE SET target.journal_no = upd.journal_no,
    target.is_sudah_dijurnal = 'T'
  ''' % dParams
  dbutil.runSQL(config, sSQL)
  
  if dParams.has_key('resulttable'):
    # MOVE TO completedtransactionjournal table  
    sSQL = '''
      INSERT INTO %(resulttable)s
      (
        id_transaksi,
        journal_no
      )
      SELECT id_transaksi, journal_no FROM
      %(srctable)s ptj
      WHERE
      ptj.journal_no IS NOT NULL AND 
      %(filter)s
    ''' % dParams
    dbutil.runSQL(config, sSQL)
    sSQL = '''
      DELETE FROM %(srctable)s ptj
      WHERE 
      ptj.journal_no IS NOT NULL AND
      %(filter)s
    ''' % dParams
    dbutil.runSQL(config, sSQL)
  #--
#--  
  