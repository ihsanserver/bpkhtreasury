import com.ihsan.foundation.pobjecthelper as phelper
#import com.ihsan.lib.trace as trace
#import rpdb2

# globals
warn = 0
warnMsg = ''

def BeforeLogin(config, appid, userid, password):
  global warn, warnMsg
  #import rpdb2; rpdb2.start_embedded_debugger("000")
      
  helper = phelper.PObjectHelper(config)
  app = helper.CreateObject('Enterprise.Global')
  warn, warnMsg = app.Login(userid, password)
      
  return 1

# new event handler for login extension
def BeforeLoginEx(config, appid, userid, password, packet):
  global warn, warnMsg
      
  helper = phelper.PObjectHelper(config)
  app = helper.CreateObject('Enterprise.Global')
  warn0, warnMsg0 = app.Login(userid, password, 'F')  
  warn, warnMsg = app.LoginExt(userid, packet)
  if warn0:
    if not warn: 
      warn = 1
      warnMsg = warnMsg0
    else:
      warnMsg = warnMsg0 + "\r\n" + warnMsg
    #--
  #--
    
  return 1

def BeforeLogout(config):
  helper = phelper.PObjectHelper(config)
  app = helper.CreateObject('Enterprise.Global')
  app.BeforeLogout()
    
def OnGetUserInfo(config, userid, userinfo):
  helper = phelper.PObjectHelper(config)    
  app = helper.CreateObject('Enterprise.Global')
  app.OnGetUserInfo(userid, userinfo)
        
def AfterSuccessfulLogin(config, reclogin, password):
  global warn, warnMsg
  
  helper = phelper.PObjectHelper(config)
  app = helper.CreateObject('Enterprise.Global')
  app.AfterSuccessfulLogin(reclogin)

  if warn:
    reclogin.WarnUserLevel = warn
    reclogin.WarnUserMessage = warnMsg
  #--                            

def AfterFailedLogin(config, appid, userid, password): 
  pass
  
def BeforeChangePassword(config, new_password, confirm_password): pass

def AfterChangePassword(config, new_password):
  helper = phelper.PObjectHelper(config)
  app = helper.CreateObject('Enterprise.Global')
  app.AfterChangePassword(new_password)
    
def OnAccessSession(config, session_id, session_file_name):
  helper = phelper.PObjectHelper(config)
  app = helper.CreateObject('Enterprise.Global')
  app.OnAccessSession(session_id, session_file_name)
  
def BeforeRestoreSession(config, session_name):
  app = config.AppObject
  if session_name == 'remote_core' and not app.lookuprsession('remote_core'):
    #rpdb2.start_embedded_debugger('000')
    server = config.GetGlobalSetting('RLOGIN_CORE_SERVER')
    app_id = config.GetGlobalSetting('RLOGIN_CORE_APP')    
    uid = config.GetGlobalSetting('RLOGIN_CORE_USERID')
    passwd = config.GetGlobalSetting('RLOGIN_CORE_PASSWORD')
    app.rlogin(server, app_id, uid, passwd, 'remote_core')  