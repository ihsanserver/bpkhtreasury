# request
save     = 0
preview  = 1
printout = 2

class CollMenus:
  def __init__(self):
    # set default request
    self.default_request = printout
        
  def GetFSpecificString(self, app, grid):
    fSpecific = None
    kode_entri = None
    asset_type = grid.GetFieldValue('LFinCollateralAsset.asset_type')
    group_asset_type = grid.GetFieldValue('LFinCollateralAsset.group_asset_type')
    
    return self.GetFormName(app, asset_type, group_asset_type)

  def GetFormName(self, app, asset_type, group_asset_type):
    fSpecific = None
    kode_entri = None
    #asset_type = grid.GetFieldValue('LFinCollateralAsset.asset_type')
    #group_asset_type = grid.GetFieldValue('LFinCollateralAsset.group_asset_type')

    if asset_type == "P":
      fSpecific = 'fFinColProperty,FinColProperty'
      kode_entri = 'FIC07'
    elif asset_type == "K":
      fSpecific = 'fFinColKendaraan,FinColKendaraan'
      kode_entri = 'FIC05'
    elif asset_type == "B":
      fSpecific = 'fFinColBankGaransi,FinColBankGaransi'
      kode_entri = 'FIC02'
    elif asset_type == "D":
      fSpecific = 'fFinColDeposito,FinColDeposito'
      kode_entri = 'FIC03'
    elif asset_type in ("G"):
      fSpecific = 'fFinColIntDeposito,FinColIntDeposit'
      kode_entri = 'FIC04'
    elif asset_type == "A":
      fSpecific = 'fFinColAlatBerat,FinColAlatBerat'
      kode_entri = 'FIC01'
    elif asset_type == "L":
      fSpecific = 'fFinColKapal,FinColKapal'
      kode_entri = 'FIC06'
    elif asset_type == "T":
      fSpecific = 'fFinColTagihan,FinColTagihan'
      kode_entri = 'FIC08'
    elif asset_type == "E":
      fSpecific = 'fFinColMetal,FinColMetal'
      kode_entri = 'FIC12'
    elif asset_type == "J":
      fSpecific = 'fFinColGuarantee,FinColGuarantee'
      kode_entri = 'FIC14'
    elif asset_type == "N":
      fSpecific = 'fFinColNotes,FinColNotes'
      kode_entri = 'FIC15'
    elif asset_type == "S":
      fSpecific = 'fFinColInventory,FinColInventory'
      kode_entri = 'FIC13'
    elif asset_type == "F":
      fSpecific = 'fFinColSecurities,FinColSecurities'
      kode_entri = 'FIC16'
    elif asset_type == "H":
      fSpecific = 'fFinColDecree,FinColDecree'
      kode_entri = 'FIC17'
    elif asset_type == "Z":
      fSpecific = 'fFinColOther,FinColOther'
      kode_entri = 'FIC18'
    elif asset_type == "C":
      if group_asset_type == 'P':
        fSpecific = 'fFinColProperty_kol,FinColGroup'
        kode_entri = 'FIC09'
      elif group_asset_type == 'K':
        fSpecific = 'fFinColKendaraan_kol,FinColGroup'
        kode_entri = 'FIC10'
      elif group_asset_type == 'A':
        fSpecific = 'fFinColAlatBerat_kol,FinColGroup'
        kode_entri = 'FIC11'
    else:
      app.ShowMessage('\n\nPERHATIAN!\nTipe aset tidak terdaftar...')
      return
      
    return fSpecific, kode_entri
