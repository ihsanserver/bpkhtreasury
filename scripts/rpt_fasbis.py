import os
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import com.ihsan.util.modman as modman
import openpyxl
from openpyxl.cell import get_column_letter
from openpyxl.styles import Border
from openpyxl.styles import Font
from openpyxl import Workbook
import com.ihsan.fileutils as fileutils
from copy import deepcopy
from com.ihsan.lib import queryreport
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.attrutil as atutil
import calendar
import datetime
from datetime import datetime

modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

GRID_ACCOUNT = 'gridsettings.listaccount_cols'

def dbg(msg):
  #app.ConWriteln(msg)
  #app.ConRead(msg2)
  message.send_udp(msg + "\n", 'localhost', 9122)

def getSQLClient(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils            
  #fr = params.FirstRecord
  param = params.FirstRecord
  tglIndo = modman.getModule(config,'TglIndo')

  #raise Exception, param.all_cabang
  r_tgl = ''
  lcond = []
  lcond.append( ''' a.jenis_treasuryaccount in ('%s') ''' % param.jenis_treasuryaccount)
  lcond.append( ''' ('1'='%(all_cabang)s' or b.kode_cabang = '%(kode_cabang)s') ''' % {'kode_cabang':param.kode_cabang,'all_cabang':param.all_cabang} )
  lcond.append( ''' ('0'='%(sr)s' or b.status_rekening = %(sr)s ) ''' % {'sr':param.status_rekening}) 

  if param.kode_produk <> "ALL":
    lcond.append( ''' a.kode_produk='%s' ''' % param.kode_produk)
      
  if param.tgl_awal > 0:
    lcond.append( '''to_date('%s', 'dd/mm/yyyy')<=a.tgl_buka ''' % (tglIndo.tgl_indo(config,param.tgl_awal,10)) )
    r_tgl += 'TGL DEAL %s ' % (tglIndo.tgl_indo(config,param.tgl_awal,2))
  if param.tgl_akhir > 0:
    lcond.append( '''to_date('%s', 'dd/mm/yyyy')>=a.tgl_buka ''' % (tglIndo.tgl_indo(config,param.tgl_akhir,10)) )
    r_tgl += '- %s' % (tglIndo.tgl_indo(config,param.tgl_akhir,2))

  if len(param.search_word) > 0 :
    if param.search_field == 'N' :
      lcond.append('''upper(b.nama_rekening) LIKE upper('%%%s%%') ''' % param.search_word)
    elif param.search_field == 'R' :
      lcond.append( '''upper(a.nomor_rekening) LIKE upper('%%%s%%') ''' % param.search_word )
    elif param.search_field == 'C' :
      lcond.append( '''upper(d.nomor_nasabah) LIKE upper('%%%s%%') ''' % param.search_word )
      
  strOqlCond = ''      
  if len(lcond) > 0 :   
    strOqlCond = " AND ".join(lcond)
    strOqlCond = strOqlCond
 
  _dict = {
    'treasuryaccount':config.MapDBTableName("treasuryaccount"),
    'rekeningtransaksi':config.MapDBTableName("core.rekeningtransaksi"),
    'treafasbis':config.MapDBTableName("treafasbis"),
    'treaproduk':config.MapDBTableName("treaproduk"),
    'treacounterpart':config.MapDBTableName("treacounterpart"), 
    'listperanuser':config.MapDBTableName("enterprise.listperanuser"), 
    'listcabangdiizinkan':config.MapDBTableName("enterprise.listcabangdiizinkan"),
    'referencedata':config.MapDBTableName("enterprise.referencedata"), 
    'id_user': mlu.QuotedStr(config.SecurityContext.InitUser),
    'strOqlCond':strOqlCond
  } 
  
  sSQL = '''
    SELECT b.tipe_akses_cabang FROM %(userapp)s b WHERE b.id_user='%(id_user)s' 
  ''' % {
    'userapp': config.MapDBTableName("enterprise.userapp"),
    'id_user':config.SecurityContext.InitUser
  }
  rSQL = config.CreateSQL(sSQL).RawResult

  _dict['LCDJoinType'] = 'LEFT JOIN' if rSQL.tipe_akses_cabang=='S' else 'INNER JOIN'
  
  sSQL = '''
    SELECT DISTINCT 
      a.Nomor_Rekening,
      b.NAMA_REKENING,
      -(b.saldo) as saldo, 
      case when b.status_rekening = 1 then 'AKTIF'
      when b.status_rekening in (3,99) then 'TUTUP' end as status_rekening,
      a.kode_produk, 
      a.tgl_buka,
      a.tgl_jatuh_tempo,
      extract(day from a.tgl_jatuh_tempo - a.tgl_buka) as jml_hari,
			a.nominal_deal,
			a.ujroh,
			a.eqv_rate_real,
			a.nisbah,
			a.nomor_ref_deal,
			a.jenis_treasuryaccount,
			b.kode_valuta,
			pr.sumber_dana,
			ji.reference_code jenis_instrumen,
			nvl(a.ppap_umum,0) ppap_umum,
      nvl(a.ppap_khusus,0) ppap_khusus,
			b.saldo+a.ujroh nominal_gross,
			cp.nama_counterpart,
			decode(b.status_rekening,1,'AKTIF','TUTUP') status
    FROM %(treasuryaccount)s a
    	INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
    	INNER JOIN %(treafasbis)s c ON a.nomor_rekening=c.nomor_rekening
			INNER JOIN %(treaproduk)s pr ON a.kode_produk=pr.kode_produk
			INNER JOIN %(treacounterpart)s cp ON a.kode_counterpart=cp.kode_counterpart
			LEFT JOIN %(referencedata)s ji ON pr.jenis_instrumen=ji.refdata_id

      %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = b.kode_cabang AND lc.id_user = %(id_user)s
	''' % _dict

  sql = {}
  sql['SELECTFROMClause'] = sSQL

  sql['WHEREClause'] = '''
    %(strOqlCond)s
  ''' % _dict
  
  sql['keyFieldName'] = 'b.NAMA_REKENING'
  sql['altOrderFieldNames'] = 'b.NAMA_REKENING'
  sql['baseOrderFieldNames'] = 'b.NAMA_REKENING'
  
  gsModule = modman.getModule(config, GRID_ACCOUNT)
  sql['columnSetting'] = gsModule.cols_setting(param.jenis_treasuryaccount, 'A')
  sql['r_tgl'] = r_tgl
  #sql['excel_cols']= EXCEL_COLUMN
  #sql['judul2']= judul2

  return sql
#--
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def getQueryData(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()

  sqlStat = getSQLClient(config, clientpacket)
  #raise Exception, sqlStat['SELECTFROMClause']+sqlStat['WHEREClause']
  try :
    rqsql = remotequery.RQSQL(config)
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '')
    rqsql.initOperation(returnpacket)

    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#

#========================= Export Excel xlsx===================================#
dTpl = { 'I':'tpl_fasbis_int.xlsx','E' :'tpl_fasbis_eks.xlsx'}

def styleCell(owb, R_, C_):
  #style border
  owb.cell(row=R_,column=C_).style.font.name = 'Trebuchet MS'
  owb.cell(row=R_,column=C_).style.font.size = 10
  owb.cell(row=R_,column=C_).style.borders.top.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.bottom.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.right.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.left.border_style = Border.BORDER_THIN
#--

def NumberFormat(owb, R_, C_):
  owb.cell(row=R_,column=C_).style.number_format.format_code = '#,##0.00�'
#--

def summarizedFields(owb, row, sF):
  owb.cell(row=row+1,column=0).value = 'JUMLAH : '
  owb.cell(row=row+1,column=0).style.font.bold = True
  for col in sF:    
    owb.cell(row=row+1,column=col).value = sF[col]
    NumberFormat(owb, row+1,col)
    owb.cell(row=row+1,column=col).style.font.bold = True
#--

def GenerateFile(config, params, returns):
  sqlStat = getSQLClient(config, params)
  try:
    status = returns.CreateValues(
      ['sucsess',0],
      ['Is_Err',0],['Err_Message','']
    )
    
    rec = params.FirstRecord
    oRec = atutil.GeneralObject(rec)    
    app = config.AppObject
    app.ConCreate('out')
    app.ConWriteln('Inisialisasi proses generate template...') 
    
    tpl_name = dTpl[rec.type_report]  
    fileNamePath = config.HomeDir + 'template\\'+tpl_name
  
    wb = openpyxl.load_workbook(fileNamePath)
    wsExcel = wb.get_sheet_by_name('data')
  
    group_by = 'jenis_treasuryaccount'
    if rec.type_report == 'I':
      group_by = 'kode_produk'
    
    order_by = '%s,' % group_by
    groups_field = [group_by]
        
    order_by += sqlStat['baseOrderFieldNames'].replace(';',',')
    sSQL = sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause']+' ORDER BY '+order_by
    #app.ConWriteln(sSQL)
    #app.ConRead('')
    q = config.CreateSQL(sSQL).RawResult
    q.First()
    
    startLine = 4
    if rec.type_report == 'E':
      startLine = 3
    
    _rRow = 2
    if rec.type_report == 'E':
      _rRow = 1
      
    if rec.tgl_awal > 0:
      wsExcel.cell(row=_rRow,column=0).value = "%s" % sqlStat['r_tgl']
    else:
      wsExcel.cell(row=_rRow,column=0).value = "SEMUA DATA"
      
    gsModule = modman.getModule(config, GRID_ACCOUNT)
    _cols = gsModule.ex_cols(rec.jenis_treasuryaccount, rec.type_report)
    _rCol = 3
    sF=[]
    for i in _cols:
      ls = i[0].split(";")
      if rec.type_report.upper() in ('I'):
        if ls[0]=='ujroh':
          sF.append(ls[0])
          _rCol += 1
      if rec.type_report.upper() in ('E'):
        if ls[0]=='nominal_deal':
          sF.append(ls[0])
          _rCol += 1
            
    mqr = QueryGrouping()
    mqr.config = config
    mqr.startLine = startLine
    mqr.cols = _cols
    mqr.group_by = groups_field
    mqr.query = q
    mqr.excel = wsExcel
    mqr.groups = groups_field
    mqr.summarizedFields = sF
    mqr.executeQuery()
    
    fileHasil = config.UserHomeDirectory + 'reportfasbis.xlsx'    
    fileutils.SafeDeleteFile(fileHasil) #Melakukan Hapus File yang sudah ada sebelumnya
    wb.save(fileHasil)    
  
    #return packet
    mmtype = fileNamePath.split(".")[-1]
    sw = returns.AddStreamWrapper()
    sw.name = 'gen_report'
    sw.LoadFromFile(fileHasil)
    if mmtype=='xlsx':
      sw.MIMEType = ".%s" % mmtype
    else:
     sw.MIMEType = config.AppObject.GetMIMETypeFromExtension(fileNamePath)
      
    #--endelse
  except:
    status.Is_Err = True
    status.Err_Message = _dbg_excmsg()#str(sys.exc_info()[1])

class QueryGrouping(queryreport.QueryReport):
  FirtGroup=1
  sumAll ={0:0}
  
  
  def eventProcessRow(self):
    # this virtual method is fired every time a row is processed
    q = self.query
    excel = self.excel
    startLine = self.startLine
    _no = self.rowCounter-self.FirtGroup
    
    styleCell(excel, startLine+self.rowCounter, 0)
    excel.cell(row=startLine + self.rowCounter,column=0).value = _no
    excel.cell(row=startLine + self.rowCounter,column=0).style.alignment.horizontal = "center"
                 
    _cols = self.cols
    _rCol = 1
    for i in _cols:
      ls = i[0].split(";")
      if i[1]=='D':
       tglIndo = modman.getModule(self.config,'TglIndo')
       tgl = eval('q.'+ls[0])
       nVal = tglIndo.tgl_indo(self.config,eval('q.'+ls[0]),2,1) if tgl<>None else ''
      else:
       nVal = eval('q.'+ls[0])
      
      excel.cell(row=startLine+self.rowCounter,column=_rCol).value = nVal
      
      if i[1]=='F':
        NumberFormat(excel, startLine+self.rowCounter, _rCol)
      else:
        excel.cell(row=startLine+self.rowCounter,column=_rCol).style.alignment.horizontal = "center"
          
      #style border cell
      styleCell(excel, startLine+self.rowCounter, _rCol)
      _rCol += 1
    pass

  def eventGroupStart(self, groupFieldName, groupValue):
    # this virtual method is fired every time a group starts
    excel = self.excel
    startLine = self.startLine    
    self.FirtGroup = self.rowCounter
    if groupFieldName == 'kode_produk':
      excel.cell(row=startLine + self.rowCounter - 1,column=0).style.font.name = 'Trebuchet MS'
      excel.cell(row=startLine + self.rowCounter - 1,column=0).style.font.size = 11
      excel.cell(row=startLine + self.rowCounter - 1,column=0).style.font.bold = True
      excel.cell(row=startLine + self.rowCounter - 1,column=0).value = '%s' % (groupValue)
      
      header = ["NO.", "NAMA  BANK", "TGL DEAL", "TGL. JTH TEMPO", "JMLH HARI", "NOMINAL (IDR)", "UJROH", "IMBALAN"]
      for i in range(len(header)):
        styleCell(excel, startLine+self.rowCounter, i)
        excel.cell(row=startLine + self.rowCounter,column=i).style.font.bold = True
        excel.cell(row=startLine + self.rowCounter,column=i).style.alignment.wrap_text= True
        excel.cell(row=startLine + self.rowCounter,column=i).style.alignment.horizontal = "center"
        excel.cell(row=startLine + self.rowCounter,column=i).style.alignment.vertical = "center"
        excel.cell(row=startLine + self.rowCounter,column=i).value = '%s' % (header[i])

    self.advanceRow()
    
  def eventGroupBreak(self, groupFieldName, groupValue):
    # this virtual method is fired every time a group breaks
    excel = self.excel
    startLine = self.startLine
    if groupFieldName in self.group_by:
      total = self.rowCounter-self.FirtGroup-1
      
      _cols = self.cols
      _rCol = 1
      for i in _cols:
        ls = i[0].split(";")
        if groupFieldName in ('kode_produk'):
          if ls[0]=='ujroh':
            sumValue = self.summaryFieldValues[(ls[0], groupFieldName)]
            excel.cell(row=startLine+self.rowCounter,column=_rCol).value = sumValue          
            excel.cell(row=startLine + self.rowCounter,column=_rCol).style.font.bold = True          
            
            if self.sumAll.has_key(_rCol):
              self.sumAll[_rCol] += sumValue
            else:
              self.sumAll[_rCol] = sumValue
        
        if groupFieldName in ('jenis_treasuryaccount'):
          if ls[0]=='nominal_deal':
            sumValue = self.summaryFieldValues[(ls[0], groupFieldName)]
            excel.cell(row=startLine+self.rowCounter,column=_rCol).value = sumValue          
            excel.cell(row=startLine + self.rowCounter,column=_rCol).style.font.bold = True          
            
            if self.sumAll.has_key(_rCol):
              self.sumAll[_rCol] += sumValue
            else:
              self.sumAll[_rCol] = sumValue
        
        NumberFormat(excel, startLine+self.rowCounter, _rCol)
        styleCell(excel, startLine+self.rowCounter, _rCol)
        _rCol += 1
        
      self.sumAll[0] += total        
      styleCell(excel, startLine+self.rowCounter, 0)
      self.advanceRow()
      self.advanceRow()
      #excel.SetCellValue(startLine + self.rowCounter - 1, 1, '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\-------------------------------------------------------------------------')
    #--endif  
    self.advanceRow()