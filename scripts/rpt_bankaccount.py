import os
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import com.ihsan.util.modman as modman
import openpyxl
from openpyxl.cell import get_column_letter
from openpyxl.styles import Border
from openpyxl.styles import Font
from openpyxl import Workbook
import com.ihsan.fileutils as fileutils
from copy import deepcopy
from com.ihsan.lib import queryreport
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.attrutil as atutil
import calendar
import datetime
from datetime import datetime

modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

GRID_ACCOUNT = 'gridsettings.listaccount_cols'

def dbg(msg):
  #app.ConWriteln(msg)
  #app.ConRead(msg2)
  message.send_udp(msg + "\n", 'localhost', 9122)

def getSQLClient(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils  
  param = params.FirstRecord
  tglIndo = modman.getModule(config,'TglIndo')

  #raise Exception, param.all_cabang
  r_tgl = ''
  lcond = []
  lcond.append( ''' ('1'='%(all_cabang)s' or b.kode_cabang = '%(kode_cabang)s') ''' % {'kode_cabang':param.kode_cabang,'all_cabang':param.all_cabang} )
  lcond.append( ''' ('0'='%(sr)s' or b.status_rekening = %(sr)s ) ''' % {'sr':param.status_rekening}) 
  lcond.append( ''' a.jenis_treasuryaccount in ('%s') ''' % param.jenis_treasuryaccount)

  if param.kode_produk <> "ALL":
    lcond.append( ''' a.kode_produk='%s' ''' % param.kode_produk)
    
  if param.kode_counterpart <> "ALL":                                 
    lcond.append( ''' a.kode_counterpart='%s' ''' % param.kode_counterpart) 
    
  if param.kode_valuta <> "ALL":                                 
    lcond.append( ''' b.kode_valuta='%s' ''' % param.kode_valuta)
      
  if param.tgl_awal > 0:
    lcond.append( '''to_date('%s', 'dd/mm/yyyy')<=a.tgl_buka ''' % (tglIndo.tgl_indo(config,param.tgl_awal,10)) )
    r_tgl += 'TGL BUKA %s ' % (tglIndo.tgl_indo(config,param.tgl_awal,2))
  if param.tgl_akhir > 0:
    lcond.append( '''to_date('%s', 'dd/mm/yyyy')>=a.tgl_buka ''' % (tglIndo.tgl_indo(config,param.tgl_akhir,10)) )
    r_tgl += '- %s' % (tglIndo.tgl_indo(config,param.tgl_akhir,2))
      
  strOqlCond = ''      
  if len(lcond) > 0 :   
    strOqlCond = " AND ".join(lcond)
    strOqlCond = strOqlCond
 
  _dict = {
    'treasuryaccount':config.MapDBTableName("treasuryaccount"),
    'rekeningtransaksi':config.MapDBTableName("core.rekeningtransaksi"),
    'treaproduk':config.MapDBTableName("treaproduk"),
    'treacounterpart':config.MapDBTableName("treacounterpart"), 
    'listperanuser':config.MapDBTableName("enterprise.listperanuser"), 
    'listcabangdiizinkan':config.MapDBTableName("enterprise.listcabangdiizinkan"), 
    'id_user': mlu.QuotedStr(config.SecurityContext.InitUser),
    'strOqlCond':strOqlCond
  } 
  
  sSQL = '''
    SELECT b.tipe_akses_cabang FROM %(userapp)s b WHERE b.id_user='%(id_user)s' 
  ''' % {
    'userapp': config.MapDBTableName("enterprise.userapp"),
    'id_user':config.SecurityContext.InitUser
  }
  rSQL = config.CreateSQL(sSQL).RawResult

  _dict['LCDJoinType'] = 'LEFT JOIN' if rSQL.tipe_akses_cabang=='S' else 'INNER JOIN'
 	
  sSQL = '''
    SELECT DISTINCT 
      a.Nomor_Rekening,
      b.NAMA_REKENING,
      -(b.saldo) as saldo, 
      a.kode_produk, 
      b.kode_cabang, b.kode_valuta,
      a.tgl_buka,
      cp.nama_counterpart,
      pr.nama_produk,
      b.keterangan,
      decode(b.status_rekening,1,'AKTIF','TUTUP') status_rekening
    FROM %(treasuryaccount)s a
    	INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
			INNER JOIN %(treaproduk)s pr ON a.kode_produk=pr.kode_produk
			INNER JOIN %(treacounterpart)s cp ON a.kode_counterpart=cp.kode_counterpart
      %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = b.kode_cabang AND lc.id_user = %(id_user)s
	''' % _dict
  
  sql = {}
  sql['SELECTFROMClause'] = sSQL
  sql['WHEREClause'] = '''
    %(strOqlCond)s
  ''' % _dict
  sql['keyFieldName'] = 'b.NAMA_REKENING'
  sql['altOrderFieldNames'] = 'b.NAMA_REKENING;cp.nama_counterpart'
  sql['baseOrderFieldNames'] = 'b.NAMA_REKENING;cp.nama_counterpart'
  
  gsModule = modman.getModule(config, GRID_ACCOUNT)
  sql['columnSetting'] = gsModule.cols_setting(param.jenis_treasuryaccount, 'A')
  sql['r_tgl'] = r_tgl
  #sql['excel_cols']= EXCEL_COLUMN
  #sql['judul2']= judul2

  return sql
#--
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def getQueryData(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()

  sqlStat = getSQLClient(config, clientpacket)
  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('1...')
  #sSQL = sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause']+' GROUP BY '+sqlStat['GROUPBYClause']
  #app.ConWriteln(sSQL)
  #app.ConRead('')
  #raise Exception, sqlStat['SELECTFROMClause']+sqlStat['WHEREClause']
  try :                        
    app.ConWriteln('2...')
    rqsql = remotequery.RQSQL(config)   
    app.ConWriteln('3...')
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '') 
    app.ConWriteln('4...')
    rqsql.initOperation(returnpacket)
    app.ConWriteln('5...')

    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#--  

#========================= Export Data===================================#
def styleCell(owb, R_, C_):
  #style border
  owb.cell(row=R_,column=C_).style.font.name = 'Trebuchet MS'
  owb.cell(row=R_,column=C_).style.font.size = 10
  owb.cell(row=R_,column=C_).style.borders.top.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.bottom.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.right.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.left.border_style = Border.BORDER_THIN
#--

def NumberFormat(owb, R_, C_):
  owb.cell(row=R_,column=C_).style.number_format.format_code = '#,##0.00�'
#--

def GenerateFile(config, params, returns):
  sqlStat = getSQLClient(config, params)
  try:
    status = returns.CreateValues(
      ['sucsess',0],
      ['Is_Err',0],['Err_Message','']
    )
       
    app = config.AppObject
    app.ConCreate('out')
    app.ConWriteln('Inisialisasi proses generate template...')
          
    reportFile = GenerateReport(config, params, sqlStat)
     
    #return packet
    mmtype = reportFile.split(".")[-1]
    sw = returns.AddStreamWrapper()
    sw.name = 'gen_report'
    sw.LoadFromFile(reportFile)                       
    if mmtype=='xlsx':
      sw.MIMEType = ".%s" % mmtype
    else:
     sw.MIMEType = config.AppObject.GetMIMETypeFromExtension(reportFile)
  except:
    status.Is_Err = True
    status.Err_Message = _dbg_excmsg()#str(sys.exc_info()[1])
    
def GenerateReport(config, params, sql):    
    rec = params.FirstRecord     
    fileNamePath = config.HomeDir + 'template\\tpl_fasbis_int.xlsx'
    
    wb = openpyxl.load_workbook(fileNamePath)
    wsExcel = wb.get_sheet_by_name('data')
    
    order_by = sql['baseOrderFieldNames'].replace(';',',')
    sSQL = sql['SELECTFROMClause']+' WHERE '+sql['WHEREClause']+' ORDER BY '+order_by
    #app.ConWriteln(sSQL)
    #app.ConRead('')
  
    q = config.CreateSQL(sSQL).RawResult
    q.First()
    
    gsModule = modman.getModule(config, GRID_ACCOUNT)
    _cols = gsModule.ex_cols(rec.jenis_treasuryaccount, rec.type_report)    
        
    #header  
    wsExcel.cell(row=0,column=0).value = 'BADAN PENGELOLA KEUANGAN HAJI (BPKH)'
    wsExcel.cell(row=1,column=0).value = 'LAPORAN REKENING BANK'
    wsExcel.cell(row=2,column=0).value = sql['r_tgl'] or "SEMUA DATA" 
    
    startLine = 4
    styleCell(wsExcel, startLine, 0)
    wsExcel.cell(row=startLine,column=0).style.font.bold = True
    wsExcel.cell(row=startLine,column=0).style.alignment.wrap_text= True
    wsExcel.cell(row=startLine,column=0).style.alignment.horizontal = "center"
    wsExcel.cell(row=startLine,column=0).style.alignment.vertical = "center"
    wsExcel.cell(row=startLine,column=0).value = 'NO.'  
    wsExcel.column_dimensions['A'].width = 6
    
    _rCol = 1
    for i in range(len(_cols)):
      ls = _cols[i][0].split(";") 
      styleCell(wsExcel, startLine, _rCol)
      wsExcel.cell(row=startLine,column=_rCol).style.font.bold = True 
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.wrap_text= True
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.horizontal = "center"
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.vertical = "center"
      wsExcel.cell(row=startLine,column=_rCol).value = '%s' % (ls[1])
      _rCol += 1
             
    _rRow = 5
    _no = 1
    while not q.Eof:   
      _rCol = 1
      styleCell(wsExcel, _rRow, 0)
      wsExcel.cell(row=_rRow,column=0).style.alignment.horizontal = "center"
      wsExcel.cell(row=_rRow,column=0).value = _no 
      for i in _cols:
        ls = i[0].split(";")
        if i[1]=='D':
         tgl = eval('q.'+ls[0])
         nVal = "%s-%s-%s" %(str(tgl[0]).zfill(4), str(tgl[1]).zfill(2), str(tgl[2]).zfill(2)) if tgl<>None else ''
        else:
         nVal = eval('q.'+ls[0])
        
        _col = get_column_letter(_rCol+1)
        styleCell(wsExcel, _rRow, _rCol)
        wsExcel.cell(row=_rRow,column=_rCol).value = nVal 
        wsExcel.column_dimensions[_col].width = i[2]
        
        if i[1]=='F':
          wsExcel.cell(row=_rRow,column=_rCol).style.number_format.format_code = '#,##0.00�'
        else:
          wsExcel.cell(row=_rRow,column=_rCol).style.number_format.format_code = '0'
          wsExcel.cell(row=_rRow,column=_rCol).style.alignment.horizontal = "left"
            
        _rCol += 1
      
      _no += 1  
      _rRow += 1        
      q.Next()
          
    fileHasil = config.UserHomeDirectory + 'reportfasbis.xlsx'    
    fileutils.SafeDeleteFile(fileHasil) #Melakukan Hapus File yang sudah ada sebelumnya
    wb.save(fileHasil)
    
    return fileHasil