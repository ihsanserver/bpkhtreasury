import os
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import com.ihsan.util.modman as modman
import openpyxl
from openpyxl.cell import get_column_letter
from openpyxl.styles import Border
from openpyxl.styles import Font
from openpyxl import Workbook
import com.ihsan.fileutils as fileutils
from copy import deepcopy
from com.ihsan.lib import queryreport
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.attrutil as atutil
import calendar
import datetime
from datetime import datetime

modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

GRID_ACCOUNT = 'gridsettings.listaccount_cols'

def dbg(msg):
  #app.ConWriteln(msg)
  #app.ConRead(msg2)
  message.send_udp(msg + "\n", 'localhost', 9122)

def getSQLClient(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils            
  #fr = params.FirstRecord
  param = params.FirstRecord
  tglIndo = modman.getModule(config,'TglIndo')
  
  #phTgl = config.AppObject.rexecscript("core", "appinterface/coreInfo.getPrevAccountingDay", config.AppObject.CreatePacket())
  #tgl_prevacc = tglIndo.tgl_indo(config,phTgl.FirstRecord.acc_date,10)
  
  tgl_awal = tglIndo.tgl_indo(config,param.tgl_awal,10)
  tgl_acc = tglIndo.tgl_indo(config,param.tgl_akhir,10) 
  tgl_prevacc = getPrevAcc(config, tgl_awal, tgl_acc)
  
  if tgl_prevacc == 'None':
    tgl_prevacc = tgl_acc
    
  r_tgl = ''
  lcond = []
  lcond.append( ''' tp.tipe_treasury in ('%s') ''' % param.tipe_treasury)
  lcond.append( ''' ('1'='%(all_cabang)s' or rt.kode_cabang = '%(kode_cabang)s') ''' % {'kode_cabang':param.kode_cabang,'all_cabang':param.all_cabang} )
  lcond.append( ''' ('0'='%(sr)s' or rt.status_rekening = %(sr)s ) ''' % {'sr':param.status_rekening}) 

  if param.jenis_treasuryaccount <> "ALL":
    lcond.append( ''' b.jenis_treasuryaccount in ('%s') ''' % param.jenis_treasuryaccount)
  if param.kode_produk <> "ALL":
    lcond.append( ''' a.kode_produk='%s' ''' % param.kode_produk)
    
  if param.tgl_awal > 0:
    lcond.append( '''to_date('%s', 'dd/mm/yyyy')<=a.periode ''' % (tglIndo.tgl_indo(config,param.tgl_awal,10)) )
    r_tgl += 'PERIODE ACCRUAL %s ' % (tglIndo.tgl_indo(config,param.tgl_awal,2))
  if param.tgl_akhir > 0:
    lcond.append( '''to_date('%s', 'dd/mm/yyyy')>=a.periode ''' % (tglIndo.tgl_indo(config,param.tgl_akhir,10)) )
    r_tgl += '- %s' % (tglIndo.tgl_indo(config,param.tgl_akhir,2))
      
  strOqlCond = ''      
  if len(lcond) > 0 :   
    strOqlCond = " AND ".join(lcond)
    strOqlCond = strOqlCond
 
  _dict = {
    'dailytreaakrubalance':config.MapDBTableName("dailytreaakrubalance"),
    
    'treasuryaccount':config.MapDBTableName("treasuryaccount"),
    'rekeningtransaksi':config.MapDBTableName("core.rekeningtransaksi"),
    'treacounterpart':config.MapDBTableName("treacounterpart"), 
    'treaproduk':config.MapDBTableName("treaproduk"), 
    
    'HISTDETILTRANSAKSI':config.MapDBTableName("HISTDETILTRANSAKSI"), 
    'HISTTRANSAKSI':config.MapDBTableName("HISTTRANSAKSI"), 
    
    'listperanuser':config.MapDBTableName("enterprise.listperanuser"), 
    'listcabangdiizinkan':config.MapDBTableName("enterprise.listcabangdiizinkan"),
    'id_user': mlu.QuotedStr(config.SecurityContext.InitUser),
    'tgl_awal':tgl_awal,         
    'tgl_prevacc':tgl_prevacc,    
    'tgl_acc':tgl_acc,
    'strOqlCond':strOqlCond
  } 
  
  sSQL = '''
    SELECT b.tipe_akses_cabang FROM %(userapp)s b WHERE b.id_user='%(id_user)s' 
  ''' % {
    'userapp': config.MapDBTableName("enterprise.userapp"),
    'id_user':config.SecurityContext.InitUser
  }
  rSQL = config.CreateSQL(sSQL).RawResult
  _dict['LCDJoinType'] = 'LEFT JOIN' if rSQL.tipe_akses_cabang=='S' else 'INNER JOIN'
  _dict['tbljoin'] = '''
      LEFT JOIN (
         SELECT
           dab.NOMOR_REKENING, MAX(dab.ACCRUE_DAY) accrue_day, SUM(dab.NILAI_AKRU) AS saldo_awal
         FROM
           DAILYTREAAKRUBALANCE dab
         WHERE
           dab.PERIODE < to_date('%(tgl_awal)s', 'dd/mm/yyyy')
         GROUP BY dab.NOMOR_REKENING
      ) c ON a.NOMOR_REKENING = c.NOMOR_REKENING
    	LEFT JOIN (
         SELECT
           dab.NOMOR_REKENING, MAX(dab.ACCRUE_DAY) accrue_day, SUM(dab.NILAI_AKRU) AS NILAI_ACCRUE
         FROM
           DAILYTREAAKRUBALANCE dab
         WHERE
           dab.PERIODE >= to_date('%(tgl_awal)s', 'dd/mm/yyyy') AND dab.PERIODE < to_date('%(tgl_acc)s', 'dd/mm/yyyy')
         GROUP BY dab.NOMOR_REKENING
      ) d ON a.NOMOR_REKENING = d.NOMOR_REKENING
  ''' % _dict
        
  sSQL = '''
    SELECT DISTINCT
    	a.NAMA_REKENING nama_counterpart,
    	a.NOMOR_REKENING,
      ROUND(a.NOMINAL_DEAL, 2) AS nominal_sbh,
      ROUND(b.EQV_RATE_REAL, 5) AS imbalan,
    	b.TGL_BUKA,	       	
    	tp.nama_produk,
    	case 
    		when to_date('%(tgl_awal)s', 'dd/mm/yyyy') < a.TGL_MULAI_AKRU then a.TGL_MULAI_AKRU 
    		else to_date('%(tgl_awal)s', 'dd/mm/yyyy') 
    	end as tgl_awal,
    	case 
    		when to_date('%(tgl_acc)s', 'dd/mm/yyyy') >= b.tgl_jatuh_tempo then b.tgl_jatuh_tempo
    		else to_date('%(tgl_acc)s', 'dd/mm/yyyy') 
    	end as tgl_accrue,
    	NVL(d.accrue_day, 0) - NVL(c.accrue_day, 0) accrue_day,
    	tp.basis_hari_pertahun,                                   	
      NVL(c.saldo_awal, 0) AS SALDO_AWAL,
      NVL(d.nilai_accrue, 0) AS nilai_accrue,
      NVL(c.saldo_awal, 0) + NVL(d.nilai_accrue, 0) AS saldo_akhir,
    	rt.KODE_CABANG,
    	case 
    		when rt.STATUS_REKENING = 1 then 'AKTIF'
    		when rt.STATUS_REKENING in (3,99) then 'TUTUP' 
    	end as status_rekening
    FROM
    	%(dailytreaakrubalance)s a
    	INNER JOIN %(treasuryaccount)s b ON a.NOMOR_REKENING = b.NOMOR_REKENING
    	INNER JOIN %(rekeningtransaksi)s rt ON b.NOMOR_REKENING = rt.NOMOR_REKENING
    	INNER JOIN %(treaproduk)s tp ON b.KODE_PRODUK = tp.KODE_PRODUK
   		%(tbljoin)s
      %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = rt.KODE_CABANG AND lc.id_user = %(id_user)s
  	''' % _dict
  
  
  sql = {}
  sql['SELECTFROMClause'] = sSQL
    
  sql['WHEREClause'] = ''' 
    %(strOqlCond)s
  ''' % _dict
  
  sql['keyFieldName'] = 'a.NOMOR_REKENING'
  sql['altOrderFieldNames'] = 'a.NOMOR_REKENING'
  sql['baseOrderFieldNames'] = 'a.NOMOR_REKENING'
  
  gsModule = modman.getModule(config, GRID_ACCOUNT)
  sql['columnSetting'] = gsModule.cols_setting(param.jenis_treasuryaccount, 'Z')
  sql['r_tgl'] = r_tgl
  #sql['excel_cols']= EXCEL_COLUMN
  #sql['judul2']= judul2

  return sql
#--
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def getQueryData(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()

  sqlStat = getSQLClient(config, clientpacket)
  
  app = config.AppObject
  app.ConCreate('out')
  sSQL = sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause']
  #app.ConWriteln(sSQL)
  #app.ConRead('')
  
  #raise Exception, sqlStat['SELECTFROMClause']+sqlStat['WHEREClause']
  try :
    rqsql = remotequery.RQSQL(config)
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '')
    rqsql.initOperation(returnpacket)

    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#--  

def getPrevAcc(config, tgl_awal, tgl_akhir):
  SQL = '''
    SELECT MAX(TO_CHAR(DATEVALUE, 'dd/mm/yyyy')) tgl
    FROM %(accountingday)s
    WHERE ISWORKDAY = 'T'
    AND DATEVALUE >= TO_DATE('%(tgl_awal)s', 'dd/mm/yyyy')
    AND DATEVALUE < TO_DATE('%(tgl_akhir)s', 'dd/mm/yyyy')
  ''' % {
    'accountingday': config.MapDBTableName("core.accountingday"),
    'tgl_awal':tgl_awal,
    'tgl_akhir':tgl_akhir
  }
  q = config.CreateSQL(SQL).RawResult
  if not q.Eof:
    tgl = '%s' % (q.tgl)

  return tgl

#========================= Export Excel xlsx===================================#
dTpl = {
        'ALL':{'Z':'tpl_fasbis_int.xlsx'},
        'F'  :{'Z':'tpl_fasbis_int.xlsx'},
        'D'  :{'Z':'tpl_fasbis_int.xlsx'},
        'S'  :{'Z':'tpl_fasbis_int.xlsx'},
        'B'  :{'Z':'tpl_fasbis_int.xlsx'},
        'R'  :{'Z':'tpl_fasbis_int.xlsx'},        
       }

def styleCell(owb, R_, C_):
  #style border
  owb.cell(row=R_,column=C_).style.font.name = 'Trebuchet MS'
  owb.cell(row=R_,column=C_).style.font.size = 10
  owb.cell(row=R_,column=C_).style.borders.top.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.bottom.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.right.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.left.border_style = Border.BORDER_THIN
#--

def NumberFormat(owb, R_, C_):
  owb.cell(row=R_,column=C_).style.number_format.format_code = '#,##0.00�'
#--

def summarizedFields(owb, row, sF):
  owb.cell(row=row+1,column=0).value = 'JUMLAH : '
  owb.cell(row=row+1,column=0).style.font.bold = True
  for col in sF:    
    owb.cell(row=row+1,column=col).value = sF[col]
    NumberFormat(owb, row+1,col)
    owb.cell(row=row+1,column=col).style.font.bold = True
#--

def GenerateFile(config, params, returns):
  sqlStat = getSQLClient(config, params)
  try:
    status = returns.CreateValues(
      ['sucsess',0],
      ['Is_Err',0],['Err_Message','']
    )
    
    rec = params.FirstRecord
    oRec = atutil.GeneralObject(rec)    
    app = config.AppObject
    app.ConCreate('out')
    app.ConWriteln('Inisialisasi proses generate template...')
    
    tpl_name = dTpl[rec.jenis_treasuryaccount][rec.type_report]  
    fileNamePath = config.HomeDir + 'template\\'+tpl_name
  
    wb = openpyxl.load_workbook(fileNamePath)
    wsExcel = wb.get_sheet_by_name('data')
  
    group_by = 'jenis_treasuryaccount'
    if rec.type_report == 'Z':
      group_by = 'nama_produk'
    
    order_by = '%s,' % group_by
    groups_field = [group_by]
        
    order_by += sqlStat['baseOrderFieldNames'].replace(';',',')
    sSQL = sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause']+' ORDER BY '+order_by
    #app.ConWriteln(sSQL)
    #app.ConRead('')
    q = config.CreateSQL(sSQL).RawResult
    q.First()
    
    startLine = 4    
    _rRow = 2
          
    # Header                                                 
    wsExcel.cell(row=0,column=0).value = "BADAN PENGELOLA KEUANGAN HAJI (BPKH)"
    wsExcel.cell(row=1,column=0).value = "LAPORAN ACCRUAL TREASURY"
    if rec.tgl_awal > 0:
      wsExcel.cell(row=_rRow,column=0).value = "%s" % sqlStat['r_tgl']
    else:
      wsExcel.cell(row=_rRow,column=0).value = "SEMUA DATA"
      
    gsModule = modman.getModule(config, GRID_ACCOUNT)
    _cols = gsModule.ex_cols(rec.jenis_treasuryaccount, rec.type_report)
    _rCol = 3
    sF=[]
    for i in _cols:
      ls = i[0].split(";")
      if ls[0] in ['nominal_sbh', 'saldo_awal', 'nilai_accrue', 'saldo', 'saldo_accrue']:
        sF.append(ls[0])
        _rCol += 1
                   
    mqr = QueryGrouping()
    mqr.config = config
    mqr.startLine = startLine
    mqr.cols = _cols
    mqr.group_by = groups_field
    mqr.query = q
    mqr.excel = wsExcel
    mqr.groups = groups_field
    mqr.summarizedFields = sF
    mqr.executeQuery()
    
    fileHasil = config.UserHomeDirectory + 'reportfasbis.xlsx'    
    fileutils.SafeDeleteFile(fileHasil) #Melakukan Hapus File yang sudah ada sebelumnya
    wb.save(fileHasil)    
  
    #return packet
    mmtype = fileNamePath.split(".")[-1]
    sw = returns.AddStreamWrapper()
    sw.name = 'gen_report'
    sw.LoadFromFile(fileHasil)
    if mmtype=='xlsx':
      sw.MIMEType = ".%s" % mmtype
    else:
     sw.MIMEType = config.AppObject.GetMIMETypeFromExtension(fileNamePath)
      
    #--endelse
  except:
    status.Is_Err = True
    status.Err_Message = _dbg_excmsg()#str(sys.exc_info()[1])

class QueryGrouping(queryreport.QueryReport):
  FirtGroup=1
  sumAll ={0:0}
  
  
  def eventProcessRow(self):
    # this virtual method is fired every time a row is processed
    q = self.query
    excel = self.excel
    startLine = self.startLine
    _no = self.rowCounter-self.FirtGroup
    
    styleCell(excel, startLine+self.rowCounter, 0)
    excel.cell(row=startLine + self.rowCounter,column=0).value = _no
    excel.cell(row=startLine + self.rowCounter,column=0).style.alignment.horizontal = "center"
                 
    _cols = self.cols
    _rCol = 1
    for i in _cols:
      ls = i[0].split(";")
      if i[1]=='D':
       tgl = eval('q.'+ls[0])
       nVal = "%s-%s-%s" %(str(tgl[0]).zfill(4), str(tgl[1]).zfill(2), str(tgl[2]).zfill(2)) if tgl<>None else ''
      else:
       nVal = eval('q.'+ls[0])
      
      excel.cell(row=startLine+self.rowCounter,column=_rCol).value = nVal
      
      if i[1]=='F':
        NumberFormat(excel, startLine+self.rowCounter, _rCol)
      else:
        excel.cell(row=startLine+self.rowCounter,column=_rCol).style.alignment.horizontal = "center"
          
      #style border cell
      styleCell(excel, startLine+self.rowCounter, _rCol)
      _rCol += 1
    pass

  def eventGroupStart(self, groupFieldName, groupValue):
    # this virtual method is fired every time a group starts
    excel = self.excel
    startLine = self.startLine    
    self.FirtGroup = self.rowCounter
    if groupFieldName == 'nama_produk':
      excel.cell(row=startLine + self.rowCounter - 1,column=0).style.font.name = 'Trebuchet MS'
      excel.cell(row=startLine + self.rowCounter - 1,column=0).style.font.size = 11
      excel.cell(row=startLine + self.rowCounter - 1,column=0).style.font.bold = True
      excel.cell(row=startLine + self.rowCounter - 1,column=0).value = '%s' % (groupValue)
      
      styleCell(excel, startLine+self.rowCounter, 0)
      excel.cell(row=startLine + self.rowCounter,column=0).style.font.bold = True
      excel.cell(row=startLine + self.rowCounter,column=0).style.alignment.wrap_text= True
      excel.cell(row=startLine + self.rowCounter,column=0).style.alignment.horizontal = "center"
      excel.cell(row=startLine + self.rowCounter,column=0).style.alignment.vertical = "center"
      excel.cell(row=startLine + self.rowCounter,column=0).value = 'NO.'  
      excel.column_dimensions['A'].width = 6
      
      _cols = self.cols
      _rCol = 1      
      for i in _cols:
        ls = i[0].split(";")
        _col = get_column_letter(_rCol+1)
        styleCell(excel, startLine+self.rowCounter, _rCol)
        excel.cell(row=startLine + self.rowCounter,column=_rCol).style.font.bold = True
        excel.cell(row=startLine + self.rowCounter,column=_rCol).style.alignment.wrap_text= True
        excel.cell(row=startLine + self.rowCounter,column=_rCol).style.alignment.horizontal = "center"
        excel.cell(row=startLine + self.rowCounter,column=_rCol).style.alignment.vertical = "center"
        excel.cell(row=startLine + self.rowCounter,column=_rCol).value = '%s' % (ls[1]).upper()
        excel.column_dimensions[_col].width = i[3]
        _rCol += 1

    self.advanceRow()
    
  def eventGroupBreak(self, groupFieldName, groupValue):
    # this virtual method is fired every time a group breaks
    excel = self.excel
    startLine = self.startLine
    sF =  self.summaryFieldValues 
    _sF = []
    
    for i in range(len(sF)):
      ls = sF.keys()[i][0]
      _sF.append(ls)
    
    if groupFieldName in self.group_by:
      total = self.rowCounter-self.FirtGroup-1
      
      _cols = self.cols
      _rCol = 1
      for i in _cols:
        ls = i[0].split(";")
        if groupFieldName in ('nama_produk'):
          if ls[0] in _sF:
            sumValue = self.summaryFieldValues[(ls[0], groupFieldName)]
            excel.cell(row=startLine+self.rowCounter,column=_rCol).value = sumValue          
            excel.cell(row=startLine + self.rowCounter,column=_rCol).style.font.bold = True          
            
            if self.sumAll.has_key(_rCol):
              self.sumAll[_rCol] += sumValue
            else:
              self.sumAll[_rCol] = sumValue
        
        if groupFieldName in ('jenis_treasuryaccount'):
          if ls[0]=='nominal_deal':
            sumValue = self.summaryFieldValues[(ls[0], groupFieldName)]
            excel.cell(row=startLine+self.rowCounter,column=_rCol).value = sumValue          
            excel.cell(row=startLine + self.rowCounter,column=_rCol).style.font.bold = True          
            
            if self.sumAll.has_key(_rCol):
              self.sumAll[_rCol] += sumValue
            else:
              self.sumAll[_rCol] = sumValue
        
        NumberFormat(excel, startLine+self.rowCounter, _rCol)
        styleCell(excel, startLine+self.rowCounter, _rCol)
        _rCol += 1
        
      self.sumAll[0] += total        
      styleCell(excel, startLine+self.rowCounter, 0)
      self.advanceRow()
      self.advanceRow()
      #excel.SetCellValue(startLine + self.rowCounter - 1, 1, '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\-------------------------------------------------------------------------')
    #--endif  
    self.advanceRow()