import os
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import com.ihsan.util.modman as modman
import openpyxl
from openpyxl.cell import get_column_letter
from openpyxl.styles import Border
from openpyxl.styles import Font
from openpyxl import Workbook
import com.ihsan.fileutils as fileutils
from copy import deepcopy
from com.ihsan.lib import queryreport
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.attrutil as atutil
import calendar
import datetime
from datetime import datetime

modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

GRID_ACCOUNT = 'gridsettings.listaccount_cols'

def dbg(msg):
  #app.ConWriteln(msg)
  #app.ConRead(msg2)
  message.send_udp(msg + "\n", 'localhost', 9122)

def getSQLClient(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils  
  param = params.FirstRecord
  tglIndo = modman.getModule(config,'TglIndo')

  #raise Exception, param.all_cabang
  r_tgl = ''
  lcond = [''' ta.jenis_treasuryaccount in ('%s') ''' % param.jenis_treasuryaccount]
  lcond.append( ''' ('1'='%(all_cabang)s' or rt.kode_cabang = '%(kode_cabang)s') ''' % {'kode_cabang':param.kode_cabang,'all_cabang':param.all_cabang} )

  fl_tgl = 'balance_date'
  if param.type_report in ('3', '2'):
    fl_tgl = 'tr.tanggal_transaksi'
    if param.kode_remark <> "ALL":
      lcond.append( ''' tr.kode_transaksi='%s' ''' % param.kode_remark)

  if param.kode_produk <> "ALL":
    lcond.append( ''' ta.kode_produk='%s' ''' % param.kode_produk)
    
  if param.kode_counterpart <> "ALL":                                 
    lcond.append( ''' ta.kode_counterpart='%s' ''' % param.kode_counterpart) 
    
  if param.kode_valuta <> "ALL":                                 
    lcond.append( ''' rt.kode_valuta='%s' ''' % param.kode_valuta)
      
  if param.keyword_search not in ["", None]:                                 
    lcond.append( ''' (upper(rt.nomor_rekening) like upper('%%%s%%') or upper(kode_instrumen) like upper('%%%s%%')) ''' % (param.keyword_search, param.keyword_search))
          
  tx_awal=''
  tx_akhir = '''to_date('%s', 'dd/mm/yyyy')>=%s ''' % (tglIndo.tgl_indo(config,param.tgl_acc,10), fl_tgl) 
  if param.tx_awal > 0:
    tx_awal = ''' and to_date('%s', 'dd/mm/yyyy')<=%s ''' % (tglIndo.tgl_indo(config,param.tx_awal,10), fl_tgl)
  if param.tx_akhir > 0:
    tx_akhir = '''to_date('%s', 'dd/mm/yyyy')>=%s ''' % (tglIndo.tgl_indo(config,param.tx_akhir,10), fl_tgl)
      
  if param.tgl_awal > 0:
    lcond.append( '''to_date('%s', 'dd/mm/yyyy')<=ta.tgl_buka ''' % (tglIndo.tgl_indo(config,param.tgl_awal,10)) )
    r_tgl += 'TGL BUKA %s ' % (tglIndo.tgl_indo(config,param.tgl_awal,2))
  if param.tgl_akhir > 0:
    lcond.append( '''to_date('%s', 'dd/mm/yyyy')>=ta.tgl_buka ''' % (tglIndo.tgl_indo(config,param.tgl_akhir,10)) )
    r_tgl += '- %s' % (tglIndo.tgl_indo(config,param.tgl_akhir,2))

  if param.tx_akhir >= param.tgl_acc or param.tx_akhir == 0 :
    lcond.append( ''' ('0'='%(sr)s' or rt.status_rekening = %(sr)s ) ''' % {'sr':param.status_rekening}) 
      

  #raise Exception, lcond
  strOqlCond = ''      
  if len(lcond) > 0 :   
    strOqlCond = " AND ".join(lcond)
    #strOqlCond = strOqlCond
 
  _dict = {
    'DailyBalanceSukuk':config.MapDBTableName("DailyBalanceSukuk"),
    'treasuryaccount':config.MapDBTableName("treasuryaccount"),
    'rekeningtransaksi':config.MapDBTableName("core.rekeningtransaksi"),
    'treasuratberharga':config.MapDBTableName("treasuratberharga"),
    'treaproduk':config.MapDBTableName("treaproduk"),
    'treacounterpart':config.MapDBTableName("treacounterpart"), 

    'transaksi':config.MapDBTableName("transaksi"), 
    'detiltransaksi':config.MapDBTableName("detiltransaksi"), 
    'histtransaksi':config.MapDBTableName("histtransaksi"), 
    'histdetiltransaksi':config.MapDBTableName("histdetiltransaksi"), 
    'kurshistory':config.MapDBTableName("kurshistory"), 
    'detiltransaksiclass':config.MapDBTableName("detiltransaksiclass"), 

    'enum_varchar':config.MapDBTableName("enum_varchar"), 

    'listperanuser':config.MapDBTableName("enterprise.listperanuser"), 
    'listcabangdiizinkan':config.MapDBTableName("enterprise.listcabangdiizinkan"), 
    'id_user': mlu.QuotedStr(config.SecurityContext.InitUser),
    'tx_awal':tx_awal,
    'tx_akhir':tx_akhir,
    'fTanggalAkhir' : ''' to_date('%s', 'dd/mm/yyyy') ''' % tglIndo.tgl_indo(config,param.tx_akhir,10),
    'strOqlCond':strOqlCond
  } 
  
  sSQL = '''
    SELECT b.tipe_akses_cabang FROM %(userapp)s b WHERE b.id_user='%(id_user)s' 
  ''' % {
    'userapp': config.MapDBTableName("enterprise.userapp"),
    'id_user':config.SecurityContext.InitUser
  }
  rSQL = config.CreateSQL(sSQL).RawResult

  _dict['LCDJoinType'] = 'LEFT JOIN' if rSQL.tipe_akses_cabang=='S' else 'INNER JOIN'
 	
  sql = {}
  if param.type_report == '1':                                  
    if param.tx_akhir >= param.tgl_acc or param.tx_akhir == 0 :
      sSQL = '''
        SELECT DISTINCT 
          ta.Nomor_Rekening, c.kode_instrumen,
          rt.NAMA_REKENING,
          -(rt.saldo) as saldo, 
          case when rt.status_rekening = 1 then 'AKTIF'
          when rt.status_rekening in (3,99) then 'TUTUP' end as status_rekening,
          ta.kode_produk, rt.kode_valuta,
          pr.nama_produk, 
          ta.tgl_buka,
          ta.tgl_jatuh_tempo,
    
    			-rt.saldo nominal_deal, 
          -(rt.saldo+ta.revaluasi) nilai_tunai,
    			ta.harga_beli,
    			ta.ujroh, -ta.saldo_accrue s_akru,
    			ta.eqv_rate_real, ta.eqv_rate_gross,
    			ta.nisbah,
    			ta.nomor_ref_deal,
          decode(c.tipe_SB, 'N', 'Negara', 'Korporat') tipe_sb,
          --c.klasifikasi_SB, 
          ki.enum_description klasifikasi_SB,
          c.tgl_settlement,
          c.tipe_kupon||' bulan' tipe_kupon,
          c.tgl_bayar_terakhir,
          c.tgl_bayar_berikutnya,
          c.seri_SB,
          c.nominal_pajak,
    			nvl(ta.ppap_umum,0) ppap_umum,
          nvl(ta.ppap_khusus,0) ppap_khusus,
    			rt.saldo+ta.ujroh nominal_gross,
    			tc.nama_counterpart
          , CASE WHEN ta.revaluasi < 0 THEN -(ta.revaluasi+ta.nilai_amortisasi) ELSE 0 END premium
          , CASE WHEN ta.revaluasi > 0 THEN (ta.revaluasi+ta.nilai_amortisasi) ELSE 0 END diskonto
          , CASE WHEN ta.revaluasi < 0 THEN -(ta.nilai_amortisasi) ELSE 0 END premium_amor
          , CASE WHEN ta.revaluasi > 0 THEN (ta.nilai_amortisasi) ELSE 0 END diskonto_amor

          , 0 as balance_revaluasi 
          , 0 as balance_amortisasi
          , 0 as balance_margin 
  
          , 0 as mut_akru
          , 0 as mut_amor_premium
          , 0 as mut_amor_diskonto
          , 0 as mut_kupon
          , nvl(ku.kurs_tengah_bi, 1.0) as rate_kurs
          , nvl(ku.kurs_tengah_bi, 1.0) * -rt.saldo as nominal_ekuivalen
        FROM %(treasuryaccount)s ta
        	INNER JOIN %(rekeningtransaksi)s rt ON ta.nomor_rekening=rt.nomor_rekening
        	INNER JOIN %(treasuratberharga)s c ON ta.nomor_rekening=c.nomor_rekening
    			INNER JOIN %(treaproduk)s pr ON ta.kode_produk=pr.kode_produk
    			INNER JOIN %(treacounterpart)s tc ON ta.kode_counterpart=tc.kode_counterpart
    			left join %(enum_varchar)s ki on ki.enum_value=c.klasifikasi_SB and ki.enum_name='eKlasifikasiSB'
          LEFT JOIN (
                  SELECT d1.currency_code, d1.kurs_tengah_bi 
                 FROM %(kurshistory)s d1,
                    (SELECT currency_code, MAX(history_date) as maxdate
                    FROM %(kurshistory)s WHERE history_date <= %(fTanggalAkhir)s
                    GROUP BY currency_code) d2
                  WHERE d1.currency_code = d2.currency_code
                    AND d1.history_date = d2.maxdate 
              ) ku on ku.currency_code = rt.kode_valuta
          
          %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = rt.kode_cabang AND lc.id_user = %(id_user)s
    	''' % _dict 
    else :
      sSQL = '''
        SELECT 
          hj.kode_instrumen, rt.nama_rekening
          , hj.nomor_rekening
          , ta.tgl_buka
          , hj.tgl_settlement, ta.tgl_jatuh_tempo
          , ta.eqv_rate_real, ta.eqv_rate_gross
          , rt.kode_valuta
          , ta.kode_counterpart, tc.nama_counterpart
          , ta.kode_produk, ta.kode_produk nama_produk
          , ki.enum_description klasifikasi_SB
          , decode(hj.tipe_SB, 'N', 'Negara', 'Korporat') tipe_sb
  
          , -db.balance_saldo as nominal_deal
          , -db.balance_accrue as s_akru
  
          , -(db.balance_saldo+(db.balance_revaluasi+db.balance_amortisasi)) nilai_tunai
          , ta.harga_beli
          , ta.ujroh
  
          , case when db.status_rekening = 1 then 'AKTIF'
            when db.status_rekening in (3,99) then 'TUTUP' end as status_rekening
          , hj.tipe_kupon||' bulan' tipe_kupon
          , hj.tgl_bayar_terakhir
          , hj.tgl_bayar_berikutnya
          , hj.seri_SB
          , hj.nominal_pajak
          , nvl(ta.ppap_umum,0) ppap_umum
          , nvl(ta.ppap_khusus,0) ppap_khusus
  
          , CASE WHEN db.balance_revaluasi < 0 THEN -(db.balance_revaluasi+db.balance_amortisasi) ELSE 0 END premium
          , CASE WHEN db.balance_revaluasi > 0 THEN (db.balance_revaluasi+db.balance_amortisasi) ELSE 0 END diskonto
          , CASE WHEN db.balance_revaluasi < 0 THEN (db.balance_amortisasi) ELSE 0 END premium_amor
          , CASE WHEN db.balance_revaluasi > 0 THEN -(db.balance_amortisasi) ELSE 0 END diskonto_amor
  
          , db.balance_revaluasi 
          , db.balance_amortisasi
          , db.balance_margin 
  
          , (dbm.mut_kredit_accrue - dbm.mut_debit_accrue) mut_akru
          , CASE WHEN db.balance_revaluasi < 0 THEN -(dbm.mut_kredit_amortisasi - dbm.mut_debit_amortisasi) ELSE 0 END mut_amor_premium
          , CASE WHEN db.balance_revaluasi > 0 THEN (dbm.mut_kredit_amortisasi - dbm.mut_debit_amortisasi) ELSE 0 END mut_amor_diskonto
          , (dbm.mut_kredit_margin - dbm.mut_debit_margin) mut_kupon
          , nvl(ku.kurs_tengah_bi, 1.0) as rate_kurs
          , nvl(ku.kurs_tengah_bi, 1.0) * -db.balance_saldo as nominal_ekuivalen
        FROM %(DailyBalanceSukuk)s db
          INNER JOIN %(treasuratberharga)s hj ON db.nomor_rekening=hj.nomor_rekening
          INNER JOIN %(treasuryaccount)s ta ON db.nomor_rekening=ta.nomor_rekening
          INNER JOIN %(rekeningtransaksi)s rt ON db.nomor_rekening=rt.nomor_rekening
          INNER JOIN %(treacounterpart)s tc ON ta.kode_counterpart=tc.kode_counterpart
          INNER JOIN (
            SELECT nomor_rekening, Max(balance_date) max_date, Min(balance_date) min_date
              , Sum(credit_accrue) mut_kredit_accrue, Sum(debit_accrue) mut_debit_accrue
              , Sum(credit_revaluasi) mut_kredit_revaluasi, Sum(debit_revaluasi) mut_debit_revaluasi
              , Sum(credit_amortisasi) mut_kredit_amortisasi, Sum(debit_amortisasi) mut_debit_amortisasi
              , Sum(credit_saldo) mut_kredit_saldo, Sum(debit_saldo) mut_debit_saldo
              , Sum(credit_margin) mut_kredit_margin, Sum(debit_margin) mut_debit_margin
            FROM %(DailyBalanceSukuk)s
            WHERE %(tx_akhir)s %(tx_awal)s
            GROUP BY nomor_rekening
          ) dbm ON db.nomor_rekening=dbm.nomor_rekening AND db.balance_date=dbm.max_date
          LEFT JOIN (
                  SELECT d1.currency_code, d1.kurs_tengah_bi 
                 FROM %(kurshistory)s d1,
                    (SELECT currency_code, MAX(history_date) as maxdate
                    FROM %(kurshistory)s WHERE history_date <= %(fTanggalAkhir)s
                    GROUP BY currency_code) d2
                  WHERE d1.currency_code = d2.currency_code
                    AND d1.history_date = d2.maxdate 
              ) ku on ku.currency_code = rt.kode_valuta
          left join %(enum_varchar)s ki on ki.enum_value=hj.klasifikasi_SB and ki.enum_name='eKlasifikasiSB'
    
          %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = rt.kode_cabang AND lc.id_user = %(id_user)s
    	''' % _dict
    _col = 'H'
    
    sql['SELECTFROMClause'] = sSQL
    sql['WHEREClause'] = '''
      %(strOqlCond)s
    ''' % _dict
    
    sql['GROUPBYClause'] = ''
    sql['keyFieldName'] = 'ta.tgl_buka'
    sql['altOrderFieldNames'] = 'ta.tgl_buka;tc.nama_counterpart'
    sql['baseOrderFieldNames'] = 'ta.tgl_buka;tc.nama_counterpart'
  
  
  elif param.type_report == '2':
    sSQL = '''
      SELECT 
        tr.keterangan_tambahan kd_remark, dt.kode_valuta, dt.kode_account
        , dt.kode_tx_class, dt.nomor_rekening
        , hj.kode_instrumen
        , tr.kode_transaksi
        , tc.kode_counterpart, tc.nama_counterpart
        , Decode(dt.jenis_mutasi,'C',dt.nilai_mutasi,-dt.nilai_mutasi) mutasi_tx
        , tr.tanggal_transaksi, dt.Nomor_Referensi, rt.nama_rekening
        , tr.journal_no
        , tr.keterangan
      FROM %(transaksi)s tr 
        INNER JOIN %(detiltransaksi)s dt on tr.id_transaksi=dt.id_transaksi
        INNER JOIN %(treasuratberharga)s hj ON dt.nomor_rekening=hj.nomor_rekening
        INNER JOIN %(treasuryaccount)s ta on dt.nomor_rekening=ta.nomor_rekening
        INNER JOIN %(rekeningtransaksi)s rt on dt.nomor_rekening=rt.nomor_rekening
        INNER JOIN %(treacounterpart)s tc on tc.kode_counterpart=ta.kode_counterpart
        %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = rt.kode_cabang AND lc.id_user = %(id_user)s
  	''' % _dict
    _col = '3'
     
    sql['SELECTFROMClause'] = sSQL
    sql['WHEREClause'] = '''
      %(strOqlCond)s AND %(tx_akhir)s %(tx_awal)s
    ''' % _dict
    sql['GROUPBYClause'] = ""
    
    sql['keyFieldName'] = 'tr.tanggal_transaksi'
    sql['altOrderFieldNames'] = 'tr.tanggal_transaksi;nama_counterpart'
    sql['baseOrderFieldNames'] = 'tr.tanggal_transaksi;nama_counterpart'
  
  elif param.type_report == '3':
    sSQL = '''
      SELECT 
        dt.kode_tx_class, dtc.deskripsi, dt.kode_valuta, dt.kode_cabang
        , tc.kode_counterpart, tc.nama_counterpart
        , ta.kode_produk
        , sum(Decode(dt.jenis_mutasi,'C',dt.nilai_mutasi,-dt.nilai_mutasi)) mutasi_tx
        , count(1) jml_tx
      FROM %(transaksi)s tr 
        INNER JOIN %(detiltransaksi)s dt on tr.id_transaksi=dt.id_transaksi
        LEFT JOIN %(detiltransaksiclass)s dtc on dtc.kode_tx_class=dt.kode_tx_class
        INNER JOIN %(treasuryaccount)s ta on dt.nomor_rekening=ta.nomor_rekening
        INNER JOIN %(rekeningtransaksi)s rt on dt.nomor_rekening=rt.nomor_rekening
        INNER JOIN %(treacounterpart)s tc on tc.kode_counterpart=ta.kode_counterpart
        %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = rt.kode_cabang AND lc.id_user = %(id_user)s
   	''' % _dict
    _col = 'G'
     
    sql['SELECTFROMClause'] = sSQL
    sql['WHEREClause'] = '''
      %(strOqlCond)s AND %(tx_akhir)s %(tx_awal)s
    ''' % _dict
    sql['GROUPBYClause'] = '''GROUP BY dt.kode_tx_class, dtc.deskripsi, dt.kode_valuta, dt.kode_cabang, tc.kode_counterpart, tc.nama_counterpart, ta.kode_produk'''
    
    sql['keyFieldName'] = 'nama_counterpart'
    sql['altOrderFieldNames'] = 'nama_counterpart;kode_tx_class'
    sql['baseOrderFieldNames'] = 'nama_counterpart;kode_tx_class'
  
  gsModule = modman.getModule(config, GRID_ACCOUNT)
  sql['columnSetting'] = gsModule.cols_setting(param.jenis_treasuryaccount, param.type_report)
  sql['r_tgl'] = r_tgl
  #sql['excel_cols']= EXCEL_COLUMN
  #sql['judul2']= judul2

  return sql
#--
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def getQueryData(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()

  sqlStat = getSQLClient(config, clientpacket)
  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('1...')
  '''
  sSQL = sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause']+' GROUP BY '+sqlStat['GROUPBYClause']
  app.ConWriteln(sSQL)
  app.ConRead('')
  #raise Exception, sqlStat['SELECTFROMClause']+sqlStat['WHEREClause']
  #'''
  try :                        
    app.ConWriteln('2...')
    rqsql = remotequery.RQSQL(config)   
    app.ConWriteln('3...')
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '') 
    app.ConWriteln('4...')
    app.ConWriteln('...Please Wait...')
    rqsql.initOperation(returnpacket)
    app.ConWriteln('5...')

    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#--  

#========================= Export Data===================================#
def styleCell(owb, R_, C_):
  #style border
  owb.cell(row=R_,column=C_).style.font.name = 'Trebuchet MS'
  owb.cell(row=R_,column=C_).style.font.size = 10
  owb.cell(row=R_,column=C_).style.borders.top.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.bottom.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.right.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.left.border_style = Border.BORDER_THIN
#--

def NumberFormat(owb, R_, C_):
  owb.cell(row=R_,column=C_).style.number_format.format_code = '#,##0.00�'
#--

def GenerateFile(config, params, returns):
  sqlStat = getSQLClient(config, params)
  try:
    status = returns.CreateValues(
      ['sucsess',0],
      ['Is_Err',0],['Err_Message','']
    )
    
    rec = params.FirstRecord
    oRec = atutil.GeneralObject(rec)    
    app = config.AppObject
    app.ConCreate('out')
    app.ConWriteln('Inisialisasi proses generate template...')
          
    if rec.type_report == '1':
      reportFile = GenerateReportRekap(config, sqlStat, rec.type_report)
    else:
      reportFile = GenerateReportRekap(config, sqlStat, rec.type_report)
     
    #return packet
    mmtype = reportFile.split(".")[-1]
    sw = returns.AddStreamWrapper()
    sw.name = 'gen_report'
    sw.LoadFromFile(reportFile)                       
    if mmtype=='xlsx':
      sw.MIMEType = ".%s" % mmtype
    else:
     sw.MIMEType = config.AppObject.GetMIMETypeFromExtension(reportFile)
  except:
    status.Is_Err = True
    status.Err_Message = _dbg_excmsg()#str(sys.exc_info()[1])
    
def GenerateReportRekap(config, sql, _col='2'):    
    app = config.AppObject
    app.ConCreate('out')
    app.ConWriteln('Inisialisasi proses generate template...') 
     
    fileNamePath = config.HomeDir + 'template\\tpl_fasbis_int.xlsx'
    
    wb = openpyxl.load_workbook(fileNamePath)
    wsExcel = wb.get_sheet_by_name('data')
    
    order_by = sql['baseOrderFieldNames'].replace(';',',')
    sSQL = sql['SELECTFROMClause']+' WHERE '+sql['WHEREClause']+sql['GROUPBYClause']+' ORDER BY '+order_by
    '''
    if config.SecurityContext.UserID=='IBOPR':
      app.ConWriteln(sSQL)
      app.ConRead('')
    #'''
    
    app.ConWriteln('...Please Wait...')
    q = config.CreateSQL(sSQL).RawResult
    q.First()
    
    gsModule = modman.getModule(config, GRID_ACCOUNT)
    _cols = gsModule.ex_cols('B', _col)    
        
    #header  
    wsExcel.cell(row=0,column=0).value = 'LAPORAN DETAIL PENEMPATAN SURAT BERHARGA'
    wsExcel.cell(row=1,column=0).value = sql['r_tgl']
    wsExcel.cell(row=2,column=0).value = 'TREASURY' 
    
    startLine = 4
    styleCell(wsExcel, startLine, 0)
    wsExcel.cell(row=startLine,column=0).style.font.bold = True
    wsExcel.cell(row=startLine,column=0).style.alignment.wrap_text= True
    wsExcel.cell(row=startLine,column=0).style.alignment.horizontal = "center"
    wsExcel.cell(row=startLine,column=0).style.alignment.vertical = "center"
    wsExcel.cell(row=startLine,column=0).value = 'NO.'  
    wsExcel.column_dimensions['A'].width = 6
    
    _rCol = 1
    for i in range(len(_cols)):
      ls = _cols[i][0].split(";") 
      styleCell(wsExcel, startLine, _rCol)
      wsExcel.cell(row=startLine,column=_rCol).style.font.bold = True 
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.wrap_text= True
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.horizontal = "center"
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.vertical = "center"
      wsExcel.cell(row=startLine,column=_rCol).value = '%s' % (ls[1])
      _rCol += 1
             
    _rRow = 5
    _no = 1
    while not q.Eof:   
      _rCol = 1
      styleCell(wsExcel, _rRow, 0)
      wsExcel.cell(row=_rRow,column=0).style.alignment.horizontal = "center"
      wsExcel.cell(row=_rRow,column=0).value = _no 
      for i in _cols:
        ls = i[0].split(";")
        #'''
        if i[1]=='D':
         tgl = eval('q.'+ls[0])
         nVal = "%s-%s-%s" %(str(tgl[0]).zfill(4), str(tgl[1]).zfill(2), str(tgl[2]).zfill(2)) if tgl<>None else ''
        else:
         nVal = eval('q.'+ls[0])
        #'''
        #nVal = eval('q.'+ls[0])
        _col = get_column_letter(_rCol+1)
        styleCell(wsExcel, _rRow, _rCol)
        wsExcel.cell(row=_rRow,column=_rCol).value = nVal or '' 
        #wsExcel.column_dimensions[_col].width = i[2]
        
        if i[1]=='F':
          wsExcel.cell(row=_rRow,column=_rCol).style.number_format.format_code = '#,##0.00�'
            
        _rCol += 1
      
      _no += 1  
      _rRow += 1        
      q.Next()
          
    fileHasil = config.UserHomeDirectory + 'reportfasbis.xlsx'    
    fileutils.SafeDeleteFile(fileHasil) #Melakukan Hapus File yang sudah ada sebelumnya
    wb.save(fileHasil)
    
    return fileHasil