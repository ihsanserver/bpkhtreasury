import com.ihsan.foundation.pobjecthelper as phelper
import sys

def DAFScriptMain(config, parameter, returnpacket):
    status =  returnpacket.CreateValues(
       ['Is_Err',0],
       ['Err_Message',''],

    )    
    recInput = parameter.FirstRecord                                                
    

    config.BeginTransaction()
    try:      
      UserActivityLog(config, recInput.event_description, recInput.info1, recInput.info2) 
                      
      config.Commit()
    except:
      config.Rollback()
      status.Is_Err = 1
      status.Err_Message = str(sys.exc_info()[1])
    #--

def UserActivityLog(config, event_description, info1, info2):
    helper = phelper.PObjectHelper(config) 
    helper.CreatePObject('Enterprise.UserAudit', {
        'event_description' : event_description 
        , 'info1': info1 
        , 'info2': info2
    })
