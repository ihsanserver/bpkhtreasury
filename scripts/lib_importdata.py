import math
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import os, pyFlexcel
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.modman as modman
import xlrd
import types, re

def getTemplateDir(config):
   return config.HomeDir + 'template\\'

modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

gFileCounter = 0
EPSILON = 0.01

def getCols(jns_rpt):
  _cols = {}
  if jns_rpt in ['TRU']:
    _cols = {
      2:['kode_jenis','psd','S','Jenis Rekening',90],
      3:['nomor_rekening','psd','S','Nomor Rekening',120],
      4:['jenis_mutasi','psd','S','Jenis Mutasi',90],
      5:['nilai_mutasi','psd','F','Nilai Mutasi',120],
      6:['kode_cabang','psd','S','Kode Cabang',100],
      7:['kode_valuta','psd','S','Kode Valuta',80],
      8:['kode_tx_class','psd','S','Kode Tx Class',100]
    }
  else:
    raise Exception, 'jenis akad belum terdefinisi..'
  
  return _cols

def cekLine(jns_rpt):
  if jns_rpt in ['TRU']:
    _ReqCol = 3
    _startLine = 3
  else:
    raise Exception, 'jenis jadwal belum terdefinisi..'
  
  return _startLine, _ReqCol

def UploadData(config,params,returns):
  status = returns.CreateValues(
    ['sucsess',''],
    ['Is_Err',0],['Err_Message','']
  )
  rec = params.uipTrx.GetRecord(0)
   
  UploadTrx = modman.getModule(config,'UploadTrx')
  _cols = eval(rec._cols)
  returns.AddDataPacketStructureEx('upl_data',
      ';'.join(_cols)
  )

  returns.BuildAllStructure()
  
  if params.StreamWrapperCount > 0:
    sw = params.GetStreamWrapper(0)
  else:
    status.Is_Err = 1
    status.Err_Message = 'Download stream not found'
    return
  #--

  fileNamePath = config.HomeDir + 'template\\tmp_upl\\upl_editJadwal.xls'
  sw.SaveToFile(fileNamePath)
  wb = xlrd.open_workbook(fileNamePath)
  oSheet = wb.sheet_by_name('data')
      
  _startLine, _ReqCol = cekLine('TRU')
  _checkLine = _startLine
  required_field = oSheet.cell_value(_checkLine,_ReqCol)

  config.BeginTransaction()
  try:    
    upl_data = returns.AddNewDataset('upl_data')
    dCols = getCols('TRU')
    _exp_temp = dCols.keys()
    _exp_temp.sort()

    num_rows = oSheet.nrows
    while required_field not in [None, ' ',''] and _checkLine < num_rows:
      required_field = oSheet.cell_value(_checkLine,_ReqCol)      
      if required_field not in [None, ' ','']:
        data = upl_data.AddRecord()
        for _col in _exp_temp:
          ls = dCols[_col]           
          nVal = UploadTrx.ValidateImp(oSheet.cell_value(_checkLine,_col-1),ls[2], config)
            
          data.SetFieldByName(ls[0], nVal)

        if data.kode_jenis=='GL':
          data.kode_account = data.nomor_rekening
          data.nomor_rekening = "%s-%s-%s" % (data.kode_account, data.kode_cabang, data.kode_valuta)
          
        nama_rekening, kode_produk = CekAccount(config, data.nomor_rekening, data.kode_jenis)
        data.nama_rekening = nama_rekening

        if data.kode_jenis<>'GL':
          kode_tx, kode_account = GetTxAccount(config, data.kode_jenis, data.kode_tx_class, kode_produk)
          data.kode_account = kode_account
          data.kode_tx = kode_tx
        #raise Exception, kode_tx 
      _checkLine += 1      
      #--
    #--endwhile
    config.Commit()
    status.sucsess = 'T'
  except:
    config.Rollback()
    status.sucsess = 'F'
    status.Is_Err = 1
    status.Err_Message = str(sys.exc_info()[1])+'  last line %s  %s ' % (_checkLine, required_field)
    
    return
  #--
  
def CekAccount(config, nomor_rekening, kode_jenis):
  kode_produk = ''
  sSQL ='''
    SELECT a.nama_rekening, b.kode_produk 
    FROM %(rekeningtransaksi)s a
    LEFT JOIN %(treasuryaccount)s b ON a.nomor_rekening=b.nomor_rekening
    WHERE a.nomor_rekening='%(nomor_rekening)s'
  ''' % {
    'rekeningtransaksi': config.MapDBTableName("rekeningtransaksi"),
    'treasuryaccount': config.MapDBTableName("treasuryaccount"),
    'nomor_rekening': nomor_rekening
  }  
  oRek = config.CreateSQL(sSQL).RawResult
  if oRek.Eof:
    raise Exception, 'Nomor Rekening '+nomor_rekening+' Tidak Terdaftar'
  else:
    nama_rekening = oRek.nama_rekening
    kode_produk   = oRek.kode_produk
    #'''
  return nama_rekening, kode_produk
#---

def GetTxAccount(config, kode_jenis, kode_tx_class, kode_produk):
  if kode_jenis in ['', None]:
    raise Exception, 'Jenis Rekening Harus isi!'

  if kode_tx_class in ['', None]:
    raise Exception, 'Untuk Jenis Rekening %s, Kode Tx Class Harus isi!'
    
  sSQL ='''
    SELECT b.kode_tx_class, b.deskripsi, a.kode_account 
    FROM %(glinterface)s a
    INNER JOIN %(detiltransaksiclass)s b ON a.kode_interface=kode_tx_class
    WHERE a.kode_produk='%(kode_produk)s' AND b.kode_tx_class='%(kode_tx_class)s'
  ''' % {
    'glinterface': config.MapDBTableName("glinterface"),
    'detiltransaksiclass': config.MapDBTableName("detiltransaksiclass"),
    'kode_tx_class': kode_tx_class, 'kode_produk': kode_produk 
  }  
  rSQL = config.CreateSQL(sSQL).RawResult
    
  return rSQL.deskripsi, rSQL.kode_account
#---
      