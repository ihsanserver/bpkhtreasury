'''
Fungsi ini digunakan untuk generate file report data jemaah by name by porsi addres 
dan summary per bank bulanan

'''
import sys, os
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman
import com.ihsan.foundation.appserver as appserver
import csv

# GLOBALS
config  = appserver.ActiveConfig
app     = config.AppObject
_CONSOLE = False

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  
  global _CONSOLE
  
  _CONSOLE = False
  # generate report sum per bank
  GenerateFile_xlsx(config, 'sum')
    
  returns.CreateValues(['Is_Err', 0])

  return 1

def getSQL(config, tgl_rep, jns_rep):
  mlu = config.ModLibUtils
  
  #prevDate = config.FormatDateTime('dd-mmm-yyyy', tgl_rep)
  #strDate = config.FormatDateTime('01-mmm-yyyy', tgl_rep)
  periode = config.FormatDateTime('yyyy-mm', tgl_rep)
  dictParam = {
    #'tgl_awal': mlu.QuotedStr(strDate),
    #'tgl_akhir': mlu.QuotedStr(prevDate),
    'periode': mlu.QuotedStr(periode),
  }

  dictParam.update(dbutil.mapDBTableNames(config, 
    [
      'rekeningtransaksi'
      , 'treasuryaccount'            
      , 'treacounterpart'
      , 'enterprise.cabang'
      , 'transaksi'
      , 'histtransaksi'            
      , 'smt.tmp_transaksijamaah'
      , 'smt.tmp_trea_transaksi'
      , 'treaproduk' 
      , 'smt.trx_mapping'          
    ])
  )

  sSQL=''
  if jns_rep=='jemaah':
    sSQL = ''' 
      select 
      	KD_BANK
      	, NAMA_BANK
      	, NOREK_BANK
      	, JENIS_DANA
      	, NO_REKENING
      	, NAMA_JEMAAH
      	, TANGGAL_TRANSAKSI
      	, KD_REMARK
      	, NAMA_CABANG
      	, KODE_VALUTA
      	, NOMINAL_TRANSAKSI
      	, JNS_MUTASI
      	, KETERANGAN
      FROM (
      	SELECT 
      	  tc.kode_counterpart kd_bank, tc.nama_counterpart nama_bank
      	  , smt.nomor_rekening_debet AS norek_bank, pr.nama_produk AS JENIS_DANA
      	  , smt.NOMOR_REF_DEAL AS NO_REKENING, smt.NAMA_REKENING AS NAMA_JEMAAH 
      	  , tr.tanggal_transaksi
      	  , smt.jenis_transaksi AS kd_remark
      	  , c.nama_cabang, smt.kode_valuta
      	  , smt.nominal_transaksi
      	  , smt.jns_mutasi
      	  , tr.KETERANGAN
      	FROM {transaksi} tr 
      	  INNER JOIN {tmp_transaksijamaah} smt on tr.id_transaksi=smt.id_transaksi
      	  INNER JOIN {treasuryaccount} ta on ta.nomor_rekening = smt.nomor_rekening_debet
      	  INNER JOIN {treaproduk} pr ON pr.KODE_PRODUK = ta.KODE_PRODUK 
      	  INNER JOIN {treacounterpart} tc on tc.kode_counterpart=ta.kode_counterpart
      	  INNER JOIN {cabang} c on c.kode_cabang=smt.kode_cabang
      	  INNER JOIN {trx_mapping} m on m.kode_remark=smt.jenis_transaksi
      	WHERE  
      	  to_char(tr.tanggal_transaksi, 'YYYY-MM') = {periode}
      
      	UNION ALL
      
      	SELECT 
      	  tc.kode_counterpart kd_bank, tc.nama_counterpart nama_bank
      	  , smt.nomor_rekening_debet AS norek_bank, pr.nama_produk AS JENIS_DANA
      	  , smt.NOMOR_REF_DEAL AS NO_REKENING, smt.NAMA_REKENING AS NAMA_JEMAAH 
      	  , tr.tanggal_transaksi
      	  , smt.jenis_transaksi AS kd_remark
      	  , c.nama_cabang, smt.kode_valuta
      	  , smt.nominal_transaksi
      	  , smt.jns_mutasi
      	  , tr.KETERANGAN
      	FROM {histtransaksi} tr 
      	  INNER JOIN {tmp_transaksijamaah} smt on tr.id_transaksi=smt.id_transaksi
      	  INNER JOIN {treasuryaccount} ta on ta.nomor_rekening = smt.nomor_rekening_debet
      	  INNER JOIN {treaproduk} pr ON pr.KODE_PRODUK = ta.KODE_PRODUK 
      	  INNER JOIN {treacounterpart} tc on tc.kode_counterpart=ta.kode_counterpart
      	  INNER JOIN {cabang} c on c.kode_cabang=smt.kode_cabang
      	  INNER JOIN {trx_mapping} m on m.kode_remark=smt.jenis_transaksi
      	WHERE  
      	  to_char(tr.tanggal_transaksi, 'YYYY-MM') = {periode}
      ) x
      ORDER BY x.KD_BANK, x.KD_REMARK
    '''.format(**dictParam)

  elif jns_rep=='nonjemaah':
    sSQL = ''' 
      select 
      	KD_BANK
      	, NAMA_BANK
      	, NOREK_BANK
      	, JENIS_DANA
      	, TANGGAL_TRANSAKSI
      	, KODE_REMARK
      	, NAMA_CABANG
      	, KODE_VALUTA
      	, NOMINAL
      	, JENIS_MUTASI
      	, KETERANGAN
      FROM (
      	SELECT 
      	  tc.kode_counterpart kd_bank, tc.nama_counterpart nama_bank
      	  , smt.nomor_rekening AS norek_bank, pr.nama_produk AS jenis_dana
      	  , tr.tanggal_transaksi
      	  , smt.kode_remark
      	  , c.nama_cabang, smt.kode_valuta
      	  , smt.nominal
      	  , smt.jenis_mutasi
      	  , tr.KETERANGAN
      	FROM {transaksi} tr 
      	  INNER JOIN {tmp_trea_transaksi} smt on tr.id_transaksi=smt.id_transaksi
      	  INNER JOIN {treasuryaccount} ta on ta.nomor_rekening = smt.nomor_rekening
      	  INNER JOIN {treaproduk} pr ON pr.KODE_PRODUK = ta.KODE_PRODUK 
      	  INNER JOIN {treacounterpart} tc on tc.kode_counterpart=ta.kode_counterpart
      	  INNER JOIN {cabang} c on c.kode_cabang=smt.kode_cabang
      	  INNER JOIN {trx_mapping} m on m.kode_remark=smt.kode_remark
      	WHERE  
      	  to_char(tr.tanggal_transaksi, 'YYYY-MM') = {periode}
      
      	UNION ALL
      
      	SELECT 
      	  tc.kode_counterpart kd_bank, tc.nama_counterpart nama_bank
      	  , smt.nomor_rekening AS norek_bank, pr.nama_produk AS jenis_dana
      	  , tr.tanggal_transaksi
      	  , smt.kode_remark
      	  , c.nama_cabang, smt.kode_valuta
      	  , smt.nominal
      	  , smt.jenis_mutasi
      	  , tr.KETERANGAN
      	FROM {histtransaksi} tr 
      	  INNER JOIN {tmp_trea_transaksi} smt on tr.id_transaksi=smt.id_transaksi
      	  INNER JOIN {treasuryaccount} ta on ta.nomor_rekening = smt.nomor_rekening
      	  INNER JOIN {treaproduk} pr ON pr.KODE_PRODUK = ta.KODE_PRODUK 
      	  INNER JOIN {treacounterpart} tc on tc.kode_counterpart=ta.kode_counterpart
      	  INNER JOIN {cabang} c on c.kode_cabang=smt.kode_cabang
      	  INNER JOIN {trx_mapping} m on m.kode_remark=smt.kode_remark
      	WHERE  
      	  to_char(tr.tanggal_transaksi, 'YYYY-MM') = {periode}
      ) x
      ORDER BY x.KD_BANK, x.KODE_REMARK
    '''.format(**dictParam)
  
  return sSQL

def getCols(config, jns_rep):
  _cols = {}
  if jns_rep=='jemaah':
    _cols = {
      2:['KD_BANK', 'S'],
      3:['NAMA_BANK', 'S'],
      4:['NOREK_BANK', 'S'],
      5:['JENIS_DANA', 'S'],
      6:['NO_REKENING', 'S'],
      7:['NAMA_JEMAAH', 'S'],
      8:['TANGGAL_TRANSAKSI', 'D'],
      9:['KD_REMARK', 'S'],
      10:['NAMA_CABANG', 'S'],
      11:['KODE_VALUTA', 'S'],
      12:['NOMINAL_TRANSAKSI', 'F'],
      13:['JNS_MUTASI', 'S'],
      14:['KETERANGAN', 'S'],
    }
    
  if jns_rep=='nonjemaah':
    _cols = {
      2:['KD_BANK', 'S'],
      3:['NAMA_BANK', 'S'],
      4:['NOREK_BANK', 'S'],
      5:['JENIS_DANA', 'S'],
      6:['TANGGAL_TRANSAKSI', 'D'],
      7:['KODE_REMARK', 'S'],
      8:['NAMA_CABANG', 'S'],
      9:['KODE_VALUTA', 'S'],
      10:['NOMINAL', 'F'],
      11:['JENIS_MUTASI', 'S'],
      12:['KETERANGAN', 'S'],
    }
  
  return _cols
#--

  
def GenReport(config, fl_date, ResultDir, jns_rep):
  app = config.AppObject
  jenis_r = {
     'jemaah':['trx_remark_jemaah', 'DETAIL TRANSAKSI REMARK JEMAAH', 3],
     'nonjemaah':['trx_remark_nonjemaah', 'DETAIL TRANSAKSI REMARK NON JEMAAH', 3]
  }
  printOut('Inisialisasi proses generate %s...' % jenis_r[jns_rep][1])
  sSQL = getSQL(config, fl_date, jns_rep)
  
  periode = config.FormatDateTime('yyyy-mm', fl_date)
  periode2 = config.FormatDateTime('yyyymmdd', fl_date)

  excel_cols = getCols(config, jns_rep)  
  _cols = excel_cols.keys()
  _cols.sort()
      
  file_ke = 1
  sBaseFileName = '%s/%s_%s_%s.csv' % (ResultDir, jenis_r[jns_rep][0], periode2, file_ke)
  headerText = '%s PERIODE - %s' % (jenis_r[jns_rep][1], periode)
    
  app.ConWriteln('open template')
  wr = openTemplate(sBaseFileName, headerText, excel_cols)
  app.ConWriteln('done..open template')
      
  app.ConWriteln('query %s to database..' % headerText)
  app.ConWriteln(sSQL) 
  resQ = config.CreateSQL(sSQL).rawresult
  resQ.First()
  app.ConWriteln('done query ,start write to file..')
  i = 1
  while not resQ.Eof:
    datarows=[]
    datarows.append(i)  
    tglIndo = modman.getModule(config,'TglIndo')
    for _col in excel_cols:
      dCol = excel_cols[_col]
      n_col=dCol[0]

      if dCol[1]=='D':
        tgl = eval('resQ.'+n_col)
        v_kolom = tglIndo.tgl_indo(config,eval('resQ.'+n_col),10,1) if tgl<>None else ''
      else:
        v_kolom = eval('resQ.%s' % n_col)

      _column = _col-1

      if dCol[1]=='S' and v_kolom not in [None, '']:
        datarows.append(v_kolom)
        
      elif dCol[1]=='F':
        datarows.append(v_kolom)

      else:
        datarows.append(v_kolom)
  
    wr.writerow(datarows)
    if (i % 100000)==0:
      app.ConWriteln('baris ke %s done..' % i)

    i += 1
    if (i % 500000)==0:
      app.ConWriteln('***save file ke %s..' % file_ke)
      file_ke += 1
      sBaseFileName = '%s/%s_%s_%s.csv' % (ResultDir, jenis_r[jns_rep][0], periode2, file_ke)
      #open new file
      wr = openTemplate(sBaseFileName, '%s (%s)' % (headerText, file_ke), excel_cols)
                                    
    resQ.Next()
  
def openTemplate(sBaseFileName, headerText, excel_cols):    
  wr = csv.writer(open(sBaseFileName, 'w'), lineterminator='\n')
  wr.writerow([headerText])
  h2=['NO']
  _cols = excel_cols.keys()
  _cols.sort()
  for _col in excel_cols:
    dCol = excel_cols[_col]
    h2.append(dCol[0])

  wr.writerow(h2)
  
  return wr

def GenerateFile(config, fl_date, ResultDir):
  #rep jemaah
  GenReport(config, fl_date, ResultDir, 'jemaah')
  #rep non jemaah
  GenReport(config, fl_date, ResultDir, 'nonjemaah')
  #----
  
  
  