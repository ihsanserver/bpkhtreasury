'''
Fungsi ini digunakan untuk generate file report data jemaah by name by porsi addres 
dan summary per bank bulanan

'''
import sys, os
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman
import com.ihsan.foundation.appserver as appserver
import openpyxl
from openpyxl.cell import get_column_letter
from openpyxl.styles import Border
from openpyxl import Workbook
import com.ihsan.fileutils as fileutils

# GLOBALS
config  = appserver.ActiveConfig
app     = config.AppObject
_CONSOLE = False

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  
  global _CONSOLE
  
  _CONSOLE = False
  # generate report sum per bank
  GenerateFile_xlsx(config, 'sum')
    
  returns.CreateValues(['Is_Err', 0])

  return 1

def getSQL(config, tgl_rep):
  mlu = config.ModLibUtils
  
  prevDate = config.FormatDateTime('dd-mmm-yyyy', tgl_rep)
  strDate = config.FormatDateTime('01-mmm-yyyy', tgl_rep)
  dictParam = {
    'tgl_awal': mlu.QuotedStr(strDate),
    'tgl_akhir': mlu.QuotedStr(prevDate),
    'dailybalance_haji':config.MapDBTableName("dailybalance_haji"),
    'fl_date': tgl_rep
  }

  sSQL = ''' 
    select 
    	dbh.kode_bank
    	, dbh.bank 
    	, sum(decode(dbh.jenis_jemaah,'HA.REG',1,0)) reg_jml
    	, sum(decode(dbh.jenis_jemaah,'HA.REG',dbh.jml_set_awal,0)) reg_nom_sa
    	, sum(decode(dbh.jenis_jemaah,'HA.REG',dbh.jml_pelunasan,0)) reg_nom_sl
    	, sum(decode(dbh.jenis_jemaah,'HA.REG',dbh.nilai_va,0)) reg_nom_va
    	, sum(decode(dbh.jenis_jemaah,'HA.REG',dbh.mut_batal_sa,0)) reg_nom_batal_sa
    	, sum(decode(dbh.jenis_jemaah,'HA.REG',dbh.mut_batal_sl,0)) reg_nom_batal_sl
    	, sum(decode(dbh.jenis_jemaah,'HA.REG',dbh.mut_batal_val,0)) reg_nom_batal_val
    	--
    	, sum(decode(dbh.jenis_jemaah,'HA.KHUSUS',1,0)) khs_jml
    	, sum(decode(dbh.jenis_jemaah,'HA.KHUSUS',dbh.jml_set_awal,0)) khs_nom_sa
    	, sum(decode(dbh.jenis_jemaah,'HA.KHUSUS',dbh.jml_pelunasan,0)) khs_nom_sl
    	, sum(decode(dbh.jenis_jemaah,'HA.KHUSUS',dbh.nilai_va,0)) khs_nom_va
    	, sum(decode(dbh.jenis_jemaah,'HA.KHUSUS',dbh.mut_batal_sa,0)) khs_nom_batal_sa
    	, sum(decode(dbh.jenis_jemaah,'HA.KHUSUS',dbh.mut_batal_sl,0)) khs_nom_batal_sl
    	, sum(decode(dbh.jenis_jemaah,'HA.KHUSUS',dbh.mut_batal_val,0)) khs_nom_batal_val
    FROM {dailybalance_haji} dbh
    INNER JOIN (
      SELECT GENERATED_KEY, MAX(PERIODE) PERIODE FROM {dailybalance_haji}
      WHERE PERIODE <= TO_DATE({tgl_akhir}) GROUP BY GENERATED_KEY
    ) cdb ON dbh.GENERATED_KEY=cdb.GENERATED_KEY AND cdb.PERIODE=dbh.PERIODE
    group by dbh.kode_bank, dbh.bank
    ORDER BY dbh.kode_bank
  '''.format(**dictParam)

  
  return sSQL

def getCols(config):
  _cols = {
    2:['KODE_BANK', 'S'],
    3:['BANK', 'S'],
    4:['REG_JML', 'I'],
    5:['REG_NOM_SA', 'F'],
    6:['REG_NOM_SL', 'F'],
    7:['REG_NOM_VA', 'F'],
    8:['REG_NOM_BATAL_SA', 'F'],
    9:['REG_NOM_BATAL_SL', 'F'],
    10:['REG_NOM_BATAL_VAL', 'F'],
    11:['KHS_JML', 'F'],
    12:['KHS_NOM_SA', 'F'],
    13:['KHS_NOM_SL', 'F'],
    14:['KHS_NOM_VA', 'F'],
    15:['KHS_NOM_BATAL_SA', 'F'],
    16:['KHS_NOM_BATAL_SL', 'F'],
    17:['KHS_NOM_BATAL_VAL', 'F'],
  }
  
  return _cols
#--

  
def GenerateFile(config, fl_date, ResultDir):
  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('Inisialisasi proses generate summary jemaah per bank...')
  sSQL = getSQL(config, fl_date)
  periode = config.FormatDateTime('dd-mmm-yyyy', fl_date)
  periode2 = config.FormatDateTime('yyyymmdd', fl_date)

  fileNamePath = config.HomeDir + 'template\\tpl_summary_jemaah.xlsx'
  headerText = 'SUMMARY JEMAAH HAJI PER BANK PER - '+periode    
  wb, wsExcel = openTemplate(fileNamePath, headerText)

  excel_cols = getCols(config)  
  _cols = excel_cols.keys()
  _cols.sort()
      
  row = 4
  app.ConWriteln('query %s to database..' % headerText)
  app.ConWriteln(sSQL) 
  resQ = config.CreateSQL(sSQL).rawresult
  resQ.First()
  app.ConWriteln('done query ,start write to file..')
  i = 1
  file_ke = 1
  while not resQ.Eof:
    wsExcel.cell(row=row,column=0).value = i
    #styleCell(wsExcel, row, 0)  
    tglIndo = modman.getModule(config,'TglIndo')
    for _col in excel_cols:
      dCol = excel_cols[_col]
      n_col=dCol[0]

      if dCol[1]=='D':
        tgl = eval('resQ.'+n_col)
        v_kolom = tglIndo.tgl_indo(config,eval('resQ.'+n_col),2,1) if tgl<>None else ''
      else:
        v_kolom = eval('resQ.%s' % n_col)

      _column = _col-1

      if dCol[1]=='S' and v_kolom not in [None, '']:
        wsExcel.cell(row=row,column=_column).set_explicit_value(v_kolom, data_type="s")
        
      elif dCol[1]=='F':
        wsExcel.cell(row=row,column=_column).value = v_kolom

      else:
        wsExcel.cell(row=row,column=_column).value = v_kolom
  
    row += 1
    if (i % 10000)==0:
      app.ConWriteln('baris ke %s done..' % file_ke)

    if (i % 500000)==0:
      app.ConWriteln('***save file ke %s..' % i)
      sBaseFileName = '%s/sum_jemaah_%s_%s.xlsx' % (ResultDir, periode2, file_ke)
      #save file 
      saveFile(wb, sBaseFileName)
      #open new file
      wb, wsExcel = openTemplate(fileNamePath, '%s (%s)' % (headerText, file_ke))
      row = 4
      file_ke += 1

    i += 1
                                    
    resQ.Next()
  
    #last save file
    sBaseFileName = '%s/sum_jemaah_%s_%s.xlsx' % (ResultDir, periode2, file_ke)
    saveFile(wb, sBaseFileName)

def saveFile(wb, sBaseFileName):
  #ResultDir = 'c:/dafapp/rawdata/jemaah/'
  fileHasil = sBaseFileName
  #app.ConWriteln('file hasil >>'+fileHasil)
  
  fileutils.SafeDeleteFile(fileHasil) #Melakukan Hapus File yang sudah ada sebelumnya
  wb.save(fileHasil)    
  
def openTemplate(fileNamePath, headerText):    
  wb = openpyxl.load_workbook(fileNamePath)
  wsExcel = wb.get_sheet_by_name('data')
  wsExcel.cell(row=0,column=1).value = headerText
  
  return wb, wsExcel

  
  