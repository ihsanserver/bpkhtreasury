'''
Fungsi ini digunakan untuk generate file report data jemaah by name by porsi addres 
dan summary per bank bulanan

'''
import sys, os
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman
import com.ihsan.foundation.appserver as appserver
import csv

# GLOBALS
config  = appserver.ActiveConfig
app     = config.AppObject
_CONSOLE = False

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  
  global _CONSOLE
  
  _CONSOLE = False
  
  # generate report detail jemaah
  GenerateFile(config)
  
  returns.CreateValues(['Is_Err', 0])

  return 1

def getSQL(config, tgl_rep):
  mlu = config.ModLibUtils
  
  prevDate = config.FormatDateTime('dd-mmm-yyyy', tgl_rep)
  strDate = config.FormatDateTime('01-mmm-yyyy', tgl_rep)
  dictParam = {
    'tgl_awal': mlu.QuotedStr(strDate),
    'tgl_akhir': mlu.QuotedStr(prevDate),
    'dailybalance_haji':config.MapDBTableName("dailybalance_haji"),
    'fl_date': tgl_rep
  }


  sSQL = ''' 
    select 
    	NO_REKENING_BANK
    	, NOMOR_PORSI
    	, NOMOR_VALIDASI
    	, NAMA_JEMAAH
    	, NAMA_AYAH
    	, NO_IDENTITAS
    	, TEMPAT_LAHIR
    	, TGL_LAHIR
    	, JENIS_KELAMIN
    	, ALAMAT
    	, NAMA_DESA
    	, KABUPATEN
    	, PROVINSI
    	, NO_HP
    	, TGL_SET_AWAL
    	, JML_SET_AWAL
    	, NILAI_VA
    	, TGL_PELUNASAN
    	, JML_PELUNASAN
    	, JENIS_JEMAAH
    	, KODE_BANK
    	, BANK
    	, TGL_KONFIRMASI
    FROM {dailybalance_haji} dbh
    INNER JOIN (
      SELECT GENERATED_KEY, MAX(PERIODE) PERIODE FROM {dailybalance_haji}
      WHERE PERIODE <= TO_DATE({tgl_akhir}) GROUP BY GENERATED_KEY
    ) cdb ON dbh.GENERATED_KEY=cdb.GENERATED_KEY AND cdb.PERIODE=dbh.PERIODE
    ORDER BY JENIS_JEMAAH, KODE_BANK ,TGL_SET_AWAL
    --FETCH FIRST 100000 ROWS ONLY
  '''.format(**dictParam)
  
  return sSQL

def getCols(config):
  _cols = {
    2:['NO_REKENING_BANK', 'S'],
    3:['NOMOR_PORSI', 'S'],
    4:['NOMOR_VALIDASI', 'S'],
    5:['NAMA_JEMAAH', 'S'],
    6:['NAMA_AYAH', 'S'],
    7:['NO_IDENTITAS', 'S'],
    8:['TEMPAT_LAHIR', 'S'],
    9:['TGL_LAHIR', 'D'],
    10:['JENIS_KELAMIN', 'S'],
    11:['ALAMAT', 'S'],
    12:['NAMA_DESA', 'S'],
    13:['KABUPATEN', 'S'],
    14:['PROVINSI', 'S'],
    15:['NO_HP', 'S'],
    16:['TGL_SET_AWAL', 'D'],
    17:['JML_SET_AWAL', 'F'],
    18:['NILAI_VA', 'F'],
    19:['TGL_PELUNASAN', 'D'],
    20:['JML_PELUNASAN', 'F'],
    21:['JENIS_JEMAAH', 'S'],                   
    22:['KODE_BANK', 'S'],
    23:['BANK', 'S'],
    24:['TGL_KONFIRMASI', 'D'],
  }
  
  return _cols
#--
    
def GenerateFile(config, fl_date, ResultDir):
  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('Inisialisasi proses generate template...')
  sSQL= getSQL(config, fl_date)
  periode = config.FormatDateTime('dd-mmm-yyyy', fl_date)
  periode2 = config.FormatDateTime('yyyymmdd', fl_date)

  excel_cols = getCols(config)  
  _cols = excel_cols.keys()
  _cols.sort()

  file_ke = 1
  sBaseFileName = '%s/data_jemaah_%s_%s.csv' % (ResultDir, periode2, file_ke)
  headerText = 'DATA JEMAAH HAJI PER - '+periode
    
  app.ConWriteln('open template')
  wr = openTemplate(sBaseFileName, headerText, excel_cols)
  app.ConWriteln('done..open template')
      
  app.ConWriteln('query %s to database..' % headerText)
  app.ConWriteln(sSQL) 
  resQ = config.CreateSQL(sSQL).rawresult
  resQ.First()
  app.ConWriteln('done query ,start write to file..')
  i = 1
  while not resQ.Eof:
    datarows=[]
    datarows.append(i)  
    tglIndo = modman.getModule(config,'TglIndo')
    for _col in excel_cols:
      dCol = excel_cols[_col]
      n_col=dCol[0]

      if dCol[1]=='D':
        tgl = eval('resQ.'+n_col)
        v_kolom = tglIndo.tgl_indo(config,eval('resQ.'+n_col),10,1) if tgl<>None else ''
      else:
        v_kolom = eval('resQ.%s' % n_col)

      _column = _col-1

      if dCol[1]=='S' and v_kolom not in [None, '']:
        datarows.append(v_kolom)
        
      elif dCol[1]=='F':
        datarows.append(v_kolom)

      else:
        datarows.append(v_kolom)
  
    wr.writerow(datarows)
    if (i % 100000)==0:
      app.ConWriteln('baris ke %s done..' % i)

    i += 1
    if (i % 500000)==0:
      app.ConWriteln('***save file ke %s..' % file_ke)
      file_ke += 1
      sBaseFileName = '%s/data_jemaah_%s_%s.csv' % (ResultDir, periode2, file_ke)
      #open new file
      wr = openTemplate(sBaseFileName, '%s (%s)' % (headerText, file_ke), excel_cols)
                                    
    resQ.Next()
  
def openTemplate(sBaseFileName, headerText, excel_cols):    
  wr = csv.writer(open(sBaseFileName, 'w'), lineterminator='\n')
  wr.writerow([headerText])
  h2=['NO']
  _cols = excel_cols.keys()
  _cols.sort()
  for _col in excel_cols:
    dCol = excel_cols[_col]
    h2.append(dCol[0])

  wr.writerow(h2)
  
  return wr

  
  