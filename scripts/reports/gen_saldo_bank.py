'''
Fungsi ini digunakan untuk generate file report data jemaah by name by porsi addres 
dan summary per bank bulanan

'''
import sys, os
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman
import com.ihsan.foundation.appserver as appserver
import openpyxl
from openpyxl.cell import get_column_letter
from openpyxl.styles import Border
from openpyxl import Workbook
import com.ihsan.fileutils as fileutils

# GLOBALS
config  = appserver.ActiveConfig
app     = config.AppObject
_CONSOLE = False

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  
  global _CONSOLE
  
  _CONSOLE = False
  # generate report sum per bank
  GenerateFile_xlsx(config, 'sum')
    
  returns.CreateValues(['Is_Err', 0])

  return 1

def getSQL(config, tgl_rep):
  mlu = config.ModLibUtils
  
  #tgl_awal = config.FormatDateTime('01-mmm-yyyy', tgl_rep)
  tgl_akhir = config.FormatDateTime('dd-mmm-yyyy', tgl_rep)
  #periode = config.FormatDateTime('yyyy-mm', tgl_rep)
  dictParam = {
    #'tgl_awal': mlu.QuotedStr(tgl_awal),
    'tgl_akhir': mlu.QuotedStr(tgl_akhir),
    #'periode': mlu.QuotedStr(periode),
    'dailybalance_bank':config.MapDBTableName("dailybalance_bank"),
  }

  sSQL = ''' 
    SELECT 
    	to_char(dbh.PERIODE, 'YYYY-MM') PERIODE
    	, dbh.NOREK_BANK
    	, dbh.NAMA_REKENING
    	, dbh.KD_BANK
    	, dbh.NAMA_BANK
    	, dbh.JENIS_DANA
    	, dbh.CABANG
    	, dbh.KODE_VALUTA
    	, -dbh.SALDO as saldo
    	, dbh.MUT_CR_SALDO
    	, dbh.MUT_DB_SALDO
    	, dbh.SALDO_MARGIN
    	, dbh.MUT_CR_SALDO_MARGIN
    	, dbh.MUT_DB_SALDO_MARGIN
    	, -dbh.SALDO_BIAYA as saldo_biaya
    	, dbh.MUT_CR_SALDO_BIAYA
    	, dbh.MUT_DB_SALDO_BIAYA 
    FROM {dailybalance_bank} dbh
    	INNER JOIN (
    	  SELECT norek_bank, MAX(PERIODE) PERIODE FROM {dailybalance_bank}
    	  WHERE PERIODE <= TO_DATE({tgl_akhir}) GROUP BY norek_bank
    	) cdb ON dbh.norek_bank=cdb.norek_bank AND cdb.PERIODE=dbh.PERIODE
    ORDER BY dbh.kd_bank ,dbh.norek_bank 
  '''.format(**dictParam)

  
  return sSQL

def getCols(config):
  _cols = {
    2:['PERIODE', 'S'],
    3:['NOREK_BANK', 'S'],
    4:['NAMA_REKENING', 'S'],
    5:['KD_BANK', 'S'],
    6:['NAMA_BANK', 'S'],
    7:['JENIS_DANA', 'S'],
    8:['CABANG', 'S'],
    9:['KODE_VALUTA', 'S'],
    10:['SALDO', 'F'],
    11:['MUT_CR_SALDO', 'F'],
    12:['MUT_DB_SALDO', 'F'],
    13:['SALDO_MARGIN', 'F'],
    14:['MUT_CR_SALDO_MARGIN', 'F'],
    15:['MUT_DB_SALDO_MARGIN', 'F'],
    16:['SALDO_BIAYA', 'F'],
    17:['MUT_CR_SALDO_BIAYA', 'F'],
    18:['MUT_DB_SALDO_BIAYA', 'F'],
  }
  
  return _cols
#--

  
def GenerateFile(config, fl_date, ResultDir):
  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('Inisialisasi proses generate posisi saldo dan transaksi rekening bank...')
  sSQL = getSQL(config, fl_date)
  periode = config.FormatDateTime('yyyy-mm', fl_date)
  periode2 = config.FormatDateTime('yyyymmdd', fl_date)

  fileNamePath = config.HomeDir + 'template\\tpl_saldo_bank.xlsx'
  headerText = 'POSISI SALDO DAN TRANSAKSI REK GIRO & TABUNGAN, PERIODE - '+periode    
  wb, wsExcel = openTemplate(fileNamePath, headerText)

  excel_cols = getCols(config)  
  _cols = excel_cols.keys()
  _cols.sort()
      
  row = 3
  app.ConWriteln('query %s to database..' % headerText)
  app.ConWriteln(sSQL) 
  resQ = config.CreateSQL(sSQL).rawresult
  resQ.First()
  app.ConWriteln('done query ,start write to file..')
  i = 1
  file_ke = 1
  while not resQ.Eof:
    wsExcel.cell(row=row,column=0).value = i
    #styleCell(wsExcel, row, 0)  
    tglIndo = modman.getModule(config,'TglIndo')
    for _col in excel_cols:
      dCol = excel_cols[_col]
      n_col=dCol[0]

      if dCol[1]=='D':
        tgl = eval('resQ.'+n_col)
        v_kolom = tglIndo.tgl_indo(config,eval('resQ.'+n_col),10,1) if tgl<>None else ''
      else:
        v_kolom = eval('resQ.%s' % n_col)

      _column = _col-1

      if dCol[1]=='S' and v_kolom not in [None, '']:
        wsExcel.cell(row=row,column=_column).set_explicit_value(v_kolom, data_type="s")
        
      elif dCol[1]=='F':
        wsExcel.cell(row=row,column=_column).value = v_kolom

      else:
        wsExcel.cell(row=row,column=_column).value = v_kolom
  
    row += 1
    if (i % 10000)==0:
      app.ConWriteln('baris ke %s done..' % file_ke)

    if (i % 500000)==0:
      app.ConWriteln('***save file ke %s..' % i)
      sBaseFileName = '%s/saldo_rek_bank_%s_%s.xlsx' % (ResultDir, periode2, file_ke)
      #save file 
      saveFile(wb, sBaseFileName)
      #open new file
      wb, wsExcel = openTemplate(fileNamePath, '%s (%s)' % (headerText, file_ke))
      row = 3
      file_ke += 1

    i += 1
                                    
    resQ.Next()
  
    #last save file
    sBaseFileName = '%s/saldo_rek_bank_%s_%s.xlsx' % (ResultDir, periode2, file_ke)
    saveFile(wb, sBaseFileName)

def saveFile(wb, sBaseFileName):
  #ResultDir = 'c:/dafapp/rawdata/jemaah/'
  fileHasil = sBaseFileName
  #app.ConWriteln('file hasil >>'+fileHasil)  
  fileutils.SafeDeleteFile(fileHasil) #Melakukan Hapus File yang sudah ada sebelumnya
  wb.save(fileHasil)    
  
def openTemplate(fileNamePath, headerText):    
  wb = openpyxl.load_workbook(fileNamePath)
  wsExcel = wb.get_sheet_by_name('data')
  wsExcel.cell(row=0,column=1).value = headerText
  
  return wb, wsExcel

  
  