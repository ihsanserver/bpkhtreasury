import math
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import os, pyFlexcel
import com.ihsan.foundation.pobjecthelper as pobjecthelper
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.modman as modman

GRID_SETTING_GROUP = 'gridsettings.grouptxlist'
GRID_SETTING_DETIL = 'gridsettings.detiltxlist'
GRID_SCHEDULE = 'gridsettings.schedule_cols'
GRID_SETTING_LHLOAN = 'gridsettings.lhloanlist'
MASTER_TEMPLATE  = 'ExportMaster.xls'
SCHED_TEMPLATE  = 'tmp_schedule.xls'
 
def getTemplateDir(config):
   return config.HomeDir + 'template\\'

def dbg(msg):
  message.send_udp(msg + "\n", 'localhost', 9125)

def getSqlLHLoan(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils            
  sql = {}
  fr = params.FirstRecord
    
  gsModule = modman.getModule(config, GRID_SETTING_LHLOAN)
  excel_cols = gsModule.EXCEL_COLUMN
  columnSetting = gsModule.COLUMN_SETTINGS
  
  sql['SELECTFROMClause'] = '''
    SELECT 
    	baris,NOMOR_LOAN,NOMINAL,TANGGAL_VALUTA,KETERANGAN_TRX,KODE_TRX
    FROM %(tmp_lhloan)s dt
    ''' % { 'tmp_lhloan': config.MapDBTableName('tmp_lhloan')}
    
  sql['WHEREClause'] = '''
      NOMOR_LOAN = %(nomor_rekening)s  
    ''' % {'nomor_rekening': mlu.QuotedStr(fr.nomor_rekening)}
  if fr.start_date not in [0,None] and fr.end_date not in [0,None]:   
    startDate = math.floor(fr.start_date) 
    sDateAwal = config.FormatDateTimeForQuery(startDate)
    endDate = math.floor(fr.end_date) + 1
    sDateAkhir = config.FormatDateTimeForQuery(endDate)
    sql['WHEREClause'] = sql['WHEREClause'] + """
       AND TANGGAL_VALUTA >= %s AND TANGGAL_VALUTA < %s
      """ % (sDateAwal, sDateAkhir)
      
  #raise Exception, sql['WHEREClause'] 
  sql['keyFieldName'] = 'tanggal_valuta'
  sql['altOrderFieldNames'] = 'tanggal_valuta;baris'
  sql['baseOrderFieldNames'] = 'tanggal_valuta'
  sql['columnSetting'] = columnSetting
  sql['excel_cols'] = excel_cols 
  
  return sql

def getSqlDetail(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils            
  sql = {}
  fr = params.FirstRecord
  if fr.src == 'today':
    tblDetilTransaksi = 'core.detiltransaksi'
    tblTransaksi = 'core.transaksi'
  else:
    tblDetilTransaksi = 'core.histdetiltransaksi'
    tblTransaksi = 'core.histtransaksi'
  if fr.kode_tx_class == "":
    allTxClass = 1
  else:
    allTxClass = 0
    
  gsModule = modman.getModule(config, GRID_SETTING_DETIL)
  excel_cols = gsModule.EXCEL_COLUMN
  columnSetting = gsModule.COLUMN_SETTINGS
  
  sql['SELECTFROMClause'] = '''
    SELECT
      tx.tanggal_transaksi,
      dt.keterangan,
      dtc.kode_tx_class,
      dt.jenis_mutasi,
      dt.nilai_mutasi,
      dt.kode_valuta,
      dtc.deskripsi,
      dt.nilai_kurs_manual as nilai_kurs,
      dt.id_detil_transaksi,
      tx.id_transaksi,
      tx.nomor_seri,
      tx.journal_no,
      tx.user_input,
      tx.user_otorisasi
    FROM
      %(detiltransaksi)s dt left outer join %(detiltransaksiclass)s dtc ON dt.kode_tx_class = dtc.kode_tx_class,
      %(transaksi)s tx
      
    ''' % {
      'detiltransaksi': config.MapDBTableName(tblDetilTransaksi), 
      'transaksi': config.MapDBTableName(tblTransaksi), 
      'detiltransaksiclass': config.MapDBTableName('core.detiltransaksiclass')
    }
  sql['WHEREClause'] = '''
      (1 = %(allTxClass)d OR dt.kode_tx_class=%(kode_tx_class)s) AND
      dt.id_transaksi = tx.id_transaksi AND 
      dt.nomor_rekening = %(nomor_rekening)s  
    ''' % {'kode_tx_class': mlu.QuotedStr(fr.kode_tx_class), 'allTxClass': allTxClass, 'nomor_rekening': mlu.QuotedStr(fr.nomor_rekening)}
  if fr.src == 'history':
    startDate = math.floor(fr.start_date) 
    sDateAwal = config.FormatDateTimeForQuery(startDate)
    endDate = math.floor(fr.end_date) + 1
    sDateAkhir = config.FormatDateTimeForQuery(endDate)
    sql['WHEREClause'] = sql['WHEREClause'] + """
       AND tx.tanggal_transaksi >= %s AND tx.tanggal_transaksi < %s
      """ % (sDateAwal, sDateAkhir)
      
  #raise Exception, sql['WHEREClause'] 
  sql['keyFieldName'] = 'tx.tanggal_transaksi'
  sql['altOrderFieldNames'] = 'tx.tanggal_transaksi;tx.id_transaksi;id_detil_transaksi;kode_tx_class'
  sql['baseOrderFieldNames'] = 'tx.tanggal_transaksi;tx.id_transaksi;id_detil_transaksi'
  sql['columnSetting'] = columnSetting
  sql['excel_cols'] = excel_cols 
  
  return sql

def getSqlGroup(config, params):
  #rpdb2.start_embedded_debugger('000')
  app = config.AppObject; 
  
  mlu = config.ModLibUtils
  sql = {}
  fr = params.FirstRecord
  if fr.src == 'today':
    tblDetilTransaksi = 'core.detiltransaksi'
    tblTransaksi = 'core.transaksi'
    tblDetilTransGroup = 'core.detiltransgroup'
  else:
    tblDetilTransaksi = 'core.histdetiltransaksi'
    tblTransaksi = 'core.histtransaksi'
    tblDetilTransGroup = 'core.histdetiltransgroup'
  helper = pobjecthelper.PObjectHelper(config)
  oFinAccount = helper.GetObject("FinAccount", fr.nomor_rekening)
  kode_jenis = oFinAccount.kode_jenis
  
  gsModule = modman.getModule(config, GRID_SETTING_GROUP)
  excel_cols = gsModule.EXCEL_COLUMN
  cs = gsModule.COLUMN_SETTINGS
  if cs.has_key(kode_jenis):
    columnSetting = cs[kode_jenis]
  else:
    columnSetting = ''
  q = config.CreateSQL("""
      SELECT * FROM %(detiltransaksiclass)s
      WHERE kode_jenis = %(kode_jenis)s AND accum_field_name IS NOT NULL 
      ORDER BY kode_tx_class
    """ % {
      'detiltransaksiclass': config.MapDBTableName('core.detiltransaksiclass'),
      'kode_jenis': mlu.QuotedStr(kode_jenis)
    }
  ).RawResult
  lColumns = []
  while not q.Eof:
    lColumns.append((q.kode_tx_class, q.tx_label))
    q.Next()
    
  sCols = []
  sColTemplate = "SUM(case when dt.kode_tx_class = %s then (case when dt.jenis_mutasi='D' then -1 else 1 end) * dt.nilai_mutasi else 0.0 end) AS %s"
  for col in lColumns:
     sCols.append(sColTemplate % (mlu.QuotedStr(col[0]), col[1].replace(' ', '_') ))
  columns = ',\n'.join(sCols)
  if columns != "":
    columns = ', ' + columns
    
  dictParam = {
    'columns': columns,
    'detiltransaksi': config.MapDBTableName(tblDetilTransaksi),
    'transaksi': config.MapDBTableName(tblTransaksi),
    'detiltransgroup': config.MapDBTableName(tblDetilTransGroup),
    'detiltransgroupclass': config.MapDBTableName('core.detiltransgroupclass') 
  }
  sSQL = '''
    SELECT
      tx.tanggal_transaksi,
      dt.keterangan,
      tx.id_transaksi,
      dt.id_detiltransgroup,
      dtg.kode_entri,
      dtc.nama_entri,
      dt.kode_valuta,
      tx.journal_no,
      tx.nomor_seri
      %(columns)s
    FROM
      %(detiltransaksi)s dt,
      %(transaksi)s tx,
      %(detiltransgroup)s dtg,
      %(detiltransgroupclass)s dtc
    ''' % dictParam
  sql['SELECTFROMClause'] = sSQL
  sql['WHEREClause'] = '''
      dt.id_transaksi = tx.id_transaksi AND
      dt.id_detiltransgroup = dtg.id_detiltransgroup AND 
      dt.nomor_rekening = %(nomor_rekening)s AND
      dtg.kode_entri = dtc.kode_entri  
    ''' % {'nomor_rekening': mlu.QuotedStr(fr.nomor_rekening)}
  sql['GROUPBYClause'] = "GROUP BY tx.tanggal_transaksi, dt.id_detiltransgroup, tx.keterangan, tx.id_transaksi, dtg.kode_entri, dtc.nama_entri, dt.kode_valuta, tx.journal_no, tx.nomor_seri"
  
  if fr.src == 'history':
    startDate = math.floor(fr.start_date) 
    sDateAwal = config.FormatDateTimeForQuery(startDate)
    endDate = math.floor(fr.end_date) + 1
    sDateAkhir = config.FormatDateTimeForQuery(endDate)
    sql['WHEREClause'] = sql['WHEREClause'] + """
       AND tx.tanggal_transaksi >= %s AND tx.tanggal_transaksi < %s
      """ % (sDateAwal, sDateAkhir)
  sql['keyFieldName'] = 'dt.id_detiltransgroup'
  sql['altOrderFieldNames'] = 'dt.id_detiltransgroup;tanggal_transaksi'
  sql['baseOrderFieldNames'] = 'dt.id_detiltransgroup;tanggal_transaksi'
  sql['columnSetting'] = columnSetting 
  sql['excel_cols'] = excel_cols 
  return sql
  
def getQueryData(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('Inisialisasi proses...')
  rec = ds.AddRecord()
  if(param.detail_level == 'D'):                                                   
    sqlStat = getSqlDetail(config, clientpacket)
  elif (param.detail_level == 'L'):
    sqlStat = getSqlLHLoan(config, clientpacket)     
  else:
    sqlStat = getSqlGroup(config, clientpacket)
      
  try :
    app.ConWriteln(sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause'])
    #app.ConRead('')
    #raise Exception,'QQ' 
    rqsql = remotequery.RQSQL(config)
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause'] 
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '')
    rqsql.initOperation(returnpacket)
    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1

def CreateExcelHeader(owb, judul_excel, excel_cols, R_=3):
  owb.SetCellValue(1, 1, judul_excel)
  i = 1
  for cols in excel_cols:
    nama_kolom = cols[0]
    cols_split = cols[0].split(';')
    if len(cols_split) > 1: 
      nama_kolom = cols_split[1]
      
    owb.SetCellValue(R_, i, nama_kolom)    
    i += 1
  return owb

def GetNilaiKolomExcel(config, _col, resQ):
  tglIndo = modman.getModule(config,'TglIndo')
  n_col=_col[0].split(';')[0]
  if _col[1]=='D':
    tgl = eval('resQ.'+n_col)
    v_kolom = tglIndo.tgl_indo(config,eval('resQ.'+n_col),2,1) if tgl<>None else ''
  #elif _col[1]=='F':
  #  v_kolom = config.FormatFloat(',0.00',float(eval('resQ.'+n_col) or 0))
  else:
    v_kolom = eval('resQ.%s' % n_col)
                                                          

  return v_kolom

def PrintXLS(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  helper = pobjecthelper.PObjectHelper(config)
  OrderBy =' order by tx.tanggal_transaksi,id_detil_transaksi'
  if(param.detail_level == 'D'):
    sqlStat = getSqlDetail(config, clientpacket)
    judul_excel = 'Riwayat Transaksi Detail' 
    sSQL = sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause']+OrderBy
  elif(param.detail_level == 'L'):
    sqlStat = getSqlLHLoan(config, clientpacket)
    judul_excel = 'Riwayat Transaksi Dari LHLOAN' 
    sSQL = sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause']+" order by tanggal_valuta,baris"
  else:
    sqlStat = getSqlGroup(config, clientpacket)
    judul_excel = 'Riwayat Transaksi Group'
    sSQL = sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause']+sqlStat['GROUPBYClause']+OrderBy

  tpl = getTemplateDir(config) + MASTER_TEMPLATE 

  #dbg(tpl) 
  workbook = pyFlexcel.Open(tpl)
  workbook.ActivateWorksheet('Data')
  try :
    excel_cols = sqlStat['excel_cols']
    workbook = CreateExcelHeader(workbook, judul_excel, excel_cols)
    resQ = config.CreateSQL(sSQL).RawResult          
    _R = 4
    while not resQ.Eof :
      _C = 1
      for _col in excel_cols:
        v_kolom = GetNilaiKolomExcel(config, _col, resQ)
        workbook.SetCellValue(_R, _C, v_kolom)        
        _C += 1
        # tutup for 
      _R += 1
      resQ.Next()
      # end while
      
    #dbg('before create corporate')
    corporate = helper.CreateObject('Corporate')
    #dbg('corporate instantiated')
    FileName  = 'peragaan_transaksi.xls'
    FullName  = corporate.GetUserHomeDir() + FileName
    dbg(FullName)
    if os.access(FullName, os.F_OK) == 1:
        os.remove(FullName)
    workbook.SaveAs(FullName)
    dbg('xls saved')        
    sw =  returnpacket.AddStreamWrapper()
    sw.Name = 'gen_report'
    sw.LoadFromFile(FullName)
    sw.MIMEType = "application/vnd.ms-excel"
    
    returnpacket.CreateValues(['IsErr', 0])
  except :
    returnpacket.CreateValues(['IsErr', 1], ['ErrMessage', str(sys.exc_info()[1]) ] )
    dbg('SERVER EXCEPTION ERROR : ' + str(sys.exc_info()[1]))

def initQueryJadwalAngsuran(config, params, returns):
  fr = params.FirstRecord
  nomor_rekening = fr.nomor_rekening
  helper = pobjecthelper.PObjectHelper(config)
  oFinAccount = helper.GetObject("FinAccount", nomor_rekening)
  oSchedule = oFinAccount.LActiveSchedule
  if oSchedule.IsNull:
    raise Exception, "Tidak ada jadwal angsuran untuk pembiayaan ini"
  rq = oSchedule.getRemoteQueryViewSchedule()
  rq.initOperation(returns)
   
#--

def ExportAngsuran(config, clientpacket, returnpacket):
  #param = clientpacket.FirstRecord
  fr = clientpacket.FirstRecord
  gsModule = modman.getModule(config, GRID_SCHEDULE)
  excel_cols = gsModule.ex_cols(fr.akad or '')
  nomor_rekening = fr.nomor_rekening
  helper = pobjecthelper.PObjectHelper(config)
  oFinAccount = helper.GetObject("FinAccount", nomor_rekening)
  oFinAccount = oFinAccount.CastToLowestDescendant()
  oSchedule = oFinAccount.LActiveSchedule
  if oSchedule.IsNull:
    raise Exception, "Tidak ada jadwal angsuran untuk pembiayaan ini"
  sSQL = oSchedule.getSqlSchedule()
  sSQL += "where sdd.id_schedule = %s and sdd.seq_number>0 order by seq_number" % oSchedule.id_schedule
  #raise Exception,sSQL
  judul_excel = 'JADWAL ANGSURAN' 
  
  tpl = getTemplateDir(config) + SCHED_TEMPLATE
  #dbg(tpl) 
  workbook = pyFlexcel.Open(tpl)
  workbook.ActivateWorksheet('Data')
  #generate Header
  workbook.SetCellValue(2, 2, nomor_rekening)
  workbook.SetCellValue(3, 2, oFinAccount.nama_rekening)
  workbook.SetCellValue(4, 2, oFinAccount.product_code)
  workbook.SetCellValue(5, 2, oFinAccount.kode_cabang)

  saldo_net=0
  if not oFinAccount.isRepaymentCompleted() and oFinAccount.status_rekening == 1 and oFinAccount.id_schedule not in [None, 0]:
    saldo_net = -(oFinAccount.saldo)
    if oFinAccount.contracttype_code in ('F'):
      saldo_net = oFinAccount.getPrincipalOutstanding()+oFinAccount.arrear_balance
    
  total_os = saldo_net-oFinAccount.arrear_balance
  
  efr = oFinAccount.targeted_eqv_rate

  workbook.SetCellValue(2, 7, oFinAccount.dropping_amount if oFinAccount.contracttype_code<>'G' else oFinAccount.income_target)
  workbook.SetCellValue(3, 7, total_os)
  workbook.SetCellValue(4, 7, "%s %%" % efr)

  try :
    _R = 7
    workbook = CreateExcelHeader(workbook, judul_excel, excel_cols, _R-1)
    resQ = config.CreateSQL(sSQL).RawResult 
    while not resQ.Eof :
      _C = 1
      for _col in excel_cols:
        v_kolom = GetNilaiKolomExcel(config, _col, resQ)
        #raise Exception, v_kolom
        workbook.SetCellValue(_R, _C, v_kolom)        
        _C += 1
        # tutup for 
      _R += 1
      resQ.Next()
      # end while
    
    #dbg('before create corporate')
    corporate = helper.CreateObject('Corporate')
    #dbg('corporate instantiated')
    FileName  = 'peragaan_transaksi.xls'
    FullName  = corporate.GetUserHomeDir() + FileName
    dbg(FullName)
    if os.access(FullName, os.F_OK) == 1:
        os.remove(FullName)
    workbook.SaveAs(FullName)
    dbg('xls saved')        

    #return packet
    sw = returnpacket.AddStreamWrapper()
    sw.Name = 'gen_report'
    sw.LoadFromFile(FullName)
    sw.MIMEType = "application/vnd.ms-excel"
    
    returnpacket.CreateValues(['IsErr', 0])
  except :
    returnpacket.CreateValues(['IsErr', 1], ['ErrMessage', str(sys.exc_info()[1]) ] )
    dbg('SERVER EXCEPTION ERROR : ' + str(sys.exc_info()[1]))