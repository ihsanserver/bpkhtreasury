import dafsys4
import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.modman as modman
import rpdb2
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil

def DAFScriptMain(config, params, returns):
  # config: ISysConfig object
  # params: TPClassUIDataPacket
  # returns: TPClassUIDataPacket (undefined structure)

  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)
  
  try:
    app = config.AppObject
    ioutil.IO_MODE = ioutil.IOMODE_DAF
    ioutil.APP_OBJECT = app
    dbutil.DEBUG_LEVEL = 1
    treajournal = modman.getModule(config, 'TreasuryJournal')
    treajournal.createTreasuryJournal(config, strDate, None)
  except:
    raise

  return 1