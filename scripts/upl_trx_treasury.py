import sys
import os
import com.ihsan.foundation.pobject as pobject
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as pobjecthelper
#import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

dParam={}    

def main(config, parameter, upload_id , debugLevel = None):
  helper = pobjecthelper.PObjectHelper(config)
  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('Inisialisasi proses...')

  periodHelper = helper.CreateObject('core.PeriodHelper')
  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  y, m, d = config.ModLibUtils.DecodeDate(float_oToday)
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)

  sutil = modman.getModule(config, 'sutil')
  sutil.setDialectToORACLE()
  
  mlu = config.ModLibUtils
  dParams = {
    'detiltransaksiumum':config.MapDBTableName("core.detiltransaksiumum"),
    'transaksiumum':config.MapDBTableName("core.transaksiumum"),
    'glinterface':config.MapDBTableName("core.glinterface"),
    'treasuryaccount':config.MapDBTableName("core.treasuryaccount"),

    'RekeningTransaksi':config.MapDBTableName("core.RekeningTransaksi"),
    'Transaksi':config.MapDBTableName("core.transaksi"),
    'DetilTransaksi':config.MapDBTableName("core.DetilTransaksi"),
    'DetilTransaksiClass':config.MapDBTableName("core.DetilTransaksiClass"),
    'Journal':config.MapDBTableName("core.journal"),
    'JournalItem':config.MapDBTableName("core.journalitem"),
    'Account':config.MapDBTableName("core.Account"),
    'AccountInstance':config.MapDBTableName("core.AccountInstance"),
    'Currency':config.MapDBTableName("core.Currency"),
    'kurshistory': config.MapDBTableName("core.kurshistory"),
    'pendingtransactionjournal': config.MapDBTableName('pendingtransactionjournal'),

    'Seq_JournalItem':config.MapDBTableName("core.Seq_JournalItem"),
    'Seq_TXSerialNumber':config.MapDBTableName("core.Seq_TXSerialNumber"),
    'upload_id': mlu.QuotedStr(upload_id),
    'session_id': config.SecurityContext.SessionID,
    
    'Today'  : sutil.toDate(config, float_oToday),
    'FToday' : oToday.GetFlatText(),
    'YToday' : str(y),
    'MToday' : str(m),
    'DToday' : str(d), 
  }
  
  convert_lib = modman.getModule(config,'convert_lib')  
  # Validasi Required Field 
  
  SQL_FLOW = [
      'CR_IDTransaksi'
      , 'CR_SetRateKursValas'
      , 'CR_Transaksi'
      , 'CR_DetilTransaksiAccount'
      , 'Setpendingtransactionjournal'
    ]
  
  convert_lib.ExcecuteSQLFlow(config, SQL_RESOURCES, SQL_FLOW, dParams, False)
  #---- 
  
  #update balance
  updateBalance(config, upload_id) 
  
  #journal transaksi
  finjournal = modman.getModule(config, 'TreasuryJournal')
  finjournal.createTreasuryJournal(config, strDate, None)
  
def updateBalance(config, upload_id):  
  app = config.AppObject
  mlu = config.ModLibUtils
  dParams = {
    'detiltransaksiumum':config.MapDBTableName("core.detiltransaksiumum"),
    'transaksiumum':config.MapDBTableName("core.transaksiumum"),
    'Transaksi':config.MapDBTableName("core.transaksi"),
    'DetilTransaksi':config.MapDBTableName("core.DetilTransaksi"),
    'detiltransaksiclass':config.MapDBTableName("core.DetilTransaksiClass"),
    'upload_id': mlu.QuotedStr(upload_id),
  }
  
  sSQL = '''
    SELECT 
      DISTINCT b.accum_field_name, b.accum_table_name
    FROM {detiltransaksiumum} a                                                        
      INNER JOIN {detiltransaksiclass} b ON a.kode_tx_class=b.kode_tx_class
    where a.nomor_referensi={upload_id}
      and b.accum_field_name is not null
  '''.format(**dParams)
  q = config.CreateSQL(sSQL).rawresult
  while not q.Eof:
    #app.ConWriteln('Account pembiayaan tidak bisa dipush, Error '+q.primarycode+' : '+q.additional_desc)
    dParams['tblName']=config.MapDBTableName(q.accum_table_name)
    dParams['fieldName']=q.accum_field_name
    
    sSQL = '''
      merge into {tblName} x using (
        SELECT 
          a.nomor_rekening, b.accum_field_name, b.accum_table_name
          , Sum(Decode(jenis_mutasi,'C',nilai_mutasi,-nilai_mutasi)) mutasi
        FROM {detiltransaksiumum} a                                                        
        INNER JOIN {detiltransaksiclass} b ON a.kode_tx_class=b.kode_tx_class
        where b.accum_field_name='{fieldName}'
          and a.nomor_referensi={upload_id}
        GROUP BY a.nomor_rekening, b.accum_field_name, b.accum_table_name
      ) y on (x.nomor_rekening=y.nomor_rekening)
      when matched then update set 
        x.{fieldName} = x.{fieldName}+y.mutasi       
    '''.format(**dParams)
    app.ConWriteln(sSQL)
    #app.ConRead('')
    dbutil.runSQL(config, sSQL)
    
    q.Next()
#---
    
SQL_RESOURCES = {
  'CR_IDTransaksi' : '''
    merge INTO {detiltransaksiumum} x USING 
      {transaksiumum} y ON (x.nomor_referensi=y.nomor_seri)
    WHEN matched THEN UPDATE SET 
      x.id_transaksi   = y.id_transaksi
      , x.id_detiltransgroup = y.ref_id_transaksi
      , x.keterangan = y.keterangan
    where nomor_referensi={upload_id}
  '''
  ,
  'CR_SetRateKursValas' : '''
    merge INTO {detiltransaksiumum} t
    USING {kurshistory} k
    ON (k.currency_code = t.kode_valuta AND {Today} = k.history_date)
    WHEN matched THEN UPDATE 
    SET nilai_kurs_manual = k.kurs_tengah_bi
    WHERE nomor_referensi={upload_id}
  '''
  ,  
  'CR_Transaksi' : '''
    INSERT INTO {Transaksi} (
      Id_Transaksi, Tanggal_Transaksi, Nomor_Referensi
      , User_Input, Terminal_Input, User_Overide
      , Keterangan, Terminal_Overide
      , User_Otorisasi, Terminal_Otorisasi, Jam_Otorisasi, Status_Otorisasi
      , Jam_Input, Tanggal_Otorisasi, Biaya, Jenis_Aplikasi
      , Nilai_Transaksi, Jenis_Transaksi, Kode_Valuta_Transaksi, Kurs_Manual
      , Nilai_Ekuivalen, Is_Sudah_Dijurnal, Pajak, Zakat
      , Tanggal_Input, Kode_Cabang_Transaksi, Id_Blok_Jurnal, Id_Transaksi_Parent
      , nomor_seri, is_draft, journal_no, ID_TransaksiAgregat
      , Id_Batch_Transaksi, Kode_Transaksi, Id_Transaction_Group
    )
    SELECT
      st.id_transaksi, st.tanggal_transaksi, st.nomor_referensi
      , st.user_input, st.terminal_input, NULL
      , substr(st.keterangan, 1, 200), NULL
      , st.user_override, st.terminal_otorisasi, current_date, 1
      , current_date, current_date, 0.0, 'A'
      , NULL, NULL, NULL, 1.0
      , NULL, 'F', NULL, NULL
      , st.tanggal_input, st.kode_cabang_transaksi, NULL, NULL
      , rawtohex({Seq_TXSerialNumber}.nextval), 'F', NULL, NULL
      , 0, st.kode_transaksi, NULL
    FROM {transaksiumum} st
    WHERE st.nomor_seri={upload_id} 
  '''
  ,
  'CR_DetilTransaksiAccount' : '''
    INSERT INTO {DetilTransaksi} (
      Id_Detil_Transaksi, Tanggal_Transaksi
      , Jenis_Mutasi, Nilai_Mutasi, Keterangan
      , Kode_Cabang, Kode_Valuta, Jenis_Detil_Transaksi
      , Nilai_Kurs_Manual, Nilai_Ekuivalen 
      , Saldo_Awal, Saldo_Akhir, Nomor_Referensi
      , Kode_Kurs, Kode_Account 
      , Kode_Valuta_RAK, Kode_Jurnal
      , Nominal_Biaya, IsBillerTransaction, hide_passbook
      , hide_statement, hide_gentrans, subsystem_code, sub_tx_code, kode_subtx_class
      , Id_Transaksi, Nomor_Rekening
      , kode_tx_class, id_detiltransgroup
      , Id_Parameter_Transaksi, Id_Parameter, ID_JournalBlock
    )
    SELECT 
      ms.id_detil_transaksi_umum, st.tanggal_transaksi
      , ms.jenis_mutasi, ms.nilai_mutasi, st.keterangan 
      , ms.kode_cabang, nvl(ms.kode_valuta,'IDR'), NULL
      , ms.nilai_kurs_manual, ms.nilai_mutasi * ms.nilai_kurs_manual as n_eqv
      , NULL, NULL, st.nomor_referensi 
      , ms.kode_kurs, ms.kode_account
      , NULL, '11'
      , 0.0, 'F', 'F' 
      , 'F', 'F', NULL, NULL, NULL 
      , ms.id_transaksi, ms.nomor_rekening
      , ms.kode_tx_class, ms.id_detiltransgroup 
      , NULL, NULL, NULL
    FROM
      {detiltransaksiumum} ms
      INNER JOIN {transaksiumum} st ON (ms.nomor_referensi=st.nomor_seri)
    where ms.nomor_referensi={upload_id}       
  '''
  ,
  'Setpendingtransactionjournal': '''
    INSERT INTO {pendingtransactionjournal} (
      Id_Transaksi, Session_id
    )
    SELECT
      mps.id_transaksi, '{session_id}'
    FROM
      {transaksiumum} mps
    where mps.nomor_seri={upload_id}       
  '''
}   
             