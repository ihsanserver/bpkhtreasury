import com.ihsan.foundation.pobjecthelper as phelper
import sys
import com.ihsan.fileutils as fileutils
import string

def DAFScriptMain(config, params, returns):
  # config: ISysConfig object
  # params: TPClassUIDataPacket
  # returns: TPClassUIDataPacket (undefined structure)

  return 1
  
  
#---------1---------2---------3---------4---------5---------6---------7---------8---------9---------10---------1---------2---------3
__tplLAYOUT1 = [
'    DKU4 D {NOMOR_REKENING_DEBET}-{KODE_CABANG}-{KODE_VALUTA} {NAMA_REKENING_DEBET}                                  {KODE_VALUTA} **{NOMINAL_TRANSAKSI}* NT {NOMOR_REFERENSI}',
'    DKU4 K {NOMOR_REKENING_KREDIT}-{KODE_CABANG}-{KODE_VALUTA} {NAMA_REKENING_KREDIT}              {KODE_CABANG} {INPUTER}', 
'    {JAM_INPUT} {TANGGAL_INPUT} {TANGGAL_TRANSAKSI} #001',                                                           
]

__tplLAYOUT2 = [
'    DKU4 D {NOMOR_REKENING_DEBET}-{KODE_CABANG}-{KODE_VALUTA} {NAMA_REKENING_DEBET}   {KODE_VALUTA} **{NOMINAL_TRANSAKSI}* NT {NOMOR_REFERENSI}',
'    DKU4 K {NOMOR_REKENING_KREDIT}-{KODE_CABANG}-{KODE_VALUTA} {NAMA_REKENING_KREDIT}                 {KODE_CABANG} {INPUTER}', 
'    {JAM_INPUT} {TANGGAL_INPUT} {TANGGAL_TRANSAKSI} #001',                                                           
]

def penggal(kalimat, max):
    s = string.splitfields(kalimat,'\n')
    #t = ""
    r = []
    for k in s:
      t = ""
      s1 = string.splitfields(k)
      for i in s1:
        if len(string.strip(t + " " + i)) > max:
          r.append(t)
          t = ""
        t = string.strip(t+" "+i)
      r.append(t)
    return r
  
def NominalTerbilang(config,nominal,matauang):
    strNominal = config.FormatFloat('0.00', nominal) 
    lsNominal = string.split(strNominal,".")
     
    matauang = " " + matauang
    cJmlNominalUtama = terbilang(lsNominal[0]) #+ matauang 
    cJmlNominalSen = ""
    if int(lsNominal[1]) != 0:
       cJmlNominalSen = " Koma " + terbilang(lsNominal[1]) + " Sen"
    Nominal_Terbilang = cJmlNominalUtama + cJmlNominalSen
    
    return Nominal_Terbilang
    
def terbilang(b):
    def ParsingTiga(b):    
      if b == 0:
        return "Nol"
      bs = str(b)
      pjg = len(bs)
      kalimat = []
      for i in range(pjg):
        c = int(bs[pjg-i-1])
        if c == 0:
          s = ""
        else:
          s = angka[c]
          if i == 1:
            if c == 1:
              sebelum = bs[pjg-1]
              if sebelum == "0":
                s = "Sepuluh "
              else:
                j = len(kalimat)
                if sebelum == "1":
                  s = "Sebelas "
                else:
                  s = kalimat[j-2] + "Belas "
                kalimat[j-2] = ""
            else:
              s = s + "Puluh "
          elif i == 2:
            if c == 1:
              s = "Seratus "
            else:
              s = s + "Ratus "

        kalimat.append(s)
      kalimat.reverse()
      s = ""
      for i in kalimat:
        s = string.strip(s + " " + i)
      return s
    
    blok = ['','Ribu ','Juta ','Milyar ','Trilyun ','Bilyun ']
    angka = ['','Satu ','Dua ','Tiga ','Empat ','Lima ','Enam ','Tujuh ','Delapan ','Sembilan ']
    bs = str(b)
    pjg = len(bs)
    JmlBlok = pjg / 3
    if pjg % 3 > 0:
      JmlBlok = JmlBlok + 1
    k = []
    for i in range(JmlBlok):
      c = int(bs[-3:])
      if i == 1 and c == 1:
        s = "seribu"
      elif c == 0 and JmlBlok > 1:
        s = ""
      else:
        s = ParsingTiga(c) + " " + blok[i]
      k.append(string.strip(s))
      bs = bs[:-3]
    k.reverse()
    s = ""
    for i in k:
      s = string.strip(s + " " + i)
    return s

def WriteDataToFile(config, dictValidasi, LAYOUT):
    
    data_validasi = chr(15)
    for rowLAYOUT in LAYOUT :
      data_validasi += rowLAYOUT.format(**dictValidasi)  + '\n'
   
    # Set Full Path 
    sBaseFileName = "validasi.txt"
    sFileName = config.UserHomeDirectory + sBaseFileName
    
    fileutils.SafeDeleteFile(sFileName)
    
    # Write To File 
    oFile = open(sFileName,'w')
    oFile.write(data_validasi)
    oFile.close()

    return sFileName    
    
def GetValidasi(config, params, returns):
    status = returns.CreateValues(
       ['Is_Err', 0],
       ['Err_Message', ''])

    config.BeginTransaction()   
    try :     
      helper = phelper.PObjectHelper(config)

      Id_Transaksi = params.FirstRecord.Id_Transaksi
      oTransaksi = helper.GetObject('Transaksi', Id_Transaksi)
      
      if not oTransaksi.SudahOtorisasi():
         raise Exception, 'Transaksi belum di otorisasi'               

      filevalidasi = GenerateValidasiFile(oTransaksi)
            
      sw = returns.AddStreamWrapper()
      sw.LoadFromFile(filevalidasi)                       
      sw.MIMEType = config.AppObject.GetMIMETypeFromExtension(filevalidasi)
      config.Commit()
    except:
      config.Rollback() 
      status.Is_Err = 1
      status.Err_Message = str(sys.exc_info()[1])
      
def GetValidasiHist(config, params, returns):
    status = returns.CreateValues(
       ['Is_Err', 0],
       ['Err_Message', ''])

    config.BeginTransaction()   
    try :     
      helper = phelper.PObjectHelper(config)

      Id_Transaksi = params.FirstRecord.Id_Transaksi
      oTransaksi = helper.GetObject('HistTransaksi', Id_Transaksi)
      
      if not oTransaksi.SudahOtorisasi():
         raise Exception, 'Transaksi belum di otorisasi'               

      filevalidasi = GenerateValidasiFile(oTransaksi)
            
      sw = returns.AddStreamWrapper()
      sw.LoadFromFile(filevalidasi)                       
      sw.MIMEType = config.AppObject.GetMIMETypeFromExtension(filevalidasi)
      config.Commit()
    except:
      config.Rollback() 
      status.Is_Err = 1
      status.Err_Message = str(sys.exc_info()[1])
 
def GenerateValidasiFile(oTransaksi):
    helper  = oTransaksi.Helper
    config = helper.Config
    phTgl = config.AppObject.rexecscript("core", "appinterface/coreInfo.getAccountingDay", config.AppObject.CreatePacket())
    tgl_transaksi = phTgl.FirstRecord.acc_date
    dt = config.ModLibUtils.DecodeDate(tgl_transaksi)
        
    #hh , mm , ss = oTransaksi.Jam_Input[3:6]
    
    # General Info
    dictValidasi = {}
    dictValidasi['INPUTER'] = oTransaksi.User_Info    
    dictValidasi['TANGGAL_TRANSAKSI'] = "%s/%s/%s" %(str(dt[2]).zfill(2), str(dt[1]).zfill(2), str(dt[0]).zfill(4))
    dictValidasi['TANGGAL_INPUT'] = "%s/%s/%s" %(str(oTransaksi.Date_Info[2]).zfill(2), str(oTransaksi.Date_Info[1]).zfill(2), str(oTransaksi.Date_Info[0]).zfill(4))         
    dictValidasi['JAM_INPUT'] = "%s:%s:%s" %(str(oTransaksi.Date_Info[3]).zfill(2), str(oTransaksi.Date_Info[4]).zfill(2), str(oTransaksi.Date_Info[5]).zfill(2))
    dictValidasi['NOMINAL_TRANSAKSI'] = config.FormatFloat('#,##0.00', oTransaksi.Nominal_Transaksi or 0.0)
    dictValidasi['NOMOR_REKENING_KREDIT'] = oTransaksi.Nomor_Rekening_Kredit
    dictValidasi['NAMA_REKENING_KREDIT'] = oTransaksi.Nama_Rekening_Kredit[:30]
    dictValidasi['NOMOR_REKENING_DEBET'] = oTransaksi.Nomor_Rekening_Debet
    dictValidasi['NAMA_REKENING_DEBET'] = oTransaksi.Nama_Rekening_Debet
    dictValidasi['NOMOR_REFERENSI'] = oTransaksi.Nomor_Ref_Deal
    dictValidasi['KODE_VALUTA'] = oTransaksi.Kode_Valuta
    dictValidasi['KODE_CABANG'] = oTransaksi.Kode_Cabang
        
    # Isi File
    LAYOUT = __tplLAYOUT1
    if oTransaksi.Nama_Rekening_Debet == 'FASBIS':
      LAYOUT = __tplLAYOUT1
    else:
      LAYOUT = __tplLAYOUT2  
      
    data_validasi = chr(15)
    for rowLAYOUT in LAYOUT :
      data_validasi += rowLAYOUT.format(**dictValidasi)  + '\n'
    #raise Exception,data_validasi
   
    # Set Full Path 
    sBaseFileName = "validasi.txt"
    sFileName = config.UserHomeDirectory + sBaseFileName
    
    fileutils.SafeDeleteFile(sFileName)
    
    #raise Exception,data_validasi 
                   
    # Write To File 
    oFile = open(sFileName,'w')
    oFile.write(data_validasi)
    oFile.close()

    return sFileName    

def currFormatStr(dValue, formatType='D', floatPrecision=2):
  pemisahRibuan = '.'
  pemisahDesimal = ','
  
  if formatType == 'I': 
    sNominal = str(int(dValue))
    
    sNominalBulat = sNominal
    sNominalDesimal = ''
  elif formatType == 'D': 
    sNominal = str(round(dValue, floatPrecision))
    
    lsNominal = (sNominal.split('.'))
    sNominalBulat = lsNominal[0]
    sNominalDesimal = lsNominal[1]
  else: raise Exception, 'Undefined format type.'
  
  lenNom = len(sNominalBulat)
  iterRibuan = lenNom/3
  iterRibuanSisa = lenNom - (iterRibuan*3)
  currFormat = sNominalBulat[:iterRibuanSisa]
  for i in range(iterRibuan):
    if currFormat != '': currFormat += pemisahRibuan
    currFormat += sNominalBulat[iterRibuanSisa+(i*3): (iterRibuanSisa+((i+1)*3))]
  #--
  
  if formatType == 'D': currFormat += pemisahDesimal + sNominalDesimal.ljust(floatPrecision, '0')
  return currFormat
#--