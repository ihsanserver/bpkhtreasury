import os
import sys
import com.ihsan.lib.remotequery as remotequery
import com.ihsan.net.message as message
import com.ihsan.util.modman as modman
import openpyxl
from openpyxl.cell import get_column_letter
from openpyxl.styles import Border
from openpyxl.styles import Font
from openpyxl import Workbook
import com.ihsan.fileutils as fileutils
from copy import deepcopy
from com.ihsan.lib import queryreport
import com.ihsan.util.dbutil as dbutil
import com.ihsan.util.attrutil as atutil
import calendar
import datetime
from datetime import datetime

modman.loadStdModules(globals(), ['Debug'])
_dbg_excmsg = Debug.getExcMsg

GRID_ACCOUNT = 'gridsettings.listaccount_cols'

def dbg(msg):
  #app.ConWriteln(msg)
  #app.ConRead(msg2)
  message.send_udp(msg + "\n", 'localhost', 9122)

def getSQLClient(config, params):
  #rpdb2.start_embedded_debugger('000')
  mlu = config.ModLibUtils  
  param = params.FirstRecord
  tglIndo = modman.getModule(config,'TglIndo')

  #raise Exception, param.all_cabang
  r_tgl = ''
  lcond = [''' ta.jenis_treasuryaccount in ('%s') ''' % param.jenis_treasuryaccount]
  lcond.append( ''' ('1'='%(all_cabang)s' or rt.kode_cabang = '%(kode_cabang)s') ''' % {'kode_cabang':param.kode_cabang,'all_cabang':param.all_cabang} )
  lcond.append( ''' ('0'='%(sr)s' or rt.status_rekening = %(sr)s ) ''' % {'sr':param.status_rekening}) 

  fl_tgl = 'balance_date'
  if param.type_report in ('3', '4'):
    fl_tgl = 'tr.tanggal_transaksi'
    if param.kode_remark <> "ALL":
      lcond.append( ''' tr.keterangan_tambahan='%s' ''' % param.kode_remark)

  if param.kode_produk <> "ALL":
    lcond.append( ''' ta.kode_produk='%s' ''' % param.kode_produk)
    
  if param.kode_counterpart <> "ALL":                                 
    lcond.append( ''' ta.kode_counterpart='%s' ''' % param.kode_counterpart) 
    
  if param.kode_valuta <> "ALL":                                 
    lcond.append( ''' rt.kode_valuta='%s' ''' % param.kode_valuta)

  if param.is_suspend == "T":                                 
    lcond.append( ''' nvl(hj.nomor_identitas,'REG')='SUSPEND' ''')
  if param.is_suspend == "F":                                 
    lcond.append( ''' nvl(hj.nomor_identitas,'REG')<>'SUSPEND' ''')
      
  tx_awal=''
  tx_akhir = '''to_date('%s', 'dd/mm/yyyy')>=%s ''' % (tglIndo.tgl_indo(config,param.tgl_acc,10), fl_tgl) 
  if param.tx_awal > 0:
    tx_awal = ''' and to_date('%s', 'dd/mm/yyyy')<=%s ''' % (tglIndo.tgl_indo(config,param.tx_awal,10), fl_tgl)
  if param.tx_akhir > 0:
    tx_akhir = '''to_date('%s', 'dd/mm/yyyy')>=%s ''' % (tglIndo.tgl_indo(config,param.tx_akhir,10), fl_tgl)
      
  if param.tgl_awal > 0:
    lcond.append( '''to_date('%s', 'dd/mm/yyyy')<=ta.tgl_buka ''' % (tglIndo.tgl_indo(config,param.tgl_awal,10)) )
    r_tgl += 'TGL BUKA %s ' % (tglIndo.tgl_indo(config,param.tgl_awal,2))
  if param.tgl_akhir > 0:
    lcond.append( '''to_date('%s', 'dd/mm/yyyy')>=ta.tgl_buka ''' % (tglIndo.tgl_indo(config,param.tgl_akhir,10)) )
    r_tgl += '- %s' % (tglIndo.tgl_indo(config,param.tgl_akhir,2))
      

  #raise Exception, lcond
  strOqlCond = ''      
  if len(lcond) > 0 :   
    strOqlCond = " AND ".join(lcond)
    #strOqlCond = strOqlCond
 
  _dict = {
    'DailyBalanceHaji':config.MapDBTableName("DailyBalanceHaji"),
    'treasuryaccount':config.MapDBTableName("treasuryaccount"),
    'rekeningtransaksi':config.MapDBTableName("core.rekeningtransaksi"),
    'hajjaccount':config.MapDBTableName("hajjaccount"),
    'treaproduk':config.MapDBTableName("treaproduk"),
    'treacounterpart':config.MapDBTableName("treacounterpart"), 

    'transaksi':config.MapDBTableName("transaksi"), 
    'detiltransaksi':config.MapDBTableName("detiltransaksi"), 
    'histtransaksi':config.MapDBTableName("histtransaksi"), 
    'histdetiltransaksi':config.MapDBTableName("histdetiltransaksi"), 

    'listperanuser':config.MapDBTableName("enterprise.listperanuser"), 
    'listcabangdiizinkan':config.MapDBTableName("enterprise.listcabangdiizinkan"), 
    'id_user': mlu.QuotedStr(config.SecurityContext.InitUser),
    'tx_awal':tx_awal,
    'tx_akhir':tx_akhir,
    'strOqlCond':strOqlCond
  } 
  
  sSQL = '''
    SELECT b.tipe_akses_cabang FROM %(userapp)s b WHERE b.id_user='%(id_user)s' 
  ''' % {
    'userapp': config.MapDBTableName("enterprise.userapp"),
    'id_user':config.SecurityContext.InitUser
  }
  rSQL = config.CreateSQL(sSQL).RawResult

  _dict['LCDJoinType'] = 'LEFT JOIN' if rSQL.tipe_akses_cabang=='S' else 'INNER JOIN'
 	
  sql = {}
  if param.type_report == '1':
    sSQL = '''
      SELECT 
        hj.nomor_porsi, rt.nama_rekening
        , ta.tgl_buka, rt.kode_valuta
        , ta.kode_counterpart, tc.nama_counterpart
        , ta.kode_produk, ta.nomor_ref_deal no_rek_bank
        , hj.norek_sa, hj.nomor_rekening
        , hj.nomor_identitas issuspend
        , db.balance_sa as setoran_awal
        , db.balance_sl as setoran_lunas
        , db.balance_td as setoran_tunda
        , db.balance_nm as nilai_manfaat
        , db.status_rekening
        , dbm.*
      FROM %(DailyBalanceHaji)s db
        INNER JOIN %(hajjaccount)s hj ON db.nomor_rekening=hj.nomor_rekening
        INNER JOIN %(treasuryaccount)s ta ON db.nomor_rekening=ta.nomor_rekening
        INNER JOIN %(rekeningtransaksi)s rt ON db.nomor_rekening=rt.nomor_rekening
        INNER JOIN %(treacounterpart)s tc ON ta.kode_counterpart=tc.kode_counterpart
        INNER JOIN (
          SELECT nomor_rekening, Max(balance_date) max_date, Min(balance_date) min_date
            , Sum(credit_sa) mut_kredit_sa, Sum(debit_sa) mut_debit_sa
            , Sum(credit_sl) mut_kredit_sl, Sum(debit_sl) mut_debit_sl
            , Sum(credit_td) mut_kredit_td, Sum(debit_td) mut_debit_td
            , Sum(credit_nm) mut_kredit_nm, Sum(debit_nm) mut_debit_nm
          FROM %(DailyBalanceHaji)s
          WHERE %(tx_akhir)s %(tx_awal)s
          GROUP BY nomor_rekening
        ) dbm ON db.nomor_rekening=dbm.nomor_rekening AND db.balance_date=dbm.max_date
  
        %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = rt.kode_cabang AND lc.id_user = %(id_user)s
  	''' % _dict
    _col = 'H'
    
    sql['SELECTFROMClause'] = sSQL
    sql['WHEREClause'] = '''
      %(strOqlCond)s
    ''' % _dict
    
    sql['GROUPBYClause'] = ''
    sql['keyFieldName'] = 'ta.tgl_buka'
    sql['altOrderFieldNames'] = 'ta.tgl_buka;tc.nama_counterpart'
    sql['baseOrderFieldNames'] = 'ta.tgl_buka;tc.nama_counterpart'
  
  elif param.type_report == '2':
    sSQL = '''
      select 
        ta.kode_produk, tc.nama_counterpart, rt.kode_valuta
        , count(1) as jml
        , sum(db.balance_sa) as setoran_awal
        , sum(db.balance_sl) as setoran_lunas
        , sum(db.balance_td) as setoran_tunda
        , sum(db.balance_nm) as nilai_manfaat
    
        , sum(mut_kredit_sa) as mut_kredit_sa
        , sum(mut_debit_sa) as mut_debit_sa
        , sum(mut_kredit_sl) as mut_kredit_sl
        , sum(mut_debit_sl) as mut_debit_sl
        , sum(mut_kredit_td) as mut_kredit_td
        , sum(mut_debit_td) as mut_debit_td
        , sum(mut_kredit_nm) as mut_kredit_nm
        , sum(mut_debit_nm) as mut_debit_nm
      FROM %(DailyBalanceHaji)s db
        INNER JOIN %(hajjaccount)s hj ON db.nomor_rekening=hj.nomor_rekening
        INNER JOIN %(treasuryaccount)s ta ON db.nomor_rekening=ta.nomor_rekening
        INNER JOIN %(rekeningtransaksi)s rt ON db.nomor_rekening=rt.nomor_rekening
        INNER JOIN %(treacounterpart)s tc ON ta.kode_counterpart=tc.kode_counterpart
        INNER JOIN (
          SELECT nomor_rekening, Max(balance_date) max_date, Min(balance_date) min_date
            , Sum(credit_sa) mut_kredit_sa, Sum(debit_sa) mut_debit_sa
            , Sum(credit_sl) mut_kredit_sl, Sum(debit_sl) mut_debit_sl
            , Sum(credit_td) mut_kredit_td, Sum(debit_td) mut_debit_td
            , Sum(credit_nm) mut_kredit_nm, Sum(debit_nm) mut_debit_nm
          FROM %(DailyBalanceHaji)s
          WHERE %(tx_akhir)s %(tx_awal)s
          GROUP BY nomor_rekening
        ) dbm ON db.nomor_rekening=dbm.nomor_rekening AND db.balance_date=dbm.max_date
        %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = rt.kode_cabang AND lc.id_user = %(id_user)s
  	''' % _dict
    _col = 'G'
     
    sql['SELECTFROMClause'] = sSQL
    sql['WHEREClause'] = '''
      %(strOqlCond)s
    ''' % _dict
    sql['GROUPBYClause'] = '''GROUP BY ta.kode_produk, tc.nama_counterpart, rt.kode_valuta'''
    
    sql['keyFieldName'] = 'kode_produk'
    sql['altOrderFieldNames'] = 'kode_produk;nama_counterpart'
    sql['baseOrderFieldNames'] = 'kode_produk;nama_counterpart'
  
  elif param.type_report == '3':
    sSQL = '''
      select * from (
        SELECT 
          tr.keterangan_tambahan kd_remark, dt.kode_valuta
          , tc.kode_counterpart, tc.nama_counterpart
          , Decode(dt.jenis_mutasi,'C',dt.nilai_mutasi,-dt.nilai_mutasi) mutasi_tx
          , tr.tanggal_transaksi, dt.Nomor_Referensi, rt.nama_rekening
          , hj.nomor_porsi, hj.nomor_rekening
        FROM %(transaksi)s tr 
          INNER JOIN %(detiltransaksi)s dt on tr.id_transaksi=dt.id_transaksi
          INNER JOIN %(hajjaccount)s hj ON dt.nomor_rekening=hj.nomor_rekening
          INNER JOIN %(treasuryaccount)s ta on dt.nomor_rekening=ta.nomor_rekening
          INNER JOIN %(rekeningtransaksi)s rt on dt.nomor_rekening=rt.nomor_rekening
          INNER JOIN %(treacounterpart)s tc on tc.kode_counterpart=ta.kode_counterpart
          %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = rt.kode_cabang AND lc.id_user = %(id_user)s
        WHERE 
          %(strOqlCond)s AND %(tx_akhir)s %(tx_awal)s

        UNION ALL
        
        SELECT 
          tr.keterangan_tambahan kd_remark, dt.kode_valuta
          , tc.kode_counterpart, tc.nama_counterpart
          , Decode(dt.jenis_mutasi,'C',dt.nilai_mutasi,-dt.nilai_mutasi) mutasi_tx
          , tr.tanggal_transaksi, dt.Nomor_Referensi, rt.nama_rekening
          , hj.nomor_porsi, hj.nomor_rekening
        FROM %(histtransaksi)s tr 
          INNER JOIN %(histdetiltransaksi)s dt on tr.id_transaksi=dt.id_transaksi
          INNER JOIN %(hajjaccount)s hj ON dt.nomor_rekening=hj.nomor_rekening
          INNER JOIN %(treasuryaccount)s ta on dt.nomor_rekening=ta.nomor_rekening
          INNER JOIN %(rekeningtransaksi)s rt on dt.nomor_rekening=rt.nomor_rekening
          INNER JOIN %(treacounterpart)s tc on tc.kode_counterpart=ta.kode_counterpart
          %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = rt.kode_cabang AND lc.id_user = %(id_user)s
        WHERE 
          %(strOqlCond)s AND %(tx_akhir)s %(tx_awal)s
      ) tr
  	''' % _dict
    _col = '3'
     
    sql['SELECTFROMClause'] = sSQL
    sql['WHEREClause'] = "1=1"
    sql['GROUPBYClause'] = ""
    
    sql['keyFieldName'] = 'tr.tanggal_transaksi'
    sql['altOrderFieldNames'] = 'tr.tanggal_transaksi;nama_counterpart'
    sql['baseOrderFieldNames'] = 'tr.tanggal_transaksi;nama_counterpart'
  
  elif param.type_report == '4':
    sSQL = '''
      SELECT 
        tr.keterangan_tambahan kd_remark, dt.kode_valuta
        , tc.kode_counterpart, tc.nama_counterpart
        , sum(Decode(dt.jenis_mutasi,'C',dt.nilai_mutasi,-dt.nilai_mutasi)) mutasi_tx
        , count(1) jml_tx
      FROM %(transaksi)s tr 
        INNER JOIN %(detiltransaksi)s dt on tr.id_transaksi=dt.id_transaksi
        INNER JOIN %(hajjaccount)s hj ON dt.nomor_rekening=hj.nomor_rekening
        INNER JOIN %(treasuryaccount)s ta on dt.nomor_rekening=ta.nomor_rekening
        INNER JOIN %(rekeningtransaksi)s rt on dt.nomor_rekening=rt.nomor_rekening
        INNER JOIN %(treacounterpart)s tc on tc.kode_counterpart=ta.kode_counterpart
        %(LCDJoinType)s %(listcabangdiizinkan)s lc ON lc.kode_cabang = rt.kode_cabang AND lc.id_user = %(id_user)s
  	''' % _dict
    _col = 'G'
     
    sql['SELECTFROMClause'] = sSQL
    sql['WHEREClause'] = '''
      %(strOqlCond)s AND %(tx_akhir)s %(tx_awal)s
    ''' % _dict
    sql['GROUPBYClause'] = '''GROUP BY tr.keterangan_tambahan, dt.kode_valuta, tc.kode_counterpart, tc.nama_counterpart'''
    
    sql['keyFieldName'] = 'kd_remark'
    sql['altOrderFieldNames'] = 'kd_remark;nama_counterpart'
    sql['baseOrderFieldNames'] = 'kd_remark;nama_counterpart'
  
  
  gsModule = modman.getModule(config, GRID_ACCOUNT)
  sql['columnSetting'] = gsModule.cols_setting(param.jenis_treasuryaccount, param.type_report)
  sql['r_tgl'] = r_tgl
  #sql['excel_cols']= EXCEL_COLUMN
  #sql['judul2']= judul2

  return sql
#--
  
def runQuery(config, params, returns):
  rq = remotequery.RQSQL(config)
  rq.handleOperation(params, returns)
#--

def getQueryData(config, clientpacket, returnpacket):
  param = clientpacket.FirstRecord
  ds = returnpacket.AddNewDatasetEx("status", "error_status: integer; error_message: string;")
  rec = ds.AddRecord()

  sqlStat = getSQLClient(config, clientpacket)
  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('1...')
  '''
  sSQL = sqlStat['SELECTFROMClause']+' WHERE '+sqlStat['WHEREClause']+' GROUP BY '+sqlStat['GROUPBYClause']
  app.ConWriteln(sSQL)
  app.ConRead('')
  #raise Exception, sqlStat['SELECTFROMClause']+sqlStat['WHEREClause']
  #'''
  try :                        
    app.ConWriteln('2...')
    rqsql = remotequery.RQSQL(config)   
    app.ConWriteln('3...')
    rqsql.SELECTFROMClause = sqlStat['SELECTFROMClause']
    rqsql.WHEREClause = sqlStat['WHEREClause']
    rqsql.GROUPBYClause = sqlStat.get('GROUPBYClause', '')
    rqsql.setAltOrderFieldNames(sqlStat['altOrderFieldNames'])
    rqsql.keyFieldName = sqlStat['keyFieldName']
    rqsql.setBaseOrderFieldNames(sqlStat['baseOrderFieldNames'])
    rqsql.columnSetting = sqlStat.get('columnSetting', '') 
    app.ConWriteln('4...')
    app.ConWriteln('...Please Wait...')
    rqsql.initOperation(returnpacket)
    app.ConWriteln('5...')

    errorStatus = 0
    errorMessage = ""
  except:
    errorStatus = 1
    errorMessage = "%s.%s" % (str(sys.exc_info()[0]),  str(sys.exc_info()[1]))
  #--
  
  # pattern untuk catch status dan error
  rec.error_status = errorStatus
  rec.error_message = errorMessage
  return 1
#--  

#========================= Export Data===================================#
def styleCell(owb, R_, C_):
  #style border
  owb.cell(row=R_,column=C_).style.font.name = 'Trebuchet MS'
  owb.cell(row=R_,column=C_).style.font.size = 10
  owb.cell(row=R_,column=C_).style.borders.top.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.bottom.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.right.border_style = Border.BORDER_THIN
  owb.cell(row=R_,column=C_).style.borders.left.border_style = Border.BORDER_THIN
#--

def NumberFormat(owb, R_, C_):
  owb.cell(row=R_,column=C_).style.number_format.format_code = '#,##0.00�'
#--

def GenerateFile(config, params, returns):
  sqlStat = getSQLClient(config, params)
  try:
    status = returns.CreateValues(
      ['sucsess',0],
      ['Is_Err',0],['Err_Message','']
    )
    
    rec = params.FirstRecord
    oRec = atutil.GeneralObject(rec)    
    app = config.AppObject
    app.ConCreate('out')
    app.ConWriteln('Inisialisasi proses generate template...')
          
    if rec.type_report == '1':
      reportFile = GenerateReportRekap(config, sqlStat, rec.type_report)
    else:
      reportFile = GenerateReportRekap(config, sqlStat, rec.type_report)
     
    #return packet
    mmtype = reportFile.split(".")[-1]
    sw = returns.AddStreamWrapper()
    sw.name = 'gen_report'
    sw.LoadFromFile(reportFile)                       
    if mmtype=='xlsx':
      sw.MIMEType = ".%s" % mmtype
    else:
     sw.MIMEType = config.AppObject.GetMIMETypeFromExtension(reportFile)
  except:
    status.Is_Err = True
    status.Err_Message = _dbg_excmsg()#str(sys.exc_info()[1])
    
def GenerateReportRekap(config, sql, _col='2'):    
    app = config.AppObject
    app.ConCreate('out')
    app.ConWriteln('Inisialisasi proses generate template...') 
     
    fileNamePath = config.HomeDir + 'template\\tpl_fasbis_int.xlsx'
    
    wb = openpyxl.load_workbook(fileNamePath)
    wsExcel = wb.get_sheet_by_name('data')
    
    order_by = sql['baseOrderFieldNames'].replace(';',',')
    sSQL = sql['SELECTFROMClause']+' WHERE '+sql['WHEREClause']+sql['GROUPBYClause']+' ORDER BY '+order_by
    #app.ConWriteln(sSQL)
    #app.ConRead('')
    app.ConWriteln('...Please Wait...')
    q = config.CreateSQL(sSQL).RawResult
    q.First()
    
    gsModule = modman.getModule(config, GRID_ACCOUNT)
    _cols = gsModule.ex_cols('H', _col)    
        
    #header  
    wsExcel.cell(row=0,column=0).value = 'LAPORAN REKAP TITIPAN REKENING JAMAAH'
    wsExcel.cell(row=1,column=0).value = sql['r_tgl']
    wsExcel.cell(row=2,column=0).value = 'TREASURY' 
    
    startLine = 4
    styleCell(wsExcel, startLine, 0)
    wsExcel.cell(row=startLine,column=0).style.font.bold = True
    wsExcel.cell(row=startLine,column=0).style.alignment.wrap_text= True
    wsExcel.cell(row=startLine,column=0).style.alignment.horizontal = "center"
    wsExcel.cell(row=startLine,column=0).style.alignment.vertical = "center"
    wsExcel.cell(row=startLine,column=0).value = 'NO.'  
    wsExcel.column_dimensions['A'].width = 6
    
    _rCol = 1
    for i in range(len(_cols)):
      ls = _cols[i][0].split(";") 
      styleCell(wsExcel, startLine, _rCol)
      wsExcel.cell(row=startLine,column=_rCol).style.font.bold = True 
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.wrap_text= True
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.horizontal = "center"
      wsExcel.cell(row=startLine,column=_rCol).style.alignment.vertical = "center"
      wsExcel.cell(row=startLine,column=_rCol).value = '%s' % (ls[1])
      _rCol += 1
             
    _rRow = 5
    _no = 1
    while not q.Eof:   
      _rCol = 1
      styleCell(wsExcel, _rRow, 0)
      wsExcel.cell(row=_rRow,column=0).style.alignment.horizontal = "center"
      wsExcel.cell(row=_rRow,column=0).value = _no 
      for i in _cols:
        ls = i[0].split(";")
        #'''
        if i[1]=='D':
         tgl = eval('q.'+ls[0])
         nVal = "%s-%s-%s" %(str(tgl[0]).zfill(4), str(tgl[1]).zfill(2), str(tgl[2]).zfill(2)) if tgl<>None else ''
        else:
         nVal = eval('q.'+ls[0])
        #'''
        #nVal = eval('q.'+ls[0])
        _col = get_column_letter(_rCol+1)
        styleCell(wsExcel, _rRow, _rCol)
        wsExcel.cell(row=_rRow,column=_rCol).value = nVal or '' 
        #wsExcel.column_dimensions[_col].width = i[2]
        
        if i[1]=='F':
          wsExcel.cell(row=_rRow,column=_rCol).style.number_format.format_code = '#,##0.00�'
            
        _rCol += 1
      
      _no += 1  
      _rRow += 1        
      q.Next()
          
    fileHasil = config.UserHomeDirectory + 'reportfasbis.xlsx'    
    fileutils.SafeDeleteFile(fileHasil) #Melakukan Hapus File yang sudah ada sebelumnya
    wb.save(fileHasil)
    
    return fileHasil