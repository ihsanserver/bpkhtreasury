import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper

# GLOBALS
_DEBUG   = True
_CONSOLE = True
app      = None

def printOut(sOut):
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--

def updateResources(sqlRes):
  global SQL_RESOURCES
  
  SQL_RESOURCES.update(sqlRes)
  
def executeFlow(config, SQLFlows, SQLParam):
  for SQLFlow in SQLFlows:
    printOut('>> executing ' + SQLFlow)
    SQL = SQL_RESOURCES[SQLFlow].format(**SQLParam)
    
    if _DEBUG: 
      printOut(SQL) 
    
    dbutil.runSQL(config, SQL)
  #--

def execBatchFlow(config, SQLFlows, dictParam, SQLParams):
  for SQLFlow in SQLFlows:
    printOut('>> executing ' + SQLFlow)
    for SQLParam in SQLParams:
      printOut('>>> processing ' + str(SQLParam))
      SQLParam.update(dictParam)
      SQL = SQL_RESOURCES[SQLFlow].format(**SQLParam)
      
      if _DEBUG:
        printOut(SQL)
         
      dbutil.runSQL(config, SQL)
    #--
  #--  

def GetBiayaFromParameter(helper, Kode_Grup):
       
  oGrupBiaya = helper.GetObjectByNames(
    'GrupParameterBiaya',
    {'Kode_Grup_Biaya' : Kode_Grup}
  ).CastToLowestDescendant()
  
  if oGrupBiaya.isnull :
     return 0.0
  else:
     return oGrupBiaya.GetTotalBiaya()

def GetParameterNumber(helper, paramName) :
  Nilai_Parameter = 0.0
  oParam = helper.GetObject('ParameterGlobal', paramName)
  if not oParam.IsNull: 
    Nilai_Parameter = oParam.Nilai_Parameter or 0.0
  
  return Nilai_Parameter     

def GetParameterString(helper, paramName) :
  Nilai_Parameter = None
  oParam = helper.GetObject('ParameterGlobal', paramName)
  if not oParam.IsNull: 
    Nilai_Parameter = oParam.Nilai_Parameter_String
      
  return Nilai_Parameter     

def setTodayForJournal(config, dictParam, oToday):
  y, m, d = config.ModLibUtils.DecodeDate(oToday.GetDate())
  dictParam.update({
    'FToday'   : oToday.GetFlatText()
    , 'YToday' : str(y)
    , 'MToday' : str(m)
    , 'DToday' : str(d) 
  })

