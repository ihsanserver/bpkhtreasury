import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil
import calendar

# PRACONDITION: ALL EOD TEMPORARY TABLES HAS BEEN CREATED
# IF NOT, RUN pod_tmp_tables.py first !

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}, 
  {'name': 'sDate', 'short_desc': 'process date', 'long_desc': 'process date is in format dd-MMM-yyyy example: 01-Jan-2013'}
]

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

# GLOBALS
_CONSOLE = False
if not _CONSOLE:
  config = appserver.ActiveConfig
  app    = config.AppObject
#--
dParam={}

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
  app = config.AppObject
  ioutil.IO_MODE = ioutil.IOMODE_DAF
  ioutil.APP_OBJECT = app
  app.ConCreate('out')
                                
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1
#-- END OF SCRIPT INJECTION
    
def main(config, debugLevel = None):
  #metaConfig = argValues['metaConfig']
  #strDate = argValues['sDate']   
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('>> Mulai proses')  
  
  if config == None: config = podUtil.openConfig(metaConfig)
  dbutil.DEBUG_LEVEL = debugLevel or 1
      
  runTreasuryReversePPAP(config, strDate)
  
  app.ConWriteln('>> Proses selesai')
#--
    
def runTreasuryReversePPAP(config, strDate):
  
  MASTER_SRC_TABLE = 'tmp.ftmp_treasury_rev_ppap'
  BALANCE_FIELDS = ['ppap_umum', 'ppap_khusus']
  MUT_FIELDS = ['mut_ppap_umum', 'mut_ppap_khusus', 'mut_pdpt_ppap_umum', 'mut_pdpt_ppap_khusus']#, 'mut_by_ppap_umum', 'mut_by_ppap_khusus']
  MUT_CLASSGROUP_FIELDS = [
    #mut ckpn
    {'field': 'mut_ppap_umum', 'classgroup': 'ppap_umum'},
    {'field': 'mut_ppap_khusus', 'classgroup': 'ppap_khusus'},
    #mut pdpt rev. ckpn
    {'field': 'mut_pdpt_ppap_umum', 'classgroup': 'pdpt_rev_ppap_umum'},
    {'field': 'mut_pdpt_ppap_khusus', 'classgroup': 'pdpt_rev_ppap_khusus'},
    #mut by ckpn
    #{'field': 'mut_by_ppap_umum', 'classgroup': 'by_ppap_umum'},
    #{'field': 'mut_by_ppap_khusus', 'classgroup': 'by_ppap_khusus'},
  ]
  
  mlu = config.ModLibUtils
  pod_fin_common = modman.getModule(config, 'server_scripts#pod_common_collective')

  dictParam = {
    'date': mlu.QuotedStr(strDate),
    'srcTableName': config.MapDBTableName(MASTER_SRC_TABLE),
    'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
    'treasuryaccount': config.MapDBTableName('treasuryaccount'),
    'treafasbis': config.MapDBTableName('treafasbis'),
    'treasima': config.MapDBTableName('treasima'),
    'treaproduk': config.MapDBTableName('treaproduk'),
    'jenistreasury': config.MapDBTableName('jenistreasury')
  }
  dParam.update(dictParam)
  
  selectData(config)
  pod_fin_common.initBalanceAndMutationFields(config, MASTER_SRC_TABLE, 
    BALANCE_FIELDS,
    MUT_FIELDS  
  )
  createTxValues(config)
  deleteNoTxRows(config)

  pod_fin_common.prepareStdTxFields(config, MASTER_SRC_TABLE)
  pod_fin_common.createTxDetails(config, MASTER_SRC_TABLE, 'trea_ppap', MUT_CLASSGROUP_FIELDS)
  pod_fin_common.createCoreTransaction(config, MASTER_SRC_TABLE, 'trea_ppap', strDate)
  pod_fin_common.updateBalanceFromTransaction(config, MASTER_SRC_TABLE, MUT_CLASSGROUP_FIELDS)
  postProcess(config)
  
def selectData(config):
  
  dbutil.runSQL(config, '''
     DELETE FROM %(srcTableName)s
    ''' % dParam
  )

  #update by BG penjumlahan timpstamp dalam orecle hasilnya <> number, harus di extract[day,hour,minute,second] jika ingin dapat number
  dbutil.runSQL(config, '''  
    INSERT INTO %(srcTableName)s (
      nomor_rekening,
      ppap_umum,
      ppap_khusus,
      
      ktr_detil, ktr_transaksi, ktr_detiltransgroup
    )
    SELECT 
      a.nomor_rekening
      , a.ppap_umum
      , a.ppap_khusus
      , 'REVERSE CKPN '||d.keterangan
      , 'REVERSE CKPN '||d.keterangan 
      , 'REVERSE CKPN '||d.keterangan ||' - ' || a.nomor_rekening
    FROM %(treasuryaccount)s a
      INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
      left JOIN %(treaproduk)s c ON a.kode_produk=c.kode_produk
      left JOIN %(jenistreasury)s d ON a.jenis_treasuryaccount=d.jenis_treasuryaccount
    WHERE b.status_rekening=3
      and Nvl(a.ppap_umum,0)+Nvl(a.ppap_khusus,0) > 0
    ''' % dParam
  )
        
  dbutil.runSQL(config, '''  
    select 1 FROM %(treasuryaccount)s WHERE nomor_rekening in
    (select nomor_rekening from %(srcTableName)s) FOR UPDATE NOWAIT
    ''' % dParam
  )
      
# generate transaction values in columns  
def createTxValues(config):
  #set mutasi transaksi
  dbutil.runSQL(config, '''
    UPDATE %(srcTableName)s SET
      mut_ppap_umum          = -ppap_umum 
      , mut_ppap_khusus      = -ppap_khusus 
      , mut_pdpt_ppap_umum   = ppap_umum 
      , mut_pdpt_ppap_khusus = ppap_khusus
    ''' % dParam
  )
#--

# delete no-transaction rows to reduce processing request
def deleteNoTxRows(config):
  pass  
#--   

def postProcess(config):
  pass  
#--
  
if __name__ == "__main__":
  podUtil.printArgs()
  argValues = podUtil.checkArgs('pod_treasury_rev_ppap.py', ARG_LIST, 2, False)
  dbutil.DEBUG_LEVEL = 0
  main(argValues)
#--
  