import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil
import calendar

# PRACONDITION: ALL EOD TEMPORARY TABLES HAS BEEN CREATED
# IF NOT, RUN pod_tmp_tables.py first !

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}, 
  {'name': 'sDate', 'short_desc': 'process date', 'long_desc': 'process date is in format dd-MMM-yyyy example: 01-Jan-2013'}
]

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

# GLOBALS
_CONSOLE = False
if not _CONSOLE:
  config = appserver.ActiveConfig
  app    = config.AppObject
#--
dParam={}

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
  app = config.AppObject
  ioutil.IO_MODE = ioutil.IOMODE_DAF
  ioutil.APP_OBJECT = app
  app.ConCreate('out')
                                
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1
#-- END OF SCRIPT INJECTION
    
def main(config, debugLevel = None):
  #metaConfig = argValues['metaConfig']
  #strDate = argValues['sDate']   
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('>> Mulai proses')  
  
  if config == None: config = podUtil.openConfig(metaConfig)
  dbutil.DEBUG_LEVEL = debugLevel or 1
      
  runTreasuryMaturity(config, strDate)
  
  app.ConWriteln('>> Proses selesai')
#--
    
def runTreasuryMaturity(config, strDate):
  
  MASTER_SRC_TABLE = 'tmp.ftmp_treasury_maturity'
  BALANCE_FIELDS = ['saldo', 'saldo_accrue']     
  MUT_FIELDS = ['mut_pymad', 'mut_profit_accrue', 'mut_saldo', 'mut_profit_amount', 'mut_payment_balance', ] #, 'mut_giro_bi'
  MUT_CLASSGROUP_FIELDS = [
    {'field': 'mut_saldo', 'classgroup': 'tr_nominal'},
    #{'field': 'mut_giro_bi', 'classgroup': 'tr_giro_bi'},
    {'field': 'mut_payment_balance', 'classgroup': 'penutupan'},
    {'field': 'mut_profit_amount', 'classgroup': 'tr_profit'},
    {'field': 'mut_pymad', 'classgroup': 'tr_pymad'},
    {'field': 'mut_profit_accrue', 'classgroup': 'tr_accrue'}
  ]
                           
  #create detil transaksi rekening liability               
  helper = phelper.PObjectHelper(config)
  norek_paymentsrc = helper.GetObject('ParameterGlobalTreasury', 'DEF_REKENING').nilai_parameter_string
  
  MUT_TDP_FIELDS = [
    {'field': 'mut_payment_balance', 'acc_code': norek_paymentsrc, 'ket':'Debet penutupan account'}
  ]
  
  mlu = config.ModLibUtils
  pod_fin_common = modman.getModule(config, 'server_scripts#pod_common_collective')

  dictParam = {
    'date': mlu.QuotedStr(strDate),
    'norek_paymentsrc': norek_paymentsrc,
    'srcTableName': config.MapDBTableName(MASTER_SRC_TABLE),
    'detiltransaksi': config.MapDBTableName('core.detiltransaksi'),
    'seq_detiltransaksi': config.MapDBTableName('core.seq_detiltransaksi'),
    'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
    'rekeningliabilitas': config.MapDBTableName('core.rekeningliabilitas'),
    'transaksitransitkas': config.MapDBTableName('core.transaksitransitkas'),
    'glinterface_core': config.MapDBTableName('core.glinterface'),
    'treasuryaccount': config.MapDBTableName('treasuryaccount'),
    'treafasbis': config.MapDBTableName('treafasbis'),
    'treaproduk': config.MapDBTableName('treaproduk'),
    'jenistreasury': config.MapDBTableName('jenistreasury')
  }
  dParam.update(dictParam)
  
  selectData(config)
  pod_fin_common.initBalanceAndMutationFields(config, MASTER_SRC_TABLE, 
    BALANCE_FIELDS,
    MUT_FIELDS  
  )
  
  q = config.CreateSQL('''
    SELECT 
    	DISTINCT 
    		case 
    			when tipe_fasbis in ('F') then 'fasbis' 
    			when tipe_fasbis in ('S') then 'sbis'
    		end tipe_fasbis
    FROM %(treafasbis)s a
    INNER JOIN %(treasuryaccount)s b on a.nomor_rekening = b.nomor_rekening
    WHERE b.tgl_jatuh_tempo<=%(date)s
  ''' % dictParam).RawResult 
  while not q.Eof:
    app.ConWriteln('>> akd %s' % q.tipe_fasbis)  
    if q.tipe_fasbis not in [None,'']:
      createTxValues(config, q.tipe_fasbis)
    q.Next()
  
  deleteNoTxRows(config)

  pod_fin_common.prepareStdTxFields(config, MASTER_SRC_TABLE)
  pod_fin_common.createTxDetails(config, MASTER_SRC_TABLE, 'trea_maturity', MUT_CLASSGROUP_FIELDS)
  
  #debet rek sumber pembayaran untuk trx debet dari rek nasabah
  DebetRekNasabah(config, dParam, MUT_TDP_FIELDS)
  #Update saldo kasteler & liability
  UpdateSaldoRekening(config, dParam)
     
  pod_fin_common.createCoreTransaction(config, MASTER_SRC_TABLE, 'trea_maturity', strDate)
  pod_fin_common.updateBalanceFromTransaction(config, MASTER_SRC_TABLE, MUT_CLASSGROUP_FIELDS)
  postProcess(config)
  
def selectData(config):
  
  dbutil.runSQL(config, '''
     DELETE FROM %(srcTableName)s
    ''' % dParam
  )

  #update by BG penjumlahan timpstamp dalam orecle hasilnya <> number, harus di extract[day,hour,minute,second] jika ingin dapat number
  dbutil.runSQL(config, '''  
    INSERT INTO %(srcTableName)s (
      nomor_rekening,
      ujroh,
      saldo,
      saldo_accrue,
      norek_paymentsrc,
      
      ktr_detil, ktr_transaksi, ktr_detiltransgroup
    )
    SELECT 
      a.nomor_rekening
      , a.ujroh
      , b.saldo
      , a.saldo_accrue
      , %(norek_paymentsrc)s norek_paymentsrc
      , 'JATUH TEMPO '||d.keterangan
      , 'JATUH TEMPO '||d.keterangan 
      , 'JATUH TEMPO '||d.keterangan ||' - ' || a.nomor_rekening
    FROM %(treasuryaccount)s a
      INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
      left JOIN %(treaproduk)s c ON a.kode_produk=c.kode_produk
      left JOIN %(jenistreasury)s d ON a.jenis_treasuryaccount=d.jenis_treasuryaccount
    WHERE a.jenis_treasuryaccount IN ('F')
      AND b.status_rekening=1
      and a.tgl_jatuh_tempo<=%(date)s
    ''' % dParam
  )
        
  dbutil.runSQL(config, '''  
    select 1 FROM %(treasuryaccount)s WHERE nomor_rekening in
    (select nomor_rekening from %(srcTableName)s) FOR UPDATE NOWAIT
    ''' % dParam
  )
      
# generate transaction values in columns  
def createTxValues(config, tipe_fasbis):
  #update by BG add NVL(accrued_amount)
  if tipe_fasbis == 'fasbis':
    dbutil.runSQL(config, '''
      UPDATE %(srcTableName)s SET 
        mut_payment_balance =(saldo-ujroh)
        , mut_saldo =-saldo 
        , mut_profit_amount = ujroh 
      ''' % dParam
    )
  elif tipe_fasbis == 'sbis':
    dbutil.runSQL(config, '''
      UPDATE %(srcTableName)s SET
        mut_payment_balance =(saldo-ujroh)
        , mut_saldo =-saldo 
        , mut_profit_amount =ujroh 
        , mut_profit_accrue =saldo_accrue 
        , mut_pymad =-saldo_accrue 
      ''' % dParam
    )
#--

# delete no-transaction rows to reduce processing request
def deleteNoTxRows(config):
  pass  
#--

def DebetRekNasabah(config, dictParam, fieldNames):
  app = config.AppObject
  for fieldInfo in fieldNames:
    dParam = {
        'field': fieldInfo['field'],
        'acc_code': fieldInfo['acc_code'],
        'ket_detil': fieldInfo['ket']
      }
    dParam.update(dictParam)

    sSQL = '''  
        INSERT INTO %(detiltransaksi)s (
          Id_Detil_Transaksi, Tanggal_Transaksi, Jenis_Mutasi, Nilai_Mutasi, Keterangan, 
          Kode_Cabang, Kode_Valuta, Jenis_Detil_Transaksi, Nilai_Kurs_Manual, Nilai_Ekuivalen, 
          Saldo_Awal, Saldo_Akhir, Nomor_Referensi, Kode_Kurs, Kode_Account, 
          Kode_Valuta_RAK, Kode_Jurnal, Nominal_Biaya, IsBillerTransaction, hide_passbook, 
          hide_statement, hide_gentrans, subsystem_code, sub_tx_code, kode_subtx_class, 
          Id_Transaksi, Nomor_Rekening, kode_tx_class, id_detiltransgroup, Id_Parameter_Transaksi, 
          Id_Parameter, ID_JournalBlock
        )
        SELECT 
          %(seq_detiltransaksi)s.nextval, 
          TO_DATE(%(date)s), 
          'D' as mnemonic, 
          ms.mut_saldo + ms.mut_profit_amount as amount, 
          ms.ktr_detiltransgroup as keterangan, 
          rt.kode_cabang, ms.currency_code, 
          'L', 
          1.0 nilai_kurs, 
          ms.mut_saldo + ms.mut_profit_amount as n_eqv, 
          NULL, NULL, 'AUTOREF', 
          'BOOKING' as kode_kurs, 
          rl.kode_account as kode_account, 
          NULL, 
          '11' as kode_jurnal, 
          0.0, 'F', 'F', 
          'F', 'F', NULL, NULL, NULL, 
          ms.id_transaksi, 
          %(acc_code)s rek_liab, 
          NULL kode_tx_class, 
          ms.id_detiltransgroup 
          ,NULL, NULL, NULL
        FROM
          %(srcTableName)s ms
          inner join %(rekeningtransaksi)s rt on rt.nomor_rekening=ms.norek_paymentsrc
          inner join (
            select rl.nomor_rekening, rl.kode_produk, gli.kode_account from %(rekeningliabilitas)s rl
            inner join %(glinterface_core)s gli on rl.kode_produk=gli.kode_produk and gli.kode_interface='Saldo_Plus'
          ) rl on rl.nomor_rekening=ms.norek_paymentsrc      
    ''' % dParam 
    app.ConWriteln(sSQL)
    dbutil.runSQL(config, sSQL)
  #--
#--

def UpdateSaldoRekening(config, dParam): 
  #insert table transaksitransitkas
  sSQL = '''  
      INSERT INTO %(transaksitransitkas)s (
        Id_Detil_Transaksi, nominal_selisih_kurs, kode_posting
      )
      SELECT 
        dt.Id_Detil_Transaksi, null, 2
      FROM
        %(srcTableName)s ms
        inner join %(detiltransaksi)s dt on dt.id_transaksi=ms.id_transaksi      
      where dt.Jenis_Detil_Transaksi='K'    
  ''' % dParam
  dbutil.runSQL(config, sSQL)
  #--

  #update saldo rekening liability
  sSQL = '''  
      merge into %(rekeningtransaksi)s x using
      (
        SELECT 
          dt.nomor_rekening, sum(decode(jenis_mutasi,'D', -nilai_mutasi, nilai_mutasi)) jml_trx
        FROM
          %(srcTableName)s ms
          inner join %(detiltransaksi)s dt on dt.id_transaksi=ms.id_transaksi      
        where dt.Jenis_Detil_Transaksi='L'
        group by dt.nomor_rekening       
      ) y on (x.nomor_rekening=y.nomor_rekening)
      when matched then update set
         x.saldo = x.saldo+y.jml_trx
  ''' % dParam
  dbutil.runSQL(config, sSQL)
  #--
#---
    

def postProcess(config):
  #update status rekening jadi JT
  dbutil.runSQL(config, '''
    merge into %(rekeningtransaksi)s x using
      %(srcTableName)s y on (x.nomor_rekening=y.nomor_rekening)
    when matched then update set 
      x.status_rekening = 3 
    ''' % dParam
  )

  #update tgl tutup
  dbutil.runSQL(config, '''
    merge into %(treasuryaccount)s x using
      %(srcTableName)s y on (x.nomor_rekening=y.nomor_rekening)
    when matched then update set 
      x.tgl_tutup = %(date)s 
    ''' % dParam
  )
#--
  
if __name__ == "__main__":
  podUtil.printArgs()
  argValues = podUtil.checkArgs('pod_fasbis_maturity.py', ARG_LIST, 2, False)
  dbutil.DEBUG_LEVEL = 0
  main(argValues)
#--
  