import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil
import calendar

# PRACONDITION: ALL EOD TEMPORARY TABLES HAS BEEN CREATED
# IF NOT, RUN pod_tmp_tables.py first !

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}, 
  {'name': 'sDate', 'short_desc': 'process date', 'long_desc': 'process date is in format dd-MMM-yyyy example: 01-Jan-2013'}
]

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

# GLOBALS
_CONSOLE = False
if not _CONSOLE:
  config = appserver.ActiveConfig
  app    = config.AppObject
#--

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
  app = config.AppObject
  ioutil.IO_MODE = ioutil.IOMODE_DAF
  ioutil.APP_OBJECT = app
  app.ConCreate('out')
                                
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1
#-- END OF SCRIPT INJECTION
    
def main(config, debugLevel = None):
  #metaConfig = argValues['metaConfig']
  #strDate = argValues['sDate']   
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  oNextDay = oToday.NextWorkDay()
  float_NextDay = oNextDay.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)

  #get last day in month
  my_date = config.FormatDateTime('yyyymm', float_oToday)  
  curdate = int(strDate[:2])
  y = int(my_date[:4])
  m = int(my_date[4:])
  last_day = calendar.monthrange(y,m)[1]

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('>> Mulai proses')  
  
  if config == None: config = podUtil.openConfig(metaConfig)
  dbutil.DEBUG_LEVEL = debugLevel or 1
  
  #jika akhir bulan selalu akru hanya sampai tgl 01
  if curdate==last_day:
    runTreasuryAccrue(config, strDate, '<')
    runTreasuryAccrue(config, strDate, '>')
    
  #runTreasuryAccrue(config, strDate)
  
  app.ConWriteln('>> Proses selesai')
#--
    
def runTreasuryAccrue(config, strDate, jenis_amor):
  
  MASTER_SRC_TABLE = 'tmp.ftmp_trea_amortized'
  BALANCE_FIELDS = []
  MUT_FIELDS = ['mut_diskonto', 'mut_diskonto_amor', 'mut_premium', 'mut_premium_amor']
  MUT_CLASSGROUP_FIELDS = [
    {'field': 'mut_diskonto', 'classgroup': 'tr_diskonto'},
    {'field': 'mut_diskonto_amor', 'classgroup': 'tr_diskonto_amor'},
    {'field': 'mut_premium', 'classgroup': 'tr_premium'},
    {'field': 'mut_premium_amor', 'classgroup': 'tr_premium_amor'}
  ]
  
  mlu = config.ModLibUtils
  pod_fin_common = modman.getModule(config, 'server_scripts#pod_common_collective')

  tDate = timeutil.parseShortMonthDate(config, strDate)

  dictParam = {
    'date': mlu.QuotedStr(strDate),
    'jenis_amor': jenis_amor,
    'ket_amor' : 'PREMIUM' if jenis_amor == '<' else 'DISKONTO'
  }
  
  selectData(config, MASTER_SRC_TABLE, dictParam)
  pod_fin_common.initBalanceAndMutationFields(config, MASTER_SRC_TABLE, 
    BALANCE_FIELDS,
    MUT_FIELDS  
  )
  createTxValues(config, MASTER_SRC_TABLE)
  deleteNoTxRows(config, MASTER_SRC_TABLE)
  #'''
  pod_fin_common.prepareStdTxFields(config, MASTER_SRC_TABLE)
  pod_fin_common.createTxDetails(config, MASTER_SRC_TABLE, 'trea_amor', MUT_CLASSGROUP_FIELDS)
  pod_fin_common.createCoreTransaction(config, MASTER_SRC_TABLE, 'trea_amor', strDate)
  pod_fin_common.updateBalanceFromTransaction(config, MASTER_SRC_TABLE, MUT_CLASSGROUP_FIELDS)
  postProcess(config, MASTER_SRC_TABLE, dictParam)
  #'''
  
def selectData(config, srcTableName, dictParam):
  dParam = {
      'srcTableName': config.MapDBTableName(srcTableName),
      'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
      'treasuryaccount': config.MapDBTableName('treasuryaccount'),
      'treasuratberharga': config.MapDBTableName('treasuratberharga'),
      'treaproduk': config.MapDBTableName('treaproduk'),
      'jenistreasury': config.MapDBTableName('jenistreasury')
    }
  dParam.update(dictParam)
  
  dbutil.runSQL(config, '''
       DELETE FROM %(srcTableName)s
    ''' % dParam
  )

  #update by BG penjumlahan timpstamp dalam orecle hasilnya <> number, harus di extract[day,hour,minute,second] jika ingin dapat number
  dbutil.runSQL(config, '''  
    INSERT INTO %(srcTableName)s (
      nomor_rekening, revaluasi
      , nilai_amortisasi
      , tenor, period_act
      , saldo_amortisasi
      , tgl_awal, tgl_akhir
      , ktr_detil, ktr_transaksi, ktr_detiltransgroup
    )
    SELECT  
      nomor_rekening
      , nvl(revaluasi,0), nvl(nilai_amortisasi,0)
      , tenor, j_bln
      , round((nvl(revaluasi,0)+nvl(nilai_amortisasi,0))*(j_bln/tenor), 8) saldo_amortisasi
      , tgl_settlement, tgl_jatuh_tempo
      , ket_, ket_, ket_
    FROM (
  		SELECT 
  		  a.nomor_rekening
  		  , a.revaluasi, nilai_amortisasi 
  		  , trunc(Months_Between (a.tgl_jatuh_tempo, f.tgl_settlement)) tenor
  		  , trunc(Months_Between (to_date(%(date)s), f.tgl_settlement)) j_bln
  		  , f.tgl_settlement, a.tgl_jatuh_tempo
  		  , 'AMORTISASI %(ket_amor)s '||d.keterangan ||' - ' || f.kode_instrumen ||' - ' || To_Char(to_date(%(date)s), 'YYYYMM') ket_
  		FROM %(treasuryaccount)s a
  		  INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening 
  		  INNER JOIN %(treasuratberharga)s f ON a.nomor_rekening=f.nomor_rekening
  		  left JOIN %(treaproduk)s c ON a.kode_produk=c.kode_produk
  		  left JOIN %(jenistreasury)s d ON a.jenis_treasuryaccount=d.jenis_treasuryaccount
  		WHERE b.status_rekening=1 and b.saldo < -0.1
  		  AND to_date(%(date)s) <= a.tgl_jatuh_tempo
  		  AND to_date(%(date)s) > f.tgl_settlement
  		  AND a.revaluasi %(jenis_amor)s 0
  		  --AND a.nomor_rekening='TRB011'
    )
     ''' % dParam
  )
        
  dbutil.runSQL(config, '''  
    select 1 FROM %(treasuryaccount)s WHERE nomor_rekening in
    (select nomor_rekening from %(srcTableName)s) FOR UPDATE NOWAIT
    ''' % dParam
  )
      
# generate transaction values in columns  
def createTxValues(config, srcTableName):
  dParam = {
      'srcTableName': config.MapDBTableName(srcTableName),
  }

  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, '''
    merge INTO %(srcTableName)s x USING ( 
      SELECT nomor_rekening
        , revaluasi
        , case when revaluasi < 0 then saldo_amortisasi-nilai_amortisasi else 0 end mut_premium
        , case when revaluasi > 0 then saldo_amortisasi-nilai_amortisasi else 0 end mut_diskonto
      FROM %(srcTableName)s a                                                
    ) y ON (x.nomor_rekening=y.nomor_rekening)
    WHEN matched THEN UPDATE SET 
      x.mut_diskonto         = -y.mut_diskonto,
      x.mut_diskonto_amor    = y.mut_diskonto,
      x.mut_premium          = -y.mut_premium,
      x.mut_premium_amor     = y.mut_premium
    ''' % dParam
  )
#--
#--

# delete no-transaction rows to reduce processing request
def deleteNoTxRows(config, srcTableName):
  dParam = {
      'srcTableName': config.MapDBTableName(srcTableName),
  }
  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, ''' 
      DELETE FROM %(srcTableName)s WHERE abs(nilai_amortisasi+saldo_amortisasi) <= 0.1
    ''' % dParam
  )
#--   

def postProcess(config, srcTableName, dictParam):
  dParam = {
      'srcTableName': config.MapDBTableName(srcTableName),
      'treasuryaccount': config.MapDBTableName('treasuryaccount')
  }
  
  #update accrue_day
  sSQL = '''
    merge into %(treasuryaccount)s x using
      %(srcTableName)s y on (x.nomor_rekening=y.nomor_rekening)
    when matched then update set 
      x.accrue_day = y.amount_accrue_day
    ''' % dParam
  #dbutil.runSQL(config, sSQL)
#--

#--
  
if __name__ == "__main__":
  podUtil.printArgs()
  argValues = podUtil.checkArgs('pod_sukuk_amortized.py', ARG_LIST, 2, False)
  dbutil.DEBUG_LEVEL = 0
  main(argValues)
#--
  