import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil
import calendar

# PRACONDITION: ALL EOD TEMPORARY TABLES HAS BEEN CREATED
# IF NOT, RUN pod_tmp_tables.py first !

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}, 
  {'name': 'sDate', 'short_desc': 'process date', 'long_desc': 'process date is in format dd-MMM-yyyy example: 01-Jan-2013'}
]

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

# GLOBALS
_CONSOLE = False
if not _CONSOLE:
  config = appserver.ActiveConfig
  app    = config.AppObject
#--

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
  app = config.AppObject
  ioutil.IO_MODE = ioutil.IOMODE_DAF
  ioutil.APP_OBJECT = app
  app.ConCreate('out')
                                
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1
#-- END OF SCRIPT INJECTION
    
def main(config, debugLevel = None):
  #metaConfig = argValues['metaConfig']
  #strDate = argValues['sDate']   
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  oLastDay = oToday.GetLastDay()
  #oNextDay = oToday.NextWorkDay()
  float_NextDay = oLastDay.GetDate()
  #strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_NextDay)

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('>> Mulai proses')  
  
  if config == None: config = podUtil.openConfig(metaConfig)
  dbutil.DEBUG_LEVEL = debugLevel or 1
  
  #strNDate = '01-mar-2019'
  runTreasuryAccrue(config, strDate)
  
  app.ConWriteln('>> Proses selesai')
#--
    
def runTreasuryAccrue(config, strDate): 
  helper = phelper.PObjectHelper(config)
  MASTER_SRC_TABLE = 'tmp.ftmp_sukuk_maturity'
  BALANCE_FIELDS = []
  MUT_FIELDS = ['mut_saldo']
  MUT_CLASSGROUP_FIELDS = [
    {'field': 'mut_saldo', 'classgroup': 'tr_nominal'}
  ]
  
  mlu = config.ModLibUtils
  MUT_TDP_FIELDS = [
    {'field': 'mut_saldo_bank', 'acc_code': 'norek_banksrc', 'ket':'Sukuk Jatuh Tempo'}
  ]
  pod_fin_common = modman.getModule(config, 'server_scripts#pod_common_collective_2')

  pct_pajak = helper.GetObject('ParameterGlobalTreasury', 'PJK_INV').nilai_parameter or 15 
  dictParam = {
    'srcTableName': config.MapDBTableName(MASTER_SRC_TABLE),
    'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
    'treasuryaccount': config.MapDBTableName('treasuryaccount'),
    'treasuratberharga': config.MapDBTableName('treasuratberharga'),
    'treaproduk': config.MapDBTableName('treaproduk'),
    'jenistreasury': config.MapDBTableName('jenistreasury'),
    'date': mlu.QuotedStr(strDate),
    #'nm_date': mlu.QuotedStr(snm_date), 
    'pct_pajak': pct_pajak/100 

    ,'detiltransaksi' : config.MapDBTableName('core.detiltransaksi')
    ,'detiltransaksiclass' : config.MapDBTableName('core.detiltransaksiclass')
    ,'seq_detiltransaksi' : config.MapDBTableName('core.seq_detiltransaksi')
    ,'glinterface' : config.MapDBTableName('glinterface')
    ,'currency' : config.MapDBTableName('core.currency')
  }
  
  selectData(config, dictParam)
  pod_fin_common.initBalanceAndMutationFields(config, MASTER_SRC_TABLE, 
    BALANCE_FIELDS,
    MUT_FIELDS  
  )
  createTxValues(config, dictParam)
  deleteNoTxRows(config, dictParam)
  
  #'''
  pod_fin_common.prepareStdTxFields(config, MASTER_SRC_TABLE)
  pod_fin_common.createTxDetails(config, MASTER_SRC_TABLE, 'trea_maturity', MUT_CLASSGROUP_FIELDS)

  DebetRekBank(config, dictParam, MUT_TDP_FIELDS) 
  UpdateSaldoRekening(config, dictParam)

  pod_fin_common.createCoreTransaction(config, MASTER_SRC_TABLE, 'trea_maturity', strDate)
  pod_fin_common.updateBalanceFromTransaction(config, MASTER_SRC_TABLE, MUT_CLASSGROUP_FIELDS)
  postProcess(config, dictParam)
  #'''
  
def selectData(config, dParam): 
  dbutil.runSQL(config, '''
       DELETE FROM %(srcTableName)s
    ''' % dParam
  )

  #update by BG penjumlahan timpstamp dalam orecle hasilnya <> number, harus di extract[day,hour,minute,second] jika ingin dapat number
  dbutil.runSQL(config, '''  
    INSERT INTO %(srcTableName)s (
      nomor_rekening
      , tgl_awal, tgl_akhir
      , saldo
      , norek_banksrc 
      , tanggal_transaksi    
      , ktr_detil, ktr_transaksi, ktr_detiltransgroup
    )
  	SELECT DISTINCT 
  	  a.Nomor_Rekening 
  	  , c.tgl_settlement, a.tgl_jatuh_tempo
  	  , b.saldo
  	  , a.norek_banksrc
  	  , a.tgl_jatuh_tempo
  	  , 'CLOSE ACCOUNT '||jt.keterangan ||' - ' || c.kode_instrumen as ket_
  	  , 'CLOSE ACCOUNT '||jt.keterangan ||' - ' || c.kode_instrumen as ket_
  	  , 'CLOSE ACCOUNT '||jt.keterangan ||' - ' || c.kode_instrumen as ket_
  	  
  	FROM %(treasuryaccount)s a
  	  INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
  	  INNER JOIN %(treasuratberharga)s c ON a.nomor_rekening=c.nomor_rekening
  	  INNER JOIN %(treaproduk)s pr ON a.kode_produk=pr.kode_produk                                                            
  	  left JOIN %(jenistreasury)s jt ON a.jenis_treasuryaccount=jt.jenis_treasuryaccount
  	WHERE
  	  b.saldo<>0 
  	  AND tgl_jatuh_tempo<=%(date)s
    ''' % dParam
  )
        
  dbutil.runSQL(config, '''  
    select 1 FROM %(treasuryaccount)s WHERE nomor_rekening in
    (select nomor_rekening from %(srcTableName)s) FOR UPDATE NOWAIT
    ''' % dParam
  )
      
# generate transaction values in columns  
def createTxValues(config, dParam):
  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, '''
    UPDATE %(srcTableName)s x SET 
      x.mut_saldo      = -x.saldo,
      x.mut_saldo_bank = x.saldo
    ''' % dParam
  )
#--

# delete no-transaction rows to reduce processing request
def deleteNoTxRows(config, dParam):
  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, ''' 
      DELETE FROM %(srcTableName)s WHERE Nvl(mut_saldo,0)<0.1 
    ''' % dParam
  )
#--   

def DebetRekBank(config, dictParam, fieldNames):
  app = config.AppObject
  for fieldInfo in fieldNames:
    dParam = {
        'field': fieldInfo['field'],
        'acc_code': fieldInfo['acc_code'],
        'ket_detil': fieldInfo['ket']
      }
    dParam.update(dictParam)

    sSQL = '''  
      INSERT INTO %(detiltransaksi)s (
        Id_Detil_Transaksi, Tanggal_Transaksi, Jenis_Mutasi, Nilai_Mutasi, Keterangan, 
        Kode_Cabang, Kode_Valuta, Jenis_Detil_Transaksi, Nilai_Kurs_Manual, Nilai_Ekuivalen, 
        Saldo_Awal, Saldo_Akhir, Nomor_Referensi, Kode_Kurs, Kode_Account, 
        Kode_Valuta_RAK, Kode_Jurnal, Nominal_Biaya, IsBillerTransaction, hide_passbook, 
        hide_statement, hide_gentrans, subsystem_code, sub_tx_code, kode_subtx_class, 
        Id_Transaksi, Nomor_Rekening, kode_tx_class, id_detiltransgroup, Id_Parameter_Transaksi, 
        Id_Parameter, ID_JournalBlock
      )
      SELECT 
        %(seq_detiltransaksi)s.nextval, 
        ms.tanggal_transaksi, 
        case when ms.mut_saldo_bank<0 then 'D' else 'C' end as mnemonic, 
        -ms.mut_saldo_bank as amount, 
        substr(ms.ktr_detil, 1, 100), 
        rt.kode_cabang, nvl(rt.kode_valuta,'IDR'), 
        NULL, 
        cr.kurs_buku, 
        -ms.mut_saldo_bank*cr.kurs_buku as n_eqv, 
        NULL, NULL, 'AUTOREF', 
        'BOOKING' as kode_kurs, 
        gli.kode_account as kode_account, 
        NULL, 
        '11' as kode_jurnal, 
        0.0, 'F', 'F', 
        'F', 'F', NULL, NULL, NULL, 
        ms.id_transaksi, 
        ms.norek_banksrc, 
        gli.kode_interface, 
        ms.id_detiltransgroup 
        ,NULL, NULL, NULL
      FROM
        %(srcTableName)s ms
        inner join %(rekeningtransaksi)s rt on rt.nomor_rekening=ms.norek_banksrc
        inner join %(treasuryaccount)s hj on hj.nomor_rekening=ms.norek_banksrc
        inner join %(glinterface)s gli on hj.kode_produk=gli.kode_produk and gli.kode_interface='37001'
        inner join %(currency)s cr on rt.kode_valuta=cr.currency_code
      where ms.mut_saldo_bank < 0      
    ''' % dParam 
    app.ConWriteln(sSQL)
    dbutil.runSQL(config, sSQL)
  #--
#--

def UpdateSaldoRekening(config, dParam): 
  #update saldo rekening liability
  sSQL = '''  
      merge into %(rekeningtransaksi)s x using
      (
        SELECT 
          dt.nomor_rekening, sum(decode(dt.jenis_mutasi,'D', -dt.nilai_mutasi, dt.nilai_mutasi)) jml_trx
        FROM %(detiltransaksi)s dt       
          inner join %(detiltransaksiclass)s dtc on dtc.kode_tx_class=dt.kode_tx_class and dtc.accum_field_name='saldo'      
        where 
          EXISTS (select 1 from %(srcTableName)s ms where dt.id_transaksi=ms.id_transaksi)
        group by dt.nomor_rekening       
      ) y on (x.nomor_rekening=y.nomor_rekening)
      when matched then update set
         x.saldo = nvl(x.saldo,0)+y.jml_trx 
  ''' % dParam
  dbutil.runSQL(config, sSQL)
  #--
#---

def postProcess(config, dParam):
  dbutil.runSQL(config, '''
    UPDATE %(rekeningtransaksi)s x set status_rekening=3
    WHERE EXISTS (
      select 1 from %(srcTableName)s y where x.nomor_rekening=y.nomor_rekening                                                 
    ) 
    ''' % dParam
  )

  dbutil.runSQL(config, '''
    UPDATE %(treasuryaccount)s x set tgl_tutup=%(date)s
    WHERE EXISTS (
      select 1 from %(srcTableName)s y where x.nomor_rekening=y.nomor_rekening                                                 
    ) 
    ''' % dParam
  )
#--
  
if __name__ == "__main__":
  podUtil.printArgs()
  argValues = podUtil.checkArgs('pod_sukuk_maturity.py', ARG_LIST, 2, False)
  dbutil.DEBUG_LEVEL = 0
  main(argValues)
#--
  