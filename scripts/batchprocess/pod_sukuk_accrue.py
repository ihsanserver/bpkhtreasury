import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil
import calendar

# PRACONDITION: ALL EOD TEMPORARY TABLES HAS BEEN CREATED
# IF NOT, RUN pod_tmp_tables.py first !

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}, 
  {'name': 'sDate', 'short_desc': 'process date', 'long_desc': 'process date is in format dd-MMM-yyyy example: 01-Jan-2013'}
]

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

# GLOBALS
_CONSOLE = False
if not _CONSOLE:
  config = appserver.ActiveConfig
  app    = config.AppObject
#--

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
  app = config.AppObject
  ioutil.IO_MODE = ioutil.IOMODE_DAF
  ioutil.APP_OBJECT = app
  app.ConCreate('out')
                                
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1
#-- END OF SCRIPT INJECTION
    
def main(config, debugLevel = None):
  #metaConfig = argValues['metaConfig']
  #strDate = argValues['sDate']   
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  oLastDay = oToday.GetLastDay()
  #oNextDay = oToday.NextWorkDay()
  float_NextDay = oLastDay.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_NextDay)
  strNDate = config.FormatDateTime('dd-mmm-yyyy', float_NextDay)
  #strNDate = config.FormatDateTime('dd-mmm-yyyy', float_NextDay)

  #get last day in month
  my_date = config.FormatDateTime('yyyymm', float_NextDay)  
  curdate = int(strDate[:2])
  y = int(my_date[:4])
  m = int(my_date[4:])
  last_day = calendar.monthrange(y,m)[1]

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('>> Mulai proses')  
  
  if config == None: config = podUtil.openConfig(metaConfig)
  #dbutil.DEBUG_LEVEL = debugLevel or 1
  
  #jika akhir bulan selalu akru hanya sampai tgl 01
  if curdate==last_day:
    #strNDate = '01-%s' % (config.FormatDateTime('mmm-yyyy', float_NextDay))
    runTreasuryAccrue(config, strDate, strNDate)
    
  app.ConWriteln('>> Proses selesai')
#--
    
def runTreasuryAccrue(config, strDate, strNDate): 
  helper = phelper.PObjectHelper(config)
  MASTER_SRC_TABLE = 'tmp.ftmp_sukuk_accrue'
  BALANCE_FIELDS = []
  MUT_FIELDS = ['mut_pymad', 'mut_profit_accrue']
  MUT_CLASSGROUP_FIELDS = [
    {'field': 'mut_pymad', 'classgroup': 'tr_pymad'},
    {'field': 'mut_profit_accrue', 'classgroup': 'tr_accrue'},
    {'field': 'mut_pajak', 'classgroup': 'tr_pajak'}
  ]
  
  mlu = config.ModLibUtils
  pod_fin_common = modman.getModule(config, 'server_scripts#pod_common_collective_2')

  pct_pajak = helper.GetObject('ParameterGlobalTreasury', 'PJK_INV').nilai_parameter or 15 
  tDate = timeutil.parseShortMonthDate(config, strNDate)
  tNextMonth = mlu.IncMonth(tDate, 1)
  snm_date = config.FormatDateTime('dd-MMM-yyyy', tNextMonth)

  dictParam = {
    'srcTableName': config.MapDBTableName(MASTER_SRC_TABLE),
    'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
    'treasuryaccount': config.MapDBTableName('treasuryaccount'),
    'treasuratberharga': config.MapDBTableName('treasuratberharga'),
    'treaproduk': config.MapDBTableName('treaproduk'),
    'jenistreasury': config.MapDBTableName('jenistreasury'),
    
    'ndate': mlu.QuotedStr(strDate),
    'date': mlu.QuotedStr(strNDate),
    'nm_date': mlu.QuotedStr(snm_date), 
    'pct_pajak': pct_pajak/100 
  }
  
  selectData(config, dictParam)
  pod_fin_common.initBalanceAndMutationFields(config, MASTER_SRC_TABLE, 
    BALANCE_FIELDS,
    MUT_FIELDS  
  )
  createTxValues(config, dictParam)
  deleteNoTxRows(config, dictParam)
  #'''
  pod_fin_common.prepareStdTxFields(config, MASTER_SRC_TABLE)
  pod_fin_common.createTxDetails(config, MASTER_SRC_TABLE, 'trea_accrue', MUT_CLASSGROUP_FIELDS)
  pod_fin_common.createCoreTransaction(config, MASTER_SRC_TABLE, 'trea_accrue', strDate)
  pod_fin_common.updateBalanceFromTransaction(config, MASTER_SRC_TABLE, MUT_CLASSGROUP_FIELDS)
  postProcess(config, dictParam)
  #'''
  
def selectData(config, dParam):
  dbutil.runSQL(config, '''
       UPDATE %(treasuratberharga)s set tgl_bayar_terakhir=tgl_settlement where tgl_bayar_terakhir is null
    ''' % dParam
  )
  
  dbutil.runSQL(config, '''
       DELETE FROM %(srcTableName)s
    ''' % dParam
  )

  #update by BG penjumlahan timpstamp dalam orecle hasilnya <> number, harus di extract[day,hour,minute,second] jika ingin dapat number
  dbutil.runSQL(config, '''  
    INSERT INTO %(srcTableName)s (
      nomor_rekening,
      accrual_type,
      hold_period, hold_period_act,
      accrued_amount,
      yearfac, accrint, nominal_deal,
      tanggal_transaksi,
      ktr_detil, ktr_transaksi, ktr_detiltransgroup
    )
    SELECT  
      nomor_rekening, 'M'
      , hold_period, hold_period_act
      , saldo_accrue
      , (100*kp_rate*(hold_period_act/hold_period))*10000 yearfac
      , round((kp_rate*(hold_period_act/hold_period)) * nominal_deal, 12) accrint
      , nominal_deal
      , to_date(%(date)s)
      , ket_, ket_, ket_
    FROM (
      SELECT 
        a.nomor_rekening
        , a.saldo_accrue 
        , extract(day from to_date(%(date)s)-f.tgl_bayar_terakhir) as hold_period_act                                               
        , extract(day from f.tgl_bayar_berikutnya-f.tgl_bayar_terakhir) as hold_period
        , a.eqv_rate_real / (12/tipe_kupon*100) kp_rate                                               
        , -b.saldo nominal_deal
        , 'ACCRUE '||d.keterangan ||' - ' || f.kode_instrumen ||' - ' || To_Char(to_date(%(ndate)s), 'YYYYMM') ket_
      FROM %(treasuryaccount)s a
        INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening 
        INNER JOIN %(treasuratberharga)s f ON a.nomor_rekening=f.nomor_rekening
        left JOIN %(treaproduk)s c ON a.kode_produk=c.kode_produk
        left JOIN %(jenistreasury)s d ON a.jenis_treasuryaccount=d.jenis_treasuryaccount
      WHERE b.status_rekening=1 and b.saldo < -0.1
        AND to_date(%(date)s) > f.tgl_bayar_terakhir
        AND to_date(%(date)s) <= f.tgl_bayar_berikutnya
  		  AND to_date(%(date)s) <= a.tgl_jatuh_tempo
        --AND a.nomor_rekening='MIG.TRB00001'
    )
    ''' % dParam
  )
        
  dbutil.runSQL(config, '''  
    select 1 FROM %(treasuryaccount)s WHERE nomor_rekening in
    (select nomor_rekening from %(srcTableName)s) FOR UPDATE NOWAIT
    ''' % dParam
  )
      
# generate transaction values in columns  
def createTxValues(config, dParam):
  dbutil.runSQL(config, ''' 
      UPDATE %(srcTableName)s SET accrued_amount_gross = accrued_amount/(1-%(pct_pajak)s)
    ''' % dParam
  )

  dbutil.runSQL(config, ''' 
      UPDATE %(srcTableName)s SET accrint_nett = accrint*%(pct_pajak)s
    ''' % dParam
  )

  dbutil.runSQL(config, ''' 
      DELETE FROM %(srcTableName)s WHERE accrued_amount_gross+accrint <= 0
    ''' % dParam
  )  
  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, '''
    merge INTO %(srcTableName)s x USING ( 
      SELECT nomor_rekening
        , accrued_amount_gross+a.accrint mut_akru
        , (accrued_amount_gross+a.accrint)*%(pct_pajak)s mut_pajak
        , accrued_amount
      FROM %(srcTableName)s a                                                
    ) y ON (x.nomor_rekening=y.nomor_rekening AND y.mut_akru>0)
    WHEN matched THEN UPDATE SET 
      x.mut_pymad         = -(y.mut_akru-y.mut_pajak),
      x.mut_pajak         = -y.mut_pajak,
      x.mut_profit_accrue = y.mut_akru
    ''' % dParam
  )
#--

# delete no-transaction rows to reduce processing request
def deleteNoTxRows(config, dParam):
  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, ''' 
      DELETE FROM %(srcTableName)s WHERE Nvl(mut_profit_accrue,0)<0.1 
    ''' % dParam
  )
#--   

def postProcess(config, dParam):
  #update accrue_day
  sSQL = '''
    merge into %(treasuratberharga)s x using
      %(srcTableName)s y on (x.nomor_rekening=y.nomor_rekening)
    when matched then update set 
      x.nominal_pajak = nvl(x.nominal_pajak,0)+y.mut_pajak
    ''' % dParam
  dbutil.runSQL(config, sSQL)
#--
  
if __name__ == "__main__":
  podUtil.printArgs()
  argValues = podUtil.checkArgs('pod_sukuk_accrue.py', ARG_LIST, 2, False)
  dbutil.DEBUG_LEVEL = 0
  main(argValues)
#--
  