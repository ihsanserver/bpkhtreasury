import sys
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.timeutil as timeutil
import com.ihsan.fileutils as utils

# GLOBALS
config = appserver.ActiveConfig
app    = config.AppObject
_CONSOLE = False

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  global _CONSOLE
  
  _CONSOLE = False
  
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1

def main(config):
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')
  oToday = periodHelper.GetToday()
  
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
                                
  #printOut('Processing raw data %s...' % oProcDay.GetDateText())   

  LsScriptName = [ 
    'gen_dailybalance_haji',   
    'gen_sum_remark_trx',
    'gen_dailybalance_bank'    
    ]
  LsResultDir = {}
  # --- SALDO HARIAN

  for ScriptName in LsScriptName :
    printOut('Execute Script %s.py ...' % ScriptName )
    
    moduleReport = modman.getModule(config, 'scripts#batchprocess/' + ScriptName)
    moduleReport.main(config, oToday)
    
    