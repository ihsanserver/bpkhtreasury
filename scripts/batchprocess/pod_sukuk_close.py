import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil
import calendar

# PRACONDITION: ALL EOD TEMPORARY TABLES HAS BEEN CREATED
# IF NOT, RUN pod_tmp_tables.py first !

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}, 
  {'name': 'sDate', 'short_desc': 'process date', 'long_desc': 'process date is in format dd-MMM-yyyy example: 01-Jan-2013'}
]

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

# GLOBALS
_CONSOLE = False
if not _CONSOLE:
  config = appserver.ActiveConfig
  app    = config.AppObject
#--

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
  app = config.AppObject
  ioutil.IO_MODE = ioutil.IOMODE_DAF
  ioutil.APP_OBJECT = app
                                
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1
#-- END OF SCRIPT INJECTION
    
def main(config, debugLevel = None):
  #metaConfig = argValues['metaConfig']
  #strDate = argValues['sDate']   
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  oLastDay = oToday.GetLastDay()
  #oNextDay = oToday.NextWorkDay()
  float_NextDay = oLastDay.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_NextDay)
  #strNDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)
  strNDate = config.FormatDateTime('dd-mmm-yyyy', float_NextDay)

  #get last day in month
  my_date = config.FormatDateTime('yyyymm', float_NextDay)  
  curdate = int(strDate[:2])
  y = int(my_date[:4])
  m = int(my_date[4:])
  last_day = calendar.monthrange(y,m)[1]

  app = config.AppObject
  #app.ConCreate('out')
  #app.ConWriteln('>> Mulai proses')  
  
  if config == None: config = podUtil.openConfig(metaConfig)
  #dbutil.DEBUG_LEVEL = debugLevel or 1
  
  #jika akhir bulan selalu akru hanya sampai tgl 01
  #if curdate==last_day:
  #strNDate = '01-%s' % (config.FormatDateTime('mmm-yyyy', float_NextDay))
  #runTreasuryAccrue(config, strDate, strNDate)
  runTreasuryAccrue(config, strDate, '<')
  runTreasuryAccrue(config, strDate, '>')
    
  app.ConWriteln('>> Proses selesai')
#--
    
def runTreasuryAccrue(config, strDate, jenis_amor): 
  helper = phelper.PObjectHelper(config)
  MASTER_SRC_TABLE = 'tmp.ftmp_sukuk_close'
  BALANCE_FIELDS = []
  MUT_FIELDS = ['mut_pymad', 'mut_profit_accrue']
  MUT_CLASSGROUP_FIELDS = [
    {'field': 'mut_pymad', 'classgroup': 'tr_pymad'},
    {'field': 'mut_profit_accrue', 'classgroup': 'tr_accrue'},
    {'field': 'mut_pajak', 'classgroup': 'tr_pajak'},

    {'field': 'mut_diskonto', 'classgroup': 'tr_diskonto'},
    {'field': 'mut_diskonto_amor', 'classgroup': 'tr_diskonto_amor'},
    {'field': 'mut_premium', 'classgroup': 'tr_premium'},
    {'field': 'mut_premium_amor', 'classgroup': 'tr_premium_amor'}
  ]
  
  mlu = config.ModLibUtils
  pod_fin_common = modman.getModule(config, 'server_scripts#pod_common_collective_2')

  pct_pajak = helper.GetObject('ParameterGlobalTreasury', 'PJK_INV').nilai_parameter or 15 
  tDate = timeutil.parseShortMonthDate(config, strDate)
  tNextMonth = mlu.IncMonth(tDate, 1)
  snm_date = config.FormatDateTime('dd-MMM-yyyy', tNextMonth)

  dictParam = {
    'srcTableName': config.MapDBTableName(MASTER_SRC_TABLE),
    'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
    'treasuryaccount': config.MapDBTableName('treasuryaccount'),
    'treasuratberharga': config.MapDBTableName('treasuratberharga'),
    'treacounterpart': config.MapDBTableName('treacounterpart'),
    'treaproduk': config.MapDBTableName('treaproduk'),
    'jenistreasury': config.MapDBTableName('jenistreasury'),
    
    'ndate': mlu.QuotedStr(strDate),
    'date': mlu.QuotedStr(strDate),
    'nm_date': mlu.QuotedStr(snm_date), 
    'pct_pajak': pct_pajak/100,
     
    'jenis_amor': jenis_amor,
    'ket_amor' : 'PREMIUM' if jenis_amor == '<' else 'DISKONTO'
  }
  
  selectData(config, dictParam)
  pod_fin_common.initBalanceAndMutationFields(config, MASTER_SRC_TABLE, 
    BALANCE_FIELDS,
    MUT_FIELDS  
  )
  createTxValues(config, dictParam)
  deleteNoTxRows(config, dictParam)
  #'''
  pod_fin_common.prepareStdTxFields(config, MASTER_SRC_TABLE)
  pod_fin_common.createTxDetails(config, MASTER_SRC_TABLE, 'trea_maturity', MUT_CLASSGROUP_FIELDS)
  pod_fin_common.createCoreTransaction(config, MASTER_SRC_TABLE, 'trea_maturity', strDate)
  pod_fin_common.updateBalanceFromTransaction(config, MASTER_SRC_TABLE, MUT_CLASSGROUP_FIELDS)
  postProcess(config, dictParam)
  #'''
  
def selectData(config, dParam):
  dbutil.runSQL(config, '''
       DELETE FROM %(srcTableName)s
    ''' % dParam
  )

  #update by BG penjumlahan timpstamp dalam orecle hasilnya <> number, harus di extract[day,hour,minute,second] jika ingin dapat number
  dbutil.runSQL(config, '''  
    INSERT INTO %(srcTableName)s (
      nomor_rekening
      , tenor, period_act
      , tgl_awal, tgl_akhir

      , revaluasi
      , nilai_amortisasi
      , saldo_amortisasi

      , accrued_amount     
      --, mut_pymad, mut_profit_accrue, mut_pajak
      , tanggal_transaksi
      , ktr_detil, ktr_transaksi, ktr_detiltransgroup
    )
    SELECT DISTINCT 
      a.Nomor_Rekening 
      , trunc(Months_Between (a.tgl_jatuh_tempo, c.tgl_settlement)) tenor
      , trunc(Months_Between (%(date)s, c.tgl_settlement)) period_act
      , c.tgl_settlement, a.tgl_jatuh_tempo

      --, b.kode_jenis, b.kode_valuta, b.kode_cabang 
      , nvl(a.revaluasi,0)
      , nvl(a.nilai_amortisasi,0)
      , (nvl(revaluasi,0)+nvl(nilai_amortisasi,0)) sisa_amor 
      , a.saldo_accrue
      , to_date(%(date)s)
      , 'CLOSE ACCOUNT '||jt.keterangan ||' - ' || c.kode_instrumen ket_
      , 'CLOSE ACCOUNT '||jt.keterangan ||' - ' || c.kode_instrumen ket_
      , 'CLOSE ACCOUNT '||jt.keterangan ||' - ' || c.kode_instrumen ket_
    FROM %(treasuryaccount)s a
      INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
      INNER JOIN %(treasuratberharga)s c ON a.nomor_rekening=c.nomor_rekening
      INNER JOIN %(treaproduk)s pr ON a.kode_produk=pr.kode_produk
      INNER JOIN %(treacounterpart)s cp ON a.kode_counterpart=cp.kode_counterpart
      left JOIN %(jenistreasury)s jt ON a.jenis_treasuryaccount=jt.jenis_treasuryaccount
    WHERE
      (a.revaluasi %(jenis_amor)s 0 OR a.saldo_accrue <> 0)
      AND b.saldo=0
    ''' % dParam
  )
        
  sSQL='''  
    select 1 FROM %(treasuryaccount)s WHERE nomor_rekening in
    (select nomor_rekening from %(srcTableName)s) FOR UPDATE NOWAIT
    ''' % dParam
  #dbutil.runSQL(config, sSQL)
      
# generate transaction values in columns  
def createTxValues(config, dParam):
  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, '''
    merge INTO %(srcTableName)s x USING ( 
      SELECT nomor_rekening
        , revaluasi
        , case when revaluasi < 0 then saldo_amortisasi-nilai_amortisasi else 0 end mut_premium
        , case when revaluasi > 0 then saldo_amortisasi-nilai_amortisasi else 0 end mut_diskonto
      FROM %(srcTableName)s a                                                
    ) y ON (x.nomor_rekening=y.nomor_rekening)
    WHEN matched THEN UPDATE SET 
      x.mut_diskonto         = -y.mut_diskonto,
      x.mut_diskonto_amor    = y.mut_diskonto,
      x.mut_premium          = -y.mut_premium,
      x.mut_premium_amor     = y.mut_premium
    ''' % dParam
  )


  dbutil.runSQL(config, '''
    merge INTO %(srcTableName)s x USING ( 
      SELECT nomor_rekening
        , -a.accrued_amount pymhd
        , (a.accrued_amount/0.85) pdpt_akru
        , -((a.accrued_amount/0.85)-a.accrued_amount) AS pajak
      FROM %(srcTableName)s a                                                
    ) y ON (x.nomor_rekening=y.nomor_rekening)
    WHEN matched THEN UPDATE SET 
      x.mut_pymad          = y.pymhd,
      x.mut_profit_accrue  = y.pdpt_akru,
      x.mut_pajak          = y.pajak
    ''' % dParam
  )
#--


# delete no-transaction rows to reduce processing request
def deleteNoTxRows(config, dParam):
  pass
#--   

def postProcess(config, dParam):
  dbutil.runSQL(config, '''
    UPDATE %(rekeningtransaksi)s x set status_rekening=3
    WHERE EXISTS (
      select 1 from %(srcTableName)s y where x.nomor_rekening=y.nomor_rekening                                                 
    ) 
    ''' % dParam
  )

  dbutil.runSQL(config, '''
    UPDATE %(treasuryaccount)s x set tgl_tutup=%(date)s
    WHERE EXISTS (
      select 1 from %(srcTableName)s y where x.nomor_rekening=y.nomor_rekening                                                 
    ) 
    ''' % dParam
  )


#--
  
if __name__ == "__main__":
  podUtil.printArgs()
  argValues = podUtil.checkArgs('pod_sukuk_close.py', ARG_LIST, 2, False)
  dbutil.DEBUG_LEVEL = 0
  main(argValues)
#--
  