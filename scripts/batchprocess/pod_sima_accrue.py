import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil
import calendar

# PRACONDITION: ALL EOD TEMPORARY TABLES HAS BEEN CREATED
# IF NOT, RUN pod_tmp_tables.py first !

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}, 
  {'name': 'sDate', 'short_desc': 'process date', 'long_desc': 'process date is in format dd-MMM-yyyy example: 01-Jan-2013'}
]

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

# GLOBALS
_CONSOLE = False
if not _CONSOLE:
  config = appserver.ActiveConfig
  app    = config.AppObject
#--

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
  app = config.AppObject
  ioutil.IO_MODE = ioutil.IOMODE_DAF
  ioutil.APP_OBJECT = app
  app.ConCreate('out')
                                
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1
#-- END OF SCRIPT INJECTION
    
def main(config, debugLevel = None):
  #metaConfig = argValues['metaConfig']
  #strDate = argValues['sDate']   
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  oNextDay = oToday.NextWorkDay()
  float_NextDay = oNextDay.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)
  strNDate = config.FormatDateTime('dd-mmm-yyyy', float_NextDay)

  #get last day in month
  my_date = config.FormatDateTime('yyyymm', float_oToday)  
  curdate = int(strDate[:2])
  y = int(my_date[:4])
  m = int(my_date[4:])
  last_day = calendar.monthrange(y,m)[1]

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('>> Mulai proses')  
  
  if config == None: config = podUtil.openConfig(metaConfig)
  dbutil.DEBUG_LEVEL = debugLevel or 1
  
  #jika akhir bulan selalu akru hanya sampai tgl 01
  if curdate==last_day:
    strNDate = '01-%s' % (config.FormatDateTime('mmm-yyyy', float_NextDay))
    
  runTreasuryAccrue(config, strDate, strNDate)
  
  app.ConWriteln('>> Proses selesai')
#--
    
def runTreasuryAccrue(config, strDate, strNDate):
  
  MASTER_SRC_TABLE = 'tmp.ftmp_treasury_accrue'
  BALANCE_FIELDS = []
  MUT_FIELDS = ['mut_biaya', 'mut_bmhd']
  MUT_CLASSGROUP_FIELDS = [
    {'field': 'mut_biaya', 'classgroup': 'tr_biaya'},
    {'field': 'mut_bmhd', 'classgroup': 'tr_accrue'}
  ]
  
  mlu = config.ModLibUtils
  pod_fin_common = modman.getModule(config, 'server_scripts#pod_common_collective')

  tDate = timeutil.parseShortMonthDate(config, strNDate)
  tNextMonth = mlu.IncMonth(tDate, 1)
  snm_date = config.FormatDateTime('dd-MMM-yyyy', tNextMonth)

  dictParam = {
    'date': mlu.QuotedStr(strNDate),
    'nm_date': mlu.QuotedStr(snm_date)  
  } 
  
  selectData(config, MASTER_SRC_TABLE, dictParam)
  pod_fin_common.initBalanceAndMutationFields(config, MASTER_SRC_TABLE, 
    BALANCE_FIELDS,
    MUT_FIELDS  
  )
  createTxValues(config, MASTER_SRC_TABLE)
  deleteNoTxRows(config, MASTER_SRC_TABLE)
  #'''
  pod_fin_common.prepareStdTxFields(config, MASTER_SRC_TABLE)
  pod_fin_common.createTxDetails(config, MASTER_SRC_TABLE, 'trea_accrue', MUT_CLASSGROUP_FIELDS)
  pod_fin_common.createCoreTransaction(config, MASTER_SRC_TABLE, 'trea_accrue', strDate)
  pod_fin_common.updateBalanceFromTransaction(config, MASTER_SRC_TABLE, MUT_CLASSGROUP_FIELDS)
  postProcess(config, MASTER_SRC_TABLE, dictParam)
  #'''
def selectData(config, srcTableName, dictParam):

  dParam = {
      'srcTableName': config.MapDBTableName(srcTableName),
      'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
      'treasuryaccount': config.MapDBTableName('treasuryaccount'),
      'treasima': config.MapDBTableName('treasima'),
      'treaproduk': config.MapDBTableName('treaproduk'),
      'jenistreasury': config.MapDBTableName('jenistreasury')
    }
  dParam.update(dictParam)
  
  dbutil.runSQL(config, '''
       DELETE FROM %(srcTableName)s
    ''' % dParam
  )

  #update by BG penjumlahan timpstamp dalam orecle hasilnya <> number, harus di extract[day,hour,minute,second] jika ingin dapat number
  dbutil.runSQL(config, '''  
    INSERT INTO %(srcTableName)s (
      nomor_rekening,
      accrual_type,
      ujroh,
      amount_accrue_day,
      jangka_waktu,     
      accrued_amount,
      
      ktr_detil, ktr_transaksi, ktr_detiltransgroup
    )
    SELECT 
      a.nomor_rekening, 'D', a.ujroh
      , least(extract(day from to_date(%(date)s)-tgl_buka),jangka_waktu) as amount_accrue_day
      , a.jangka_waktu
      , a.saldo_biaya
      , 'ACCRUE '||d.keterangan
      , 'ACCRUE '||d.keterangan 
      , 'ACCRUE '||d.keterangan ||' - ' || a.nomor_rekening
    FROM %(treasuryaccount)s a
      INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
      left JOIN %(treaproduk)s c ON a.kode_produk=c.kode_produk
      left JOIN %(jenistreasury)s d ON a.jenis_treasuryaccount=d.jenis_treasuryaccount
    WHERE b.status_rekening=1
      AND a.jenis_treasuryaccount IN ('S')
      AND c.tipe_treasury IN ('B')
      AND a.ujroh+a.saldo_biaya > 0.1
      --AND a.nomor_rekening='TRS013'
    ''' % dParam
  )
        
  dbutil.runSQL(config, '''  
    select 1 FROM %(treasuryaccount)s WHERE nomor_rekening in
    (select nomor_rekening from %(srcTableName)s) FOR UPDATE NOWAIT
    ''' % dParam
  )
      
# generate transaction values in columns  
def createTxValues(config, srcTableName):
  dParam = {
      'srcTableName': config.MapDBTableName(srcTableName),
  }

  #update by BG add NVL(accrued_amount)      
  dbutil.runSQL(config, '''
      merge INTO %(srcTableName)s x USING ( 
        SELECT nomor_rekening
          , Round((ujroh/jangka_waktu)*amount_accrue_day,4) n_akru
          , accrued_amount+Round((ujroh/jangka_waktu)*amount_accrue_day,4) mut_akru
          , accrued_amount, ujroh
        FROM %(srcTableName)s a                                                
      ) y ON (x.nomor_rekening=y.nomor_rekening AND y.mut_akru>0)
      WHEN matched THEN UPDATE SET 
        x.mut_biaya       = y.mut_akru,
        x.mut_bmhd        = y.mut_akru
    ''' % dParam
  )
#--

# delete no-transaction rows to reduce processing request
def deleteNoTxRows(config, srcTableName):
  dParam = {
      'srcTableName': config.MapDBTableName(srcTableName),
  }
  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, ''' 
      DELETE FROM %(srcTableName)s WHERE Nvl(mut_bmhd,0)<0.1 
    ''' % dParam
  )
#--   

def postProcess(config, srcTableName, dictParam):
  pass  
#--
  
if __name__ == "__main__":
  podUtil.printArgs()
  argValues = podUtil.checkArgs('pod_sima_accrue.py', ARG_LIST, 2, False)
  dbutil.DEBUG_LEVEL = 0
  main(argValues)
#--
  