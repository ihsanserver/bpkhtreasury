'''
Fungsi ini digunakan untuk generate data saldo harian.
Pergerakan saldo harian akan dicatat jika memenuhi kondisi berikut :
  - ada transaksi (dilihat dari tabel transaksi dan detiltransaksi)
  - ada perubahan kode cabang rekening (dilihat dari tabel inforeposisirekening)
  - ada perubahan kode produk rekening (dilihat dari tabel infoubahprodukrekening)
  Catatan : perubahan kode cabang / kode produk mungkin saja lebih dari 1 kali dalam 1 hari untuk rekening yg sama 
    
Selain itu terdapat juga mencatat accountinstance_id yang sesuai dengan produk rekening 
agar dapat dimanfaatkan sebagai data LBV (Ledger Balance Verification)

'''
import sys
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman
import com.ihsan.foundation.appserver as appserver

# application-level modules, loaded via modman
modman.loadStdModules(globals(), 
  [
    "sutil"
    , "AppError"
    , "scripts#batchprocess.batch_sql"
  ]
)

sutil.setDialectToORACLE()

# GLOBALS
config  = appserver.ActiveConfig
app     = config.AppObject
_CONSOLE = False

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  
  global _CONSOLE
  
  _CONSOLE = False
  batch_sql._CONSOLE = False
  batch_sql.app      = app
  batch_sql.updateResources(SQL_RESOURCES)
  
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1

def main(config):
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('PeriodHelper')

  oToday = periodHelper.GetToday()
  # Replace oToday With PrevWorkDay
  oPrevDay = oToday.PrevWorkDay()
    
  app = config.AppObject
  app.ConCreate('out')
  
  printOut('>> Create Daily Balance')

  dictParam = {
      'BalanceField' : 'saldo'
      , 'TodayFlat' : oPrevDay.GetFlatText()
      , 'Today': sutil.toDate(config, oPrevDay.GetDate())
      , 'TEMP_ReposisiRekeningCandidate' : config.MapDBTableName('tmp.ReposisiRekeningCandidate')
  }
  
  dictParam.update(dbutil.mapDBTableNames(config, 
    [
      'snapshot_balancerekening'        , 'RekeningTransaksi'
      , 'TreasuryAccount   '            , 'DetilTransaksiClass'
      , 'TreaProduk'                    , 'GLInterface'            
      , 'AccountInstance'  
      , 'seq_DailyBalanceTreasury'      , 'DailyBalanceTreasury'           
      , 'Transaksi'                     , 'DetilTransaksi'
      , 'HistTransaksi'                 , 'HistDetilTransaksi'
    ])
  )

  SQLFlows = [
    'DB_GenerateDailyBalance'          
    , 'DB_UpdateAccountInstanceLiab'     
    , 'DB_UpdateAccountInstanceKas'
    , 'DB_GenerateSnapshotBalance'
    , 'DB_UpdateDailyBalance'
    , 'DB_UnsetDailyBalanceLatestStatus' 
    , 'DB_SetDailyBalanceLatestStatus'
  ]
  
  batch_sql.executeFlow(config, SQLFlows, dictParam)

  app.ConWriteln('>>> Process selesai')
  
  return 1
  
SQL_RESOURCES = {
  # Hapus data untuk bulan yang bersangkutan
  'DB_PrepareTable' : '''
     truncate table {snapshot_balancerekening}
  '''
  ,
  'DB_GenerateDailyBalance' : ''' 
    insert into {DailyBalanceTreasury} ( 
       dailybalance_id
       , nomor_rekening
       , Balance_Date
       , Balance_Field 
       , Balance_HariLalu 
       , Balance_HariLalu_Ekuiv 
       , Credit 
       , Credit_Ekuiv 
       , Debit 
       , Debit_Ekuiv 
       , Balance 
       , Balance_Ekuiv 
       , Total_Mutation 
       , Total_Mutation_Ekuiv 
       , Is_Latest 
    )
    select {seq_DailyBalanceTreasury}.nextval
       , r.nomor_rekening
       , {Today}
       , {BalanceField!r} 
       , 0.0
       , 0.0
       , q.credit 
       , q.credit_ekuiv 
       , q.debit 
       , q.debit_ekuiv 
       , 0.0 
       , 0.0
       , (credit - debit ) * r.balance_sign
       , (credit_ekuiv - debit_ekuiv ) * r.balance_sign
       , null
    from {RekeningTransaksi} r
      INNER JOIN (
        select x.nomor_rekening
          , sum(case when x.jenis_mutasi = 'D' then  x.nilai_mutasi else 0.0 end) as debit
          , sum(case when x.jenis_mutasi = 'D' then  x.nilai_mutasi * x.nilai_kurs_manual else 0.0 end) as debit_ekuiv
          , sum(case when x.jenis_mutasi = 'C' then  x.nilai_mutasi else 0.0 end) as credit
          , sum(case when x.jenis_mutasi = 'C' then  x.nilai_mutasi * x.nilai_kurs_manual else 0.0 end) as credit_ekuiv
        FROM (
          SELECT
            dt.nomor_rekening, dt.jenis_mutasi, dt.nilai_mutasi, dt.nilai_kurs_manual
          from {Transaksi}  t
            INNER JOIN {DetilTransaksi} dt ON t.id_transaksi = dt.id_transaksi
            INNER JOIN {DetilTransaksiClass} dtc ON dt.kode_tx_class=dtc.kode_tx_class
          where 
            t.tanggal_transaksi = {Today}
            and dtc.accum_field_name='saldo'

          UNION ALL

          SELECT
            dt.nomor_rekening, dt.jenis_mutasi, dt.nilai_mutasi, dt.nilai_kurs_manual
          from {HistTransaksi}  t
            INNER JOIN {HistDetilTransaksi} dt ON t.id_transaksi = dt.id_transaksi
            INNER JOIN {DetilTransaksiClass} dtc ON dt.kode_tx_class=dtc.kode_tx_class
          where 
            t.tanggal_transaksi = {Today}
            and dtc.accum_field_name='saldo'
        ) x
        group by x.nomor_rekening
      ) q ON r.nomor_rekening = q.nomor_rekening
    where 
      r.kode_jenis in ('TRD', 'TRB', 'TRC', 'TRA')
  '''
  ,
  'DB_UpdateAccountInstanceLiab' : '''
     merge into {DailyBalanceTreasury} d
     using ( 
       select rt.nomor_rekening
           , a.accountinstance_id         
       from {RekeningTransaksi} rt
           , {TreasuryAccount} rl
           , {AccountInstance} a
           , {GLInterface} g
           , {DetilTransaksiClass} dtc
       where rt.nomor_rekening = rl.nomor_rekening  
         and rt.kode_cabang = a.branch_code
         and rt.kode_valuta = a.currency_code
         and g.kode_produk = rl.kode_produk
         and dtc.kode_jenis=rt.kode_jenis and g.kode_interface=dtc.kode_tx_class
         and dtc.accum_field_name='saldo'
         and g.kode_account = a.account_code
     ) r on (d.nomor_rekening = r.nomor_rekening)  
     when matched then update set 
       accountinstance_id = r.accountinstance_id 
     where accountinstance_id is null
  '''
  ,    
  # Generate data saldo untuk bulan yang bersangkutan   
  'DB_GenerateSnapshotBalance' : '''
     insert into {snapshot_balancerekening} 
        ( Nomor_Rekening
          , Balance_Field
          , Balance
          , Balance_Ekuiv
          , Daily_Balance_Id )
    select 
          r.Nomor_Rekening
          , {BalanceField!r}
          , nvl(db.Balance , 0.0)
          , nvl(db.Balance_Ekuiv , 0.0)
          , db.DailyBalance_Id  
    from 
          {RekeningTransaksi} r 
          left outer join 
            ( select db1.nomor_rekening
                 , db1.balance
                 , db1.balance_ekuiv
                 , db1.dailybalance_id
              from 
                 {DailyBalanceTreasury} db1                 
                 , ( select nomor_rekening, max(balance_date) as balance_date 
                     from {DailyBalanceTreasury} 
                     where balance_date < {Today}
                        and balance_field = {BalanceField!r} 
                     group by nomor_rekening                      
                    ) db2
              where db1.Nomor_Rekening = db2.Nomor_Rekening
                    and db1.balance_field = {BalanceField!r}
                    and db1.balance_date = db2.balance_date
                    -- and Balance_Date < {Today}
                    -- and db1.is_latest = 'T'
              ) db on (db.Nomor_Rekening = r.nomor_rekening)
        where exists(
                select 1 from {DailyBalanceTreasury} 
                where nomor_rekening = r.nomor_rekening
                    and is_latest is NULL
                    and balance_field = {BalanceField!r}
                 )
        order by nomor_rekening
  '''
  ,
  # Update Tanggal Penutupan Rekening Tabungan Giro
  'DB_UpdateDailyBalance' : '''
      update {DailyBalanceTreasury} db set 
          (IS_LATEST
          , BALANCE , BALANCE_EKUIV 
          , BALANCE_HARILALU , BALANCE_HARILALU_EKUIV)
          = 
          (select 'T' 
               ,  s.balance + db.total_mutation
               ,  s.balance_ekuiv + db.total_mutation_ekuiv
               ,  s.balance 
               ,  s.balance_ekuiv 
             from {snapshot_balancerekening} s 
             where s.nomor_rekening = db.nomor_rekening)
      where balance_date = {Today} and is_latest is null
          
  '''
  ,   
  # Update Tanggal Pencairan Awal Deposito
  'DB_UnsetDailyBalanceLatestStatus'  : '''  
     update {DailyBalanceTreasury} db 
     set is_latest = 'F'
     where exists (
          select 1 from {snapshot_balancerekening}
          where nomor_rekening = db.nomor_rekening   
        )
      and is_latest = 'T'
      and balance_field = {BalanceField!r}
      and Balance_Date < {Today}
                 
  '''
  ,
  # Update Tanggal Jatuh Tempo Deposito  
  'DB_SetDailyBalanceLatestStatus' : '''
     update {DailyBalanceTreasury} db 
     set is_latest = 'T'
     where is_latest is null
       and balance_field = {BalanceField!r}
       and Balance_Date = {Today} 
  '''

}  