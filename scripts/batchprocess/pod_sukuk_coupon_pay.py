import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil
import calendar

# PRACONDITION: ALL EOD TEMPORARY TABLES HAS BEEN CREATED
# IF NOT, RUN pod_tmp_tables.py first !

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}, 
  {'name': 'sDate', 'short_desc': 'process date', 'long_desc': 'process date is in format dd-MMM-yyyy example: 01-Jan-2013'}
]

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

# GLOBALS
_CONSOLE = False
if not _CONSOLE:
  config = appserver.ActiveConfig
  app    = config.AppObject
#--

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
  app = config.AppObject
  ioutil.IO_MODE = ioutil.IOMODE_DAF
  ioutil.APP_OBJECT = app
  app.ConCreate('out')
                                
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1
#-- END OF SCRIPT INJECTION
    
def main(config, debugLevel = None):
  #metaConfig = argValues['metaConfig']
  #strDate = argValues['sDate']   
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  oLastDay = oToday.GetLastDay()
  #oNextDay = oToday.NextWorkDay()
  float_NextDay = oLastDay.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)
  strNDate = config.FormatDateTime('dd-mmm-yyyy', float_NextDay)

  #get last day in month
  my_date = config.FormatDateTime('yyyymm', float_oToday)  
  curdate = int(strDate[:2])
  y = int(my_date[:4])
  m = int(my_date[4:])
  last_day = calendar.monthrange(y,m)[1]

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('>> Mulai proses')  
  
  if config == None: config = podUtil.openConfig(metaConfig)
  dbutil.DEBUG_LEVEL = debugLevel or 1
  
  #raise Exception, strNDate
  #strNDate = '01-mar-2019'
  runTreasuryAccrue(config, strDate, strNDate)
  
  app.ConWriteln('>> Proses selesai')
#--
    
def runTreasuryAccrue(config, strDate, strNDate): 
  helper = phelper.PObjectHelper(config)
  MASTER_SRC_TABLE = 'tmp.ftmp_sukuk_kupon'
  BALANCE_FIELDS = []
  MUT_FIELDS = ['mut_pymad', 'mut_profit_accrue', 'mut_pdpt_sukuk']
  MUT_CLASSGROUP_FIELDS = [
    {'field': 'mut_pymad', 'classgroup': 'tr_pymad'},
    {'field': 'mut_profit_accrue', 'classgroup': 'tr_accrue'},
    {'field': 'mut_pajak_rev', 'classgroup': 'tr_pajak'},
    
    {'field': 'mut_pajak', 'classgroup': 'tr_pajak'},
    {'field': 'mut_pdpt_sukuk', 'classgroup': 'tr_profit'}
  ]
  
  mlu = config.ModLibUtils
  MUT_TDP_FIELDS = [
    {'field': 'mut_pdpt_bank', 'acc_code': 'norek_banksrc', 'ket':'Pembayaran Kupon'}
  ]
  pod_fin_common = modman.getModule(config, 'server_scripts#pod_common_collective_2')

  pct_pajak = helper.GetObject('ParameterGlobalTreasury', 'PJK_INV').nilai_parameter or 15 
  dictParam = {
    'srcTableName': config.MapDBTableName(MASTER_SRC_TABLE),
    'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
    'treasuryaccount': config.MapDBTableName('treasuryaccount'),
    'treasuratberharga': config.MapDBTableName('treasuratberharga'),
    'treaproduk': config.MapDBTableName('treaproduk'),
    'jenistreasury': config.MapDBTableName('jenistreasury'),
    'date': mlu.QuotedStr(strNDate),
    #'nm_date': mlu.QuotedStr(snm_date), 
    'pct_pajak': pct_pajak/100 

    ,'detiltransaksi' : config.MapDBTableName('core.detiltransaksi')
    ,'detiltransaksiclass' : config.MapDBTableName('core.detiltransaksiclass')
    ,'seq_detiltransaksi' : config.MapDBTableName('core.seq_detiltransaksi')
    ,'glinterface' : config.MapDBTableName('glinterface')
    ,'currency' : config.MapDBTableName('core.currency')
  }
  
  selectData(config, dictParam)
  pod_fin_common.initBalanceAndMutationFields(config, MASTER_SRC_TABLE, 
    BALANCE_FIELDS,
    MUT_FIELDS  
  )
  createTxValues(config, dictParam)
  deleteNoTxRows(config, dictParam)
  
  #'''
  pod_fin_common.prepareStdTxFields(config, MASTER_SRC_TABLE)
  pod_fin_common.createTxDetails(config, MASTER_SRC_TABLE, 'trea_kupon', MUT_CLASSGROUP_FIELDS)

  DebetRekBank(config, dictParam, MUT_TDP_FIELDS) 
  UpdateSaldoRekening(config, dictParam)

  pod_fin_common.createCoreTransaction(config, MASTER_SRC_TABLE, 'trea_kupon', strNDate)
  pod_fin_common.updateBalanceFromTransaction(config, MASTER_SRC_TABLE, MUT_CLASSGROUP_FIELDS)
  postProcess(config, dictParam)
  #'''
  
def selectData(config, dParam): 
  dbutil.runSQL(config, '''
       DELETE FROM %(srcTableName)s
    ''' % dParam
  )

  #update by BG penjumlahan timpstamp dalam orecle hasilnya <> number, harus di extract[day,hour,minute,second] jika ingin dapat number
  dbutil.runSQL(config, '''  
    INSERT INTO %(srcTableName)s (
      nomor_rekening
      , hold_period, hold_period_act
      , initial_accrue
      , accrued_amount
      , yearfac, accrint
      , nominal_deal
      , mut_pajak_rev
      , tgl_bayar_terakhir, tgl_bayar_berikutnya, tgl_jatuh_tempo
      , tipe_kupon, norek_banksrc
      , tanggal_transaksi
      , ktr_detil, ktr_transaksi, ktr_detiltransgroup
    )
    SELECT  
      nomor_rekening
      , hold_period, hold_period_act
      , initial_accrue
      , saldo_accrue
      , round(hold_period_act/hold_period, 12) yearfac
      , round(kp_rate * nominal_deal, 8) accrint
      , nominal_deal
      , -nvl(nominal_pajak,0)
      , tgl_bayar_terakhir, tgl_bayar_berikutnya, tgl_jatuh_tempo
      , tipe_kupon
      , norek_banksrc
      , tgl_bayar_berikutnya as tgl_trx
      , ket_, ket_, ket_
    FROM (
      SELECT 
        a.nomor_rekening
        , a.norek_banksrc
        , nvl(a.ujroh,0) as initial_accrue
        , a.saldo_accrue 
        , extract(day from f.tgl_bayar_berikutnya-greatest(f.tgl_settlement, f.tgl_bayar_terakhir)) as hold_period_act                                               
        , extract(day from f.tgl_bayar_berikutnya-f.tgl_bayar_terakhir) as hold_period
        , a.eqv_rate_real / (12/tipe_kupon*100) kp_rate                                               
        , f.tgl_settlement, f.tgl_bayar_berikutnya, f.tgl_bayar_terakhir, a.tgl_jatuh_tempo
        , f.tipe_kupon
        , -b.saldo nominal_deal
        , f.nominal_pajak
        , 'PEMBAYARAN KUPON '||d.keterangan ||' - ' || f.kode_instrumen ||' - ' || To_Char(f.tgl_bayar_berikutnya, 'YYYYMM') ket_
      FROM %(treasuryaccount)s a
        INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening 
        INNER JOIN %(treasuratberharga)s f ON a.nomor_rekening=f.nomor_rekening
        left JOIN %(treaproduk)s c ON a.kode_produk=c.kode_produk
        left JOIN %(jenistreasury)s d ON a.jenis_treasuryaccount=d.jenis_treasuryaccount
      WHERE 
        b.status_rekening=1 and b.saldo < -0.1 AND 
        to_date(%(date)s) >= f.tgl_bayar_berikutnya
  		  --AND to_date(%(date)s) <= a.tgl_jatuh_tempo
        --AND a.nomor_rekening='MIG.TRB00006'
    )
    ''' % dParam
  )
        
  dbutil.runSQL(config, '''  
    select 1 FROM %(treasuryaccount)s WHERE nomor_rekening in
    (select nomor_rekening from %(srcTableName)s) FOR UPDATE NOWAIT
    ''' % dParam
  )
      
# generate transaction values in columns  
def createTxValues(config, dParam):
  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, '''
    merge INTO %(srcTableName)s x USING ( 
      SELECT nomor_rekening
        , a.accrint mut_kupon
        , (a.yearfac*a.accrint)*%(pct_pajak)s mut_pajak
        , accrued_amount, mut_pajak_rev
        , a.initial_accrue
      FROM %(srcTableName)s a                                                
    ) y ON (x.nomor_rekening=y.nomor_rekening AND y.mut_kupon>0)
    WHEN matched THEN UPDATE SET 
      x.mut_profit_accrue = (y.accrued_amount-y.initial_accrue-mut_pajak_rev),
      x.mut_pymad         = -(y.accrued_amount),
      
      x.mut_pdpt_bank     = (y.mut_pajak - y.mut_kupon),
      x.mut_pajak         = -y.mut_pajak,
      x.mut_pdpt_sukuk    = (y.mut_kupon+y.initial_accrue)
    ''' % dParam
  )
#--

# delete no-transaction rows to reduce processing request
def deleteNoTxRows(config, dParam):
  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, ''' 
      DELETE FROM %(srcTableName)s WHERE Nvl(mut_pdpt_sukuk,0)<0.1 
    ''' % dParam
  )
#--   

def DebetRekBank(config, dictParam, fieldNames):
  app = config.AppObject
  for fieldInfo in fieldNames:
    dParam = {
        'field': fieldInfo['field'],
        'acc_code': fieldInfo['acc_code'],
        'ket_detil': fieldInfo['ket']
      }
    dParam.update(dictParam)

    sSQL = '''  
      INSERT INTO %(detiltransaksi)s (
        Id_Detil_Transaksi, Tanggal_Transaksi, Jenis_Mutasi, Nilai_Mutasi, Keterangan, 
        Kode_Cabang, Kode_Valuta, Jenis_Detil_Transaksi, Nilai_Kurs_Manual, Nilai_Ekuivalen, 
        Saldo_Awal, Saldo_Akhir, Nomor_Referensi, Kode_Kurs, Kode_Account, 
        Kode_Valuta_RAK, Kode_Jurnal, Nominal_Biaya, IsBillerTransaction, hide_passbook, 
        hide_statement, hide_gentrans, subsystem_code, sub_tx_code, kode_subtx_class, 
        Id_Transaksi, Nomor_Rekening, kode_tx_class, id_detiltransgroup, Id_Parameter_Transaksi, 
        Id_Parameter, ID_JournalBlock
      )
      SELECT 
        %(seq_detiltransaksi)s.nextval, 
        ms.tanggal_transaksi, 
        case when ms.mut_pdpt_bank<0 then 'D' else 'C' end as mnemonic, 
        -ms.mut_pdpt_bank as amount, 
        substr(ms.ktr_detil, 1, 100), 
        rt.kode_cabang, nvl(rt.kode_valuta,'IDR'), 
        NULL, 
        cr.kurs_buku, 
        -ms.mut_pdpt_bank*cr.kurs_buku as n_eqv, 
        NULL, NULL, 'AUTOREF', 
        'BOOKING' as kode_kurs, 
        gli.kode_account as kode_account, 
        NULL, 
        '11' as kode_jurnal, 
        0.0, 'F', 'F', 
        'F', 'F', NULL, NULL, NULL, 
        ms.id_transaksi, 
        ms.norek_banksrc, 
        gli.kode_interface, 
        ms.id_detiltransgroup 
        ,NULL, NULL, NULL
      FROM
        %(srcTableName)s ms
        inner join %(rekeningtransaksi)s rt on rt.nomor_rekening=ms.norek_banksrc
        inner join %(treasuryaccount)s hj on hj.nomor_rekening=ms.norek_banksrc
        inner join %(glinterface)s gli on hj.kode_produk=gli.kode_produk and gli.kode_interface='37001'
        inner join %(currency)s cr on rt.kode_valuta=cr.currency_code
      where ms.mut_pdpt_bank < 0      
    ''' % dParam 
    app.ConWriteln(sSQL)
    dbutil.runSQL(config, sSQL)
  #--
#--

def UpdateSaldoRekening(config, dParam): 
  #update saldo rekening liability
  sSQL = '''  
      merge into %(rekeningtransaksi)s x using
      (
        SELECT 
          dt.nomor_rekening, sum(decode(dt.jenis_mutasi,'D', -dt.nilai_mutasi, dt.nilai_mutasi)) jml_trx
        FROM %(detiltransaksi)s dt       
          inner join %(detiltransaksiclass)s dtc on dtc.kode_tx_class=dt.kode_tx_class and dtc.accum_field_name='saldo'      
        where 
          EXISTS (select 1 from %(srcTableName)s ms where dt.id_transaksi=ms.id_transaksi)
        group by dt.nomor_rekening       
      ) y on (x.nomor_rekening=y.nomor_rekening)
      when matched then update set
         x.saldo = nvl(x.saldo,0)+y.jml_trx 
  ''' % dParam
  dbutil.runSQL(config, sSQL)
  #--
#---

def postProcess(config, dParam):
  #update next coupon date
  sSQL = '''
    merge into %(treasuratberharga)s x using (
      select nomor_rekening
        , ADD_MONTHS(tgl_bayar_berikutnya, tipe_kupon) ndc
        , tgl_jatuh_tempo
        , tgl_bayar_berikutnya
        , mut_pajak_rev
      from %(srcTableName)s 
    )y on (x.nomor_rekening=y.nomor_rekening)
    when matched then update set 
      x.tgl_bayar_terakhir     = y.tgl_bayar_berikutnya
      , x.tgl_bayar_berikutnya = y.ndc
      , x.nominal_pajak = nvl(x.nominal_pajak,0)+y.mut_pajak_rev
    ''' % dParam
  dbutil.runSQL(config, sSQL)

  sSQL = '''
    merge into %(treasuryaccount)s x using
      %(srcTableName)s y on (x.nomor_rekening=y.nomor_rekening)
    when matched then update set 
      x.ujroh = nvl(x.ujroh,0)-y.initial_accrue
    ''' % dParam
  dbutil.runSQL(config, sSQL)
#--
  
if __name__ == "__main__":
  podUtil.printArgs()
  argValues = podUtil.checkArgs('pod_sukuk_coupon_pay.py', ARG_LIST, 2, False)
  dbutil.DEBUG_LEVEL = 0
  main(argValues)
#--
  