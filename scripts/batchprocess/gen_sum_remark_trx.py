'''
Fungsi ini digunakan untuk generate data saldo harian.
Pergerakan saldo harian akan dicatat jika memenuhi kondisi berikut :
  - ada transaksi (dilihat dari tabel transaksi dan detiltransaksi)
  - ada perubahan kode cabang rekening (dilihat dari tabel inforeposisirekening)
  - ada perubahan kode produk rekening (dilihat dari tabel infoubahprodukrekening)
  Catatan : perubahan kode cabang / kode produk mungkin saja lebih dari 1 kali dalam 1 hari untuk rekening yg sama 
    
Selain itu terdapat juga mencatat accountinstance_id yang sesuai dengan produk rekening 
agar dapat dimanfaatkan sebagai data LBV (Ledger Balance Verification)

'''
import sys
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman
import com.ihsan.foundation.appserver as appserver

# GLOBALS
config  = appserver.ActiveConfig
app     = config.AppObject
_CONSOLE = False

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  
  global _CONSOLE
  
  _CONSOLE = False
  
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()

  main(config, oToday)
  returns.CreateValues(['Is_Err', 0])

  return 1

def main(config, oToday):    
  app = config.AppObject
  app.ConCreate('out')
  mlu = config.ModLibUtils
  
  printOut('>> Create Summary Transaksi Per Remark')
  float_oToday = oToday.GetDate()
  oLastDay = oToday.GetLastDay()
  float_NextDay = oLastDay.GetDate()
  
  oProcDay = oToday.PrevWorkDay()
  fPrevDay = oProcDay.GetDate()

  #oNextDay = oToday.NextWorkDay()
  tgl_awal = config.FormatDateTime('01-mmm-yyyy', fPrevDay)
  tgl_akhir = config.FormatDateTime('dd-mmm-yyyy', fPrevDay)
  periode = config.FormatDateTime('yyyy-mm', fPrevDay)
  #tgl_awal = '01-DEC-2018'
  #tgl_akhir = '31-DEC-2018'
  #periode = '2018-12'
  
  dictParam = {
    'tgl_awal': mlu.QuotedStr(tgl_awal),
    'tgl_akhir': mlu.QuotedStr(tgl_awal),
    'periode': mlu.QuotedStr(periode)
  }
   
  dictParam.update(dbutil.mapDBTableNames(config, 
    [
      'rekeningtransaksi'
      , 'treasuryaccount'            
      , 'treacounterpart'
      , 'enterprise.cabang'
      , 'transaksi'
      , 'histtransaksi'            
      , 'smt.tmp_transaksijamaah'
      , 'smt.tmp_trea_transaksi'
      , 'smt.seq_sum_rkolah'
      , 'smt.sum_rkolah' 
      , 'smt.trx_mapping'          
    ])
  )

  sSQL = ''' 
      delete from {sum_rkolah} where last_trx > to_date({tgl_awal})
    '''.format(**dictParam)
  app.ConWriteln(sSQL)
  dbutil.runSQL(config, sSQL)  

  # generate sum rk olah transaksi jemaah
  sSQL = ''' 
    INSERT INTO {sum_rkolah} (
    	NOMOR_REKENING
    	, NAMA_REKENING
    	, KD_BANK
    	, NAMA_BANK
    	, PERIODE
    	, KD_REMARK
    	, GROUP_REMARK
    	, CABANG
    	, KODE_VALUTA
    	, JNS_MUTASI
    	, NOMINAL_TRX
    	, JML_TX
    	, LAST_TRX
    )
    SELECT
    	x.nomor_rekening_debet, x.nama_rekening 
    	, x.kd_bank, x.nama_bank, x.periode
    	, x.kd_remark, x.group_transaksi
    	, x.nama_cabang, x.kode_valuta
    	, x.jns_mutasi 
    	, sum(x.nominal_transaksi) as nominal_trx
    	, count(1) jml_trx
    	, max(x.tanggal_transaksi)
    FROM (
    	SELECT 
    	  tc.kode_counterpart kd_bank, tc.nama_counterpart nama_bank
    	  , smt.nomor_rekening_debet, rt.nama_rekening
    	  , to_char(tr.tanggal_transaksi, 'YYYY-MM') periode
    	  , smt.jenis_transaksi kd_remark, m.group_transaksi
    	  , tr.tanggal_transaksi
    	  , c.nama_cabang
    	  , smt.jns_mutasi
    	  , smt.kode_valuta
    	  , smt.NOMINAL_TRANSAKSI
    	FROM {transaksi} tr 
    	  INNER JOIN {tmp_transaksijamaah} smt on tr.id_transaksi=smt.id_transaksi
    	  INNER JOIN {rekeningtransaksi} rt on smt.nomor_rekening_debet=rt.nomor_rekening  
    	  INNER JOIN {treacounterpart} tc on tc.kode_counterpart=smt.kode_counterpart
    	  INNER JOIN {cabang} c on c.kode_cabang=smt.kode_cabang
    	  INNER JOIN {trx_mapping} m on m.kode_remark=smt.jenis_transaksi
    	WHERE  
    	  to_char(tr.tanggal_transaksi, 'YYYY-MM') = {periode}
    
    	UNION ALL
    
    	SELECT 
    	  tc.kode_counterpart kd_bank, tc.nama_counterpart nama_bank
    	  , smt.nomor_rekening_debet, rt.nama_rekening
    	  , to_char(tr.tanggal_transaksi, 'YYYY-MM') periode
    	  , smt.jenis_transaksi kd_remark, m.group_transaksi
    	  , tr.tanggal_transaksi
    	  , c.nama_cabang
    	  , smt.jns_mutasi
    	  , smt.kode_valuta
    	  , smt.NOMINAL_TRANSAKSI
    	FROM {histtransaksi} tr 
    	  INNER JOIN {tmp_transaksijamaah} smt on tr.id_transaksi=smt.id_transaksi
    	  INNER JOIN {rekeningtransaksi} rt on smt.nomor_rekening_debet=rt.nomor_rekening  
    	  INNER JOIN {treacounterpart} tc on tc.kode_counterpart=smt.kode_counterpart
    	  INNER JOIN {cabang} c on c.kode_cabang=smt.kode_cabang
    	  INNER JOIN {trx_mapping} m on m.kode_remark=smt.jenis_transaksi
    	WHERE  
    	  to_char(tr.tanggal_transaksi, 'YYYY-MM') = {periode}
    ) x
    GROUP BY x.nomor_rekening_debet, x.nama_rekening, x.kd_bank, x.nama_bank, x.periode, x.kd_remark, x.group_transaksi, x.nama_cabang, x.kode_valuta, x.jns_mutasi
  '''.format(**dictParam)
  app.ConWriteln(sSQL)
  dbutil.runSQL(config, sSQL)  

  # generate sum rk olah transaksi non jemaah
  sSQL = ''' 
    INSERT INTO {sum_rkolah} (
    	NOMOR_REKENING
    	, NAMA_REKENING
    	, KD_BANK
    	, NAMA_BANK
    	, PERIODE
    	, KD_REMARK
    	, GROUP_REMARK
    	, CABANG
    	, KODE_VALUTA
    	, JNS_MUTASI
    	, NOMINAL_TRX
    	, JML_TX
    	, LAST_TRX
    )
    SELECT
    	x.nomor_rekening, x.nama_rekening 
    	, x.kd_bank, x.nama_bank, x.periode
    	, x.kode_remark, x.group_transaksi
    	, x.nama_cabang, x.kode_valuta
    	, x.jenis_mutasi 
    	, sum(x.nominal) as nominal_trx
    	, count(1) jml_trx
    	, max(x.tanggal_transaksi)
    FROM (
    	SELECT 
    	  smt.nomor_rekening, rt.nama_rekening
    	  , tc.kode_counterpart kd_bank, tc.nama_counterpart nama_bank
    	  , to_char(tr.tanggal_transaksi, 'YYYY-MM') periode
    	  , smt.kode_remark, m.group_transaksi
    	  , c.nama_cabang, smt.kode_valuta
    	  , smt.jenis_mutasi
    	  , smt.nominal
    	  , tr.tanggal_transaksi
    	FROM {transaksi} tr 
    	  INNER JOIN {tmp_trea_transaksi} smt on tr.id_transaksi=smt.id_transaksi
    	  INNER JOIN {rekeningtransaksi} rt on smt.nomor_rekening=rt.nomor_rekening  
    	  INNER JOIN {treasuryaccount} ta on ta.nomor_rekening = rt.nomor_rekening
    	  INNER JOIN {treacounterpart} tc on tc.kode_counterpart=ta.kode_counterpart
    	  INNER JOIN {cabang} c on c.kode_cabang=smt.kode_cabang
    	  INNER JOIN {trx_mapping} m on m.kode_remark=smt.kode_remark
    	WHERE  
    	  to_char(tr.tanggal_transaksi, 'YYYY-MM') = {periode}
    
    	UNION ALL
    
    	SELECT 
    	  smt.nomor_rekening, rt.nama_rekening
    	  , tc.kode_counterpart kd_bank, tc.nama_counterpart nama_bank
    	  , to_char(tr.tanggal_transaksi, 'YYYY-MM') periode
    	  , smt.kode_remark, m.group_transaksi
    	  , c.nama_cabang, smt.kode_valuta
    	  , smt.jenis_mutasi
    	  , smt.nominal
    	  , tr.tanggal_transaksi
    	FROM {histtransaksi} tr 
    	  INNER JOIN {tmp_trea_transaksi} smt on tr.id_transaksi=smt.id_transaksi
    	  INNER JOIN {rekeningtransaksi} rt on smt.nomor_rekening=rt.nomor_rekening  
    	  INNER JOIN {treasuryaccount} ta on ta.nomor_rekening = rt.nomor_rekening
    	  INNER JOIN {treacounterpart} tc on tc.kode_counterpart=ta.kode_counterpart
    	  INNER JOIN {cabang} c on c.kode_cabang=smt.kode_cabang
    	  INNER JOIN {trx_mapping} m on m.kode_remark=smt.kode_remark
    	WHERE  
    	  to_char(tr.tanggal_transaksi, 'YYYY-MM') = {periode}
    ) x
    GROUP BY x.nomor_rekening, x.nama_rekening, x.kd_bank, x.nama_bank, x.periode, x.kode_remark, x.group_transaksi, x.nama_cabang, x.kode_valuta, x.jenis_mutasi
  '''.format(**dictParam)
  app.ConWriteln(sSQL)
  dbutil.runSQL(config, sSQL)  

  app.ConWriteln('>>> Process selesai')
  
  return 1
  
  