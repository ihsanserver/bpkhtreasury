import dafsys4
import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.modman as modman
import rpdb2
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil

# PRACONDITION: ALL EOD TEMPORARY TABLES HAS BEEN CREATED
# IF NOT, RUN pod_tmp_tables.py first !

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}, 
  {'name': 'sDate', 'short_desc': 'process date', 'long_desc': 'process date is in format dd-MMM-yyyy example: 01-Jan-2013'}
]

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

# GLOBALS
_CONSOLE = False
if not _CONSOLE:
  config = appserver.ActiveConfig
  app    = config.AppObject
#--

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
  
  app = config.AppObject
  ioutil.IO_MODE = ioutil.IOMODE_DAF
  ioutil.APP_OBJECT = app
  app.ConCreate('out')
                                
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1
#-- END OF SCRIPT INJECTION
    
def main(config = None, debugLevel = None):
  #rpdb2.start_embedded_debugger('000')
  #metaConfig = argValues['metaConfig']
  #strDate = argValues['sDate']
  
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)
  
  app.ConWriteln('>> Mulai proses')  
  
  if config == None: config = podUtil.openConfig(metaConfig)
  
  dbutil.DEBUG_LEVEL = debugLevel or 1
  config.BeginTransaction()
  try:
    finjournal = modman.getModule(config, 'TreasuryJournal')
    finjournal.createTreasuryJournal(config, strDate, None)
    config.Commit()
  except:
    config.Rollback()
    raise
  #--
#--
    
if __name__ == "__main__":
  podUtil.printArgs()
  argValues = podUtil.checkArgs('pod_treasury_journal.py', ARG_LIST, 2, False)
  dbutil.DEBUG_LEVEL = 0
  main(argValues)
#--
  