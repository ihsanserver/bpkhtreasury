import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.timeutil as timeutil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.ioutil as ioutil
import calendar

# PRACONDITION: ALL EOD TEMPORARY TABLES HAS BEEN CREATED
# IF NOT, RUN pod_tmp_tables.py first !

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}, 
  {'name': 'sDate', 'short_desc': 'process date', 'long_desc': 'process date is in format dd-MMM-yyyy example: 01-Jan-2013'}
]

#-- POD SCRIPT INJECTION - START HERE...    
import com.ihsan.foundation.appserver as appserver

# GLOBALS
_CONSOLE = False
if not _CONSOLE:
  config = appserver.ActiveConfig
  app    = config.AppObject
#--
dParam={}

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
  app = config.AppObject
  ioutil.IO_MODE = ioutil.IOMODE_DAF
  ioutil.APP_OBJECT = app
  app.ConCreate('out')
                                
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1
#-- END OF SCRIPT INJECTION
    
def main(config, debugLevel = None):
  #metaConfig = argValues['metaConfig']
  #strDate = argValues['sDate']   
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()
  float_oToday = oToday.GetDate()
  strDate = config.FormatDateTime('dd-mmm-yyyy', float_oToday)

  app = config.AppObject
  app.ConCreate('out')
  app.ConWriteln('>> Mulai proses')  
  
  if config == None: config = podUtil.openConfig(metaConfig)
  dbutil.DEBUG_LEVEL = debugLevel or 1
      
  runTreasuryMaturity(config, strDate)
  
  app.ConWriteln('>> Proses selesai')
#--
    
def runTreasuryMaturity(config, strDate):
  
  MASTER_SRC_TABLE = 'tmp.ftmp_treasury_maturity'
  BALANCE_FIELDS = ['saldo', 'saldo_margin']     
  MUT_FIELDS = ['mut_saldo', 'mut_giro_bi', 'mut_profit_amount']
  MUT_CLASSGROUP_FIELDS = [
    {'field': 'mut_saldo', 'classgroup': 'tr_nominal'},
    {'field': 'mut_giro_bi', 'classgroup': 'tr_giro_bi'},
    {'field': 'mut_profit_amount', 'classgroup': 'tr_profit'},
  ]
  
  mlu = config.ModLibUtils
  pod_fin_common = modman.getModule(config, 'server_scripts#pod_common_collective')

  dictParam = {
    'date': mlu.QuotedStr(strDate),
    'srcTableName': config.MapDBTableName(MASTER_SRC_TABLE),
    'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
    'treasuryaccount': config.MapDBTableName('treasuryaccount'),
    'treaproduk': config.MapDBTableName('treaproduk'),
    'treasima': config.MapDBTableName('treasima'),
    'jenistreasury': config.MapDBTableName('jenistreasury')
  }
  dParam.update(dictParam)
  
  selectData(config)
  pod_fin_common.initBalanceAndMutationFields(config, MASTER_SRC_TABLE, 
    BALANCE_FIELDS,
    MUT_FIELDS  
  )
  createTxValues(config)
  deleteNoTxRows(config)

  pod_fin_common.prepareStdTxFields(config, MASTER_SRC_TABLE)
  pod_fin_common.createTxDetails(config, MASTER_SRC_TABLE, 'trea_maturity', MUT_CLASSGROUP_FIELDS)
  pod_fin_common.createCoreTransaction(config, MASTER_SRC_TABLE, 'trea_maturity', strDate)
  pod_fin_common.updateBalanceFromTransaction(config, MASTER_SRC_TABLE, MUT_CLASSGROUP_FIELDS)
  postProcess(config)
  
def selectData(config):
  
  dbutil.runSQL(config, '''
     DELETE FROM %(srcTableName)s
    ''' % dParam
  )

  #update by BG penjumlahan timpstamp dalam orecle hasilnya <> number, harus di extract[day,hour,minute,second] jika ingin dapat number
  dbutil.runSQL(config, '''  
    INSERT INTO %(srcTableName)s (
      nomor_rekening,
      ujroh,
      saldo,
      saldo_margin,
      
      ktr_detil, ktr_transaksi, ktr_detiltransgroup
    )
    SELECT 
      a.nomor_rekening
      , a.ujroh
      , b.saldo
      , a.saldo_margin
      , 'JATUH TEMPO '||d.keterangan
      , 'JATUH TEMPO '||d.keterangan 
      , 'JATUH TEMPO '||d.keterangan ||' - ' || a.nomor_rekening
    FROM %(treasuryaccount)s a
      INNER JOIN %(rekeningtransaksi)s b ON a.nomor_rekening=b.nomor_rekening
      left JOIN %(treaproduk)s c ON a.kode_produk=c.kode_produk
      left JOIN %(jenistreasury)s d ON a.jenis_treasuryaccount=d.jenis_treasuryaccount
    WHERE a.jenis_treasuryaccount IN ('S')
      AND b.status_rekening=1
      AND a.tgl_jatuh_tempo<=%(date)s
    ''' % dParam
  )
        
  dbutil.runSQL(config, '''  
    select 1 FROM %(treasuryaccount)s WHERE nomor_rekening in
    (select nomor_rekening from %(srcTableName)s) FOR UPDATE NOWAIT
    ''' % dParam
  )
      
# generate transaction values in columns  
def createTxValues(config):
  #update by BG add NVL(accrued_amount)
  dbutil.runSQL(config, '''
    UPDATE %(srcTableName)s SET
      mut_giro_bi =(saldo-(ujroh-saldo_margin)) 
      , mut_saldo =-saldo 
      , mut_profit_amount=(ujroh-saldo_margin)
    ''' % dParam
  )
#--

# delete no-transaction rows to reduce processing request
def deleteNoTxRows(config):
  pass  
#--   

def postProcess(config):
  #update status rekening jadi JT
  dbutil.runSQL(config, '''
    merge into %(rekeningtransaksi)s x using
      %(srcTableName)s y on (x.nomor_rekening=y.nomor_rekening)
    when matched then update set 
      x.status_rekening = 3 
    ''' % dParam
  )

  #update tgl tutup
  dbutil.runSQL(config, '''
    merge into %(treasuryaccount)s x using
      %(srcTableName)s y on (x.nomor_rekening=y.nomor_rekening)
    when matched then update set 
      x.tgl_tutup = %(date)s 
    ''' % dParam
  )
#--
  
if __name__ == "__main__":
  podUtil.printArgs()
  argValues = podUtil.checkArgs('pod_sima_maturity.py', ARG_LIST, 2, False)
  dbutil.DEBUG_LEVEL = 0
  main(argValues)
#--
  