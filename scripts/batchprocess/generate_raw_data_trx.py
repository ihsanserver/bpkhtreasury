import sys
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman
import com.ihsan.foundation.appserver as appserver
import com.ihsan.util.timeutil as timeutil
import com.ihsan.fileutils as utils

# GLOBALS
config = appserver.ActiveConfig
app    = config.AppObject
_CONSOLE = False

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  global _CONSOLE
  
  _CONSOLE = False
  
  main(config)
  returns.CreateValues(['Is_Err', 0])

  return 1

def main(config):
  helper = phelper.PObjectHelper(config)
  oToday = helper.CreateObject('core.PeriodHelper').GetToday()
  oProcDay = oToday.PrevWorkDay()  
  fPrevDay = oProcDay.GetDate()
  #fPrevDay = config.ModLibUtils.EncodeDate(2019,01,31)
  
  if not _CONSOLE:  
    global app
    app.ConCreate('out')
  #--
                                
  printOut('Processing raw data transaksi %s...' % oProcDay.GetDateText())   

  LsScriptName = [ 
    'gen_report_trx_remark',
    'gen_trx_remark_detail',
    'gen_saldo_bank'    
    ]
  LsResultDir = {}
  # --- SALDO HARIAN
  periode = config.FormatDateTime('yyyymmdd', fPrevDay)
  ResultDir = 'c:/dafapp/rawdata/jemaah/%s' % periode

  if not utils.DirectoryExist(ResultDir):
    utils.CreateDeepDirectory(ResultDir)
              
  for ScriptName in LsScriptName :
    printOut('Execute Script %s.py ...' % ScriptName )
    
    moduleReport = modman.getModule(config, 'scripts#reports/' + ScriptName)
    moduleReport.GenerateFile(config, fPrevDay, ResultDir)
    
    