'''
Fungsi ini digunakan untuk generate data saldo harian.
'''
import sys
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman
import com.ihsan.foundation.appserver as appserver

# GLOBALS
config  = appserver.ActiveConfig
app     = config.AppObject
_CONSOLE = False

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  
  global _CONSOLE
  
  _CONSOLE = False
  
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()

  main(config, oToday)
  returns.CreateValues(['Is_Err', 0])

  return 1

def main(config, oToday):    
  float_oToday = oToday.GetDate()
  oLastDay = oToday.GetLastDay()
  float_NextDay = oLastDay.GetDate()
  
  oProcDay = oToday.PrevWorkDay()
  fPrevDay = oProcDay.GetDate()

  #oNextDay = oToday.NextWorkDay()
  tgl_awal = config.FormatDateTime('01-mmm-yyyy', fPrevDay)
  tgl_akhir = config.FormatDateTime('dd-mmm-yyyy', fPrevDay)
  #tgl_awal = '01-JAN-2019'
  #tgl_akhir = '31-JAN-2019'
    
  app = config.AppObject
  app.ConCreate('out')
  mlu = config.ModLibUtils
  
  printOut('>> Create Daily Balance Haji')

  dictParam = {
    'tgl_awal': mlu.QuotedStr(tgl_awal),
    'tgl_akhir': mlu.QuotedStr(tgl_akhir),
  }
   
  dictParam.update(dbutil.mapDBTableNames(config, 
    [
      'hajjaccount'        
      , 'rekeningtransaksi'
      , 'treasuryaccount'            
      , 'treacounterpart'
      , 'transaksi'
      , 'histtransaksi'
      , 'detiltransaksi'
      , 'histdetiltransaksi'            
      , 'detiltransaksiclass'
      , 'seq_dailybalance_haji'
      , 'dailybalance_haji'           
    ])
  )

  sSQL = ''' 
      delete from {dailybalance_haji} where periode > to_date({tgl_awal})
    '''.format(**dictParam)
  app.ConWriteln(sSQL)
  dbutil.runSQL(config, sSQL)  

  sSQL = ''' 
    insert into {dailybalance_haji} (
    	ID_DBHAJI
    	, PERIODE
    	, NO_REKENING_BANK
    	, NOMOR_PORSI
    	, NOMOR_VALIDASI
    	, NAMA_JEMAAH
    	, NAMA_AYAH
    	, NO_IDENTITAS
    	, TEMPAT_LAHIR
    	, TGL_LAHIR
    	, JENIS_KELAMIN
    	, ALAMAT
    	, NAMA_DESA
    	, KABUPATEN
    	, PROVINSI
    	, NO_HP
    	, TGL_SET_AWAL
    	, JML_SET_AWAL
    	, NILAI_VA
    	, TGL_PELUNASAN
    	, JML_PELUNASAN
    	, JENIS_JEMAAH
    	, KODE_BANK
    	, BANK
    	, TGL_KONFIRMASI
    	, MUT_TUNDA
    	, MUT_BATAL_SA
    	, MUT_BATAL_SL
    	, MUT_BATAL_VAL
    	, GENERATED_KEY
    )
    select
    	{seq_dailybalance_haji}.nextval as id_dbhaji,
    	tx.tgl_trx as periode 
    	, ta.nomor_ref_deal as no_rekening_bank
    	, ha.nomor_porsi
    	, ha.nomor_validasi
    	, rt.nama_rekening as nama_jemaah
    	, ha.nama_ayah_kandung as nama_ayah
    	, ha.nomor_identitas as no_identitas
    	, ha.tempat_lahir 
    	, ha.tanggal_lahir as tgl_lahir 
    	, ha.jenis_kelamin 
    	, ha.alamat_rumah as alamat 
    	, ha.alamat_kelurahan as nama_desa
    	, ha.alamat_kota as kabupaten 
    	, ha.alamat_provinsi as provinsi 
    	, ha.nomor_hp as no_hp 
    	, ta.tgl_buka as tgl_set_awal 
    	, nvl(d.jml_set_awal,0) + tx.mut_sa as jml_set_awal 
    	, nvl(d.nilai_va,0) + tx.mut_va as nilai_va 
    	, ha.tgl_lunas as tgl_pelunasan 
    	, nvl(d.jml_pelunasan,0) + tx.mut_sl as jml_pelunasan 
    	, ta.kode_produk as jenis_jemaah 
    	, ta.kode_counterpart as kode_bank
    	, tc.nama_counterpart as bank 
    	, ha.tgl_konfirmasi
    	, nvl(d.mut_tunda,0) + tx.mut_tunda
    	, nvl(d.mut_batal_sa,0) + tx.mut_batal_sa
    	, nvl(d.mut_batal_sl,0) + tx.mut_batal_sl
    	, nvl(d.mut_batal_val,0) + tx.mut_batal_val
    	, ha.nomor_rekening as generated_key
    from {hajjaccount} ha 
    	inner join {rekeningtransaksi} rt on ha.nomor_rekening=rt.nomor_rekening
    	inner join {treasuryaccount} ta on ha.nomor_rekening=ta.nomor_rekening
    	inner join {treacounterpart} tc on ta.kode_counterpart=tc.kode_counterpart
    	left join (
    		SELECT dbh.* FROM {dailybalance_haji} dbh
    		INNER JOIN (
    		  SELECT GENERATED_KEY, MAX(PERIODE) PERIODE FROM {dailybalance_haji}
    		  WHERE PERIODE < TO_DATE(to_date({tgl_awal})) GROUP BY GENERATED_KEY
    		) cdb ON dbh.GENERATED_KEY=cdb.GENERATED_KEY AND cdb.PERIODE=dbh.PERIODE
    	) d on d.GENERATED_KEY=ha.nomor_rekening
    	inner join (
    		SELECT 
    		  nomor_rekening
    		  ,max(tgl_transaksi) tgl_trx
    		  ,Sum(Decode(classgroup,'tr_nominal',mutasi,0)) mut_sa 
    		  ,Sum(Decode(classgroup,'tr_lunas',mutasi,0)) mut_sl 
    		  ,Sum(Decode(classgroup,'tr_manfaat',mutasi,0)) mut_va 
    		  ,Sum(Decode(classgroup,'tr_tunda',mutasi,0)) mut_tunda 
    		  ,Sum(Decode(classgroup,'tr_batal_sa',mutasi,0)) mut_batal_sa 
    		  ,Sum(Decode(classgroup,'tr_batal_sl',mutasi,0)) mut_batal_sl 
    		  ,Sum(Decode(classgroup,'tr_batal_val',mutasi,0)) mut_batal_val 
    		FROM (
    		  SELECT dt.nomor_rekening, dtc.classgroup, Sum(Decode(jenis_mutasi,'C',nilai_mutasi,-nilai_mutasi)) mutasi, max(tr.tanggal_transaksi) tgl_transaksi 
    		  FROM {histdetiltransaksi} dt 
    		    inner join {histtransaksi} tr on dt.id_transaksi =tr.id_transaksi 
    		    inner join {detiltransaksiclass} dtc on dt.kode_tx_class=dtc.kode_tx_class
    		    inner join {hajjaccount} h on dt.nomor_rekening=h.nomor_rekening
    		  WHERE 
    		    dtc.classgroup IN ('tr_nominal','tr_lunas','tr_manfaat', 'tr_tunda', 'batal_sa', 'batal_sl', 'batal_val')
    		    and (tr.tanggal_transaksi >= to_date({tgl_awal}) and tr.tanggal_transaksi <= to_date({tgl_akhir}))
    		  GROUP BY dt.nomor_rekening, dtc.classgroup
    
    		  UNION ALL 
    
    		  SELECT dt.nomor_rekening, dtc.classgroup, Sum(Decode(jenis_mutasi,'C',nilai_mutasi,-nilai_mutasi)) mutasi, max(tr.tanggal_transaksi) tgl_transaksi 
    		  FROM {detiltransaksi} dt
    		    inner join {transaksi} tr on dt.id_transaksi =tr.id_transaksi 
    		    inner join {detiltransaksiclass} dtc on dt.kode_tx_class=dtc.kode_tx_class
    		    inner join {hajjaccount} h on dt.nomor_rekening=h.nomor_rekening
    		  WHERE 
    		    dtc.classgroup IN ('tr_nominal','tr_lunas','tr_manfaat', 'tr_tunda', 'batal_sa', 'batal_sl', 'batal_val')
    		    and (tr.tanggal_transaksi >= to_date({tgl_awal}) and tr.tanggal_transaksi <= to_date({tgl_akhir}))
    		  GROUP BY dt.nomor_rekening, dtc.classgroup
    		) x
    		GROUP BY x.nomor_rekening
    	) tx on ha.nomor_rekening=tx.nomor_rekening
    '''.format(**dictParam)
  app.ConWriteln(sSQL)
  dbutil.runSQL(config, sSQL)  

  app.ConWriteln('>>> Process selesai')
  
  return 1
  
  