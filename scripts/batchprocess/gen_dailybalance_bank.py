'''
Fungsi ini digunakan untuk generate data saldo harian.
'''
import sys
import com.ihsan.util.dbutil as dbutil
import com.ihsan.foundation.pobjecthelper as phelper
import com.ihsan.util.modman as modman
import com.ihsan.foundation.appserver as appserver

# GLOBALS
config  = appserver.ActiveConfig
app     = config.AppObject
_CONSOLE = False

def printOut(sOut):
  global app
  
  if _CONSOLE:
    print sOut
  else:
    app.ConWriteln(sOut)
  #--
    
def DAFScriptMain(config, parameter, returns):
  # config: ISysConfig object
  # parameter: TPClassUIDataPacket
  # returnpacket: TPClassUIDataPacket (undefined structure)
  
  global _CONSOLE
  
  _CONSOLE = False
  
  helper = phelper.PObjectHelper(config)
  periodHelper = helper.CreateObject('core.PeriodHelper')

  oToday = periodHelper.GetToday()

  main(config, oToday)
  returns.CreateValues(['Is_Err', 0])

  return 1

def main(config, oToday):    
  float_oToday = oToday.GetDate()
  oLastDay = oToday.GetLastDay()
  float_NextDay = oLastDay.GetDate()
  
  oProcDay = oToday.PrevWorkDay()
  fPrevDay = oProcDay.GetDate()

  #oNextDay = oToday.NextWorkDay()
  tgl_awal = config.FormatDateTime('01-mmm-yyyy', fPrevDay)
  tgl_akhir = config.FormatDateTime('dd-mmm-yyyy', fPrevDay)
  #tgl_awal = '01-DEC-2018'
  #tgl_akhir = '31-DEC-2018'
    
  app = config.AppObject
  mlu = config.ModLibUtils
  
  printOut('>> Create Daily Balance Rekening Bank')

  dictParam = {
    'tgl_awal': mlu.QuotedStr(tgl_awal),
    'tgl_akhir': mlu.QuotedStr(tgl_akhir),
  }
   
  dictParam.update(dbutil.mapDBTableNames(config, 
    [
      'rekeningtransaksi'
      , 'treasuryaccount'            
      , 'treacounterpart'
      , 'treaproduk'
      , 'transaksi'
      , 'histtransaksi'
      , 'detiltransaksi'
      , 'histdetiltransaksi'            
      , 'detiltransaksiclass'
      #, 'seq_dailybalance_bank'
      , 'dailybalance_bank'           
    ])
  )

  sSQL = ''' 
      delete from {dailybalance_bank} where periode > to_date({tgl_awal})
    '''.format(**dictParam)
  printOut(sSQL)
  dbutil.runSQL(config, sSQL)  

  sSQL = ''' 
    insert into {dailybalance_bank} (
    	PERIODE
    	, NOREK_BANK
    	, NAMA_REKENING
    	, KD_BANK
    	, NAMA_BANK
    	, JENIS_DANA
    	, CABANG
    	, KODE_VALUTA
    	, SALDO
    	, MUT_CR_SALDO
    	, MUT_DB_SALDO
    	, SALDO_MARGIN
    	, MUT_CR_SALDO_MARGIN
    	, MUT_DB_SALDO_MARGIN
    	, SALDO_BIAYA
    	, MUT_CR_SALDO_BIAYA
    	, MUT_DB_SALDO_BIAYA
    )
    select
    	tx.tgl_trx as periode 
    	, ta.nomor_rekening
    	, rt.nama_rekening
    	, ta.kode_counterpart as kode_bank
    	, tc.nama_counterpart as bank 
    	, pr.nama_produk
    	, rt.kode_cabang, rt.kode_valuta
    	--
    	, (nvl(d.saldo,0) + nvl(tx.mut_cr_saldo,0) - nvl(tx.mut_db_saldo,0)) as saldo 
    	, nvl(tx.mut_cr_saldo,0) mut_cr_saldo 
    	, nvl(tx.mut_db_saldo,0) mut_db_saldo 
    	--
    	, nvl(d.saldo_margin,0) + nvl(tx.mut_cr_margin,0) - nvl(tx.mut_db_margin,0) as saldo_margin 
    	, nvl(tx.mut_cr_margin,0) mut_cr_margin 
    	, nvl(tx.mut_db_margin,0) mut_db_margin 
    	--
    	, (nvl(d.saldo_biaya,0) + nvl(tx.mut_cr_biaya,0) - nvl(tx.mut_db_biaya,0)) as saldo_biaya 
    	, nvl(tx.mut_cr_biaya,0) mut_cr_biaya 
    	, nvl(tx.mut_db_biaya,0) mut_db_biaya 
    from {rekeningtransaksi} rt
    	inner join {treasuryaccount} ta on rt.nomor_rekening=ta.nomor_rekening and ta.JENIS_TREASURYACCOUNT='C'
    	inner join {treacounterpart} tc on ta.kode_counterpart=tc.kode_counterpart
    	inner join {treaproduk} pr on pr.kode_produk=ta.kode_produk
    	left join (
    		SELECT dbh.* FROM {dailybalance_bank} dbh
    		INNER JOIN (
    		  SELECT norek_bank, MAX(PERIODE) PERIODE FROM {dailybalance_bank}
    		  WHERE PERIODE <= TO_DATE(to_date({tgl_awal})) GROUP BY norek_bank
    		) cdb ON dbh.norek_bank=cdb.norek_bank AND cdb.PERIODE=dbh.PERIODE
    	) d on d.norek_bank=rt.nomor_rekening
    	inner join (
    		SELECT 
    		  nomor_rekening
    		  , max(tgl_transaksi) tgl_trx
    		  , Sum(case when x.jenis_mutasi='C' and x.classgroup='tr_nominal' then mutasi else 0 end) mut_cr_saldo 
    		  , Sum(case when x.jenis_mutasi='D' and x.classgroup='tr_nominal' then mutasi else 0 end) mut_db_saldo 
    		  , Sum(case when x.jenis_mutasi='C' and x.classgroup='tr_margin' then mutasi else 0 end) mut_cr_margin 
    		  , Sum(case when x.jenis_mutasi='D' and x.classgroup='tr_margin' then mutasi else 0 end) mut_db_margin 
    		  , Sum(case when x.jenis_mutasi='C' and x.classgroup in ('tr_by_adm', 'tr_by_pajak') then mutasi else 0 end) mut_cr_biaya 
    		  , Sum(case when x.jenis_mutasi='D' and x.classgroup in ('tr_by_adm', 'tr_by_pajak') then mutasi else 0 end) mut_db_biaya 
    		FROM (
    		  SELECT dt.nomor_rekening, dtc.classgroup, dt.jenis_mutasi, Sum(nilai_mutasi) mutasi, max(tr.tanggal_transaksi) tgl_transaksi 
    		  FROM {histdetiltransaksi} dt 
    		    inner join {histtransaksi} tr on dt.id_transaksi =tr.id_transaksi 
    		    inner join {detiltransaksiclass} dtc on dt.kode_tx_class=dtc.kode_tx_class
    		    inner join {treasuryaccount} h on dt.nomor_rekening=h.nomor_rekening and h.jenis_treasuryaccount='C'
    		  WHERE 
    		    dtc.classgroup IN ('tr_nominal','tr_margin','tr_by_adm', 'tr_by_pajak')
    		    and (tr.tanggal_transaksi >= to_date({tgl_awal}) and tr.tanggal_transaksi <= to_date({tgl_akhir}))
    		  GROUP BY dt.nomor_rekening, dtc.classgroup, dt.jenis_mutasi
    
    		  UNION ALL 
    
    		  SELECT dt.nomor_rekening, dtc.classgroup, dt.jenis_mutasi, Sum(nilai_mutasi) mutasi, max(tr.tanggal_transaksi) tgl_transaksi 
    		  FROM {detiltransaksi} dt
    		    inner join {transaksi} tr on dt.id_transaksi =tr.id_transaksi 
    		    inner join {detiltransaksiclass} dtc on dt.kode_tx_class=dtc.kode_tx_class
    		    inner join {treasuryaccount} h on dt.nomor_rekening=h.nomor_rekening and h.jenis_treasuryaccount='C'
    		  WHERE 
    		    dtc.classgroup IN ('tr_nominal','tr_margin','tr_by_adm', 'tr_by_pajak')
    		    and (tr.tanggal_transaksi >= to_date({tgl_awal}) and tr.tanggal_transaksi <= to_date({tgl_akhir}))
    		  GROUP BY dt.nomor_rekening, dtc.classgroup, dt.jenis_mutasi
    		) x
    		GROUP BY x.nomor_rekening
    	) tx on rt.nomor_rekening=tx.nomor_rekening
    '''.format(**dictParam)
  printOut(sSQL)
  dbutil.runSQL(config, sSQL)  

  printOut('>>> Process selesai')
  
  return 1
  
  