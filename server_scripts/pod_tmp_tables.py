import dafsys4 as dafsys
import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.cmdlineutil as podUtil
from com.ihsan.util.cmdlineutil import printOutput, readInput

ARG_LIST = [
  {'name': 'metaConfig', 'short_desc': 'config', 'long_desc': 'config is the .pcf file name, with complete extension'}
]

#def main(argValues, config = None):
def main(config):
  
  #metaConfig = argValues['metaConfig']
  #if config == None:
  #  config = podUtil.openConfig(metaConfig)
  createFinTxDetail(config)
  createMultiFinTxDetail(config)
  tr_Accrue(config)
  
def createFinTxDetail(config):
  printOutput('Creating tx detail tables...')
  try:
    dbutil.runSQL(config, '''DROP TABLE %s''' % config.MapDBTableName('tmp.ftmp_txdetail'))
  except:
    pass
  dbutil.runSQL(config, '''
      create table %s (
        kode_entri varchar(10) not null,
        id_detiltransaksi int not null,
        id_detiltransgroup int,
        id_transaksi int,
        nomor_rekening varchar(20), -- used for link with parents
        norek_type varchar(1), -- 'F' for financing, 'G' for GL
        
        kode_tx_class varchar(10),
        kode_account varchar(20), -- custom kode account for norek_type = 'G'
        kode_cabang varchar(5), -- custom cabang for norek_type = 'G'
        currency_code varchar(3), -- custom currency for norek_type = 'G' 
        kode_jurnal varchar(10),
        mnemonic varchar(1),
        
        signed_amount number(38, 16),
        amount number(38, 16),
        kode_kurs varchar(20),
        nilai_kurs number(38, 16),
        
        nomor_rekening_glaccount varchar(20), -- added automatically for norek_type = 'G' 
        
        primary key(kode_entri, id_detiltransaksi)
      )
    ''' % config.MapDBTableName('tmp.ftmp_txdetail')
  )

def createMultiFinTxDetail(config):
  printOutput('Creating tx detail tables for multiple payment...')
  try:
    dbutil.runSQL(config, '''DROP TABLE %s''' % config.MapDBTableName('tmp.ftmp_txdetailmulti'))
  except:
    pass
  dbutil.runSQL(config, '''
      create table %s (
        id_extbatch int not null,
        kode_entri varchar(10) not null,
        id_detiltransaksi int not null,
        id_detiltransgroup int,
        id_transaksi int,
        nomor_rekening varchar(20),
        
        kode_tx_class varchar(10),
        kode_jurnal varchar(10),
        mnemonic varchar(1),
        
        signed_amount number(38, 16),
        amount number(38, 16),
        kode_kurs varchar(20),
        nilai_kurs number(38, 16),
        
        primary key(id_extbatch, kode_entri, id_detiltransaksi)
      )
    ''' % config.MapDBTableName('tmp.ftmp_txdetailmulti')
  )
  
 
def tr_Accrue(config):
  printOutput('Creating temp accrue tables...')
  try:
    dbutil.runSQL(config, '''DROP TABLE %s''' % config.MapDBTableName('tmp.ftmp_treasury_accrue'))
  except:
    pass
  dbutil.runSQL(config, '''
    CREATE TABLE %s (
      nomor_rekening            VARCHAR2(20)  NOT NULL,
      accrual_type              VARCHAR2(1)   NULL,
      ujroh                     NUMBER(38,16) NULL,
      amount_accrue_day         NUMBER(38,16) NULL,
      jangka_waktu              NUMBER(38,16) NULL,
      kode_jenis                VARCHAR2(3)   NULL,
      currency_code             VARCHAR2(3)   NULL,
      kode_cabang               VARCHAR2(5)   NULL,
      kurs_booking              NUMBER(38,16) NULL,
      kurs_tt                   NUMBER(38,16) NULL,
      id_transaksi              INTEGER       NULL,
      id_detiltransgroup        INTEGER       NULL,
      ktr_detil                 VARCHAR2(100) NULL,
      ktr_transaksi             VARCHAR2(100) NULL,
      ktr_detiltransgroup       VARCHAR2(100) NULL,
      accrued_amount            NUMBER(38,16) NULL,
      mut_pymad                 NUMBER(38,16) NULL,
      mut_profit_accrue         NUMBER(38,16) NULL,
    
      primary key (nomor_rekening)
    )
    ''' % (config.MapDBTableName('tmp.ftmp_treasury_accrue'))
  )

 
#-- MAIN SCRIPT HERE

if __name__ == '__main__':
  #podUtil.printArgs()
  #argValues = podUtil.checkArgs('podTmpTables.py', ARG_LIST)
  #main(argValues)
  cfgName    = raw_input("Masukan nama config : ")
  
  DAFCONFIG_FILE = "c:\\dafapp\\ibank2\\treasury\\%s.cfg" % cfgName
  DAFUSERINFO = ["MIGRASI", "SYSTEM", "ibank2.financing", cfgName]   
  config = dafsys.OpenConfig(DAFCONFIG_FILE, DAFUSERINFO)
  main(config)
  
#--