import dafsys4
import os
import sys
import com.ihsan.util.intrdbutil as dbutil
import com.ihsan.util.ioutil as ioutil
import com.ihsan.util.cmdlineutil as podUtil
import com.ihsan.util.modman as modman
import com.ihsan.foundation.pobjecthelper as pobjecthelper

# dynamically fills-in balance fields in source table from account tables
# lsClassGroups is list of detiltransaksiclass kode_tx_class classgroup. example: ['pembayaran', 'outstanding']
# lsMutFieldNames is list of mutation fields to be initialized with zero. example: ['mut_payment_balance', 'mut_saldo']
def initBalanceAndMutationFields(config, srcTableName, lsClassGroups, lsMutFieldNames):
  
  mlu = config.ModLibUtils
  
  l = []
  for mut in lsMutFieldNames:
    l.append('%s = 0.0' % mut)
  sSQL = '''
    UPDATE %(srcTableName)s
    SET %(updList)s
  ''' % {
    'srcTableName': config.MapDBTableName(srcTableName),
    'updList': ','.join(l)
  }
  dbutil.runSQL(config, sSQL)
  
  l = []
  for cg in lsClassGroups:
    l.append(mlu.QuotedStr(cg))
    
  
  sSQL = '''
    select distinct
      dtx.accum_table_name, dtx.accum_field_name
    from 
      %(detiltransaksiclass)s dtx,
      %(rekeningtransaksi)s rt,
      %(srcTableName)s src
    where
      dtx.classgroup in (%(classgrouplist)s) AND
      dtx.subsystem_code = 'FN' AND 
      rt.nomor_rekening = src.nomor_rekening AND
      dtx.kode_jenis = rt.kode_jenis
    ''' % {
      'detiltransaksiclass': config.MapDBTableName('core.detiltransaksiclass'),
      'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
      'srcTableName': config.MapDBTableName(srcTableName),
      'classgrouplist': ','.join(l)
    }
  dTableList = {}
  # ioutil.printOutput('SQL = %s' % sSQL)
  if len(lsClassGroups) > 0:
    q = config.CreateSQL(sSQL).RawResult
    while not q.Eof:
      act = q.accum_table_name
      acf = q.accum_field_name
      if dTableList.has_key(act):
        ent = dTableList[act]
      else:
        ent = []
        dTableList[act] = ent
      ent.append(acf)
      q.Next()
    #--
  for act in dTableList.keys():
    l = []
    for acf in dTableList[act]:
      l.append('%s = updrecs.%s' % (acf, acf))
    sSQL = '''
      merge into %(srcTableName)s src
      using %(updateTableName)s updrecs
      on (updrecs.nomor_rekening = src.nomor_rekening)
      when matched then update set %(updateList)s
    ''' % {
      'srcTableName': config.MapDBTableName(srcTableName),
      'updateTableName': config.MapDBTableName(act),
      'updateList': ', '.join(l)      
    }
    dbutil.runSQL(config, sSQL)
  pass

# prepare standard transaction fields like currency_code, kode_jenis, kurs, kode_cabang, id_transaksi and id_detiltransgroup
# fill-in standard transaction fields
def prepareStdTxFields(config, srcTableName):
  sSQL = '''
    merge into %(srcTableName)s src
    using (
      SELECT
        rt.kode_jenis,
        rt.nomor_rekening,
        rt.kode_valuta,
        rt.kode_cabang,
        cu.kurs_buku,
        cu.kurs_jual_transfer
      FROM
        %(rekeningtransaksi)s rt,
        %(currency)s cu
      WHERE
        rt.kode_valuta = cu.currency_code
    ) updrecs
    on (updrecs.nomor_rekening = src.nomor_rekening)
    when matched then update set 
      kode_jenis = updrecs.kode_jenis,
      currency_code = updrecs.kode_valuta, 
      kode_cabang = updrecs.kode_cabang, 
      kurs_booking = updrecs.kurs_buku, 
      kurs_tt = updrecs.kurs_jual_transfer
  ''' % {
    'srcTableName': config.MapDBTableName(srcTableName),
    'rekeningtransaksi': config.MapDBTableName('core.rekeningtransaksi'),
    'currency': config.MapDBTableName('core.currency')
  }
  dbutil.runSQL(config, sSQL)
  
  # fill in id_transaksi and id_detiltransgroup
  sSQL = '''
    update %(srcTableName)s
    set id_transaksi = %(seq_transaksi)s.nextval, 
      id_detiltransgroup = %(seq_detiltransgroup)s.nextval
  ''' % {
    'srcTableName': config.MapDBTableName(srcTableName),
    'seq_transaksi': config.MapDBTableName('core.seq_transaksi'),
    'seq_detiltransgroup': config.MapDBTableName('core.seq_detiltransgroup')
  }
  dbutil.runSQL(config, sSQL)
#--
  
# fieldNames is list of mutation field in <srcTableName> along with its corresponding detiltransaksiclass kode_tx_class classgroup
# example: [{'field': 'mut_payment_balance', 'classgroup': 'pembayaran'}, {'field': 'mut_saldo', 'classgroup': 'outstanding'}]
def createTxDetails(config, srcTableName, kodeEntriClassParameter, fieldNames):

  mlu = config.ModLibUtils
  dbutil.runSQL(config, '''
    DELETE FROM %(ftmp_txdetail)s
    ''' % {
      'ftmp_txdetail': config.MapDBTableName('tmp.ftmp_txdetail')
    }
  )
  
  for fieldInfo in fieldNames:
    fieldName = fieldInfo['field']
    classgroup = fieldInfo['classgroup']
    sSQL = '''
      insert into %(ftmp_txdetail)s
      (
        kode_entri, 
        id_detiltransaksi,
        id_detiltransgroup,
        id_transaksi,
        nomor_rekening,
        norek_type,
        kode_tx_class,
        kode_jurnal,
        mnemonic,
        signed_amount,
        amount,
        kode_kurs,
        nilai_kurs
      )
      select 
        dtc.kode_entri,
        %(seq_detiltransaksi)s.nextval,
        ms.id_detiltransgroup,
        ms.id_transaksi,
        ms.nomor_rekening,
        'F', -- mark for financing account
        dtx.kode_tx_class,
        dtx.kode_jurnal,
        case when ms.%(fieldName)s >= 0 then 'C' else 'D' end as mnemonic,
        round( ms.%(fieldName)s ,2) as signed_amount,
        case when ms.%(fieldName)s >= 0 then round( ms.%(fieldName)s ,2) else -round( ms.%(fieldName)s ,2) end as amount,
        case when ms.currency_code = 'IDR' then NULL else dtx.kode_kurs end as kode_kurs,
        case when ms.currency_code = 'IDR' then 1.0 else 
        (
          case 
            when dtx.kode_kurs = 'BOOKING' then ms.kurs_booking
            else ms.kurs_tt
          end
        )
        end as nilai_kurs
      from
        %(srcTableName)s ms,
        %(detiltransaksiclass)s dtx,
        %(detiltransgroupclass)s dtc
      where 
        dtx.kode_jenis = ms.kode_jenis and dtx.classgroup = %(classgroup)s and
        dtc.classgroup = ms.kode_jenis and dtc.classparameter = %(kodeEntriClassParameter)s and 
        ms.%(fieldName)s <> 0.0
    ''' % {
      'ftmp_txdetail': config.MapDBTableName('tmp.ftmp_txdetail'),
      'seq_detiltransaksi': config.MapDBTableName('core.seq_detiltransaksi'),
      'fieldName': fieldName,
      'srcTableName': config.MapDBTableName(srcTableName),
      'detiltransaksiclass': config.MapDBTableName('core.detiltransaksiclass'),
      'detiltransgroupclass': config.MapDBTableName('core.detiltransgroupclass'),
      'classgroup': mlu.QuotedStr(classgroup),
      'kodeEntriClassParameter': mlu.QuotedStr(kodeEntriClassParameter)
    }
    dbutil.runSQL(config, sSQL)
  #-- for loop
#--  
  
# table that is named by srcTableName must have and define contents of the following fields: id_transaksi, id_detiltransgroup, ktr_detil, ktr_transaksi, ktr_detiltransgroup,
#   currency_code, kode_cabang, kurs_booking, kurs_tt
def createCoreTransaction(config, srcTableName, kodeEntriClassParameter, sDate, kodeEntriClassGroup = None, id_otorentri=0):
  mlu = config.ModLibUtils
  helper = pobjecthelper.PObjectHelper(config)

  # INSERT FOR NOREK_TYPE = 'F'
  sSQL = '''  
      INSERT INTO %(detiltransaksi)s (
        Id_Detil_Transaksi, Tanggal_Transaksi, Jenis_Mutasi, Nilai_Mutasi, Keterangan, 
        Kode_Cabang, Kode_Valuta, Jenis_Detil_Transaksi, Nilai_Kurs_Manual, Nilai_Ekuivalen, 
        Saldo_Awal, Saldo_Akhir, Nomor_Referensi, Kode_Kurs, Kode_Account, 
        Kode_Valuta_RAK, Kode_Jurnal, Nominal_Biaya, IsBillerTransaction, hide_passbook, 
        hide_statement, hide_gentrans, subsystem_code, sub_tx_code, kode_subtx_class, 
        Id_Transaksi, Nomor_Rekening, kode_tx_class, id_detiltransgroup, Id_Parameter_Transaksi, 
        Id_Parameter, ID_JournalBlock
      )
      SELECT 
        mpd.id_detiltransaksi, mps.tanggal_transaksi, mpd.mnemonic, mpd.amount, mps.ktr_detil, 
        mps.kode_cabang, mps.currency_code, NULL, mpd.nilai_kurs, mpd.nilai_kurs * mpd.amount, 
        NULL, NULL, 'AUTOREF', mpd.kode_kurs, gli.kode_account, 
        NULL, mpd.kode_jurnal, 0.0, 'F', 'F', 
        'F', 'F', NULL, NULL, NULL, 
        mpd.id_transaksi, mps.nomor_rekening, mpd.kode_tx_class, mps.id_detiltransgroup, NULL,
        NULL,  NULL
      FROM
        %(ftmp_txdetail)s mpd
        inner join %(srcTableName)s mps on mpd.nomor_rekening = mps.nomor_rekening and mps.id_detiltransgroup=mpd.id_detiltransgroup
        inner join %(treasuryaccount)s fa on fa.nomor_rekening = mps.nomor_rekening
        left join %(glinterface)s gli on gli.kode_produk = fa.kode_produk and gli.kode_interface = mpd.kode_tx_class
      WHERE
        mpd.norek_type = 'F'
        
  ''' % {
    'detiltransaksi': config.MapDBTableName('core.detiltransaksi'),
    'treasuryaccount': config.MapDBTableName('treasuryaccount'),
    'glinterface': config.MapDBTableName('glinterface'),
    'sDate': mlu.QuotedStr(sDate),
    'ftmp_txdetail': config.MapDBTableName('tmp.ftmp_txdetail'),
    'srcTableName': config.MapDBTableName(srcTableName)
  }
  dbutil.runSQL(config, sSQL)
  
  # INSERT FOR NOREK_TYPE = 'G'
  # check glaccount instance
  sSQL = '''
      SELECT 
        mpd.kode_account, 
        mpd.kode_cabang, 
        mpd.currency_code
      FROM %(ftmp_txdetail)s mpd
      WHERE
        mpd.norek_type = 'G' and not exists(
          SELECT 1
          FROM 
            %(glaccount)s gla,
            %(accountinstance)s ai
          WHERE 
          gla.accountinstance_id = ai.accountinstance_id
          AND ai.account_code = mpd.kode_account 
          AND ai.branch_code = mpd.kode_cabang
          AND ai.currency_code = mpd.currency_code
        )
    ''' % {
      'ftmp_txdetail': config.MapDBTableName('tmp.ftmp_txdetail'),
      'glaccount': config.MapDBTableName('core.glaccount'),
      'accountinstance': config.MapDBTableName('core.accountinstance')
    }
  q = config.CreateSQL(sSQL).RawResult
  if not q.Eof:
    raise Exception, 'Account instance not found for account = %s, branch = %s, currency = %s' % (
      str(q.kode_account), 
      str(q.kode_cabang), 
      str(q.currency_code)
    )
  sSQL = '''
      UPDATE %(ftmp_txdetail)s mpd
      SET (nomor_rekening_glaccount) = (
        SELECT 
          gla.nomor_rekening
        FROM
          %(glaccount)s gla,
          %(accountinstance)s ai
        WHERE
          gla.accountinstance_id = ai.accountinstance_id
          AND ai.account_code = mpd.kode_account 
          AND ai.branch_code = mpd.kode_cabang
          AND ai.currency_code = mpd.currency_code
      )
      WHERE
        mpd.norek_type = 'G'
    ''' % {
      'ftmp_txdetail': config.MapDBTableName('tmp.ftmp_txdetail'),
      'glaccount': config.MapDBTableName('core.glaccount'),
      'accountinstance': config.MapDBTableName('core.accountinstance')
    }
  dbutil.runSQL(config, sSQL)
  
  sSQL = '''  
      INSERT INTO %(detiltransaksi)s (
        Id_Detil_Transaksi, Tanggal_Transaksi, Jenis_Mutasi, Nilai_Mutasi, Keterangan, 
        Kode_Cabang, Kode_Valuta, Jenis_Detil_Transaksi, Nilai_Kurs_Manual, Nilai_Ekuivalen, 
        Saldo_Awal, Saldo_Akhir, Nomor_Referensi, Kode_Kurs, Kode_Account, 
        Kode_Valuta_RAK, Kode_Jurnal, Nominal_Biaya, IsBillerTransaction, hide_passbook, 
        hide_statement, hide_gentrans, subsystem_code, sub_tx_code, kode_subtx_class, 
        Id_Transaksi, Nomor_Rekening, kode_tx_class, id_detiltransgroup, Id_Parameter_Transaksi, 
        Id_Parameter, ID_JournalBlock
      )
      SELECT 
        mpd.id_detiltransaksi, mps.tanggal_transaksi, mpd.mnemonic, mpd.amount, mps.ktr_detil, 
        mpd.kode_cabang, mpd.currency_code, NULL, mpd.nilai_kurs, mpd.nilai_kurs * mpd.amount, 
        NULL, NULL, 'AUTOREF', mpd.kode_kurs, mpd.kode_account, 
        NULL, mpd.kode_jurnal, 0.0, 'F', 'F', 
        'F', 'F', NULL, NULL, NULL, 
        mpd.id_transaksi, mpd.nomor_rekening_glaccount, NULL, NULL, NULL,
        NULL,  NULL
      FROM
        %(ftmp_txdetail)s mpd,
        %(srcTableName)s mps
      WHERE
        mpd.nomor_rekening = mps.nomor_rekening
        AND mpd.norek_type = 'G'
        
  ''' % {
    'detiltransaksi': config.MapDBTableName('core.detiltransaksi'),
    'sDate': mlu.QuotedStr(sDate),
    'ftmp_txdetail': config.MapDBTableName('tmp.ftmp_txdetail'),
    'srcTableName': config.MapDBTableName(srcTableName)
  }
  dbutil.runSQL(config, sSQL)
  
  
  sSQL = '''
    INSERT INTO %(detiltransgroup)s (
      id_detiltransgroup,
      keterangan,
      tx_source_type,
      tx_source_id,
      jenis_detiltransgroup,
      kode_entri,
      Id_Transaksi,
      Nomor_Rekening
    )
    SELECT
      mps.id_detiltransgroup,
      mps.ktr_detiltransgroup,
      NULL,
      NULL,
      NULL,
      dtc.kode_entri,
      mps.id_transaksi,
      mps.nomor_rekening
    FROM   
      %(srcTableName)s mps,
      %(detiltransgroupclass)s dtc
    WHERE
  '''
  
  if kodeEntriClassGroup == None:
    sSQL = sSQL + '''
      mps.kode_jenis = dtc.classgroup and dtc.classparameter=%(kodeEntriClassParameter)s
    '''
  else:
    sSQL = sSQL + '''
      dtc.classgroup = %(kodeEntriClassGroup)s and dtc.classparameter=%(kodeEntriClassParameter)s
    '''
  sSQL = sSQL % {
    'detiltransgroup': config.MapDBTableName('core.detiltransgroup'),
    'srcTableName': config.MapDBTableName(srcTableName),
    'detiltransgroupclass': config.MapDBTableName('core.detiltransgroupclass'),
    'kodeEntriClassParameter': mlu.QuotedStr(kodeEntriClassParameter),
    'kodeEntriClassGroup': mlu.QuotedStr(str(kodeEntriClassGroup))
  }
  dbutil.runSQL(config, sSQL)

  terminal_input=terminal_otor=user_input=user_otor='SYSTEM'
  if id_otorentri>0: 
    Otr = helper.GetObject('enterprise.OtorEntri', id_otorentri)
    terminal_input = Otr.terminal_input    
    user_input     = Otr.user_input
    user_otor      = config.SecurityContext.InitUser
    terminal_otor  = config.SecurityContext.InitIP

  sSQL = '''
    INSERT INTO %(transaksi)s (
      Id_Transaksi, Tanggal_Transaksi, Nomor_Referensi, User_Input,
      Terminal_Input, User_Overide, Keterangan, Terminal_Overide,
      User_Otorisasi, Terminal_Otorisasi, Jam_Otorisasi, Status_Otorisasi,
      Jam_Input, Tanggal_Otorisasi, Biaya, Jenis_Aplikasi,
      Nilai_Transaksi, Jenis_Transaksi, Kode_Valuta_Transaksi, Kurs_Manual,
      Nilai_Ekuivalen, Is_Sudah_Dijurnal, Pajak, Zakat,
      Tanggal_Input, Kode_Cabang_Transaksi, Id_Blok_Jurnal, Id_Transaksi_Parent,
      nomor_seri, is_draft, journal_no, ID_TransaksiAgregat,
      Id_Batch_Transaksi, Kode_Transaksi, Id_Transaction_Group 
    )
    SELECT
      mps.id_transaksi, mps.tanggal_transaksi, 'SYSTEM', %(user_input)s,
      %(terminal_input)s, NULL, mps.ktr_transaksi, NULL,
      %(user_otor)s, %(terminal_otor)s, current_timestamp, 1,
      current_timestamp, current_date, 0.0, NULL,
      NULL, NULL, NULL, NULL,
      NULL, 'F', NULL, NULL,
      current_date, '000', NULL, NULL,
      'SYS' || rawtohex(%(seq_txserialnumber)s.nextval), 'F', NULL, NULL,
      NULL, dtc.kode_entri, NULL 
    FROM
      %(srcTableName)s mps,
      %(detiltransgroupclass)s dtc
    WHERE
    ''' 
  if kodeEntriClassGroup == None:
    sSQL = sSQL + '''
      mps.kode_jenis = dtc.classgroup and dtc.classparameter=%(kodeEntriClassParameter)s
    '''
  else:
    sSQL = sSQL + '''
      dtc.classgroup = %(kodeEntriClassGroup)s and dtc.classparameter=%(kodeEntriClassParameter)s
    '''
    
  sSQL = sSQL % {
      'transaksi': config.MapDBTableName('core.transaksi'),
      'sDate': mlu.QuotedStr(sDate),
      'srcTableName': config.MapDBTableName(srcTableName),
      'detiltransgroupclass': config.MapDBTableName('core.detiltransgroupclass'),
      'seq_txserialnumber': config.MapDBTableName('core.seq_txserialnumber'),
      'kodeEntriClassParameter': mlu.QuotedStr(kodeEntriClassParameter),
      'kodeEntriClassGroup': mlu.QuotedStr(str(kodeEntriClassGroup)),
      'terminal_input': mlu.QuotedStr(terminal_input),
      'terminal_otor': mlu.QuotedStr(terminal_otor),
      'user_input': mlu.QuotedStr(user_input),
      'user_otor': mlu.QuotedStr(user_otor)
    }  
    
  dbutil.runSQL(config, sSQL)
  
  sSQL = '''
    INSERT INTO %(pendingtransactionjournal)s (
      Id_Transaksi, Session_id
    )
    SELECT
      mps.id_transaksi, '%(session_id)s'
    FROM
      %(srcTableName)s mps
    ''' % {
      'srcTableName': config.MapDBTableName(srcTableName),
      'pendingtransactionjournal': config.MapDBTableName('pendingtransactionjournal'),
      'session_id': config.SecurityContext.SessionID
    }
  dbutil.runSQL(config, sSQL)
  pass

# update balance in tables from srcTableName based on fieldNames
# fieldNames is list of mutation field in <srcTableName> along with its corresponding detiltransaksiclass kode_tx_class classgroup
# example: [{'field': 'mut_payment_balance', 'classgroup': 'pembayaran'}, {'field': 'mut_saldo', 'classgroup': 'outstanding'}]
def updateBalanceFromTransaction(config, srcTableName, fieldNames):
  # create reverse index for field names
  dReverseFields = {}
  for f in fieldNames:
    dReverseFields[f['classgroup']] = f['field']
    
  # check list of transactions, get all possible tx_class, tables and field names    
  q = config.CreateSQL('''
    SELECT DISTINCT
      dtx.classgroup,
      dtx.accum_table_name,
      dtx.accum_field_name
    FROM %(ftmp_txdetail)s dt,
    %(detiltransaksiclass)s dtx
    WHERE 
    dt.kode_tx_class = dtx.kode_tx_class
  ''' % {
    'ftmp_txdetail': config.MapDBTableName('tmp.ftmp_txdetail'),
    'detiltransaksiclass': config.MapDBTableName('core.detiltransaksiclass')
    
  }).RawResult
  
  dTables = {}
  while not q.Eof:
    tbl = q.accum_table_name
    if dTables.has_key(tbl):
      ent = dTables[tbl]
    else:
      ent = []
      dTables[tbl] = ent
    srcField = dReverseFields.get(q.classgroup, None)
    if srcField == None:
      raise Exception, 'Field source not defined for classgroup %s' % q.classgroup
    accum_field_name = q.accum_field_name
    if accum_field_name != None:
      ent.append({'src': srcField, 'dst': accum_field_name})
    q.Next()
    
  mappedSrcTableName = config.MapDBTableName(srcTableName)
  for table in dTables.keys():
    ent = dTables[table]
    l = []
    l2 = []
    for e in ent:
      l.append('target.%s = nvl(target.%s,0) + updrecs.%s' % (e['dst'], e['dst'], e['src']))
      l2.append('sum(%s) as %s' % (e['src'],e['src']))
    updateList = ', \n'.join(l)
    selectList = ', \n'.join(l2)
    if len(ent) == 0:
      continue # no field to update
    sSQL = '''
      MERGE INTO %(targetTable)s target
      USING (
        SELECT 
          nomor_rekening,
          %(selectList)s
        FROM 
          %(mappedSrcTableName)s
        group by nomor_rekening
      ) updrecs
      ON (updrecs.nomor_rekening = target.nomor_rekening)
      WHEN MATCHED THEN UPDATE SET
        %(updateList)s
      ''' % {
        'targetTable': config.MapDBTableName(table),
        'mappedSrcTableName': mappedSrcTableName,
        'selectList': selectList,
        'updateList': updateList
      }
    dbutil.runSQL(config, sSQL)
  #-- for
#--    