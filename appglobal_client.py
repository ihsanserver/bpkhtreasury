class record: pass

class Application:
  def __init__(self, dafApp):
    # we control async tasks by storing their request IDs in global application object
    # after async task is completed, the request id is set to None 
    self.asyncTaskDemo_RID = None
    self.dafApp = dafApp
    self.modLU = dafApp.ModLibUtils
    
    # other initialization here
    self.loadedForms = {} # dictionary with formName: formObject pairs
    self.loadedresources = []
    self.openfunctions   = {}
    self.systemTxCodes = {}
    self.lookupForm = None
    
    clientAttrUtilClass = dafApp.GetClientClass("clientattrutil", "ClientAttrUtil")
    self.clientAttrUtil = clientAttrUtilClass()
    #--
    pass
  #--
  
  def checkLoadedForm(self, formID):
    return self.loadedForms.get(formID, None)
    
  def registerForm(self, formID, formObject):
    if self.loadedForms.get(formID) != None:
      raise Exception, "Form already registered %s" % formID
    else:
      self.loadedForms[formID] = formObject
      return formObject
    #--
  #--
  
  def unregisterForm(self, formID):
    if self.loadedForms.get(formID) != None:
      del self.loadedForms[formID]
    #--
  #--
  
  def clearLoadedForms(self):
    self.loadedForms.clear()
    
  def OnEnterNum(self, sender):
    uip = sender.owner.UIPart
    uip.Edit()
    
    numValue = uip.GetFieldValue(sender.Name)
    if numValue in (0.0, None):
      uip.SetFieldValue(sender.Name, None)
    else:
      #sender.Text = str(int(numValue))
      
      sender.Text = str("%0.2f" % float(numValue)).replace('.', ',')
    #--
  #--
  
  def OnExitNum(self, sender):
    uip = sender.owner.UIPart
    uip.Edit()

    numValue = sender.Text
    if numValue == '':
      uip.SetFieldValue(sender.Name, 0.0)
    #--
  #--
  
  def stdLookup(self, comboControl, serverLookupID, linkElmtName, displayFields, addParamFieldNames = None, dParameterValues = {}):
    # simplified interface to lookup method
    # linkElmtName: link element name (string)
    # displayFields: list of key (inputed) and displayed fields (string), separated with ";" character. key field must appear first
    # addParamFieldNames: list of field names for additional parameters (string), separated with ";"
    #   note: parameters in form linkname.fieldname will be translated into fieldname, unless a name conflict occurs
    #         the key field will always be a parameter with key field name as parameter
    ownerForm = comboControl.OwnerForm
    systemContext = ownerForm.SystemContext
    if systemContext != "":
      form_id = "%s://lookups/fGenLookup" % systemContext
    else:
      form_id = "lookups/fGenLookup"
    lookupForm = self.checkLoadedForm(form_id)
    if lookupForm == None:
      lookupForm = self.dafApp.CreateForm(form_id, "lookups.fGenLookup", 0, None, None)
      self.registerForm(form_id, lookupForm)
    #--
    self.lookupForm = lookupForm
    return lookupForm.stdLookup(comboControl, serverLookupID, linkElmtName, displayFields, addParamFieldNames, dParameterValues) 
    
  # fieldNames diisi list field. default bagian dari linkElmtname. elemen pertama
  # harus key
  def checkStdLookup(self, comboControl, linkElmtName, fieldNames):
    lsFields = []
    uip = comboControl.owner.UIPart
    uip.Edit()
    
    l = fieldNames.split(';')
    for m in l:
      m = m.strip()
      k = m.find('.')
      if k >= 0:
        dv = m.split('.')
        if dv[0] == 'uipart':
          lsFields.append(dv[1])
        else:
          lsFields.append(m)
      else:
        lsFields.append('%s.%s' % (linkElmtName, m))
    #--
    keyField = lsFields[0]
    key_val = uip.GetFieldValue(keyField)
    if key_val in ['', None, '?']:
      for field in lsFields:
        uip.SetFieldValue(field, None)
      if key_val not in ['?']:
        return 1
    return None

  def getDateTuple(self, aDate):
    mlu = self.dafApp.ModDateTime
    if aDate == None:
      aDate = mlu.Now()
    if type(aDate) is list or type(aDate) is tuple:
      return (aDate[0], aDate[1], aDate[2])
    else:
      elmts = mlu.DecodeDate(aDate)
      return (elmts[0], elmts[1], elmts[2])
    #--
  #--
    
  def lookupCustomer(self):
    lookupForm = self.checkLoadedForm("lookups.fSelectCustomer")
    if lookupForm == None:
      lookupForm = self.dafApp.CreateForm("lookups/fSelectCustomer", "lookups.fSelectCustomer", 0, None, None)
      self.registerForm("lookups.fSelectCustomer", lookupForm)
    #--
    return lookupForm.getCustomer()
  #--
  
  def lookupRekSumber(self):
    lookupForm = self.checkLoadedForm("lookups.fSelectRekSumber")
    if lookupForm == None:
      lookupForm = self.dafApp.CreateForm("lookups/fSelectRekSumber", "lookups.fSelectRekSumber", 0, None, None)
      self.registerForm("lookups.fSelectRekSumber", lookupForm)
    #--
    return lookupForm.getRekSumber()
  #--
  
  def lookupRekBank(self):
    lookupForm = self.checkLoadedForm("lookups.fSelectRekBank")
    if lookupForm == None:
      lookupForm = self.dafApp.CreateForm("lookups/fSelectRekBank", "lookups.fSelectRekBank", 0, None, None)
      self.registerForm("lookups.fSelectRekBank", lookupForm)
    #--
    return lookupForm.getRekBank()
  #--
  
  def getDateTuple(self, aDate):
    mlu = self.dafApp.ModDateTime
    if type(aDate) is list or type(aDate) is tuple:
      return (aDate[0], aDate[1], aDate[2])
    else:
      elmts = mlu.DecodeDate(aDate)
      return (elmts[0], elmts[1], elmts[2])
    #--
  #--
    
  def stdDate(self, aDate):
    datelib = self.dafApp.ModDateTime
    if type(aDate) is list or type(aDate) is tuple:
      return datelib.EncodeDate(aDate[0], aDate[1], aDate[2])
    else:
      return aDate
    #--
  #--
  
  def selectCustomer(self):
    frm = self.checkLoadedForm('fSelectCustomer')
    if frm == None:
      frm = self.dafApp.CreateForm('fSelectCustomer', 'fSelectCustomer', 0, None, None)
      self.registerForm('fSelectCustomer', frm)
    return frm.getCustomer()
  #--
  
  def addSystemTxCode(self, tx_code, is_reserved, remote_form_id, subsystem_code, is_local, session_name):
    dEntry = {'is_reserved': is_reserved, 'remote_form_id': remote_form_id, 'is_local': is_local, 'session_name': session_name} 
    self.systemTxCodes[tx_code] = dEntry 
    return dEntry
  
  def checkSystemTxCode(self, tx_code):
    return self.systemTxCodes.get(tx_code, None)
  #--
  
  # parsing kondisi validasi dengan memperhitungkan token :parameter_name untuk kondisi yang terkait dengan variabel lain
  def parseCondition(self, uipart, defValidation) :
    TOKEN_PARAM = ':'
    elmt = defValidation
    fname = elmt[0]; ftype = elmt[1] ; cond = elmt[2] ; errMsg = elmt[3]
    arrCondWords = cond.split(' ') ; idx = 0 ; value = ''
    for word in arrCondWords :
      # check token params
      if word[0] == TOKEN_PARAM :
        paramFieldName = word[1:] #without first char
        #get param value
        tmpval = uipart.GetFieldValue(paramFieldName) or ""
        if isinstance(tmpval, str) :
          arrCondWords[idx] = ''' '%s' ''' % tmpval
        else :
          arrCondWords[idx] = str(tmpval)
      else :
        pass # currently do nothing
              
      if ftype == 'str' :
        value = uipart.GetFieldValue(fname) or ""
        value = value.strip()
      elif ftype == 'num' :
        tmpval = uipart.GetFieldValue(fname)
        value = -999 if tmpval == None else tmpval          
      elif ftype == 'date' :
        tmpval = uipart.GetFieldValue(fname)
        value = -999 if tmpval == None else len(tmpval)          
      else : raise Exception, "doValidation : unknown type %s" % (ftype)         
      idx = idx + 1
    # rebuild cond
    cond = " ".join(arrCondWords)
    return cond, value
  #--
  
  ### NOTE : refactor this, so it can be used by another formvalidation globally
  # eksekusi validasi
  # return array of error messages, array kosong jika validasi sukses
  def doValidation(self, uipart, arrDefValidation) :
    arrErrMsg = []
    sDebug = ''
    for elmt in arrDefValidation :        
      cond, value = self.parseCondition(uipart, elmt)
      sEvalCondScript = '%s' % cond   
      isValidationPass = eval(sEvalCondScript) # NOTE : automatically parse with value !!!
      if not isValidationPass :
        errMsg = elmt[3]
        arrErrMsg.append(errMsg)
      sDebug += sEvalCondScript + " %s" % (value) + "\n"
    #raise Exception, sDebug
    return arrErrMsg
  #--
  
  def compareDateField(self, uipart, fieldName, csign, dliteral):
    # date literal format: 'dd-mm-yyyy'
    literals = dliteral.split('-')
    if len(literals) != 3:
      raise Exception, "invalid date literal: %s" % dliteral
    try:
      dd = int(literals[0])
      mm = int(literals[1])
      yyyy = int(literals[2])
    except:
      raise Exception, "invalid date literal: %s" % dliteral
    #--
    
    d = self.modLU.EncodeDate(yyyy, mm, dd)
    sd = self.stdDate(uipart.GetFieldByName(fieldName))
    if csign == '>':
      return sd > d
    elif csign == '>=':
      return sd >= d
    elif csign == '<':
      return sd < d  
    elif csign == '<=':
      return sd <= d  
    elif csign == '=':
      return sd == d
    else:
      raise "Unknown date operator %s" % str(csign)

  def createAndPreviewReport(self, formObject):
    formObject.CommitBuffer()
    ph = formObject.GetDataPacket()
    res = formObject.CallServerMethod('runReport', ph)
    fr = res.FirstRecord
    if fr.isErr:
      self.dafApp.ShowMessage(fr.errMsg)
    else:
      if res.packet.StreamWrapperCount > 0:
        self.previewReport(res.packet, 0, True)    
          
  def previewReport(self, packet, swname_or_idx, doNotPrint = False):
    dafApp = self.dafApp
    if type(swname_or_idx) is int:
      sw = packet.GetStreamWrapper(swname_or_idx)
    else:
      sw = packet.GetStreamWrapperByName(swname_or_idx)
    if sw == None:
      dafApp.ShowMessage("Stream not found")
      return
    #--
    tmpFileName = dafApp.GetTemporaryFileName("dl")
    extension = dafApp.GetExtensionFromMIMEType(sw.MIMEType)
    savedFileName = tmpFileName + extension
    sw.SaveToFile(savedFileName)
    if not doNotPrint:
      try:
        dafApp.ShellExecuteFile(savedFileName, "print")  
      except:
        dafApp.ShellExecuteFile(savedFileName)
      #--
    else:
      dafApp.ShellExecuteFile(savedFileName)
  #-- preview report
     
  def UserActivityLog(self, event_description, info1, info2):
    resp = self.dafApp.ExecuteScript(
              "S_UserActivityLog"
              , self.dafApp.CreateValues( 
                   ["event_description", event_description]
                   , ["info1", info1]
                   , ["info2", info2]                    
                )
          )
    
    aStatus = resp.FirstRecord
    if aStatus.Is_Err :
      raise Exception, aStatus.Err_Message
    #--
  #--

  def CloseFormDialog(self):
    return self.dafApp.ConfirmDialog('CLOSE FORM\n\nApakah Anda yakin ?') 
  #--
  
  def AuthorizeAlert(self):
    return self.dafApp.ShowMessage('AUTHORIZATION REQUIRED\n\nData berhasil disimpan.\nSilakan lakukan otorisasi.') 
  #--
  
  def SuccessInfo(self):
    return self.dafApp.ShowMessage('SUBMIT SUCCESS') 
  #--
  
#-- class  
    
  
#-- class Application
    
def OnAsyncTaskTermination(app, requestID, bError, errMessage, scriptResult):
  appObject = app.UserAppObject
  if appObject.asyncTaskDemo_RID == requestID:
    appObject.asyncTaskDemo_RID = None
    if not bError:
      rec = scriptResult.FirstRecord
      app.ShowMessage("Task is completed successfully: %d" % rec.loop_passed)
    else:
      app.ShowMessage("Task was completed with error\r\n%s" % errMessage)
  #--
#--

def OnLogout(app):
    return app.ConfirmDialog('Anda yakin untuk logout?')  